/**
 * Create by hanlu
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    //分页-当前页---当前条数
    var nowPage = 1;
    var transferNowPage = 1;//区域转派当前页数
    var leaveAllNowPage = 1;//维修员请假列表当前页数(全部小区)
    var leaveItemNowPage = 1;//维修员请假列表当前页数(维修员为综合师傅的小区/维修员为电器师傅的小区)
    var leaveItemType;//维修员请假列表  当前区域类型
    // var leaveEleNowPage=1;//维修员请假列表当前页数(维修员为电器师傅的小区)
    var batch = true;

    var chooseArea = [];//添加区域结果集
    var selectedRowIndex = 1;
    var villageIndex = 0;
    var tableArr = [];

    var listVillage;//列表选择小区数据
    var transferVillage; //转派选择小区数据
    var leaveVillage; //请假转派小区数据
    var isSearch = false;//请假弹层是否点击过搜索,根据这个数值判断保存的时候提示内容

    // 请求参数
    var leavePostData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        cityName: null, //城市
        districtName: null, //行政区
        cbdName: null, //商圈
        blockId: null, //小区id
        workerId: null, //维修员id
        operator: null, //操作人
        synthesizeWorker: null, //有无综合师傅
        eleWorker: null,//有无电器师傅
        rows: 10,
        position: leaveItemNowPage,
        skillKind: null,
        isMain: 1
    };
    var transferData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        cityName: $('#transferCity').find('option:selected').text() || null, //城市
        regionId:$('#transferCity').val(),
        districtName: null, //行政区
        cbdName: null, //商圈
        blockId: null, //小区id
        workerId: null, //维修员id
        // operator: synthesizeWorker, //操作人
        synthesizeWorker: null, //有无综合师傅
        eleWorker: null,//有无电器师傅
        rows: 50,
        position: transferNowPage,
        isMain: null
    };
    var data = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        cityName: null, //城市
        districtName: null, //行政区
        cbdName: null, //商圈
        blockId: null, //小区id
        workerId: null, //维修员id
        operator: null, //操作人
        synthesizeWorker: null, //有无综合师傅
        eleWorker: null,//有无电器师傅
        rows: 20,
        position: nowPage
    };


    flagInput = false;

    // 搜索标识
    is_search = false;
    is_transfer_search = false;
    is_leave_search = false;


    // 操作开关锁
    var transferSuo = true; //转派开关锁
    var beiyongSuo = true; //添加备用师傅开关锁

    /**从地址栏获取的参数*/
    var repairmanInfo = {};
    /**从地址栏获取的参数*/

    var comperTableData = [];//维修员请假综合区域所有数据
    var eleTableData = [];//维修员请假电器区域所有数据


    // 搜索
    $('#btnSearch').on('click', function () {
        area_list.SearchRecord();
    });
    // 刷新
    $('#btnQuery').on('click', function () {
        area_list.refresh();
    });

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
    };

    var area_list = {
        //初始化
        init: function () {
            var that = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            //判断是否有维修员区域管理权限
            utils.isUserMenuIndexRight('area_list', function (result) {
                if (result == 1) {
                    win.utils.clearSpaces();

                    that.initList();
                    var b = $(".jqGrid_wrapper").width();
                    $("#table").setGridWidth(b);
                    $("#areaTable").setGridWidth(b);
                    $("#LeaveRepairmanTable").setGridWidth(b);
                    $("#table").setGridHeight(utils.getAutoGridHeight() - 80);
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });

            /**-----------------列表搜索条件初始化开始-------------------------------------------*/
            // 获取搜索选项城市数据
            area_list.getQueryUserCity($('#city'));
            // 列表搜索选择城市事件
            $('#city').on('change', function () {
                cityId = $('#city').val();
                cityName = $('#city').find("option:selected").text();
                if (cityId) {
                    area_list.getListSearchTradingData($('#trading'), $('#business'), cityId);
                    $('.tradingBox').show();
                    $('.businessBox').show();
                    $('.searchVillageBox').hide();
                } else {
                    // utils.showMessage('请选择城市！');
                    $('.tradingBox').hide();
                    $('.businessBox').hide();
                    $('.searchVillageBox').hide();
                }

                $('#trading').html('<option value="" selected>行政区域</option>');
                $('#business').html('<option value="" selected>商圈</option>');
                $('#searchVillageSelect').html('<option value="" selected>小区</option>');
                layui.use('form', function () {
                    var form = layui.form;
                    form.render('select');
                });
                listVillage = null;
            });
            $('#trading').on('change', function () {
                $('#villageShowTextBox').removeClass('chooseVillage');

                tradingId = $('#trading').val();
                tradingName = $('#trading').find("option:selected").text();
                if (tradingId) {
                    area_list.getListSearchBusinessData($('#business'), tradingId);
                    $('.businessBox').show();
                    $('.searchVillageBox').hide();
                } else {
                    // 不选择行政区域的时候获取城市下商圈数据
                    area_list.getListSearchTradingData($('#trading'), $('#business'), cityId);
                    $('.businessBox').show();
                    $('.searchVillageBox').hide();
                }

                $('#business').html('<option value="" selected>商圈</option>');
                $('#searchVillageSelect').html('<option value="" selected>小区</option>');
                $('#villageShowTextBox').html('小区');
                layui.use('form', function () {
                    var form = layui.form;
                    form.render('select');
                });
                listVillage = null;
            });
            layui.use('form', function () {
                var form = layui.form;
                form.on('select(business)', function (data) {
                    if (data.value) {
                        businessId = data.value;
                        businessName = $('#business').find("option:selected").text();
                        if (businessId) {
                            // 回显行政区域
                            var tradingId = $('#business').find('option[value="' + data.value + '"]').attr('data-trading');
                            $('#trading').find('option[value="' + tradingId + '"]').attr('selected', true);

                            area_list.getListSearchVillageData($('#searchVillageSelect'), businessId);
                            $('.searchVillageBox').show();
                        } else {
                            $('.searchVillageBox').hide();
                        }

                        $('#searchVillageSelect').html('<option value="" selected>小区</option>');
                        $('#villageShowTextBox').html('小区');
                        layui.use('form', function () {
                            var form = layui.form;
                            form.render('select');
                        });
                        listVillage = null;
                    } else {
                        $('.searchVillageBox').hide();
                    }
                });
            });

            // area_list.getListSearchRepairman($('#searchRepairman'),'listRepairman');
            $('#searchRepairman').on('click', function () {
                $('#villageShowTextBox').removeClass('chooseVillage');
                $('#village').removeClass('showVillageBox');
            });
            $('#operator').on('click', function () {
                $('#villageShowTextBox').removeClass('chooseVillage');
                $('#village').removeClass('showVillageBox');
            });
            // 获取操作人数据
            area_list.getOperatorData();

            // 提交搜索
            $('#submit').on('click', function () {
                is_search = true;
                $("#table").jqGrid('resetSelection');
                nowPage = 1;
                area_list.searchList();
            });
            // 清空搜索条件
            $('#clear').on('click', function () {
                $("#table").jqGrid('resetSelection');

                $('#city').val('');
                $('#trading').val('');
                $('#business').val('');
                $('select[name="searchVillageSelect"]').val('');
                $('select[name="searchRepairman"]').val('');
                $('.tradingBox').hide();
                $('.businessBox').hide();
                $('.searchVillageBox').hide();
                layui.use('form', function () {
                    var form = layui.form;
                    form.render();
                });
                listVillage = null;
                $('#isComperMan').val('');
                $('#isEleMan').val('');
                $('#operator').val('');

                // 请求参数
                nowPage = 1;
                is_search = false;
                data = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    cityName: null, //城市
                    districtName: null, //行政区
                    cbdName: null, //商圈
                    blockId: null, //小区id
                    workerId: null, //维修员id
                    operator: null, //操作人
                    synthesizeWorker: null, //有无综合师傅
                    eleWorker: null,//有无电器师傅
                    rows: 20,
                    position: nowPage
                };
                area_list.getServerData(nowPage, data);
            });
            /**-----------------列表搜索条件初始化结束-------------------------------------------*/

            /**-----------------列表搜索小区相关事件开始-------------------------------------------*/
            layui.use('form', function () {
                form = layui.form;
                form.on('select(searchVillageSelect)', function (data) { // 打开控制台查看效果
                    listVillage = data.value;
                });
            });
            /**-----------------列表搜索小区相关事件结束-------------------------------------------*/


            /**-----------------区域转派搜索条件初始化开始-------------------------------------------*/
            // 获取搜索选项城市数据
            area_list.getQueryUserCity($('#transferCity'));

            // 列表搜索选择城市事件
            $('#transferCity').on('change', function () {
                cityId = $('#transferCity').val();
                cityName = $('#transferCity').find("option:selected").text();
                if (cityId) {
                    area_list.getListSearchTradingData($('#transferTrading'), $('#transferBusiness'), cityId);
                    $('.transferTradingBox').show();
                    $('.transferBusinessBox').show();
                    $('.transferVillageBox').hide();
                } else {
                    // utils.showMessage('请选择城市！');
                    $('.transferTradingBox').hide();
                    $('.transferBusinessBox').hide();
                    $('.transferVillageBox').hide();
                }

                $('#transferTrading').html('<option value="" selected>行政区域</option>');
                $('#transferBusiness').html('<option value="" selected>商圈</option>');
                $('#transferVillageSelect').html('<option value="" selected>小区</option>');
                layui.use('form', function () {
                    var form = layui.form;
                    form.render('select');
                });
                transferVillage = null;
            });
            // 列表搜索选择区域事件
            $('#transferTrading').on('change', function () {
                tradingId = $('#transferTrading').val();
                tradingName = $('#transferTrading').find("option:selected").text();
                if (tradingId) {
                    area_list.getListSearchBusinessData($('#transferBusiness'), tradingId);
                    $('.transferBusinessBox').show();
                    $('.transferVillageBox').hide();
                } else {
                    // 不选择行政区域的时候获取城市下的商圈数据
                    area_list.getListSearchTradingData($('#transferTrading'), $('#transferBusiness'), cityId);
                    $('.transferBusinessBox').show();
                    $('.transferVillageBox').hide();
                }
                $('#transferBusiness').html('<option value="" selected>商圈</option>');
                $('#transferVillageSelect').html('<option value="" selected>小区</option>');
                layui.use('form', function () {
                    var form = layui.form;
                    form.render('select');
                });
                transferVillage = null;
            });
            // 列表搜索选择商圈事件
            layui.use('form', function () {
                var form = layui.form;
                form.on('select(transferBusiness)', function (data) {
                    if (data.value) {
                        businessId = data.value;
                        businessName = $('#transferBusiness').find("option:selected").text();
                        if (businessId) {
                            // 回显行政区域
                            var transferTradingId = $('#transferBusiness').find('option[value="' + data.value + '"]').attr('data-trading');
                            $('#transferTrading').find('option[value="' + transferTradingId + '"]').attr('selected', true);

                            area_list.getListSearchVillageData($('#transferVillageSelect'), businessId);
                            $('.transferVillageBox').show();
                        } else {
                            $('.transferVillageBox').hide();
                        }
                        $('#transferVillageSelect').html('<option value="" selected>小区</option>');
                        layui.use('form', function () {
                            var form = layui.form;
                            form.render('select');
                        });
                        transferVillage = null;
                    } else {
                        $('.transferVillageBox').hide();
                    }
                });
            });


            /**-----------------区域转派搜索条件初始化结束-------------------------------------------*/

            /**-----------------区域转派搜索小区相关事件开始-------------------------------------------*/
            layui.use('form', function () {
                form = layui.form;
                form.on('select(transferVillageSelect)', function (data) { // 打开控制台查看效果
                    transferVillage = data.value;
                });
            });
            /**-----------------区域转派搜索小区相关事件结束-------------------------------------------*/


            /**-----------------请假列表搜索条件初始化开始-------------------------------------------*/
            // 获取搜索选项城市数据
            area_list.getQueryUserCity($('#leaveCity'));
            // 列表搜索选择城市事件
            $('#leaveCity').on('change', function () {
                cityId = $('#leaveCity').val();
                cityName = $('#leaveCity').find("option:selected").text();
                if (cityId) {
                    area_list.getListSearchTradingData($('#leaveTrading'), $('#leaveBusiness'), cityId);
                    $('.leaveTradingBox').show();
                    $('.leaveBusinessBox').show();
                    $('.leaveVillageBox').hide();
                } else {
                    // utils.showMessage('请选择城市！');
                    $('.leaveTradingBox').hide();
                    $('.leaveBusinessBox').hide();
                    $('.leaveVillageBox').hide();
                }

                $('#leaveTrading').html('<option value="" selected>行政区域</option>');
                $('#leaveBusiness').html('<option value="" selected>商圈</option>');
                $('#leaveVillageSelect').html('<option value="" selected>小区</option>');
                layui.use('form', function () {
                    var form = layui.form;
                    form.render('select');
                });
                leaveVillage = null;
            });
            // 列表搜索选择区域事件
            $('#leaveTrading').on('change', function () {
                $('#leaveVillageShowTextBox').removeClass('chooseVillage');

                tradingId = $('#leaveTrading').val();
                tradingName = $('#leaveTrading').find("option:selected").text();
                if (tradingId) {
                    area_list.getListSearchBusinessData($('#leaveBusiness'), tradingId);
                    $('.leaveBusinessBox').show();
                    $('.leaveVillageBox').hide();
                } else {
                    // 不选择行政区域的时候获取城市对应的商圈数据
                    area_list.getListSearchTradingData($('#leaveTrading'), $('#leaveBusiness'), cityId);
                    $('.leaveBusinessBox').show();
                    $('.leaveVillageBox').hide();
                }

                $('#leaveBusiness').html('<option value="" selected>商圈</option>');
                $('#leaveVillageSelect').html('<option value="" selected>小区</option>');
                layui.use('form', function () {
                    var form = layui.form;
                    form.render('select');
                });
                leaveVillage = null;
            });
            // 列表搜索选择商圈事件
            layui.use('form', function () {
                var form = layui.form;
                form.on('select(leaveBusiness)', function (data) {
                    if (data.value) {
                        businessId = data.value;
                        businessName = $('#leaveBusiness').find("option:selected").text();
                        if (businessId) {
                            // 回显行政区域
                            var leaveTradingId = $('#leaveBusiness').find('option[value="' + data.value + '"]').attr('data-trading');
                            $('#leaveTrading').find('option[value="' + leaveTradingId + '"]').attr('selected', true);

                            area_list.getListSearchVillageData($('#leaveVillageSelect'), businessId);
                            $('.leaveVillageBox').show();
                        } else {
                            // utils.showMessage('请选择商圈！');
                            $('.leaveVillageBox').hide();
                        }

                        $('#leaveVillageSelect').html('<option value="" selected>小区</option>');
                        layui.use('form', function () {
                            var form = layui.form;
                            form.render('select');
                        });
                        leaveVillage = null;
                    } else {
                        $('.leaveVillageBox').hide();
                    }
                });
            });

            /**-----------------请假列表搜索条件初始化结束-------------------------------------------*/

            /**-----------------请假列表搜索小区相关事件开始-------------------------------------------*/
            layui.use('form', function () {
                form = layui.form;
                form.on('select(leaveVillageSelect)', function (data) { // 打开控制台查看效果
                    leaveVillage = data.value;
                });
            });
            /**-----------------请假列表搜索小区相关事件结束-------------------------------------------*/

            // 创建区域
            $('#addAreaBtn').on('click', function () {
                area_list.createArea();
            });

            // 添加区域——显示添加区域删除操作
            $('.add-result').off('click', '.result-show-village').on('click', '.result-show-village', function () {
                area_list.deleteVillage(chooseArea, villageIndex, this);
            });

            // 创建区域 选择维修员 提交按钮
            $('#submitBtn').off('click').on('click', function () {
                var repairmanId = $('#repairman').val();
                if (repairmanId) {
                    area_list.showDetail(repairmanId);
                } else {
                    utils.showMessage('请选择维修员！');
                }
            });

            // 区域转派
            $('#transferAreaBtn').on('click', function () {
                area_list.getListSearchRepairman($('#transferRepairman'), 'transferRepairman');
                area_list.transferArea();
            });


            // 维修员请假
            $('#repairmanLeaveBtn').on('click', function () {
                area_list.repairmanLeave();
            });
            layui.use('form', function () {
                var form = layui.form;
                form.on('select(repairmanLeaveSelect)', function () {
                    isSearch = false;
                    $('#repairmanLeave').find('.infoBox').hide();
                    $('#repairmanLeave').find('.tableBox').hide();
                });
            });


            // 搜索维修员  获取维修员信息
            $('#submitRepairmanLeaveBtn').on('click', function () {
                isSearch = true;
                leaveAllNowPage = 1;
                area_list.leaveRepairmanInit();

                var repairmanId = $('#repairmanLeaveSelect').val();
                var reparimanName = $('#repairmanLeaveSelect').find('option:selected').text().split('/')[0];

                if (repairmanId) {
                    area_list.checkLeaveWorkerInfo(repairmanId);//获取维修员信息

                    $('#leaveTime').val('');
                    $('.infoBox').show();
                    $('#spareRepairmanTable').jqGrid('clearGridData');
                    leavePostData.workerId = repairmanId;

                    // 获取师傅负责区域列表
                    area_list.checkWorkerList(repairmanId, leaveAllNowPage);
                    // 查看师傅负责区域
                    $('.checkWorkerList').on('click', function () {
                        // area_list.checkWorkerList(repairmanId,leaveAllNowPage);
                        $('.repairman-detail.tableBox').show();
                    });

                    // 添加备用师傅
                    $('#addComperSpare').off('click').on('click', function () {
                        leaveItemNowPage = 1;
                        area_list.addSpareWorker(1, repairmanId, reparimanName);
                    });
                    $('#addEleSpare').off('click').on('click', function () {
                        leaveItemNowPage = 1;
                        area_list.addSpareWorker(2, repairmanId, reparimanName);
                    });
                } else {
                    utils.showMessage('请选择要请假的师傅！');
                }
            });

            $(document).keydown(function (event) {
                var e = window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if (flagInput == false) {
                        if (e.preventDefault) {
                            e.preventDefault();
                        } else {
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });
        },
        // 请假维修员信息初始化
        leaveRepairmanInit: function () {
            $('.workerName').html('');
            $('.regionName').html('');
            $('.workerNum').html('');
            $('.skillKind').html('');
            $('.status').html('');
            $('.comperSpareText').html('');
            $('.eleSpareText').html('');
        },
        initList: function () {
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                shrinkToFit: false,
                autoScroll: true,
                autowidth: false,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["id", "城市", "行政区", "商圈", "小区楼盘", "公寓数量", "师傅（综合）", "备用师傅（综合）", "师傅（电器）", "备用师傅（电器）", "更新时间", "操作人"],
                colModel: [
                    // id
                    {
                        name: "blockId",
                        index: "blockId",
                        editable: false,
                        width: 40,
                        sortable: false,
                        align: 'center',
                        hidden: true
                    },
                    // 城市
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 100,
                        sortable: false,
                        align: 'center'
                    },
                    // 行政区
                    {
                        name: "districtName",
                        index: "districtName",
                        editable: false,
                        width: 110,
                        sortable: false,
                        align: 'center',
                    },
                    // 商圈
                    {
                        name: "cbdName",
                        index: "cbdName",
                        editable: false,
                        width: 170,
                        sortable: false,
                        align: 'center',
                    },
                    // 小区楼盘
                    {
                        name: "blockName",
                        index: "blockName",
                        editable: false,
                        width: 220,
                        sortable: false,
                        align: 'center'
                    },
                    // 公寓数量
                    {
                        name: "blockNum",
                        index: "blockNum",
                        editable: false,
                        width: 160,
                        sortable: false,
                        align: 'center',
                        formatter: function (cellvalue, options, rowObject) {
                            if (rowObject.blockNum && rowObject.blockNum > 0) {
                                return '共计' + rowObject.blockNum + '个公寓';
                            } else if (!rowObject.blockNum || rowObject.blockNum <= 0) {
                                return '暂无公寓'
                            }
                        }
                    },
                    // 师傅（综合）
                    {
                        name: "syntWorker",
                        index: "syntWorker",
                        editable: false,
                        width: 140,
                        sortable: false,
                        align: 'center',
                        formatter: syntWorker
                    },
                    // 备用师傅（综合）
                    {
                        name: "backSyntWorker",
                        index: "backSyntWorker",
                        editable: false,
                        width: 140,
                        sortable: false,
                        align: 'center',
                        formatter: backSyntWorker
                    },
                    // 师傅（电器）
                    {
                        name: "eleWorker",
                        index: "eleWorker",
                        editable: false,
                        width: 140,
                        sortable: false,
                        align: 'center',
                        formatter: eleWorker
                    },
                    // 备用师傅（电器）
                    {
                        name: "backEleWorker",
                        index: "backEleWorker",
                        editable: false,
                        width: 140,
                        sortable: false,
                        align: 'center',
                        formatter: backEleWorker
                    },
                    // 更新时间
                    {
                        name: "finalTime",
                        index: "finalTime",
                        editable: false,
                        width: 200,
                        sortable: false,
                        align: 'center',
                        formatter: finalTime
                    },
                    // 操作人
                    {
                        name: "finalUser",
                        index: "finalUser",
                        editable: false,
                        width: 120,
                        sortable: false,
                        align: 'center',
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                multiselect: false,
                // 翻页
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        data.position = nowPage;
                        area_list.searchList();
                    }
                },
                // 选择每页显示条数
                beforeRequest: function () {
                    // data.rows=$('.ui-pg-selbox.form-control option:selected').text();
                    if (deferLoadData == 1) {
                        nowPage = currentPage;
                        area_list.searchList();
                        // area_list.getServerData(currentPage, data);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    data.position = nowPage;
                    area_list.searchList();
                    // area_list.getServerData(currentPage, data);
                },
                // 当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowIndex = $("#" + this.id).getGridParam('selrow');
                    // console.log(selectedRowIndex, "12345");
                    //返回点击这行xlmc的值
                    // selectedRowValue = $("#gridTable").jqGridRowValue("xlmc");
                },
            });

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });


            // 综合区域表------创建区域
            $("#compreAreaTable").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',//utils.getAutoGridHeightSong(),
                shrinkToFit: false,
                autoScroll: true,
                autowidth: false,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: -1,
                colNames: ["城市", "区域", "商圈", "小区", "公寓房源"],
                colModel: [
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 90,
                        sortable: false
                    },
                    {
                        name: "districtName",
                        index: "districtName",
                        editable: false,
                        width: 90,
                        sortable: false
                    },
                    {
                        name: "businessDistrictName",
                        index: "businessDistrictName",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    {
                        name: "blockName",
                        index: "blockName",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    {
                        name: "apartment",
                        index: "apartment",
                        editable: false,
                        width: 120,
                        sortable: false,
                        formatter: apartmentNum
                    }
                ],
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                // multiselect: true,
            });
            // 电器区域表------创建区域
            $("#eleAreaTable").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',//utils.getAutoGridHeightSong(),
                shrinkToFit: false,
                autoScroll: true,
                autowidth: false,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: -1,
                colNames: ["城市", "区域", "商圈", "小区", "公寓房源"],
                colModel: [
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 90,
                        sortable: false
                    },
                    {
                        name: "districtName",
                        index: "districtName",
                        editable: false,
                        width: 90,
                        sortable: false
                    },
                    {
                        name: "businessDistrictName",
                        index: "businessDistrictName",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    {
                        name: "blockName",
                        index: "blockName",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    {
                        name: "apartment",
                        index: "apartment",
                        editable: false,
                        width: 120,
                        sortable: false,
                        formatter: apartmentNum
                    }
                ],
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                // multiselect: true,
            });

            function apartmentNum(cellvalue, options, rowObject) {
                if (rowObject.blockNum && rowObject.blockNum > 0) {
                    return '共计' + rowObject.blockNum + '套公寓'
                } else if (!rowObject.blockNum || rowObject.blockNum <= 0) {
                    return '暂无公寓'
                }
            }

            function syntWorker(cellvalue, options, rowObject) {
                if (rowObject.syntWorker) {
                    if (rowObject.syntWorkerState == 1) {
                        return rowObject.syntWorker + '<span style="padding: 2px; background: #22cc99; margin-left: 3px; color: #fff; border-radius: 2px;">启用</span>'
                    } else if (rowObject.syntWorkerState == 2) {
                        return rowObject.syntWorker + '<span style="padding: 2px; background: #fdcf70; margin-left: 3px; color: #fff; border-radius: 2px;">休假</span>'
                    } else if (rowObject.syntWorkerState == 3) {
                        return rowObject.syntWorker + '<span style="padding: 2px; background: #ff6e6e; margin-left: 3px; color: #fff; border-radius: 2px;">禁用</span>'
                    } else {
                        return rowObject.syntWorker;
                    }
                } else {
                    return ''
                }
            };

            function backSyntWorker(cellvalue, options, rowObject) {
                if (rowObject.backSyntWorker) {
                    if (rowObject.backSyntWorkerState == 1) {
                        return rowObject.backSyntWorker + '<span style="padding: 2px; background: #22cc99; margin-left: 3px; color: #fff; border-radius: 2px;">启用</span>'
                    } else if (rowObject.backSyntWorkerState == 2) {
                        return rowObject.backSyntWorker + '<span style="padding: 2px; background: #fdcf70; margin-left: 3px; color: #fff; border-radius: 2px;">休假</span>'
                    } else if (rowObject.backSyntWorkerState == 3) {
                        return rowObject.backSyntWorker + '<span style="padding: 2px; background: #ff6e6e; margin-left: 3px; color: #fff; border-radius: 2px;">禁用</span>'
                    } else {
                        return rowObject.backSyntWorker;
                    }
                } else {
                    return ''
                }
            };

            function eleWorker(cellvalue, options, rowObject) {
                if (rowObject.eleWorker) {
                    if (rowObject.eleWorkerState == 1) {
                        return rowObject.eleWorker + '<span style="padding: 2px; background: #22cc99; margin-left: 3px; color: #fff; border-radius: 2px;">启用</span>'
                    } else if (rowObject.eleWorkerState == 2) {
                        return rowObject.eleWorker + '<span style="padding: 2px; background: #fdcf70; margin-left: 3px; color: #fff; border-radius: 2px;">休假</span>'
                    } else if (rowObject.eleWorkerState == 3) {
                        return rowObject.eleWorker + '<span style="padding: 2px; background: #ff6e6e; margin-left: 3px; color: #fff; border-radius: 2px;">禁用</span>'
                    } else {
                        return rowObject.eleWorker;
                    }
                } else {
                    return ''
                }
            };

            function backEleWorker(cellvalue, options, rowObject) {
                if (rowObject.backEleWorker) {
                    if (rowObject.backEleWorkerState == 1) {
                        return rowObject.backEleWorker + '<span style="padding: 2px; background: #22cc99; margin-left: 3px; color: #fff; border-radius: 2px; ">启用</span>'
                    } else if (rowObject.backEleWorkerState == 2) {
                        return rowObject.backEleWorker + '<span style="padding: 2px; background: #fdcf70; margin-left: 3px; color: #fff; border-radius: 2px;">休假</span>'
                    } else if (rowObject.backEleWorkerState == 3) {
                        return rowObject.backEleWorker + '<span style="padding: 2px; background: #ff6e6e; margin-left: 3px; color: #fff; border-radius: 2px;">禁用</span>'
                    } else {
                        return rowObject.backEleWorker
                    }
                } else {
                    return ''
                }
            };

            function finalTime(cellvalue, options, rowObject) {
                if (rowObject.finalTime) {
                    var data = area_list.timeStampFormat(rowObject.finalTime);
                    return data;
                } else {
                    return '';
                }

            };


            // 搜索小区添加区域
            $("#searchVillageTable").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 400,//utils.getAutoGridHeightSong(),
                shrinkToFit: false,
                autoScroll: true,
                autowidth: false,
                rowNum: -1,
                colNames: ["小区"],
                colModel: [
                    {
                        name: "village",
                        index: "village",
                        editable: false,
                        width: 400,
                        sortable: false
                    }
                ],
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                multiselect: true,//复选框
            });


            // 区域转派列表
            $("#areaTable").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                shrinkToFit: false,
                autoScroll: true,
                autowidth: false,
                rowNum: 50,
                rownumbers: true,
                rownumWidth: 60,
                rowList: [50, 100, 200],
                pgtext: "第X页 / 共X页",
                colNames: ["id", "城市", "行政区", "商圈", "小区楼盘", "公寓数量", "师傅（综合）", "备用师傅（综合）", "师傅（电器）", "备用师傅（电器）"],
                colModel: [
                    // id
                    {
                        name: "blockId",
                        index: "blockId",
                        editable: false,
                        width: 40,
                        sortable: false,
                        align: 'center',
                        hidden: true
                    },
                    // 城市
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 100,
                        sortable: false,
                        align: 'center'
                    },
                    // 行政区
                    {
                        name: "districtName",
                        index: "districtName",
                        editable: false,
                        width: 120,
                        sortable: false,
                        align: 'center',
                    },
                    // 商圈
                    {
                        name: "cbdName",
                        index: "cbdName",
                        editable: false,
                        width: 180,
                        sortable: false,
                        align: 'center',
                    },
                    // 小区楼盘
                    {
                        name: "blockName",
                        index: "blockName",
                        editable: false,
                        width: 240,
                        sortable: false,
                        align: 'center'
                    },
                    // 公寓数量
                    {
                        name: "blockNum",
                        index: "blockNum",
                        editable: false,
                        width: 200,
                        sortable: false,
                        align: 'center',
                        formatter: function (cellvalue, options, rowObject) {
                            if (rowObject.blockNum && rowObject.blockNum > 0) {
                                return '共计' + rowObject.blockNum + '个公寓';
                            } else if (!rowObject.blockNum || rowObject.blockNum <= 0) {
                                return '暂无公寓'
                            }
                        }
                    },
                    // 师傅（综合）
                    {
                        name: "syntWorker",
                        index: "syntWorker",
                        editable: false,
                        width: 180,
                        sortable: false,
                        align: 'center',
                        formatter: syntWorker
                    },
                    // 备用师傅（综合）
                    {
                        name: "backSyntWorker",
                        index: "backSyntWorker",
                        editable: false,
                        width: 180,
                        sortable: false,
                        align: 'center',
                        formatter: backSyntWorker
                    },
                    // 师傅（电器）
                    {
                        name: "eleWorker",
                        index: "eleWorker",
                        editable: false,
                        width: 180,
                        sortable: false,
                        align: 'center',
                        formatter: eleWorker
                    },
                    // 备用师傅（电器）
                    {
                        name: "backEleWorker",
                        index: "backEleWorker",
                        editable: false,
                        width: 180,
                        sortable: false,
                        align: 'center',
                        formatter: backEleWorker
                    },
                ],
                pager: "#areaTable_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                multiselect: true,//复选框
                // 翻页
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    transferCurrentPage = $("#areaTable").jqGrid('getGridParam', 'page');
                    transferLastPage = $("#areaTable").jqGrid('getGridParam', 'lastpage');
                    if (pgButton == 'next') {
                        transferCurrentPage = transferCurrentPage + 1;
                    }
                    if (pgButton == 'last') {
                        transferCurrentPage = transferLastPage;
                    }
                    if (pgButton == 'prev') {
                        transferCurrentPage = transferCurrentPage - 1;
                    }
                    if (pgButton == 'first') {
                        transferCurrentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    transferNowPage = transferCurrentPage;
                    if (deferLoadData == 0) {
                        transferData.position = transferNowPage;
                        area_list.searchTransferList();
                    }
                },
                // 选择每页显示条数
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        transferCurrentPage = $("#areaTable").jqGrid('getGridParam', 'page');
                        transferLastPage = $("#areaTable").jqGrid('getGridParam', 'lastpage');
                        pageCount = $("#areaTable").jqGrid('getGridParam', 'rowNum');
                        if (transferNowPage <= transferLastPage) {
                            transferCurrentPage = 1;
                            transferNowPage = 1;
                        }

                        transferNowPage = transferCurrentPage;
                        transferData.position = transferNowPage;
                        area_list.searchTransferList();
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    transferCurrentPage = $("#areaTable").jqGrid('getGridParam', 'page');
                    transferNowPage = transferCurrentPage;
                    area_list.searchTransferList();
                },
                // 当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowIndex = $("#" + this.id).getGridParam('selrow');
                    console.log(selectedRowIndex, "12345");
                    //返回点击这行xlmc的值
                    // selectedRowValue = $("#gridTable").jqGridRowValue("xlmc");
                },
            });

            $("#areaTable").jqGrid("navGrid", "#areaTable_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });


            // 备用师傅表
            $("#spareRepairmanTable").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                shrinkToFit: false,
                autoScroll: true,
                autowidth: false,
                rowNum: 10,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [10, 20, 50, 100],
                colNames: ["id", "城市", "行政区", "商圈", "小区楼盘", "公寓数量", "师傅（综合）", "备用师傅（综合）", "师傅（电器）", "备用师傅（电器）"],
                colModel: [
                    // id
                    {
                        name: "blockId",
                        index: "blockId",
                        editable: false,
                        width: 40,
                        sortable: false,
                        align: 'center',
                        hidden: true
                    },
                    // 城市
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 60,
                        sortable: false,
                        align: 'center'
                    },
                    // 行政区
                    {
                        name: "districtName",
                        index: "districtName",
                        editable: false,
                        width: 80,
                        sortable: false,
                        align: 'center',
                    },
                    // 商圈
                    {
                        name: "cbdName",
                        index: "cbdName",
                        editable: false,
                        width: 100,
                        sortable: false,
                        align: 'center',
                    },
                    // 小区楼盘
                    {
                        name: "blockName",
                        index: "blockName",
                        editable: false,
                        width: 180,
                        sortable: false,
                        align: 'center'
                    },
                    // 公寓数量
                    {
                        name: "blockNum",
                        index: "blockNum",
                        editable: false,
                        width: 140,
                        sortable: false,
                        align: 'center',
                        formatter: function (cellvalue, options, rowObject) {
                            if (rowObject.blockNum && rowObject.blockNum > 0) {
                                return '共计' + rowObject.blockNum + '个公寓';
                            } else if (!rowObject.blockNum || rowObject.blockNum <= 0) {
                                return '暂无公寓'
                            }
                        }
                    },
                    // 师傅（综合）
                    {
                        name: "syntWorker",
                        index: "syntWorker",
                        editable: false,
                        width: 120,
                        sortable: false,
                        align: 'center',
                        formatter: syntWorker
                    },
                    // 备用师傅（综合）
                    {
                        name: "backSyntWorker",
                        index: "backSyntWorker",
                        editable: false,
                        width: 140,
                        sortable: false,
                        align: 'center',
                        formatter: backSyntWorker
                    },
                    // 师傅（电器）
                    {
                        name: "eleWorker",
                        index: "eleWorker",
                        editable: false,
                        width: 120,
                        sortable: false,
                        align: 'center',
                        formatter: eleWorker
                    },
                    // 备用师傅（电器）
                    {
                        name: "backEleWorker",
                        index: "backEleWorker",
                        editable: false,
                        width: 140,
                        sortable: false,
                        align: 'center',
                        formatter: backEleWorker
                    },
                ],
                pager: "#spareRepairmanTable_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                // 翻页
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    leaveAllCurrentPage = $("#spareRepairmanTable").jqGrid('getGridParam', 'page');
                    leaveAllLastPage = $("#spareRepairmanTable").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        leaveAllCurrentPage = leaveAllCurrentPage + 1;
                    }
                    if (pgButton == 'last') {
                        leaveAllCurrentPage = leaveAllLastPage;
                    }
                    if (pgButton == 'prev') {
                        leaveAllCurrentPage = leaveAllCurrentPage - 1;
                    }
                    if (pgButton == 'first') {
                        leaveAllCurrentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    leaveAllNowPage = leaveAllCurrentPage;
                    if (deferLoadData == 0) {
                        // 请求参数
                        var datas = {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            cityName: null, //城市
                            districtName: null, //行政区
                            cbdName: null, //商圈
                            blockId: null, //小区id
                            workerId: $('#repairmanLeaveSelect').val(), //维修员id
                            operator: null, //操作人
                            synthesizeWorker: null, //有无综合师傅
                            eleWorker: null,//有无电器师傅
                            rows: 10,
                            position: leaveAllNowPage,
                            skillKind: null,
                            isMain: 1
                        };
                        area_list.checkWorkerList($('#repairmanLeaveSelect').val(), leaveAllNowPage);
                    }
                },
                // 选择每页显示条数
                beforeRequest: function () {
                    // data.rows=$('.ui-pg-selbox.form-control option:selected').text();
                    if (deferLoadData == 1) {
                        leaveAllNowPage = leaveAllCurrentPage;
                        // area_list.getServerData(leaveAllCurrentPage, data);
                        // 请求参数
                        var datas = {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            cityName: null, //城市
                            districtName: null, //行政区
                            cbdName: null, //商圈
                            blockId: null, //小区id
                            workerId: $('#repairmanLeaveSelect').val(), //维修员id
                            operator: null, //操作人
                            synthesizeWorker: null, //有无综合师傅
                            eleWorker: null,//有无电器师傅
                            rows: 10,
                            position: leaveAllNowPage,
                            skillKind: null,
                            isMain: 1
                        };
                        area_list.checkWorkerList($('#repairmanLeaveSelect').val(), leaveAllNowPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    leaveAllCurrentPage = $("#spareRepairmanTable").jqGrid('getGridParam', 'page');
                    leaveAllNowPage = leaveAllCurrentPage;
                    // 请求参数
                    var datas = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        cityName: null, //城市
                        districtName: null, //行政区
                        cbdName: null, //商圈
                        blockId: null, //小区id
                        workerId: $('#repairmanLeaveSelect').val(), //维修员id
                        operator: null, //操作人
                        synthesizeWorker: null, //有无综合师傅
                        eleWorker: null,//有无电器师傅
                        rows: 10,
                        position: leaveAllNowPage,
                        skillKind: null,
                        isMain: 1
                    };
                    area_list.checkWorkerList($('#repairmanLeaveSelect').val(), leaveAllNowPage);
                },
                // 当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowIndex = $("#" + this.id).getGridParam('selrow');
                    console.log(selectedRowIndex, "12345");
                    //返回点击这行xlmc的值
                    // selectedRowValue = $("#gridTable").jqGridRowValue("xlmc");
                },
            });
            $("#spareRepairmanTable").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            // 维修员请假列表
            $("#LeaveRepairmanTable").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                shrinkToFit: false,
                autoScroll: true,
                autowidth: false,
                rowNum: 10,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [10, 20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["id", "城市", "行政区", "商圈", "小区楼盘", "公寓数量", "师傅（综合）", "备用师傅（综合）", "师傅（电器）", "备用师傅（电器）"],
                colModel: [
                    // id
                    {
                        name: "blockId",
                        index: "blockId",
                        editable: false,
                        width: 40,
                        sortable: false,
                        align: 'center',
                        hidden: true
                    },
                    // 城市
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 60,
                        sortable: false,
                        align: 'center'
                    },
                    // 行政区
                    {
                        name: "districtName",
                        index: "districtName",
                        editable: false,
                        width: 80,
                        sortable: false,
                        align: 'center',
                    },
                    // 商圈
                    {
                        name: "cbdName",
                        index: "cbdName",
                        editable: false,
                        width: 100,
                        sortable: false,
                        align: 'center',
                    },
                    // 小区楼盘
                    {
                        name: "blockName",
                        index: "blockName",
                        editable: false,
                        width: 180,
                        sortable: false,
                        align: 'center'
                    },
                    // 公寓数量
                    {
                        name: "blockNum",
                        index: "blockNum",
                        editable: false,
                        width: 140,
                        sortable: false,
                        align: 'center',
                        formatter: function (cellvalue, options, rowObject) {
                            if (rowObject.blockNum && rowObject.blockNum > 0) {
                                return '共计' + rowObject.blockNum + '个公寓';
                            } else if (!rowObject.blockNum || rowObject.blockNum <= 0) {
                                return '暂无公寓'
                            }
                        }

                    },
                    // 师傅（综合）
                    {
                        name: "syntWorker",
                        index: "syntWorker",
                        editable: false,
                        width: 120,
                        sortable: false,
                        align: 'center',
                        formatter: syntWorker
                    },
                    // 备用师傅（综合）
                    {
                        name: "backSyntWorker",
                        index: "backSyntWorker",
                        editable: false,
                        width: 140,
                        sortable: false,
                        align: 'center',
                        formatter: backSyntWorker
                    },
                    // 师傅（电器）
                    {
                        name: "eleWorker",
                        index: "eleWorker",
                        editable: false,
                        width: 120,
                        sortable: false,
                        align: 'center',
                        formatter: eleWorker
                    },
                    // 备用师傅（电器）
                    {
                        name: "backEleWorker",
                        index: "backEleWorker",
                        editable: false,
                        width: 140,
                        sortable: false,
                        align: 'center',
                        formatter: backEleWorker
                    },
                ],
                pager: "#leaveTable_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                multiselect: true,//复选框
                // 翻页
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    leaveItemCurrentPage = $("#LeaveRepairmanTable").jqGrid('getGridParam', 'page');
                    lastPage = $("#LeaveRepairmanTable").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        leaveItemCurrentPage = leaveItemCurrentPage + 1;
                    }
                    if (pgButton == 'last') {
                        leaveItemCurrentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        leaveItemCurrentPage = leaveItemCurrentPage - 1;
                    }
                    if (pgButton == 'first') {
                        leaveItemCurrentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    leaveItemNowPage = leaveItemCurrentPage;
                    if (deferLoadData == 0) {
                        leavePostData.position = leaveItemNowPage;
                        area_list.LeaveSearchList(leavePostData);
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    leaveItemCurrentPage = $("#LeaveRepairmanTable").jqGrid('getGridParam', 'page');
                    leaveItemNowPage = leaveItemCurrentPage;

                    leavePostData.position = leaveItemNowPage;
                    area_list.LeaveSearchList(leavePostData);
                },
                // 当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowIndex = $("#" + this.id).getGridParam('selrow');
                    console.log(selectedRowIndex, "12345");
                    //返回点击这行xlmc的值
                    // selectedRowValue = $("#gridTable").jqGridRowValue("xlmc");
                },
            });
            $("#LeaveRepairmanTable").jqGrid("navGrid", "#areaTable_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });


            $(win).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#areaTable").setGridWidth(b);
                $("#LeaveRepairmanTable").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight() - 74);
            });

            area_list.getParamsData();
        },
        getParamsData: function () {
            // 获取地址栏中的参数
            repairmanInfo = JSON.parse(window.localStorage.getItem('repairmanInfo'));
            // console.log('111'+repairmanInfo)
            if (!repairmanInfo) {
                var data = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    cityName: null, //城市
                    districtName: null, //行政区
                    cbdName: null, //商圈
                    blockId: null, //小区id
                    workerId: null, //维修员id
                    operator: null, //操作人
                    synthesizeWorker: null, //有无综合师傅
                    eleWorker: null,//有无电器师傅
                    rows: 20,
                    position: nowPage
                };
                area_list.getListSearchRepairman($('#searchRepairman'), 'listRepairman');
                area_list.getServerData(nowPage, data);
            } else {
                window.localStorage.removeItem('repairmanInfo');//清空缓存数据

                layer.closeAll();
                setTimeout(function () {
                    // 查看服务区域
                    if (repairmanInfo.type == 1) {
                        $('#city').val('');
                        $('#trading').val('');
                        $('#business').val('');
                        $('#searchVillageSelect').val('');
                        $('#isComperMan').val('');
                        $('#isEleMan').val('');
                        $('#isEleMan').val('');
                        layui.use('form', function () {
                            var form = layui.form;
                            form.render('select');
                        });

                        $('.tradingBox').hide();
                        $('.businessBox').hide();
                        $('.searchVillageBox').hide();


                        // 清空列表数据，每次请求重新渲染数据
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: 0
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                        $('.totalNum').text(0);

                        nowPage = 1;
                        is_search = true;
                        listVillage = null;
                        area_list.getListSearchRepairman($('#searchRepairman'), 'searchRepairman');
                    }
                    // 请假
                    else if (repairmanInfo.type == 2) {
                        area_list.repairmanLeave(repairmanInfo); //调用请假方法，模拟请假点击事件
                    }
                }, 200);
            }
        },
        // 获取列表数据
        getServerData: function (pageIndex, postData) {
            is_search = false;
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(
                win.utils.services.getAreaWorkerList,
                postData,
                function (result) {
                    if (result.result_code == 200) {
                        if (result.total == 0) {
                            gridData = {};
                            var gridJson = {
                                total: 0,
                                rows: gridData,
                                page: 0,
                                records: result.total
                            };
                            pageText = '第0页 / 共0页';
                            $("#table").jqGrid('setGridParam', {
                                pgtext: pageText
                            });
                            $("#table")[0].addJSONData(gridJson);
                            $('.totalNum').text(result.total);

                            utils.showMessage('暂无数据！');
                        } else {
                            gridData = result.areaWorkerList;
                            totalPages = Math.ceil(result.total / pageCount);
                            var gridJson = {
                                total: totalPages,
                                rows: gridData,
                                page: pageIndex,
                                records: result.total
                            };
                            pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';


                            $("#table").jqGrid('setGridParam', {
                                pgtext: pageText
                            });
                            $("#table")[0].addJSONData(gridJson);
                            $('.totalNum').text(result.total);

                        }
                    } else {
                        utils.showMessage('获取数据失败，请重试！');
                    }
                });
        },
        // 搜索
        SearchRecord: function () {
            var that = this;
            initializeEditForm();
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '<i class="fa fa-search">&nbsp;&nbsp;搜索评论</i>',
                content: $('#model'),
                shadeClose: true,
                btn: ['保存', '取消'],
                yes: function (index, layero) {
                    var createTime = area_list.dateFormat($('#timeChoose').val()) || null;

                    data = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        city: document.selected.city.value || null, //城市
                        orderId: document.selected.orderId.value || null, //工单id
                        type: document.selected.type.value || null, //工单类型
                        houseSite: document.selected.houseSite.value || null, //公寓地址
                        houseId: document.selected.houseId.value || null, //公寓id1
                        roomNumber: document.selected.roomNumber.value || null, //房间号
                        clientType: document.selected.clientType.value || null, //客户类型
                        clientName: document.selected.clientName.value || null, //客户姓名
                        creatTimeStart: createTime ? createTime[0] : null, //创建开始时间
                        creatTimeEnd: createTime ? createTime[1] : null, //创建结束时间
                        responsible: document.selected.responsible.value || null, //责任人姓名
                        position: nowPage, //当前页
                        rows: 20
                    };
                    that.getServerData(nowPage, data);
                }
            });
        },
        refresh: function () {
            $('#city').val('');
            $('#orderId').val('');
            $('#type').val('');
            $('#houseSite').val('');
            $('#houseId').val('');
            $('#roomNumber').val('');
            $('#clientType').val('');
            $('#clientName').val('');
            $('#creatTimeStart').val('');
            $('#responsible').val('');
            data = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                city: null, //城市
                clientName: null, //客户姓名
                clientType: null, //客户类型
                creatTimeEnd: null, //创建结束时间
                creatTimeStart: null, //创建开始时间
                houseId: null, //公寓id
                houseSite: null, //公寓地址
                orderId: null, //工单id
                position: nowPage, //当前页
                responsible: null, //责任人姓名
                roomNumber: null, //房间号
                type: null, //工单类型
                rows: 20
            };
            area_list.getServerData(nowPage, data);
        },


        /**-----------------列表相关方法开始-------------------------------------------*/
        // 获取列表搜索城市数据
        getListSearchCityData: function (nodeName) {
            win.utils.ajaxPost(win.utils.services.getSysArea, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                levelFlag: 1
            }, function (result) {
                if (result.result_code == 200) {
                    var cityOptionStr = '<option value="">城市</option>';
                    if (nodeName.attr('id') == 'transferCity') {
                        cityOptionStr = '';
                    }
                    result.areaVOList.forEach(function (i, k) {
                        cityOptionStr += '<option value="' + i.id + '">' + i.addressName + '</option>';
                    });
                    nodeName.html(cityOptionStr);
                    // if(nodeName.attr('id')=='transferCity'){
                    //     nodeName.find('option').eq(1).attr('selected',true);
                    // }
                }
            })
        },
        // 查询当前管理员负责城市
        getQueryUserCity: function (nodeName) {
            win.utils.ajaxPost(win.utils.services.queryUserCity, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                // levelFlag:1
                userId: win.utils.getCookie('user_id')
            }, function (result) {
                if (result.result_code == 200) {
                    var cityOptionStr = '<option value="">城市</option>';
                    if (nodeName.attr('id') == 'transferCity') {
                        cityOptionStr = '';
                    }
                    result.sysUserCityVOS.forEach(function (i, k) {
                        cityOptionStr += '<option value="' + i.cityId + '">' + i.addressName + '</option>';
                    });
                    nodeName.html(cityOptionStr);
                    // if(nodeName.attr('id')=='transferCity'){
                    //     nodeName.find('option').eq(1).attr('selected',true);
                    // }
                }
            })
        },
        // 获取列表搜索行政区域数据
        getListSearchTradingData: function (areaName, cbdName, cityId) {
            win.utils.ajaxPost(win.utils.services.getSysAreaResultMap, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                levelFlag: 3,
                cityId: cityId
            }, function (result) {
                if (result.result_code == 200) {
                    var tradingOptionStr = '<option value="">行政区域</option>';
                    result.data[2].forEach(function (i, k) {
                        tradingOptionStr += '<option value="' + i.id + '">' + i.addressName + '</option>';
                    });
                    areaName.html(tradingOptionStr);

                    var businessOptionStr = '<option value="">商圈</option>';
                    result.data[3].forEach(function (i, k) {
                        businessOptionStr += '<option value="' + i.id + '" data-trading="' + i.parentId + '">' + i.addressName + '</option>';
                    });
                    cbdName.html(businessOptionStr);

                    layui.use('form', function () {
                        form = layui.form;
                        form.render('select');
                    });
                }
            });
        },
        // 获取列表搜索商圈数据
        getListSearchBusinessData: function (nodeName, districtId) {
            win.utils.ajaxPost(win.utils.services.getSysArea, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                levelFlag: 3,
                districtId: districtId
            }, function (result) {
                if (result.result_code == 200) {
                    var businessOptionStr = '<option value="">商圈</option>';
                    result.areaVOList.forEach(function (i, k) {
                        businessOptionStr += '<option value="' + i.id + '">' + i.addressName + '</option>';
                    });
                    nodeName.html(businessOptionStr);
                    layui.use('form', function () {
                        form = layui.form;
                        form.render('select');
                    });
                }
            })
        },
        // 获取列表搜索小区数据
        getListSearchVillageData: function (nodeName, businessId) {
            win.utils.ajaxPost(win.utils.services.getSysArea, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                levelFlag: 4,
                businessDistrictId: businessId
            }, function (result) {
                if (result.result_code == 200) {
                    var villageOptionStr = '<option value="">小区</option>';
                    result.areaVOList.forEach(function (i, k) {
                        villageOptionStr += '<option value="' + i.id + '">' + i.addressName + '</option>';
                    });
                    nodeName.html(villageOptionStr);
                    layui.use('form', function () {
                        form = layui.form;
                        form.render();
                        // form.render('select');
                    });
                }
            })
        },
        // 获取师傅数据
        getListSearchRepairman: function (nodeName, filterName, data) {
            win.utils.ajaxPost(win.utils.services.getWorker, data ? data : {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                isInservice: 1,
            }, function (result) {
                if (result.result_code == 200) {
                    var repairmanOptionStr = '<option value="">请选择维修员</option>';

                    result.workerInfoVOS.forEach(function (i, k) {
                        repairmanOptionStr += '<option value="' + i.workerId + '">' + i.workerName + '/' + i.workerPhone + '</option>'
                    });
                    nodeName.html(repairmanOptionStr);
                    layui.use('form', function () {
                        form = layui.form;
                        form.render('select');
                    });

                    // 如果地址栏中有workerid参数，从维修员列表跳过来的，搜索显示对应维修员
                    if (repairmanInfo && repairmanInfo.workerId && repairmanInfo.type == 1) {
                        // console.log(repairmanInfo)
                        is_search = true;
                        var paramsWorkerName;
                        var repairmanOpitons = $('#searchRepairman').find('option');
                        for (var i = 0; i < repairmanOpitons.length; i++) {
                            if ($(repairmanOpitons[i]).val() == repairmanInfo.workerId) {
                                paramsWorkerName = $(repairmanOpitons[i]).text().split('/')[0];
                            }
                        }
                        $('#searchRepairman').val(repairmanInfo.workerId);
                        $('#searchRepairman').find("option[text=" + paramsWorkerName + "]").attr('selected', true);
                        layui.use('form', function () {
                            var form = layui.form;
                            form.render('select');
                        });

                        is_search = true;
                        nowPage = 1;

                        $('#table').jqGrid('clearGridData');
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: 0
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                        $('.totalNum').text(0);


                        area_list.searchList();
                    }
                }
            })
        },
        // 获取操作员数据
        getOperatorData: function () {
            win.utils.ajaxPost(win.utils.services.queryUser, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
            }, function (result) {
                if (result.result_code == 200) {
                    var operatorStr = '<option value="">操作人</option>';
                    result.data.forEach(function (i, k) {
                        operatorStr += '<option value="' + i.userId + '">' + i.userName + '</option>';
                    });
                    $('#operator').html(operatorStr);
                }
            })
        },
        // 获取代理执行人数据
        getTransferProxy: function (nodeName, filterName, data) {
            win.utils.ajaxPost(win.utils.services.getTransferProxy, data, function (result) {
                if (result.result_code == 200) {
                    var repairmanOptionStr = '<option value="">请选择维修员</option>';

                    result.infoVOList.forEach(function (i, k) {
                        repairmanOptionStr += '<option value="' + i.workerId + '">' + i.workerName + '/' + i.workerPhone + '</option>'
                    });
                    nodeName.html(repairmanOptionStr);
                    layui.use('form', function () {
                        form = layui.form;
                        form.render();
                    });

                    // 如果地址栏中有workerid参数，从维修员列表跳过来的，搜索显示对应维修员
                    // if(paramsWorkerId && paramsType==1){
                    //     var repairmanOptionNodes=$('#searchRepairman').find('option');
                    //     for(var i=0;i<repairmanOptionNodes.length;i++){
                    //         var value=$(repairmanOptionNodes[i]).val();
                    //         if(value==paramsWorkerId){
                    //             $(repairmanOptionNodes[i]).attr('selected',true);
                    //         }
                    //     }
                    // }
                }
            })
        },

        // 列表搜索选择小区
        villageChange: function (villageId, villageName, divNode) {
            if (villageTextArr.length) {
                var isSet = false;
                villageTextArr.forEach(function (i, k) {
                    if ($(i).attr('data-villageId') == villageId) {
                        isSet = true;
                        utils.showMessage('您已选择过改小区，请重新选择!');
                    }
                });
                if (!isSet) {
                    villageTextArr.push('<div class="result-show-village" data-villageId="' + villageId + '">\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t<span>' + villageName + '</span>\n' +
                        '\t\t\t\t\t\t\t\t\t\t\t<img src="../../images/close.png" alt="">\n' +
                        '\t\t\t\t</div>');
                }
            } else {
                villageTextArr.push('<div class="result-show-village" data-villageId="' + villageId + '">\n' +
                    '\t\t\t\t\t\t\t\t\t\t\t<span>' + villageName + '</span>\n' +
                    '\t\t\t\t\t\t\t\t\t\t\t<img src="../../images/close.png" alt="">\n' +
                    '\t\t\t\t</div>');
            }

            divNode.html(villageTextArr.join(''));
        },

        searchList: function () {
            if (is_search) {
                var cityName = $('#city').find('option:selected').text() || null;
                var districtName = $('#trading').find('option:selected').text() || null;
                var cbdName = $('#business').find('option:selected').text() || null;

                var workerId = $('#searchRepairman').val() || null;
                var synthesizeWorker = $('#isComperMan').val() || null;
                var eleWorker = $('#isEleMan').val() || null;
                var operator = $('#operator').val() || null;

                data.cityName = cityName == "城市" ? null : cityName;
                data.districtName = districtName == "行政区域" ? null : districtName;
                data.cbdName = cbdName == "商圈" ? null : cbdName;
                data.blockId = listVillage ? (listVillage.length ? listVillage : null) : null;
                data.workerId = workerId;
                data.operator = operator;
                data.synthesizeWorker = synthesizeWorker;
                data.eleWorker = eleWorker;
                data.position = nowPage;
            }
            area_list.getServerData(nowPage, data);
        },

        /**-----------------列表相关方法结束------------------------------------------*/


        /**-----------------创建区域相关方法开始-------------------------------------------*/
        // 获取维修员数据
        getRepairmanData: function () {
            win.utils.ajaxPost(win.utils.services.getWorker, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                isInservice: 1
            }, function (result) {
                if (result.result_code == 200) {
                    var optionStr = '<option value="">请选择维修员</option>';
                    result.workerInfoVOS.forEach(function (i, k) {
                        optionStr += '<option value="' + i.workerId + '">' + i.workerName + '/' + i.workerPhone + '</option>'
                    });
                    $('#repairman').html(optionStr);
                    // form.render('select','repairman');
                    layui.use('form', function () {
                        form = layui.form;
                        form.render('select');
                    });
                }
            })
        },
        // 选择维修员，展示维修员详情区域
        showDetail: function (id) {
            $('.repairman_select').addClass('showDetail');
            $('.repairman-detail').show();

            var areaGrideData = [];
            var areaGridJson = {
                rows: areaGrideData
            };
            $("#compreAreaTable")[0].addJSONData(areaGridJson);
            $("#eleAreaTable")[0].addJSONData(areaGridJson);

            win.utils.ajaxPost(win.utils.services.getWorker, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                workerId: id,
                isInservice: 1
            }, function (result) {
                if (result.result_code == 200) {
                    $('.repairman-info').find('.workerName').text(result.workerInfoVOS[0].workerName);
                    $('.repairman-info').find('.regionName').text(result.workerInfoVOS[0].regionName);
                    $('.repairman-info').find('.workerNum').text(result.workerInfoVOS[0].workerId);
                    var status;
                    if (result.workerInfoVOS[0].workerStatus == 1) {
                        status = '<span style="padding: 2px 5px;color: #fff;background: #22cc99; border-radius: 2px;">启用</span>'
                    } else if (result.workerInfoVOS[0].workerStatus == 2) {
                        status = '<span style="padding: 2px 5px;color: #fff;background: #fdcf70; border-radius: 2px;">休假</span>'
                    } else if (result.workerInfoVOS[0].workerStatus == 3) {
                        status = '<span style="padding: 2px 5px;color: #fff;background: #ff6e6e; border-radius: 2px;">禁用</span>'
                    }
                    $('.repairman-info').find('.status').html(status);

                    var userInfo = {
                        workerId: result.workerInfoVOS[0].workerId,
                        workerName: result.workerInfoVOS[0].workerName,
                        // skillKind: result.workerInfoVOS[0].skillKind==1?'综合':result.workerInfoVOS[0].skillKind==2?'电器':'综合 电器',
                        skillKind: result.workerInfoVOS[0].skillKind,
                        cityId: result.workerInfoVOS[0].regionId,
                        cityName: result.workerInfoVOS[0].regionName
                    };

                    // 维修员技能——综合
                    if (result.workerInfoVOS[0].skillKind == 1) {
                        area_list.getArea(result.workerInfoVOS[0].workerId, 1, $("#compreAreaTable"));
                        $('.compreAreaBox').show();
                        $('.eleAreaBox').hide();
                    }
                    // 维修员技能——电器
                    else if (result.workerInfoVOS[0].skillKind == 2) {
                        area_list.getArea(result.workerInfoVOS[0].workerId, 2, $("#eleAreaTable"));
                        $('.eleAreaBox').show();
                        $('.compreAreaBox').hide();
                    } else {
                        area_list.getArea(result.workerInfoVOS[0].workerId, 1, $("#compreAreaTable"));
                        area_list.getArea(result.workerInfoVOS[0].workerId, 2, $("#eleAreaTable"));
                        $('.compreAreaBox').show();
                        $('.eleAreaBox').show();
                    }

                    // 添加区域按钮绑定事件
                    $('#addCompreAreaBtn').off('click').on('click', function () {
                        // 添加区域之前获取已添加区域，并在添加页面显示
                        win.utils.ajaxPost(win.utils.services.queryEditWorkerArea, {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            workerId: userInfo.workerId,
                            skillKind: 1,
                            skillType: 1,
                        }, function (result) {
                            if (result.result_code == 200) {
                                chooseArea = result.editQueryAreas;
                                area_list.addAreaResultOpt(chooseArea, villageIndex); //获取已添加区域成功，调取添加结果显示方法
                                area_list.addZoning(1, userInfo); //显示添加区域弹层
                            }
                        });

                    });
                    $('#addEleAreaBtn').off('click').on('click', function () {
                        // 添加区域之前获取已添加区域，并在添加页面显示
                        win.utils.ajaxPost(win.utils.services.queryEditWorkerArea, {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            workerId: userInfo.workerId,
                            skillKind: 2,
                            skillType: 1,
                        }, function (result) {
                            if (result.result_code == 200) {
                                chooseArea = result.editQueryAreas;
                                area_list.addAreaResultOpt(chooseArea, villageIndex);
                                area_list.addZoning(2, userInfo);
                            }
                        });
                    });
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            })
        },
        // 创建区域
        createArea: function () {
            area_list.createLayerInit();//弹层初始化
            var i = layer.open({
                type: 1,
                area: ['60%', '80%'],
                title: '<i class="fa fa-plus">&nbsp;&nbsp;创建区域</i>',
                content: $('#createArea'),
                shadeClose: true,
                success: function () {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);

                    flagInput = false;

                    area_list.getRepairmanData();
                },
            });
        },
        // 创建区域弹层初始化
        createLayerInit: function () {
            $('.repairman_select').removeClass('showDetail');
            $('.repairman-detail').hide();
            $('.compreAreaBox').hide();
            $('.eleAreaBox').hide();
        },
        // 获取维修员负责区域
        getArea: function (workerId, skillKind, node) {
            var data = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                workerId: workerId,
                skillKind: skillKind
            }
            win.utils.ajaxPost(win.utils.services.queryWorkerBlockArea, data, function (result) {
                if (result.result_code == 200) {
                    if (result.areas.length) {
                        var areaGrideData = result.areas;
                        var areaGridJson = {
                            rows: areaGrideData
                        };
                        node[0].addJSONData(areaGridJson);
                    } else {
                        var areaGrideData = [];
                        var areaGridJson = {
                            rows: areaGrideData
                        };
                        node[0].addJSONData(areaGridJson);
                    }
                }
            });
        },
        /**-----------------创建区域相关方法结束-------------------------------------------*/
        /**-----------------增加区域相关方法开始-------------------------------------------*/
        // 增加区域
        //type-------添加区域类型  1 综合  2 电器
        //obj--------维修员信息资料   workerId 维修员id   workerName 维修员姓名   skillKind 维修员技能类型  cityId 维修员城市id  cityName 维修员城市名称
        addZoning: function (type, obj) {
            var i = layer.open({
                type: 1,
                area: ['100%', '100%'],
                title: "添加区域",
                content: $('#addArea'),
                shadeClose: true,
                btn: ['添加', '取消'],
                success: function () {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);

                    flagInput = false;

                    // 获取弹层高度，赋值给添加区域显示盒子
                    var layerHeight = $(".layui-layer").height() - 200;
                    $('.add-result').css('height', layerHeight);

                    area_list.addAreaLayerShow();

                    $('.repairman-info').find('.addAreaCity').text(obj.cityName);
                    $('.repairman-info').find('.addAreaName').text(obj.workerName);
                    $('.repairman-info').find('.addAreaSkill').text(obj.skillKind == 1 ? '综合' : obj.skillKind == 2 ? '电器' : '综合 电器');


                    obj.skillType = type;
                    area_list.areaAddHandle(chooseArea, obj, type);
                    // 按照区域级别添加
                    $('#areaAdd').on('click', function () {
                        $('#searchAdd').removeClass('btn-primary');
                        $(this).addClass('btn-primary');
                        $('.area-add').show();
                        $('.search-add').hide();
                        $('.trading').removeClass('showBox');
                        $('.village').removeClass('showBox');
                        area_list.areaAddHandle(chooseArea, obj, type);
                    });
                    // 按照搜索小区添加
                    $('#searchAdd').on('click', function () {
                        $('#areaAdd').removeClass('btn-primary');
                        $(this).addClass('btn-primary');
                        $('.area-add').hide();
                        $('.search-add').show();

                        var gridJson = {
                            rows: [],
                        };
                        $("#searchVillageTable")[0].addJSONData(gridJson);


                        // 取消水平滚动条
                        $("#searchVillageTable").closest(".ui-jqgrid-bdiv").css({'overflow-x': 'hidden'});
                        // 增加垂直滚动条
                        $("#searchVillageTable").closest(".ui-jqgrid-bdiv").css({'overflow-y': 'scroll'});
                        $('#searchVillage').off('click').on('click', function () {
                            area_list.areaAddBySearch(chooseArea, obj, type);
                        });

                    });
                },
                yes: function (index, layero) {
                    var idArr = [];
                    chooseArea.forEach(function (areaI, areaK) {
                        chooseArea[areaK].trading.forEach(function (tradingI, tradingK) {
                            chooseArea[areaK].trading[tradingK].village.forEach(function (villageI, villageK) {
                                idArr.push(villageI.id);
                            })
                        })
                    });
                    win.utils.ajaxPost(win.utils.services.updateWorkerArea, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        blockIds: idArr,
                        workerId: obj.workerId,
                        skillKind: type,
                        workerName: obj.workerName
                    }, function (result) {
                        if (result.result_code == 200) {
                            layer.close(i);
                            // 添加成功之后重新渲染详情表格
                            area_list.showDetail(obj.workerId);
                        } else {
                            utils.showMessage('添加失败，请重试！');
                        }
                    });
                }
            });
        },
        // 获取区域数据
        getAreaData: function (data, fn) {
            win.utils.ajaxPost(win.utils.services.getSysAreaSort, data, fn);
        },
        // 按照区域层级添加区域操作
        areaAddHandle: function (chooseArea, userInfo, type) {
            var postData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                levelFlag: 2,
                workerId: userInfo.workerId,
                skillType: type
            };
            var districtId; //区域id
            var districtName; //区域名
            var businessDistrictId; //商圈id
            var businessDistrictName; //商圈名
            var villageId;//小区id
            var villageName;//小区名

            // 获取区域数据
            area_list.getAreaData(postData, function (areaRes) {
                if (areaRes.result_code == 200) {
                    if (areaRes.areaSortVOS.length) {
                        var areaStr = '';
                        areaRes.areaSortVOS.forEach(function (i, k) {
                            if (i.sysAreaVOS.length) {
                                i.sysAreaVOS.forEach(function (sysI, sysK) {
                                    areaStr += '<div data-districtId="' + sysI.id + '">' + sysI.addressName + '<span></span></div>';
                                })
                            }

                        });
                        $('.zoning .itemBox').html(areaStr);
                        $('.zoning').addClass('showBox');
                        // 区域数字角标显示
                        if (chooseArea.length) {
                            var index = area_list.findNodeIndex($('.zoning .itemBox'), 'data-districtid', chooseArea[0].id);
                            if (index) {
                                $('.zoning .itemBox').find('div').removeClass('checkedLeval').eq(index).addClass('checkedLeval');
                            }
                            var areaNodes = $('.areaBox .itemBox').find('div');
                            for (var i = 0; i < areaNodes.length; i++) {
                                if ($(areaNodes[i]).attr('data-districtid') == chooseArea[0].id) {
                                    $(areaNodes[i]).addClass('checkedLeval');
                                }
                            }
                        }
                        chooseArea.forEach(function (historyAreaI, historyAreaK) {
                            area_list.areaSpanShow({id: historyAreaI.id, num: historyAreaI.trading.length});
                        });

                        // 点击选择区域
                        $('.zoning .itemBox').off('click', 'div').on('click', 'div', function () {
                            $('.trading').removeClass('showBox');
                            $('.village').removeClass('showBox');
                            districtId = $(this).attr("data-districtId"); //区域id
                            districtName = $(this).text();
                            var areaIndex = $(this).index();


                            $('.zoning .itemBox div').removeClass('checkedLeval').eq(areaIndex).addClass('checkedLeval');

                            postData.levelFlag = 3;
                            postData.districtId = districtId;
                            // 获取商圈数据
                            area_list.getAreaData(postData, function (tradingRes) {
                                if (tradingRes.result_code == 200) {
                                    if (tradingRes.areaSortVOS.length) {
                                        var tradingStr = '';
                                        tradingRes.areaSortVOS.forEach(function (i, k) {
                                            if (i.sysAreaVOS.length) {
                                                var itemStr = '<span class="letter">' + i.letter + '</span>';
                                                i.sysAreaVOS.forEach(function (sysI, sysK) {
                                                    itemStr += '<div data-businessDistrictId ="' + sysI.id + '">' + sysI.addressName + '<span></span></div>'
                                                });
                                                tradingStr += itemStr;
                                            }

                                            // tradingStr += '<div data-businessDistrictId ="'+i.id+'">'+i.addressName+'<span></span></div>';
                                        });
                                        $('.trading .itemBox').html(tradingStr);
                                        $('.trading').addClass('showBox');
                                        // 商圈默认选择显示数字角标显示
                                        chooseArea.forEach(function (historyAreaI, historyAreaK) {
                                            if (historyAreaI.trading.length) {
                                                var index = area_list.findNodeIndex($('.trading .itemBox'), 'data-businessdistrictid', historyAreaI.trading[0].id);
                                                if (index) {
                                                    $('.trading .itemBox').find('div').removeClass('checkedLeval').eq(index).addClass('checkedLeval');
                                                }
                                            }
                                            if (historyAreaI.id == districtId) {
                                                chooseArea[historyAreaK].trading.forEach(function (historyTradingI, historyTradingK) {
                                                    if (historyTradingI.village.length) {
                                                        area_list.tradingSpanShow({
                                                            id: historyTradingI.id,
                                                            num: historyTradingI.village.length
                                                        });
                                                    }
                                                })
                                            }
                                        });

                                        // 点击选择商圈
                                        $('.trading .itemBox').off('click', 'div').on('click', 'div', function () {
                                            $('.village').removeClass('showBox');
                                            businessDistrictId = $(this).attr("data-businessDistrictId"); //商圈id
                                            businessDistrictName = $(this).text();//商圈名

                                            $('.trading .itemBox div').removeClass('checkedLeval');
                                            $(this).addClass('checkedLeval');

                                            postData.levelFlag = 4;
                                            postData.businessDistrictId = businessDistrictId;
                                            postData.districtId = null;

                                            // 获取小区数据
                                            area_list.getAreaData(postData, function (villageRes) {
                                                if (villageRes.result_code == 200) {
                                                    if (villageRes.areaSortVOS.length) {
                                                        var villageStr = '<div data-villageId="0" class="selectAll">全选</div>'; //小区全选按钮
                                                        var villageSpanShowArr = [];
                                                        villageRes.areaSortVOS.forEach(function (i, k) {
                                                            if (i.sysAreaVOS.length) {
                                                                var itemStr = '<span class="letter">' + i.letter + '</span>';
                                                                i.sysAreaVOS.forEach(function (sysI, sysK) {
                                                                    itemStr += '<div data-villageId ="' + sysI.id + '">' + sysI.addressName + '<span></span></div>'
                                                                });
                                                                villageStr += itemStr;
                                                            }

                                                        });

                                                        $('.village .itemBox').html(villageStr);
                                                        $('.village').addClass('showBox');

                                                        // 小区数量
                                                        var villageNum = $('.village .itemBox').find('div').length - 1;

                                                        var villageNodes = $('.village .itemBox').find('div');
                                                        for (var nodeI = 0; nodeI < villageNodes.length; nodeI++) {
                                                            var villageId = $(villageNodes[nodeI]).attr('data-villageId');
                                                            // 商圈下对应已选择的小区展示
                                                            chooseArea.forEach(function (historyAreaI, historyAreaK) {
                                                                if (historyAreaI.id == districtId) {
                                                                    historyAreaI.trading.forEach(function (historyTradingI, historyTradingK) {
                                                                        if (historyTradingI.id == businessDistrictId) {
                                                                            if (historyTradingI.village.length == villageNum) {
                                                                                var villageNodes = $('.village .itemBox').find('div');
                                                                                for (var i = 0; i < villageNodes.length; i++) {
                                                                                    $(villageNodes[i]).addClass('checked');
                                                                                }
                                                                                $(villageNodes[0]).text('取消全选');
                                                                            } else {
                                                                                historyTradingI.village.forEach(function (historyVillageI, historyVillageK) {
                                                                                    if (historyVillageI.id == villageId) {
                                                                                        $('.village .itemBox').find('div').eq(nodeI).addClass('checked');
                                                                                        villageSpanShowArr.push({index: nodeI});
                                                                                    }
                                                                                })
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            })
                                                        }

                                                        // if(villageSpanShowArr.length){
                                                        //     // 选择的小区数量等于所有小区的数量——全选
                                                        //     if(villageSpanShowArr.length==villageNum){
                                                        //         var villageNodes = $('.village .itemBox').find('div');
                                                        //         for(var i=0; i<villageNodes.length; i++){
                                                        //             $(villageNodes[i]).addClass('checked');
                                                        //         }
                                                        //         $(villageNodes[0]).text('取消全选');
                                                        //     }
                                                        //     else{
                                                        //         villageSpanShowArr.forEach(function (villageSpanI,villageSpanK) {
                                                        //             $('.village .itemBox').find('div').eq(villageSpanI.index+1).addClass('checked');
                                                        //         });
                                                        //     }
                                                        // }


                                                        // 选择小区
                                                        $('.village .itemBox').off('click', 'div').on('click', 'div', function () {
                                                            villageId = $(this).attr('data-villageId');//小区id
                                                            villageName = $(this).text();//小区名
                                                            // 全选
                                                            if (villageId == 0) {
                                                                var villageNodes = $('.village .itemBox').find('div');
                                                                if ($(this).hasClass('checked')) {
                                                                    for (var i = 0; i < villageNodes.length; i++) {
                                                                        $(villageNodes[i]).removeClass('checked');
                                                                    }
                                                                    $(villageNodes[0]).text('全选');

                                                                    chooseArea.forEach(function (vi, vk) {
                                                                        if (vi.id == districtId) {
                                                                            chooseArea[vk].trading.forEach(function (vii, vkk) {
                                                                                if (vii.id == businessDistrictId) {
                                                                                    chooseArea[vk].trading[vkk].village = [];
                                                                                    // 当小区数量为0时 删除对应小区
                                                                                    if (!chooseArea[vk].trading[vkk].village.length) {
                                                                                        chooseArea[vk].trading.splice(vkk, 1);
                                                                                    }
                                                                                    if (!chooseArea[vk].trading.length) {
                                                                                        chooseArea.splice(vk, 1);
                                                                                    }
                                                                                }

                                                                            });
                                                                        }
                                                                    });

                                                                    area_list.addAreaResultOpt(chooseArea, villageIndex);
                                                                    area_list.spanNumShow(chooseArea);
                                                                } else {
                                                                    for (var i = 0; i < villageNodes.length; i++) {
                                                                        $(villageNodes[i]).addClass('checked');
                                                                    }
                                                                    $(villageNodes[0]).text('取消全选');
                                                                    if (chooseArea.length) {
                                                                        isSetArea = false;
                                                                        isSetTrading = false;
                                                                        chooseArea.forEach(function (areaI, areaK) {
                                                                            if (areaI.id == districtId) {
                                                                                isSetArea = true;
                                                                                chooseArea[areaK].trading.forEach(function (tradingI, tradingK) {
                                                                                    if (tradingI.id == businessDistrictId) {
                                                                                        isSetTrading = true;
                                                                                        for (var i = 1; i < villageNodes.length; i++) {
                                                                                            villageId = $(villageNodes[i]).attr('data-villageId');
                                                                                            villageName = $(villageNodes[i]).text();//小区名

                                                                                            if (JSON.stringify(chooseArea[areaK].trading[tradingK].village).indexOf(JSON.stringify({
                                                                                                id: villageId,
                                                                                                name: villageName
                                                                                            })) == -1) {
                                                                                                chooseArea[areaK].trading[tradingK].village.push({
                                                                                                    id: villageId,
                                                                                                    name: villageName
                                                                                                });
                                                                                            }
                                                                                        }

                                                                                    }
                                                                                })
                                                                                if (!isSetTrading) {
                                                                                    var villageArr = [];
                                                                                    for (var i = 1; i < villageNodes.length; i++) {
                                                                                        villageId = $(villageNodes[i]).attr('data-villageId');
                                                                                        villageName = $(villageNodes[i]).text();//小区名


                                                                                        villageArr.push({
                                                                                            id: villageId,
                                                                                            name: villageName
                                                                                        });
                                                                                    }
                                                                                    chooseArea[areaK].trading.push({
                                                                                        id: businessDistrictId,
                                                                                        name: businessDistrictName,
                                                                                        village: villageArr
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                        if (!isSetArea) {
                                                                            var villageArr = [];
                                                                            for (var i = 1; i < villageNodes.length; i++) {
                                                                                villageId = $(villageNodes[i]).attr('data-villageId');
                                                                                villageName = $(villageNodes[i]).text();//小区名


                                                                                villageArr.push({
                                                                                    id: villageId,
                                                                                    name: villageName
                                                                                });
                                                                            }
                                                                            chooseArea.push({
                                                                                id: districtId,
                                                                                name: districtName,
                                                                                trading: [
                                                                                    {
                                                                                        id: businessDistrictId,
                                                                                        name: businessDistrictName,
                                                                                        village: villageArr
                                                                                    }
                                                                                ]
                                                                            });
                                                                        }
                                                                    } else {
                                                                        var villageArr = [];
                                                                        for (var i = 1; i < villageNodes.length; i++) {
                                                                            villageId = $(villageNodes[i]).attr('data-villageId');
                                                                            villageName = $(villageNodes[i]).text();//小区名


                                                                            villageArr.push({
                                                                                id: villageId,
                                                                                name: villageName
                                                                            });
                                                                        }
                                                                        chooseArea.push({
                                                                            id: districtId,
                                                                            name: districtName,
                                                                            trading: [
                                                                                {
                                                                                    id: businessDistrictId,
                                                                                    name: businessDistrictName,
                                                                                    village: villageArr
                                                                                }
                                                                            ]
                                                                        });
                                                                    }
                                                                    area_list.addAreaResultOpt(chooseArea, villageIndex);
                                                                    area_list.spanNumShow(chooseArea);
                                                                }

                                                            } else {
                                                                // 删除小区
                                                                if ($(this).hasClass('checked')) {
                                                                    $('.village .itemBox').find('div').eq(0).removeClass('checked'); //取消全选
                                                                    $('.village .itemBox').find('div').eq(0).text('全选');
                                                                    // villageIndex--;
                                                                    $(this).removeClass('checked');
                                                                    chooseArea.forEach(function (vi, vk) {
                                                                        if (vi.id == districtId) {
                                                                            chooseArea[vk].trading.forEach(function (vii, vkk) {
                                                                                if (vii.id == businessDistrictId) {
                                                                                    chooseArea[vk].trading[vkk].village.forEach(function (vtvi, vivk) {
                                                                                        if (vtvi.id == villageId) {
                                                                                            chooseArea[vk].trading[vkk].village.splice(vivk, 1);
                                                                                            // 当小区数量为0时 删除对应小区
                                                                                            if (!chooseArea[vk].trading[vkk].village.length) {
                                                                                                chooseArea[vk].trading.splice(vkk, 1);
                                                                                            }
                                                                                            if (!chooseArea[vk].trading.length) {
                                                                                                chooseArea.splice(vk, 1);
                                                                                            }
                                                                                        }
                                                                                    })
                                                                                }

                                                                            });
                                                                        }
                                                                    });
                                                                    console.log(chooseArea);

                                                                    area_list.addAreaResultOpt(chooseArea, villageIndex);
                                                                    area_list.spanNumShow(chooseArea);
                                                                }
                                                                // 添加小区
                                                                else {
                                                                    // villageIndex++;
                                                                    $(this).addClass('checked');
                                                                    if (chooseArea.length) {
                                                                        isSetArea = false;
                                                                        isSetTrading = false;
                                                                        chooseArea.forEach(function (areaI, areaK) {
                                                                            if (areaI.id == districtId) {
                                                                                isSetArea = true;
                                                                                chooseArea[areaK].trading.forEach(function (tradingI, tradingK) {
                                                                                    if (tradingI.id == businessDistrictId) {
                                                                                        isSetTrading = true;
                                                                                        if (JSON.stringify(chooseArea[areaK].trading[tradingK].village).indexOf(JSON.stringify({
                                                                                            id: villageId,
                                                                                            name: villageName
                                                                                        })) == -1) {
                                                                                            chooseArea[areaK].trading[tradingK].village.push({
                                                                                                id: villageId,
                                                                                                name: villageName
                                                                                            });
                                                                                        }

                                                                                        if (tradingI.village.length == villageSpanShowArr.length) {
                                                                                            $('.village .itemBox').find('div').eq(0).addClass('checked'); //取消全选
                                                                                            $('.village .itemBox').find('div').eq(0).text('取消全选');
                                                                                        }
                                                                                    }
                                                                                })
                                                                                if (!isSetTrading) {
                                                                                    chooseArea[areaK].trading.push({
                                                                                        id: businessDistrictId,
                                                                                        name: businessDistrictName,
                                                                                        village: [{
                                                                                            id: villageId,
                                                                                            name: villageName
                                                                                        }]
                                                                                    });
                                                                                }
                                                                            }
                                                                        });
                                                                        if (!isSetArea) {
                                                                            chooseArea.push({
                                                                                id: districtId,
                                                                                name: districtName,
                                                                                trading: [
                                                                                    {
                                                                                        id: businessDistrictId,
                                                                                        name: businessDistrictName,
                                                                                        village: [
                                                                                            {
                                                                                                id: villageId,
                                                                                                name: villageName
                                                                                            }
                                                                                        ]
                                                                                    }
                                                                                ]
                                                                            });
                                                                        }
                                                                    } else {
                                                                        chooseArea.push({
                                                                            id: districtId,
                                                                            name: districtName,
                                                                            trading: [
                                                                                {
                                                                                    id: businessDistrictId,
                                                                                    name: businessDistrictName,
                                                                                    village: [
                                                                                        {
                                                                                            id: villageId,
                                                                                            name: villageName
                                                                                        }
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        });
                                                                    }

                                                                    area_list.addAreaResultOpt(chooseArea, villageIndex);
                                                                    area_list.spanNumShow(chooseArea);

                                                                    chooseArea.forEach(function (areaI, areaK) {
                                                                        if (areaI.id == districtId) {
                                                                            chooseArea[areaK].trading.forEach(function (tradingI, tradingK) {
                                                                                if (tradingI.id == businessDistrictId) {
                                                                                    if (tradingI.village.length == villageNum) {
                                                                                        $('.village .itemBox').find('div').eq(0).addClass('checked'); //取消全选
                                                                                        $('.village .itemBox').find('div').eq(0).text('取消全选');
                                                                                    }
                                                                                }
                                                                            })
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        utils.showMessage('该商圈暂无小区！');
                                                    }
                                                } else {
                                                    utils.showMessage('获取小区数据失败，请重试！');
                                                }

                                            })
                                        });
                                    } else {
                                        utils.showMessage('该区域下暂无商圈！')
                                    }
                                } else {
                                    utils.showMessage('获取商圈数据失败，请重试！');
                                }

                            })
                        })
                    } else {
                        utils.showMessage('该城市暂无区域数据！');
                    }
                } else {
                    utils.showMessage('获取区域数据失败，请重试！');
                }

            });
        },
        // 按照搜索小区添加
        areaAddBySearch: function (chooseArea, userObj, type) {
            tableArr = [];
            area_list.getSearchVillageData(userObj, function (result) {
                if (result.result_code == 200) {
                    result.queryWorkerAreaDTOS.forEach(function (resI, resK) {
                        tableArr.push({
                            districtId: resI.id,
                            districtName: resI.address_name,
                            businessDistrictId: resI.bsId,
                            businessDistrictName: resI.bsName,
                            villageId: resI.blockId,
                            villageName: resI.blockName,
                            village: resI.address_name + '-' + resI.bsName + '-' + resI.blockName,
                        });
                    });
                    var searchVillageGridJson = {rows: tableArr};
                    $('#searchVillageTable')[0].addJSONData(searchVillageGridJson);

                    $('#submitVillage').on('click', function () {
                        area_list.submitVillage(chooseArea, tableArr);
                    })
                }
            });
        },
        // 获取搜索小区数据
        getSearchVillageData: function (userObj, fn) {
            var villageName = $('#villageName').val() || null;
            console.log(selectedRowIndex)

            win.utils.ajaxPost(win.utils.services.queryWorkerAddArea, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                workerId: userObj.workerId,
                blockName: villageName
            }, fn);
        },
        // 搜索小区 提交选择
        submitVillage: function (chooseArea, tableArr) {
            var selectedIds = $("#searchVillageTable").jqGrid("getGridParam", "selarrrow");
            // 全选
            if (selectedIds.length == tableArr.length) {
                $('#searchVillageTable_cb').find('input[type=checkbox]').attr('checked', true);
            }
            selectedIds.forEach(function (idsI, idsK) {
                var districtId = tableArr[idsI - 1].districtId;
                var districtName = tableArr[idsI - 1].districtName;
                var businessDistrictId = tableArr[idsI - 1].businessDistrictId;
                var businessDistrictName = tableArr[idsI - 1].businessDistrictName;
                var villageId = tableArr[idsI - 1].villageId;
                var villageName = tableArr[idsI - 1].villageName;
                // 添加小区
                if (chooseArea.length) {
                    isSetArea = false;
                    isSetTrading = false;
                    chooseArea.forEach(function (areaI, areaK) {
                        if (areaI.id == districtId) {
                            isSetArea = true;
                            chooseArea[areaK].trading.forEach(function (tradingI, tradingK) {
                                if (tradingI.id == businessDistrictId) {
                                    isSetTrading = true;
                                    if (JSON.stringify(chooseArea[areaK].trading[tradingK].village).indexOf(JSON.stringify({
                                        id: villageId,
                                        name: villageName
                                    })) == -1) {
                                        chooseArea[areaK].trading[tradingK].village.push({
                                            id: villageId,
                                            name: villageName
                                        });
                                    }
                                }
                            })
                            if (!isSetTrading) {
                                chooseArea[areaK].trading.push({
                                    id: businessDistrictId,
                                    name: businessDistrictName,
                                    village: [{
                                        id: villageId,
                                        name: villageName
                                    }]
                                });
                            }
                        }
                    });
                    if (!isSetArea) {
                        chooseArea.push({
                            id: districtId,
                            name: districtName,
                            trading: [
                                {
                                    id: businessDistrictId,
                                    name: businessDistrictName,
                                    village: [
                                        {
                                            id: villageId,
                                            name: villageName
                                        }
                                    ]
                                }
                            ]
                        });
                    }
                } else {
                    chooseArea.push({
                        id: districtId,
                        name: districtName,
                        trading: [
                            {
                                id: businessDistrictId,
                                name: businessDistrictName,
                                village: [
                                    {
                                        id: villageId,
                                        name: villageName
                                    }
                                ]
                            }
                        ]
                    });
                }
            });
            area_list.addAreaResultOpt(chooseArea, villageIndex);
        },
        // 同步添加操作和显示
        addAreaResultOpt: function (chooseArea, villageIndex) {
            $('.add-result').find('ul').html('');
            var nodeStr = '';
            chooseArea.forEach(function (addResutltStrAreaI, addResultStrAreaK) {
                chooseArea[addResultStrAreaK].trading.forEach(function (addResultTradingI, addResultStrTradingK) {
                    if (addResultTradingI.village.length) {
                        villageIndex++;
                        nodeStr = '<li style="background: #f8f8f8; padding: 8px 0; border-radius: 4px; margin-bottom: 14px;">\n' +
                            '\t\t\t\t\t\t\t<div class="form-horizontal">\n' +
                            '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                            '\t\t\t\t\t\t\t\t\t<label class="col-sm-2" style="text-align: right;">区域</label>\n' +
                            '\t\t\t\t\t\t\t\t\t<div class="col-sm-10" data-areaId="' + chooseArea[addResultStrAreaK].id + '">' + chooseArea[addResultStrAreaK].name + '</div>\n' +
                            '\t\t\t\t\t\t\t\t</div>\n' +
                            '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                            '\t\t\t\t\t\t\t\t\t<label class="col-sm-2" style="text-align: right;">商圈</label>\n' +
                            '\t\t\t\t\t\t\t\t\t<div class="col-sm-10" data-tradingId="' + addResultTradingI.id + '">' + addResultTradingI.name + '</div>\n' +
                            '\t\t\t\t\t\t\t\t</div>\n' +
                            '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                            '\t\t\t\t\t\t\t\t\t<label class="col-sm-2" style="text-align: right;">小区</label>\n' +
                            '\t\t\t\t\t\t\t\t\t<div class="col-sm-10 add_village" style="display: flex; flex-wrap: wrap; justify-items: flex-start; align-items: flex-start;">\n' +
                            '\t\t\t\t\t\t\t\t\t</div>\n' +
                            '\t\t\t\t\t\t\t\t</div>\n' +
                            '\t\t\t\t\t\t\t</div>\n' +
                            '\t\t\t\t\t\t</li>';
                        var villageStr = '';
                        chooseArea[addResultStrAreaK].trading[addResultStrTradingK].village.forEach(function (addResultStrVillageI, addResultStrVillageK) {
                            villageStr += '<div class="result-show-village" data-areaId="' + chooseArea[addResultStrAreaK].id + '" data-tradingId="' + addResultTradingI.id + '" data-villageId="' + addResultStrVillageI.id + '">\n' +
                                '\t\t\t\t\t\t\t\t\t\t\t<span>' + addResultStrVillageI.name + '</span>\n' +
                                '\t\t\t\t\t\t\t\t\t\t\t<img src="../../images/close.png" alt="">\n' +
                                '\t\t\t\t</div>'
                        });

                        $('.add-result').find('ul').append(nodeStr);
                        $('.add-result').find('ul').find('li').eq(villageIndex - 1).find('.add_village').append(villageStr);

                    }
                });
            });
        },
        // 删除小区
        deleteVillage: function (chooseArea, villageIndex, that) {
            $('.village .itemBox').find('div').eq(0).removeClass('checked'); //取消全选
            $('.village .itemBox').find('div').eq(0).text('全选');

            var districtId = $(that).attr('data-areaId');
            var businessDistrictId = $(that).attr('data-tradingId');
            var villageId = $(that).attr('data-villageId');
            chooseArea.forEach(function (vi, vk) {
                if (vi.id == districtId) {
                    chooseArea[vk].trading.forEach(function (vii, vkk) {
                        if (vii.id == businessDistrictId) {
                            chooseArea[vk].trading[vkk].village.forEach(function (vtvi, vivk) {
                                if (vtvi.id == villageId) {
                                    chooseArea[vk].trading[vkk].village.splice(vivk, 1);
                                    // 当小区数量为0时 删除对应小区
                                    if (!chooseArea[vk].trading[vkk].village.length) {
                                        chooseArea[vk].trading.splice(vkk, 1);
                                    }
                                    if (!chooseArea[vk].trading.length) {
                                        chooseArea.splice(vk, 1);
                                    }
                                }
                            })
                        }

                    });
                }
            });
            area_list.deleteVillageClassName(villageId);
            area_list.deleteVillageChoose(villageId);
            area_list.addAreaResultOpt(chooseArea, villageIndex);
            area_list.spanNumShow(chooseArea);
        },
        // 删除小区之后，小区去掉选中类名
        deleteVillageClassName: function (id) {
            var villageNodes = $('.village .itemBox').find('div');
            for (var i = 0; i < villageNodes.length; i++) {
                var villageId = $(villageNodes[i]).attr('data-villageId');
                if (villageId == id) {
                    $('.village .itemBox').find('div').eq(i).removeClass('checked');
                }
            }
        },
        // 删除小区，取消小区勾选
        deleteVillageChoose: function (id) {
            var index;
            tableArr.forEach(function (gi, gk) {
                if (gi.villageId == id) {
                    index = gk;
                }
            });
            // 取消全选
            $('#searchVillageTable_cb').find('input[type=checkbox]').attr('checked', false);

            $('#searchVillageTable').find('tr').eq(index + 1).removeClass('success');
            $('#searchVillageTable').find('tr').eq(index + 1).attr('aria-selected', false);
            $('#searchVillageTable').find('tr').eq(index + 1).find('input[type=checkbox]').attr('checked', false);
        },
        // 角标展示
        spanNumShow: function (chooseArea) {
            var areaId;
            var areaNum;
            var tradingId;
            var tradingNum;
            var spanArr = [];
            if (chooseArea.length) {
                $('.zoning .itemBox').find('div').find('span').text(' ').hide();
                $('.trading .itemBox').find('div').find('span').text(' ').hide();
                $('.village .itemBox').find('div').find('span').text(' ').hide();
                chooseArea.forEach(function (spanNumAreaI, spanNumAreaK) {
                    areaId = spanNumAreaI.id;
                    chooseArea[spanNumAreaK].trading.forEach(function (spanNumTradingI, spanNumTradingK) {
                        if (chooseArea[spanNumAreaK].trading[spanNumTradingK].village.length) {
                            areaNum = chooseArea[spanNumAreaK].trading.length;
                            tradingId = spanNumTradingI.id;
                            tradingNum = chooseArea[spanNumAreaK].trading[spanNumTradingK].village.length;
                            spanArr.push({
                                areaSpanObj: {id: areaId, num: areaNum},
                                tradingSpanObj: {id: tradingId, num: tradingNum}
                            });
                        }
                    })
                });
                if (spanArr.length) {
                    spanArr.forEach(function (spanArrI, spanArrK) {
                        area_list.areaSpanShow(spanArr[spanArrK].areaSpanObj);
                        area_list.tradingSpanShow(spanArr[spanArrK].tradingSpanObj);
                    });
                }
                // else{
                // 	$('.zoning .itemBox').find('div').find('span').text(' ').hide();
                // 	$('.trading .itemBox').find('div').find('span').text(' ').hide();
                // 	$('.village .itemBox').find('div').find('span').text(' ').hide();
                // }
            } else {
                $('.zoning .itemBox').find('div').find('span').text(' ').hide();
                $('.trading .itemBox').find('div').find('span').text(' ').hide();
                $('.village .itemBox').find('div').find('span').text(' ').hide();
            }
        },
        areaSpanShow: function (obj) {
            var areaNodes = $('.zoning .itemBox').find('div');
            for (var i = 0; i < areaNodes.length; i++) {
                var areaId = $(areaNodes[i]).attr('data-districtid');
                if (areaId == obj.id) {
                    $('.zoning .itemBox').find('div').eq(i).find('span').text(obj.num).show();
                }
            }
        },
        tradingSpanShow: function (obj) {
            var tradingNodes = $('.trading .itemBox').find('div');
            for (var i = 0; i < tradingNodes.length; i++) {
                var tradingId = $(tradingNodes[i]).attr('data-businessdistrictid');
                if (tradingId == obj.id) {
                    $('.trading .itemBox').find('div').eq(i).find('span').text(obj.num).show();
                }
            }
        },
        findNodeIndex: function (NodeName, attrName, id) {
            var Nodes = NodeName.find('div');
            for (var i = 0; i < Nodes.length; i++) {
                var nodeId = $(Nodes[i]).attr(attrName);
                if (nodeId == id) {
                    // $('.zoning .itemBox').find('div').eq(i).find('span').text(obj.num).show();
                    return i;
                }
            }
        },
        // 增加区域弹层显示
        addAreaLayerShow: function () {
            $('#searchAdd').removeClass('btn-primary');
            $('#areaAdd').addClass('btn-primary');
            $('.area-add').show();
            $('.search-add').hide();
            $('.trading').removeClass('showBox');
            $('.village').removeClass('showBox');
            $('#villageName').val('');
        },
        /**-----------------增加区域相关方法结束-------------------------------------------*/


        /**-----------------区域转派相关方法开始-------------------------------------------*/
        transferArea: function () {
            area_list.transferLayerInit();

            // 默认城市
            var defaultCityId = $('#transferCity').val();
            var defaultCityName = $('#transferCity').find('option').eq(0).text();

            // console.log(defaultCityId,defaultCityName)
            transferData.cityName = defaultCityName;
            transferData.regionId = defaultCityId;
            // console.log(transferData);

            $('#transferCity').val(defaultCityId);
            $('#transferCity').find("option[text=" + defaultCityName + "]").attr('selected', true);
            area_list.getListSearchTradingData($('#transferTrading'), $('#transferBusiness'), $('#transferCity').val());

            villageTextArr = [];
            var i = layer.open({
                type: 1,
                area: ['100%', '100%'],
                title: '<i class="fa fa-plus">&nbsp;&nbsp;区域转派</i>',
                content: $('#transferArea'),
                shadeClose: true,
                // btn: ['保存', '取消'],
                success: function () {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);
                    flagInput = false;


                    var b = $(".jqGrid_wrapper").width();
                    $("#areaTable").setGridWidth(b);
                    // $("#areaTable").setGridHeight(utils.getAutoGridHeight()-$('#gbox_areaTable').position().top-40);

                    // 获取列表数据
                    // transferData.cityName = $('#transferCity').find('option:selected').text();
                    transferData = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        cityName: $('#transferCity').find('option:selected').text() || null, //城市
                        regionId:$('#transferCity').val(),
                        districtName: null, //行政区
                        cbdName: null, //商圈
                        blockId: null, //小区id
                        workerId: null, //维修员id
                        // operator: synthesizeWorker, //操作人
                        synthesizeWorker: null, //有无综合师傅
                        eleWorker: null,//有无电器师傅
                        rows: 50,
                        position: transferNowPage,
                        isMain: null
                    };
                    area_list.getTransferData(transferNowPage, transferData);


                    // 转派搜索维修员id
                    var transferSearchWorkerId;

                    // 区域转派提交按钮绑定事件
                    $('#transferSubmit').off('click').on('click', function () {
                        is_transfer_search = true;
                        $("#areaTable").jqGrid('resetSelection');

                        transferNowPage = 1;
                        transferSearchWorkerId = $('#transferRepairman').val();
                        area_list.searchTransferList(1);
                    });
                    // 区域转派清空搜索条件
                    $('#transferClear').off('click').on('click', function () {
                        $("#areaTable").jqGrid('resetSelection');

                        // 默认城市
                        var defaultCityId = $('#transferCity').find('option').eq(0).val();
                        var defaultCityName = $('#transferCity').find('option').eq(0).text();
                        $('#transferCity').val(defaultCityId);
                        $('#transferCity').find("option[text=" + defaultCityName + "]").attr('selected', true);
                        area_list.getListSearchTradingData($('#transferTrading'), $('#transferBusiness'), $('#transferCity').val());

                        $('#transferTrading').val('');
                        $('#transferBusiness').val('');
                        $('.transferBusinessBox').show();
                        $('.transferVillageBox').hide();
                        $('select[name="transferVillageSelect"]').val('');
                        $('select[name="transferRepairman"]').val('');
                        form.render();
                        transferVillage = null;
                        $('#isTransferComperMan').val('');
                        $('#isTransferEleMan').val('');

                        transferSearchWorkerId = null;//区域转派搜索维修员为空

                        transferNowPage = 1;
                        is_transfer_search = false;
                        transferData = {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            cityName: $('#transferCity').find('option:selected').text() || null, //城市
                            regionId:$('#transferCity').val(),
                            districtName: null, //行政区
                            cbdName: null, //商圈
                            blockId: null, //小区id
                            workerId: null, //维修员id
                            // operator: synthesizeWorker, //操作人
                            synthesizeWorker: null, //有无综合师傅
                            eleWorker: null,//有无电器师傅
                            rows: 50,
                            position: transferNowPage,
                            isMain: null
                        };
                        // console.log(transferData)
                        area_list.getTransferData(transferNowPage, transferData);

                        // transferNowPage=1;
                        // area_list.getTransferData(transferNowPage,data);
                    });

                    // 区域转派按钮点击事件
                    $('#compreAreaTransfer').off('click').on('click', function () {
                        area_list.areaTransfer(1, transferSearchWorkerId)
                    });
                    $('#eleAreaTransfer').off('click').on('click', function () {
                        area_list.areaTransfer(2, transferSearchWorkerId)
                    });

                }
            });
        },
        // 获取转派列表数据
        getTransferData: function (pageIndex, postData) {
            is_transfer_search = false;
            // console.log(postData);
            pageCount = $("#areaTable").jqGrid('getGridParam', 'rowNum');
            postData.rows = pageCount;
            win.utils.ajaxPost(
                win.utils.services.getAreaWorkerList,
                postData,
                function (result) {
                    if (result.result_code == 200) {
                        if (result.total == 0) {
                            gridData = {};
                            var gridJson = {
                                total: 0,
                                rows: gridData,
                                page: 0,
                                records: result.total
                            };
                            pageText = '第0页 / 共0页';
                            $("#areaTable").jqGrid('setGridParam', {
                                pgtext: pageText
                            });
                            $("#areaTable")[0].addJSONData(gridJson);
                            $('.totalNum').text(result.total);

                            utils.showMessage('暂无数据！');
                        } else {
                            gridData = result.areaWorkerList;
                            totalPages = Math.ceil(result.total / pageCount);
                            var gridJson = {
                                total: totalPages,
                                rows: gridData,
                                page: pageIndex,
                                records: result.total
                            };
                            pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                            $("#areaTable").jqGrid('setGridParam', {
                                pgtext: pageText
                            });
                            $("#areaTable")[0].addJSONData(gridJson);
                            $('.totalNum').text(result.total);

                        }
                    } else {
                        utils.showMessage('获取数据失败，请重试！');
                    }
                });
        },
        searchTransferList: function (isMain) {
            if (is_transfer_search) {
                var cityName = $('#transferCity').find('option:selected').text() || null;
                var regionId = $('#transferCity').val();
                var districtName = $('#transferTrading').find('option:selected').text() || null;
                var cbdName = $('#transferBusiness').find('option:selected').text() || null;
                var workerId = $('#transferRepairman').val() || null;
                var synthesizeWorker = $('#isTransferComperMan').val() || null;
                var eleWorker = $('#isTransferEleMan').val() || null;

                transferData.cityName = cityName == '城市' ? null : cityName;
                transferData.districtName = districtName == '行政区域' ? null : districtName;
                transferData.cbdName = cbdName == '商圈' ? null : cbdName;
                transferData.blockId = transferVillage ? (transferVillage.length ? transferVillage : null) : null;
                transferData.workerId = workerId;
                transferData.synthesizeWorker = synthesizeWorker;
                transferData.eleWorker = eleWorker;
                transferData.position = transferNowPage;
                transferData.regionId = regionId;
            }
            area_list.getTransferData(transferNowPage, transferData);
        },
        // 区域转派弹层搜索条件初始化
        transferLayerInit: function () {
            $("#areaTable").jqGrid('resetSelection');

            // 默认城市
            var defaultCityId = $('#transferCity').find('option').eq(0).val();
            var defaultCityName = $('#transferCity').find('option').eq(0).text();
            $('#transferCity').val(defaultCityId);
            $('#transferCity').find("option[text=" + defaultCityName + "]").attr('selected', true);

            $('#transferTrading').val('');
            $('#transferBusiness').val('');
            $('select[name="transferVillageSelect"]').val('');
            $('select[name="transferRepairman"]').val('');
            form.render();
            transferVillage = null;
            $('#isTransferComperMan').val('');
            $('#isTransferEleMan').val('');

        },
        // 区域转派方法
        //type  转派区域类型 1 综合区域   2 电器区域
        areaTransfer: function (type, transferSearchWorkerId) {
            var ids = $("#areaTable").jqGrid("getGridParam", "selarrrow");

            if (type == 1) {
                $('#transferPepairman').find('label').text('转派综合师傅');
            } else if (type == 2) {
                $('#transferPepairman').find('label').text('转派电器师傅');
            }

            if (ids.length) {
                var blockIds = [];
                ids.forEach(function (i, k) {
                    var rowData = $("#areaTable").jqGrid('getRowData', i);
                    blockIds.push(rowData.blockId);
                });
                var i = layer.open({
                    type: 1,
                    area: ['60%', '60%'],
                    title: '<i class="fa fa-plus">&nbsp;&nbsp;区域转派</i>',
                    content: $('#transferPepairman'),
                    shade: 0.3,
                    zIndex: 2,
                    // shadeClose:true,
                    btn: ['确认', '取消'],
                    success: function () {
                        $('.layui-layer-page').css('zIndex', 666);
                        $('.layui-layer-shade').css('zIndex', 600);
                        flagInput = false;


                        // 维修员筛选条件
                        var data = {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            // regionId: $('#transferCity').find('option[text='+transferData.cityName+']'),
                            regionId: transferData.regionId,
                            isInservice: 1,
                            skillKind: type,
                            // workerId:transferSearchWorkerId||null,
                            workerStatus: 1,
                            status: 1
                        };
                        // console.log(data)
                        area_list.getListSearchRepairman($('#chooseTransferRepairman'), 'choosetransferRepairman', data);

                    },
                    yes: function (index, layero) {
                        if (transferSuo) {
                            transferSuo = false; //关锁
                            var workerId = $('#chooseTransferRepairman').val();
                            var workerName = $('#chooseTransferRepairman').find('option:selected').text().split('/')[0];

                            if (workerId && workerName) {
                                win.utils.ajaxPost(win.utils.services.transferDistrict, {
                                    user_id: win.utils.getCookie('user_id'),
                                    session: win.utils.getCookie('session'),
                                    blockIds: blockIds,
                                    skillKind: type,
                                    workerId: workerId,
                                    workerName: workerName
                                }, function (result) {
                                    if (result.result_code == 200) {
                                        utils.showMessage('转派成功！');
                                        layer.close(index);

                                        $("#areaTable").jqGrid('resetSelection');
                                        ids = [];

                                        // 刷新转派列表
                                        area_list.searchTransferList();

                                        transferSuo = true; //开锁
                                    } else {
                                        utils.showMessage('转派失败，请重试！');
                                    }
                                });
                            } else {
                                utils.showMessage('请选择维修员！');
                            }
                        }
                    }
                });
            } else {
                utils.showMessage('请选择转派区域！');
            }
        },
        /**-----------------区域转派相关方法结束-------------------------------------------*/


        /**-----------------维修员请假相关方法开始-------------------------------------------*/
        repairmanLeave: function (obj) {
            area_list.leaveLayerInit();//请假弹层初始化

            var i = layer.open({
                type: 1,
                area: ['100%', '100%'],
                title: '<i class="fa fa-plus">&nbsp;&nbsp;维修员请假</i>',
                content: $('#repairmanLeave'),
                shadeClose: true,
                // zIndex:11,
                btn: ['请假', '取消'],
                success: function (layero) {
                    isSearch = false;

                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);
                    flagInput = false;


                    if (obj) {
                        if (obj.workerId && obj.type == 2) {
                            var optionStr = '<option value="' + obj.workerId + '" selected>' + obj.workerName + '/' + obj.workerPhone + '</option>';
                            $('#repairmanLeaveSelect').html(optionStr);
                            layui.use('form', function () {
                                var form = layui.form;
                                form.render('select');
                            });


                            // var reparimanName = $('#repairmanLeaveSelect').find('option:selected').text().split('/')[0];


                            area_list.checkLeaveWorkerInfo(obj.workerId);//获取维修员信息
                            $('.infoBox').show();

                            // 获取师傅负责区域列表
                            area_list.checkWorkerList(obj.workerId, leaveAllNowPage);
                            // 查看师傅负责区域
                            $('.checkWorkerList').off('click').on('click', function () {
                                // area_list.checkWorkerList(repairmanId,leaveAllNowPage);
                                $('.repairman-detail.tableBox').show();
                            });

                            // 添加备用师傅
                            $('#addComperSpare').off('click').on('click', function () {
                                leaveItemNowPage = 1;
                                area_list.addSpareWorker(1, obj.workerId, obj.workerName);
                            });
                            $('#addEleSpare').off('click').on('click', function () {
                                leaveItemNowPage = 1;
                                area_list.addSpareWorker(2, obj.workerId, obj.workerName);
                            });

                        }
                    } else {
                        // 获取维修员数据
                        win.utils.ajaxPost(win.utils.services.getWorker, {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            isInservice: 1,
                            workerStatus: 1
                        }, function (result) {
                            if (result.result_code == 200) {
                                var optionStr = '<option value="">请选择维修员</option>';
                                result.workerInfoVOS.forEach(function (i, k) {
                                    optionStr += '<option value="' + i.workerId + '">' + i.workerName + '/' + i.workerPhone + '</option>'
                                });
                                $('#repairmanLeaveSelect').html(optionStr);
                                // form.render('select','repairmanLeaveSelect');
                                layui.use('form', function () {
                                    form = layui.form;
                                    form.render('select');
                                });
                            }
                        })
                    }


                },
                yes: function (index, layero) {
                    if (!isSearch && !obj) {
                        utils.showMessage('请先选择请假师傅！');
                    } else {
                        var isAllSynWorker = false;//是否全部都有综合备用师傅
                        var isAllEleWorker = false; //是否全部都有电器备用师傅
                        if (comperTableData.length) {
                            comperTableData.forEach(function (i, k) {
                                if (!i.backSyntWorker) {
                                    isAllSynWorker = true; //有一个没有综合备用师傅，则isAllSynWorker的值为true
                                }
                            });
                        }
                        if (eleTableData.length) {
                            eleTableData.forEach(function (i, k) {
                                if (!i.backEleWorker) {
                                    isAllEleWorker = true;//有一个没有电器备用师傅，则isAllEleWorker的值为true
                                }
                            });
                        }
                        console.log(isAllEleWorker, isAllSynWorker)
                        // 综合区域电器区域全部有备用师傅，可请假
                        if (!isAllSynWorker && !isAllEleWorker) {
                            area_list.reparimanLeaveStatusUpdate(index, obj);
                        } else {
                            utils.showMessage('有区域未分派，请先分派区域！');
                        }
                    }
                },
                cancel: function (index, layero) {
                    layer.closeAll();
                    $('#repairmanLeave').hide();
                    // 初始化列表数据
                    area_list.searchList();
                    leaveAllNowPage = 1;
                },
                btn2: function (index, layero) {
                    layer.closeAll();
                    $('#repairmanLeave').hide();
                    // 初始化列表数据
                    area_list.searchList();
                    leaveAllNowPage = 1;
                }
            });
        },
        // 维修员请假弹层，保存操作获取维修员技能分类区域数据
        getSKillKindTableData: function (type) {
            var datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                isMain: 1,
                workerId: $('#repairmanLeave .workerNum').text(),
                skillKind: type
            };
            win.utils.ajaxPost(win.utils.services.getAreaWorkerList, datas, function (result) {
                if (result.result_code == 200) {
                    if (type == 1) {
                        comperTableData = result.areaWorkerList;
                    } else if (type == 2) {
                        eleTableData = result.areaWorkerList;
                    }
                } else {
                    utils.showMessage('获取技能分类数据失败，请重试！');
                }
            });
        },
        // 维修员请假保存按钮触发事件
        reparimanLeaveStatusUpdate: function (i, obj) {
            // if(obj){
            var data = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                workerId: $('#repairmanLeave .workerNum').text(),
                workerName: $('#repairmanLeave .workerName').text(),
                synthesisSpare: $('#repairmanLeave .comperSpareText').text(), //综合备用师傅
                electricSpare: $('#repairmanLeave .eleSpareText').text(), //电器备用师傅
                leaveStartTime: area_list.dateFormat($('#leaveTime').val()) ? area_list.dateFormat($('#leaveTime').val())[0] : null, //请假开始时间
                leaveEndTime: area_list.dateFormat($('#leaveTime').val()) ? area_list.dateFormat($('#leaveTime').val())[1] : null //请假结束时间
            };
            area_list.leaveStatus(data, function (result) {
                if (result.result_code == 200) {
                    // console.log(obj)
                    if (obj) {
                        if (obj.workerId && obj.type == 2) {
                            // location.href=utils.domain_name+'web/repairmanManage/repairman_list.html';
                            parent.$(window.parent.document).find('.nav .J_menuItem[data-id='+obj.source+']').click();
                            // utils.showMessage('请假成功！');
                            if(obj.source==32){
                                parent.index.backToRepairmanList(obj);
                            }
                            else if(obj.source==37){
                                parent.index.backToSpecialMaintainer(obj);
                            }
                            layer.closeAll();
                            $('#repairmanLeave').hide();
                        }
                    } else {
                        utils.showMessage('请假成功！');
                        layer.closeAll();
                        $('#repairmanLeave').hide();
                    }

                    // 请求参数
                    var data = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        cityName: null, //城市
                        districtName: null, //行政区
                        cbdName: null, //商圈
                        blockId: null, //小区id
                        workerId: null, //维修员id
                        operator: null, //操作人
                        synthesizeWorker: null, //有无综合师傅
                        eleWorker: null,//有无电器师傅
                        rows: 20,
                        position: nowPage
                    };
                    area_list.getServerData(nowPage, data);
                }
                else if(result.result_code==999){
                    utils.showMessage(result.description);
                }
            });
            // }
            // else{
            //     utils.showMessage('请先选择请假师傅！');
            // }

        },
        // 维修员请假——添加备用师傅弹层
        addSpareWorker: function (type, workerId, workerName) {
            // 搜索条件初始化
            $('#leaveCity').val('');
            $('#leaveTrading').val('');
            $('#leaveBusiness').val('');
            $('#leaveVillageSelect').val('');
            layui.use('form', function () {
                var form = layui.form;
                form.render('select');
            });
            $('.leaveTradingBox').hide();
            $('.leaveBusinessBox').hide();
            $('.leaveVillageBox').hide();

            $("#LeaveRepairmanTable").jqGrid("clearGridData");
            pageText = '第0页 / 共0页';
            $("#LeaveRepairmanTable").jqGrid('setGridParam', {
                pgtext: pageText
            });

            leaveItemType = type;
            var i = layer.open({
                type: 1,
                area: ['100%', '100%'],
                title: '维修员请假 — 添加备用师傅</i>',
                content: $('#addSpareWorker'),
                btn: ['确定'],
                shade: 0,
                shadeClose: true,
                zIndex: 2,
                success: function (layero, index) {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);

                    flagInput = false;

                    $("#LeaveRepairmanTable").jqGrid('resetSelection');

                    // 获取转派代理执行人数据
                    var data = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        workerStatus: 1,
                        isInservice: 1,
                        // regionId:$('#repairmanLeave .repairman-info').find('.regionName').attr('data-cityId'),
                        // skillKind:$('#repairmanLeave .repairman-info').find('.skillKind').attr('data-skillKind'),
                        skillKind: type,
                        workerId: workerId
                    }
                    area_list.getTransferProxy($('#leaveRepairman'), 'leaveRepairman', data);

                    // 获取列表数据
                    leavePostData.workerId = workerId;
                    leavePostData.skillKind = type;
                    leavePostData.isMain = 1;
                    leavePostData.cityName = null;
                    leavePostData.districtName = null;
                    leavePostData.cbdName = null;
                    leavePostData.blockId = null;
                    leavePostData.position = leaveItemNowPage;
                    // area_list.getLeaveTransferData(leaveItemNowPage,leavePostData);
                    area_list.LeaveSearchList(leavePostData);


                    // 维修员请假批量编辑备用师傅按钮颜色变化
                    $('.tableBox').off('click').on('click', '#gbox_LeaveRepairmanTable input', function () {
                        var ids = $("#LeaveRepairmanTable").jqGrid("getGridParam", "selarrrow");
                        if (ids.length) {
                            $('#chooseRepairmanBtn').removeClass('unified-gray-btn-global');
                            $('#chooseRepairmanBtn').addClass('unified-green-btn-global');
                        } else {
                            $('#chooseRepairmanBtn').removeClass('unified-green-btn-global');
                            $('#chooseRepairmanBtn').addClass('unified-gray-btn-global');
                        }
                    });


                    // 选择全部备用师傅
                    $('#allRepairmanBtn').off('click').on('click', function () {
                        var i = layer.open({
                            type: 1,
                            area: ['60%', '60%'],
                            title: '<i class="fa fa-plus">&nbsp;&nbsp;选择全部备用师傅</i>',
                            content: $('#chooseRepairman'),
                            shade: 0,
                            shadeClose: true,
                            zIndex: 3,
                            btn: ['保存', '取消'],
                            success: function () {
                                $('.layui-layer-page').css('zIndex', 333);
                                $('.layui-layer-shade').css('zIndex', 300);

                                flagInput = false;

                                // 获取转派代理执行人数据
                                var data = {
                                    user_id: win.utils.getCookie('user_id'),
                                    session: win.utils.getCookie('session'),
                                    workerStatus: 1,
                                    isInservice: 1,
                                    // regionId:$('#repairmanLeave .repairman-info').find('.regionName').attr('data-cityId'),
                                    // skillKind:$('#repairmanLeave .repairman-info').find('.skillKind').attr('data-skillKind'),
                                    skillKind: type,
                                    workerId: workerId
                                }
                                area_list.getTransferProxy($('#chooseRepairmanManual'), 'chooseRepairmanManual', data);
                            },
                            yes: function (index, layero) {
                                if (beiyongSuo) {
                                    beiyongSuo = false;

                                    var proxyWorkerId = $('#chooseRepairmanManual').val();
                                    if (proxyWorkerId) {
                                        var proxyWorkerName = $('#chooseRepairmanManual').find('option:selected').text().split('/')[0];
                                        var data = {
                                            user_id: win.utils.getCookie('user_id'),
                                            session: win.utils.getCookie('session'),
                                            proxyWorkerId: proxyWorkerId,
                                            proxyWorkerName: proxyWorkerName,
                                            workerAreaIds: null,
                                            workerId: workerId,
                                            skillKind: type,
                                        };
                                        area_list.leaveTransfer(data, function (result) {
                                            if (result.result_code == 200) {
                                                layer.close(index);
                                                utils.showMessage('添加备用师傅成功！');
                                                // 刷新列表
                                                leaveItemNowPage = 1;
                                                area_list.LeaveSearchList(leavePostData);

                                                beiyongSuo = true;
                                            } else {
                                                utils.showMessage('添加失败，请重试！');
                                            }
                                        });
                                    } else {
                                        utils.showMessage('请选择备用师傅！');
                                    }

                                }

                            }
                        });
                    });

                    // 编辑备用师傅——手动选择区域添加备用师傅
                    $('#chooseRepairmanBtn').off('click').on('click', function () {
                        var ids = $("#LeaveRepairmanTable").jqGrid("getGridParam", "selarrrow");

                        if (ids.length) {
                            var blockIds = [];
                            ids.forEach(function (i, k) {
                                var rowData = $("#LeaveRepairmanTable").jqGrid('getRowData', i);
                                blockIds.push(rowData.blockId);
                            });
                            var i = layer.open({
                                type: 1,
                                area: ['60%', '60%'],
                                title: '<i class="fa fa-plus">&nbsp;&nbsp;选择备用师傅</i>',
                                content: $('#chooseRepairman'),
                                shade: 0,
                                shadeClose: true,
                                zIndex: 3,
                                btn: ['保存', '取消'],
                                success: function () {
                                    $('.layui-layer-page').css('zIndex', 333);
                                    $('.layui-layer-shade').css('zIndex', 300);

                                    flagInput = false;

                                    // 获取转派代理执行人数据
                                    var data = {
                                        user_id: win.utils.getCookie('user_id'),
                                        session: win.utils.getCookie('session'),
                                        workerStatus: 1,
                                        isInservice: 1,
                                        // regionId:$('#repairmanLeave .repairman-info').find('.regionName').attr('data-cityId'),
                                        // skillKind:$('#repairmanLeave .repairman-info').find('.skillKind').attr('data-skillKind'),
                                        skillKind: type,
                                        workerId: workerId
                                    }
                                    area_list.getTransferProxy($('#chooseRepairmanManual'), 'chooseRepairmanManual', data);
                                },
                                yes: function (index, layero) {
                                    // 请假转派
                                    var proxyWorkerId = $('#chooseRepairmanManual').val();
                                    if (proxyWorkerId) {
                                        var proxyWorkerName = $('#chooseRepairmanManual').find('option:selected').text().split('/')[0];
                                        var data = {
                                            user_id: win.utils.getCookie('user_id'),
                                            session: win.utils.getCookie('session'),
                                            proxyWorkerId: proxyWorkerId,
                                            proxyWorkerName: proxyWorkerName,
                                            workerAreaIds: blockIds,
                                            workerId: workerId,
                                            skillKind: type
                                        }
                                        area_list.leaveTransfer(data, function (result) {
                                            if (result.result_code == 200) {
                                                layer.close(index);
                                                area_list.LeaveSearchList(leavePostData);
                                                utils.showMessage('添加备用师傅成功！');

                                                // 清空全选
                                                $("#LeaveRepairmanTable").jqGrid('resetSelection');
                                                ids = [];
                                                $('#chooseRepairmanBtn').removeClass('unified-green-btn-global');
                                                $('#chooseRepairmanBtn').addClass('unified-gray-btn-global');

                                            } else {
                                                utils.showMessage('添加失败，请重试！');
                                            }
                                        });
                                    } else {
                                        utils.showMessage('请选择备用师傅！');
                                    }
                                },
                            });
                        } else {
                            utils.showMessage('请先选择区域！');
                        }
                    });


                    // 提交搜索
                    $('#leaveSubmit').off('click').on('click', function () {
                        is_leave_search = true;
                        $("#LeaveRepairmanTable").jqGrid('resetSelection');

                        leaveItemNowPage = 1;
                        area_list.LeaveSearchList(leavePostData);
                    });
                    // 清空搜索条件
                    $('#leaveClear').off('click').on('click', function () {
                        $("#LeaveRepairmanTable").jqGrid('resetSelection');

                        $('#leaveCity').val('');
                        $('#leaveTrading').val('');
                        $('#leaveBusiness').val('');
                        $('.leaveTradingBox').hide();
                        $('.leaveBusinessBox').hide();
                        $('.leaveVillageBox').hide();
                        $('select[name="leaveVillageSelect"]').val('');
                        form.render();
                        leaveVillage = null;

                        leaveItemNowPage = 1;
                        is_leave_search = false;
                        leavePostData = {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            cityName: null, //城市
                            districtName: null, //行政区
                            cbdName: null, //商圈
                            blockId: null, //小区id
                            workerId: workerId, //维修员id
                            operator: null, //操作人
                            synthesizeWorker: null, //有无综合师傅
                            eleWorker: null,//有无电器师傅
                            rows: 10,
                            position: leaveItemNowPage,
                            skillKind: type,
                            isMain: 1
                        };
                        // console.log(leavePostData)
                        area_list.getLeaveTransferData(leaveItemNowPage, leavePostData);
                    });
                },
                // 添加好备用师傅之后保存按钮事件
                yes: function (index, layero) {
                    var _thisLayer = index;
                    // 获取列表所有数据
                    var tableData = area_list.getJQAllData();

                    var hasNoBackSyntWorker = false;
                    var hasNoBackEleWorker = false;

                    if (type == 1) {
                        tableData.forEach(function (i, k) {
                            console.log(i.backSyntWorker);
                            if (!i.backSyntWorker) {
                                hasNoBackSyntWorker = true;
                            }
                        });
                        // 缺少综合备用师傅
                        if (!hasNoBackSyntWorker) {
                            layer.close(index);
                            area_list.checkLeaveWorkerInfo(workerId);
                            leaveAllNowPage = 1;
                            area_list.checkWorkerList(workerId);
                        } else {
                            utils.showMessage('有区域未添加综合备用师傅，请先添加!');
                        }
                    } else if (type == 2) {
                        tableData.forEach(function (i, k) {
                            if (!i.backEleWorker) {
                                hasNoBackEleWorker = true;
                            }
                        });
                        // 缺少电器备用师傅
                        if (!hasNoBackEleWorker) {
                            layer.close(index);
                            area_list.checkLeaveWorkerInfo(workerId);
                            leaveAllNowPage = 1;
                            area_list.checkWorkerList(workerId);
                        } else {
                            utils.showMessage('有区域未添加电器备用师傅，请先添加!');
                        }
                    }

                },
                cancel: function (index, layero) {
                    var _thisLayer = index;
                    // 获取列表所有数据
                    var tableData = area_list.getJQAllData();

                    var hasNoBackSyntWorker = false;
                    var hasNoBackEleWorker = false;

                    if (type == 1) {
                        tableData.forEach(function (i, k) {
                            console.log(i.backSyntWorker);
                            if (!i.backSyntWorker) {
                                hasNoBackSyntWorker = true;
                            }
                        });
                        // 缺少综合备用师傅
                        if (!hasNoBackSyntWorker) {
                            layer.close(index);
                            area_list.checkLeaveWorkerInfo(workerId);
                            leaveAllNowPage = 1;
                            area_list.checkWorkerList(workerId);
                        } else {
                            utils.showMessage('有区域未添加综合备用师傅，请先添加!');
                        }
                    } else if (type == 2) {
                        tableData.forEach(function (i, k) {
                            if (!i.backEleWorker) {
                                hasNoBackEleWorker = true;
                            }
                        });
                        // 缺少电器备用师傅
                        if (!hasNoBackEleWorker) {
                            layer.close(index);
                            area_list.checkLeaveWorkerInfo(workerId);
                            leaveAllNowPage = 1;
                            area_list.checkWorkerList(workerId);
                        } else {
                            utils.showMessage('有区域未添加电器备用师傅，请先添加!');
                        }
                    }

                }
            });
        },
        // 获取请假转派列表数据
        getLeaveTransferData: function (pageIndex, data) {
            is_leave_search = false;
            pageCount = $("#LeaveRepairmanTable").jqGrid('getGridParam', 'rowNum');

            win.utils.ajaxPost(
                win.utils.services.getAreaWorkerList,
                data,
                function (result) {
                    if (result.result_code == 200) {
                        if (result.total == 0) {
                            var leaveGridData = {};
                            var gridJson = {
                                total: 0,
                                rows: leaveGridData,
                                page: 0,
                                records: result.total
                            };
                            pageText = '第0页 / 共0页';
                            $("#LeaveRepairmanTable").jqGrid('setGridParam', {
                                pgtext: pageText
                            });
                            $("#LeaveRepairmanTable")[0].addJSONData(gridJson);
                            $('.totalNum').text(result.total);

                            $("#LeaveRepairmanTable").setGridHeight(utils.getAutoGridHeight() - 150);

                            // $('#LeaveRepairmanTable').jqGrid('clearGridData');
                            // pageText = '第0页 / 共0页';
                            // $("#LeaveRepairmanTable").jqGrid('setGridParam', {
                            //     pgtext: pageText
                            // });

                            utils.showMessage('暂无数据！');
                        } else {
                            $("#LeaveRepairmanTable").setGridHeight('auto');

                            var leaveGridData = result.areaWorkerList;
                            totalPages = Math.ceil(result.total / pageCount);
                            var gridJson = {
                                total: totalPages,
                                rows: leaveGridData,
                                page: leaveItemNowPage,
                                records: result.total
                            };
                            pageText = '第' + leaveItemNowPage + '页 / 共' + totalPages + '页';
                            $("#LeaveRepairmanTable").jqGrid('setGridParam', {
                                pgtext: pageText
                            });
                            $("#LeaveRepairmanTable")[0].addJSONData(gridJson);
                            $('.totalNum').text(result.total);

                            $('.repairman-detail').show();
                        }
                    } else {
                        utils.showMessage('获取数据失败，请重试！');
                    }
                });
        },
        // 请假转派
        // type 全部转派还手动选择转派  1  全部转派  2手动选择转派
        leaveTransfer: function (data, fn) {
            win.utils.ajaxPost(win.utils.services.addBackWorker, data, fn);
        },
        // 获取列表所有行数据
        getJQAllData: function () {
            //拿到grid对象
            var obj = $("#LeaveRepairmanTable");
            //获取grid表中所有的rowid值
            var rowIds = obj.getDataIDs();
            //初始化一个数组arrayData容器，用来存放rowData
            var arrayData = [];
            if (rowIds.length > 0) {
                for (var i = 0; i < rowIds.length; i++) {
                    //rowData=obj.getRowData(rowid);//这里rowid=rowIds[i];
                    arrayData.push(obj.getRowData(rowIds[i]));
                }
            }
            return arrayData;
        },
        // 维修员请假——修改维修员状态
        leaveStatus: function (data, fn) {
            win.utils.ajaxPost(win.utils.services.addLeaveInfo, data, fn)
        },
        // 查询请假维修员信息
        checkLeaveWorkerInfo: function (repairmanId) {
            // 获取维修员信息
            win.utils.ajaxPost(win.utils.services.queryLeaveInfo, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                workerId: repairmanId
            }, function (result) {
                if (result.result_code == 200) {
                    $('#repairmanLeave .repairman-info').find('.workerName').text(result.data.workerName);
                    $('#repairmanLeave .repairman-info').find('.regionName').text(result.data.regionName);
                    $('#repairmanLeave .repairman-info').find('.regionName').attr('data-cityId', result.data.regionId);
                    $('#repairmanLeave .repairman-info').find('.workerNum').text(result.data.workerId);
                    var workerStatus;
                    if (result.data.workerStatus == 1) {
                        workerStatus = '启用';
                    } else if (result.data.workerStatus == 2) {
                        workerStatus = '休假';
                    } else if (result.data.workerStatus == 3) {
                        workerStatus = '禁用';
                    }
                    $('#repairmanLeave .repairman-info').find('.status').text(workerStatus);
                    // $('#repairmanLeave .repairman-info').find('.status').attr('data-skillKind',result.data.workerStatus);

                    var skillKind;
                    // 获取师傅技能分类对应区域的数据
                    if (result.data.skillKind == 1) {
                        skillKind = '综合';

                        $('.comperSpareBox').show();
                        $('.eleSpareBox').hide();

                        // datas.skillKind=1;
                        area_list.getSKillKindTableData(1);
                    } else if (result.data.skillKind == 2) {
                        skillKind = '电器';

                        $('.eleSpareBox').show();
                        $('.comperSpareBox').hide();

                        // datas.skillKind=2;
                        area_list.getSKillKindTableData(2);
                    } else if (result.data.skillKind == 3) {
                        skillKind = '综合,电器';

                        $('.comperSpareBox').show();
                        $('.eleSpareBox').show();

                        // datas.skillKind=1;
                        area_list.getSKillKindTableData(1);

                        // datas.skillKind=2;
                        area_list.getSKillKindTableData(2);
                    }
                    $('#repairmanLeave .repairman-info').find('.skillKind').text(skillKind);
                    $('#repairmanLeave .repairman-info').find('.skillKind').attr('data-skillKind', result.data.skillKind);

                    $('.comperSpareBox').find('.comperSpareText').text(result.data.synthesisSpare);
                    $('.eleSpareBox').find('.eleSpareText').text(result.data.electricSpare);
                }
            });
        },
        // 维修员请假——查看维修员负责区域
        checkWorkerList: function (repairmanId, page) {
            // 请求参数
            var datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                cityName: null, //城市
                districtName: null, //行政区
                cbdName: null, //商圈
                blockId: null, //小区id
                workerId: repairmanId, //维修员id
                operator: null, //操作人
                synthesizeWorker: null, //有无综合师傅
                eleWorker: null,//有无电器师傅
                rows: 10,
                position: leaveAllNowPage,
                skillKind: null,
                isMain: 1
            };
            pageCount = $("#spareRepairmanTable").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(
                win.utils.services.getAreaWorkerList,
                datas,
                function (result) {
                    // $('.repairman-detail.tableBox').show();
                    if (result.result_code == 200) {
                        if (result.total == 0) {
                            var leaveGridData = {};
                            var gridJson = {
                                total: 0,
                                rows: leaveGridData,
                                page: 0,
                                records: result.total
                            };
                            pageText = '第0页 / 共0页';
                            $("#spareRepairmanTable").jqGrid('setGridParam', {
                                pgtext: pageText
                            });
                            $("#spareRepairmanTable")[0].addJSONData(gridJson);
                            $('.totalNum').text(result.total);

                            utils.showMessage('暂无数据！');
                        } else {
                            var leaveGridData = result.areaWorkerList;
                            totalPages = Math.ceil(result.total / pageCount);
                            var gridJson = {
                                total: totalPages,
                                rows: leaveGridData,
                                page: leaveAllNowPage,
                                records: result.total
                            };
                            pageText = '第' + leaveAllNowPage + '页 / 共' + totalPages + '页';
                            $("#spareRepairmanTable").jqGrid('setGridParam', {
                                pgtext: pageText
                            });
                            $("#spareRepairmanTable")[0].addJSONData(gridJson);
                            $('.totalNum').text(result.total);

                        }
                    } else {
                        utils.showMessage('获取数据失败，请重试！');
                    }
                }
            );
        },
        // 请假搜索
        LeaveSearchList: function (leavePostData) {
            if (is_leave_search) {
                var cityName = $('#leaveCity').find('option:selected').text() || null;
                var districtName = $('#leaveTrading').find('option:selected').text() || null;
                var cbdName = $('#leaveBusiness').find('option:selected').text() || null;

                leavePostData.cityName = cityName == "城市" ? null : cityName;
                leavePostData.districtName = districtName == "行政区域" ? null : districtName;
                leavePostData.cbdName = cbdName == "商圈" ? null : cbdName;
                leavePostData.blockId = leaveVillage ? (leaveVillage.length ? leaveVillage : null) : null;
                leavePostData.position = leaveItemNowPage;
            }

            area_list.getLeaveTransferData(leaveItemNowPage, leavePostData);
        },
        // 请假弹层初始化
        leaveLayerInit: function () {
            // 清空列表
            $('#spareRepairmanTable').jqGrid('clearGridData');
            pageText = '第' + 0 + '页 / 共' + 0 + '页';
            $("#spareRepairmanTable").jqGrid('setGridParam', {
                pgtext: pageText
            });

            // 隐藏转派按钮
            $('.comperSpareBox').hide();
            $('.eleSpareBox').hide();

            // 隐藏列表
            $('.repairman-detail.tableBox').hide();

            // 隐藏信息展示盒子
            $('.infoBox').hide();
        },
        // 获取表格所有行数据
        getRowsData: function (obj) {
            //获取当前显示的数据
            var rows = obj.jqGrid('getRowData');
            var rowNum = obj.jqGrid('getGridParam', 'rowNum'); //获取显示配置记录数量
            var total = obj.jqGrid('getGridParam', 'records'); //获取查询得到的总记录数量
            //设置rowNum为总记录数量并且刷新jqGrid，使所有记录现出来调用getRowData方法才能获取到所有数据
            obj.jqGrid('setGridParam', {rowNum: total}).trigger('reloadGrid');
            var rows = obj.jqGrid('getRowData');  //此时获取表格所有匹配的
            obj.jqGrid('setGridParam', {rowNum: rowNum}).trigger('reloadGrid'); //还原原来显示的记录数量
            return rows;
        },
        /**-----------------维修员请假相关方法结束-------------------------------------------*/

        // 时间戳转时间格式
        timeStampFormat: function (timestamp) {
            var date = new Date(timestamp);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
            h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y + M + D + h + m + s;
        },
        // 日期格式处理
        dateFormat: function (dataStr) {
            if (dataStr) {
                var startCreateTime = dataStr.split(' - ')[0];
                var endCreateTime = dataStr.split(' - ')[1];
                startCreateTime = area_list.datePartFormat(startCreateTime);
                endCreateTime = area_list.datePartFormat(endCreateTime);
                return [startCreateTime, endCreateTime];
            } else {
                return null
            }
        },
        datePartFormat: function (date) {
            return date.split(' ')[0].split('-').join('') + ' ' + date.split(' ')[1];
        }
    };
    win.area_list = area_list;
})(window, jQuery);
