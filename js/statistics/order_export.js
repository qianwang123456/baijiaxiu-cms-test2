/**
 * Created by hanlu.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';

    var nowPage = 1;
    var is_search = false;

    getStatementData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
    };

    var orderExport = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            // 统计查询权限
            utils.isUserMenuIndexRight('order_export', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });

            // 获取城市
            orderExport.getQueryUserCity();
            // 获取维修员
            orderExport.getRepairmanData();

            // 刷新
            $('#btnRefresh').on('click', function () {
                obj.refersh();
            });
            // 查询
            $('#btnQuery').on('click', function () {
                is_search = true;
                obj.queryRecord();
            });


            // 导出功能
            $('#export').on('click', function () {
                // 请求参数
                var dataArr = [];
                for (var key in getStatementData) {
                    dataArr.push(key + '=' + getStatementData[key]);
                }

                window.location.href = win.utils.services.exportData + '?' + dataArr.join('&');
            });
        },
        initList: function () {
            $("#order_table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: '800',
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                // pgtext: "第X页 / 共X页",
                colNames: [
                    "来源",
                    "供应商类型",
                    "供应商名称",
                    "城市",
                    "工单ID",
                    "公寓号",
                    "行政区",
                    "商圈",
                    "小区",
                    "详细地址",
                    "报修区域",
                    "租户报修描述",
                    "工单紧急标签",
                    "工单状态",
                    "创建人",
                    "工单报修人姓名",
                    "工单报修人电话",
                    "维修师傅姓名",
                    "维修师傅技能",
                    "维修师傅电话",
                    "维修师傅状态",
                    "蛋壳客服认领时间",
                    "接客客服认领人",
                    "订单创建时间",
                    "转派供应商时间",
                    "维修员联系时间",
                    "维修员上门签到时间",
                    "当前预约日期",
                    "当前预约时间",
                    "首次转派时间",
                    "退单时间",
                    "百家修受理时间",
                    "分派维修员时间",
                    "是否可评价",
                    "90天返修",
                    "评价满意度",
                    "评价均分",
                    "服务礼仪",
                    "服务态度",
                    "上门时效",
                    "维修结果",
                    "整体服务",
                    "评价备注",
                    "评价时间",
                    "改期次数",
                    "完成时间",
                    "一级分类",
                    "二级分类",
                    "三级分类",
                    "首次维修方案创建时间",
                    "首次维修方案审核结果",
                    "是否协调单",
                    "首次维修方案通过时间",
                    "首次维修方案通过备注",
                    "协调反馈通过次数",
                    "最后通过协调维修方案审核时间",
                    "最后通过协调维修方案审核人",
                    "最后通过协调维修方案",
                    "结算审核信息",
                    "上门费",
                    "配件费",
                    "加急费",
                    "其他费",
                    "责任鉴定",
                    "维修方案结算审核状态",
                    "结算审核人",
                    "结算审核通过时间",
                    "客服报修描述",
                    "是否保外维修单",
                    "自定义配件信息",
                    "统计更新时间",
                ],
                colModel: [
                    // 来源1
                    {
                        name: "createSource",
                        index: "createSource",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 供应商类型2
                    {
                        name: "supplierType",
                        index: "supplierType",
                        editable: false,
                        width: 80,
                        sortable: true
                    },
                    // 供应商名称2
                    {
                        name: "supplierName",
                        index: "supplierName",
                        editable: false,
                        width: 150,
                        sortable: true
                    },
                    // 城市3
                    {
                        name: "city",
                        index: "city",
                        editable: false,
                        width: 80,
                        sorttype: "string"
                    },
                    // 工单ID4
                    {
                        name: "outOrderNo",
                        index: "outOrderNo",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 公寓号5
                    {
                        name: "houseId",
                        index: "houseId",
                        editable: false,
                        width: 80,
                        sorttype: "string",
                    },
                    // 行政区域6
                    {
                        name: "district",
                        index: "district",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 商圈7
                    {
                        name: "tradingArea",
                        index: "tradingArea",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    // 小区8
                    {
                        name: "blockName",
                        index: "blockName",
                        editable: false,
                        width: 160,
                        sortable: false
                    },
                    // 详细地址9
                    {
                        name: "detailAddress",
                        index: "detailAddress",
                        editable: false,
                        width: 200,
                        sortable: false
                    },
                    // 保修区域10
                    {
                        name: "initiateRepairArea",
                        index: "initiateRepairArea",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 租户保修描述11
                    {
                        name: "faultDescriptionUser",
                        index: "faultDescriptionUser",
                        editable: false,
                        width: 400,
                        sortable: false
                    },
                    // 工单紧急标签12
                    {
                        name: "exigencyFlag",
                        index: "exigencyFlag",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    // 工单状态13
                    {
                        name: "orderStatus",
                        index: "orderStatus",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    // 创建人14
                    {
                        name: "createUser",
                        index: "createUser",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 工单报修人姓名15
                    {
                        name: "initiateRepairName",
                        index: "initiateRepairName",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 工单报修人电话16
                    {
                        name: "initiateRepairTel",
                        index: "initiateRepairTel",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 维修师傅姓名17
                    {
                        name: "workerName",
                        index: "workerName",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 维修师傅技能18
                    {
                        name: "skillKind",
                        index: "skillKind",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 维修师傅电话19
                    {
                        name: "workerTel",
                        index: "workerTel",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 维修师傅状态20
                    {
                        name: "isInservice",
                        index: "isInservice",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 蛋壳客服认领时间21
                    {
                        name: "claimTime",
                        index: "claimTime",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 接客客服认领人22
                    {
                        name: "claimer",
                        index: "claimer",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 订单创建时间23
                    {
                        name: "createTime",
                        index: "createTime",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 转派供应商时间24
                    {
                        name: "allocationTime",
                        index: "allocationTime",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 维修员联系时间25
                    {
                        name: "workerContactTime",
                        index: "workerContactTime",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 维修员上门签到时间26
                    {
                        name: "visitSignTime",
                        index: "visitSignTime",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 当前预约日期27
                    {
                        name: "changeDay",
                        index: "changeDay",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 当前预约时间28
                    {
                        name: "changeTimes",
                        index: "changeTimes",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 首次转派时间29
                    {
                        name: "transferTime",
                        index: "transferTime",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 退单时间30
                    {
                        name: "sendbackTime",
                        index: "sendbackTime",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    // 百家修受理时间31
                    {
                        name: "createTimeBjx",
                        index: "createTimeBjx",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 分派维修员时间32
                    {
                        name: "reppairmanTime",
                        index: "reppairmanTime",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 是否可评价--新增32（1）是否可评价 1.表示可以评价，0.不能评价")isCanEvaluate;
                    {
                        name: "isCanEvaluate",
                        index: "isCanEvaluate",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 90天返修 --新增32（2）
                    {
                        name: "sysNinetyRework",
                        index: "sysNinetyRework",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 评价满意度33
                    {
                        name: "evaluationStatisfaction",
                        index: "evaluationStatisfaction",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 评价均分34
                    {
                        name: "avgScore",
                        index: "avgScore",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 服务礼仪35
                    {
                        name: "serviceEtiquette",
                        index: "serviceEtiquette",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 服务态度36
                    {
                        name: "serviceAttitude",
                        index: "serviceAttitude",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 上门时效37
                    {
                        name: "visitEfficiency",
                        index: "visitEfficiency",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 维修结果38
                    {
                        name: "repairResult",
                        index: "repairResult",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 整体服务39
                    {
                        name: "serviceInteg",
                        index: "serviceInteg",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 评价备注40
                    {
                        name: "clientRemark",
                        index: "clientRemark",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 评价时间41
                    {
                        name: "evaluateTime",
                        index: "evaluateTime",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 改期次数42
                    {
                        name: "countChange",
                        index: "countChange",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 完成时间43
                    {
                        name: "practicalFinishTime",
                        index: "practicalFinishTime",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 一级分类44
                    {
                        name: "firstGradeName",
                        index: "firstGradeName",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 二级分类45
                    {
                        name: "secondGradeName",
                        index: "secondGradeName",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 三级分类46
                    {
                        name: "thirdGradeName",
                        index: "thirdGradeName",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 首次维修方案创建时间47
                    {
                        name: "createTimeFirstPlan",
                        index: "createTimeFirstPlan",
                        editable: false,
                        width: 40,
                        sortable: false
                    },
                    // 首次维修方案审核结果48
                    {
                        name: "checkResultFirstPlan",
                        index: "checkResultFirstPlan",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 是否协调单49
                    {
                        name: "ifCoordinated",
                        index: "ifCoordinated",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 首次维修方案通过时间50
                    {
                        name: "psstimeFirstPlan",
                        index: "psstimeFirstPlan",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 首次维修方案通过备注51
                    {
                        name: "passRemarkFirstPlan",
                        index: "passRemarkFirstPlan",
                        editable: false,
                        width: 800,
                        sortable: false
                    },
                    // 协调反馈通过次数52
                    {
                        name: "coordinatedFeedbackPasscount",
                        index: "coordinatedFeedbackPasscount",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 最后通过协调维修方案审核时间53
                    {
                        name: "passtimeFinallCoorcoordinatedplan",
                        index: "passtimeFinallCoorcoordinatedplan",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 最后通过协调维修方案审核人54
                    {
                        name: "finallCoorcoordinatedplanCheckuser",
                        index: "finallCoorcoordinatedplanCheckuser",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 最后通过协调维修方案55
                    {
                        name: "passFinallCoorcoordinatedplan",
                        index: "passFinallCoorcoordinatedplan",
                        editable: false,
                        width: 800,
                        sortable: false
                    },
                    // 结算审核信息56
                    {
                        name: "accountInfo",
                        index: "accountInfo",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 上门费57
                    {
                        name: "visitAmount",
                        index: "visitAmount",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 配件费58
                    {
                        name: "partsAmount",
                        index: "partsAmount",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 加急费59
                    {
                        name: "urgentAmount",
                        index: "urgentAmount",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 其他费60
                    {
                        name: "otherAmount",
                        index: "otherAmount",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 责任鉴定61
                    {
                        name: "dutyIdentify",
                        index: "dutyIdentify",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 维修方案结算审核状态62
                    {
                        name: "accountStatus",
                        index: "accountStatus",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 结算审核人63
                    {
                        name: "accountCheckUser",
                        index: "accountCheckUser",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 结算审核通过时间64
                    {
                        name: "passtimeAccountCheck",
                        index: "passtimeAccountCheck",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 客服报修描述65
                    {
                        name: "faultDescriptionCuse",
                        index: "faultDescriptionCuse",
                        editable: false,
                        width: 480,
                        sortable: false
                    },
                    // 是否保外维修单66
                    {
                        name: "isReleased",
                        index: "isReleased",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 自定义配件信息67
                    {
                        name: "partsInfo",
                        index: "partsInfo",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 统计更新时间68
                    {
                        name: "computeTime",
                        index: "computeTime",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                ],
                // pager: "#table_pager",
                viewrecords: true,
                // pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                multiselect: false,
                // 翻页
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        data.position = nowPage;
                        user_review_list.searchData();
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    data.position = nowPage;
                    user_review_list.searchData();
                },
            });

            // getStatementData.startDate=orderExport.getTodayDate();
            // getStatementData.endDate=orderExport.getTodayDate();
            orderExport.getServerData(nowPage, getStatementData);

            $("#order_table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $("#order_table").setGridWidth($(".order_table").width());
            $(window).bind("resize", function () {
                var b = $(".order_table").width();
                $("#order_table").setGridWidth(b);
            });
        },
        // 获取数据
        getServerData: function (pageIndex, postData) {
            // pageCount = $("#table").jqGrid('getGridParam', 'rowNum');

            win.utils.ajaxPost(win.utils.services.findDataList, postData, function (result) {
                if (result.code == 200) {
                    is_search = false;

                    if (result.data.length) {
                        // totalPages = Math.ceil(result.total / pageCount);

                        gridData = result.data;
                        var gridJson = {
                            // total: totalPages,
                            rows: gridData,
                            // page: pageIndex,
                            // records: result.total
                        };
                        // pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        // $("#order_table").jqGrid('setGridParam', {
                        //     pgtext: pageText
                        // });
                        $("#order_table")[0].addJSONData(gridJson);
                    }
                } else if (result.code == 501) {
                    gridData = {};
                    var gridJson = {
                        total: 0,
                        rows: gridData,
                        page: 0,
                        records: 0
                    };
                    // pageText = '第'+pageIndex+'页 / 共'+totalPages+'页';
                    // $("#order_table").jqGrid('setGridParam', {
                    //     pgtext: pageText
                    // });
                    $("#order_table")[0].addJSONData(gridJson);

                    utils.showMessage('暂无数据！');
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 获取当前用户所负责城市
        getQueryUserCity: function () {
            win.utils.ajaxPost(win.utils.services.queryUserCity, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                userId: win.utils.getCookie('user_id')
            }, function (result) {
                if (result.result_code == 200) {
                    var cityOptionStr = '<option value="">请选择城市</option>';
                    result.sysUserCityVOS.forEach(function (i, k) {
                        cityOptionStr += '<option value="' + i.cityId + '">' + i.addressName + '</option>';
                    });
                    $('#search_city').html(cityOptionStr);
                }
            })
        },
        // 获取维修员数据
        getRepairmanData: function () {
            win.utils.ajaxPost(win.utils.services.getWorker, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
            }, function (result) {
                if (result.result_code == 200) {
                    var optionStr = '<option value="">请选择维修员</option>';
                    result.workerInfoVOS.forEach(function (i, k) {
                        optionStr += '<option value="' + i.workerId + '">' + i.workerName + '</option>'
                    });
                    $('select[name="search_repairman"]').html(optionStr);
                    layui.use('form', function () {
                        form = layui.form;
                        form.render('select');
                    });
                }
            });
        },
        // 查询
        queryRecord: function () {
            if (is_search) {
                getStatementData.cityName = $('#search_city').val() ? $('#search_city').find('option:selected').text() : "";
                getStatementData.workerId = $('#search_repairman').val() ? $('#search_repairman').val(): "";

                var createStartDate = $('#createTime').val().split(' - ')[0] ? $('#createTime').val().split(' - ')[0] + ' 00:00:00' : null,
                    createEndDate = $('#createTime').val().split(' - ')[1] ? $('#createTime').val().split(' - ')[1] + ' 23:59:59' : null,

                    allocationStartTime = $('#acceptTime').val().split(' - ')[0] ? $('#acceptTime').val().split(' - ')[0] + ' 00:00:00' : null,
                    allocationEndTime = $('#acceptTime').val().split(' - ')[1] ? $('#acceptTime').val().split(' - ')[1] + ' 23:59:59' : null,

                    finishStartTime = $('#finishTime').val().split(' - ')[0] ? $('#finishTime').val().split(' - ')[0] + ' 00:00:00' : null,
                    finishEndTime = $('#finishTime').val().split(' - ')[1] ? $('#finishTime').val().split(' - ')[1] + ' 23:59:59' : null;


                if ((createStartDate && !allocationStartTime && !finishStartTime) || (!createStartDate && allocationStartTime && !finishStartTime) || (!createStartDate && !allocationStartTime && finishStartTime)) {
                    getStatementData.createStartDate = createStartDate || "";
                    getStatementData.createEndDate = createEndDate || "";
                    getStatementData.allocationStartTime = allocationStartTime || "";
                    getStatementData.allocationEndTime = allocationEndTime || "";
                    getStatementData.finishStartTime = finishStartTime || "";
                    getStatementData.finishEndTime = finishEndTime || "";
                    orderExport.getServerData(nowPage, getStatementData);
                } else {
                    utils.showMessage('有且只能有一个时间筛选项！');
                }
            }
        },
        // 刷新
        refersh: function () {
            $('#search_city').val('');
            $('#createTime').val('');
            $('#acceptTime').val('');
            $('#finishTime').val('');
            $('#search_repairman').val('');
            layui.use('form', function () {
                var form = layui.form;
                form.render('select')
            });

            getStatementData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
            };
            orderExport.getServerData(nowPage, getStatementData);
        },
        // // 获取当前日期
        // getTodayDate:function(){
        // 	var date=new Date();
        // 	var year = date.getFullYear().toString();
        // 	var month = (+date.getMonth()+1>=10?+date.getMonth()+1:'0'+(+date.getMonth()+1)).toString();
        // 	var day = (date.getDate()-1)>=10?date.getDate()-1:'0'+(date.getDate()-1).toString();
        //
        // 	return year+month+day;
        // },
    };
    win.orderExport = orderExport;
})(window, jQuery);
