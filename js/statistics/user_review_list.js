/**
 * Create by hanlu
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    //分页-当前页---当前条数
    var nowPage = 1;
    var batch = true;
    var flagInput = false;
    var is_search = false;
    // 请求参数
    var data = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        city: null, //城市
        clientName: null, //客户姓名
        clientType: null, //客户类型
        creatTimeEnd: null, //创建结束时间
        creatTimeStart: null, //创建开始时间
        houseId: null, //公寓id
        houseSite: null, //公寓地址
        orderId: null, //工单id
        position: nowPage, //当前页
        responsible: null, //责任人姓名
        roomNumber: null, //房间号
        type: null, //工单类型
        rows: 20
    };

    // 搜索
    $('#btnSearch').on('click', function () {
        user_review_list.searchRecord();
    });
    // 刷新
    $('#btnQuery').on('click', function () {
        user_review_list.refresh();
    });

    // 批量操作
    $('.batch_opt').on('click', function () {
        if (batch) {
            $(this).addClass('btn-info');
            $("#table").setGridParam({multiselect: true}).trigger("reloadGrid");
            batch = false;
        } else {
            $(this).removeClass('btn-info');
            batch = true;
        }
    });

    // 表格宽度自适应
    $(window).bind("resize", function () {
        var b = $(".jqGrid_wrapper").width();
        $("#table").setGridWidth(b);
        $("#table").setGridHeight(utils.getAutoGridHeight());
    });
    $(window).resize();

    var user_review_list = {
        //初始化
        init: function () {
            var that = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }

            //判断是否有订单权限，此时为配置管理的工单权限
            utils.isUserMenuIndexRight('evaluate_statistics', function (result) {
                if (result == 1) {
                    that.getServerData(nowPage, data);
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });


            user_review_list.getCityData();
            user_review_list.getRepairmanData();
            user_review_list.getApartmentAddr();
            user_review_list.getCustomerName();
            user_review_list.getSatisfaction();


            $(document).keydown(function (event) {
                var e = window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if (flagInput == false) {
                        if (e.preventDefault) {
                            e.preventDefault();
                        } else {
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });

        },
        getServerData: function (pageIndex, postData) {
            is_search = false;
            win.utils.ajaxPost(
                win.utils.services.getEvaluate,
                postData,
                function (result) {
                    if (result.result_code == 200) {
                        if (result.total == 0) {
                            gridData = {};
                            var gridJson = {
                                total: 0,
                                rows: gridData,
                                page: 0,
                                records: result.total
                            };
                            pageText = '第0页 / 共0页';
                            $("#table").jqGrid('setGridParam', {
                                pgtext: pageText
                            });
                            $("#table")[0].addJSONData(gridJson);
                            $('.totalNum').text(result.total);
                        } else {
                            // 获取表头数据
                            win.utils.ajaxPost(win.utils.services.queryHeader, {
                                user_id: win.utils.getCookie('user_id'),
                                session: win.utils.getCookie('session'),
                            }, function (res) {
                                if (res.result_code == 200) {
                                    var colNames = [];  // 表头数据
                                    var colModel = [];  // 列表数据
                                    for (var i = 0; i < res.data.length; i++) {
                                        colNames.push(res.data[i].value);

                                        colModel.push(
                                            {
                                                name: res.data[i].key,
                                                index: res.data[i].key,
                                                editable: false,
                                                width: 200,
                                                sortable: false,
                                                align: 'center'
                                            }
                                        );
                                    }
                                    ;

                                    // 列表初始化
                                    $("#table").jqGrid({
                                        url: '',
                                        styleUI: 'Bootstrap',
                                        datatype: "json",
                                        height: utils.getAutoGridHeight(),
                                        // width:'100%',
                                        autowidth: false,
                                        shrinkToFit: false,
                                        autoScroll: true,
                                        rowNum: 20,
                                        rownumbers: true,
                                        rownumWidth: 60,
                                        pgtext: "第X页 / 共X页",
                                        colNames: colNames,
                                        colModel: colModel,
                                        pager: "#table_pager",
                                        viewrecords: true,
                                        pagerpos: "center",
                                        recordpos: "left",
                                        caption: "",
                                        hidegrid: false,
                                        multiselect: false,
                                        // 翻页
                                        onPaging: function (pgButton) {
                                            deferLoadData = 0;
                                            currentPage = $("#table").jqGrid('getGridParam', 'page');
                                            lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                                            if (pgButton == 'next') {
                                                currentPage = currentPage + 1;
                                            }
                                            if (pgButton == 'last') {
                                                currentPage = lastPage;
                                            }
                                            if (pgButton == 'prev') {
                                                currentPage = currentPage - 1;
                                            }
                                            if (pgButton == 'first') {
                                                currentPage = 1;
                                            }
                                            if (pgButton == 'user') {
                                                deferLoadData = 1;
                                            }
                                            if (pgButton == 'records') {
                                                deferLoadData = 1;
                                            }
                                            nowPage = currentPage;
                                            if (deferLoadData == 0) {
                                                data.position = nowPage;
                                                user_review_list.searchData();
                                            }
                                        },
                                        onSortCol: function (index, iCol, sortorder) {
                                            orderByString = index + ' ' + sortorder;
                                            currentPage = $("#table").jqGrid('getGridParam', 'page');
                                            nowPage = currentPage;
                                            data.position = nowPage;
                                            user_review_list.searchData();
                                        },
                                    });


                                    var b = $(".jqGrid_wrapper").width();
                                    $("#table").setGridWidth(b);
                                    $(window).bind("resize", function () {
                                        var b = $(".jqGrid_wrapper").width();
                                        $("#table").setGridWidth(b);
                                        $("#table").setGridHeight(utils.getAutoGridHeight());
                                    });

                                    pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
                                    totalPages = Math.ceil(result.total / pageCount);
                                    var gridJson = {
                                        total: totalPages,
                                        rows: result.clientEvaluates,
                                        page: pageIndex,
                                        records: result.total
                                    };
                                    pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                                    $("#table").jqGrid('setGridParam', {
                                        pgtext: pageText
                                    });
                                    $("#table")[0].addJSONData(gridJson);
                                    $('.totalNum').text(result.total);
                                } else {
                                    utils.showMessage('获取数据失败，请重试！');
                                }
                            });

                        }
                    } else {
                        utils.showMessage('获取数据失败，请重试！');
                    }
                });
        },
        // 获取当前调度员负责的城市
        getCityData: function () {
            win.utils.ajaxPost(win.utils.services.queryUserCity, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                userId: win.utils.getCookie('user_id')
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.sysUserCityVOS.length) {
                        var cityOptionsStr = '<option value="">请选择城市</option>';
                        result.sysUserCityVOS.forEach(function (i, k) {
                            cityOptionsStr += '<option value="' + i.cityId + '">' + i.addressName + '</option>';
                        });
                        $('#city').html(cityOptionsStr);
                    } else {
                        utils.showMessage('暂无负责城市！');
                    }
                } else {
                    utils.showMessage('获取城市数据失败，请重试！')
                }
            });
        },
        // 获取维修员数据
        getRepairmanData: function () {
            win.utils.ajaxPost(win.utils.services.getWorker, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                // workerStatus:1,
                // isInservice:1
            }, function (result) {
                if (result.result_code == 200) {
                    var optionStr = '<option value="">请选择维修员</option>';
                    result.workerInfoVOS.forEach(function (i, k) {
                        optionStr += '<option value="' + i.workerId + '">' + i.workerName + '</option>'
                    });
                    $('select[name="repairman"]').html(optionStr);
                    layui.use('form', function () {
                        form = layui.form;
                        form.render('select');
                    });
                }
            });
        },
        // 获取公寓地址
        getApartmentAddr: function () {
            win.utils.ajaxPost(win.utils.services.queryOption, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                queryOption: 2
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.data.length) {
                        var apartmentOptionStr = '<option value="">请选择公寓</option>';
                        result.data.forEach(function (i, k) {
                            apartmentOptionStr += "<option value=" + i.split('-')[0].split('#')[1] + ">" + i.split('-').join(' ') + "</option>"
                        });
                        $('select[name="houseSite"]').html(apartmentOptionStr);
                        layui.use('form', function () {
                            form = layui.form;
                            form.render('select');
                        });
                    } else {
                        utils.showMessage('暂无公寓数据！');
                    }
                } else {
                    utils.showMessage('获取公寓地址数据失败，请重试！');
                }
            })
        },
        // 获取客户姓名
        getCustomerName: function () {
            win.utils.ajaxPost(win.utils.services.queryOption, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                queryOption: 1
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.data.length) {
                        var customerOptionStr = '<option value="">请选择客户</option>';
                        result.data.forEach(function (i, k) {
                            customerOptionStr += '<option>' + i + '</option>'
                        });
                        $('select[name="customerName"]').html(customerOptionStr);
                        layui.use('form', function () {
                            form = layui.form;
                            form.render('select');
                        });
                    } else {
                        utils.showMessage('暂无客户数据！');
                    }
                } else {
                    utils.showMessage('获取客户数据失败，请重试！');
                }
            })
        },
        // 获取满意度
        getSatisfaction: function () {
            win.utils.ajaxPost(win.utils.services.querySatisficing, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
            }, function (result) {
                var str = "<option value='' selected>请选择满意度</option>";
                result.data.forEach(function (i, k) {
                    str += '<option value="' + i + '">' + i + '</option>'
                })
                $('#satisfaction').html(str);
                layui.use('form', function () {
                    var form = layui.form;
                    form.render('select');
                })
            })
        },
        // 搜索评价
        searchRecord: function () {
            user_review_list.searchLayerInit();
            user_review_list.forbidScroll();

            var houseSite, customerName, repairman, satisficing;
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '<i class="fa fa-search">&nbsp;&nbsp;搜索评论</i>',
                content: $('#model'),
                btn: ['确认', '取消'],
                success: function () {
                    // layui.use('form',function () {
                    //    var form = layui.form;
                    //     form.on('select(houseSite)',function (data) {
                    //         console.log(data);
                    //         houseSite=data.value;
                    //     });
                    //     form.on('select(customerName)',function (data) {
                    //         customerName = data.value;
                    //     });
                    //     form.on('select(repairman)',function (data) {
                    //         repairman = data.value;
                    //     });
                    //     form.on('select()',function (data) {
                    //         satisficing=data.value;
                    //     })
                    // });

                    flagInput = false;
                },
                yes: function (index, layero) {
                    is_search = true;
                    nowPage = 1;
                    user_review_list.searchData();

                    layer.close(index);
                    user_review_list.allowScroll();
                },
                btn2: function () {
                    is_search = false;
                    user_review_list.allowScroll();
                },
                cancel: function () {
                    is_search = false;
                    user_review_list.allowScroll();
                }
            });
        },
        searchData: function () {
            var createTime = user_review_list.dateFormat($('#timeChoose').val()) || null;
            if (is_search) {
                data.city = $('#city').find('option:selected').text() == '请选择城市' ? null : $('#city').find('option:selected').text() || null; //城市
                data.outOrderNo = $('#orderId').val() || null;
                data.houseId = $('#houseSite').find('option:selected').val() || null;
                // data.houseSite =  $('#houseSite').find('option:selected').text() == '请选择公寓' ? null:$('#houseSite').find('option:selected').text() ||null;
                data.clientName = $('#customerName').find('option:selected').text() == '请选择客户' ? null : $('#customerName').find('option:selected').text() || null; //客户姓名
                data.creatTimeStart = createTime ? createTime[0] : null; //创建开始时间
                data.creatTimeEnd = createTime ? createTime[1] : null; //创建结束时间
                data.responsible = $('#repairman').find('option:selected').text() == '请选择维修员' ? null : $('#repairman').find('option:selected').text() || null; //维修员
                data.satisficing = $('#satisfaction').find('option:selected').val() || null;
                data.position = nowPage;
            }
            user_review_list.getServerData(nowPage, data);
        },
        searchLayerInit: function () {
            $('#city').val('');
            $('#orderId').val('');
            // $('#houseSite').val('请选择公寓');
            // $('#houseSite').find("option[text='请选择公寓']").attr('selected',true);
            $('#houseSite').val('');
            $('#satisfaction').val('');
            $('#customerName').val('');
            $('#repairman').val('');
            $('#timeChoose').val('');
            form.render();
        },
        // 日期格式处理
        dateFormat: function (dataStr) {
            if (dataStr) {
                var startCreateTime = dataStr.split(' - ')[0];
                var endCreateTime = dataStr.split(' - ')[1];
                startCreateTime = user_review_list.datePartFormat(startCreateTime) + ' 00:00:00';
                endCreateTime = user_review_list.datePartFormat(endCreateTime) + ' 23:59:59';
                return [startCreateTime, endCreateTime];
            } else {
                return null
            }
        },
        datePartFormat: function (date) {
            return date.split('-').join('');
        },
        // 刷新
        refresh: function () {
            $('#city').val('');
            $('#orderId').val('');
            $('#type').val('');
            $('#houseSite').val('');
            $('#houseId').val('');
            $('#roomNumber').val('');
            $('#clientType').val('');
            $('#clientName').val('');
            $('#creatTimeStart').val('');
            $('#responsible').val('');


            nowPage = 1;
            is_search = false;
            data = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                city: null, //城市
                clientName: null, //客户姓名
                clientType: null, //客户类型
                creatTimeEnd: null, //创建结束时间
                creatTimeStart: null, //创建开始时间
                houseId: null, //公寓id
                houseSite: null, //公寓地址
                orderId: null, //工单id
                position: nowPage, //当前页
                responsible: null, //责任人姓名
                roomNumber: null, //房间号
                type: null, //工单类型
                rows: 20
            };
            user_review_list.getServerData(nowPage, data);
        },

        //禁止与释放滚动条
        forbidScroll: function () {
            document.body.style.height = '100%'
            document.body.style['overflow-y'] = 'hidden'
        },
        allowScroll: function () {
            document.body.style.height = 'auto';
            document.body.style['overflow-y'] = 'auto';
        }
    }

    win.user_review_list = user_review_list;
})(window, jQuery);