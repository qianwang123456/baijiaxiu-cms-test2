/**
 * Created by hanlu.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';

    var nowPage = 1;

    getNoticeListData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        rows: 20,
        position: 1,
        id: null
    };

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
    }

    var repairman_service_list = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            // 统计查询权限
            utils.isUserMenuIndexRight('epairman_service_data', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });
            // 刷新
            $('#btnRefresh').on('click', function () {
                obj.refersh();
            });
            // 查询
            $('#btnQuery').click(obj.queryRecord);

            obj.getServerData(nowPage, getNoticeListData);
        },
        initList: function () {
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: [
                    "维修员",
                    "报表日期",
                    "待联系(联系中)",
                    "待上门(上门中)",
                    "已上门",
                    "维修方案协调中",
                    "已完成",
                    "结算待审核",
                    "结算审核通过",
                    "当天结算总额",
                    "已撤销",
                    "客户确认(评价数)",
                ],
                colModel: [
                    // 维修员
                    {
                        name: "repairman",
                        index: "repairman",
                        editable: false,
                        width: 60,
                        sortable: false
                    },
                    // 报表日期
                    {
                        name: "reportDate",
                        index: "reportDate",
                        editable: false,
                        width: 100,
                        sortable: true
                    },
                    // 待联系(联系中)
                    {
                        name: "contactStatus",
                        index: "contactStatus",
                        editable: false,
                        width: 80,
                        sorttype: "string"
                    },
                    // 待上门(上门中)
                    {
                        name: "arriveStatus",
                        index: "arriveStatus",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 已上门
                    {
                        name: "arrived",
                        index: "arrived",
                        editable: false,
                        width: 60,
                        sorttype: "string",
                    },
                    // 维修方案协调中
                    {
                        name: "planCoordinate",
                        index: "planCoordinate",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 已完成
                    {
                        name: "completed",
                        index: "completed",
                        editable: false,
                        width: 60,
                        sortable: false
                    },
                    // 结算待审核
                    {
                        name: "toBeAudited",
                        index: "toBeAudited",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 结算审核通过
                    {
                        name: "audited",
                        index: "audited",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 当天结算总额
                    {
                        name: "balanceTotalNum",
                        index: "balanceTotalNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 已撤销
                    {
                        name: "rescinded",
                        index: "rescinded",
                        editable: false,
                        width: 60,
                        sortable: false
                    },
                    // 客户确认(评价数)
                    {
                        name: "constomerConfirm",
                        index: "constomerConfirm",
                        editable: false,
                        width: 80,
                        sortable: false
                    },

                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    if (deferLoadData == 0) {
                        repairman_service_list.getServerData(currentPage, getNoticeListData);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        repairman_service_list.getServerData(currentPage, getNoticeListData);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    repairman_service_list.getServerData(currentPage, getNoticeListData);
                },
                gridComplete: function () {
                }
            });

            repairman_service_list.getServerData(nowPage, getNoticeListData);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        // 获取数据
        getServerData: function (pageIndex, postData) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.getNoticeInfo, postData, function (result) {
                if (result.result_code == 200) {
                    if (result.noticeInfos == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.mains.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table").addJSONData(gridJson);
                    } else {
                        gridData = result.noticeInfos;
                        gridData = [
                            {
                                repairman: "尚国昌",
                                reportDate: "2018-02-03",
                                contactStatus: "0",
                                arriveStatus: "1",
                                arrived: "0",
                                planCoordinate: "2",
                                completed: "1",
                                toBeAudited: "17",
                                audited: "32",
                                balanceTotalNum: "0",
                                rescinded: "0",
                                constomerConfirm: "0"
                            },
                            {
                                repairman: "尚国昌",
                                reportDate: "2018-02-03",
                                contactStatus: "0",
                                arriveStatus: "1",
                                arrived: "0",
                                planCoordinate: "2",
                                completed: "1",
                                toBeAudited: "17",
                                audited: "32",
                                balanceTotalNum: "0",
                                rescinded: "0",
                                constomerConfirm: "0"
                            },
                            {
                                repairman: "尚国昌",
                                reportDate: "2018-02-03",
                                contactStatus: "0",
                                arriveStatus: "1",
                                arrived: "0",
                                planCoordinate: "2",
                                completed: "1",
                                toBeAudited: "17",
                                audited: "32",
                                balanceTotalNum: "0",
                                rescinded: "0",
                                constomerConfirm: "0"
                            }
                        ];
                        gridData.forEach(function (i, k) {
                            i.createTime = i.createTime ? repairman_service_list.dateFormat(i.createTime) : '';
                            i.modifyTime = i.modifyTime ? repairman_service_list.dateFormat(i.modifyTime) : '';
                        });
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 查询
        queryRecord: function () {
            initializeEditForm();
            repairman_service_list.searchLayerInit();
            var i = layer.open({
                type: 1,
                area: ['60%', '60%'],
                title: '<i class="fa fa-search">&nbsp;&nbsp;查询维修员业务数据</i>',
                content: $('#model'),
                btn: ['确认', '取消'],
                yes: function (index, layero) {
                    console.log($('#timeChoose').val());
                    var createTime = repairman_service_list.dateFormat($('#timeChoose').val()) || null;
                    console.log(createTime);

                    console.log(document.selected.repairman.value || null);
                    console.log(document.selected.status.value || null);
                    console.log(document.selected.phone.value || null);
                    console.log(document.selected.wordNum.value || null);
                    // console.log(repairman_service_list.dateFormat(document.selected.startTime.value));
                    // console.log(repairman_service_list.dateFormat(document.selected.endTime.value));
                    layer.close(index);
                }
            });
        },
        // 刷新
        refersh: function () {
            $('#repairman').val('');
            $('#status').val('');
            $('#phone').val('');
            $('#wordNum').val('');
            $('#startTime').val('');
            $('#endTime').val('');
            getNoticeListData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rows: 20,
                position: 1,
                id: null
            };
            repairman_service_list.getServerData(nowPage, getNoticeListData);
        },
        // 日期格式处理
        dateFormat: function (dataStr) {
            if (dataStr) {
                var startCreateTime = dataStr.split(' - ')[0];
                var endCreateTime = dataStr.split(' - ')[1];
                startCreateTime = repairman_service_list.datePartFormat(startCreateTime);
                endCreateTime = repairman_service_list.datePartFormat(endCreateTime);
                return [startCreateTime, endCreateTime];
            } else {
                return null
            }
        },
        datePartFormat: function (date) {
            return date.split(' ')[0].split('-').join('') + ' ' + date.split(' ')[1];
        },
        // 查询弹层初始化
        searchLayerInit: function () {
            $('#repairman').val('');
            $('#status').val('');
            $('#phone').val('');
            $('#wordNum').val('');
            $('#timeChoose').val('');
        },
    };
    win.repairman_service_list = repairman_service_list;
})(window, jQuery);
