/**
 * Created by song.
 */
(function(win, $) {
	var role = 'dispatch'; // 调度与协调
	var classifyType = -1 ;// 个人，小组，全部- -1， 个人-0  小组 -1
	var deferLoadData =0
	var flagInput = false
	// 初始化
	function clearAllDataNow(){
		//强制清空所有数据
		$("#createTime").val('')
		role = 'dispatch';
		classifyType = -1
	}
    // 公共调用方法合并单元格（无需修改）
	function MergerRowspan(gridName, CellName) {
		//得到显示到界面的id集合
		var mya = $("#" + gridName + "").getDataIDs();
		// console.log(mya)
		//数据总行数
		var length = mya.length;
		//定义合并行数
		var rowSpanTaxCount = 1;
		var bgFlag = true;
		for (var i = 0; i < length; i += rowSpanTaxCount) {
			//从当前行开始比对下面的信息
			var before = $("#" + gridName + "").jqGrid('getRowData', mya[i]);
			$('#'+mya[i]).find("td").addClass("SelectBG");
			// console.log(mya[i])
	        if(bgFlag == true){
				bgFlag = false
				$('#'+mya[i]).find("td").eq(1).addClass("SelectOneBG");
			}else{
				bgFlag = true
				$('#'+mya[i]).find("td").eq(1).addClass("SelectTwoBG");
			}
			rowSpanTaxCount = 1;
			for (j = i + 1; j <= length; j++) {
				//和上边的信息对比 如果值一样就合并行数+1 然后设置rowspan 让当前单元格隐藏
				var end = $("#" + gridName + "").jqGrid('getRowData', mya[j]);
				if (before[CellName] == end[CellName]) {
					rowSpanTaxCount++;
					$("#" + gridName + "").setCell(mya[j], CellName, '', { display: 'none' });
				} else {
					break;
				}
			}
			$("#" + gridName + "").setCell(mya[i], CellName, '', '', { rowspan: rowSpanTaxCount });
		}
	}
	var report = {
		//初始化
		init: function() {
			var obj = this;
			if(utils.isLogin() == false) {
				utils.loginDomain();
				return;
			}
			//判断是否有kpi报表权限
			utils.isUserMenuIndexRight('kpiReport', function(result) {
				if(result == 1) {
					//initialize...
					obj.initList();
					win.utils.clearSpacesTextarea();
				} else {
					self.location = '../../../web/error/access_refuse.html';
					return;
				}
			});
			//刷新
			$('#empty').on('click', function() {
				clearAllDataNow()
				report.getServerData(1);
			});
			//搜搜-more
			$('#search').on('click',function(){
				report.getServerData(1);
			});
			//导出
			$('#export').on('click',function(){
				report.getServerData(2);
			});
			//搜索栏-下面-状态按钮操作
			$("#seBtnAlls button").on("click",function(){
				let nameVal=$(this).attr('name');
				role = nameVal
				$(this).find(".green").eq(0).hide();
				$("#seBtnAlls button").removeClass('btn-info');
				$(this).addClass('btn-info');
				report.getServerData(1);
			});
		},
		//初始化请求
		initList: function() {
		    $("#xtCon").css({opacity:0})
			//调度table
			$("#tableDd").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height:'auto',//utils.getAutoGridHeightSong(),
				autowidth: true,
				shrinkToFit:false,
				autoScroll: true,
				rowNum: 999,
				rownumbers: true,
				rownumWidth:80,
				// rowList: [10,20,50,100],
				// pgtext: "第X页 / 共X页",
				colNames: ["组别",'姓名','派单量',"派单占比","派单超时占比","派单转单量","派单转单率","审核单量","审核率","审核平均时长(分钟)","审核超时时长","审核转单量","审核转单率","结算单量","结算占比","结算平均时长(分钟)","结算超时占比","结算转单量","结算转单率"],
				colModel: [
                    {
                        name: "teamName",
                        index: "teamName",
                        editable: false,
                        width: 120,
                        hidden:false,
						sortable: false
                    },
                    {
                        name: "userName",
                        index: "userName",
                        editable: false,
                        width: 120,
                        hidden:false,
						sortable: false,
						formatter: function(cellvalue,options, rowObject) {
							// 0-组员 1-组长
							let str = rowObject.dataType ==1?'<b>合计</b>':cellvalue
							return str
						}
                    },
					{
						name: "sendOrdersNum",
						index: "sendOrdersNum",
						editable: false,
						width: 120,
						hidden:false,
						sortable: false,
						sorttype: "string",
					},
					{
						name: "sendOrderRatio",
						index: "sendOrderRatio",
						editable: false,
						width: 80,
						hidden:false,
						sortable: false,
                        formatter: function(cellvalue,options, rowObject) {
                            let str = Number(cellvalue) == 0?cellvalue:cellvalue+'%'
                            return str
                        }
					},
					{
						name: "sendOvertimeRatio",
						index: "sendOvertimeRatio",
						editable: false,
						width: 80,
						hidden:false,
						sortable: false,
						formatter: function(cellvalue,options, rowObject) {
							let str = Number(cellvalue) == 0?cellvalue:cellvalue+'%'
							return str
						}
					},
					{
						name: "sendFlowOrdersNum",
						index: "sendFlowOrdersNum",
						editable: false,
						width: 180,
						hidden:false,
						sortable: false,
						sorttype: "string",
					},
					{
						name: "sendFlowOrdersRatio",
						index: "sendFlowOrdersRatio",
						editable: false,
						width: 150,
						sortable: false,
						sorttype: "string",
                        formatter: function(cellvalue,options, rowObject) {
                            let str = Number(cellvalue) == 0?cellvalue:cellvalue+'%'
                            return str
                        }
					},
					{
						name: "checkOrdersNum",
						index: "checkOrdersNum",
						editable: false,
						width: 180,
						sorttype: "string",
						sortable: false
					},
					{
						name: "checkOrdersRatio",
						index: "checkOrdersRatio",
						editable: false,
						width: 80,
						sorttype: "string",
                        formatter: function(cellvalue,options, rowObject) {
                            let str = Number(cellvalue) == 0?cellvalue:cellvalue+'%'
                            return str
                        },
						sortable: false
					},
					{
						name: "personCheckAvgtime",
						index: "personCheckAvgtime",
						editable: false,
						width: 80,
						sorttype: "string",
						sortable: false
					},
					{
						name: "checkOvertimeTimeRatio", 
						index: "checkOvertimeTimeRatio",
						editable: false,
						width: 120,
						sorttype: "string",
						sortable: false
					},
					{
						name: "checkFlowOrdersNum",
						index: "checkFlowOrdersNum",
						editable: false,
						width: 150,
						sorttype: "string",
						sortable: false
					},
					{
						name: "checkFlowOrdersRatio",
						index: "checkFlowOrdersRatio",
						editable: false,
						width: 200,
						sorttype: "string",
                        formatter: function(cellvalue,options, rowObject) {
                            let str = Number(cellvalue) == 0?cellvalue:cellvalue+'%'
                            return str
                        },
						sortable: false
					},
					{
						name: "settlementOrdersNum",
						index: "settlementOrdersNum",
						editable: false,
						width: 150,
						sorttype: "string",
						sortable: false
					},
					{
						name: "settlementOrdersRatio",
						index: "settlementOrdersRatio",
						editable: false,
						width: 120,
						sorttype: "string",
                        formatter: function(cellvalue,options, rowObject) {
						    let str = Number(cellvalue) == 0?cellvalue:cellvalue+'%'
                            return str
                        },
						sortable: false
					},
					{
						name: "settlementOrdersAvgRatio",
						index: "settlementOrdersAvgRatio",
						editable: false,
						width: 120,
						sorttype: "string",
						sortable: false
					},
					{
						name: "settlementOvertimeTimeRatio",
						index: "settlementOvertimeTimeRatio",
						editable: false,
						width: 150,
						sorttype: "string",
                        formatter: function(cellvalue,options, rowObject) {
                            let str = Number(cellvalue) == 0?cellvalue:cellvalue+'%'
                            return str
                        },
						sortable: false
					},
					{
						name: "settlementFlowNum",
						index: "settlementFlowNum",
						editable: false,
						width: 120,
						sorttype: "string",
						sortable: false
					},
					{
						name: "settlementFlowRatio",
						index: "settlementFlowRatio",
						editable: false,
						width: 92,
						sorttype: "string",
                        formatter: function(cellvalue,options, rowObject) {
                            let str = Number(cellvalue) == 0?cellvalue:cellvalue+'%'
                            return str
                        },
						sortable: false
					}
				],
				pager: "#table_pager",
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
				pgbuttons:true,
				pginput:true,
				onPaging: function(pgButton) {
					deferLoadData = 0;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					lastPage = $("#table").jqGrid('getGridParam', 'lastpage');
					if(pgButton == 'next') {
						currentPage = currentPage + 1;
					}
					if(pgButton == 'last') {
						currentPage = lastPage;
					}
					if(pgButton == 'prev') {
						currentPage = currentPage - 1;
					}
					if(pgButton == 'first') {
						currentPage = 1;
					}
					if(pgButton == 'user') {
						deferLoadData = 1;
					}
					if(pgButton == 'records') {
						deferLoadData = 1;
					}
					if(deferLoadData == 0) {
						report.getServerData(1);
					}
				},
				beforeRequest: function() {
					if(deferLoadData == 1) {
						report.getServerData(1);
						deferLoadData = 0;
					}
				},
				onSortCol: function(index, iCol, sortorder) {
					orderByString = index + ' ' + sortorder;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					report.getServerData(1);
				},
				//当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
				onSelectRow: function () {
				},
				gridComplete: function() {
					var gridName = 'tableDd';
                    //动态合并纵行
                    MergerRowspan(gridName, 'teamName');
				}
			});
			//协调table
			$("#tableXt").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height:'auto',//utils.getAutoGridHeightSong(),
				autowidth: true,
				shrinkToFit:false,
				autoScroll: true,
				rowNum: 999,
				rownumbers: true,
				rownumWidth:80,
				// rowList: [10,20,50,100],
				// pgtext: "第X页 / 共X页",
                colNames: ["组别",'姓名','工作量',"关单量","关单率","关单时效(分钟)","一键转办单量","一键转办时效","转办防水工作量","转派防水时效"],
				colModel: [
					{
						name: "teamName",
						index: "teamName",
						editable: false,
						width: 120,
						sorttype: "string",
						sortable: false
					},
					{
						name: "userName",
						index: "userName",
						editable: false,
						width: 120,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							// 0-组员 1-组长
							let str = rowObject.dataType ==1?'<b>合计</b>':cellvalue
							return str
						},
						sortable: false
					},
					{
						name: "workNum",
						index: "workNum",
						editable: false,
						width: 150,
						sorttype: "string",
						sortable: false
					},
					{
						name: "closeOrdersNum",
						index: "closeOrdersNum",
						editable: false,
						width: 120,
						sorttype: "string",
						sortable: false
					},
					{
						name: "closeOrdersRatio",
						index: "closeOrdersRatio",
						editable: false,
						width: 120,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							let z='';
							if(Number(cellvalue)==0){
                                z='';
							}else{
                                z='%';
							}
							return  cellvalue+z
						},
						sortable: false
					},
					{
						name: "closeOrdersTotalTimeRatio",
						index: "closeOrdersTotalTimeRatio",
						editable: false,
						width: 180,
						sorttype: "string",
						sortable: false
                        // formatter: function(cellvalue,options, rowObject) {
                        //     let z='';
                        //     if(Number(cellvalue)==0){
                        //         z='';
                        //     }else{
                        //         z='%';
                        //     }
                        //     return  cellvalue+z
                        // },
					},
					{
						name: "oneComplaintNum",
						index: "oneComplaintNum",
						editable: false,
						width: 120,
						sorttype: "string",
						sortable: false
					},
					{
						name: "oneComplaintTotalTimeRatio",
						index: "oneComplaintTotalTimeRatio",
						editable: false,
						width: 120,
						sorttype: "string",
						sortable: false
                        // formatter: function(cellvalue,options, rowObject) {
                        //     let z='';
                        //     if(Number(cellvalue)==0){
                        //         z='';
                        //     }else{
                        //         z='%';
                        //     }
                        //     return  cellvalue+z
                        // },
					},
					{
						name: "waterproofNum",
						index: "waterproofNum",
						editable: false,
						width: 100,
						sorttype: "string",
						sortable: false
					},
					{
						name: "waterproofTotalTimeRatio",
						index: "waterproofTotalTimeRatio",
						editable: false,
						width: 120,
						sorttype: "string",
						sortable: false
					},
				],
				pager: "#table_pager",
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
				pgbuttons:true,
				pginput:true,
				onPaging: function(pgButton) {
					deferLoadData = 0;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					lastPage = $("#table").jqGrid('getGridParam', 'lastpage');
					if(pgButton == 'next') {
						currentPage = currentPage + 1;
					}
					if(pgButton == 'last') {
						currentPage = lastPage;
					}
					if(pgButton == 'prev') {
						currentPage = currentPage - 1;
					}
					if(pgButton == 'first') {
						currentPage = 1;
					}
					if(pgButton == 'user') {
						deferLoadData = 1;
					}
					if(pgButton == 'records') {
						deferLoadData = 1;
					}
					if(deferLoadData == 0) {
						report.getServerData(1);
					}
				},
				beforeRequest: function() {
					if(deferLoadData == 1) {
						report.getServerData(1);
						deferLoadData = 0;
					}
				},
				onSortCol: function(index, iCol, sortorder) {
					orderByString = index + ' ' + sortorder;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					report.getServerData(1);
				},
				//当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
				onSelectRow: function () {
				},
				gridComplete: function() {
                    var gridName = 'tableXt';
                    //动态合并纵行
                    MergerRowspan(gridName, 'teamName');
				}
			});
			report.getServerData(1);
			$(window).bind("resize", function () {
				var b = $(".jqGrid_wrapper").width();
				if(b>=1844){
					b=1844;
				};
				$("#tableDd").setGridWidth(b);
				$("#tableXt").setGridWidth(b);
			});
		},
		//获取list列表---导出
		getServerData: function(type) {
			//type:1-获取list 2-导出list
		    if(role=='coordinate'){
                $("#xtCon").css({opacity:1})
            }else{
			}
			let strFp = $("#createTime").val()
            let orderFpTimeSt=''
            let orderFpTimeEn='';
			if(strFp.length!=0){
				orderFpTimeSt=strFp.slice(0,10);//创建时间开始
				orderFpTimeEn=strFp.slice(13,strFp.length);//创建时间结束
				// orderFpTimeSt=strFp.slice(0,17)+"00";//创建时间开始---时分秒
				// orderFpTimeEn=strFp.slice(22,strFp.length-2)+"00";//创建时间结束---时分秒
				// console.log(orderFpTimeSt,'===',orderFpTimeEn);
			}else{
				orderFpTimeSt='';//创建时间开始
				orderFpTimeEn='';//创建时间结束
				let gridData = [];
				var gridJson = {
					rows: gridData
				};
				if(role=='dispatch'){
					$("#ddCon").show()
					$("#xtCon").hide()
					$("#tableDd")[0].addJSONData(gridJson);
				}else{
					$("#ddCon").hide()
					$("#xtCon").show()
					$("#tableXt")[0].addJSONData(gridJson);
				}
				return;
			};
			let dateValueType = $("input[name='dataType']:checked").val()
            if(dateValueType.indexOf('single')>=0){
                classifyType = 0
            }else if(dateValueType.indexOf('group')>=0){
                classifyType = 1
            }else if(dateValueType.indexOf('all')>=0){
                classifyType = -1
            }
			if(classifyType==1){
				if(role=='dispatch'){
					$("#tableDd").setGridParam().hideCol("userName").trigger("reloadGrid");
				}else{
					$("#tableXt").setGridParam().hideCol("userName").trigger("reloadGrid");
				};
			}else{
				if(role=='dispatch'){
					$("#tableDd").setGridParam().showCol("userName").trigger("reloadGrid");
				}else{
					$("#tableXt").setGridParam().showCol("userName").trigger("reloadGrid");
				};
			}
			let datas = {
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				createTimeStart:orderFpTimeSt,
				createTimeEnd:orderFpTimeEn,
				dateType:classifyType, //全部= -1， 个人=0  小组 =1 classifyType
				kpiType:role // 调度与协调角色("dispatch","调度"),"coordinate","协调");
			}
			let urlLink = type==1?win.utils.services.kpiList:win.utils.services.kpiListExport;
            if(type==1){
				win.utils.ajaxPost(urlLink, datas, function(result) {
					if(result.code == 200) {
						let gridData = result.data;
						var gridJson = {
							rows: gridData
						};
						if(role=='dispatch'){
							$("#ddCon").show()
							$("#xtCon").hide()
							$("#tableDd")[0].addJSONData(gridJson);
						}else{
							$("#ddCon").hide()
							$("#xtCon").show()
							$("#tableXt")[0].addJSONData(gridJson);
						}
					} else {
						let errormsg = '';
						if(role=='dispatch'){
							errormsg = result.message?result.message:'获取调度数据失败';
						}else{
							errormsg = result.message?result.message:'获取协调数据失败';
						}
						utils.showMessage(errormsg);
					}
				});
			}else{
				// 请求参数
				let dataArr = [];
				for (let key in datas) {
					dataArr.push(key + '=' + datas[key]);
				}
				window.location.href = win.utils.services.kpiListExport + '?' + dataArr.join('&');
			}
		},
	};
	// 阻止空格与enter键触发layer.open事件（即出现蒙层）
	$(document).keydown(function (event) {
		var  e=window.event || event;
		var keyCode = event.keyCode;
		if (keyCode == 13 || keyCode == 32) {
			if(flagInput==false){
				if(e.preventDefault){
					e.preventDefault();
				}else{
					window.event.returnValue = false;
				}
				return false;
			}
		}
	});
	win.report = report;
})(window, jQuery);
