/**
 * Created by hanlu.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';

    var nowPage = 1;

    getStatementData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        startDate: null,
        endDate: null,
        cityId: null,
        type: 1
    };

    var BIstatement = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            // 统计查询权限
            utils.isUserMenuIndexRight('biStatement', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });

            // 获取城市
            BIstatement.getQueryUserCity();

            // 刷新
            $('#btnRefresh').on('click', function () {
                obj.refersh();
            });
            // 查询
            $('#btnQuery').click(obj.queryRecord)

            $('.content button').on('click', function () {
                var type = $(this).index();
                $('.content button').removeClass('active').eq(type).addClass('active');
                $('.jqGrid_wrapper').hide().eq(type).show();
                $('.rule div').hide().eq(type).show();
                // getStatementData.type=type+1;

                switch (type) {
                    case 0:
                        $("#order_type_table").setGridWidth($(".order_type_table").width());
                        break;
                    case 1:
                        $("#quality_table").setGridWidth($(".quality_table").width());
                        break;
                    case 2:
                        $("#aging_table").setGridWidth($(".aging_table").width());
                        break;
                    case 3:
                        $("#cost_table").setGridWidth($(".cost_table").width());
                        break;
                    case 4:
                        $("#scale_table").setGridWidth($(".scale_table").width());
                        break;
                    case 5:
                        $("#KPI_table").setGridWidth($(".KPI_table").width());
                        break;
                }

                BIstatement.queryRecord();
            });


            // 导出功能
            $('#export').on('click', function () {
                // 请求参数
                data = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    startDate: $('#startTime').val().split('-').join(''),
                    endDate: $('#endTime').val().split('-').join(''),
                    cityId: $('#search_city').val(),
                    type: 1
                };

                var buttonArr = $('.toolbar button');
                var tableName;
                for (let i = 0; i < buttonArr.length; i++) {
                    if ($(buttonArr[i]).hasClass('active')) {
                        data.type = i + 1;
                    }
                }

                var dataArr = [];
                for (var key in data) {
                    dataArr.push(key + '=' + data[key]);
                }
                window.location.href = win.utils.services.exportReport + '?' + dataArr.join('&');
            });
        },
        initList: function () {
            $("#order_type_table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                colNames: [
                    "城市",
                    "单量",
                    "综合单量",
                    "综合完成",
                    "电器单量",
                    "电器完成",
                    "电器综合单量",
                    "电器综合完成",
                    "防水单量",
                    "防水完成",
                    "报修单量",
                    "返工单量",
                    "巡房单量",
                    "退租单量",
                    "巡检单量 ",
                    "撤销单量",
                    "单量",
                    "综合单量",
                    "综合完成",
                    "电器单量",
                    "电器完成",
                    "电器综合单量",
                    "电器综合完成",
                    "防水单量",
                    "防水完成",
                    "报修单量",
                    "返工单量",
                    "巡房单量",
                    "退租单量",
                    "巡检单量 ",
                    "撤销单量",
                ],
                colModel: [
                    // 城市
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 单量
                    {
                        name: "orderNum",
                        index: "orderNum",
                        editable: false,
                        width: 80,
                        sortable: true
                    },
                    // 当日综合单量
                    {
                        name: "synthesizeNum",
                        index: "synthesizeNum",
                        editable: false,
                        width: 80,
                        sorttype: "string"
                    },
                    // 当日综合完成单量
                    {
                        name: "synthesizeFinishNum",
                        index: "synthesizeFinishNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 当日电器单量
                    {
                        name: "electricNum",
                        index: "electricNum",
                        editable: false,
                        width: 80,
                        sorttype: "string",
                    },
                    // 当日电器完成单量
                    {
                        name: "electricFinishNum",
                        index: "electricFinishNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 当日电器综合单量
                    {
                        name: "synElectricNum",
                        index: "synElectricNum",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    // 当日电器综合完成单量
                    {
                        name: "synElectricFinishNum",
                        index: "synElectricFinishNum",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    // 当日防水单量
                    {
                        name: "waterproofNum",
                        index: "waterproofNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 当日防水完成单量
                    {
                        name: "waterproofFinishNum",
                        index: "waterproofFinishNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 当日报修单量
                    {
                        name: "repairNum",
                        index: "repairNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 当日返工单量
                    {
                        name: "reworkNum",
                        index: "reworkNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 当日巡房单量
                    {
                        name: "roundHouseNum",
                        index: "roundHouseNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 当日退租单量
                    {
                        name: "surrenderNum",
                        index: "surrenderNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 单日巡检单量
                    {
                        name: "pollingNum",
                        index: "pollingNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 当日撤销单量
                    {
                        name: "backoutNum",
                        index: "backoutNum",
                        editable: false,
                        width: 80,
                        sortable: true
                    },
                    // 累计单量
                    {
                        name: "monthOrderNum",
                        index: "monthOrderNum",
                        editable: false,
                        width: 80,
                        sortable: true
                    },
                    // 累计综合单量
                    {
                        name: "monthSynthesizeNum",
                        index: "monthSynthesizeNum",
                        editable: false,
                        width: 80,
                        sorttype: "string"
                    },
                    // 累计综合完成单量
                    {
                        name: "monthSynthesizeFinishNum",
                        index: "monthSynthesizeFinishNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 累计电器单量
                    {
                        name: "monthElectricNum",
                        index: "monthElectricNum",
                        editable: false,
                        width: 80,
                        sorttype: "string",
                    },
                    // 累计电器完成单量
                    {
                        name: "monthElectricFinishNum",
                        index: "monthElectricFinishNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 累计电器综合单量
                    {
                        name: "monthSynElectricNum",
                        index: "monthSynElectricNum",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    // 累计电器综合完成单量
                    {
                        name: "monthSynElectricFinishNum",
                        index: "monthSynElectricFinishNum",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    // 累计防水单量
                    {
                        name: "monthWaterproofNum",
                        index: "monthWaterproofNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 累计防水完成单量
                    {
                        name: "monthWaterproofFinishNum",
                        index: "monthWaterproofFinishNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 累计保修单量
                    {
                        name: "monthRepairNum",
                        index: "monthRepairNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 累计返工单量
                    {
                        name: "monthReworkNum",
                        index: "monthReworkNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 累计巡房单量
                    {
                        name: "monthRoundHouseNum",
                        index: "monthRoundHouseNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 累计退租单量
                    {
                        name: "monthSurrenderNum",
                        index: "monthSurrenderNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 累计巡检单量
                    {
                        name: "monthPollingNum",
                        index: "monthPollingNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 累计撤销单量
                    {
                        name: "monthBackoutNum",
                        index: "monthBackoutNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },

                ],
                pager: "#order_type_table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
            });

            getStatementData.startDate = BIstatement.getTodayDate();
            getStatementData.endDate = BIstatement.getTodayDate();
            BIstatement.getServerData(getStatementData, $('#order_type_table'));

            $("#order_type_table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $("#order_type_table").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [
                    {startColumnName: 'cityName', numberOfColumns: 2, titleText: '名称'},
                    {startColumnName: 'synthesizeNum', numberOfColumns: 8, titleText: '当日工单类型'},
                    {startColumnName: 'repairNum', numberOfColumns: 6, titleText: '当日工单标签'},
                    // {startColumnName:'chexiao_day', numberOfColumns: 1, titleText: '名称'},
                    {startColumnName: 'monthOrderNum', numberOfColumns: 8, titleText: '累计工单类型'},
                    {startColumnName: 'monthRepairNum', numberOfColumns: 6, titleText: '累计工单标签'},
                ]
            })


            $("#order_type_table").setGridWidth($(".order_type_table").width());
            $(window).bind("resize", function () {
                var b = $(".order_type_table").width();
                $("#order_type_table").setGridWidth(b);
            });


            BIstatement.init_quality_table();
            BIstatement.init_scale_table();
            BIstatement.init_aging_table();
            BIstatement.init_cost_table();
            BIstatement.init_KPI_table();
        },
        // 品质列表初始化
        init_quality_table: function () {
            $("#quality_table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                colNames: [
                    "城市",
                    "当日完成单量",
                    "当日评价单量",
                    "当日评价率",
                    "当日5星好评率",
                    "当日满意率",
                    "累计完成单量",
                    "累计评价单量",
                    "累计评价率",
                    "累计5星好评率",
                    "累计满意率",
                ],
                colModel: [
                    // 城市
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 当日完成单量
                    {
                        name: "dayCompleted",
                        index: "dayCompleted",
                        editable: false,
                        width: 140,
                        sortable: true
                    },
                    // 当日评价单量
                    {
                        name: "dayEvaluationQuantity",
                        index: "dayEvaluationQuantity",
                        editable: false,
                        width: 140,
                        sorttype: "string"
                    },
                    // 当日评价率
                    {
                        name: "dayEvaluationRate",
                        index: "dayEvaluationRate",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 当日5星好评率
                    {
                        name: "dayFiveStarRating",
                        index: "dayFiveStarRating",
                        editable: false,
                        width: 140,
                        sorttype: "string",
                    },
                    // 当日满意率
                    {
                        name: "daySatisfactionRate",
                        index: "daySatisfactionRate",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 累计完成单量
                    {
                        name: "totalCompleted",
                        index: "totalCompleted",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 累计评价单量
                    {
                        name: "totalEvaluationQuantity",
                        index: "totalEvaluationQuantity",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 累计评价率
                    {
                        name: "totalEvaluationRate",
                        index: "totalEvaluationRate",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 累计5星好评率
                    {
                        name: "totalFiveStarRating",
                        index: "totalFiveStarRating",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 累计满意率
                    {
                        name: "totalSatisfactionRate",
                        index: "totalSatisfactionRate",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                ],
                pager: "#quality_table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
            });

            $("#quality_table").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [
                    {startColumnName: 'cityName', numberOfColumns: 1, titleText: '名称'},
                    {startColumnName: 'dayCompleted', numberOfColumns: 5, titleText: '当日品质'},
                    {startColumnName: 'totalCompleted', numberOfColumns: 5, titleText: '累计品质'},
                ]
            });

            $("#quality_table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });


            $("#quality_table").setGridWidth($(".quality_table").width());
            $(window).bind("resize", function () {
                var b = $(".quality_table").width();
                $("#quality_table").setGridWidth(b);
            });
        },
        // 规模列表初始化
        init_scale_table: function () {
            $("#scale_table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                colNames: [
                    "城市",
                    "创建单量",
                    "单量环比",
                    "紧急占比",
                    "报修单量",
                    "完成单量",
                    "完成环比",
                    "结算单量",
                    "结算积压单量",
                    "师傅人数",
                    "接单人数",
                    "接单人效",
                    "完成人效",
                    "48h遗留单量",
                ],
                colModel: [
                    // 城市
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 创建单量
                    {
                        name: "createOrderQuantity",
                        index: "createOrderQuantity",
                        editable: false,
                        width: 120,
                        sortable: true
                    },
                    // 单量环比
                    {
                        name: "quantityRingRatio",
                        index: "quantityRingRatio",
                        editable: false,
                        width: 120,
                        sorttype: "string"
                    },
                    // 紧急占比
                    {
                        name: "emergencyRatio",
                        index: "emergencyRatio",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 报修单量
                    {
                        name: "repairAmount",
                        index: "repairAmount",
                        editable: false,
                        width: 120,
                        sorttype: "string",
                    },
                    // 完成单量
                    {
                        name: "completeSingleQuantity",
                        index: "completeSingleQuantity",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 完成环比
                    {
                        name: "completeRingRatio",
                        index: "completeRingRatio",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 结算单量
                    {
                        name: "billingAmount",
                        index: "billingAmount",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 结算积压单量
                    {
                        name: "settlementBacklogAmount",
                        index: "settlementBacklogAmount",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 师傅人数
                    {
                        name: "numberMasters",
                        index: "numberMasters",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 接单人数
                    {
                        name: "numberOrders",
                        index: "numberOrders",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 接单人效
                    {
                        name: "orderEffect",
                        index: "orderEffect",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 完成人效
                    {
                        name: "completeHumanEffect",
                        index: "completeHumanEffect",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 48h遗留单量
                    {
                        name: "fortyEightHourLegacyAmount",
                        index: "fortyEightHourLegacyAmount",
                        editable: false,
                        width: 120,
                        sortable: false
                    },

                ],
                pager: "#scale_table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
            });
            $("#scale_table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });


            $("#scale_table").setGridWidth($(".scale_table").width());
            $(window).bind("resize", function () {
                var b = $(".scale_table").width();
                $("#scale_table").setGridWidth(b);
            });
        },
        // 时效列表初始化
        init_aging_table: function () {
            $("#aging_table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                colNames: [
                    "城市",
                    "单量",
                    "1h联系及时率",
                    "1h联系及时率环比",
                    "紧急工单",
                    "紧急3h上门率",
                    "紧急3h上门率环比",
                    "一次上门及时率",
                    "一次上门及时率环比",
                    "上门及时率",
                    "上门及时率环比",
                    "上门单量",
                    "4h反馈率",
                    "4h环比",
                    "完成单量",
                    "24h完成率",
                    "24h完成率环比",
                    "48h完成率",
                    "48h完成率环比",
                    "完成率",
                ],
                colModel: [
                    // 城市
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 单量
                    {
                        name: "orderNum",
                        index: "orderNum",
                        editable: false,
                        width: 80,
                        sortable: true
                    },
                    // 1h联系及时率
                    {
                        name: "contactOntime",
                        index: "contactOntime",
                        editable: false,
                        width: 120,
                        sorttype: "string"
                    },
                    // 1h联系及时率环比
                    {
                        name: "contactOntimeRingRatio",
                        index: "contactOntimeRingRatio",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 紧急工单
                    {
                        name: "exigencyNum",
                        index: "exigencyNum",
                        editable: false,
                        width: 80,
                        sorttype: "string",
                    },
                    // 紧急3h上门率
                    {
                        name: "visitOntimeExigency",
                        index: "visitOntimeExigency",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 紧急3h上门率环比
                    {
                        name: "visitOntimeExigencyRingRatio",
                        index: "visitOntimeExigencyRingRatio",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 一次上门及时率
                    {
                        name: "visitOntimeFirst",
                        index: "visitOntimeFirst",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 一次上门及时率环比
                    {
                        name: "visitOntimeFirstRingRatio",
                        index: "visitOntimeFirstRingRatio",
                        editable: false,
                        width: 140,
                        sortable: false
                    },
                    // 上门及时率
                    {
                        name: "visitOntimeChange",
                        index: "visitOntimeChange",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    // 上门及时率环比
                    {
                        name: "visitOntimeChangeRingRatio",
                        index: "visitOntimeChangeRingRatio",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 上门单量
                    {
                        name: "visitNum",
                        index: "visitNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 4h反馈率
                    {
                        name: "feedbackOntime",
                        index: "feedbackOntime",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 4h反馈率环比
                    {
                        name: "feedbackOntimeRingRatio",
                        index: "feedbackOntimeRingRatio",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    // 完成单量
                    {
                        name: "finishNum",
                        index: "finishNum",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 24完成率
                    {
                        name: "twentyFourFinishrate",
                        index: "twentyFourFinishrate",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 24h完成率环比
                    {
                        name: "twentyFourFinishrateRingRatio",
                        index: "twentyFourFinishrateRingRatio",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 48h完成率
                    {
                        name: "fortyEightFinishrate",
                        index: "fortyEightFinishrate",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 48h完成率环比
                    {
                        name: "fortyEightFinishrateRingRatio",
                        index: "fortyEightFinishrateRingRatio",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    // 完成率
                    {
                        name: "finishRate",
                        index: "finishRate",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                ],
                pager: "#aging_table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
            });

            $("#aging_table").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [
                    {startColumnName: 'cityName', numberOfColumns: 1, titleText: '名称'},
                    {startColumnName: 'orderNum', numberOfColumns: 3, titleText: '联系时效'},
                    {startColumnName: 'exigencyNum', numberOfColumns: 3, titleText: '紧急3h时效'},
                    {startColumnName: 'visitOntimeFirst', numberOfColumns: 4, titleText: '上门时效'},
                    {startColumnName: 'visitNum', numberOfColumns: 3, titleText: '4h反馈时效'},
                    {startColumnName: 'finishNum', numberOfColumns: 6, titleText: '完成时效'},
                ]
            });

            $("#aging_table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });


            $("#aging_table").setGridWidth($(".aging_table").width());
            $(window).bind("resize", function () {
                var b = $(".aging_table").width();
                $("#aging_table").setGridWidth(b);
            });
        },
        // 成本列表初始化
        init_cost_table: function () {
            $("#cost_table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                colNames: [
                    "城市",
                    "总单均",
                    "综合单均",
                    "电器单均",
                    "综合电器单均",
                ],
                colModel: [
                    // 城市
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 总单均
                    {
                        name: "totalAverage",
                        index: "totalAverage",
                        editable: false,
                        width: 200,
                        sortable: true
                    },
                    // 综合单均
                    {
                        name: "synthesizeAverage",
                        index: "synthesizeAverage",
                        editable: false,
                        width: 200,
                        sorttype: "string"
                    },
                    // 电器单均
                    {
                        name: "electricalAverage",
                        index: "electricalAverage",
                        editable: false,
                        width: 220,
                        sortable: false
                    },
                    // 综合电器单均
                    {
                        name: "mixtureAverage",
                        index: "mixtureAverage",
                        editable: false,
                        width: 220,
                        sorttype: "string",
                    },
                ],
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
            });
            $("#cost_table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });


            $("#cost_table").setGridWidth($(".cost_table").width());
            $(window).bind("resize", function () {
                var b = $(".cost_table").width();
                $("#cost_table").setGridWidth(b);
            });
        },
        // KPI列表初始化
        init_KPI_table: function () {
            $("#KPI_table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                colNames: [
                    "城市",
                    "1小时联系及时率",
                    "普通上门及时率",
                    "紧急3小时上门及时率",
                    "48小时完成率",
                    "评价率",
                    "5星好评率",
                    "90天多次维修率",
                    "单均成本",
                    "人效超标率",
                ],
                colModel: [
                    // 城市
                    {
                        name: "cityName",
                        index: "cityName",
                        editable: false,
                        width: 80,
                        sortable: false
                    },
                    // 1小时联系及时率
                    {
                        name: "oneHourContactRate",
                        index: "oneHourContactRate ",
                        editable: false,
                        width: 140,
                        sortable: false,
                        // formatter:function (cellvalue, options, rowObject) {
                        // 	return rowObject.oneHourContactRate*100+'%'
                        // }
                    },
                    // 普通上门及时率
                    {
                        name: "ordinaryVisitTimeRate",
                        index: "ordinaryVisitTimeRate",
                        editable: false,
                        width: 140,
                        sorttype: "string",
                        // formatter:function (cellvalue, options, rowObject) {
                        // 	return rowObject.ordinaryVisitTimeRate*100+'%'
                        // }
                    },
                    // 紧急3小时上门及时率
                    {
                        name: "emergencyThreeHourHomeRate",
                        index: "emergencyThreeHourHomeRate",
                        editable: false,
                        width: 140,
                        sorttype: "string",
                        // formatter:function (cellvalue, options, rowObject) {
                        // 	return rowObject.emergencyThreeHourHomeRate*100+'%'
                        // }
                    },
                    // 48小时完成率
                    {
                        name: "fortyEightHourCompletionRate",
                        index: "fortyEightHourCompletionRate",
                        editable: false,
                        width: 120,
                        sorttype: "string",
                        // formatter:function (cellvalue, options, rowObject) {
                        // 	return rowObject.fortyEightHourCompletionRate*100+'%'
                        // }
                    },
                    // 评价率
                    {
                        name: "evaluationOfRate",
                        index: "evaluationOfRate",
                        editable: false,
                        width: 80,
                        sorttype: "string",
                        // formatter:function (cellvalue, options, rowObject) {
                        // 	return rowObject.evaluationOfRate*100+'%'
                        // }
                    },
                    // 5星好评率
                    {
                        name: "fiveStarRate",
                        index: "fiveStarRate",
                        editable: false,
                        width: 100,
                        sorttype: "string",
                        // formatter:function (cellvalue, options, rowObject) {
                        // 	return rowObject.fiveStarRate*100+'%'
                        // }
                    },
                    // 90天多次维修率
                    {
                        name: "ninetyDaysMultipleMaintenanceRate",
                        index: "ninetyDaysMultipleMaintenanceRate",
                        editable: false,
                        width: 140,
                        sorttype: "string",
                        // formatter:function (cellvalue, options, rowObject) {
                        // 	return rowObject.ninetyDaysMultipleMaintenanceRate*100+'%'
                        // }
                    },
                    // 单均成本
                    {
                        name: "singleCost",
                        index: "singleCost",
                        editable: false,
                        width: 100,
                        sorttype: "string"
                    },
                    // 人效超标率
                    {
                        name: "humanEffectRate",
                        index: "humanEffectRate",
                        editable: false,
                        width: 120,
                        sorttype: "string",
                        // formatter:function (cellvalue, options, rowObject) {
                        // 	return rowObject.humanEffectRate*100+'%'
                        // }
                    },
                ],
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
            });
            $("#KPI_table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $("#KPI_table").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [
                    {startColumnName: 'cityName', numberOfColumns: 1, titleText: '名称'},
                    {startColumnName: 'oneHourContactRate', numberOfColumns: 4, titleText: '时效'},
                    {startColumnName: 'evaluationOfRate', numberOfColumns: 3, titleText: '品质'},
                    {startColumnName: 'singleCost', numberOfColumns: 1, titleText: '成本'},
                    {startColumnName: 'humanEffectRate', numberOfColumns: 1, titleText: '规模'},
                ]
            });


            $("#KPI_table").setGridWidth($(".KPI_table").width());
            $(window).bind("resize", function () {
                var b = $(".KPI_table").width();
                $("#KPI_table").setGridWidth(b);
            });
        },
        // 获取数据
        getServerData: function (postData, tableName) {
            // pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.findReportList, postData, function (result) {
                if (result.code == 200) {
                    gridData = result.data;
                    var gridJson = {
                        rows: gridData,
                    };
                    tableName[0].addJSONData(gridJson);
                } else if (result.code == 501) {
                    gridData = {};
                    var gridJson = {
                        rows: gridData,
                    };
                    tableName[0].addJSONData(gridJson);

                    utils.showMessage(result.message);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 获取当前用户所负责城市
        getQueryUserCity: function () {
            win.utils.ajaxPost(win.utils.services.queryUserCity, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                userId: win.utils.getCookie('user_id')
            }, function (result) {
                if (result.result_code == 200) {
                    var cityOptionStr = '<option value="">请选择城市</option>';
                    result.sysUserCityVOS.forEach(function (i, k) {
                        cityOptionStr += '<option value="' + i.cityId + '">' + i.addressName + '</option>';
                    });
                    $('#search_city').html(cityOptionStr);
                }
            })
        },
        // 查询
        queryRecord: function () {
            getStatementData.cityId = $('#search_city').val();
            getStatementData.startDate = $('#startTime').val().split('-').join('');
            getStatementData.endDate = $('#endTime').val().split('-').join('');

            var buttonArr = $('.toolbar button');
            var tableName;
            for (let i = 0; i < buttonArr.length; i++) {
                if ($(buttonArr[i]).hasClass('active')) {
                    getStatementData.type = i + 1;
                    tableName = $(buttonArr[i]).attr('data-name');
                }
            }

            BIstatement.getServerData(getStatementData, $('#' + tableName + ''));
        },
        // 刷新
        refersh: function () {
            $('#search_city').val('');
            $('#startTime').val('');

            getStatementData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                startDate: null,
                endDate: null,
                cityId: null,
                type: 1
            };
            BIstatement.getServerData(getStatementData, $('#order_type_table'));
        },
        // 获取当前日期
        getTodayDate: function () {
            var date = new Date();
            var year = date.getFullYear().toString();
            var month = (+date.getMonth() + 1 >= 10 ? +date.getMonth() + 1 : '0' + (+date.getMonth() + 1)).toString();
            var day = (date.getDate() - 1) >= 10 ? date.getDate() - 1 : '0' + (date.getDate() - 1).toString();

            return year + month + day;
        },
    };
    win.BIstatement = BIstatement;
})(window, jQuery);
