/**
 * Created by song on 2019-04-17
 */
(function (win, $, BMap) {
    /**
     * 地图相关开始
     */
    var map = new BMap.Map("allmap");
    var point = new BMap.Point(116.404, 39.915);  // 创建点坐标
    map.centerAndZoom(point, 15);                 // 初始化地图，设置中心点坐标和地图级别
    map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    map.addControl(new BMap.NavigationControl());
    map.addControl(new BMap.ScaleControl());
    map.addControl(new BMap.OverviewMapControl());
    map.addControl(new BMap.MapTypeControl());
    // map.setCurrentCity("北京"); // 仅当设置城市信息时，MapTypeControl的切换功能才能可用


    var data_info = [];
    var opts = {
        width: 250,     // 信息窗口宽度
        height: 80,     // 信息窗口高度
        title: "位置", // 信息窗口标题
        enableMessage: true//设置允许信息窗发送短息
    };

    function loopMarkers(markers) {
        for (var i = 0; i < markers.length; i++) {
            var marker = new BMap.Marker(new BMap.Point(markers[i].longitude, markers[i].latitude));  // 创建标注
            var content = markers[i].siteDetails;
            map.addOverlay(marker);               // 将标注添加到地图中
            addClickHandler(content, marker);
        }
    }

    // 用经纬度设置地图中心点
    function theLocation(point) {
        map.clearOverlays();
        var new_point = new BMap.Point(point[0].longitude, point[0].latitude);
        map.panTo(new_point);

        loopMarkers(point);
    }

    function addClickHandler(content, marker) {
        marker.addEventListener("click", function (e) {
                openInfo(content, e)
            }
        );
    }

    function openInfo(content, e) {
        var p = e.target;
        var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
        var infoWindow = new BMap.InfoWindow(content, opts);  // 创建信息窗口对象
        map.openInfoWindow(infoWindow, point); //开启信息窗口
    }

    function remove() {
        var overlays = map.getOverlays();
        for (var i = 0; i < overlays.length; i++) {
            // if(overlays[i].getTitle()==name){
            map.removeOverlay(overlays[i]);
            // }
        }
    }

    /**
     * 地图相关结束
     */

        //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();

        $('#txtLessonName').val('');
        $('#txtLessonDate').val('');
        $('#txtLessonTeacher').val('');
        $('#txtLessonUrl').val('');
    }

    var repairman = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            // 当前位置权限
            utils.isUserMenuIndexRight('currentLocation', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });

            // 获取维修员数据
            repairman.getRepairmanData();

            $('#btnQuery').on('click', function () {
                obj.refreshServerData();
            });
            $('#search').click(obj.getWorkerInfo);
        },
        initList: function () {
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                autowidth: false,
                autoScroll: true,
                shrinkToFit: false,
                rownumbers: true,
                rownumWidth: 60,
                colNames: ["id", "维修员", "时间", "位置信息", "工单编号"],
                colModel: [
                    // id
                    {
                        name: "blockId",
                        index: "blockId",
                        editable: false,
                        width: 40,
                        sortable: false,
                        align: 'center',
                        hidden: true
                    },
                    // 维修员
                    {
                        name: "createUser",
                        index: "createUser",
                        editable: false,
                        width: 100,
                        sortable: false,
                        align: 'center'
                    },
                    // 时间
                    {
                        name: "createTime",
                        index: "createTime",
                        editable: false,
                        width: 180,
                        sortable: false,
                        align: 'center',
                        formatter: function (cellvalue, options, rowObject) {
                            if (rowObject.createTime) {
                                return repairman.timeStampFormat(rowObject.createTime)
                            }
                        }
                    },
                    // 位置信息
                    {
                        name: "siteDetails",
                        index: "siteDetails",
                        editable: false,
                        width: 260,
                        sortable: false,
                        align: 'center',
                    },
                    // 工单编号
                    {
                        name: "orderId",
                        index: "orderId",
                        editable: false,
                        width: 200,
                        sortable: false,
                        align: 'center',
                    }
                ],
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                multiselect: false,
            });

            var b = $(".jqGrid_wrapper").width();
            $("#table").setGridWidth(b);
            $(win).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
            });
        },
        refreshServerData: function () {
            repairman.getServerData(1);
        },
        getWorkerInfo: function () {
            var repairmanName = $('#repairman').val();
            var date = $('#timeChoose').val();
            console.log(repairmanName, date);
            if (!repairmanName) {
                utils.showMessage('请选择维修员！');
                return;
            }
            if (!date) {
                utils.showMessage('请选择日期时间！');
                return;
            }

            var postData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                startTime: repairman.dateFormat(date)[0],
                endTime: repairman.dateFormat(date)[1],
                workerId: repairmanName
            }
            repairman.getServerData(1, postData);
        },
        getServerData: function (pageIndex, postData) {
            $('#table').jqGrid('clearGridData');
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.getWorkerLocation, postData, function (result) {
                if (result.result_code == 200) {
                    if (result.workerLocationDetails.length) {
                        data_info = result.workerLocationDetails;
                        theLocation(data_info)

                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            rows: data_info,
                        };

                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        remove();
                        utils.showMessage('该师傅没有上门签到位置信息');
                    }
                } else {
                    remove();
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 获取维修员数据
        getRepairmanData: function () {
            win.utils.ajaxPost(win.utils.services.getWorker, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.workerInfoVOS.length) {
                        var optionStr = '<option value="" selected>请选择维修工程师</option>';
                        result.workerInfoVOS.forEach(function (i, k) {
                            optionStr += '<option value="' + i.workerId + '">' + i.workerName + '/' + i.workerPhone + '</option>'
                        });
                        $('#repairman').html(optionStr);
                        layui.use('form', function () {
                            var form = layui.form;
                            form.render('select');
                        });
                    } else {
                        utils.showMessage('暂无维修工程师数据！');
                    }
                } else {
                    utils.showMessage('获取维修工程师失败，请重试！');
                }
            });
        },
        // 时间戳转时间格式
        timeStampFormat: function (timestamp) {
            var date = new Date(timestamp);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
            h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y + M + D + h + m + s;
        },
        // 日期格式处理
        dateFormat: function (dataStr) {
            if (dataStr) {
                var startCreateTime = dataStr.split(' - ')[0];
                var endCreateTime = dataStr.split(' - ')[1];
                startCreateTime = startCreateTime + ' 00:00:00';
                endCreateTime = endCreateTime + ' 23:59:59';
                return [startCreateTime, endCreateTime];
            } else {
                return null
            }
        },
        datePartFormat: function (date) {
            return date.split('-').join('-');
        }
    };
    win.repairman = repairman;
})(window, jQuery, BMap);