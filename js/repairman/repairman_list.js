/**
 * Created by song on 2019-04-17.
 * modified by hanlu
 */
(function(win, $, qiniu) {
	//延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
	var deferLoadData = 0;
	var orderByString = '';
	//分页-当前页---当前条数
	var nowPage=1;
	// 维修员列表当前选择id
	var selectedRowIndex;
	var chooseArea=[]; //提交时添加的区域
	var tableArr=[];
	var villageIndex=0;//显示选择结果区域——小区字符串插入li的index

	var getRepairmanListData={
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        name:"",
        status:null,
		isQueryFinishNum:1,
        position: nowPage,
        rows: 10
	};
	var data={
		user_id: win.utils.getCookie('user_id'),
		session: win.utils.getCookie('session'),
		workerId:null,
		workerName:null,
		regionId: null,
		skillKind:null,
		districtId:null,
		districtCbdId:null,
		blockId:null,
		workerStatus:null,
		isInservice:null,
		position: nowPage,
		rows: 10,
		isQueryFinishNum:1,
		skillsLabel:null
	};

	// 维修员原数据
	var oldSkillKind,oldCity,oldPhone,oldurgencyPhone,oldurgencyName,oldCardId='',oldBankInfo=[];

	var is_search=false; //搜索标识

    var flagInput=false;

	var repairman = {
		//初始化
		init: function()  {
			var obj = this;
			if(utils.isLogin() == false) {
				utils.loginDomain();
				return;
			}
			//判断是否有维修员权限，此时为维修员管理的维修员列表权限
			utils.isUserMenuIndexRight('repairman_list', function(result) {
				if(result == 1) {
					//initialize...
					win.utils.clearSpaces();
					obj.initList();
				} else {
					self.location = '../../../web/error/access_refuse.html';
					return;
				}
			});

			// 搜索条件处理
			var postData = {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				userId: win.utils.getCookie('user_id')
			};
			repairman.getCityData(postData, function (result) {
				var optionStr='<option value="">请选择城市</option>';
				result.sysUserCityVOS.forEach(function (i,k) {
					optionStr+='<option value="'+i.cityId+'">'+i.addressName+'</option>'
				});
				$('#search_city').html(optionStr);
				$('#search_city').find('option').eq(0).attr("selected",true);
			});
			var cityId, cityName;
			$('#search_city').off('change').on('change', function (e) {
				cityId=e.target.value;
				cityName=$('#search_city').find('option:selected').text();

				$('.search_zoning').hide();
				$('.search_tradingArea').hide();
				$('.search_village').hide();

				$('#search_zoning').val('');
				$('#search_tradingArea').val('');
				$('#search_village').val('');

				if(cityId){
					postData.levelFlag=2;
					postData.cityId=cityId;
					repairman.getAreaData(postData, function (result) {
						var optionStr='<option value="">请选择区域</option>';
						result.areaVOList.forEach(function (i,k) {
							optionStr+='<option value="'+i.id+'">'+i.addressName+'</option>'
						});
						$('#search_zoning').html(optionStr);
						layui.use('form',function () {
							var form = layui.form;
							form.render('select');
						})
						$('#search_zoning').find('option').eq(0).attr("selected",true);
						$('.search_zoning').show();
					});
				}
			});

			// 搜索区域
			$('#search_zoning').off('click').on('click', function () {
				if(!cityId){
					utils.showMessage('请选择城市！');
				}
			});
			var zoningId,zoningName;
			$('#search_zoning').off('change').on('change',function (e) {
				zoningId=e.target.value;
				zoningName=$('#search_zoning').find('option').eq(e.target.selectedIndex).text();

				$('.search_tradingArea').hide();
				$('.search_village').hide();

				$('#search_tradingArea').val('');
				$('#search_village').val('');

				if(zoningId){
					postData.levelFlag=3;
					postData.cityId=null;
					postData.districtId=zoningId;
					repairman.getAreaData(postData, function (result) {
						var optionStr='<option value="">请选择商圈</option>';
						result.areaVOList.forEach(function (i,k) {
							optionStr+='<option value="'+i.id+'">'+i.addressName+'</option>'
						});
						$('#search_tradingArea').html(optionStr);
						layui.use('form',function () {
							var form = layui.form;
							form.render('select');
						})
						$('#search_tradingArea').find('option').eq(0).attr("selected",true);

						$('.search_tradingArea').show();
					});
				}
			});

			// 搜索商圈
			$('#search_tradingArea').off('click').on('click', function () {
				if(!zoningId){
					utils.showMessage('请选择区域！');
				}
			});
			var tradingId,tradingName;
			layui.use('form',function(){
				var form = layui.form;
				form.on('select(search_tradingArea)', function(data){
					if(data.value){
						tradingId=data.value;
						tradingName=$('#search_tradingArea').find('option:selected').text();

						$('.search_village').hide();

						$('#search_village').val('');

						if(tradingId){
							postData.levelFlag=4;
							postData.cityId=null;
							postData.districtId=null;
							postData.businessDistrictId=tradingId;
							repairman.getAreaData(postData, function (result) {
								var optionStr='<option value="">请选择小区</option>';
								result.areaVOList.forEach(function (i,k) {
									optionStr+='<option value="'+i.id+'">'+i.addressName+'</option>'
								});
								$('#search_village').html(optionStr);
								layui.use('form',function () {
									var form = layui.form;
									form.render('select');
								})
								$('#search_village').find('option').eq(0).attr("selected",true);

								$('.search_village').show();
							});
						}
					}
					else{
						$('.search_village').hide();
					}
				});
			});
			// 搜索小区
			var villageId,villageName;
			$('#search_village').off('change').on('change',function (e) {
				villageId=e.target.value;
				villageName=$('#search_village').find('option').eq(e.target.selectedIndex).text();
			});

			// 搜索
			$('#search').on('click',function(){
				$('.detail').hide();
				is_search=true;
				nowPage=1;
				repairman.searchFn();
			});
			//刷新
			$('#btnRefresh').on('click', function() {
				is_search=false;
				nowPage=1;
				obj.refreshServerData();
			});
			//新增
			$('#btnAdd').on('click',function(){
				repairman.addConInit();
				repairman.addWorkOrder(1)
			});
			// 维修员请假销假
			$('#table').off('click').on('click','#changeReceiptStatus',function (){
                var id=$(this).attr('data-id');
                var workerStatus=$(this).attr('data-status');
                var rowData=$('#table').jqGrid('getRowData',id);

                var workerInfo={
                	"id":id,
					"workerName":rowData.workerName,
					"workerStatus":workerStatus,
					"workerPhone":rowData.workerPhone
				};
				repairman.changeReceiptStatus(workerInfo);
			});
            //修改维修员
            $('#table').on('click', '#btnUpdate', function(){
				var id=$(this).attr('data-id');
                repairman.addConInit();
                repairman.addWorkOrder(2, id);
            });
			// 图片上传
			$('#btnCardUpload').click(obj.uploadCardImage);
			$('#btnCardDelete').click(obj.deleteCardImage);


			// 维修员列表查看服务区域
			$('#table').on('click','a',function () {
				var id=$(this).attr('data-id');

				var href=$('.nav .J_menuItem[data-id=21 ]',parent.document).attr('href');

				var repairmanInfo={
					workerId:id,
					type:1
				};
				win.localStorage.setItem("repairmanInfo",JSON.stringify(repairmanInfo));
				parent.$(window.parent.document).find('.nav .J_menuItem[data-id=21]').click();
				parent.index.getParamsToChild(repairmanInfo);

			});


			$('#areasCon').find('.form-group').hide();

			// 添加银行卡
            $('.add-card-btn').on('click',function(){
                var num=$('.add-card-item').length;

                var cardDomStr='<div class="add-card-item" data-id="'+num+'">\n' +
                    '\t\t\t\t\t\t<div class="card-tit">\n' +
                    '\t\t\t\t\t\t\t<span>银行卡-'+(num+1)+'</span>\n' +
                    '\t\t\t\t\t\t\t<span></span>\n' +
                    '\t\t\t\t\t\t\t<button type="button" class="btn deleteCardBtn" data-index="'+num+'">删除</button>\n' +
                    '\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t<div class="card-info">\n' +
                    '\t\t\t\t\t\t\t<div class="form-horizontal">\n' +
                    '\t\t\t\t\t\t\t\t<!--银行卡号-->\n' +
                    '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                    '\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>银行卡号</label>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="bankCard" type="number" class="bankCard form-control" placeholder="请输入银行卡号" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label" style="text-align: center; color: rgba(107,111,129,1); ">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"></label>\n' +
                    '\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6 bankcardtip">（必须是维修员本人的银行卡号）</div>\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t<!--持卡人姓名-->\n' +
                    '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                    '\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>持卡人姓名</label>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="cardholder" type="text" class="cardholder form-control" placeholder="请输入持卡人姓名" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-3 control-label checkcardholderCon" style="text-align: left; color: red; display: none;">姓名不符合规范</div>\n' +
                    '\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t<!--持卡人身份证号-->\n' +
                    '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                    '\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>持卡人身份证号</label>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="cardId" type="text" class="cardId form-control" placeholder="请输入持卡人身份证号" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-3 control-label checkBankCardIdCon" style="text-align: left; color: red; display: none;">开户城市不符合规范</div>\n' +
                    '\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t<!--开户银行-->\n' +
                    '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                    '\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>开户银行</label>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-3">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="bank" type="text" class="bank form-control" placeholder="请输入开户银行名称" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-3">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="bankBranch" type="text" class="bankBranch form-control" placeholder="请输入开户分行" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-3">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="bankfiliale" type="text" class="bankfiliale form-control" placeholder="请输入开户支行" autocomplete="off">\n' +
					'\t\t\t\t\t\t\t\t\t</div>\n' +
					'\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label checkbankCon" style="text-align: left; color: red; display: none;"><label class="col-sm-3 control-label"></label><span>开户银行名称不符合规范</span></div>\n' +
					'\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label checkbankBranchCon" style="text-align: left; color: red; display: none;"><label class="col-sm-3 control-label"></label><span>开户分行不符合规范</span></div>\n' +
					'\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label checkbankfilialeCon" style="text-align: left; color: red; display: none;"><label class="col-sm-3 control-label"></label><span>开户支行不符合规范</span></div>\n' +
                    '\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t<!--开户城市-->\n' +
                    '\t\t\t\t\t\t\t\t<div class="form-group city">\n' +
                    '\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>开户城市</label>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="city" type="text" class="city form-control" placeholder="开户城市" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
					'\t\t\t\t\t\t\t\t\t<div class="col-sm-3 control-label checkcityCon" style="text-align: left; color: red; display: none;">开户城市不符合规范</div>\n' +
                    '\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t</div>';
                $('.add-card-box').append(cardDomStr);

                // 第一张银行卡身份证号不可输入，且回显
				$("input[name='cardId']").eq(0).prop('readonly',true);
				// 银行卡提示文字
				$('.bankcardtip').text('(必须非维修员本人的银行卡)').eq(0).text('(必须为维修员本人的银行卡)');
				if($('#cardNum').val()){
					$("input[name='cardId']").eq(0).val($('#cardNum').val());
				}
            });

            // 删除银行卡
            $('.add-card-box').on('click','.deleteCardBtn',function(){
                var index = $(this).attr('data-index');

				$('.add-card-item').eq(index).remove(); //删除节点

				// 银行卡节点标题号、index重新赋值
				var cardInfoNodes=$('.add-card-item');
				for (var i=0;i<cardInfoNodes.length;i++){
					$(cardInfoNodes[i]).find('button').attr('data-index',i);
					var titleText=$(cardInfoNodes[i]).find('.card-tit').find('span').eq(0).text();
					$(cardInfoNodes[i]).find('.card-tit').find('span').eq(0).text(titleText.split('-')[0]+'-'+(i+1));
				}


                // 第一张银行卡身份证号不可输入，且回显
                $("input[name='cardId']").eq(0).prop('readonly',true);
                // 银行卡提示文字
                $('.bankcardtip').text('(必须非维修员本人的银行卡)').eq(0).text('(必须为维修员本人的银行卡)');
                if($('#cardNum').val()){
                    $("input[name='cardId']").eq(0).val($('#cardNum').val());
                }
            });

            /*****************校验*************************/
			// 校验手机号
			var str;
			$('#phone').on('input',function(){
				var phone=$('#phone').val();
				if(phone.length>=11){
					if(phone!==oldPhone){
						str = repairman.checkMobile(phone);
						if(!str){
							$('.checkPhoneCon').text('手机号不符合规范，请重新输入').show();
						}else{
							$('.checkPhoneCon').text('').hide();
							win.utils.ajaxPost(win.utils.services.verifyPhone,{
								user_id: win.utils.getCookie('user_id'),
								session: win.utils.getCookie('session'),
								phoneNum:phone
							},function (result) {
								if(!result.isExist){
									$('.checkPhoneCon').text('手机号不符合规范，请重新输入').hide();
								}else{
									$('.checkPhoneCon').text('手机号已存在，请重新输入').show();
								}
							})
						}
					}
					else{
						$('.checkPhoneCon').text('').hide();
					}
				}
			});
			$('#phone').off('blur').on('blur',function(){
				var phone=$('#phone').val();
				if(phone!==oldPhone){
					str = repairman.checkMobile(phone);
					if(!str){
						$('.checkPhoneCon').text('手机号不符合规范，请重新输入').show();
					}else{
						$('.checkPhoneCon').text('').hide();
						win.utils.ajaxPost(win.utils.services.verifyPhone,{
							user_id: win.utils.getCookie('user_id'),
							session: win.utils.getCookie('session'),
							phoneNum:phone
						},function (result) {
							if(!result.isExist){
								$('.checkPhoneCon').text('手机号不符合规范，请重新输入').hide();
							}else{
								$('.checkPhoneCon').text('手机号已存在，请重新输入').show();
							}
						})
					}
				}
				else{
					$('.checkPhoneCon').text('').hide();
				}
			});

			$('#urgencyPhone').on('input',function(){
				var urgencyPhone=$('#urgencyPhone').val();
				if(urgencyPhone.length>=11){
					if(urgencyPhone!==oldurgencyPhone){
						str = repairman.checkMobile(urgencyPhone);
						if(!str){
							$('.checkurgencyPhoneCon').text('手机号不符合规范，请重新输入').show();
						}else{
							$('.checkurgencyPhoneCon').text('').hide();
						}
					}
					else{
						$('.checkurgencyPhoneCon').text('').hide();
					}
				}
			});
			$('#urgencyPhone').off('blur').on('blur',function(){
				var urgencyPhone=$('#urgencyPhone').val();
				if(urgencyPhone!==oldurgencyPhone){
					str = repairman.checkMobile(urgencyPhone);
					if(!str){
						$('.checkurgencyPhoneCon').text('手机号不符合规范，请重新输入').show();
					}else{
						$('.checkurgencyPhoneCon').text('').hide();
					}
				}
				else{
					$('.checkurgencyPhoneCon').text('').hide();
				}
			});

			// 姓名校验
			var nameStr, urgencyName, cardholder, bank, bankBranch, bankfiliale, city;
			$('#name').on('input',function () {
				var name=$(this).val();
				if(name){
					nameStr=repairman.checkName(name);
				}
				if(!nameStr){
					$('.checkNameCon').text('姓名不符合规范，请重新输入').show();
					// $(this).val('');
				}else{
					$('.checkNameCon').text('').hide();
				}
			});
			$('#urgencyName').on('input',function () {
				var name=$(this).val();
				urgencyName=repairman.checkName(name);
				if(!urgencyName){
					$('.checkurgencyNameCon').text('姓名不符合规范，请重新输入').show();
				}else{
					$('.checkurgencyNameCon').text('').hide();
				}
			});
			// 银行卡持卡人姓名
			$(document).on('input','.cardholder',function () {
				var parentIndex=$(this).parents('.add-card-item').index();

				var name=$(this).val();
				if(name){
					cardholder=repairman.checkName(name);
				}
				if(!cardholder){
					$('.checkcardholderCon').eq(parentIndex).text('姓名不符合规范，请重新输入').show();
					// $(this).val('');
				}else{
					if(name.length>10){
						$(this).val(name.slice(0,10));
					}
					$('.checkcardholderCon').eq(parentIndex).text('').hide();
				}
			});
			// 银行卡号长度限制
			$(document).on('input','.bankCard',function () {
				var bankCardNum=$(this).val();
				console.log(bankCardNum)
				if(bankCardNum.length>20){
					$(this).val(bankCardNum.slice(0,20));
				}
			});
			// 银行卡号重复校验
			$('.add-card-box').off('blur').on('blur','.bankCard',function () {
				var parentIndex=$(this).parents('.add-card-item').index();

				var bankCardNum=$(this).val();
				// 银行卡号前端重复校验
				var bankCardNodes=$('.bankCard');
				for(var i=0;i<bankCardNodes.length;i++){
					if(i!=parentIndex){
						if($(bankCardNodes[i]).val()== bankCardNum){
							utils.showMessage('当前银行卡号与第'+(i+1)+'张银行卡号重复，请重新输入！');
							$(this).focus();
							return;
						}
					}
				}

				// 当没有原银行卡号或者原银行卡号不等于现银行卡号的时候校验银行卡号是否重复
				if(oldBankInfo && oldBankInfo.length){
					if(!oldBankInfo[parentIndex].bankCard || oldBankInfo[parentIndex].bankCard!=bankCardNum){
						win.utils.ajaxGet(win.utils.services.verifyBankId,{
							user_id: win.utils.getCookie('user_id'),
							session: win.utils.getCookie('session'),
							bankId:bankCardNum,
							workerId:0
						},function (result) {
							console.log(result);
							if(result.result_code==200){
								if(result.data.is_exist){
									utils.showMessage('银行卡已存在，请重新输入！');
									$(this).val('');
								}
							}
							else{
								utils.showMessage('银行卡校验失败！');
								$(this).val('');
							}
						})
					}
				}
			});
			// 开户银行名称
			$(document).on('input','.bank',function () {
				var parentIndex=$(this).parents('.add-card-item').index();

				var name=$(this).val();
				if(name){
					bank=repairman.checkName(name);
				}
				if(!bank){
					$('.checkbankCon').eq(parentIndex).find('span').text('开户银行名称不符合规范，请重新输入');
					$('.checkbankCon').eq(parentIndex).show();
				}else{
					$('.checkbankCon').eq(parentIndex).find('span').text('');
					$('.checkbankCon').eq(parentIndex).hide();
				}
			});
			// 开户分行名称
			$(document).on('input','.bankBranch',function () {
				var parentIndex=$(this).parents('.add-card-item').index();

				var name=$(this).val();
				if(name){
					bankBranch=repairman.checkName(name);
				}
				if(!bankBranch){
					$('.checkbankBranchCon').eq(parentIndex).find('span').text('开户分行名称不符合规范，请重新输入');
					$('.checkbankBranchCon').eq(parentIndex).show();
					// $(this).val('');
				}else{
					$('.checkbankBranchCon').eq(parentIndex).find('span').text('');
					$('.checkbankBranchCon').eq(parentIndex).hide();
				}
			});
			// 开户支行名称
			$(document).on('input','.bankfiliale',function () {
				var parentIndex=$(this).parents('.add-card-item').index();

				var name=$(this).val();
				if(name){
					bankfiliale=repairman.checkName(name);
				}
				if(!bankfiliale){
					$('.checkbankfilialeCon').eq(parentIndex).find('span').text('开户支行名称不符合规范，请重新输入');
					$('.checkbankfilialeCon').eq(parentIndex).show();
					// $(this).val('');
				}else{
					$('.checkbankfilialeCon').eq(parentIndex).find('span').text('');
					$('.checkbankfilialeCon').eq(parentIndex).hide();
				}
			});
			// 开户城市名称
			$(document).on('input','.city',function () {
				var parentIndex=$(this).parents('.add-card-item').index();

				var name=$(this).val();
				if(name){
					city=repairman.checkName(name);
				}
				if(!city){
					$('.checkcityCon').eq(parentIndex).text('开户城市不符合规范，请重新输入').show();
				}else{
					$('.checkcityCon').eq(parentIndex).text('').hide();
				}
			});
			// 持卡人身份证号
			$('.add-card-box').off('blur').on('blur','.cardId',function () {
				var parentIndex=$(this).parents('.add-card-item').index();

				var cardId=$(this).val();
				if(cardId){
					var cardId_flag=repairman.checkCardId(cardId);
				}
				if(!cardId_flag){
					if(parentIndex!=0 || (cardId.length!=18 && cardId.length!=15)){
						$('.checkBankCardIdCon').eq(parentIndex).text('身份证号不符合规范，请重新输入').show();
					}
				}else{
					$('.checkBankCardIdCon').eq(parentIndex).text('').hide();
					if(parentIndex!=0 && $('#cardNum').val()){
						if(cardId==$('#cardNum').val()){
							utils.showMessage('此银行卡必须是非维修员本人银行卡！');
							$(this).val('');
						}
					}
				}

				// 银行卡持卡人身份证号前端重复校验
				var cardIdNodes=$('.cardId');
				for(var i=0;i<cardIdNodes.length;i++){
					if(i!=parentIndex){
						if($(cardIdNodes[i]).val()== cardId){
							utils.showMessage('当前持卡人身份证号与第'+(i+1)+'张银行卡持卡人身份证号重复，请重新输入！');
							$(this).focus();
							return;
						}
					}
				}

				// 当没有持卡人原身份证号或者持卡人原身份证号不等于现身份证号的时候校验身份证号是否重复
				if(oldBankInfo && oldBankInfo.length){
					if(!oldBankInfo[parentIndex].cardId || oldBankInfo[parentIndex].cardId!=cardId){
						win.utils.ajaxGet(win.utils.services.verifyCardId,{
							user_id: win.utils.getCookie('user_id'),
							session: win.utils.getCookie('session'),
							cardId:cardId,
							workerId:0
						},function (result) {
							if(result.result_code==200){
								if(result.data.is_exist){
									utils.showMessage('持卡人身份证号已存在，请重新输入！');
									$(this).focus();
								}
							}
							else{
								utils.showMessage('持卡人身份证号校验失败！');
								$(this).focus();
							}
						})
					}
				}
			});

			// 师傅身份证号
			$('#cardNum').off('blur').on('blur',function(){
				var cardId=$(this).val();
				if(cardId){
					var cardId_flag=repairman.checkCardId(cardId);
				}
				if(!cardId_flag || (cardId.length!=18 && cardId.length!=15)){
					$('.checkcardIdCon').text('身份证号不符合规范，请重新输入').show();
				}else{
					$('.checkcardIdCon').text('').hide();
					// 第一张银行卡身份证号赋值
					if($("input[name='cardId']").eq(0)){
						$("input[name='cardId']").eq(0).val(cardId);
					}

					// 除第一张银行卡外，其他银行卡必须非本人银行卡校验
					var bankCardNodes=$("input[name='cardId']");
					if(bankCardNodes && bankCardNodes.length){
						var cardId=$('#cardNum').val();
						for (var i=0;i<bankCardNodes.length;i++){
							var bank_cardId=$(bankCardNodes[i]).val();
							if(i!=0 && bank_cardId==cardId){
								utils.showMessage('除第一张银行卡外，其他银行卡必须非维修员本人银行卡！');
							}
						}
					}


					// 当没有原身份证号或者原身份证号不等于现身份证号的时候校验身份证号是否重复
					if(!oldCardId || oldCardId!=cardId){
						win.utils.ajaxGet(win.utils.services.verifyCardId,{
							user_id: win.utils.getCookie('user_id'),
							session: win.utils.getCookie('session'),
							cardId:cardId,
							workerId:0
						},function (result) {
							if(result.result_code==200){
								if(result.data.is_exist){
									utils.showMessage('身份证号已存在，请重新输入！');
									$(this).val('');
								}
							}
							else{
								utils.showMessage('身份证号 校验失败！');
								$(this).val('');
							}
						})
					}
				}
			});
			/*****************校验*************************/

			// 导出功能
			$('#export').on('click',function () {
                var exportData={
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                };
				var dataArr=[];
				for(var key in exportData){
					dataArr.push(key+'='+exportData[key]);
				}
				window.location.href=win.utils.services.exportWorkerInfoList+'?'+dataArr.join('&');
			});

			// 自动分配设置----by 韩璐【20.1.13】
			let autoAllocationsSwitchValue=false;
			$('#autoAllocations').click(function () {
				var i = layer.open({
					type: 1,
					area: ['30%', '30%'],
					title:'自动分配设置',
					content: $('#AutoAllocationsLayer'),
					btn: ["确定", "取消"],
					success: function(){
						$('.layui-layer-page').css('zIndex',333);
						$('.layui-layer-shade').css('zIndex', 300);

						// 弹窗按钮颜色修改
						$('#AutoAllocationsLayer').parents('.layui-layer').find('.layui-layer-btn0').css({
                            'backgroundColor':'#5FB878',
                            'borderColor':'#5FB878'
                        });

						win.utils.ajaxGet(win.utils.services.queryAutoAssignWorkerSwitch,{
							user_id: win.utils.getCookie('user_id'),
							session: win.utils.getCookie('session'),
						},function (result) {
							if(result.result_code==200){
								if(result.data){
									$("input[name='autoAllocationsSwitch']").attr('checked','checked');
									$('#AutoAllocationsLayer .layui-form-switch').addClass('layui-form-onswitch');
									$('#AutoAllocationsLayer .layui-form-switch').find('em').text('开');
								}
								else{
									$("input[name='autoAllocationsSwitch']").removeAttr('checked');
									$('#AutoAllocationsLayer .layui-form-switch').removeClass('layui-form-onswitch');
									$('#AutoAllocationsLayer .layui-form-switch').find('em').text('关');
								}

								layer.use('form',function () {
									let form = layui.form;
									form.render();
								});
							}
							else{
								utils.showMessage('自动配置开关状态获取失败，请重试！');
							}
						});
					},
					yes: function(index, layero) {
						win.utils.ajaxGet(win.utils.services.updateAutoAssignWorkerSwitch,{
							user_id: win.utils.getCookie('user_id'),
							session: win.utils.getCookie('session'),
							autoAssignSwitch: autoAllocationsSwitchValue
						},function (result) {
							if(result.result_code==200){
								utils.showMessage('自动配置开关状态修改成功！');
								layer.close(i);
							}
							else{
								utils.showMessage('自动配置开关状态修改失败，请重试！');
							}
						});
					},
				});
			});
			layui.use('form',function () {
				let form=layui.form;
				form.on('switch(autoAllocationsSwitch)', function(data){
					autoAllocationsSwitchValue=data.elem.checked
				});
			});

			$(document).keydown(function (event) {
                var  e=window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if(flagInput==false){
                        if(e.preventDefault){
                            e.preventDefault();
                        }else{
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });
		},
		//初始化请求
		initList: function(){
			$("#table").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height:'auto',
				autowidth: false,
				rowNum: 10,
				rownumbers: true,
				rownumWidth:60,
				// rowList: [20, 50, 100],
				pgtext: "第X页 / 共X页",
				colNames: ["id","操作","维修员工号", "姓名", "电话","身份证号","城市", "技能种类","查看服务区域","接单状态","状态","完单量/未完单量","紧急联系人","紧急联系人电话"],
				colModel: [
					{
						name: "id",
						index: "id",
						editable: false,
						width: 40,
						sortable: false,
						formatter:function(cellvalue, options, rowObject){
							return rowObject.workerId
						},
						hidden:true
					},
					{
						name: "workerStatus",
						index: "workerStatus",
						editable: false,
						width: 240,
						sortable: false,
						formatter: tableOptions
					},
					{
						name: "workerId",
						index: "workerId",
						editable: false,
						width: 190,
						sortable: false,
					},
					{
						name: "workerName",
						index: "workerName",
						editable: false,
						width: 120,
						sortable: false
					},
					{
						name: "workerPhone",
						index: "workerPhone",
						editable: false,
						width: 120,
						sortable: false
					},
					{
						name: "cardId",
						index: "cardId",
						editable: false,
						width: 160,
						sortable: false,
						formatter:function (ellvalue, options, rowObject) {
							if(rowObject.cardId){
								if(rowObject.cardId.length==15){
									var beforeStr=rowObject.cardId.substring(0,3);
									var afterStr=rowObject.cardId.substring(11);
									return beforeStr+'********'+afterStr
								}
								else if(rowObject.cardId.length==18){
									var beforeStr=rowObject.cardId.substring(0,6);
									var afterStr=rowObject.cardId.substring(14);
									return beforeStr+'********'+afterStr
								}
								else{
									return '无'
								}
							}
							else{
								return '无'
							}
						}
					},
					{
						name: "regionName",
						index: "regionName",
						editable: false,
						width: 100,
						sortable: false
					},
					{
						name: "skillKind",
						index: "skillKind",
						editable: false,
						width: 150,
						sortable: false,
						formatter: function(cellvalue, options, rowObject) {
							if(rowObject.skillsLabel==1){
								if(rowObject.skillKind==1) return '综合维修，防水维修';
								else if(rowObject.skillKind==2) return '电器维修，防水维修';
								else if(rowObject.skillKind==3) return '综合维修，电器维修，防水维修'
								else if(rowObject.skillKind==0) return '防水维修'
							}
							else{
								if(rowObject.skillKind==1) return '综合维修';
								else if(rowObject.skillKind==2) return '电器维修';
								else if(rowObject.skillKind==3) return '综合维修，电器维修'
								else return ''
							}

						}
					},
					{
						name: "isInservice",
						index: "isInservice",
						editable: false,
						width: 100,
						sortable: false,
						formatter: function (cellvalue, options, rowObject) {
							if(rowObject.isInservice===1){
								return '<a id="checkArea" class="unified-a" href="###" data-id="'+rowObject.workerId+'">查看</a>'
							}
							else{
								return  '';
							}
						},
					},
					{
						name: "workerStatus",
						index: "workerStatus",
						editable: false,
						width: 180,
						sortable: false,
						formatter: getWorkerStatus
					},
					{
						name: "isInservice",
						index: "isInservice",
						editable: false,
						width: 180,
						sortable: false,
						formatter: isInservice
					},
					{
						name: "statistic",
						index: "statistic",
						editable: false,
						width: 180,
						sortable: false
					},
					{
						name: "urgentContacts",
						index: "urgentContacts",
						editable: false,
						width: 160,
						sortable: false
					},
					{
						name: "urgentPhone",
						index: "urgentPhone",
						editable: false,
						width: 180,
						sortable: false
					},
				],
				pager: "#table_pager",
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
				shrinkToFit: false,
				autoScroll:true,
				onPaging: function(pgButton) {
					deferLoadData = 0;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

					if(pgButton == 'next') {
						currentPage = currentPage + 1;
					}
					if(pgButton == 'last') {
						currentPage = lastPage;
					}
					if(pgButton == 'prev') {
						currentPage = currentPage - 1;
					}
					if(pgButton == 'first') {
						currentPage = 1;
					}
					if(pgButton == 'user') {
						deferLoadData = 1;
					}
					if(pgButton == 'records') {
						deferLoadData = 1;
					}
					nowPage=currentPage;
					if(deferLoadData == 0) {
						data.position=nowPage;
						repairman.searchFn();

						$('.detail').hide();
					}
				},
				onSortCol: function(index, iCol, sortorder) {
					alert('onSortCol')
					orderByString = index + ' ' + sortorder;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					nowPage=currentPage;
					repairman.searchFn();
				},
				//当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
				onSelectRow: function () {
					//返回选中的id
					selectedRowIndex = $("#" + this.id).getGridParam('selrow');
					repairman.rowOption(selectedRowIndex);
					// console.log(selectedRowIndex);
				},
			});
			repairman.getServerData(1, getRepairmanListData);

			$("#table").jqGrid("navGrid", "#table_pager", {
				edit: false,
				add: false,
				del: false,
				search: false,
				refresh: false
			});


			function tableOptions(cellvalue, options, rowObject) {
				if(rowObject.isInservice==1){
					if(rowObject.workerStatus ==1){
						return '<button type="button" class="btn btn-info opertion unified-btn-inline unified-change-btn-inline" id="btnUpdate" data-id="'+rowObject.workerId+'" style="margin-right: 20px">修改</button>' +
							'<button type="button" class="btn btn-info opertion unified-btn-inline unified-change-btn-inline" id="changeReceiptStatus"  data-id="'+rowObject.workerId+'" data-status="'+rowObject.workerStatus+'">请假</button>'
					}
					else if(rowObject.workerStatus ==2){
						return '<button type="button" class="btn btn-info opertion unified-btn-inline unified-change-btn-inline" id="btnUpdate" data-id="'+rowObject.workerId+'" style="margin-right: 20px">修改</button>' +
							'<button type="button" class="btn btn-info opertion unified-btn-inline unified-publish-btn-inline" id="changeReceiptStatus"  data-id="'+rowObject.workerId+'" data-status="'+rowObject.workerStatus+'">销假</button>'
					}
				}else{
					return  ''
				}

			};
			function getWorkerStatus(cellvalue, options, rowObject){
				if(rowObject.workerStatus==1){
					return '接单';
				}
				else if(rowObject.workerStatus==2){
					return '不接单';
				}
			};
			function isInservice(cellvalue, options, rowObject){
				if(rowObject.isInservice==1){
					return '启用';
				}
				else if(rowObject.isInservice==2){
					return '禁用';
				}
			};


			// 请假记录表
			$("#leaveRecordTable").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height: 600,
				maxHeight:600,
				autowidth:false,
				shrinkToFit:false,
				autoScroll: true,
				rownumbers:true,
				rownumWidth:60,
				rowNum:-1,
				colNames: ["请假开始时间", "请假结束时间", "销假时间", "综合备用师傅", "电器备用师傅", "操作人", "操作时间"],
				colModel: [
					{
						name: "leaveStartTime",
						index: "leaveStartTime",
						editable: false,
						width: 180,
						sortable: false,
                        formatter:function (cellvalue, options, rowObject) {
							if(rowObject.leaveStartTime){
								var leaveStartTime = repairman.timeStampFormat(rowObject.leaveStartTime);
								return leaveStartTime;
							}
							else{
								return '';
							}
						}
					},
					{
						name: "leaveEndTime",
						index: "leaveEndTime",
						editable: false,
						width: 180,
						sortable: false,
						formatter:function (cellvalue, options, rowObject) {
							if(rowObject.leaveEndTime){
								var leaveEndTime = repairman.timeStampFormat(rowObject.leaveEndTime);
								return leaveEndTime;
							}
							else{
								return '';
							}
						}
					},
					{
						name: "removeLeaveTime",
						index: "removeLeaveTime",
						editable: false,
						width: 180,
						sortable: false,
						formatter:function (cellvalue, options, rowObject) {
							if(rowObject.removeLeaveTime){
								var removeLeaveTime = repairman.timeStampFormat(rowObject.removeLeaveTime);
								return removeLeaveTime;
							}
							else{
								return '';
							}
						}
					},
					{
						name: "synthesisSpare",
						index: "synthesisSpare",
						editable: false,
						width: 120,
						sortable: false,
					},
					{
						name: "electricSpare",
						index: "electricSpare",
						editable: false,
						width: 120,
						sortable: false,
					},
					{
						name: "createUser",
						index: "createUser",
						editable: false,
						width: 120,
						sortable: false,
					},
					{
						name: "createTime",
						index: "createTime",
						editable: false,
						width: 180,
						sortable: false,
						formatter:function (cellvalue, options, rowObject) {
							if(rowObject.createTime){
								var createTime = repairman.timeStampFormat(rowObject.createTime);
								return createTime;
							}
							else{
								return '';
							}
						}
					},
				],
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
			});

            // 银行卡信息
			$("#cardInfoTable").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height: 600,
				maxHeight:600,
				autowidth:false,
				shrinkToFit:false,
				autoScroll: true,
				rownumbers:true,
				rownumWidth:60,
				rowNum:-1,
				colNames: ["身份证姓名", "身份证号", "银行卡号", "开户银行", "开户城市", "操作人", "操作时间"],
				colModel: [
					{
						name: "cardholder",
						index: "cardholder",
						editable: false,
						width: 180,
						sortable: false,
					},
					{
						name: "cardId",
						index: "cardId",
						editable: false,
						width: 180,
						sortable: false,
					},
					{
						name: "bankCard",
						index: "bankCard",
						editable: false,
						width: 180,
						sortable: false,
					},
					{
						name: "bank",
						index: "bank",
						editable: false,
						width: 200,
						sortable: false,
						formatter:function (cellvalue, options, rowObject) {
							return rowObject.bank +'-'+rowObject.bankBranch+'-'+rowObject.bankfiliale
						}
					},
					{
						name: "city",
						index: "city",
						editable: false,
						width: 120,
						sortable: false,
					},
					{
						name: "createUser",
						index: "createUser",
						editable: false,
						width: 120,
						sortable: false,
					},
					{
						name: "createTime",
						index: "createTime",
						editable: false,
						width: 180,
						sortable: false,
						formatter:function (cellvalue, options, rowObject) {
							if(rowObject.createTime){
								var createTime = repairman.timeStampFormat(rowObject.createTime);
								return createTime;
							}
							else{
								return '';
							}
						}
					},
				],
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
			});

			// 操作记录表
			$("#optionRecordTable").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height:600,
				maxHeight:600,
				autowidth:false,
				shrinkToFit:false,
				autoScroll: true,
				rownumbers:true,
				rownumWidth:60,
				rowNum:-1,
				colNames: ["操作", "操作详情","操作时间"],
				colModel: [
					{
						name: "operationType",
						index: "operationType",
						editable: false,
						width: 160,
						sortable: false
					},
					{
						name: "operationDesc",
						index: "operationDesc",
						editable: false,
						width: 460,
						sortable: false
					},
                    {
                        name: "createTime",
                        index: "operationDesc",
                        editable: false,
                        width: 200,
                        sortable: false,
                        formatter:cearteTime
                    },
				],
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
			});

			function cearteTime(cellvalue, options, rowObject) {
                if(rowObject.createTime){
                    return repairman.timeStampFormat(rowObject.createTime)
                }
            }


			var b = $(".jqGrid_wrapper").width();
			$("#table").setGridWidth(b);
			$("#leaveRecordTable").setGridWidth(b);
			$("#optionRecordTable").setGridWidth(b);
			$('#cardInfoTable').setGridWidth(b);
			$(window).bind("resize", function () {
				var b = $(".jqGrid_wrapper").width();
				$("#table").setGridWidth(b);
				$("#leaveRecordTable").setGridWidth(b);
				$("#optionRecordTable").setGridWidth(b);
				$("#cardInfoTable").setGridWidth(b);
				// $("#table").setGridHeight(utils.getAutoGridHeight());
			});
		},
		// 刷新
		refreshServerData: function() {
			repairman.searchConInit();

			$('.search_zoning').hide();
			$('.search_tradingArea').hide();
			$('.search_village').hide();

			data={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				workerId:null,
				workerName:null,
				regionId: null,
				skillKind:null,
				districtId:null,
				districtCbdId:null,
				blockId:null,
				workerStatus:null,
				isInservice:null,
				isQueryFinishNum:1,
				position: nowPage,
				rows: 10
			};
			repairman.getServerData(nowPage, data);
			$('.detail').hide();
			$('#AllBtnAction button').removeClass('active').eq(0).addClass('active');
		},
		getServerData: function(pageIndex,postData,fn)  {
			is_search=false;
			pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
			win.utils.ajaxPost(win.utils.services.getWorker, postData, fn?fn:function(result) {
				if(result.result_code == 200) {
					if(result.total == 0) {
						gridData = [];
						var gridJson = {
							total: 0,
							rows: gridData,
							page: 0,
							records: result.total
						};
						pageText = '第0页 / 共0页';
						$("#table").jqGrid('setGridParam', {
							pgtext: pageText
						});
						$("#table")[0].addJSONData(gridJson);
						utils.showMessage('暂无数据！');
					} else {
						result.workerInfoVOS.forEach(function (i,k) {
							i.id=i.workerId;
						});
						gridData = result.workerInfoVOS;
						selectId=gridData[0].id;
						totalPages = Math.ceil(result.total / pageCount);
						var gridJson = {
							total: totalPages,
							rows: gridData,
							page: pageIndex,
							records: result.total
						};
						pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
						$("#table").jqGrid('setGridParam', {
							pgtext: pageText
						});
						$("#table")[0].addJSONData(gridJson);
					}
				} else {
					utils.showMessage('获取数据失败，请重试！');
				}
			});
		},
		// 搜索
		searchFn:function(){
			if(is_search){
				var workerId=$('#search_workerId').val()||null;
				var name = $('#search_name').val()||null;

				var receiptStatus = $('#search_receiptStatus').val();
				var isInService = $('#search_isInService').val();
				// var skillType=$('#skillTypeSearch').val();
				var phone=$('#search_phone').val();

				/**----------------处理技能分类开始--------------------------*/
				var skillTypeAll,skillTypeEle, skillKind, skillsLabel;
				if($('#search_all').prop('checked')) {
					skillTypeAll = 1;
				}else{
					skillTypeAll=0;
				}
				if($('#search_ele').prop('checked')) {
					skillTypeEle = 1;
				}else{
					skillTypeEle=0;
				}

				if($('#search_waterproof').prop('checked')){
					skillsLabel=1;
				}else{
					skillsLabel=null;
				}

				if(skillTypeAll==1 && skillTypeEle==1) {
					skillKind=3;
				}
				else if(skillTypeAll==1 && skillTypeEle!==1){
					skillKind=1;
				}
				else if(skillTypeAll!==1 && skillTypeEle==1){
					skillKind=2;
				}
				else if(skillTypeAll==0 && skillTypeEle==0){
					skillKind=null;
				}
				/**----------------处理技能分类结束--------------------------*/

				data.workerId = workerId;
				data.workerName = name;
				data.regionId = $('#search_city').val() || null;
				data.skillKind = skillKind;
				data.districtId = $('#search_zoning').val() || null;
				data.districtCbdId = $('#search_tradingArea').val()||null;
				data.blockId = $('#search_village').val() || null;
				data.workerStatus = receiptStatus?receiptStatus:null;
				data.isInservice = isInService?isInService:null;
				data.isQueryFinishNum=1;
				data.position = nowPage;
				data.skillsLabel = skillsLabel;
				data.phoneNum=phone;
			}

			repairman.getServerData(nowPage,data);
		},
		//图片上传
		loadLocalImage: function(fileObject, imageID, imageType, imgShowNode) {
			if (typeof FileReader == 'undefined') {
				alert("当前浏览器不支持FileReader接口");
				return;
			}
			var file = fileObject.files[0];
			let reader = new FileReader();//新建一个FileReader对象
			reader.readAsDataURL(file);//将读取的文件转换成base64格式
			reader.onload = function (e) {

				// 压缩图片
				var image = new Image()     //新建一个img标签（不嵌入DOM节点，仅做canvas操作)
				image.src = e.target.result    //让该标签加载base64格式的原图
				image.onload = function () {    //图片加载完毕后再通过canvas压缩图片，否则图片还没加载完就压缩，结果图片是全黑的
					let canvas = document.createElement('canvas'), //创建一个canvas元素
						context = canvas.getContext('2d'),    //context相当于画笔，里面有各种可以进行绘图的API
						imageWidth = image.width / 2,    //压缩后图片的宽度，这里设置为缩小一半
						imageHeight = image.height / 2,    //压缩后图片的高度，这里设置为缩小一半
						data = ''    //存储压缩后的图片
					canvas.width = imageWidth    //设置绘图的宽度
					canvas.height = imageHeight    //设置绘图的高度

					//使用drawImage重新设置img标签中的图片大小，实现压缩。drawImage方法的参数可以自行查阅W3C
					context.drawImage(image, 0, 0, imageWidth, imageHeight)

					//使用toDataURL将canvas上的图片转换为base64格式
					data = canvas.toDataURL('image/jpeg');
					var blobURL = repairman.dataURItoBlob(data);

					// 图片上传至七牛云
					repairman.qiniuUpload(
						blobURL,
						// file.name,
						'bjx-public-' + new Date().getTime() + (parseInt(Math.random() * 100 + 1)),
                        imgShowNode
					);
				}
			}
		},
		// base64转blob
		dataURItoBlob: function(base64Data) {
			var byteString;
			if(base64Data.split(',')[0].indexOf('base64') >= 0)
				byteString = atob(base64Data.split(',')[1]);
			else
				byteString = unescape(base64Data.split(',')[1]);
			var mimeString = base64Data.split(',')[0].split(':')[1].split(';')[0];
			var ia = new Uint8Array(byteString.length);
			for(var i = 0; i < byteString.length; i++) {
				ia[i] = byteString.charCodeAt(i);
			}
			return new Blob([ia], {
				type: mimeString
			});
		},
		/**七牛云上传图片
		 * cardImageUrl-----需要上传图片的bolb地址
		 * token-----token
		 * fileName-----上传图片的原文件名
		 * nodeObj------显示图片的盒子
		 * */
		qiniuUpload: function(cardImageUrl, fileName, nodeObj) {
			$.ajax({
				url: win.utils.services.getQiniuToken,
				type: 'get',
				dataType: 'json',
				success: function (res) {
					var qiniuToken = res.token;
					var observable = qiniu.upload(
						cardImageUrl,
						fileName,
						qiniuToken,
						{
							fname: fileName,
							params: {}, //用来放置自定义变量
							mimeType: null
						}, {
							useCdnDomain: true,

							region: null
						}
					);
					observable.subscribe({
						complete(res) {
							var qiniuImgUrl = 'http://images.baijiaxiu.com/'+res.key;
                            nodeObj.attr('src', qiniuImgUrl);
							utils.showMessage('图片上传成功！');
							$('input[type="file"]').val("");
						}
					});
				}
			});
		},

		// 增加修改搜索弹层初始化
		addConInit: function(){
			$('#workerId').val('');
			$('#name').val('');
			$('#phone').val('');
			$('#status').val();
			$('#all').iCheck('uncheck');
			$('#ele').iCheck('uncheck');
			$('#waterproof').iCheck('uncheck');
			$('#city').html('<option value="null" selected>请选择城市</option>');
			$('#zoning').html('<option value="null" selected>请选择区域</option>');
			$('#tradingArea').html('<option value="null" selected>请选择商圈</option>');
			$('#village').html('<option value="null" selected>请选择小区</option>');
			$('#skillTypeSearch').val('');
			$('#receiptStatus').val('');
			$('#isInService').val('');
			$('#uploadImgBox').find('img').attr('src','');
			$('.zoning').hide();
			$('.tradingArea').hide();
			$('.village').hide();
			$('#urgencyPhone').val('');
			$('#urgencyName').val('');

            $('#formAdd #cardNum').val(''); //身份证号
            $('#formAdd #cardUploadBtn1').find('img').attr('src','../../images/upload_card_img1.png'); //身份照片
            $('#formAdd #cardUploadBtn2').find('img').attr('src','../../images/upload_card_img2.png'); //身份照片
            $('.card-box .add-card-box').html(''); //银行卡集合

		},
		searchConInit:function(){
			$('#search_workerId').val('');
			$('#search_name').val('');
			$('#search_phone').val('');
			$('#search_city').val('');
			$('#search_zoning').val('');
			$('#search_tradingArea').val('');
			$('#search_village').val('');
			$('#search_receiptStatus').val('');
			$('#search_isInService').val('');
			$('#search_all').iCheck('uncheck');
			$('#search_ele').iCheck('uncheck');
			$('#search_waterproof').iCheck('uncheck');
			layui.use('form',function () {
				var form=layui.form;
				form.render('select');
			})
		},
		// 增加弹层内容显示
		addLayerContent: function () {
			$('#formAdd .form-group').find('strong').show();
			$('#formAdd .wordId-box').hide(); //维修员工号
			$('#formAdd .name-box').show(); //维修员姓名
			$('#formAdd .phone-box').show(); //手机号
			$('#formAdd .cardNum-box').show(); //身份证号
			$('#formAdd .cardPic-box').show(); //身份证正反面照片
			$('#formAdd .status-box').hide(); //状态
			$('#formAdd .skill-box').show();  //技能种类
			$('#formAdd .water-box').hide(); //是否会防水
			$('#formAdd .headImg-box').show(); //上传头像
			$('#formAdd .sikllType-box').hide(); //技能类型
			$('#formAdd .city').show(); //城市
			$('#formAdd .trading').hide(); //行政区域
			$('#formAdd .zoning').hide(); //区域
			$('#formAdd .village').hide(); //小区
 			$('#formAdd .skillSearch-box').hide(); //技能分类搜索
			$('#formAdd .receipt-box').hide(); //接单状态
			$('#formAdd .isInserver-box').hide(); //在职状态
			$('#formAdd .urgencyName-box').show(); //紧急联系人姓名
			$('#formAdd .urgencyPhone-box').show(); //紧急联系人手机号
		},
		// 修改弹层内容显示
		updateLayerContent: function () {
			$('#formAdd .form-group').find('strong').hide();
			$('#formAdd .wordId-box').hide(); //维修员工号
			$('#formAdd .name-box').show(); //维修员姓名
			$('#formAdd .phone-box').show(); //手机号
			$('#formAdd .cardNum-box').show(); //身份证号
			$('#formAdd .cardPic-box').show(); //身份证正反面照片
			$('#formAdd .status-box').hide(); //状态
			$('#formAdd .skill-box').show();  //技能种类
			$('#formAdd .water-box').hide(); //是否会防水
			$('#formAdd .headImg-box').show(); //上传头像
			$('#formAdd .sikllType-box').hide(); //技能类型
			$('#formAdd .city').show(); //城市
			$('#formAdd .trading').hide(); //行政区域
			$('#formAdd .zoning').hide(); //区域
			$('#formAdd .village').hide(); //小区
			$('#formAdd .skillSearch-box').hide(); //技能分类搜索
			$('#formAdd .receipt-box').hide(); //接单状态
			$('#formAdd .isInserver-box').show(); //在职状态
			$('#formAdd .urgencyName-box').show(); //紧急联系人姓名
			$('#formAdd .urgencyPhone-box').show(); //紧急联系人手机号
		},

		/**-----------------维修员请假/销假相关方法开始---------------------------*/
		// 维修员请假/休假
		changeReceiptStatus: function (obj)  {
			// 请假
			if(obj.workerStatus==1){
				// location.href=utils.domain_name+'web/areaManage/area_list.html?workerId='+obj.id+'/'+encodeURI(obj.workerName)+'/'+obj.workerPhone+'&type='+'2';
				var repairmanInfo={
					workerId:obj.id,
					workerName:obj.workerName,
					workerPhone:obj.workerPhone,
					type:2,
					source:32
				};
				win.localStorage.setItem("repairmanInfo",JSON.stringify(repairmanInfo));
				parent.$(window.parent.document).find('.nav .J_menuItem[data-id=21]').click();
				parent.index.getParamsToChild(repairmanInfo);
			}
			// 销假
			else if(obj.workerStatus==2){
				utils.showConfirm('是否确定销假？销假后维修员将重新接受服务区域的订单自动分派？', '确定', function() {
					win.utils.ajaxPost(win.utils.services.addCancelLeaveInfo, {
						user_id: win.utils.getCookie('user_id'),
						session: win.utils.getCookie('session'),
						workerId: obj.id,
						workerName:obj.workerName
					}, function(result) {
						if(result.result_code == 200) {
							utils.showMessage('销假成功！');
							utils.closeMessage();

							$('#table').jqGrid('setRowData',obj.id,{
								workerStatus:1,
								isInservice:1,
								workerId:obj.id
							});

						} else {
							utils.showMessage('销假失败，请重试！');
						}
					});
				});
			}
		},
		/**-----------------维修员请假/销假相关方法结束---------------------------*/

		/**-----------------新增维修员相关方法开始-------------------------------------------*/
		//新增-修改维修员
		addWorkOrder: function(act, id){
			$('.checkPhoneCon').hide();
			$('.checkNameCon').hide();
			$('.checkurgencyPhoneCon').hide();
			$('.checkurgencyNameCon').hide();

			$(':radio[value=0]').prop("checked",true);
			$(':radio[value=1]').prop("checked",false);
			skillsLabel=0;
			layui.use('form',function () {
				var form=layui.form;
				form.render('radio');
			});


            let datas={
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                workerName:null,
                workerPhone: null,
                regionId: null,
                regionName:null,
                skillKind:null,
                avatarUrl:null,
                urgentPhone: null,
                urgentContacts: null,
                skillsLabel:skillsLabel
            };

			var regionId, regionName, skillsLabel;

			// 新增维修员
			if(act==1){
				repairman.addLayerContent();

				// 获取城市数据
				var postData = {
					user_id: win.utils.getCookie('user_id'),
					session: win.utils.getCookie('session'),
					userId: win.utils.getCookie('user_id')
				};
				repairman.getCityData(postData, function (result) {
					var optionStr='';
					result.sysUserCityVOS.forEach(function (i,k) {
						optionStr+='<option value="'+i.cityId+'">'+i.addressName+'</option>'
					});
					$('#city').html(optionStr);
					$('#city').find('option').eq(0).attr("selected",true);
				});

				regionId=1;
				regionName='北京市';
				$('#name').removeAttr('disabled');//姓名可修改
			}
			// 修改维修员
			else if(act==2){
				repairman.updateLayerContent();

				if(id==null){
					utils.showMessage('请选择要进行修改的记录！');
					return;
				};
				$('#formAdd .form-group').find('strong').hide();

			};


			let titleVal=act==1?'新增':'修改';
			let arr=act==1?['新增','取消']:['修改','取消'];
			var i = layer.open({
				type: 1,
				area: ['80%', '80%'],
				title:titleVal,
				content: $('#AddCon'),
				btn: arr,
				success: function(){
                    $('.layui-layer-page').css('zIndex',333);
                    $('.layui-layer-shade').css('zIndex', 300);

                    flagInput=false;

                    if(act==2){
						// 获取数据
						getRepairmanListData.workerId=id;
						repairman.getServerData(nowPage, getRepairmanListData,function (result) {
							$('#name').val(result.workerInfoVOS[0].workerName);
							$('#phone').val(result.workerInfoVOS[0].workerPhone);
							$('#name').attr('disabled','disabled');//姓名不可修改

							$('#urgencyName').val(result.workerInfoVOS[0].urgentContacts);
							$('#urgencyPhone').val(result.workerInfoVOS[0].urgentPhone);


                            // 是否会防水维修
                            if(result.workerInfoVOS[0].skillsLabel==1){
                                $('#waterproof').iCheck('check');
                            }

							oldPhone=result.workerInfoVOS[0].workerPhone;
							oldurgencyPhone=result.workerInfoVOS[0].urgentPhone;
							skillsLabel=result.workerInfoVOS[0].skillsLabel;


							// 获取城市数据
							var postData = {
								user_id: win.utils.getCookie('user_id'),
								session: win.utils.getCookie('session'),
								userId: win.utils.getCookie('user_id')
							};
							repairman.getCityData(postData, function (res) {
								var optionStr='';
								res.sysUserCityVOS.forEach(function (i,k) {
									optionStr+='<option value="'+i.cityId+'">'+i.addressName+'</option>'
								});
								$('#city').html(optionStr);
								// $('#city').find('option').eq(0).attr("selected",true);
								$('#city').val(result.workerInfoVOS[0].regionId);
								$('#city').find('option[text='+result.workerInfoVOS[0].regionName+']').attr('selected',true);
							});


							regionId=result.workerInfoVOS[0].regionId;

							if(result.workerInfoVOS[0].skillKind==1){
								$('#all').iCheck('check');
							}
							else if(result.workerInfoVOS[0].skillKind==2){
								$('#ele').iCheck('check');
							}
							else if(result.workerInfoVOS[0].skillKind==3){
								$('#all').iCheck('check');
								$('#ele').iCheck('check');
							}

							$('#uploadImgBox').find('img').attr('src',result.workerInfoVOS[0].avatarUrl);

							$('#isInService').val(result.workerInfoVOS[0].isInservice);


							oldSkillKind=result.workerInfoVOS[0].skillKind;
							oldCity=result.workerInfoVOS[0].regionId;

							repairman.canOutInservice(id);

							// 身份证号回显
							oldCardId=result.workerInfoVOS[0].cardId;
							oldBankInfo=result.workerInfoVOS[0].workerBankInfoListVo;
							$('#formAdd #cardNum').val(result.workerInfoVOS[0].cardId);
							// 身份证图片回显
							if(result.workerInfoVOS[0].cardPic){
								$('#cardUploadBtn1').find('img').attr('src',result.workerInfoVOS[0].cardPic.split(',')[0]);
								$('#cardUploadBtn2').find('img').attr('src',result.workerInfoVOS[0].cardPic.split(',')[1]);
							}

							// 银行卡信息回显
							if(result.workerInfoVOS[0].workerBankInfoListVo && result.workerInfoVOS[0].workerBankInfoListVo.length){
								var cardInfoStr='';
								result.workerInfoVOS[0].workerBankInfoListVo.forEach(function (i,k) {
									cardInfoStr+='<div class="add-card-item">\n' +
										'\t\t\t\t\t\t<div class="card-tit">\n' +
										'\t\t\t\t\t\t\t<span>银行卡-'+(k+1)+'</span>\n' +
										'\t\t\t\t\t\t\t<span></span>\n' +
										'\t\t\t\t\t\t\t<button type="button" class="btn deleteCardBtn" data-index="'+k+'" data-id="'+i.id+'" data-workerid="'+result.workerInfoVOS[0].workerId+'">删除</button>\n' +
										'\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t<div class="card-info">\n' +
										'\t\t\t\t\t\t\t<div class="form-horizontal">\n' +
										'\t\t\t\t\t\t\t\t<!--银行卡号-->\n' +
										'\t\t\t\t\t\t\t\t<div class="form-group">\n' +
										'\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>银行卡号</label>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
										'\t\t\t\t\t\t\t\t\t\t<input name="bankCard" type="number" class="bankCard form-control" placeholder="请输入银行卡号" autocomplete="off">\n' +
										'\t\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label" style="text-align: center; color: rgba(107,111,129,1); ">\n' +
										'\t\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"></label>\n' +
										'\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6 bankcardtip">（必须是维修员本人的银行卡号）</div>\n' +
										'\t\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t<!--持卡人姓名-->\n' +
										'\t\t\t\t\t\t\t\t<div class="form-group">\n' +
										'\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>持卡人姓名</label>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
										'\t\t\t\t\t\t\t\t\t\t<input name="cardholder" type="text" class="cardholder form-control" placeholder="请输入持卡人姓名" autocomplete="off">\n' +
										'\t\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-3 control-label checkcardholderCon" style="text-align: left; color: red; display: none;">姓名不符合规范</div>\n' +
										'\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t<!--持卡人身份证号-->\n' +
										'\t\t\t\t\t\t\t\t<div class="form-group">\n' +
										'\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>持卡人身份证号</label>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
										'\t\t\t\t\t\t\t\t\t\t<input name="cardId" type="text" class="cardId form-control" placeholder="请输入持卡人身份证号" autocomplete="off">\n' +
										'\t\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-3 control-label checkBankCardIdCon" style="text-align: left; color: red; display: none;">开户城市不符合规范</div>\n' +
										'\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t<!--开户银行-->\n' +
										'\t\t\t\t\t\t\t\t<div class="form-group">\n' +
										'\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>开户银行</label>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-3">\n' +
										'\t\t\t\t\t\t\t\t\t\t<input name="bank" type="text" class="bank form-control" placeholder="请输入开户银行名称" autocomplete="off">\n' +
										'\t\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-3">\n' +
										'\t\t\t\t\t\t\t\t\t\t<input name="bankBranch" type="text" class="bankBranch form-control" placeholder="请输入开户分行" autocomplete="off">\n' +
										'\t\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-3">\n' +
										'\t\t\t\t\t\t\t\t\t\t<input name="bankfiliale" type="text" class="bankfiliale form-control" placeholder="请输入开户支行" autocomplete="off">\n' +
										'\t\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label checkbankCon" style="text-align: left; color: red; display: none;"><label class="col-sm-3 control-label"></label><span>开户银行名称不符合规范</span></div>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label checkbankBranchCon" style="text-align: left; color: red; display: none;"><label class="col-sm-3 control-label"></label><span>开户分行不符合规范</span></div>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label checkbankfilialeCon" style="text-align: left; color: red; display: none;"><label class="col-sm-3 control-label"></label><span>开户支行不符合规范</span></div>\n' +
										'\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t<!--开户城市-->\n' +
										'\t\t\t\t\t\t\t\t<div class="form-group city">\n' +
										'\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>开户城市</label>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-4">\n' +
										'\t\t\t\t\t\t\t\t\t\t<input name="city" type="text" class="city form-control" placeholder="开户城市" autocomplete="off">\n' +
										'\t\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t\t\t<div class="col-sm-3 control-label checkcityCon" style="text-align: left; color: red; display: none;">开户城市不符合规范</div>\n' +
										'\t\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t\t</div>\n' +
										'\t\t\t\t\t</div>';
								});
								$('.add-card-box').html(cardInfoStr);
								$("input[name='cardId']").eq(0).prop('readonly',true); //第一张银行卡身份证号不可编辑
                                // 银行卡提示文字
                                $('.bankcardtip').text('(必须非维修员本人的银行卡)').eq(0).text('(必须为维修员本人的银行卡)');

								// 获取银行卡集合循环赋值
								var cardInfoNodes=$('.card-box').find('.add-card-item');
								if(cardInfoNodes.length){
									for(var i=0;i<cardInfoNodes.length;i++){
										$(cardInfoNodes[i]).find('.bankCard').val(result.workerInfoVOS[0].workerBankInfoListVo[i].bankCard);
										$(cardInfoNodes[i]).find('.cardholder').val(result.workerInfoVOS[0].workerBankInfoListVo[i].cardholder);
										$(cardInfoNodes[i]).find('.cardId').val(result.workerInfoVOS[0].workerBankInfoListVo[i].cardId);
										$(cardInfoNodes[i]).find('.bank').val(result.workerInfoVOS[0].workerBankInfoListVo[i].bank);
										$(cardInfoNodes[i]).find('.bankBranch').val(result.workerInfoVOS[0].workerBankInfoListVo[i].bankBranch);
										$(cardInfoNodes[i]).find('.bankfiliale').val(result.workerInfoVOS[0].workerBankInfoListVo[i].bankfiliale);
										$(cardInfoNodes[i]).find('.city').val(result.workerInfoVOS[0].workerBankInfoListVo[i].city);
									}
								}
							}
						});
					}

					$('#city').off('change').on('change', function (e) {
						regionId=$('#city').val();
						regionName=$('#city').find('option:selected').text();
					});

					// 是否会防水维修选择
					layui.use('form',function () {
						var form=layui.form;
						form.on('radio',function (data) {
							skillsLabel=data.value;
						})
					})
				},
				yes: function(index, layero) {
					var isPhoneConShow = $(".checkPhoneCon").is(":visible"); //手机号
					var isNameConShow = $(".checkNameCon").is(":visible"); //姓名
					var isCardIdConShow = $(".checkcardIdCon").is(":visible"); //身份证号
					var isurgencyPhoneConShow=$(".checkurgencyPhoneCon").is(":visible"); //紧急联系人电话
					var isurgencyNameConShow=$(".checkurgencyNameCon").is(":visible"); //紧急联系人姓名
					var iscardholderConShow=$(".checkcardholderCon").is(":visible"); //银行卡持有人
					var iscardBankCardIdShow=$(".checkBankCardIdCon").is(":visible"); //银行卡持有人身份证号
					var isbankConShow=$(".checkbankCon").is(":visible"); //银行卡
					var isbankBranchConShow=$(".checkbankBranchCon").is(":visible"); //银行卡分行
					var isbankfilialeConShow=$(".checkbankfilialeCon").is(":visible");//银行卡支行
					var iscityConShow=$(".checkcityCon").is(":visible"); //开卡城市


					if(!isPhoneConShow && !isNameConShow && !isCardIdConShow && !isurgencyPhoneConShow && !isurgencyNameConShow && !iscardholderConShow && !iscardBankCardIdShow && !isbankConShow && !isbankBranchConShow && !isbankfilialeConShow && !iscityConShow){
						var name = $('#name').val();
						var phone = $('#phone').val();
						var urgencyName = $('#urgencyName').val();
						var urgencyPhone = $('#urgencyPhone').val();

						var cardId=$('#cardNum').val(); //身份证号
						var cardPic1=$('#cardUploadBtn1 img').attr('src').split('/')[3]!='upload_card_img1.png'?$('#cardUploadBtn1 img').attr('src').split('/')[3]:null; //身份证图片
						var cardPic2=$('#cardUploadBtn2 img').attr('src').split('/')[3]!='upload_card_img2.png'?$('#cardUploadBtn2 img').attr('src').split('/')[3]:null; //身份证图片

						var imgUrl = $('#uploadImgBox').find('img').attr('src').split('/')[3];

						/**----------------处理技能分类开始--------------------------*/
						var skillTypeAll,skillTypeEle,skillsLabel;
						if($('#all').prop('checked')) {
							skillTypeAll = 1;
						}else{
							skillTypeAll=0;
						}
						if($('#ele').prop('checked')) {
							skillTypeEle = 1;
						}else{
							skillTypeEle=0;
						}
						if($('#waterproof').prop('checked')){
                            skillsLabel=1;
                        }else{
							skillsLabel=0;
						}
						var skillKind;
						if(skillTypeAll==1 && skillTypeEle==1) {
							skillKind=3;
						}
						else if(skillTypeAll==1 && skillTypeEle!==1){
							skillKind=1;
						}
						else if(skillTypeAll!==1 && skillTypeEle==1){
							skillKind=2;
						}
						else if(skillTypeAll==0 && skillTypeEle==0){
							skillKind=0;
						}
						// else{
						// 	utils.showLayerMessage('请选择技能分类！');
						// 	return;
						// }
                        if(!(skillKind || skillsLabel)){
                            utils.showMessage('请选择技能分类！');
                            return;
                        }
						/**----------------处理技能分类结束--------------------------*/

						if(name == '') {
							$('#name').focus();
							utils.showLayerMessage('请输入姓名！');
							return;
						}
						if(name.length>20){
							utils.showMessage('姓名不得超过20个字，请重新输入！');
							return;
						}
						if(phone == '') {
							$('#phone').focus();
							utils.showLayerMessage('请输入手机号！');
							return;
						}

						if(!cardId){
							utils.showMessage('请填写身份证号！');
							return;
						}

						if(!cardPic1 || !cardPic2){
							utils.showMessage('请上传身份证图片！');
							return;
						}

						if(!imgUrl) {
							utils.showLayerMessage('请上传头像！');
							return;
						}
						if(!urgencyName){
							utils.showMessage('请输入紧急联系人姓名！');
							return;
						}
						if(urgencyName.length>20){
							utils.showMessage('紧急联系人姓名不得超过20个字，请重新输入！');
							return;
						}
						if(!urgencyPhone){
							utils.showMessage('请输入紧急联系人电话！');
							return;
						}
						// if(skillsLabel!==0 && skillsLabel!==1){
						// 	utils.showMessage('请选择是否有防水技能！');
						// 	return;
						// }


                        datas.workerName=name;
                        datas.workerPhone=phone;
                        datas.cardId=cardId;
                        datas.cardPic=cardPic1+","+cardPic2;
                        datas.regionId=regionId;
                        datas.regionName=regionName;
                        datas.skillKind=skillKind;
                        datas.avatarUrl=imgUrl;
                        datas.urgentPhone=urgencyPhone;
                        datas.urgentContacts=urgencyName;
                        datas.skillsLabel=skillsLabel||0;

                        var cardInfoObj=repairman.getCardInfo();

                        if(cardInfoObj && !cardInfoObj.isCardInfoFlage){
                        	datas.workerBankInfoListVo=cardInfoObj.WorkerBankInfoVO;

							if(act==1){
								datas.regionId=$('#city').val();
								datas.regionName=$('#city').find('option:selected').text();
								win.utils.ajaxPost(win.utils.services.addWorker, datas, function(result) {
									if(result.result_code == 200) {
										utils.showMessage('新增成功！');
										repairman.addConInit();
										layer.close(i);
										// repairman.refreshServerData();
										$('#table').jqGrid('addRowData', result.data.workerId, {
											id:result.data.workerId,
											avatarUrl:result.data.avatarUrl,
											createTime:result.data.createTime,
											createUser:result.data.createUser,
											isInservice:1,
											regionId:result.data.regionId,
											regionName:result.data.regionName,
											skillKind:skillKind,
											skillKindName:'',
											statistic:'0/0',
											workerId:result.data.workerId,
											workerName:result.data.workerName,
											workerPhone:result.data.workerPhone,
											workerStatus:1,
											skillsLabel:skillsLabel || 0,
											urgentPhone: urgencyPhone,
											urgentContacts: urgencyName,
											cardId:cardId
										});
									} else {
										utils.showMessage('新增失败，请重试！');
									}
								});
							}
							// 修改
							if(act==2){
								var id=$('#table').jqGrid('getGridParam','selrow');
								regionName = $('#city').find('option:selected').text();

								if(skillKind==1 && skillKind!==oldSkillKind && regionId==oldCity){
									layer.confirm(
										'您修改了该维修员的技能分类，修改后该维修员当前负责区域全部清空。是否确认修改？',
										function(index) {
											layer.close(index);
											datas.workerId=id;
											datas.isInservice=Number($('#isInService').val());
											datas.skillsLabel=skillsLabel;
											if(datas.isInservice==2){
												datas.workerStatus=2;
											}
											win.utils.ajaxPost(win.utils.services.updateWorker, datas, function(result) {
												if(result.result_code == 200) {
													utils.showMessage('修改成功！');
													repairman.addConInit();
													layer.close(i);
													$('#table').jqGrid('setRowData', id, {
														workerId:id,
														workerName: name,
														workerPhone:phone,
														regionId: regionId,
														regionName:regionName,
														skillKind:skillKind,
														avatarUrl:imgUrl,
														workerStatus:datas.workerStatus,
														isInservice:datas.isInservice,
														skillsLabel:skillsLabel || 0,
														cardId:cardId
													});
													id=null;
												}
												else if(result.result_code == 6011){
													utils.showMessage(result.description);
												}
												else {
													let msg=act==1?'新增失败，请稍后重试！':'修改失败，请稍后重试！';
													utils.showLayerMessage(msg);
												}
											});
										},
										function(){
											layer.close(index);
											// 获取数据
											getRepairmanListData.workerId=id;
											repairman.getServerData(nowPage, getRepairmanListData,function (result) {
												// $('#name').val(result.workerInfoVOS[0].workerName);
												$('#phone').val(result.workerInfoVOS[0].workerPhone);
												$('#phone').attr('disabled','disabled');//手机号不可修改

												$('#city').val(result.workerInfoVOS[0].regionId);
												$('#city').find('option[text='+result.workerInfoVOS[0].regionName+']').attr('selected',true);

												$('#urgencyPhone').val(result.workerInfoVOS[0].urgencyPhone);
												$('#urgencyName').val(result.workerInfoVOS[0].urgencyName);

												if(result.workerInfoVOS[0].skillKind==1){
													$('#all').iCheck('check');
												}
												else if(result.workerInfoVOS[0].skillKind==2){
													$('#ele').iCheck('check');
												}
												else if(result.workerInfoVOS[0].skillKind==3){
													$('#all').iCheck('check');
													$('#ele').iCheck('check');
												}

												$('#uploadImgBox').find('img').attr('src',result.workerInfoVOS[0].avatarUrl);

												$('#isInService').val(result.workerInfoVOS[0].isInservice);


												oldSkillKind=result.workerInfoVOS[0].skillKind;
												oldCity=result.workerInfoVOS[0].regionId;
											});
										}
									);
									return;
								}
								if(skillKind==2 && skillKind!==oldSkillKind){
									layer.confirm(
										'您修改了该维修员的技能分类，修改后该维修员当前负责区域全部清空。是否确认修改？',
										function(index) {
											layer.close(index);
											datas.workerId=id;
											datas.isInservice=Number($('#isInService').val());
											datas.skillsLabel=skillsLabel;
											if(datas.isInservice==2){
												datas.workerStatus=2;
											}
											win.utils.ajaxPost(win.utils.services.updateWorker, datas, function(result) {
												if(result.result_code == 200) {
													utils.showMessage('修改成功！');
													repairman.addConInit();
													layer.close(i);
													// repairman.refreshServerData();
													$('#table').jqGrid('setRowData', id, {
														workerId:id,
														workerName: name,
														workerPhone:phone,
														regionId: regionId,
														regionName:regionName,
														skillKind:skillKind,
														avatarUrl:imgUrl,
														workerStatus:datas.workerStatus,
														isInservice:datas.isInservice,
														urgentPhone: urgencyPhone,
														urgentContacts: urgencyName,
														skillsLabel:skillsLabel||0,
														cardId:cardId
													});
													id=null;
												}
												else if(result.result_code == 6011){
													utils.showMessage(result.description);
												}
												else {
													let msg=act==1?'新增失败，请稍后重试！':'修改失败，请稍后重试！';
													utils.showLayerMessage(msg);
												}
											});
										},
										function(){
											layer.close(index);
											// 获取数据
											getRepairmanListData.workerId=id;
											repairman.getServerData(nowPage, getRepairmanListData,function (result) {
												$('#name').val(result.workerInfoVOS[0].workerName);
												$('#phone').val(result.workerInfoVOS[0].workerPhone);
												$('#phone').attr('disabled','disabled');//手机号不可修改

												$('#city').val(result.workerInfoVOS[0].regionId);
												$('#city').find('option[text='+result.workerInfoVOS[0].regionName+']').attr('selected',true);

												if(result.workerInfoVOS[0].skillKind==1){
													$('#all').iCheck('check');
												}
												else if(result.workerInfoVOS[0].skillKind==2){
													$('#ele').iCheck('check');
												}
												else if(result.workerInfoVOS[0].skillKind==3){
													$('#all').iCheck('check');
													$('#ele').iCheck('check');
												}

												$('#uploadImgBox').find('img').attr('src',result.workerInfoVOS[0].avatarUrl);

												$('#isInService').val(result.workerInfoVOS[0].isInservice);


												oldSkillKind=result.workerInfoVOS[0].skillKind;
												oldCity=result.workerInfoVOS[0].regionId;
											});
										}
									);
									return;
								}
								if(regionId!==oldCity){
									layer.confirm(
										'您修改了该维修员的城市，修改后该维修员当前负责区域全部清空。是否确认修改？',
										function(index) {
											layer.close(index);
											datas.workerId=id;
											datas.isInservice=Number($('#isInService').val());
											datas.skillsLabel=skillsLabel;
											if(datas.isInservice==2){
												datas.workerStatus=2;
											}
											win.utils.ajaxPost(win.utils.services.updateWorker, datas, function(result) {
												if(result.result_code == 200) {
													utils.showMessage('修改成功！');
													repairman.addConInit();
													layer.close(i);
													// repairman.refreshServerData();
													$('#table').jqGrid('setRowData', id, {
														workerId:id,
														workerName: name,
														workerPhone:phone,
														regionId: regionId,
														regionName:regionName,
														skillKind:skillKind,
														avatarUrl:imgUrl,
														workerStatus:datas.workerStatus,
														isInservice:datas.isInservice,
														urgentPhone: urgencyPhone,
														urgentContacts: urgencyName,
														skillsLabel:skillsLabel||0,
														cardId:cardId
													});
													id=null;
												}
												else if(result.result_code == 6011){
													utils.showMessage(result.description);
												}
												else {
													let msg=act==1?'新增失败，请稍后重试！':'修改失败，请稍后重试！';
													utils.showLayerMessage(msg);
												}
											});
										},
										function(index){
											regionId=oldCity;
											layer.close(index);
											// 获取数据
											getRepairmanListData.workerId=id;
											repairman.getServerData(nowPage, getRepairmanListData,function (result) {
												$('#name').val(result.workerInfoVOS[0].workerName);
												$('#phone').val(result.workerInfoVOS[0].workerPhone);
												$('#phone').attr('disabled','disabled');//手机号不可修改
												$('#city').val(result.workerInfoVOS[0].regionId);
												$('#city').find('option[text='+result.workerInfoVOS[0].regionName+']').attr('selected',true);

												if(result.workerInfoVOS[0].skillKind==1){
													$('#all').iCheck('check');
												}
												else if(result.workerInfoVOS[0].skillKind==2){
													$('#ele').iCheck('check');
												}
												else if(result.workerInfoVOS[0].skillKind==3){
													$('#all').iCheck('check');
													$('#ele').iCheck('check');
												}

												$('#uploadImgBox').find('img').attr('src',result.workerInfoVOS[0].avatarUrl);

												$('#isInService').val(result.workerInfoVOS[0].isInservice);

												oldSkillKind=result.workerInfoVOS[0].skillKind;
												oldCity=result.workerInfoVOS[0].regionId;
											});
										}
									);
									return;
								}

								datas.workerId=id;
								datas.isInservice=Number($('#isInService').val());
								datas.skillsLabel=skillsLabel;
								if(datas.isInservice==2){
									datas.workerStatus=2;
								}
								datas.regionName=regionName;

								win.utils.ajaxPost(win.utils.services.updateWorker, datas, function(result) {
									if(result.result_code == 200) {
										$('#table').jqGrid('setRowData', id, {
											workerId:id,
											workerName: name,
											workerPhone:phone,
											regionId: regionId,
											regionName:regionName,
											skillKind:skillKind,
											avatarUrl:imgUrl,
											workerStatus:datas.workerStatus,
											isInservice:datas.isInservice,
											urgentPhone: urgencyPhone,
											urgentContacts: urgencyName,
											skillsLabel:skillsLabel||0,
											cardId:cardId
										});

										utils.showMessage('修改成功！');
										repairman.addConInit();
										layer.close(i);
										id=null;
									}
									else if(result.result_code == 6011){
										utils.showMessage(result.description);
									}
									else {
										utils.showMessage('修改失败，请重试！');
									}
								});

							}
						}

					}
					else{
						utils.showMessage('请先填写正确格式的信息！');
					}
					//
					// $('#phone').off('input');
					// $('#phone').off('blur');
				},
                btn2:function () {
				    repairman.addConInit();
					id=null;
                },
                cancel:function(){
                    repairman.addConInit();
					id=null;
                }
			});
		},
		// 获取银行卡信息
		getCardInfo:function(){

			var isCardInfoFlage=false; //银行卡是否完善标识  true: 有未完善信息   false：无未完善信息

			var WorkerBankInfoVO=[]; //银行卡信息集合

			var cardsNode=$('.add-card-item');
			if(cardsNode.length){
				for(var i=0;i<cardsNode.length;i++){
					var cardInfo={}; //单张银行卡信息

					var bankCard=$(cardsNode[i]).find('.bankCard').val(); //银行卡号
					var cardholder=$(cardsNode[i]).find('.cardholder').val(); //持卡人姓名
					var cardId=$(cardsNode[i]).find('.cardId').val(); //持卡人身份证号
					var bank=$(cardsNode[i]).find('.bank').val(); //开户银行名称
					var bankBranch=$(cardsNode[i]).find('.bankBranch').val(); //开户分行
					var bankfiliale=$(cardsNode[i]).find('.bankfiliale').val(); //开户支行
					var city=$(cardsNode[i]).find('input.city').val(); //开户城市

					if(!bankCard){
						isCardInfoFlage=true;
						utils.showMessage('请输入第'+(i+1)+'张银行卡卡号！');
						return;
					}
					if(!cardholder){
						isCardInfoFlage=true;
						utils.showMessage('请输入第'+(i+1)+'张银行卡持卡人姓名！');
						return;
					}
					if(!cardId){
						isCardInfoFlage=true;
						utils.showMessage('请输入第'+(i+1)+'张银行卡持卡人身份证号！');
						return;
					}
					if(!bank){
						isCardInfoFlage=true;
						utils.showMessage('请输入第'+(i+1)+'张银行卡开户银行！');
						return;
					}
					if(!bankBranch){
						isCardInfoFlage=true;
						utils.showMessage('请输入第'+(i+1)+'张银行卡开户分行！');
						return;
					}
					if(!bankfiliale){
						isCardInfoFlage=true;
						utils.showMessage('请输入第'+(i+1)+'张银行卡开户支行！');
						return;
					}
					if(!city){
						isCardInfoFlage=true;
						utils.showMessage('请输入第'+(i+1)+'张银行卡开卡城市！');
						return;
					}

					cardInfo.bankCard=bankCard;
					cardInfo.cardholder=cardholder;
					cardInfo.cardId=cardId;
					cardInfo.bank=bank;
					cardInfo.bankBranch=bankBranch;
					cardInfo.bankfiliale=bankfiliale;
					cardInfo.city=city;

					WorkerBankInfoVO.push(cardInfo);
				}
				return {"isCardInfoFlage":isCardInfoFlage,"WorkerBankInfoVO":WorkerBankInfoVO}
			}
			else{
                return {"isCardInfoFlage":false,"WorkerBankInfoVO":[]}
            }
		},
		canOutInservice:function(id){
			$('#isInService').off('change').on('change',function(){
				if(id){
					var rowData = $('#table').jqGrid('getRowData', id);
					console.log(rowData)
					var unfinishOrder=rowData.statistic.split('/')[1];

					var isInservice = $('#isInService').val();
					if(isInservice==2 && unfinishOrder>0){
						utils.showMessage('有订单未完成，不可禁用！');
						$('#isInService').val('1');
						$('#isInService').find("option[text='启用']").attr('selected',true);
					}
				}
			});
		},
		// 正则验证——校验手机号正确性
		checkMobile:function(s){
			var regu =/^[1][3-9][0-9]{9}$/;
			var re = new RegExp(regu);
			if (re.test(s)) {
				return true;
			}else{
				return false;
			}
		},
		checkName: function(name){
			var regu ="^[\u4e00-\u9fa5]+$";
			var re = new RegExp(regu);
			if (re.test(name)) {
				return true;
			}else{
				return false;
			}
		},
        // 校验身份证号
        checkCardId:function(cardId){
            // var regu ="/(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)/";
			var regu = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
			var re = new RegExp(regu);
            if (re.test(cardId)) {
                return true;
            }else{
                return false;
            }
        },
		/**-----------------新增维修员相关方法结束-------------------------------------------*/

        getAreaData: function(data, fn){
            win.utils.ajaxPost(win.utils.services.getSysArea, data, fn);
        },
		// 获取城市数据
		getCityData: function(data, fn){
			win.utils.ajaxPost(win.utils.services.queryUserCity, data, fn);
		},


		// 选中列表某一行,渲染区域详情部分
		rowOption: function (index) {
		$('#contanerDetail>div').hide().eq(0).show();
		$('.detail').show();

			var id=$('#table').jqGrid('getGridParam','selrow');
			// repairman.getLeaveRecord(id);
			var btn_index=$('#AllBtnAction .active').index();

			switch (btn_index) {
				case 0:
					repairman.getLeaveRecord(id);
					break;
				case 1:
					repairman.queryBankInfo(id);
					break;
				case 2:
					repairman.getOptionRecord(id);
					break;
			}
			// 对应列表切换
			$('#contanerDetail>div').hide().eq(btn_index).show();

			// 详情区域按钮
			$('#AllBtnAction button').off('click').on('click',function () {
				// 详情区域滚动
				repairman.detailScroll($('.detail').offset().top);

				var btn_index=$(this).index();

				switch (btn_index) {
					case 0:
						repairman.getLeaveRecord(id);
						break;
					case 1:
						repairman.queryBankInfo(id);
						break;
					case 2:
						repairman.getOptionRecord(id);
						break;
				}
				// 按钮切换
				$('#AllBtnAction button').removeClass('active').eq(btn_index).addClass('active');

				// 对应列表切换
				$('#contanerDetail>div').hide().eq(btn_index).show();
			});
		},
		// 获取请假记录
		getLeaveRecord: function(workerId){
			win.utils.ajaxPost(win.utils.services.getWorkerLeaveList,{
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				workerId: workerId,
			},function(result){
				if(result.result_code==200){
					var leaveGrideData=result.workerLeaveRecords;
					var gridJson = {
						rows: leaveGrideData,
					};
					$("#leaveRecordTable")[0].addJSONData(gridJson);

					$('.leaveTbaleBox').show();
				}
			});
		},
		// 获取操作记录
		getOptionRecord: function(workerId){
			win.utils.ajaxPost(win.utils.services.queryWorkerLog,{
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				workerId: workerId,
			},function(result){
				if(result.result_code==200){
					var optionGrideData=result.workerLogs;
					var optionGridJson = {
						rows: optionGrideData
					};
					$("#optionRecordTable")[0].addJSONData(optionGridJson);
				}
			});
		},
		// 获取银行卡信息
		queryBankInfo:function(workerId){
			win.utils.ajaxGet(win.utils.services.queryBankInfo,{
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				workerId: workerId,
			},function(result){
				if(result.result_code==200){
					var cardGrideData=result.data;
					var cardGridJson = {
						rows: cardGrideData
					};
					$("#cardInfoTable")[0].addJSONData(cardGridJson);
				}
			});
		},

		// 详情区域按钮点击页面滚动
		detailScroll:function(h){
			$('body,html').animate({
				scrollTop:h
			},300)
		},

        // 时间戳转时间格式
        timeStampFormat: function (timestamp) {
            var date = new Date(timestamp);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
            h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y + M + D + h + m + s;
        },

		leaveOk:function(obj)  {
            utils.showMessage('请假成功！');
        	$('#table').jqGrid('setRowData',obj.workerId,{
				workerStatus:2,
				isInservice:1,
				workerId:obj.workerId
			});
		},
	};

	win.repairman = repairman;
})(window, jQuery, qiniu);
