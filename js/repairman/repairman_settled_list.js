/**
 * created by hanlu
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    var nowPage = 1;

    var postData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        startCreateTime: null,
        endCreateTime: null, //申请结束时间
        startWorkYears: null,
        endWorkYears: null,
        workYears: null,
        isElectrician: null,
        isWorkHeight: null,
        name: null,
        phone: null,
        skillKind: null,
        position: nowPage,
        rows: 20,
    };

    var flagInput = false;
    var is_search = false;

    var repairman = {
        //初始化
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            //判断是否有维修员申请入驻列表权限，此时为维修员管理的维修员申请入驻列表权限
            utils.isUserMenuIndexRight('repairman_settled_list', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });
            // 搜索
            $('#search').on('click', function () {
                repairman.searchWorkOrder();
            });
            // 刷新
            $('#btnRefresh').on('click', function () {
                repairman.refreshServerData();
            });
            // 通过申请
            $('#table').on('click', '#pass', function () {
                var index = $(this).parents('tr').index() - 1;
                var updateData = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    id: gridData[index].id,
                    isPass: 1
                }
                repairman.changeApplyStatus(updateData);
            });
            // 驳回申请
            $('#table').on('click', '#reject', function () {
                var index = $(this).parents('tr').index() - 1;
                var updateData = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    id: gridData[index].id,
                    isPass: 2
                }
                repairman.changeApplyStatus(updateData);
            });
            obj.getServerData(1, postData);

            $(document).keydown(function (event) {
                var e = window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if (flagInput == false) {
                        if (e.preventDefault) {
                            e.preventDefault();
                        } else {
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });
        },
        //初始化请求
        initList: function () {
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                shrinkToFit: false,
                autoScroll: true,
                autowidth: false,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [10,20,50,100],
                pgtext: "第X页 / 共X页",
                colNames: ["ID", "操作", "联系人", "手机号", "城市", "维修种类", "电工证", "高空作业证", "工作年限", "我的特长", "申请时间"],
                colModel: [
                    {
                        name: "id",
                        index: "id",
                        editable: false,
                        width: 90,
                        sorttype: "string",
                        hidden: true
                    },
                    {
                        name: "operation",
                        index: "operation",
                        editable: false,
                        width: 180,
                        sortable: false,
                        formatter: repairman.opertionText
                    },
                    {
                        name: "name",
                        index: "name",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    {
                        name: "phone",
                        index: "phone",
                        editable: false,
                        width: 120,
                        sortable: false
                    },
                    {
                        name: "workCity",
                        index: "workCity",
                        editable: false,
                        width: 100,
                        sortable: false
                    },
                    {
                        name: "skillKind",
                        index: "skillKind",
                        editable: false,
                        width: 150,
                        sortable: false
                    },
                    {
                        name: "isElectrician",
                        index: "isElectrician",
                        editable: false,
                        width: 150,
                        sortable: false,
                        formatter: repairman.isElectrician
                    },
                    {
                        name: "isWorkHeight",
                        index: "isWorkHeight",
                        editable: false,
                        width: 150,
                        sortable: false,
                        formatter: repairman.isWorkHeight
                    },
                    {
                        name: "workYears",
                        index: "workYears",
                        editable: false,
                        width: 130,
                        sorttype: "string"
                    },
                    {
                        name: "speciality",
                        index: "speciality",
                        editable: false,
                        width: 240,
                        sortable: false
                    },
                    {
                        name: "createTime",
                        index: "createTime",
                        editable: false,
                        width: 180,
                        sorttype: "string",
                        formatter: repairman.getTime
                    },

                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        postData.position = nowPage;
                        repairman.searchFn();
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        nowPage = currentPage;
                        postData.position = nowPage;
                        repairman.searchFn();
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    postData.position = nowPage;
                    repairman.searchFn();
                },
                //当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowIndex = $("#" + this.id).getGridParam('selrow');
                    //返回点击这行xlmc的值
                    // selectedRowValue = $("#gridTable").jqGridRowValue("xlmc");
                    // contanerDetail
                    let id = parseInt(selectedRowIndex);
                    selectId = id;
                },
            });
            repairman.getServerData(1, postData);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            var b = $(".jqGrid_wrapper").width();
            $("#table").setGridWidth(b);
            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        // 刷新
        refreshServerData: function () {
            repairman.initSearch();
            nowPage = 1;
            is_search = false;
            postData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                startCreateTime: null,
                endCreateTime: null, //申请结束时间
                startWorkYears: null,
                endWorkYears: null,
                workYears: null,
                isElectrician: null,
                isWorkHeight: null,
                name: null,
                phone: null,
                skillKind: null,
                position: nowPage,
                rows: 20,
            };
            repairman.getServerData(nowPage, postData);
        },
        initSearch: function () {
            $("#name").val("");
            $("#phone").val("");
            $("#workYears").val("");
            $("#startCreateTime").val("");
            $("#endCreateTime").val("");
            $("#skillKind").val("");
            $("#isElectrician").val("");
            $("#isWorkHeight").val("");
        },
        getServerData: function (pageIndex, postData) {
            is_search = false;
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.getEnterApply, postData, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                        utils.showMessage('暂无数据！');
                    } else {
                        gridData = result.workerEnterApplies;
                        selectId = gridData[0].id;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 搜索
        searchWorkOrder: function () {
            repairman.searchLayerInit();
            let arr = ['搜索', '取消'];
            var i = layer.open({
                type: 1,
                area: ['60%', '80%'],
                title: '搜索入驻维修员',
                content: $('#SearchCon'),
                btn: arr,
                success: function () {
                    flagInput = false;
                },
                yes: function (index, layero) {
                    nowPage = 1;
                    is_search = true;

                    // if(is_search){
                    //     var createTime = repairman.dateFormat($('#timeChoose').val()) || null;
                    //
                    //     postData.name = $('#name').val() || null;
                    //     postData.phone = $('#phone').val() || null;
                    //     postData.workYears = $('#workYears').val() || null;
                    //     postData.startCreateTime = createTime ? createTime[0]+' 00:00:00' : null;
                    //     postData.endCreateTime = createTime ? createTime[1]+' 23:59:59' : null;
                    //     postData.skillKind = $('#skillKind').val() || null;
                    //     postData.isElectrician = $('#isElectrician').val() || null;
                    //     postData.isWorkHeight = $('#isWorkHeight').val() || null;
                    //     postData.position = nowPage;
                    // }
                    //
                    // repairman.getServerData(nowPage, postData);
                    repairman.searchFn();
                    layer.close(i);
                },
                btn2: function () {
                    is_search = false;
                },
                cancel: function () {
                    is_search = false;
                }
            });
        },
        searchFn: function () {
            if (is_search) {
                var createTime = repairman.dateFormat($('#timeChoose').val()) || null;

                postData.name = $('#name').val() || null;
                postData.phone = $('#phone').val() || null;
                postData.workYears = $('#workYears').val() || null;
                postData.startCreateTime = createTime ? createTime[0] + ' 00:00:00' : null;
                postData.endCreateTime = createTime ? createTime[1] + ' 23:59:59' : null;
                postData.skillKind = $('#skillKind').val() || null;
                postData.isElectrician = $('#isElectrician').val() || null;
                postData.isWorkHeight = $('#isWorkHeight').val() || null;
                postData.position = nowPage;
            }

            repairman.getServerData(nowPage, postData);
        },
        // 弹层重置
        searchLayerInit: function () {
            $('#name').val('');
            $('#phone').val('');
            $('#workYears').val('');
            $('#timeChoose').val('');
            $('#skillKind').val('');
            $('#isElectrician').val('');
            $('#isWorkHeight').val('');
        },
        // 修改申请状态
        changeApplyStatus: function (updateData) {
            win.utils.ajaxPost(win.utils.services.updateEnterApply, updateData, function (result) {
                console.log(result);
                if (result.result_code == 200) {
                    repairman.refreshServerData();
                }
            })
        },
        // 是否有电工证
        isElectrician: function (cellvalue, options, rowObject) {
            if (rowObject.isElectrician == 1) {
                return '拥有'
            } else if (rowObject.isElectrician == 2) {
                return '没有'
            } else {
                return ''
            }
        },
        // 是否有高空作业证
        isWorkHeight: function (cellvalue, options, rowObject) {
            if (rowObject.isWorkHeight == 1) {
                return '拥有'
            } else if (rowObject.isWorkHeight == 2) {
                return '没有'
            } else {
                return ''
            }
        },
        // 申请列表操作
        opertionText: function (cellvalue, options, rowObject) {
            if (rowObject.isPass) {
                if (rowObject.isPass == 1) {
                    return '申请已通过'
                }
                if (rowObject.isPass == 2) {
                    return '申请已被驳回'
                }
            } else {
                return '<button type="button" class="btn btn-info opertion unified-btn-inline unified-change-btn-inline" id="pass" style="margin-right: 20px">通过</button>' +
                    '<button type="button" class="btn opertion unified-btn-inline unified-deleted-btn-inline" id="reject">驳回</button>'
            }

        },
        // 列表日期显示转换
        getTime: function (cellvalue, options, rowObject) {
            return repairman.timeStampFormat(rowObject.createTime);
        },
        // 时间戳转时间格式
        timeStampFormat: function (timestamp) {
            var date = new Date(timestamp);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
            h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y + M + D + h + m + s;
        },
        // 时间转时间戳
        dateToTimeStamp: function (date) {
            if (date) {
                var date = new Date(date);
                return date.getTime(date);
            } else {
                return null
            }
        },
        // 日期格式处理
        dateFormat: function (dataStr) {
            if (dataStr) {
                var startCreateTime = dataStr.split(' - ')[0];
                var endCreateTime = dataStr.split(' - ')[1];
                startCreateTime = repairman.datePartFormat(startCreateTime);
                endCreateTime = repairman.datePartFormat(endCreateTime);
                return [startCreateTime, endCreateTime];
            } else {
                return null
            }
        },
        datePartFormat: function (date) {
            // return date.split(' ')[0].split('-').join('')+' '+date.split(' ')[1];
            return date.split('-').join('');
        }
    };

    win.repairman = repairman;
})(window, jQuery);
