/**
 * Created by song on 2019-04-17.
 * modified by hanlu
 */
(function(win, $, qiniu) {
	//延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
	var deferLoadData = 0;
	var orderByString = '';
	//分页-当前页---当前条数
	var nowPage=1;
	// 维修员列表当前选择id
	var selectedRowIndex;
	var chooseArea=[]; //提交时添加的区域
	var tableArr=[];
	var villageIndex=0;//显示选择结果区域——小区字符串插入li的index
	var getRepairmanListData={
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        name:"",
        status:null,
		isQueryFinishNum:1,
        position: nowPage,
        rows: 10
	};
	var data={
		user_id: win.utils.getCookie('user_id'),
		session: win.utils.getCookie('session'),
		workerId:null,
		workerName:null,
		regionId: null,
		skillKind:null,
		districtId:null,
		districtCbdId:null,
		blockId:null,
		workerStatus:null,
		isInservice:null,
		position: nowPage,
		rows: 10,
		isQueryFinishNum:1,
		skillsLabel:null
	};

	var is_search=false; //搜索标识

    var flagInput=false; //

	var repairman = {
		//初始化
		init: function()  {
			var obj = this;
			if(utils.isLogin() == false) {
				utils.loginDomain();
				return;
			}
			//判断是否有维修员权限，此时为维修员管理的维修员列表管理专用权限
			utils.isUserMenuIndexRight('special_maintainer_list', function(result) {
				if(result == 1) {
					//initialize...
					win.utils.clearSpaces();
					obj.initList();
				} else {
					self.location = '../../../web/error/access_refuse.html';
					return;
				}
			});
			
			// 搜索条件处理
			var postData = {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				userId: win.utils.getCookie('user_id')
			};
			repairman.getCityData(postData, function (result) {
				var optionStr='<option value="">请选择城市</option>';
				result.sysUserCityVOS.forEach(function (i,k) {
					optionStr+='<option value="'+i.cityId+'">'+i.addressName+'</option>'
				});
				$('#search_city').html(optionStr);
				$('#search_city').find('option').eq(0).attr("selected",true);
			});
			var cityId, cityName;
			$('#search_city').off('change').on('change', function (e) {
				cityId=e.target.value;
				cityName=$('#search_city').find('option:selected').text();
			
				$('.search_zoning').hide();
				$('.search_tradingArea').hide();
				$('.search_village').hide();

				$('#search_zoning').val('');
				$('#search_tradingArea').val('');
				$('#search_village').val('');

				if(cityId){
					postData.levelFlag=2;
					postData.cityId=cityId;
					repairman.getAreaData(postData, function (result) {
						var optionStr='<option value="">请选择区域</option>';
						result.areaVOList.forEach(function (i,k) {
							optionStr+='<option value="'+i.id+'">'+i.addressName+'</option>'
						});
						$('#search_zoning').html(optionStr);
						layui.use('form',function () {
							var form = layui.form;
							form.render('select');
						})
						$('#search_zoning').find('option').eq(0).attr("selected",true);
						$('.search_zoning').show();
					});
				}
			});
			
			// 搜索区域
			$('#search_zoning').off('click').on('click', function () {
				if(!cityId){
					utils.showMessage('请选择城市！');
				}
			});
			var zoningId,zoningName;
			$('#search_zoning').off('change').on('change',function (e) {
				zoningId=e.target.value;
				zoningName=$('#search_zoning').find('option').eq(e.target.selectedIndex).text();
			
				$('.search_tradingArea').hide();
				$('.search_village').hide();

				$('#search_tradingArea').val('');
				$('#search_village').val('');
			
				if(zoningId){
					postData.levelFlag=3;
					postData.cityId=null;
					postData.districtId=zoningId;
					repairman.getAreaData(postData, function (result) {
						var optionStr='<option value="">请选择商圈</option>';
						result.areaVOList.forEach(function (i,k) {
							optionStr+='<option value="'+i.id+'">'+i.addressName+'</option>'
						});
						$('#search_tradingArea').html(optionStr);
						layui.use('form',function () {
							var form = layui.form;
							form.render('select');
						})
						$('#search_tradingArea').find('option').eq(0).attr("selected",true);
			
						$('.search_tradingArea').show();
					});
				}
			});
			
			// 搜索商圈
			$('#search_tradingArea').off('click').on('click', function () {
				if(!zoningId){
					utils.showMessage('请选择区域！');
				}
			});
			var tradingId,tradingName;
			layui.use('form',function(){
				var form = layui.form;
				form.on('select(search_tradingArea)', function(data){
					if(data.value){
						tradingId=data.value;
						tradingName=$('#search_tradingArea').find('option:selected').text();
			
						$('.search_village').hide();

						$('#search_village').val('');
			
						if(tradingId){
							postData.levelFlag=4;
							postData.cityId=null;
							postData.districtId=null;
							postData.businessDistrictId=tradingId;
							repairman.getAreaData(postData, function (result) {
								var optionStr='<option value="">请选择小区</option>';
								result.areaVOList.forEach(function (i,k) {
									optionStr+='<option value="'+i.id+'">'+i.addressName+'</option>'
								});
								$('#search_village').html(optionStr);
								layui.use('form',function () {
									var form = layui.form;
									form.render('select');
								})
								$('#search_village').find('option').eq(0).attr("selected",true);
			
								$('.search_village').show();
							});
						}
					}
					else{
						$('.search_village').hide();
					}
				});
			});
			// 搜索小区
			var villageId,villageName;
			$('#search_village').off('change').on('change',function (e) {
				villageId=e.target.value;
				villageName=$('#search_village').find('option').eq(e.target.selectedIndex).text();
			});
			
			// 搜索
			$('#search').on('click',function(){
				$('.detail').hide();
				is_search=true;
				nowPage=1;
				repairman.searchFn();
			});

			//搜索框---enter键搜索
			$('#orderList').on('keyup','input', function(event) {
				if (event.keyCode == "13") {
					$('.detail').hide();
					is_search=true;
					nowPage=1;
					repairman.searchFn();
				}
			});

			//刷新
			$('#btnRefresh').on('click', function() {
				is_search=false;
				nowPage=1;
				obj.refreshServerData();
			});
			//新增
			$('#btnAdd').on('click',function(){
				repairman.addConInit();
				repairman.addWorkOrder(1)
			});
			// 维修员请假销假
			$('#table').off('click').on('click','#changeReceiptStatus',function (){
                var id=$(this).attr('data-id');
                var workerStatus=$(this).attr('data-status');
                var rowData=$('#table').jqGrid('getRowData',id);

                var workerInfo={
                	"id":id,
					"workerName":rowData.workerName,
					"workerStatus":workerStatus,
					"workerPhone":rowData.workerPhone
				};
				repairman.changeReceiptStatus(workerInfo);
			});
            //修改维修员
            $('#table').on('click', '#btnUpdate', function(){
				var id=$(this).attr('data-id');
                repairman.addConInit();
                repairman.addWorkOrder(2, id);
            });
			// 图片上传
			$('#btnCardUpload').click(obj.uploadCardImage);
			$('#btnCardDelete').click(obj.deleteCardImage);


			// 维修员列表查看服务区域
			$('#table').on('click','a',function () {
				var id=$(this).attr('data-id');

				var href=$('.nav .J_menuItem[data-id=21 ]',parent.document).attr('href');

				var repairmanInfo={
					workerId:id,
					type:1
				};
				win.localStorage.setItem("repairmanInfo",JSON.stringify(repairmanInfo));
				parent.$(window.parent.document).find('.nav .J_menuItem[data-id=21]').click();
				parent.index.getParamsToChild(repairmanInfo);

			});


			$('#areasCon').find('.form-group').hide();

			// 添加银行卡
            $('.add-card-btn').on('click',function(){
                var num=$('.add-card-item').length;

                var cardDomStr='<div class="add-card-item" data-id="'+num+'">\n' +
                    '\t\t\t\t\t\t<div class="card-tit">\n' +
                    '\t\t\t\t\t\t\t<span>银行卡-'+(num+1)+'</span>\n' +
                    '\t\t\t\t\t\t\t<span></span>\n' +
                    '\t\t\t\t\t\t\t<button type="button" class="btn deleteCardBtn" data-index="'+num+'">删除</button>\n' +
                    '\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t<div class="card-info">\n' +
                    '\t\t\t\t\t\t\t<div class="form-horizontal">\n' +
                    '\t\t\t\t\t\t\t\t<!--银行卡号-->\n' +
                    '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                    '\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>银行卡号</label>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="bankCard" type="number" class="bankCard form-control" placeholder="请输入银行卡号" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label" style="text-align: center; color: rgba(107,111,129,1); ">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"></label>\n' +
                    '\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6 bankcardtip">（必须是维修员本人的银行卡号）</div>\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t<!--持卡人姓名-->\n' +
                    '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                    '\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>持卡人姓名</label>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="cardholder" type="text" class="cardholder form-control" placeholder="请输入持卡人姓名" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-3 control-label checkcardholderCon" style="text-align: left; color: red; display: none;">姓名不符合规范</div>\n' +
                    '\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t<!--持卡人身份证号-->\n' +
                    '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                    '\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>持卡人身份证号</label>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="cardId" type="text" class="cardId form-control" placeholder="请输入持卡人身份证号" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-3 control-label checkBankCardIdCon" style="text-align: left; color: red; display: none;">开户城市不符合规范</div>\n' +
                    '\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t<!--开户银行-->\n' +
                    '\t\t\t\t\t\t\t\t<div class="form-group">\n' +
                    '\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>开户银行</label>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-3">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="bank" type="text" class="bank form-control" placeholder="请输入开户银行名称" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-3">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="bankBranch" type="text" class="bankBranch form-control" placeholder="请输入开户分行" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-3">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="bankfiliale" type="text" class="bankfiliale form-control" placeholder="请输入开户支行" autocomplete="off">\n' +
					'\t\t\t\t\t\t\t\t\t</div>\n' +
					'\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label checkbankCon" style="text-align: left; color: red; display: none;"><label class="col-sm-3 control-label"></label><span>开户银行名称不符合规范</span></div>\n' +
					'\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label checkbankBranchCon" style="text-align: left; color: red; display: none;"><label class="col-sm-3 control-label"></label><span>开户分行不符合规范</span></div>\n' +
					'\t\t\t\t\t\t\t\t\t<div class="col-sm-12 control-label checkbankfilialeCon" style="text-align: left; color: red; display: none;"><label class="col-sm-3 control-label"></label><span>开户支行不符合规范</span></div>\n' +
                    '\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t\t<!--开户城市-->\n' +
                    '\t\t\t\t\t\t\t\t<div class="form-group city">\n' +
                    '\t\t\t\t\t\t\t\t\t<label class="col-sm-3 control-label"><strong class="red">*</strong>开户城市</label>\n' +
                    '\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n' +
                    '\t\t\t\t\t\t\t\t\t\t<input name="city" type="text" class="city form-control" placeholder="开户城市" autocomplete="off">\n' +
                    '\t\t\t\t\t\t\t\t\t</div>\n' +
					'\t\t\t\t\t\t\t\t\t<div class="col-sm-3 control-label checkcityCon" style="text-align: left; color: red; display: none;">开户城市不符合规范</div>\n' +
                    '\t\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t\t</div>\n' +
                    '\t\t\t\t\t</div>';
                $('.add-card-box').append(cardDomStr);

                // 第一张银行卡身份证号不可输入，且回显
				$("input[name='cardId']").eq(0).prop('readonly',true);
				// 银行卡提示文字
				$('.bankcardtip').text('(必须非维修员本人的银行卡)').eq(0).text('(必须为维修员本人的银行卡)');
				if($('#cardNum').val()){
					$("input[name='cardId']").eq(0).val($('#cardNum').val());
				}
            });

            // 删除银行卡
            $('.add-card-box').on('click','.deleteCardBtn',function(){
                var index = $(this).attr('data-index');

				$('.add-card-item').eq(index).remove(); //删除节点

				// 银行卡节点标题号、index重新赋值
				var cardInfoNodes=$('.add-card-item');
				for (var i=0;i<cardInfoNodes.length;i++){
					$(cardInfoNodes[i]).find('button').attr('data-index',i);
					var titleText=$(cardInfoNodes[i]).find('.card-tit').find('span').eq(0).text();
					$(cardInfoNodes[i]).find('.card-tit').find('span').eq(0).text(titleText.split('-')[0]+'-'+(i+1));
				}


                // 第一张银行卡身份证号不可输入，且回显
                $("input[name='cardId']").eq(0).prop('readonly',true);
                // 银行卡提示文字
                $('.bankcardtip').text('(必须非维修员本人的银行卡)').eq(0).text('(必须为维修员本人的银行卡)');
                if($('#cardNum').val()){
                    $("input[name='cardId']").eq(0).val($('#cardNum').val());
                }
            });


            $(document).keydown(function (event) {
                var  e=window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if(flagInput==false){
                        if(e.preventDefault){
                            e.preventDefault();
                        }else{
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });
		},
		//初始化请求
		initList: function(){
			$("#table").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height:'auto',
				autowidth: false,
				rowNum: 10,
				rownumbers: true,
				rownumWidth:60,
				// rowList: [20, 50, 100],
				pgtext: "第X页 / 共X页",
				colNames: ["id","操作","维修员工号", "姓名", "电话","城市", "技能种类","查看服务区域","接单状态","状态","完单量/未完单量","紧急联系人","紧急联系人电话"],
				colModel: [
					{
						name: "id",
						index: "id",
						editable: false,
						width: 40,
						sortable: false,
						formatter:function(cellvalue, options, rowObject){
							return rowObject.workerId
						},
						hidden:true
					},
					{
						name: "workerStatus",
						index: "workerStatus",
						editable: false,
						width: 120,
						sortable: false,
						formatter: tableOptions
					},
					{
						name: "workerId",
						index: "workerId",
						editable: false,
						width: 190,
						sortable: false,
					},
					{
						name: "workerName",
						index: "workerName",
						editable: false,
						width: 120,
						sortable: false
					},
					{
						name: "workerPhone",
						index: "workerPhone",
						editable: false,
						width: 120,
						sortable: false
					},
					{
						name: "regionName",
						index: "regionName",
						editable: false,
						width: 100,
						sortable: false
					},
					{
						name: "skillKind",
						index: "skillKind",
						editable: false,
						width: 150,
						sortable: false,
						formatter: function(cellvalue, options, rowObject) {
							if(rowObject.skillsLabel==1){
								if(rowObject.skillKind==1) return '综合维修，防水维修';
								else if(rowObject.skillKind==2) return '电器维修，防水维修';
								else if(rowObject.skillKind==3) return '综合维修，电器维修，防水维修'
								else if(rowObject.skillKind==0) return '防水维修'
							}
							else{
								if(rowObject.skillKind==1) return '综合维修';
								else if(rowObject.skillKind==2) return '电器维修';
								else if(rowObject.skillKind==3) return '综合维修，电器维修'
								else return ''
							}

						}
					},
					{
						name: "isInservice",
						index: "isInservice",
						editable: false,
						width: 100,
						sortable: false,
						formatter: function (cellvalue, options, rowObject) {
							if(rowObject.isInservice===1){
								return '<a id="checkArea" class="unified-a" href="###" data-id="'+rowObject.workerId+'">查看</a>'
							}
							else{
								return  '';
							}
						},
					},
					{
						name: "workerStatus",
						index: "workerStatus",
						editable: false,
						width: 180,
						sortable: false,
						formatter: getWorkerStatus
					},
					{
						name: "isInservice",
						index: "isInservice",
						editable: false,
						width: 180,
						sortable: false,
						formatter: isInservice
					},
					{
						name: "statistic",
						index: "statistic",
						editable: false,
						width: 180,
						sortable: false
					},
					{
						name: "urgentContacts",
						index: "urgentContacts",
						editable: false,
						width: 160,
						sortable: false
					},
					{
						name: "urgentPhone",
						index: "urgentPhone",
						editable: false,
						width: 180,
						sortable: false
					},
				],
				pager: "#table_pager",
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
				shrinkToFit: false,
				autoScroll:true,
				onPaging: function(pgButton) {
					deferLoadData = 0;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

					if(pgButton == 'next') {
						currentPage = currentPage + 1;
					}
					if(pgButton == 'last') {
						currentPage = lastPage;
					}
					if(pgButton == 'prev') {
						currentPage = currentPage - 1;
					}
					if(pgButton == 'first') {
						currentPage = 1;
					}
					if(pgButton == 'user') {
						deferLoadData = 1;
					}
					if(pgButton == 'records') {
						deferLoadData = 1;
					}
					nowPage=currentPage;
					if(deferLoadData == 0) {
						data.position=nowPage;
						repairman.searchFn();
					}
				},
				onSortCol: function(index, iCol, sortorder) {
					alert('onSortCol')
					orderByString = index + ' ' + sortorder;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					nowPage=currentPage;
					repairman.searchFn();
				},
				//当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
				onSelectRow: function () {
					//返回选中的id
					selectedRowIndex = $("#" + this.id).getGridParam('selrow');
					if(selectedRowIndex && selectedRowIndex!=''){
						repairman.rowOption(selectedRowIndex);
					};
					// console.log(selectedRowIndex);
				},
				gridComplete: function() {
					$("#table").jqGrid('resetSelection');
					$("#table").jqGrid('setSelection',selectedRowIndex);
				}
			});
			repairman.getServerData(1, getRepairmanListData);

			$("#table").jqGrid("navGrid", "#table_pager", {
				edit: false,
				add: false,
				del: false,
				search: false,
				refresh: false
			});


			function tableOptions(cellvalue, options, rowObject) {
				if(rowObject.isInservice==1){
					if(rowObject.workerStatus ==1){
						return '<button type="button" class="btn btn-info opertion unified-btn-inline unified-change-btn-inline" id="changeReceiptStatus"  data-id="'+rowObject.workerId+'" data-status="'+rowObject.workerStatus+'">请假</button>'
					}
					else if(rowObject.workerStatus ==2){
						return '<button type="button" class="btn btn-info opertion unified-btn-inline unified-publish-btn-inline" id="changeReceiptStatus"  data-id="'+rowObject.workerId+'" data-status="'+rowObject.workerStatus+'">销假</button>'
					}
				}else{
					return  ''
				}

			};
			function getWorkerStatus(cellvalue, options, rowObject){
				if(rowObject.workerStatus==1){
					return '接单';
				}
				else if(rowObject.workerStatus==2){
					return '不接单';
				}
			};
			function isInservice(cellvalue, options, rowObject){
				if(rowObject.isInservice==1){
					// return '在职';
					return '启用';
				}
				else if(rowObject.isInservice==2){
					// return '离职';
					return '禁用';
				}
			};


			// 请假记录表
			$("#leaveRecordTable").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height: 600,
				maxHeight:600,
				autowidth:false,
				shrinkToFit:false,
				autoScroll: true,
				rownumbers:true,
				rownumWidth:60,
				rowNum:-1,
				colNames: ["请假开始时间", "请假结束时间", "销假时间", "综合备用师傅", "电器备用师傅", "操作人", "操作时间"],
				colModel: [
					{
						name: "leaveStartTime",
						index: "leaveStartTime",
						editable: false,
						width: 180,
						sortable: false,
                        formatter:function (cellvalue, options, rowObject) {
							if(rowObject.leaveStartTime){
								var leaveStartTime = repairman.timeStampFormat(rowObject.leaveStartTime);
								return leaveStartTime;
							}
							else{
								return '';
							}
						}
					},
					{
						name: "leaveEndTime",
						index: "leaveEndTime",
						editable: false,
						width: 180,
						sortable: false,
						formatter:function (cellvalue, options, rowObject) {
							if(rowObject.leaveEndTime){
								var leaveEndTime = repairman.timeStampFormat(rowObject.leaveEndTime);
								return leaveEndTime;
							}
							else{
								return '';
							}
						}
					},
					{
						name: "removeLeaveTime",
						index: "removeLeaveTime",
						editable: false,
						width: 180,
						sortable: false,
						formatter:function (cellvalue, options, rowObject) {
							if(rowObject.removeLeaveTime){
								var removeLeaveTime = repairman.timeStampFormat(rowObject.removeLeaveTime);
								return removeLeaveTime;
							}
							else{
								return '';
							}
						}
					},
					{
						name: "synthesisSpare",
						index: "synthesisSpare",
						editable: false,
						width: 120,
						sortable: false,
					},
					{
						name: "electricSpare",
						index: "electricSpare",
						editable: false,
						width: 120,
						sortable: false,
					},
					{
						name: "createUser",
						index: "createUser",
						editable: false,
						width: 120,
						sortable: false,
					},
					{
						name: "createTime",
						index: "createTime",
						editable: false,
						width: 180,
						sortable: false,
						formatter:function (cellvalue, options, rowObject) {
							if(rowObject.createTime){
								var createTime = repairman.timeStampFormat(rowObject.createTime);
								return createTime;
							}
							else{
								return '';
							}
						}
					},
				],
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
			});

            // 银行卡信息
			$("#cardInfoTable").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height: 600,
				maxHeight:600,
				autowidth:false,
				shrinkToFit:false,
				autoScroll: true,
				rownumbers:true,
				rownumWidth:60,
				rowNum:-1,
				colNames: ["身份证姓名", "身份证号", "银行卡号", "开户银行", "开户城市", "操作人", "操作时间"],
				colModel: [
					{
						name: "cardholder",
						index: "cardholder",
						editable: false,
						width: 180,
						sortable: false,
					},
					{
						name: "cardId",
						index: "cardId",
						editable: false,
						width: 180,
						sortable: false,
					},
					{
						name: "bankCard",
						index: "bankCard",
						editable: false,
						width: 180,
						sortable: false,
					},
					{
						name: "bank",
						index: "bank",
						editable: false,
						width: 200,
						sortable: false,
						formatter:function (cellvalue, options, rowObject) {
							return rowObject.bank +'-'+rowObject.bankBranch+'-'+rowObject.bankfiliale
						}
					},
					{
						name: "city",
						index: "city",
						editable: false,
						width: 120,
						sortable: false,
					},
					{
						name: "createUser",
						index: "createUser",
						editable: false,
						width: 120,
						sortable: false,
					},
					{
						name: "createTime",
						index: "createTime",
						editable: false,
						width: 180,
						sortable: false,
						formatter:function (cellvalue, options, rowObject) {
							if(rowObject.createTime){
								var createTime = repairman.timeStampFormat(rowObject.createTime);
								return createTime;
							}
							else{
								return '';
							}
						}
					},
				],
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
			});

			// 操作记录表
			$("#optionRecordTable").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height:600,
				maxHeight:600,
				autowidth:false,
				shrinkToFit:false,
				autoScroll: true,
				rownumbers:true,
				rownumWidth:60,
				rowNum:-1,
				colNames: ["操作", "操作详情","操作时间"],
				colModel: [
					{
						name: "operationType",
						index: "operationType",
						editable: false,
						width: 160,
						sortable: false
					},
					{
						name: "operationDesc",
						index: "operationDesc",
						editable: false,
						width: 460,
						sortable: false
					},
                    {
                        name: "createTime",
                        index: "operationDesc",
                        editable: false,
                        width: 200,
                        sortable: false,
                        formatter:cearteTime
                    },
				],
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
			});

			function cearteTime(cellvalue, options, rowObject) {
                if(rowObject.createTime){
                    return repairman.timeStampFormat(rowObject.createTime)
                }
            }


			var b = $(".jqGrid_wrapper").width();
			$("#table").setGridWidth(b);
			$("#leaveRecordTable").setGridWidth(b);
			$("#optionRecordTable").setGridWidth(b);
			$('#cardInfoTable').setGridWidth(b);
			$(window).bind("resize", function () {
				var b = $(".jqGrid_wrapper").width();
				$("#table").setGridWidth(b);
				$("#leaveRecordTable").setGridWidth(b);
				$("#optionRecordTable").setGridWidth(b);
				$("#cardInfoTable").setGridWidth(b);
				// $("#table").setGridHeight(utils.getAutoGridHeight());
			});
		},
		// 刷新
		refreshServerData: function() {
			repairman.searchConInit();

			$('.search_zoning').hide();
			$('.search_tradingArea').hide();
			$('.search_village').hide();

			data={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				workerId:null,
				workerName:null,
				regionId: null,
				skillKind:null,
				districtId:null,
				districtCbdId:null,
				blockId:null,
				workerStatus:null,
				isInservice:null,
				isQueryFinishNum:1,
				position: nowPage,
				rows: 10
			};
			repairman.getServerData(nowPage, data);
			$('.detail').hide();
		},
		getServerData: function(pageIndex,postData,fn)  {
			is_search=false;
			pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
			win.utils.ajaxPost(win.utils.services.getWorker, postData, fn?fn:function(result) {
				selectedRowIndex='';//清空选中维修员id
				$('.detail').hide();//隐藏底部信息
				if(result.result_code == 200) {
					if(result.total == 0) {
						gridData = [];
						var gridJson = {
							total: 0,
							rows: gridData,
							page: 0,
							records: result.total
						};
						pageText = '第0页 / 共0页';
						$("#table").jqGrid('setGridParam', {
							pgtext: pageText
						});
						$("#table")[0].addJSONData(gridJson);
						utils.showMessage('暂无数据！');
					} else {
						result.workerInfoVOS.forEach(function (i,k) {
							i.id=i.workerId;
						});
						gridData = result.workerInfoVOS;
						selectedRowIndex = gridData[0].workerId;
						totalPages = Math.ceil(result.total / pageCount);
						var gridJson = {
							total: totalPages,
							rows: gridData,
							page: pageIndex,
							records: result.total
						};
						pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
						$("#table").jqGrid('setGridParam', {
							pgtext: pageText
						});
						$("#table")[0].addJSONData(gridJson);
					}
				} else {
					utils.showMessage('获取数据失败，请重试！');
				}
			});
		},
		// 搜索
		searchFn:function(){
			if(is_search){
				var workerId=$('#search_workerId').val()||null;
				var name = $('#search_name').val()||null;

				var receiptStatus = $('#search_receiptStatus').val();
				var isInService = $('#search_isInService').val();
				// var skillType=$('#skillTypeSearch').val();
				var phone=$('#search_phone').val();

				/**----------------处理技能分类开始--------------------------*/
				var skillTypeAll,skillTypeEle, skillKind, skillsLabel;
				if($('#search_all').prop('checked')) {
					skillTypeAll = 1;
				}else{
					skillTypeAll=0;
				}
				if($('#search_ele').prop('checked')) {
					skillTypeEle = 1;
				}else{
					skillTypeEle=0;
				}

				if($('#search_waterproof').prop('checked')){
					skillsLabel=1;
				}else{
					skillsLabel=null;
				}

				if(skillTypeAll==1 && skillTypeEle==1) {
					skillKind=3;
				}
				else if(skillTypeAll==1 && skillTypeEle!==1){
					skillKind=1;
				}
				else if(skillTypeAll!==1 && skillTypeEle==1){
					skillKind=2;
				}
				else if(skillTypeAll==0 && skillTypeEle==0){
					skillKind=null;
				}
				/**----------------处理技能分类结束--------------------------*/

				data.workerId = workerId;
				data.workerName = name;
				data.regionId = $('#search_city').val() || null;
				data.skillKind = skillKind;
				data.districtId = $('#search_zoning').val() || null;
				data.districtCbdId = $('#search_tradingArea').val()||null;
				data.blockId = $('#search_village').val() || null;
				data.workerStatus = receiptStatus?receiptStatus:null;
				data.isInservice = isInService?isInService:null;
				data.isQueryFinishNum=1;
				data.position = nowPage;
				data.skillsLabel = skillsLabel;
				data.phoneNum=phone;
			}

			repairman.getServerData(nowPage,data);
		},
		//图片上传
		loadLocalImage: function(fileObject, imageID, imageType, imgShowNode) {
			if (typeof FileReader == 'undefined') {
				alert("当前浏览器不支持FileReader接口");
				return;
			}
			var file = fileObject.files[0];
			let reader = new FileReader();//新建一个FileReader对象
			reader.readAsDataURL(file);//将读取的文件转换成base64格式
			reader.onload = function (e) {

				// 压缩图片
				var image = new Image()     //新建一个img标签（不嵌入DOM节点，仅做canvas操作)
				image.src = e.target.result    //让该标签加载base64格式的原图
				image.onload = function () {    //图片加载完毕后再通过canvas压缩图片，否则图片还没加载完就压缩，结果图片是全黑的
					let canvas = document.createElement('canvas'), //创建一个canvas元素
						context = canvas.getContext('2d'),    //context相当于画笔，里面有各种可以进行绘图的API
						imageWidth = image.width / 2,    //压缩后图片的宽度，这里设置为缩小一半
						imageHeight = image.height / 2,    //压缩后图片的高度，这里设置为缩小一半
						data = ''    //存储压缩后的图片
					canvas.width = imageWidth    //设置绘图的宽度
					canvas.height = imageHeight    //设置绘图的高度

					//使用drawImage重新设置img标签中的图片大小，实现压缩。drawImage方法的参数可以自行查阅W3C
					context.drawImage(image, 0, 0, imageWidth, imageHeight)

					//使用toDataURL将canvas上的图片转换为base64格式
					data = canvas.toDataURL('image/jpeg');
					var blobURL = repairman.dataURItoBlob(data);

					// 图片上传至七牛云
					repairman.qiniuUpload(
						blobURL,
						// file.name,
						'bjx-public-' + new Date().getTime() + (parseInt(Math.random() * 100 + 1)),
                        imgShowNode
					);
				}
			}
		},
		// base64转blob
		dataURItoBlob: function(base64Data) {
			var byteString;
			if(base64Data.split(',')[0].indexOf('base64') >= 0)
				byteString = atob(base64Data.split(',')[1]);
			else
				byteString = unescape(base64Data.split(',')[1]);
			var mimeString = base64Data.split(',')[0].split(':')[1].split(';')[0];
			var ia = new Uint8Array(byteString.length);
			for(var i = 0; i < byteString.length; i++) {
				ia[i] = byteString.charCodeAt(i);
			}
			return new Blob([ia], {
				type: mimeString
			});
		},
		/**七牛云上传图片
		 * cardImageUrl-----需要上传图片的bolb地址
		 * token-----token
		 * fileName-----上传图片的原文件名
		 * nodeObj------显示图片的盒子
		 * */
		qiniuUpload: function(cardImageUrl, fileName, nodeObj) {
			$.ajax({
				url: win.utils.services.getQiniuToken,
				type: 'get',
				dataType: 'json',
				success: function (res) {
					var qiniuToken = res.token;
					var observable = qiniu.upload(
						cardImageUrl,
						fileName,
						qiniuToken,
						{
							fname: fileName,
							params: {}, //用来放置自定义变量
							mimeType: null
						}, {
							useCdnDomain: true,

							region: null
						}
					);
					observable.subscribe({
						complete(res) {
							var qiniuImgUrl = 'http://images.baijiaxiu.com/'+res.key;
                            nodeObj.attr('src', qiniuImgUrl);
							utils.showMessage('图片上传成功！');
							$('input[type="file"]').val("");
						}
					});
				}
			});
		},

		// 增加修改搜索弹层初始化
		addConInit: function(){
			$('#workerId').val('');
			$('#name').val('');
			$('#phone').val('');
			$('#status').val();
			$('#all').iCheck('uncheck');
			$('#ele').iCheck('uncheck');
			$('#waterproof').iCheck('uncheck');
			$('#city').html('<option value="null" selected>请选择城市</option>');
			$('#zoning').html('<option value="null" selected>请选择区域</option>');
			$('#tradingArea').html('<option value="null" selected>请选择商圈</option>');
			$('#village').html('<option value="null" selected>请选择小区</option>');
			$('#skillTypeSearch').val('');
			$('#receiptStatus').val('');
			$('#isInService').val('');
			$('#uploadImgBox').find('img').attr('src','');
			$('.zoning').hide();
			$('.tradingArea').hide();
			$('.village').hide();
			$('#urgencyPhone').val('');
			$('#urgencyName').val('');

            $('#formAdd #cardNum').val(''); //身份证号
            $('#formAdd #cardUploadBtn1').find('img').attr('src','../../images/upload_card_img1.png'); //身份照片
            $('#formAdd #cardUploadBtn2').find('img').attr('src','../../images/upload_card_img2.png'); //身份照片
            $('.card-box .add-card-box').html(''); //银行卡集合

		},
		searchConInit:function(){
			$('#search_workerId').val('');
			$('#search_name').val('');
			$('#search_phone').val('');
			$('#search_city').val('');
			$('#search_zoning').val('');
			$('#search_tradingArea').val('');
			$('#search_village').val('');
			$('#search_receiptStatus').val('');
			$('#search_isInService').val('');
			$('#search_all').iCheck('uncheck');
			$('#search_ele').iCheck('uncheck');
			$('#search_waterproof').iCheck('uncheck');
			layui.use('form',function () {
				var form=layui.form;
				form.render('select');
			})
		},

		/**-----------------维修员请假/销假相关方法开始---------------------------*/
		// 维修员请假/休假
		changeReceiptStatus: function (obj)  {
			// 请假
			if(obj.workerStatus==1){
				// location.href=utils.domain_name+'web/areaManage/area_list.html?workerId='+obj.id+'/'+encodeURI(obj.workerName)+'/'+obj.workerPhone+'&type='+'2';
				var repairmanInfo={
					workerId:obj.id,
					workerName:obj.workerName,
					workerPhone:obj.workerPhone,
					type:2,
					source:37
				};
				win.localStorage.setItem("repairmanInfo",JSON.stringify(repairmanInfo));
				parent.$(window.parent.document).find('.nav .J_menuItem[data-id=21]').click();
				parent.index.getParamsToChild(repairmanInfo);
			}
			// 销假
			else if(obj.workerStatus==2){
				utils.showConfirm('是否确定销假？销假后维修员将重新接受服务区域的订单自动分派？', '确定', function() {
					win.utils.ajaxPost(win.utils.services.addCancelLeaveInfo, {
						user_id: win.utils.getCookie('user_id'),
						session: win.utils.getCookie('session'),
						workerId: obj.id,
						workerName:obj.workerName
					}, function(result) {
						if(result.result_code == 200) {
							utils.showMessage('销假成功！');
							utils.closeMessage();

							$('#table').jqGrid('setRowData',obj.id,{
								workerStatus:1,
								isInservice:1,
								workerId:obj.id
							});

						} else {
							utils.showMessage('销假失败，请重试！');
						}
					});
				});
			}
		},
		/**-----------------维修员请假/销假相关方法结束---------------------------*/

		/**-----------------新增维修员相关方法开始-------------------------------------------*/
		// 正则验证——校验手机号正确性
		checkMobile:function(s){
			var regu =/^[1][3-9][0-9]{9}$/;
			var re = new RegExp(regu);
			if (re.test(s)) {
				return true;
			}else{
				return false;
			}
		},
		checkName: function(name){
			var regu ="^[\u4e00-\u9fa5]+$";
			var re = new RegExp(regu);
			if (re.test(name)) {
				return true;
			}else{
				return false;
			}
		},
        // 校验身份证号
        checkCardId:function(cardId){
            // var regu ="/(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)/";
			var regu = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
			var re = new RegExp(regu);
            if (re.test(cardId)) {
                return true;
            }else{
                return false;
            }
        },
		/**-----------------新增维修员相关方法结束-------------------------------------------*/

        getAreaData: function(data, fn){
            win.utils.ajaxPost(win.utils.services.getSysArea, data, fn);
        },
		// 获取城市数据
		getCityData: function(data, fn){
			win.utils.ajaxPost(win.utils.services.queryUserCity, data, fn);
		},


		// 选中列表某一行,渲染区域详情部分
		rowOption: function (index) {

			var id=$('#table').jqGrid('getGridParam','selrow');
			repairman.getLeaveRecord(id);

			// 详情区域按钮
			$('#AllBtnAction button').off('click').on('click',function () {
				// 详情区域滚动
				repairman.detailScroll($('.detail').offset().top);

				var btn_index=$(this).index();

				let nameVal = $(this).attr('name');//tab切换---获取具体信息

				switch (nameVal) {
					case 'leaveRecord':
						repairman.getLeaveRecord(id);
						break;
					case 'cardInfo':
						repairman.queryBankInfo(id);
						break;
					case 'actRecord':
						repairman.getOptionRecord(id);
						break;
				}
				// 按钮切换
				$('#AllBtnAction button').removeClass('active').eq(btn_index).addClass('active');

				// 对应列表切换
				$('#contanerDetail>div').hide().eq(btn_index).show();
			});
		},
		// 获取请假记录
		getLeaveRecord: function(workerId){
			win.utils.ajaxPost(win.utils.services.getWorkerLeaveList,{
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				workerId: workerId,
			},function(result){
				if(result.result_code==200){
					// 按钮切换
					$('#AllBtnAction button').removeClass('active').eq(0).addClass('active');

					// 对应列表切换
					$('#contanerDetail>div').hide().eq(0).show();
					$('.detail').show();

					var leaveGrideData=result.workerLeaveRecords;
					var gridJson = {
						rows: leaveGrideData,
					};
					$("#leaveRecordTable")[0].addJSONData(gridJson);

					$('.leaveTbaleBox').show();
				}
			});
		},
		// 获取操作记录
		getOptionRecord: function(workerId){
			win.utils.ajaxPost(win.utils.services.queryWorkerLog,{
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				workerId: workerId,
			},function(result){
				if(result.result_code==200){
					var optionGrideData=result.workerLogs;
					var optionGridJson = {
						rows: optionGrideData
					};
					$("#optionRecordTable")[0].addJSONData(optionGridJson);
				}
			});
		},
		// 获取银行卡信息
		queryBankInfo:function(workerId){
			win.utils.ajaxGet(win.utils.services.queryBankInfo,{
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				workerId: workerId,
			},function(result){
				if(result.result_code==200){
					var cardGrideData=result.data;
					var cardGridJson = {
						rows: cardGrideData
					};
					$("#cardInfoTable")[0].addJSONData(cardGridJson);
				}
			});
		},

		// 详情区域按钮点击页面滚动
		detailScroll:function(h){
			$('body,html').animate({
				scrollTop:h
			},300)
		},

        // 时间戳转时间格式
        timeStampFormat: function (timestamp) {
            var date = new Date(timestamp);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
            h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y + M + D + h + m + s;
        },

		leaveOk:function(obj)  {
            utils.showMessage('请假成功！');
        	$('#table').jqGrid('setRowData',obj.workerId,{
				workerStatus:2,
				isInservice:1,
				workerId:obj.workerId
			});
		},
	};

	win.repairman = repairman;
})(window, jQuery, qiniu);
