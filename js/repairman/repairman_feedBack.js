/**
 * Created by song on 2019-04-17
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';

    var nowPage = 1;
    var is_search = false;

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        // $(".form-group span").remove();
    }

    var postData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        position: nowPage,
        rows: 20,
        startCreateTime: null,
        endCreateTime: null
    };
    var repairman = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            // 维修员反馈权限
            utils.isUserMenuIndexRight('repairman_feedBack', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                    // $("#table").setGridHeight(utils.getAutoGridHeight()-$('#gbox_table').position().top);
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });

            $('#btnQuery').on('click', function () {
                obj.refreshServerData();
            });
            $('#search').on('click', function () {
                nowPage = 1;
                is_search = true;
                repairman.search();
            });
            $('#empty').on('click', function () {
                $('#timeChoose').val('');
                repairman.refreshServerData();
            });
            repairman.getServerData(1, postData);
            layui.use('layer', function () {
                var layer = layui.layer;
                // 查看图片
                $('#table').on('click', '.checkImg', function () {
                    var issueImg = $(this).attr('data-img');
                    if (issueImg) {
                        var imgNode = '';
                        var imgArray = issueImg.split(',');
                        for (var i = 0; i < imgArray.length - 1; i++) {
                            imgNode += '<img layer-pid="' + i + '" layer-src="' + imgArray[i] + '" src="' + imgArray[i] + '"/>'
                        }
                        $('#checkImgLayer').html(imgNode);

                        var i = layer.open({
                            type: 1,
                            area: ['80%', '80%'],
                            title: '<i class="fa fa-eye">&nbsp;&nbsp;查看图片</i>',
                            content: $('#checkImgLayer'),
                            success: function (index) {
                                var imgs = $('#checkImgLayer img');
                                var imgArr = [];
                                for (var i = 0; i < imgs.length; i++) {
                                    imgArr.push({
                                        "alt": "",
                                        "pid": i, //图片id
                                        "src": $(imgs[i]).attr('src'), //原图地址
                                        "thumb": $(imgs[i]).attr('layer-src')//缩略图地址
                                    })
                                }
                                if (imgArr.length) {
                                    // 图片放大
                                    $('#checkImgLayer>img').click(function () {
                                        var index = $(this).index();
                                        var json = {
                                            "title": "", //相册标题
                                            "id": index, //相册id
                                            "start": index, //初始显示的图片序号，默认0
                                            "data": imgArr
                                        };
                                        layer.photos({
                                            photos: json,
                                            closeBtn: true,
                                            anim: 5, //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
                                            success: function () {

                                                // $('.layui-layer-phimg').off('mouseover').on('mouseover','img',function () {
                                                // 	alert('222');
                                                // })
                                            }
                                        });
                                    })
                                }

                            }
                        });
                    }
                });
            })


        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["ID", "问题内容", "问题图片", "反馈人", "反馈时间"],
                colModel: [
                    {
                        name: "id",
                        index: "id",
                        editable: false,
                        width: 90,
                        sorttype: "string",
                        hidden: true
                    },
                    {
                        name: "content",
                        index: "content",
                        editable: false,
                        width: 320,
                        sorttype: "string"
                    },
                    {
                        name: "issueImg",
                        index: "issueImg",
                        editable: false,
                        width: 100,
                        sorttype: "string",
                        formatter: repairman.listImg
                    },
                    {
                        name: "workerName",
                        index: "workerName",
                        editable: false,
                        width: 90,
                        sorttype: "string",
                    },
                    {
                        name: "createTime",
                        index: "createTime",
                        editable: false,
                        width: 120,
                        sorttype: "string",
                        formatter: repairman.creatTimeHandle
                    }
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        postData.position = nowPage;
                        repairman.search(postData);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        postData.position = nowPage;
                        repairman.search(postData);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    postData.position = nowPage;
                    repairman.search(postData);
                },
            });

            repairman.getServerData(nowPage, postData);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            var b = $(".jqGrid_wrapper").width();
            $("#table").setGridWidth(b);
            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        refreshServerData: function () {
            $('#startCreateTime').val('');
            $('#endCreateTime').val('');
            nowPage = 1;
            is_search = false;
            postData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                position: nowPage,
                rows: 20,
                startCreateTime: null,
                endCreateTime: null
            };
            repairman.getServerData(1, postData);
        },
        getServerData: function (pageIndex, postData) {
            is_search = false;
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.getIssueFeedBack, postData, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: 0
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                        utils.showMessage('暂无数据!');
                    } else {
                        gridData = result.workerIssueFeedbacks;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        search: function () {
            // 清空列表
            var gridJson = {
                total: 0,
                rows: [],
                page: 0,
                records: 0
            };
            pageText = '第0页 / 共0页';
            $("#table").jqGrid('setGridParam', {
                pgtext: pageText
            });
            $("#table")[0].addJSONData(gridJson);

            if (is_search) {
                var createTime = repairman.dateFormat($('#timeChoose').val()) || null;
                postData.endCreateTime = createTime ? createTime[1] : null;
                postData.startCreateTime = createTime ? createTime[0] : null;
                postData.position = nowPage;
            }

            repairman.getServerData(nowPage, postData);
        },
        creatTimeHandle: function (cellvalue, options, rowObject) {
            if (rowObject.createTime) {
                return repairman.timeStampFormat(rowObject.createTime);
            } else {
                return '';
            }
        },
        listImg: function (cellvalue, options, rowObject) {
            if (rowObject.issueImg) {
                return '<a class="checkImg unified-a" data-img="' + rowObject.issueImg + '">查看图片</a>'
            } else {
                return '暂无图片'
            }

        },
        // 时间戳转时间格式
        timeStampFormat: function (timestamp) {
            var date = new Date(timestamp);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
            h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y + M + D + h + m + s;
        },
        // 日期格式处理
        dateFormat: function (dataStr) {
            if (dataStr) {
                var startCreateTime = dataStr.split(' - ')[0];
                var endCreateTime = dataStr.split(' - ')[1];
                startCreateTime = repairman.datePartFormat(startCreateTime) + ' 00:00:00';
                endCreateTime = repairman.datePartFormat(endCreateTime) + ' 23:59:59';
                return [startCreateTime, endCreateTime];
            } else {
                return null
            }
        },
        datePartFormat: function (date) {
            return date.split('-').join('');
        }
    };

    win.repairman = repairman;
})(window, jQuery);