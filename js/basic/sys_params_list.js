/**
 * Created by yimao on 2016-06-24.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
        $('#hiddenID').val('');
        $('#txtValueName').val('');
    }

    var sys_params_list = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('basic_params', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });

            win.vaildation.init("#form");

            $('#btnQuery').on('click', function () {
                obj.getServerData(1);
            });
            $('#btnUpdate').click(obj.updateRecord);
        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["参数名称", "参数值"],
                colModel: [
                    {name: "name", index: "name", editable: false, width: 90, sorttype: "string"},
                    {name: "value", index: "value", editable: false, width: 90, sorttype: "string"}
                ],
                pager: "#table_pager",
                viewrecords: true,
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    if (deferLoadData == 0) {
                        sys_params_list.getServerData(currentPage);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        sys_params_list.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    sys_params_list.getServerData(currentPage);
                }
            });

            sys_params_list.getServerData(1);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });
            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        getServerData: function (pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajax(win.utils.services.sysparams_list, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                batch: pageIndex,
                rows: pageCount,
                order: orderByString
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.params.total == 0) {
                        gridData = {};
                        var gridJson = {total: 0, rows: gridData, page: 0, records: result.params.total};
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {pgtext: pageText});
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.params.rows;
                        totalPages = Math.ceil(result.params.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.params.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {pgtext: pageText});
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        updateRecord: function () {
            initializeEditForm();
            var data = $("#table").jqGrid('getGridParam', 'selrow');
            if (data == null) {
                utils.showMessage('请选择要进行修改的记录！');
                return;
            }
            var paramName = $('#table').jqGrid('getCell', data, 'name');

            win.utils.ajax(win.utils.services.sysparams_get, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                name: paramName
            }, function (result) {
                if (result.result_code == 200) {
                    $('#hiddenID').val(result.params.name);
                    $('#txtValueName').val(result.params.value);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                    return;
                }
            });

            var i = layer.open({
                type: 1,
                area: '480px',
                title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;修改参数值',
                content: $('#model'),
                btn: ['保存', '取消'],
                yes: function (index, layero) {
                    var param_value = $('#txtValueName').val();

                    if (!$('#form').valid()) {
                        if (param_value == '') {
                            $('#txtValueName').focus();
                            return;
                        }
                        return;
                    }

                    var param_name = $('#hiddenID').val();
                    win.utils.ajax(win.utils.services.sysparams_update, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        name: param_name,
                        value: param_value
                    }, function (result) {
                        if (result.result_code == 200) {
                            $('#table').jqGrid('setRowData', data, {name: param_name, value: param_value});
                            layer.close(i);
                        } else {
                            utils.showLayerMessage('保存失败，请重试！');
                        }
                    });
                }
            });
        }
    };

    win.sys_params_list = sys_params_list;
})(window, jQuery);