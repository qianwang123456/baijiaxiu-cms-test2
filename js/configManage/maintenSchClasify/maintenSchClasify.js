/**
 * Created by song.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    //table选中行#id
    var selectedId = 0;
    //search---
    var searchName = '';//搜索名称
    var searchSta = '';//搜索状态
    var dataSelect = {};//删除数据
    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
    };

    function initSearch() {
        searchName = '';//搜索名称
        searchSta = '';//搜索状态
        $("#secNameB").val("");//名称清空
        // $("#skilClassify").val("");
        $("#seaSta").val("");
    };
    var maintenSchClasify = {
        updateList: function () {
            let url = win.utils.services.configMan_maintenSchClasify_list;
            let datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                id: parseInt(selectedId),
            };
            win.utils.ajaxPost(url, datas, function (result) {
                if (result.result_code == 200) {
                    var myObj = result.servicePlans[0];
                    $("#secNameA").val(myObj.name);
                    $("#isCheck").val(myObj.isCheck);
                    $("#staAdd").val(myObj.status);
                    $("#explain").val(myObj.planExplain);
                } else {
                    utils.showMessage('获取数据失败，请稍后重试！');
                }
            });
        },
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('maintenSchClasify', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                    win.utils.clearSpaces();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });
            // 搜索
            $('#btnSearch').click(obj.searchBox);
            //刷新
            $('#btnQuery').on('click', function () {
                initSearch();
                obj.refreshServerData();
            });
            //新增
            $('#btnAdd').on('click', function () {
                maintenSchClasify.addWorkOrder(1)
            });
            //修改
            $('#btnUpdate').on('click', function () {
                maintenSchClasify.addWorkOrder(2)
            });
            //删除
            $("#btnDel").on('click', function () {
                if (selectedId == null || selectedId == 0) {
                    utils.showMessage('请选择要进行删除的维修方案！');
                    return;
                }
                ;
                dataSelect.user_id = win.utils.getCookie('user_id');
                dataSelect.session = win.utils.getCookie('session');
                utils.showConfirm('您确定要删除该维修方案吗？', '删除', function () {
                    win.utils.ajaxPost(win.utils.services.configMan_maintenSchClasify_del, dataSelect, function (result) {
                        if (result.result_code == 200) {
                            utils.closeMessage();
                            $('#table').jqGrid('delRowData', data);
                            dataSelect = {};
                        } else {
                            utils.showMessage('删除失败，请重试！');
                        }
                    });
                });
            })
        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: 20,
                rownumbers: true,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["ID", "分类名称", "需要审核", "状态", "创建子工单类型", "子工单类型一级", "子工单类型二级", " 说明"],
                colModel: [{
                    name: "id",
                    index: "id",
                    editable: false,
                    width: 90,
                    sorttype: "string",
                    hidden: true
                },
                    {
                        name: "name",
                        index: "name",
                        editable: false,
                        width: 180,
                        sorttype: "string"
                    },
                    {
                        name: "isCheck",
                        index: "isCheck",
                        editable: false,
                        width: 120,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '是';
                            else return '否';
                        }
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 120,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '启用';
                            else return '禁用';
                        }
                    },
                    {
                        name: "subTaskName",
                        index: "subTaskName",
                        editable: false,
                        width: 120,
                        sorttype: "string"
                    },
                    {
                        name: "subTaskCategoryOneName",
                        index: "subTaskCategoryOneName",
                        editable: false,
                        width: 180,
                        sorttype: "string"
                    },
                    {
                        name: "subTaskCategoryTwoName",
                        index: "subTaskCategoryTwoName",
                        editable: false,
                        width: 180,
                        sorttype: "string"
                    },
                    {
                        name: "planExplain",
                        index: "planExplain",
                        editable: false,
                        width: 640,
                        sorttype: "string"
                    },

                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    if (deferLoadData == 0) {
                        maintenSchClasify.getServerData(currentPage);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        maintenSchClasify.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    maintenSchClasify.getServerData(currentPage);
                },
                //当选择行时触发此事件。
                onSelectRow: function () {
                    //返回选中的id
                    var data = $("#table").jqGrid('getGridParam', 'selrow');
                    // var selectedIds = $("#table").jqGrid('getRowData',data,'serviceId');
                    selectedId = $('#table').jqGrid('getCell', data, 'id');
                    let a = $('#table').jqGrid('getCell', data, 'isCheck');
                    let b = $('#table').jqGrid('getCell', data, 'name');
                    let c = $('#table').jqGrid('getCell', data, 'planExplain');
                    let d = $('#table').jqGrid('getCell', data, 'status');
                    dataSelect.id = selectedId;
                    dataSelect.isCheck = a == '否' ? 2 : 1;//是否审核 1-审核，2-不审核 ,
                    dataSelect.name = b;//name
                    dataSelect.planExplain = c;//说明
                    dataSelect.status = d == '启用' ? 1 : 2;
                },
                gridComplete: function () {

                }
            });

            maintenSchClasify.getServerData(1);
            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        refreshServerData: function () {
            maintenSchClasify.getServerData(1);
        },
        getServerData: function (pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            var datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                // isCheck:$("#skilClassify").val()==''?'':parseInt($("#skilClassify").val()),//是否审核 1-审核，2-不审核
                name: searchName.replace(/\s*/g, ""),
                status: searchSta == '' ? '' : parseInt(searchSta),
                rank: 3,
                position: pageIndex,
                rows: pageCount
            };
            win.utils.ajaxPost(win.utils.services.configMan_maintenSchClasify_list, datas, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.servicePlans;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        //搜索
        searchBox: function () {
            initializeEditForm();
            initSearch();
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '查询',
                content: $('#SearchCon'),
                btn: ['查询', '取消'],
                yes: function (index, layero) {
                    layer.close(i);
                    searchName = $("#secNameB").val();//搜索名称
                    searchSta = $("#seaSta").val();
                    ;//搜索状态
                    maintenSchClasify.refreshServerData();
                }
            });
        },
        //新增-修改act:1新增  2:修改
        addWorkOrder: function (act) {
            initializeEditForm();
            if (act == 2) {
                if (selectedId == null || selectedId == 0) {
                    utils.showMessage('请选择要进行修改的记录！');
                    return;
                }
                ;
                maintenSchClasify.updateList();
            }
            ;
            let titleVal = act == 1 ? '新增' : '修改';
            let arr = act == 1 ? ['新增', '取消'] : ['修改', '取消'];
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: titleVal,
                content: $('#AddCon'),
                btn: arr,
                yes: function (index, layero) {
                    var nameVal = $("#secNameA").val();
                    if ($.trim(nameVal).length <= 0) {
                        utils.showLayerMessage('请填写名称！');
                        return;
                    }
                    ;
                    var isCheck = $("#isCheck").val();
                    if ($.trim(isCheck).length <= 0) {
                        utils.showLayerMessage('请选择是否审核！');
                        return;
                    }
                    ;
                    var statuss = $("#staAdd").val();
                    if (statuss.length <= 0) {
                        utils.showLayerMessage('请选择状态！');
                        return;
                    }
                    ;
                    var explain = $("#explain").val();
                    if ($.trim(explain).length <= 0) {
                        utils.showLayerMessage('请填写说明！');
                        return;
                    }
                    ;
                    let url = act == 1 ? win.utils.services.configMan_maintenSchClasify_add : win.utils.services.configMan_maintenSchClasify_update;
                    let datas = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        name: nameVal,
                        status: parseInt(statuss),
                        isCheck: parseInt(isCheck),
                        planExplain: explain,
                    };
                    if (act == 2) {
                        datas.id = selectedId;
                    }
                    win.utils.ajaxPost(url, datas, function (result) {
                        if (result.result_code == 200) {
                            layer.close(i);
                            maintenSchClasify.refreshServerData();
                        } else {
                            let msg = act == 1 ? '新增失败，请稍后重试！' : '修改失败，请稍后重试！';
                            utils.showLayerMessage(msg);
                        }
                    });
                }
            });
        }
    };
    var flagInput = false;
    //禁止弹窗与浏览器滚动
    $("body").on('focus', 'input,textarea', function () {
        flagInput = true;
    });
    $("body").on('blur', 'input,textarea', function () {
        flagInput = false;
    });
    $(document).keydown(function (event) {
        var e = window.event || event;
        var keyCode = event.keyCode;
        if (keyCode == 13 || keyCode == 32) {
            if (flagInput == false) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    window.event.returnValue = false;
                }
                return false;
            }
            ;
        }
    });
    win.maintenSchClasify = maintenSchClasify;
})(window, jQuery);
