/**
 * Created by song.---ok版
 */
(function(win, $) {
	var selectId='';	//table-选中的订单id
	var flagInput=false;//空格输入按钮  // 为true时,允许 输入空格与换行,false禁止输入空格与换行
    var ConFlag;//弹层关闭
    var partsFlag;//配件弹层关闭
	var deferLoadData = 0;
	var  nowCurPage= 0;//订单列表---当前页码数
	var  totalNowPages = 0;//订单列表---总的页码数
	var inputPageFlag=false;//订单列表---输入页码跳转
	var accountParts=[];//配件数组---before
    var accountPartsAf=[];//配件数组---after
    var partsFirstIdVal='';//一级配件id
    var partsSecondIdVal='';//二级配件id
    var partsData = [];//所有配件
    var partsArrData=[];//配件数组---类型-partcode
    var unitBlFlag = true ; // true允许，false禁止
	var focusFlag = false;
	var pos = null;
	//延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题

	// 初始化
	function initializeEditForm() {
		$("#addName").parent('div').find('em').eq(0).text('');
		$(".form-group").removeClass("has-error");
	};
	// 复制处理文本
	function textInit(e) {
		e.preventDefault();
		var text;
		var clp = (e.originalEvent || e).clipboardData;
		if (clp === undefined || clp === null) {
			// console.log(window.clipboardData)
			text = window.clipboardData.getData("html") || "";
			text = text.replace('<html>','').replace('</html>','').replace('<body>','').replace('</body>','').replace(/<!--StartFragment-->/g,'').replace(/<!--EndFragment-->/g,'')
			if (text !== "") {
				if (window.getSelection) {
					var newNode = document.createElement("span");
					newNode.innerHTML = text;
					window.getSelection().getRangeAt(0).insertNode(newNode);
				} else {
					document.selection.createRange().pasteHTML(text);
				}
			}
		} else {
			// console.log(clp)
			text = clp.getData('text/html') || "";
			text = text.replace('<html>','').replace('</html>','').replace('<body>','').replace('</body>','').replace(/<!--StartFragment-->/g,'').replace(/<!--EndFragment-->/g,'')
			if (text !== "") {
				console.log(text)
				// document.execCommand('insertHTML', false, text);
				document.execCommand('insertHTML', true, text);
			}
		}
	}
		//关闭弹层
    function closeFlagCon(){
        $("#selectUnitCon").hide();
        $("#inputUnitCon").hide();
        $("#addUnitType").val('')
		$("#addName").parent('div').find('em').eq(0).text('');
        layer.close(ConFlag);
        layer.close(partsFlag);
		$("#AddCon").hide();
		$("#parts").hide();
        maintenPlan_list.getServerData(1);
    }
    // 校验输入内容只能为汉字数字字母
	function checkIsChEnNum(str) {
		var pattern = /^[A-Za-z0-9\u4e00-\u9fa5]+$/gi;
		//如果值为空或通过，通过校验
		if (str == "" || pattern.test(str) ) {
			unitBlFlag = true
		} else {
			unitBlFlag = false
		}
		// return unitBlFlag;
	}
	function timerFormatter(cellvalue,options, rowObject) {
		if(!cellvalue){
			cellvalue='';
			return cellvalue;
		}else{
			let obj=new Date(cellvalue);
			let y = obj.getFullYear();
			let mon = obj.getMonth() + 1;
			let d = obj.getDate();
			let h = obj.getHours();
			let min = obj.getMinutes();
			let s = obj.getSeconds();

			mon = mon<10?'0'+mon:mon;
			d = d<10?'0'+d:d;
			h = h<10?'0'+h:h;
			min = min<10?'0'+min:min;
			s = s<10?'0'+s:s;

			return y+'-'+mon+'-'+d+' '+h+':'+min+':'+s;
		};
	}
	var maintenPlan_list = {
		//查询配件库-类型
		queryOrderPartsType(num,typeVal){
			num=num==undefined?1:num;
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				status:1,
				rank:num
			};
			if(!isNaN(typeVal)){
				datas.firstTypeId=typeVal;
			};
			win.utils.ajaxPost(win.utils.services.region_queryPartsType, datas,function(result){
				let obj=result.configPartsTypes;
				let str="";
				if(isNaN(typeVal)){
					$.each(obj,function(index,value){
						str+='<button type="button" class="btn" name="'+value.firstTypeId+'">'+value.firstTypeName+'</button>';
					});
				}else{
					$.each(obj,function(index,value){
						str+='<button type="button" class="btn" name="'+value.secondTypeId+'">'+value.secondTypeName+'</button>';
					});
				};
				if(isNaN(typeVal)){
					$("#firstPartsType").empty().append(str);
					$("#firstPartsType button").eq(0).addClass('btn-info');
                    partsFirstIdVal=obj[0].firstTypeId;
					maintenPlan_list.queryOrderPartsType(2,partsFirstIdVal);
				}else{
					$("#secondPartsType").empty().append(str);
					$("#secondPartsType button").eq(0).addClass('btn-info');
					let partsSecondIdVal='';
					if(obj.length>0){
						partsSecondIdVal=obj[0].secondTypeId;
					};
					maintenPlan_list.queryOrderParts(partsFirstIdVal,partsSecondIdVal);
				};
			});
		},
		//查询配件库-list
		queryOrderParts(a,b){
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				status:1
			};
			if(a!=''){
				datas.firstTypeId =a;
				datas.rank=1;
			};
			if(b!=''){
				datas.secondTypeId =b;
				datas.rank=2;
			}
			win.utils.ajaxPost(win.utils.services.region_queryParts, datas,function(result){
				let obj=result.parts;
				if(obj&&obj.length<=0){
                    return;
                };
				if(a==''&&b==''){
					partsData=result.parts;
					let strs="<option value=''>请选择</option>";
					$.each(obj,function(index,value){
						let strVal=JSON.stringify(value);
						strs+='<option name="'+strVal.replace(/"/g,"@#")+'" value='+value.partsCode+'>  '+value.partsName+'</option>';
					});
					$("#partsName").empty().append(strs);
					maintenPlan_list.refreshRepairman();
				}else{
                    let strVals='';
                    partsArrData=[];
                    let arrs=[];
                    let partsCheckedOwnBf=[];
                    $.each(accountParts,function(index,value){
                        arrs.push(value.partsCode);
                        partsCheckedOwnBf.push(value);
                    });
                    $.each(obj,function(index,value){
                        partsArrData.push(value.partsCode);
                        let strVal=JSON.stringify(value);
                        let strNameVal=arrs.indexOf(value.partsCode)<0?'uncheck':'partsTypeCheck';
                        strVals+=`<li name="`+strVal.replace(/"/g,"@#")+`" value="`+value.partsCode+`" class="`+strNameVal+`">`
                            +`<span class="uncheck">√</span><span class="name">`+value.partsName+`</span><span class="brand">品牌:`+value.brandName+`</span><span class="type">型号:`+value.partsType+`</span><span class="fee">配件费:`+value.price+`元/`+value.unitName+`</span></li><br>`;
                    });
                    $("#partTypeCon>ul").empty().append(strVals);
                };
			});
		},
		//根据id获取对应方案模板 --- api:orderMan_config_getFeedbackTem
		getFeedbackTem:function(){
			win.utils.ajaxPost(win.utils.services.orderMan_config_getFeedbackTem, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				id :selectId
			}, function(result) {
				if(result.result_code == 200) {
					maintenPlan_list.setFeedbackTem(result.data)
				}else{
					let msg= result.description?result.description:'获取数据失败，请重试！';
					utils.showMessage(msg);
				}
			});
		},
		//根据id展现对应方案模板
		setFeedbackTem:function(obj){
			if(obj){
                accountParts=obj.accountParts?obj.accountParts:[];// (Array[PlanPartsDTO]: 配件信息列表 ,
				let faultCause=obj.faultCause?obj.faultCause:'';//故障原因 ,
				let id=obj.id?obj.id:'';//id ,
				let name=obj.name?obj.name:'';//模板名称 ,
				let templateProjectDesc=obj.templateProjectDesc?obj.templateProjectDesc:'';//维修方案描述说明 ,
				let remark=obj.remark?obj.remark:'';//备注 ,
				let servicePlanId=obj.servicePlanId?obj.servicePlanId:'';//协调方案id ,
				$("#addName").val(name);
				$("#addFaultRea").val(faultCause);
				// $("#addMaint").val(projectDesc);
				$("#addMatinText").html(templateProjectDesc);
				$("#addNote").val(remark);
				$("#addClassify").val(servicePlanId);
                accountPartsAf=accountParts;//配件数组---after
                if(accountParts.length>0){
                    let str=``;
                    $.each(accountParts,function(index,value){
                        str+=`<li>
									<div class="b">`+value.partsName+`</div>
									<div class="a">`+value.brandName+`</div>
									<div class="a">`+value.partsType+`</div>
									<div class="a">`+Number(value.price).toFixed(2)+`元</div>
									<div class="a"><input class="num" type="number" onmousewheel="return false;" min=0 title="oneNumber" id="`+value.id+`" value="`+value.partsNum+`">`+value.unitName+`</div>
									<div class="deletePartOwn" name="del">✖</div>
								</li>`;
                    });
                    // 配件展示
                    $("#checkedPartsOwnShowJss").show();
                    $("#checkedPartsOwnShowJs").empty().append(str);
                }else{
                    $("#checkedPartsOwnShowJss").hide();
                    $("#checkedPartsOwnShowJs").empty();
                };
			}
		},
		// 获取维修方案-list
		getServiceScheme : function(type) {
			win.utils.ajaxPost(win.utils.services.configMan_maintenSchClasify_list, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				status :1
			}, function(result) {
				if(result.result_code == 200) {
					var obj=result.servicePlans;
					let str="<option value=''>请选择维修方案类型</option>";
					$.each(obj,function(index,value){
						str+="<option value='"+value.id+"'>"+value.name+"</option>"
					});
					$("#addClassify").empty().append(str);
					if(type==2){
						maintenPlan_list.getFeedbackTem();
					}
				} else {
					let msg= result.description?result.description:'获取数据失败，请重试！';
					utils.showMessage(msg);
				}
			});
		},
		// 城市-弹窗-配件
		refreshRepairman:function () {
			let partsCheckedOwnBf=[];//配件-已选择配件---配件库---修改前
            let idArr=[];
            $.each(accountParts,function(index,value){
                partsCheckedOwnBf.push(value);
                idArr.push(value.partsCode);
            });
			layui.use(['form','code'], function () {
				var form = layui.form,
					$ = layui.$;
				// 代码块
				layui.code({
					title: 'html',
					encode: true,
					about: false
				});
                // 手动赋值
                $('select[name="配件名称搜索"]').val(idArr);
				form.render();
				//配件库-添加配件---配件名称
                form.on('select(multiPartOwnName)', function (data) {
// 						console.log(data.elem); //得到select原始DOM对象
// 						console.log(data.value,"value"); //得到被选中的值（数组类型）
// 						console.log(data.othis); //得到美化后的DOM对象
// 						console.log(data.current_value,"current_value"); //当前操作的值（选中或取消的值）
                    let nowCheckNum=data.value.length;
                    if(partsData.length==nowCheckNum){
                        $("#checkedPartsOwn").empty();
                        partsCheckedOwnBf=partsData;
                        $.each(partsCheckedOwnBf,function(index,valuess){
                            if(partsArrData.indexOf(valuess.partsCode)>-1){
                                $("#partTypeCon>ul>li").eq(partsArrData.indexOf(valuess.partsCode)).removeClass('uncheck').addClass('partsTypeCheck');
                            };
                            valuess.partsNum=1.0;
                        });
                        $.each(partsCheckedOwnBf,function(index,valuess){
                            valuess.partsNum=1.0;
                            let strOwnS=`<li>
                                    <div>`+valuess.partsName+`</div> 
                                    <div>`+valuess.brandName+`</div>
                                    <div>`+valuess.partsType+`</div>
                                    <div>`+Number(valuess.price).toFixed(2)+`元/`+valuess.unitName+`</div>
                                </li>`;
                            $("#checkedPartsOwn").append(strOwnS);
                        });
                    }else if(nowCheckNum==0&&data.current_value==''){
                        partsCheckedOwnBf=[];
                        $("#checkedPartsOwn").empty();
                        $.each($("#partTypeCon>ul>li"),function(index,valuess){
                            $("#partTypeCon>ul>li").eq(index).removeClass('partsTypeCheck').addClass('uncheck');
                        });
                    }else if(data.current_value==''&&nowCheckNum!=0){
                        $.each($("#partTypeCon>ul>li"),function(index,valuess){
                            let idValNow=$("#partTypeCon>ul>li").eq(index).attr('value');
                            if(data.value.indexOf(idValNow)<0){
                                $("#partTypeCon>ul>li").eq(index).removeClass('partsTypeCheck').addClass('uncheck');
                            }else{
                                $("#partTypeCon>ul>li").eq(index).addClass('partsTypeCheck').removeClass('uncheck');
                            };
                        });
                        let bfArr=[];
                        $("#checkedPartsOwn").empty();
                        let idArr=[];
                        $.each(partsCheckedOwnBf,function(index,valuess){
                            idArr.push(valuess.partsCode)
                        });
                        $.each(partsData,function(index,valuess){
                            if(idArr.indexOf(valuess.partsCode)<0){
                                valuess.partsNum=1.0;
                                let strOwnS=`<li>
                                        <div>`+valuess.partsName+`</div> 
                                        <div>`+valuess.brandName+`</div>
                                        <div>`+valuess.partsType+`</div>
                                        <div>`+Number(valuess.price).toFixed(2)+`元/`+valuess.unitName+`</div>
                                    </li>`;
                                $("#checkedPartsOwn").append(strOwnS);
                                bfArr.push(valuess);
                            }
                        });
                        partsCheckedOwnBf=bfArr;
                    }else{
                        let indexVal=null;
                        $.each(partsCheckedOwnBf,function(index,valuess){
                            if(valuess.partsCode==data.current_value){
                                indexVal=index;
                                return;
                            };
                        });
                        if(data.value.indexOf(data.current_value)<0){
                            if(partsArrData.indexOf(data.current_value)>-1){
                                $("#partTypeCon>ul>li").eq(partsArrData.indexOf(data.current_value)).addClass('uncheck').removeClass('partsTypeCheck');
                            };
                            if(indexVal>-1){
                                partsCheckedOwnBf.splice(indexVal,1);
                                $("#checkedPartsOwn>li").eq(indexVal).remove();
                            };
                        }else{
                            if(partsArrData.indexOf(data.current_value)>-1){
                                $("#partTypeCon>ul>li").eq(partsArrData.indexOf(data.current_value)).removeClass('uncheck').addClass('partsTypeCheck');
                            };
                            if(indexVal<0||!indexVal){
                                $("#partsName").val(data.current_value);
                                let transStr=$("#partsName option:selected")[0].getAttribute('name').replace(/@#/g,'"');
                                let obj=JSON.parse(transStr);
                                obj.isCustomParts=2;
                                obj.partsNum=1;
                                partsCheckedOwnBf.push(obj);
                                let strOwnS=`<li>
                                        <div>`+obj.partsName+`</div> 
                                        <div>`+obj.brandName+`</div>
                                        <div>`+obj.partsType+`</div>
                                        <div>`+Number(obj.price).toFixed(2)+`元/`+obj.unitName+`</div>
                                    </li>`;
                                $("#checkedPartsOwn").append(strOwnS);
                            }
                        };
                    };
                    accountPartsAf=[];
                    $.each(partsCheckedOwnBf,function(index,value){
                        accountPartsAf.push(value);
                    });
                });
			});
		},
		//新增/修改
		addWorkOrder: function(type){//1:add;2:edit
			initializeEditForm();
			maintenPlan_list.clearAddEditVal();
			maintenPlan_list.getServiceScheme(type);
			flagInput=false;
			let stl=type==1?'新增':'编辑';
			ConFlag = layer.open({
				type: 1,
				area: ['80%', '80%'],
				title:stl,
				content: $('#AddCon'),
				btn: ['保存', '取消'],
				yes: function(index, layero) {
					let str=$("#addName").val();
					if($.trim(str).length<=0){
						$("#addName").parent('div').find('em').eq(0).text('请注意模板名称为必填项！');
						return;
					}else if($.trim(str).length>10){
						// let strVal=$.trim(str).substring(0,10);
						// $(this).val(strVal);
						$("#addName").parent('div').find('em').eq(0).text('请注意模板名称不可超过10个字！');
						return;
					}else{
						$("#addName").parent('div').find('em').eq(0).text('');
					};
					let faultCause=$("#addFaultRea").val();
					// let projectDesc=$("#addMaint").val();
					let projectDesc=$("#addMatinText").text(); //addMatinText
                    let templateProjectDesc = $("#addMatinText").html();
					let remark=$("#addNote").val();
					let servicePlanId=$("#addClassify").val();
					if(projectDesc.trim().length<=0){
						let msg = `请输入维修方案！`;
						utils.showMessage(msg);
						return;
					}
                    if(projectDesc.length>800){
                        let msg = `维修方案输入内容太长啦，限制800字！`;
                        utils.showMessage(msg);
                        return;
                    }
					let data={
						user_id: win.utils.getCookie('user_id'),
						session: win.utils.getCookie('session'),
						accountParts:accountParts,// 配件信息列表 ,
						faultCause :faultCause,// 故障原因 ,
						name :str,// 模板名称 ,
						projectDesc :projectDesc,// 维修方案描述说明 ,
                        templateProjectDesc:templateProjectDesc,// 维修方案描述说明---带标签 ,
						remark: remark,//备注 ,
						servicePlanId: servicePlanId,//协调方案id ,
					};
					let url='';
					if(type==1){
						selectId='';
						url=win.utils.services.orderMan_config_addFeedbackTem;
					}else{
						url=win.utils.services.orderMan_config_updateFeedbackTem;
						data.id=selectId;
					};
					win.utils.ajaxPost(url, data, function(result) {
						closeFlagCon();
						if(result.result_code == 200) {
							maintenPlan_list.refreshServerData();
						} else {
							let msg= result.description?result.description:'失败啦，请稍后重试！';
							utils.showMessage(msg);
						}
					});
				},
				btn2: function(index, layero) {
					layer.close(partsFlag);
					layer.close(ConFlag);
					$("#AddCon").hide();
					$("#parts").hide();
				},
				cancel: function(index, layero) {
					layer.close(partsFlag);
					layer.close(ConFlag);
					$("#AddCon").hide();
					$("#parts").hide();
				},
				end:function(){
					$("#AddCon").hide();
					$("#parts").hide();
				}

			});
		},
		//初始化
		init: function() {
			var obj = this;
			if(utils.isLogin() == false) {
				utils.loginDomain();
				return;
			}
			//判断是否有自动结算权限，此时为配置管理的自动结算权限
			utils.isUserMenuIndexRight('maintenPlan_list', function(result) {
				if(result == 1) {
					//initialize...
					obj.initList();
					win.utils.clearSpacesTextarea();
				} else {
					self.location = '../../../web/error/access_refuse.html';
					return;
				}
			});
			//订单列表---输入页码跳转
			$("#table_pager").on('input','#inputNowPages',function(event){
				flagInput=true;
				inputPageFlag = true;
				let pag=Number($(this).val());
				if(!isNaN(pag)){
					if(pag<=nowCurPage){
						pag=nowCurPage
					}else if(pag>=totalNowPages){
						pag=totalNowPages
					};
					nowCurPage=pag;
					$(this).val(nowCurPage);
				}else{
					nowCurPage=1;
					pag=1;
					$(this).val(1);
				};
			});
			$("#table_pager").on('keyup','#inputNowPages',function(event){
				if (event.keyCode == "13") {
					//回车执行查询
					$(this).blur();
					tradingAreaSearch=$("#busDistrict option:selected").val()=='商圈'?'':$("#busDistrict option:selected").val();//商圈名称 ,
					tradingAreaIdSearch=$("#busDistrict").val();//商圈id ,
					blockIdSearch=!$("#village").val()?'':parseInt($("#village").val());//小区id ,
					exigencyFlagSearch=parseInt($("#exigencyFlagSear").val());// 紧急标识(1、紧急；2、普通) ,
					firstGradeSearch=$("#firstGrade").val();// 一级工单类型id ,
					secondGradeSearch=$("#secondGrade").val();// 二级工单类型id ,
					linkOrderStatus=$("#relationWorkId").val();// 关联的工单状态 ,
					districtId=$("#districtId").val();//区域
					houseIdSearch=$("#apartment").val();//公寓id ,===暂无
					orderIdValSear=$("#orderId").val();//工单id ,===暂无
					adressValSear=$("#apartAdressCon").val();//公寓地址 ,===暂无
					orderIdValSears=orderIdValSear;//订单搜索id
					houseIdSearchs=houseIdSearch;//公寓id
					adressValSears=adressValSear;//公寓地址
					progressValSears=progressValSear;//进度状态
					OrderFlagValSears=OrderFlagValSear;//工单标签--sure
					repairTelValSears=repairTelValSear;//报修电话
					servicePlanSer=$("#servicePlan").val();// 维修方案id ,
					finishStatusSer=$("#isFinishFlagSear").val();// 工单是否完结
					isChangeDateSer=$("#isChangeDate").val();//是否改期上门
					sysNinetyReworkSer=$("#sysNinetyRework").val();//90天返修工单
					workerIdsArrs=workerIdsArr;//维修员
					// 订单-搜索-时间
					let strFp=$('#fenPaiTime').val();
					if(strFp.length!=0){
						orderFpTimeSt=strFp.slice(0,17)+"00";//创建时间开始
						orderFpTimeEn=strFp.slice(22,strFp.length-2)+"00";//创建时间结束
					}else{
						orderFpTimeSt='';//创建时间开始
						orderFpTimeEn='';//创建时间结束
					};
					let strYy=$('#orderTime').val();
					if(strYy.length!=0){
						orderYyTimeSt=strYy.slice(0,17)+"00";//预约时间开始
						orderYyTimeEn=strYy.slice(22,strYy.length-2)+"00";//预约时间结束
					}else{
						orderYyTimeSt='';//预约时间开始
						orderYyTimeEn='';//预约时间结束
					};
					let strFin=$('#finishTime').val();
					if(strFin.length!=0){
						orderFinTimeSt=strFin.slice(0,17)+"00";//完成时间开始
						orderFinTimeEn=strFin.slice(22,strFin.length-2)+"00";//完成时间结束
					}else{
						orderFinTimeSt='';//完成时间开始
						orderFinTimeEn='';//完成时间结束
					};
					let strCr=$('#createTime').val();
					if(strCr.length!=0){
						orderCreTimeSt=strCr.slice(0,17)+"00";//创建时间开始
						orderCreTimeEn=strCr.slice(22,strCr.length-2)+"00";//创建时间结束
					}else{
						orderCreTimeSt='';//创建时间开始
						orderCreTimeEn='';//创建时间结束
					};
					let strContact=$('#contactTime').val();
					if(strContact.length!=0){
						orderContTimeSt=strContact.slice(0,17)+"00";//联系时间开始
						orderContTimeEn=strContact.slice(22,strContact.length-2)+"00";//联系时间结束
					}else{
						orderContTimeSt='';//联系时间开始
						orderContTimeEn='';//联系时间结束
					};
					let strVis=$('#visitTime').val();
					if(strVis.length!=0){
						orderDoorTimeSt=strVis.slice(0,10);//上门时间开始
						orderDoorTimeEn=strVis.slice(13);//上门时间结束
					}else{
						orderDoorTimeSt='';//上门时间开始
						orderDoorTimeEn='';//上门时间结束
					};
					notnormal=$("#notnormal").val();//异常
					$('#SearchConss').hide();
					nowCurPage = $("#inputNowPages").val();
					maintenPlan_list.getServerData(nowCurPage);
				}
			});
			$("#table_pager").on('blur','#inputNowPages',function(){
				flagInput=false;
				inputPageFlag= false;
			});
			//刷新
			$('#empty').on('click', function() {
				obj.getServerData(1);
			});
			//新增
			$('#btnAdd').on('click',function(){
				maintenPlan_list.addWorkOrder(1)
			});
			//配件类型
			$("#partTypeCon>ul").off('li').on('click','li',function(){
				let partsCheckedOwnAf=[];
                $.each(accountPartsAf,function(index,value){
                    partsCheckedOwnAf.push(value);
                });
				$(this).toggleClass('partsTypeCheck');
				let transStr=$(this).attr('name').replace(/@#/g,'"');
				let obj=JSON.parse(transStr);
				obj.isCustomParts=2;
				obj.partsNum=1;
				let idVal=obj.partsCode;//获取id
				let indexVal=-1;
				$.each(partsCheckedOwnAf,function(index,valuess){
					if(valuess.partsCode.indexOf(idVal)>-1){
						indexVal=index;
					};
					return;
				});
				if(indexVal<0){//选中
					partsCheckedOwnAf.push(obj);
					let strOwnS=`<li>
							<div>`+obj.partsName+`</div> 
							<div>`+obj.brandName+`</div>
							<div>`+obj.partsType+`</div>
							<div>`+Number(obj.price).toFixed(2)+`元/`+obj.unitName+`</div>
						</li>`;
					$("#checkedPartsOwn").append(strOwnS);
				}else{//未选中
					partsCheckedOwnAf.splice(indexVal,1);
					$("#checkedPartsOwn>li").eq(indexVal).remove();
				};
                accountPartsAf=[];
                $.each(partsCheckedOwnAf,function(index,value){
                    accountPartsAf.push(value);
                });
				layui.use(['form','code'], function () {
					var form = layui.form,
						$ = layui.$;
					// 代码块
					layui.code({
						title: 'html',
						encode: true,
						about: false
					});
					// 手动赋值
					let idArr=[];
					$.each(partsCheckedOwnAf,function(index,value){
						idArr.push(value.partsCode);
					});
					$('select[name="配件名称搜索"]').val(idArr);
					form.render();
				});
			});
			//---删除配件-----配件库---ok
			$("#checkedPartsOwnShowJs").on('click','.deletePartOwn',function(){
				let indexVal=$("#checkedPartsOwnShowJs .deletePartOwn").index($(this));
                accountPartsAf.splice(indexVal,1);
                accountParts=accountPartsAf;
				$("#checkedPartsOwnShowJs li").eq(indexVal).remove();
				if($("#checkedPartsOwnShowJs>li").length>0){
					$("#checkedPartsOwnShowJss").show();
				}else{
					$("#checkedPartsOwnShowJss").hide();
				}
			});
			//---修改配件---输入框变化---存值---配件库配件-ok
			$("#checkedPartsOwnShowJs").on('change','input',function(){
				let titleVal=$(this).attr('title');
				let valNow=$(this).val();
				if(titleVal=='oneNumber'){
					valNow=Number(valNow).toFixed(1)<=0?0.0:Number(valNow).toFixed(1);
				}else if(titleVal=='twoNumber'){
					valNow=Number(valNow).toFixed(1)<=0?0.00:Number(valNow).toFixed(2);
				};
				$(this).val(valNow);
				let indexVal=$("#checkedPartsOwnShowJs input").index($(this));
                accountPartsAf[indexVal].partsNum=valNow;
			});
			//保留1位或2位小数--并且强制去除空格
			$("body").on('input','input',function(){
				let titleVal=$(this).attr('title');
				let valNow=$(this).val();
				if(titleVal=='oneNumber'){
					valNow =valNow.replace(/[^\d.]/g,'');
					$(this).val(valNow);
				}else if(titleVal=='twoNumber'){
					valNow =valNow.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
					$(this).val(valNow);
				};
			});
			//保留1位或2位小数--并且强制去除空格
			$("body").on('blur','input',function(){
				// let typeVal=$(this).attr('type');
				let titleVal=$(this).attr('title');
				let valNow=$(this).val();
				if(titleVal=='oneNumber'||titleVal=='twoNumber'){
					valNow = valNow.replace(/\s*/g,"");
				}else{
					valNow = valNow.trim();
				};
				if(titleVal=='oneNumber'){
					valNow =valNow.replace(/[^\d.]/g,'');
					valNow=Number(valNow).toFixed(1);
				}else if(titleVal=='twoNumber'){
					valNow =valNow.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
					valNow=Number(valNow).toFixed(2);
				};
				$(this).val(valNow);
				let nameVal=$(this).attr('name');
				if(nameVal=="jsfas"){
					let a=Number($("#otherMoneyJs").val()),//其他费用
						b=Number($("#urgentMoneyJs").val()),// 加急费用
						c=Number($("#indoorJs").val()),//上门费用
						d=Number($("#partsTotalCostJs").val()),//配件费用
						e=Number($("#doorOverJs").val()),//上门超时费用
						f=Number($("#linkOverJs").val()) ,//联系超时费用 ,
						g=Number($("#backOverJs").val()),//反馈超时费用 ,
						h=Number($("#partOverJs").val());//配件超时费用 ,
					$("#partsFeeJs").text(Number(a+b+c+d).toFixed(2));//工单费用-未扣除
					let deleteFeeVal=Number(e+f+g+h).toFixed(2);
					if(deleteFeeVal>0){
						deleteFeeVal='-'+deleteFeeVal;
					};
					$("#deleteFee").text(deleteFeeVal);//扣除费用
				};
			});
			//禁止弹窗与浏览器滚动
			$("body").on('focus','textarea',function(){
				flagInput=true;
			});
			$("body").on('blur','textarea',function(){
				flagInput=false;
			});
			// 维修方案内容复制内，粘贴时操作
			$("#addMatinText").on('paste', function (e) {
				// textInit(e)
			})
			// 维修方案内容复制做处理
			$("#addMatinText").bind('keyup', function (e) {
				let htmlText = $("#addMatinText").text();
				$(this).parent('div').find('.inputNums em').eq(0).text(htmlText.length)
			})
			$("#addMatinText").focus(function (e) {
				flagInput = true
				let htmlText = $("#addMatinText").text();
				$(this).parent('div').find('.inputNums em').eq(0).text(htmlText.length)
			})
			// 处理维修方案内容
			$("#addMatinText").blur(function(){
				flagInput = false
				let htmlText = $("#addMatinText").html();
				htmlText = htmlText.replace(new RegExp('div','g'), 'span')
				$("#addMatinText").html(htmlText)
				let TextNum = $("#addMatinText").text().length;
				$(this).parent('div').find('.inputNums em').eq(0).text(TextNum)
                if(TextNum>800){
                    let msg = `维修方案输入内容太长啦，限制800字！`;
                    utils.showMessage(msg);
                }
			})
			//统计数字--textarea
			$("body").on('input','textarea',function(){
				let nums=$(this).val().length;
				$(this).parent('div').find('em').eq(0).text(nums);
			});
			//-配件库按类型搜索-点击事件-二级
			$("#secondPartsType").on('click','button',function(){
				$("#secondPartsType button").removeClass('btn-info');
				$(this).addClass('btn-info');
				let idVal=$(this).attr('name');
				partsSecondIdVal=idVal;//创建维修方案-配件-二级类型ID
				maintenPlan_list.queryOrderParts(partsFirstIdVal,partsSecondIdVal);
			});
			//配件库按类型搜索-点击事件-一级
			$("#firstPartsType").on('click','button',function(){
				$("#firstPartsType button").removeClass('btn-info');
				$(this).addClass('btn-info');
				let idVal=$(this).attr('name');
				partsFirstIdVal=idVal;//创建维修方案-配件-一级类型ID
				partsSecondIdVal='';
				maintenPlan_list.queryOrderPartsType(2,idVal);
			});
			//添加配件-配件库
			$(".addParts").on('click',function(){
                maintenPlan_list.queryOrderParts('','');//查询配件-根据配件名称全局搜索
				maintenPlan_list.queryOrderPartsType();//查询配件类型---一级
				let strOwnS='';
				$.each(accountParts,function(index,value){
					strOwnS+=`<li>
							<div>`+value.partsName+`</div>
							<div>`+value.brandName+`</div>
							<div>`+value.partsType+`</div>
							<div>`+value.price+`元/`+value.unitName+`</div>
						</li>`;
				});
				$("#checkedPartsOwn").empty().append(strOwnS);
				flagInput=false;
                partsFlag = layer.open({
					type: 1,
					area: ['99%', '99%'],
					title: '添加配件',
					content: $('#parts'),
					btn: ['保存', '取消'],
					yes: function(index, layero) {
						accountParts=[];
                        $.each(accountPartsAf,function(index,value){
                            accountParts.push(value);
                        });
						if(accountParts.length>0){
							$("#checkedPartsOwnShows").show();
							$("#checkedPartsOwnShowJss").show();
						}else{
							$("#checkedPartsOwnShows").hide()
							$("#checkedPartsOwnShowJss").hide();
						};
						let str=``;
						$.each(accountParts,function(index,value){
							str+=`<li>
									<div class="b">`+value.partsName+`</div>
									<div class="a">`+value.brandName+`</div>
									<div class="a">`+value.partsType+`</div>
									<div class="a">`+Number(value.price).toFixed(2)+`元</div>
									<div class="a"><input class="num" type="number" onmousewheel="return false;" min=0 title="oneNumber" id="`+value.id+`" value="`+value.partsNum+`">`+value.unitName+`</div>
									<div class="deletePartOwn" name="del">✖</div>
								</li>`;
						});
                        // 配件展示
                        $("#checkedPartsOwnShowJss").show();
                        $("#checkedPartsOwnShowJs").empty().append(str);
                        layer.close(partsFlag);
						$("#parts").hide();
					},
					btn2: function(index, layero) {
                        accountPartsAf=[];
                        $.each(accountParts,function(index,value){
                            accountPartsAf.push(value);
                        });
                        layer.close(partsFlag);
						$("#parts").hide();
					},
					cancel: function(index, layero) {
                        layer.close(partsFlag);
						$("#parts").hide();
					},
				});
			});
			//编辑删除
			$("#table").on('click',"button",function(){
				let strTitle='';
				let strCon=null;
				let msgSta=$(this).attr('name');
				let w='100%';
				let h='100%';
				selectId = $(this).attr('data-id');
				if(msgSta=='edit'){
					maintenPlan_list.addWorkOrder(2);
				};
				if(msgSta=='del'){
					strTitle='删除';
					utils.showConfirm('此模板删除后，此列表将不存在，您是否确认删除？', '确定', function() {
						maintenPlan_list.delFn();//上门
					});
					return;
				};
				flagInput=false;
			});
			// 添加变量事件---变量类型与变量格式选择
			$("#addUnitType").on('change',function(){
				let unitVal=$(this).val();
				unitVal=Number(unitVal)
				// console.log(unitVal)
				if(unitVal==1){
					$("#selectUnitCon").hide();
					$("#inputUnitCon").show();
				}else if(unitVal==2){
					$("#selectUnitCon").show();
					$("#inputUnitCon").hide();
				}else{
					$("#selectUnitCon").hide();
					$("#inputUnitCon").hide();
				}
			});
			// 添加变量事件---添加变量格式
			$("#addRow").on('click',function(){
				// console.log('添加')
				let indexVal = $("#inputUnitConText>div").length;
				indexVal++;
				let appendStr = `<div class="form-group removeDiv">
									<label class="col-sm-3 control-label">选项`+indexVal+`</label>
									<div class="col-sm-5"><input name="" class="form-control" autocomplete="off"></div>
									<div class="col-sm-3"><em class="remove"><i class="fa fa-trash-o"></i></em></div>
								</div>`
				$("#inputUnitConText").append(appendStr)
			})
			//添加变量事件---移除变量格式
			$("#inputUnitConText").on('click','.remove',function(){
				// console.log('移除')
				$(this).parents('.removeDiv').remove()
				$("#inputUnitCon .removeDiv").each(function(i,v){
					let optNum = i+3;
					$("#inputUnitCon .removeDiv").eq(i).find('label').eq(0).text('选项'+optNum)
				})
			})
		},
		//初始化请求
		initList: function() {
			//订单列表
			$("#table").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height:'auto',//utils.getAutoGridHeightSong(),
				autowidth: true,
				shrinkToFit:false,
				autoScroll: true,
				rowNum: 10,
				rownumbers: false,
				rownumWidth:80,
				// rowList: [10,20,50,100],
				pgtext: "第X页 / 共X页",
				colNames: ['id','操作','模板编号',"模板名称","创建时间","创建人","更新时间","操作人","备注"],
				colModel: [
					{
						name: "id",
						index: "id",
						width: 84,
						hidden:true,
					},
					{
						name: "servicePlanId",
						index: "servicePlanId",
						editable: false,
						width: 140,
						formatter: function(cellvalue,options, rowObject) {
							return '<button name="edit" class="btn btn-primary unified-btn-inline unified-change-btn-inline" type="button"data-id="'+rowObject.id+'">编辑</button>' +
								'<button name="del" class="btn btn-danger unified-btn-inline unified-deleted-btn-inline" type="button" data-id="'+rowObject.id+'">删除</button>'
						},
					},
					{
						name: "id",
						index: "id",
						width: 84,
						key:true,
						formatter: function(cellvalue,options, rowObject) {
							if(!cellvalue){
								cellvalue='';
							}else{
								if(Number(cellvalue)<10){
									cellvalue='0'+cellvalue;
								}
							};
							return cellvalue;
						},
					},
					{
						name: "name",
						index: "name",
						editable: false,
						width: 150,
						sorttype: "string",
					},
					{
						name: "createTime",
						index: "createTime",
						editable: false,
						width: 220,
						sorttype: "string",
						formatter: timerFormatter
					},
					{
						name: "createUser",
						index: "createUser",
						editable: false,
						width: 84,
						sorttype: "string",
					},
					{
						name: "modifyTime",
						index: "modifyTime",
						editable: false,
						width: 220,
						sorttype: "string",
						formatter: timerFormatter
					},
					{
						name: "modifyUser",
						index: "modifyUser",
						editable: false,
						width: 84,
						sorttype: "string",
					},
					{
						name: "remark",
						index: "remark",
						editable: false,
						width: 200,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							if(!cellvalue){
								cellvalue='';
							}else{
								if(cellvalue.length>10){
									cellvalue=cellvalue.substring(0,9)+'...';
								}
							};
							return cellvalue;
						},
					},
				],
				pager: "#table_pager",
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
				pgbuttons:true,
				pginput:true,
				onPaging: function(pgButton) {
					deferLoadData = 0;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					lastPage = $("#table").jqGrid('getGridParam', 'lastpage');
					if(pgButton == 'next') {
						currentPage = currentPage + 1;
					}
					if(pgButton == 'last') {
						currentPage = lastPage;
					}
					if(pgButton == 'prev') {
						currentPage = currentPage - 1;
					}
					if(pgButton == 'first') {
						currentPage = 1;
					}
					if(pgButton == 'user') {
						deferLoadData = 1;
					}
					if(pgButton == 'records') {
						deferLoadData = 1;
					}
					if(deferLoadData == 0) {
						maintenPlan_list.getServerData(currentPage);
					}
				},
				beforeRequest: function() {
					if(deferLoadData == 1) {
						maintenPlan_list.getServerData(currentPage);
						deferLoadData = 0;
					}
				},
				onSortCol: function(index, iCol, sortorder) {

				},
				//当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
				onSelectRow: function () {
					$("#contanerDetail>div").hide();//展示信息-隐藏
					//返回选中的id
					// selectId = $("#" + this.id).getGridParam('selrow');
				},
				gridComplete: function() {
					$("#table").jqGrid('resetSelection');
					$("#table").jqGrid('setSelection',selectId);
				}
			});
			$("#table").jqGrid("navGrid", "#table_pager", {
				edit: false,
				add: false,
				del: false,
				search: false,
				refresh: false,
			});
			maintenPlan_list.getServerData(1);
			$(window).bind("resize", function () {
				var b = $(".jqGrid_wrapper").width();
				if(b>=1844){
					b=1844;
				};
				$("#table").setGridWidth(b);
			});
		},
		refreshServerData: function() {
			maintenPlan_list.getServerData(1);
		},
		clearAddEditVal:function(){
			$("#addName").val('');
			$("#addFaultRea").val('');
			// $("#addMaint").val('');
			$("#addUnitName").val(''); //清空名变量称
			$("#addUnitType").val(''); //清空变量类型
			$("#selectUnitCon").hide();
			$("#inputUnitCon").hide();
			$("#selectUnitConGeshi").val('1'); //清空变量格式（输入类型）
			// $("#inputUnitConText")  // 清空变量格式（选择类型）选项1，2

			$("#inputUnitConText .removeDiv").each(function(i,k){
				$("#inputUnitConText .removeDiv").eq(i).remove()
			})
			$("#inputUnitConText").find('input').eq(0).val('')
			$("#inputUnitConText").find('input').eq(1).val('')
			$("#addMatinText").html('');
			$("#addNote").val('');
			$("#addClassify").val('');
            partsFirstIdVal='';//一级配件id
            partsSecondIdVal='';//二级配件id
            accountParts = [];//所有配件
            accountPartsAf=[];//
			$("#checkedPartsOwnShowJss").hide();
			$("#checkedPartsOwnShowJs").empty();
		},
		delFn:function(){
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				id:selectId
			};
			win.utils.ajaxPost(win.utils.services.orderMan_config_delFeedbackTem, datas, function(result) {
				if(result.result_code == 200) {
                    utils.showConfirmWithCabackSong('删除成功',closeFlagCon());
				}else{
					let msg= result.description?result.description:'操作失败，请稍后重试！';
					utils.showMessage(msg);
				};
			})
		},
		// 获取光标位置
		getPos:function () {
			let obj = null
			let val = ''
			if(window.getSelection()){
				obj = window.getSelection()
				pos =obj.getRangeAt(0);
			}else if(document.selection){
				obj = document.selection.createRange()
				pos = obj.htmlText
			}
			alert(obj)
			alert(pos)
		},
	     // 光标位置插入内容
	     fn_insertPos: function () {
			if(pos!=null) {
				pos.text="插入文字内容";
				//pos.pasteHTML("<font color='red'>文字内容</font>");
				//pos.pasteHTML("<br/>文本框控件:<input id='Text1' type='text' />");
				//释放位置
				pos=null;
			} else {
				alert("没有正确选择位置");
			}
		},
		//订单list----getData
		getServerData: function(pageIndex) {
			selectId='';
			selectId='';//清空订单id
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				position: pageIndex,
				rows: 10,
			};
			pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
			win.utils.ajaxPost(win.utils.services.orderMan_config_listFeedbackTem, datas, function(result) {
				if(result.result_code == 200) {
					if(result.total == 0) {
						gridData = {};
						var gridJson = {
							total: 0,
							rows: gridData,
							page: 0,
							records: result.total
						};
						pageText = '第0页 / 共0页';
						nowCurPage= 0;//订单列表---当前页码数
						totalNowPages = 0;//订单列表---总的页码数
						$("#table").jqGrid('setGridParam', {
							pgtext: pageText
						});
						$("#table")[0].addJSONData(gridJson);
					} else {
						gridData = result.lists;
						if(gridData.length>0){
							selectId=gridData[0].id;
						}else{
							selectId='';
						};
						totalPages = Math.ceil(result.total / pageCount);
						nowCurPage= pageIndex;//订单列表---当前页码数
						totalNowPages = totalPages;//订单列表---总的页码数
						var gridJson = {
							total: totalPages,
							rows: gridData,
							page: pageIndex,
							records: result.total
						};
						// pageText = '第<input id="inputNowPages" type="number" min=1 value="'+pageIndex+'">页 / 共' + totalPages + '页';
						pageText = '第'+pageIndex+'页 / 共' + totalPages + '页';
						$("#table").jqGrid('setGridParam', {
							pgtext: pageText
						});
						$("#table")[0].addJSONData(gridJson);
					}
				} else {
					let msg= result.description?result.description:'操作失败，请稍后重试！';
					utils.showMessage(msg);
				}
			});
		},
	};
	$("#addName").on('mouseout',function(){
		let str=$(this).val();
		// console.log(str,'999')
		if($.trim(str).length<=0){
			$("#addName").parent('div').find('em').eq(0).text('请注意模板名称为必填项！');
		}else if($.trim(str).length>10){
			let strVal=$.trim(str).substring(0,10);
			$(this).val(strVal);
			$("#addName").parent('div').find('em').eq(0).text('请注意模板名称不可超过10个字！');
		}else{
			$("#addName").parent('div').find('em').eq(0).text('');
		};
	});
    $("#addName").on('blur',function(){
    	let str=$(this).val();
		if($.trim(str).length<=0){
			$("#addName").parent('div').find('em').eq(0).text('请注意模板名称为必填项！');
		}else if($.trim(str).length>10){
			let strVal=$.trim(str).substring(0,10);
			$(this).val(strVal);
			$("#addName").parent('div').find('em').eq(0).text('请注意模板名称不可超过10个字！');
		}else{
			$("#addName").parent('div').find('em').eq(0).text('');
		};
	});
    // 维修方案模板-插入变量操作
	$("#formAdd").on('click','#addUnit',function(){
		flagInput=false;
		let appendOptVal = `<div class="form-group">
								<label class="col-sm-3 control-label">选项1</label>
								<div class="col-sm-5"><input name="" class="form-control" autocomplete="off"></div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">选项2</label>
								<div class="col-sm-5"><input name="" class="form-control" autocomplete="off"></div>
							</div>`
		$("#inputUnitConText").empty().append(appendOptVal)
		$("#addUnitName").val('')
        $("#selectUnitCon").hide();
        $("#inputUnitCon").hide();
        $("#addUnitType").val('')
		partsFlag = layer.open({
			type: 1,
			area: ['80%', '88%'],
			title: '  ',
			content: $('#partUnitCon'),
			btn: ['确定', '取消'],
			yes: function(index, layero) {
				let nameVal = '' // 变量名称
				let typeVal = '' // 变量类型- 1：选择  2：输入
                let blTextVal = ''// 变量html文本
                let blTypeVal = ''// 变量格式类型（输入类型才有-1，2，3-）
				let modeVal = [] //  变量格式

                nameVal=$("#addUnitName").val();
				if($.trim(nameVal).length<=0){
					utils.showMessage('请输入变量名称！');
					return;
				}else if($.trim(nameVal).length>30){
					utils.showMessage('已输入的变量名称不可超过30个字！');
					return;
				};
				checkIsChEnNum(nameVal);
				if(unitBlFlag == false){
					utils.showMessage('请输入正确的变量名称！');
					return;
				}
                typeVal = Number($("#addUnitType").val())
				if(typeVal == 0){
					utils.showMessage('请选择变量类型！');
					return;
				}else if(typeVal == 2){
                    blTypeVal = Number($("#selectUnitConGeshi").val())
				}else if(typeVal == 1){
					let iVal = [];  // 类型不对
					let overValLen = []; // 长度过长
					$("#inputUnitConText input").each(function(i,v){
						let index_val = i + 1;
						let patternStr = /^[A-Za-z0-9\u4e00-\u9fa5]+$/gi;
						// console.log(i,v)
						let vals = $("#inputUnitConText input").eq(i).val()
						// console.log(vals)
						if(!patternStr.test(vals) || vals == ''){
							// console.log(index_val)
							iVal.push(index_val)
						}
						if(vals.length>30){
							// console.log(index_val)
							overValLen.push(index_val)
						}
						modeVal.push(vals)
					})
					if(iVal.length > 0){
						// console.log(iVal,'999')
						utils.showMessage('选项'+iVal[0]+'请输入正确的变量类型！');
						return;
					}
				    if(overValLen.length >0){
						// console.log(iVal,'999')
						utils.showMessage('选项'+overValLen[0]+'输入的变量过长！');
						return;
					}
				};
				let myOption = {
					nameVal: nameVal, // 变量名称
                    attrType:typeVal, // 变量类型 1：选择（前提：选择-1-类型-下拉选择）  2：输入
                    textType:blTypeVal, //  变量格式: (前提：输入-2-类型 数值代表 1:不限，2:仅数字 3：仅文字)
                    val:modeVal.join(',') // 数组值
				}
                let jsonToStr = JSON.stringify(myOption)
                // console.log(myOption,jsonToStr,'9988',typeof(jsonToStr))
				if(typeVal == 2){
					if(blTypeVal == 1){
						blTextVal = `<em name="`+nameVal+`"><i class="highLight" name='`+jsonToStr+`' contenteditable='false'>【&数字or文字】</i></em>`
					}else if(blTypeVal == 2){
						blTextVal = `<em name="`+nameVal+`"><i class="highLight" name='`+jsonToStr+`' contenteditable='false'>【&数字】</i></em>`
					} else if(blTypeVal == 3){
						blTextVal = `<em name="`+nameVal+`"><i class="highLight" name='`+jsonToStr+`' contenteditable='false'>【&文字】</i></em>`
					}
				}else{
				    let selText =``;
					for(let j=0;j<modeVal.length;j++){
						let line_i = '/'
						if(j == (modeVal.length-1)){
							line_i = ''
						}
                        selText += modeVal[j]+line_i
					}
					blTextVal = `<em name="`+nameVal+`"><i class="highLight" name='`+jsonToStr+`' contenteditable='false'>【&`+selText+`】</i></em>`
				}
				// console.log($("#addMatinText").html())
				// console.log(blTypeVal)
				let contextVal = $("#addMatinText").html() + blTextVal
				$("#addMatinText").html(contextVal)
				// console.log('确定')
				layer.close(partsFlag);
				$("#addMatinText").focus()
				let htmlText = $("#addMatinText").text();
				$("#addMatinText").parent('div').find('.inputNums em').eq(0).text(htmlText.length)
			},
			btn2: function(index, layero) {
				$("#addMatinText").focus()
				layer.close(partsFlag);
			},
			cancel: function(index, layero) {
				$("#addMatinText").focus()
				layer.close(partsFlag);
			},
		});
	})
	// 阻止空格与enter键触发layer.open事件（即出现蒙层）
	$(document).keydown(function (event) {
		var  e=window.event || event;
		var keyCode = event.keyCode;
		if (keyCode == 13 || keyCode == 32) {
			if(flagInput==false){
				if(e.preventDefault){
					e.preventDefault();
				}else{
					window.event.returnValue = false;
				}
				return false;
			}
		}
	});
	win.maintenPlan_list = maintenPlan_list;
})(window, jQuery);
