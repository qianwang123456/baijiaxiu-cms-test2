/**
 * Created by hanlu123
 */
(function (win, $, qiniu) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    var nowPage = 1;
    // 维修员列表当前选择id
    var selectedRowData,currentRuleCode,currentId;
    var partsChooseArr = []; //添加的配件

    var postData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        position: nowPage,
        rows: 10
    };


    var autoClear = {
        //初始化
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            //判断是否有维修员权限，此时为维修员管理的维修员列表权限
            utils.isUserMenuIndexRight('AutoSettlementRuleConfig', function (result) {
                if (result == 1) {
                    //initialize...
                    win.utils.clearSpaces();
                    obj.initList();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });


            $('#AllBtnAction button').on('click',function () {
                var index = $(this).index();
                $('#AllBtnAction button').removeClass('active').eq(index).addClass('active');

                $('#contanerDetail>div').hide().eq(index).show();

                switch (index) {
                    case 0:
                        autoClear.getOptionLog();
                        break;
                    case 1:
                        autoClear.getCurrentStatus();
                        break;
                }
            });

            // autoClear.getFitstPartsType();
            // autoClear.getSearchParts();


            // 一级配件类型点击事件
            $('.first-parts-type .itemBox').on('click','div',function(){
                var index=$(this).index();
                $('.first-parts-type .itemBox').find('div').removeClass('checked').eq(index).addClass('checked');

                var id=$(this).attr('data-id');
                autoClear.getSecondPartsType(id);
            });
            // 二级配件类型点击事件
            $('.second-parts-type .itemBox').on('click','div',function () {
                var index=$(this).index();
                $('.second-parts-type .itemBox').find('div').removeClass('checked').eq(index).addClass('checked');

                var firstTypeId=$(this).attr('data-firstTypeId'),
                    secondTypeId=$(this).attr('data-secondTypeId');
                autoClear.getParts(firstTypeId,secondTypeId);
            });

            // 添加配件
            $('.add-parts-btn').on('click', function () {
                currentId=null;
                var arr=[].concat(partsChooseArr);
                autoClear.chooseParts(arr);
            });

            // 添加配件--编辑
            $('.edit-parts-btn').on('click', function () {
                var arr=[].concat(partsChooseArr);
                autoClear.chooseParts(arr);
            });


            // 新增规则费用区间校验
            $('#free_start').on('input',function(){
                var free_start=$(this).val();
                if(free_start){
                    var can_start=autoClear.checkDecimals(free_start);
                    if(!can_start){
                        if(free_start<=0){
                            $(this).val('0')
                        }else{
                            $(this).val(free_start.split('.')[0]+'.'+free_start.split('.')[1].slice(0,1))
                        }
                    }
                }
            });
            $('#free_start').on('blur',function(){
                var free_start=$(this).val();
                var free_end=$('#free_end').val();

                if(free_end){
                    if(Number(free_start)>=Number(free_end)){
                        utils.showMessage('最高费用应高于最低费用！');
                        $(this).val('')
                    }
                }
            });
            $('#free_end').on('input',function(){
                var free_end=$(this).val();
                if(free_end){
                    var can_end=autoClear.checkDecimals(free_end);
                    if(!can_end){
                        if(free_end<=0){
                            $(this).val('0')
                        }else{
                            $(this).val(free_end.split('.')[0]+'.'+free_end.split('.')[1].slice(0,1))
                        }
                    }
                }
            });
            $('#free_end').on('blur',function(){
                var free_end=$(this).val();
                var free_start=$('#free_start').val();
                if(free_start){
                    if(Number(free_start)>=Number(free_end)){
                        utils.showMessage('最高费用应高于最低费用！');
                        $(this).val('')
                    }
                }
            });

            // 编辑规则费用区间校验
            $('#edit_minCost').on('input',function(){
                var free_start=$(this).val();
                if(free_start){
                    var can_start=autoClear.checkDecimals(free_start);
                    if(!can_start){
                        if(free_start<=0){
                            $(this).val('0')
                        }else{
                            $(this).val(free_start.split('.')[0]+'.'+free_start.split('.')[1].slice(0,1))
                        }
                    }
                }
            });
            $('#edit_minCost').on('blur',function(){
                var free_start=$(this).val();
                var free_end=$('#edit_maxCost').val();
                if(free_end){
                    if(Number(free_start)>=Number(free_end)){
                        utils.showMessage('最高费用应高于最低费用！');
                        $(this).val('')
                    }
                }
            });
            $('#edit_maxCost').on('input',function(){
                var free_end=$(this).val();
                if(free_end){
                    var can_end=autoClear.checkDecimals(free_end);
                    if(!can_end){
                        if(free_end<=0){
                            $(this).val('0')
                        }else{
                            $(this).val(free_end.split('.')[0]+'.'+free_end.split('.')[1].slice(0,1))
                        }
                    }
                }
            });
            $('#edit_maxCost').on('blur',function(){
                var free_end=$(this).val();
                var free_start=$('#edit_minCost').val();
                if(free_start){
                    if(Number(free_start)>=Number(free_end)){
                        utils.showMessage('最高费用应高于最低费用！');
                        $(this).val('')
                    }
                }
            });

            layui.use('form',function () {
                var form=layui.form;
                // 新增添加配件
                form.on('radio(is_parts)', function(data){
                    var is_parts=data.value;
                    if(is_parts==="false"){
                        $('.add-parts-btn').attr('disabled',true);
                    }else{
                        $('.add-parts-btn').removeAttr('disabled');
                    }
                });

                // 修改添加配件
                form.on('radio(edit_is_parts)', function(data){
                    var is_parts=data.value;
                    if(is_parts==="false"){
                        $('.edit-parts-btn').attr('disabled',true);
                    }else{
                        $('.edit-parts-btn').removeAttr('disabled');
                    }
                });
            })


            // 保存
            $('#save').on('click',function(){
                var data={
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                };

                var free_start=$('#free_start').val();
                var free_end=$('#free_end').val();
                var is_urgent=$("input[name='is_urgent']:checked").val();
                var is_other=$("input[name='is_other']:checked").val();
                var is_parts=$("input[name='is_parts']:checked").val();
                var remark=$('#remark').val();

                if(!free_start || !free_end){
                    utils.showMessage('请填写订单费用区间！');
                    return;
                }
                if(is_urgent!=="false" && is_urgent!=="true"){
                    utils.showMessage('请选择是否含加急费！');
                    return;
                }
                if(is_other!=="false" && is_other!=="true"){
                    utils.showMessage('请选择是否含其他费用！');
                    return;
                }
                if(is_parts!=="false" && is_parts!=="true"){
                    utils.showMessage('请选择是否有配件！');
                    return;
                }
                else{
                    if(is_parts=='false'){
                        partsChooseArr=[];
                    }
                    else if(is_parts=='true'){
                        if(!partsChooseArr.length){
                            utils.showMessage('请选择配件！');
                            return;
                        }
                    }
                }

                data.maxCost=free_end;
                data.minCost=free_start;
                data.isIncludeUrgentMoney=is_urgent;
                data.isIncludeOtherMoney=is_other;
                data.isIncludeParts=is_parts;
                data.accountParts=partsChooseArr;
                data.remark=remark;

                win.utils.ajaxPost(win.utils.services.saveAutoAccountRule,data,function (result) {
                    if(result.result_code==200){
                        utils.showMessage('保存成功！');
                        autoClear.getServerData(nowPage,postData);
                        autoClear.saveConInit();
                        partsChooseArr=[];
                    }
                    else{
                        utils.showMessage(result.description);
                    }
                })
            });


            // 表格修改
            $('#table').on('click','#btnUpdate',function (e) {
                e.stopPropagation(); //阻止事件冒泡

               var effectiveStatus=2;
                currentRuleCode=$(this).attr('data-ruleCode');
                currentId=$(this).attr('data-id');

               autoClear.getRule(effectiveStatus);

                var i = layer.open({
                    type: 1,
                    area: ['80%', '60%'],
                    title: "规则修改",
                    content: $('#editLayer'),
                    shadeClose: true,
                    btn: ['修改', '取消'],
                    success: function () {
                        $('.layui-layer-page').css('zIndex', 333);
                        $('.layui-layer-shade').css('zIndex', 300);
                    },
                    yes: function (index, layero) {
                        var data={
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                        };

                        var free_start=$('#edit_minCost').val();
                        var free_end=$('#edit_maxCost').val();
                        var is_urgent=$("input[name='edit_is_urgent']:checked").val();
                        var is_other=$("input[name='edit_is_other']:checked").val();
                        var is_parts=$("input[name='edit_is_parts']:checked").val();
                        var remark=$('#edit_remark').val();

                        if(!free_start || !free_end){
                            utils.showMessage('请填写订单费用区间！');
                            return;
                        }
                        if(is_urgent!=="false" && is_urgent!=="true"){
                            utils.showMessage('请选择是否含加急费！');
                            return;
                        }
                        if(is_other!=="false" && is_other!=="true"){
                            utils.showMessage('请选择是否含其他费用！');
                            return;
                        }
                        if(is_parts!=="false" && is_parts!=="true"){
                            utils.showMessage('请选择是否有配件！');
                            return;
                        }
                        else{
                            if(is_parts=='false'){
                                partsChooseArr=[];
                            }
                            else if(is_parts=='true'){
                                if(!partsChooseArr.length){
                                    utils.showMessage('请选择配件！');
                                    return;
                                }
                            }
                        }

                        data.maxCost=free_end;
                        data.minCost=free_start;
                        data.isIncludeUrgentMoney=is_urgent;
                        data.isIncludeOtherMoney=is_other;
                        data.isIncludeParts=is_parts;
                        data.accountParts=is_parts=='false'?[]:partsChooseArr;
                        data.remark=remark;
                        data.effectiveStatus=2;
                        data.ruleCode=currentRuleCode;
                        data.id=currentId;


                        autoClear.updateRule(data);
                    },
                    end:function () {
                        autoClear.saveConInit();
                    }
                });
            });

            // 表格启用禁用
            $('#table').on('click','.changeReceiptStatus',function (e) {
                e.stopPropagation(); //阻止事件冒泡

                var status=$(this).attr('data-status');
                currentRuleCode=$(this).attr('data-ruleCode');
                autoClear.updateRuleStatus(status);
            });

            $(document).keydown(function (event) {
                var e = window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if (flagInput == false) {
                        if (e.preventDefault) {
                            e.preventDefault();
                        } else {
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });
        },

        // 获取一级配件类型
        getFitstPartsType:function(){
            win.utils.ajaxPost(win.utils.services.queryPartsType,{
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 1,
                status:1
            },function (result) {
                if(result.result_code==200){
                    if(result.configPartsTypes.length){
                        var firstPartsStr='';
                        result.configPartsTypes.forEach(function (i,k) {
                            firstPartsStr += '<div data-id="'+i.firstTypeId+'">'+i.firstTypeName+'</div>'
                        });
                        $('.first-parts-type .itemBox').html(firstPartsStr);

                        $('.first-parts-type .itemBox>div').eq(0).addClass('checked');
                        var id=$('.first-parts-type .itemBox>div').eq(0).attr('data-id');
                        autoClear.getSecondPartsType(id);
                    }
                }
            })
        },
        // 获取二级配件类型
        getSecondPartsType:function(id){
            win.utils.ajaxPost(win.utils.services.queryPartsType,{
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 2,
                status:1,
                firstTypeId:id
            },function (result) {
                if(result.result_code==200){
                    if(result.configPartsTypes.length){
                        var secondPartsStr='';
                        result.configPartsTypes.forEach(function (i,k) {
                            secondPartsStr += '<div data-firstTypeId="'+i.firstTypeId+'" data-secondTypeId="'+i.secondTypeId+'">'+i.secondTypeName+'</div>'
                        });
                        $('.second-parts-type .itemBox').html(secondPartsStr);

                        $('.second-parts-type .itemBox>div').eq(0).addClass('checked');
                        var firstTypeId=$('.second-parts-type .itemBox>div').eq(0).attr('data-firsttypeid'),
                            secondTypeId=$('.second-parts-type .itemBox>div').eq(0).attr('data-secondtypeid');
                        autoClear.getParts(firstTypeId,secondTypeId);
                    }
                    else{
                        $('.second-parts-type .itemBox').html('');
                        $('.parts .itemBox').html('');
                        // utils.showMessage('暂无二级配件类型！')
                    }
                }
            })
        },
        // 获取配件
        getParts:function(firstTypeId,secondTypeId){
            win.utils.ajaxPost(win.utils.services.autoClear_queryParts,{
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                firstTypeId,
                secondTypeId,
                id:currentId||null,
                rank:2,
                status:1
            },function (result) {
                if(result.result_code==200){
                    if(result.parts.length){
                        var partsStr='';
                        result.parts.forEach(function (i,k) {
                            var partsInfo=JSON.stringify(i);
                            partsStr+='<div class="partsItem" data-obj=\''+ partsInfo +'\'>\n' +
                                '                            <div class="partsName">'+i.partsName+'</div>\n' +
                                '                            <div>品牌：<span>'+i.brandName+'</span></div>\n' +
                                '                            <div>型号：<span>'+i.partsType+'</span></div>\n' +
                                '                            <div>配件费：<span>'+i.price+'</span></div>\n' +
                                '                        </div>'
                        });

                        $('.parts .itemBox').html(partsStr);

                        if(partsChooseArr.length){
                            autoClear.showParts(partsChooseArr);
                        }
                    }
                    else{
                        // utils.showMessage('暂无配件！');
                        $('.parts .itemBox').html('');
                    }
                }
            })
        },
        // 获取搜索配件
        getSearchParts:function(){
            win.utils.ajaxPost(win.utils.services.autoClear_queryParts,{
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                id:currentId||null,
                rank:2,
                status:1
            },function (result) {
                if(result.result_code==200){
                    if(result.parts.length){
                        var partsStr='<option value="">选择配件</option>';
                        result.parts.forEach(function (i,k) {
                            var partsInfo=encodeURI(JSON.stringify(i));
                            partsStr+='<option value="'+i.partsCode+'" data-partsInfo="'+partsInfo+'">'+i.partsName+'</option>'
                        });

                        $('#searchParts').html(partsStr);

                        layui.use('form', function () {
                            form = layui.form;
                            form.render();
                        });

                        if(partsChooseArr.length){
                            autoClear.showParts(partsChooseArr);
                        }
                    }
                }
            })
        },

        // 选择配件
        chooseParts:function(arr){
            autoClear.getSearchParts();
            autoClear.getFitstPartsType();

            autoClear.showParts(arr);

            var i = layer.open({
                type: 1,
                area: ['100%', '100%'],
                title: "添加配件",
                content: $('#addPartsLayer'),
                shadeClose: true,
                btn: ['添加', '取消'],
                success: function () {

                    // autoClear.getFitstPartsType();

                    // 获取弹层高度，赋值给添加区域显示盒子
                    var layerHeight = $(".layui-layer").height() - 200;
                    $('.add-result').css('height', layerHeight);


                    // 配件点击事件
                    $('.parts').off('click').on('click','.partsItem',function () {
                        var index=$(this).index();
                        var partsInfo=JSON.parse($(this).attr('data-obj'));

                        if(arr && arr.length){
                            var is_exit=false; //是否存在当前配件
                            arr.forEach(function (i,k) {
                                // 删除
                                if(i.partsCode==partsInfo.partsCode){
                                    is_exit=true; //存在当前配件
                                }
                            })

                            // 删除
                            if(is_exit){
                                var partsCode=partsInfo.partsCode;
                                autoClear.deletePartsArr(arr,partsCode);
                                autoClear.showParts(arr);
                            }
                            // 新增
                            else{
                                $('.parts .itemBox').find('.partsItem').eq(index).addClass('checked');
                                arr.push(partsInfo);
                                autoClear.showParts(arr);
                            }
                        }
                        else{
                            $('.parts .itemBox').find('.partsItem').eq(index).addClass('checked');
                            arr.push(partsInfo);
                            autoClear.showParts(arr);
                        }
                    });
                    // 删除配件
                    $('.add-result').on('click','img',function () {
                        var partsCode=$(this).parents('li').attr('data-partsCode');
                        autoClear.deletePartsArr(arr,partsCode);
                    });

                    // 搜索添加配件点击事件
                    layui.use('form', function () {
                        var form = layui.form;
                        form.on('select(searchParts)', function (data) {
                            var partsArr=data.value; //配件code
                            if(partsArr.length){
                                partsArr.forEach(function (i,k) {
                                    var partsInfoStr=$('#searchParts option:selected').eq(k).attr('data-partsInfo'),
                                        partsInfo=JSON.parse(decodeURI(partsInfoStr));
                                    var is_exit=false;
                                    if(arr && arr.length){
                                        arr.forEach(function (arrI,arrK) {
                                            if(arrI.partsCode==partsInfo.partsCode){
                                                is_exit=true;
                                            }
                                        });
                                        if(!is_exit){
                                            arr.push(partsInfo);
                                            autoClear.showParts(arr);
                                        }
                                    }
                                })
                            }
                        });
                    });
                },
                yes: function (index, layero) {
                    partsChooseArr=[].concat(arr);
                    layer.close(i);
                },
                // end:function () {
                //     // $('.first-parts-type .itemBox').html('');
                //     // $('.second-parts-type .itemBox').html('');
                //     // $('.parts .itemBox').html('');
                // }
            });
        },

        // 修改自动结算规则
        updateRule:function(data){
            win.utils.ajaxPost(win.utils.services.updateAutoAccountRule,data,function (result) {
                if(result.result_code==200){
                    utils.showMessage('修改成功！');
                    layer.closeAll();
                    autoClear.getServerData(nowPage,postData);
                    partsChooseArr=[];

                    autoClear.getOptionLog();
                    autoClear.getCurrentStatus();
                }else{
                    utils.showMessage(result.description);
                }
            })
        },

        // 查询单个自动结算规则
        getRule:function(effectiveStatus){
            win.utils.ajaxPost(win.utils.services.getAutoAccountRule,{
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                effectiveStatus:effectiveStatus,
                ruleCode:currentRuleCode,
            },function (result) {
                if(result.result_code==200){
                    autoClear.editInfoShow(result.data);
                }
            })
        },
        // 编辑信息回显
        editInfoShow:function(info){
            partsChooseArr=info.accountParts||[];
            $('#edit_minCost').val(info.minCost);
            $('#edit_maxCost').val(info.maxCost);
            $("input[name='edit_is_urgent'][value='"+info.isIncludeUrgentMoney+"']").prop('checked',true);
            $("input[name='edit_is_other'][value='"+info.isIncludeOtherMoney+"']").prop('checked',true);
            $("input[name='edit_is_parts'][value='"+info.isIncludeParts+"']").prop('checked',true);
            $('#edit_remark').val(info.remark);

            if(!info.isIncludeParts){
                $('.edit-parts-btn').attr('disabled',true);
            }else{
                $('.edit-parts-btn').removeAttr('disabled');
            }

            layui.use('form',function () {
                var form=layui.form;
                form.render('radio')
            });
        },

        // 删除配件数组中的数据
        deletePartsArr:function(arr,partsCode){
            arr.forEach(function (i,k) {
                if(i.partsCode==partsCode){
                    arr.splice(k,1);
                }
            });
            autoClear.showParts(arr);


            var partsNodes=$('.parts .partsItem');
            for(var i=0;i<partsNodes.length;i++){
                var partsInfo=JSON.parse($(partsNodes[i]).attr('data-obj'));
                if(partsInfo.partsCode==partsCode){
                    $(partsNodes[i]).removeClass('checked');
                }
            }
        },

        // 配件选择结果展示
        showParts:function(arr){
            var partsShowStr=' <li>\n' +
                '                    <div>配件名称</div>\n' +
                '                    <div>配件品牌</div>\n' +
                '                    <div>配件型号</div>\n' +
                '                    <div>配件费（元）</div>\n' +
                '                </li>';
            if(arr && arr.length){
                arr.forEach(function (i,k) {
                    partsShowStr+='<li data-partsCode="'+i.partsCode+'">\n' +
                        '                    <div>'+i.partsName+'</div>\n' +
                        '                    <div>品牌：<span>'+i.brandName+'</span></div>\n' +
                        '                    <div>型号：<span>'+i.partsType+'</span></div>\n' +
                        '                    <div>配件费：<span>'+i.price+'</span></div>\n' +
                        '                    <div><img src="../../../images/close.png" style="width: 15px;"></div>\n' +
                        '                </li>'
                });
            }
            $('.add-result ul').html(partsShowStr);


            // 搜索配件手动赋值
            var searchArr=[];
            if(arr && arr.length){
                arr.forEach(function (i,k) {
                    searchArr.push(i.partsCode);
                })
            }
            $('select[name="searchParts"]').val(searchArr);
            layui.use('form',function () {
                var form=layui.form;
                form.render();
            });


            // 配件选中展示
            var partsNodes=$('.parts .partsItem');
            if(arr && arr.length){
                arr.forEach(function (i,k) {
                    for (var nodeI=0;nodeI<partsNodes.length;nodeI++){
                        var partsInfo=JSON.parse($(partsNodes[nodeI]).attr('data-obj'));
                        if(i.partsCode==partsInfo.partsCode){
                            $(partsNodes[nodeI]).addClass('checked');
                        }
                    }
                })
            }
        },

        //初始化请求
        initList: function () {
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 'auto',
                autowidth: false,
                rowNum: 10,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["id", "操作","公式", "费用限制（元）", "规则详情", "备注", "更新时间","生效状态","规则ID"],
                colModel: [
                    {
                        name: "id",
                        index: "id",
                        editable: false,
                        width: 40,
                        sortable: false,
                        hidden: true
                    },
                    {
                        name: "options",
                        index: "options",
                        editable: false,
                        width: 240,
                        sortable: false,
                        formatter: tableOptions
                    },
                    {
                        name: "formulaName",
                        index: "formulaName",
                        editable: false,
                        width: 240,
                        sortable: false,
                    },
                    {
                        name: "free_limit",
                        index: "free_limit",
                        editable: false,
                        width: 190,
                        sortable: false,
                        formatter:function (cellvalue, options, rowObject) {
                            return rowObject.minCost + '-' +rowObject.maxCost
                        }
                    },
                    {
                        name: "accountParts",
                        index: "accountParts",
                        editable: false,
                        width: 520,
                        sortable: false,
                        formatter:function (cellvalue, options, rowObject) {
                            var partsArr=[];
                            if(rowObject.accountParts && rowObject.accountParts.length){
                                rowObject.accountParts.forEach(function (i,k) {
                                    partsArr.push(i.partsName);
                                });
                                return partsArr.join(',')
                            }
                            else {
                                return '无'
                            }

                        }
                    },
                    {
                        name: "remark",
                        index: "remark",
                        editable: false,
                        width: 420,
                        sortable: false
                    },
                    {
                        name: "modifyTime",
                        index: "modifyTime",
                        editable: false,
                        width: 180,
                        sortable: false,
                        formatter:function (cellvalue, options, rowObject) {
                            return autoClear.timeStampFormat(rowObject.modifyTime)
                        }
                    },
                    {
                        name: "effectiveStatus",
                        index: "effectiveStatus",
                        editable: false,
                        width: 140,
                        sortable: false,
                        hidden: true
                    },
                    {
                        name: "ruleCode",
                        index: "ruleCode",
                        editable: false,
                        width: 140,
                        sortable: false,
                        hidden: true
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                shrinkToFit: false,
                autoScroll: true,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        postData.position = nowPage;
                        autoClear.getServerData(nowPage,postData);
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    postData.position = nowPage;
                    autoClear.getServerData(nowPage,postData);
                },
                //当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    selectedRowIndex = $("#" + this.id).getGridParam('selrow');
                    autoClear.rowOption(selectedRowIndex);
                },
            });
            autoClear.getServerData(1, postData);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });


            function tableOptions(cellvalue, options, rowObject) {
                if (rowObject.status) {
                    if (rowObject.status == 1) {
                        return '<button type="button" ' +
                            'class="btn btn-info opertion unified-btn-inline unified-change-btn-inline" ' +
                            'id="btnUpdate" ' +
                            'data-ruleCode="' + rowObject.ruleCode + '" ' +
                            'data-status="' + rowObject.status + '" ' +
                            'data-id="' + rowObject.id + '" ' +
                            'style="margin-right: 20px">修改</button>' +

                            '<button type="button" ' +
                            'class="btn btn-info opertion unified-btn-inline unified-change-btn-inline changeReceiptStatus" ' +
                            'data-ruleCode="' + rowObject.ruleCode + '" ' +
                            'data-status="' + rowObject.status + '">禁用</button>'
                    } else if (rowObject.status == 2) {
                        return '<button type="button" ' +
                            'class="btn btn-info opertion unified-btn-inline unified-publish-btn-inline  changeReceiptStatus" ' +
                            'data-ruleCode="' + rowObject.ruleCode + '" ' +
                            'data-status="' + rowObject.status + '">启用</button>'
                    }
                } else {
                    return ''
                }

            };


            // 操作记录表
            $("#optionTable").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 600,
                maxHeight: 600,
                autowidth: false,
                shrinkToFit: false,
                autoScroll: true,
                rownumbers: true,
                rownumWidth: 60,
                rowNum: -1,
                colNames: ["操作", "修改记录", "修改时间", "修改人"],
                colModel: [
                    {
                        name: "operationAction",
                        index: "operationAction",
                        editable: false,
                        width: 180,
                        sortable: false,
                    },
                    {
                        name: "operationDesc",
                        index: "operationDesc",
                        editable: false,
                        width: 380,
                        sortable: false,
                    },
                    {
                        name: "createTime",
                        index: "createTime",
                        editable: false,
                        width: 180,
                        sortable: false,
                    },
                    {
                        name: "userName",
                        index: "userName",
                        editable: false,
                        width: 200,
                        sortable: false,
                    },
                ],
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
            });

            // 当前状态表
            $("#statusTable").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 600,
                maxHeight: 600,
                autowidth: false,
                shrinkToFit: false,
                autoScroll: true,
                rownumbers: true,
                rownumWidth: 60,
                rowNum: -1,
                colNames: ["公式","费用限制（元）", "规则详情", "备注", "当前状态"],
                colModel: [
                    {
                        name: "formulaName",
                        index: "formulaName",
                        editable: false,
                        width: 200,
                        sortable: false,
                    },
                    {
                        name: "operationType",
                        index: "operationType",
                        editable: false,
                        width: 140,
                        sortable: false,
                        formatter:function (cellvalue, options, rowObject) {
                            if(rowObject.minCost>=0 && rowObject.maxCost>=0){
                                return rowObject.minCost+'—'+rowObject.maxCost
                            }
                            else {
                                return ''
                            }
                        }
                    },
                    {
                        name: "operationDesc",
                        index: "operationDesc",
                        editable: false,
                        width: 400,
                        sortable: false,
                        formatter:function (cellvalue, options, rowObject) {
                            var partsArr=[];
                            if(rowObject.accountParts && rowObject.accountParts.length){
                                rowObject.accountParts.forEach(function (i,k) {
                                    partsArr.push(i.partsName);
                                });
                                return partsArr.join(',')
                            }
                            else {
                                return  ''
                            }
                        }
                    },
                    {
                        name: "remark",
                        index: "remark",
                        editable: false,
                        width: 200,
                        sortable: false,
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 120,
                        sortable: false,
                        formatter:function (cellvalue, options, rowObject) {
                            if(rowObject.status==1){
                                return '启用'
                            }
                            else if(rowObject.status==2){
                                return '禁用'
                            }
                            else {
                                return ''
                            }
                        }
                    },
                ],
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
            });


            var b = $(".jqGrid_wrapper").width();
            $("#table").setGridWidth(b);
            $("#optionTable").setGridWidth(b);
            $("#statusTable").setGridWidth(b);
            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#actRecordTable").setGridWidth(b);
                $("#optionRecordTable").setGridWidth(b);
                // $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },

        getServerData: function (pageIndex, postData, fn) {
            is_search = false;
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.getAutoAccountRuleList, postData, fn ? fn : function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = [];
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                        utils.showMessage('暂无数据！');
                    } else {
                        gridData = result.lists;
                        selectId = gridData[0].id;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },

        // 保存内容初始化
        saveConInit:function(){
            $('#free_start').val('');
            $('#free_end').val('');
            $("input[name='is_urgent'][value='1']").prop('checked',false);
            $("input[name='is_urgent'][value='2']").prop('checked',false);
            $("input[name='is_other'][value='1']").prop('checked',false);
            $("input[name='is_other'][value='2']").prop('checked',false);
            $("input[name='is_parts'][value='1']").prop('checked',false);
            $("input[name='is_parts'][value='2']").prop('checked',false);
            partsChooseArr=[];
        },

        // 启用禁用操作
        updateRuleStatus:function(status){
            win.utils.ajaxPost(win.utils.services.updateRuleStatus,{
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                ruleCode:currentRuleCode,
                status:status==1?2:1
            },function (result) {
                if(result.result_code==200){
                    utils.showMessage('修改成功！');
                    autoClear.getServerData(nowPage,postData);

                    autoClear.getOptionLog();
                    autoClear.getCurrentStatus();
                }
                else{
                    utils.showMessage('修改失败！');
                }
            })
        },

        // 获取操作记录
        getOptionLog:function(){
            if(selectedRowData){
                win.utils.ajaxPost(win.utils.services.getAutoAccountRuleLog,{
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    ruleCode: selectedRowData.ruleCode,
                    // ruleCode: 20191023225251385,
                },function (result) {
                    if(result.result_code==200){
                        if(!result.data.length){
                            // utils.showMessage('暂无操作记录！');
                            gridData_option = [];
                            var gridJson_option = {
                                rows: gridData_option,
                            };
                            $("#optionTable")[0].addJSONData(gridJson_option);
                        }
                        else{
                            gridData_option = result.data;
                            var gridJson_option = {
                                rows: gridData_option,
                            };
                            $("#optionTable")[0].addJSONData(gridJson_option);
                        }
                    }
                })
            }
        },

        // 获取当前状态
        getCurrentStatus:function(){
            if(selectedRowData){
                win.utils.ajaxPost(win.utils.services.getAutoAccountRule,{
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    ruleCode: selectedRowData.ruleCode,
                    effectiveStatus: 1
                },function (result) {
                    if(result.result_code==200){
                        if([result.data].length){
                            gridData_status = [result.data];
                            var gridJson_status = {
                                rows: gridData_status,
                            };
                            $("#statusTable")[0].addJSONData(gridJson_status);
                        }
                        else{
                            gridData_status = [];
                            var gridJson_status = {
                                rows: gridData_status,
                            };
                            $("#statusTable")[0].addJSONData(gridJson_status);
                        }

                    }
                })
            }
        },



        // 正则验证——校验手机号正确性
        checkMobile: function (s) {
            var regu = /^[1][3-9][0-9]{9}$/;
            var re = new RegExp(regu);
            if (re.test(s)) {
                return true;
            } else {
                return false;
            }
        },
        checkName: function (name) {
            var regu = "^[\u4e00-\u9fa5]+$";
            var re = new RegExp(regu);
            if (re.test(name)) {
                return true;
            } else {
                return false;
            }
        },
        // 正则校验一位小数
        checkDecimals:function(num){
            var res = /^(\d+\.\d{1,1}|\d+)$/ ;
            if(!res.test(num)){
                return false;
            }else
                return true;
        },


        // 选中列表某一行,渲染区域详情部分
        rowOption: function (index) {
            $('.detail').show();
            var id = $('#table').jqGrid('getGridParam', 'selrow');
            selectedRowData=$('#table').jqGrid('getRowData',id);

            $('#AllBtnAction button').removeClass('active').eq(0).addClass('active');
            $('#contanerDetail>div').hide().eq(0).show();
            autoClear.getOptionLog();
        },
        // 获取操作记录
        getLeaveRecord: function (workerId) {
            win.utils.ajaxPost(win.utils.services.getWorkerLeaveList, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                workerId: workerId,
            }, function (result) {
                if (result.result_code == 200) {
                    var leaveGrideData = result.workerLeaveRecords;
                    var gridJson = {
                        rows: leaveGrideData,
                    };
                    $("#actRecordTable")[0].addJSONData(gridJson);

                    $('.leaveTbaleBox').show();
                }
            });
        },
        // 获取当前状态
        getOptionRecord: function (workerId) {
            win.utils.ajaxPost(win.utils.services.queryWorkerLog, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                workerId: workerId,
            }, function (result) {
                if (result.result_code == 200) {
                    var optionGrideData = result.workerLogs;
                    var optionGridJson = {
                        rows: optionGrideData
                    };
                    $("#optionRecordTable")[0].addJSONData(optionGridJson);
                }
            });
        },

        // 时间戳转时间格式
        timeStampFormat: function (timestamp) {
            var date = new Date(timestamp);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
            h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y + M + D + h + m + s;
        },
    };

    win.autoClear = autoClear;
})(window, jQuery, qiniu);
