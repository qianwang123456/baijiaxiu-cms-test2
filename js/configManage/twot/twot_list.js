/**
 * Created by song.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    //table选中行#id
    var selectedId = 0;
    //搜索-search
    var thirdName = '';//三级工单名称
    var thirdSta = '';//三级工单状态
    var thirdExigency = '';//三级紧急
    var secondClassify = '';//二级工单分类
    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
    };
    var twot_list = {
        updateList: function () {
            let url = win.utils.services.configMan_firstClassWorkOrder_list;
            let datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                // serviceId: selectedId,
                rank: 3
            };
            win.utils.ajaxPost(url, datas, function (result) {
                if (result.result_code == 200) {
                    var myObj = result.serviceItems[0];
                    $("#firstClassifyId").val(myObj.firstClassifyId);
                    $("#secondClassifyId").val(myObj.secondClassifyId);
                    $("#names").val(myObj.name);
                    $("#staAdd").val(myObj.status);
                    $("#isExigency").val(myObj.isExigency);
                    $("#isManualClaim").val(myObj.isManualClaim);
                    $("#issueDiagnose").val(myObj.issueDiagnose);
                    $("#customerServiceMotion").val(myObj.customerServiceMotion);
                    $("#suggest").val(myObj.suggest);
                } else {
                    utils.showMessage('获取数据失败，请稍后重试！');
                }
            });
        },

        // 获取一级类别名称
        getFirstClassName: function () {
            win.utils.ajaxPost(win.utils.services.configMan_firstClassWorkOrder_list, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 1,
            }, function (result) {
                if (result.result_code == 200) {
                    var obj = result.serviceItems;
                    var str = "";
                    $.each(obj, function (index, value) {
                        str += "<option value='" + value.serviceId + "'>" + value.name + "</option>"
                    });
                    $("#firstClassifyId").append(str);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 获取二级类别名称
        getSecondClassName: function () {
            win.utils.ajaxPost(win.utils.services.configMan_firstClassWorkOrder_list, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 2,
                status: 1
            }, function (result) {
                if (result.result_code == 200) {
                    var obj = result.serviceItems;
                    var str = "";
                    $.each(obj, function (index, value) {
                        str += "<option value='" + value.serviceId + "'>" + value.name + "</option>"
                    });
                    $("#secondClassifyId").append(str);
                    $("#skilClasifyThird").append(str);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('config_third', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                    win.utils.clearSpaces();
                    twot_list.getFirstClassName();
                    twot_list.getSecondClassName()
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });
            // 搜索
            $('#btnSearch').click(obj.searchBox);
            //刷新
            $('#btnQuery').on('click', function () {
                thirdName = '';//三级工单名称
                thirdSta = '';//三级工单状态
                thirdExigency = '';//三级紧急
                secondClassify = '';//二级工单分类
                $('#thirName').val('');
                $('#seaSta').val('');
                $("#Exigency").val('');
                $('#skilClasifyThird').val('');
                obj.refreshServerData();
            });
            //新增
            $('#btnAdd').on('click', function () {
                twot_list.addWorkOrder(1)
            });
            //修改
            $('#btnUpdate').on('click', function () {
                twot_list.addWorkOrder(2)
            });
            //删除
            $("#btnDel").on('click', function () {
                if (selectedId == null || selectedId == 0) {
                    utils.showMessage('请选择要进行删除的记录！');
                    return;
                }
                ;
                return;
                win.utils.ajaxPost(win.utils.services.configMan_firstClassWorkOrder_list, {}, function (result) {
                    if (result.result_code == 200) {
                    } else {
                        utils.showMessage('获取数据失败，请重试！');
                    }
                });
            })
        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: 20,
                rownumbers: true,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["ID", "一级分类名称", "二级分类名称", "名称", "前端显示名称", "状态", "前端显示状态", "是否紧急", "是否手动认领", "创建时间", "问题诊断", "客服动作", "建议话术"],
                colModel: [{
                    name: "serviceId",
                    index: "serviceId",
                    editable: false,
                    width: 90,
                    sorttype: "string",
                    hidden: true
                },
                    {
                        name: "firstClassifyName",
                        index: "firstClassifyName",
                        editable: false,
                        width: 130,
                        sorttype: "string"
                    },
                    {
                        name: "secondClassifyName",
                        index: "secondClassifyName",
                        editable: false,
                        width: 130,
                        sorttype: "string"
                    },
                    {
                        name: "name",
                        index: "name",
                        editable: false,
                        width: 120,
                        sorttype: "string"
                    },
                    {
                        name: "name",
                        index: "name",
                        editable: false,
                        width: 120,
                        sorttype: "string"
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 48,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '启用';
                            else return '禁用';
                        }
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 100,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '启用';
                            else return '禁用';
                        }
                    },
                    {
                        name: "isExigency",
                        index: "isExigency",
                        editable: false,
                        width: 72,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '紧急';
                            else return '不紧急';
                        }
                    },
                    {
                        name: "isManualClaim",
                        index: "isManualClaim",
                        editable: false,
                        width: 120,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '认领';
                            else return '不认领';
                        }
                    },
                    {
                        name: "createTime",
                        index: "createTime",
                        editable: false,
                        width: 150,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            let obj = new Date(cellvalue);
                            return obj.toLocaleDateString() + obj.toLocaleTimeString();
                        }
                    },
                    {
                        name: "issueDiagnose",
                        index: "issueDiagnose",
                        editable: false,
                        width: 185,
                        sorttype: "string"
                    },
                    {
                        name: "customerServiceMotion",
                        index: "customerServiceMotion",
                        editable: false,
                        width: 185,
                        sorttype: "string"
                    },
                    {
                        name: "suggest",
                        index: "suggest",
                        editable: false,
                        width: 280,
                        sorttype: "string"
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    if (deferLoadData == 0) {
                        twot_list.getServerData(currentPage);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        twot_list.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    twot_list.getServerData(currentPage);
                },
                //当选择行时触发此事件。
                onSelectRow: function () {
                    //返回选中的id
                    var data = $("#table").jqGrid('getGridParam', 'selrow');
                    // var selectedIds = $("#table").jqGrid('getRowData',data,'serviceId');
                    selectedId = $('#table').jqGrid('getCell', data, 'serviceId');
                },
                gridComplete: function () {

                }
            });

            twot_list.getServerData(1);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        refreshServerData: function () {
            twot_list.getServerData(1);
        },
        getServerData: function (pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            var datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                // serviceId:parseInt($('#serviceId').val()),
                name: thirdName,
                status: thirdSta,//parseInt($('#seaSta').val()),  
                isExigency: thirdExigency,//parseInt($("#Exigency").val()),
                secondClassifyId: secondClassify,//parseInt($('#skilClasifyThird').val()),
                rank: 3,
                position: pageIndex,
                rows: pageCount
            };
            win.utils.ajaxPost(win.utils.services.configMan_firstClassWorkOrder_list, datas, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.serviceItems;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        //搜索
        searchBox: function () {
            initializeEditForm();
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '查询',
                content: $('#SearchCon'),
                btn: ['查询', '取消'],
                yes: function (index, layero) {
                    layer.close(i);
                    thirdName = $("#thirName").val();
                    thirdSta = parseInt($('#seaSta').val());
                    thirdExigency = parseInt($("#Exigency").val());
                    secondClassify = parseInt($('#skilClasifyThird').val());
                    twot_list.refreshServerData();
                }
            });
        },
        //新增-修改act:1新增  2:修改
        addWorkOrder: function (act) {
            initializeEditForm();
            if (act == 2) {
                if (selectedId == null || selectedId == 0) {
                    utils.showMessage('请选择要进行修改的记录！');
                    return;
                }
                ;
                twot_list.updateList();
            }
            ;
            let titleVal = act == 1 ? '新增' : '修改';
            let arr = act == 1 ? ['新增', '取消'] : ['修改', '取消'];
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: titleVal,
                content: $('#AddCon'),
                btn: arr,
                yes: function (index, layero) {
                    var idFirst = $("#firstClassifyId").val();
                    var nameFirst = $("#firstClassifyId :selected").text();
                    if ($.trim(idFirst).length <= 0) {
                        utils.showLayerMessage('请选择一级分类！');
                        return;
                    }
                    ;
                    var idSecond = $("#secondClassifyId").val();
                    var nameSecond = $("#secondClassifyId :selected").text();
                    if ($.trim(idSecond).length <= 0) {
                        utils.showLayerMessage('请选择二级分类！');
                        return;
                    }
                    ;
                    var ss = $("#names").val();
                    if ($.trim(ss).length <= 0) {
                        utils.showLayerMessage('请填写名称！');
                        return;
                    }
                    ;
                    var statuss = $("#staAdd").val();
                    if (statuss.length <= 0) {
                        utils.showLayerMessage('请选择状态！');
                        return;
                    }
                    ;
                    if ($("#isExigency").val().length <= 0) {
                        utils.showLayerMessage('请选择是否紧急！');
                        return;
                    }
                    ;
                    var isManualClaim = $("#isManualClaim").val();
                    if ($.trim(isManualClaim).length <= 0) {
                        utils.showLayerMessage('请选择是否手动认领！');
                        return;
                    }
                    ;
                    var issueDiagnose = $("#issueDiagnose").val();
                    if ($.trim(issueDiagnose).length <= 0) {
                        utils.showLayerMessage('请填写问题诊断！');
                        return;
                    }
                    ;
                    var customerServiceMotion = $("#customerServiceMotion").val();
                    if ($.trim(customerServiceMotion).length <= 0) {
                        utils.showLayerMessage('请填写客服动作！');
                        return;
                    }
                    ;
                    let url = act == 1 ? win.utils.services.configMan_WorkOrder_add : win.utils.services.configMan_WorkOrder_update;
                    let datas = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        rank: 3,
                        firstClassifyId: idFirst,
                        firstClassifyName: nameFirst,
                        secondClassifyId: idSecond,
                        secondClassifyName: nameSecond,
                        name: ss,
                        status: statuss,
                        isExigency: $("#isExigency").val(),
                        isManualClaim: isManualClaim,
                        issueDiagnose: issueDiagnose,
                        customerServiceMotion: customerServiceMotion,
                        suggest: $("#suggest").val()
                    };
                    if (act == 2) {
                        datas.serviceId = selectedId;
                    }
                    win.utils.ajaxPost(url, datas, function (result) {
                        if (result.result_code == 200) {
                            layer.close(i);
                            twot_list.refreshServerData();
                        } else {
                            let msg = act == 1 ? '新增失败，请稍后重试！' : '修改失败，请稍后重试！';
                            utils.showLayerMessage(msg);
                        }
                    });
                }
            });
        }
    };
    var flagInput = false;
    //禁止弹窗与浏览器滚动
    $("body").on('focus', 'input,textarea', function () {
        flagInput = true;
    });
    $("body").on('blur', 'input,textarea', function () {
        flagInput = false;
    });
    $(document).keydown(function (event) {
        var e = window.event || event;
        var keyCode = event.keyCode;
        if (keyCode == 13 || keyCode == 32) {
            if (flagInput == false) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    window.event.returnValue = false;
                }
                return false;
            }
            ;
        }
    });
    win.twot_list = twot_list;
})(window, jQuery);