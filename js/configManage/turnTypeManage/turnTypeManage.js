/**
 * Created by hanlu.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
    };
    var turnTypeManage = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('turnTypeManage', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                    win.utils.clearSpaces();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });
        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: 20,
                rownumbers: true,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["ID", "子工单类型一级", "子工单类型二级", "转办说明", "状态", "创建子工单类型", "名称"],
                colModel: [
                    {
                        name: "id",
                        index: "id",
                        editable: false,
                        width: 90,
                        sorttype: "string",
                        hidden: true
                    },
                    {
                        name: "subTaskCategoryOneName",
                        index: "subTaskCategoryOneName",
                        editable: false,
                        width: 180,
                        sorttype: "string"
                    },
                    {
                        name: "subTaskCategoryTwoName",
                        index: "subTaskCategoryTwoName",
                        editable: false,
                        width: 300,
                        sorttype: "string",
                    },
                    {
                        name: "introduce",
                        index: "introduce",
                        editable: false,
                        width: 400,
                        sorttype: "string",
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 100,
                        sorttype: "string"
                    },
                    {
                        name: "subTaskName",
                        index: "subTaskName",
                        editable: false,
                        width: 160,
                        sorttype: "string"
                    },
                    {
                        name: "name",
                        index: "name",
                        editable: false,
                        width: 120,
                        sorttype: "string"
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    if (deferLoadData == 0) {
                        turnTypeManage.getServerData(currentPage);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        turnTypeManage.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    turnTypeManage.getServerData(currentPage);
                },
                //当选择行时触发此事件。
                onSelectRow: function () {
                    //返回选中的id
                    // var data = $("#table").jqGrid('getGridParam', 'selrow');
                    // var selectedIds = $("#table").jqGrid('getRowData',data,'serviceId');
                    // selectedId = $('#table').jqGrid('getCell', data,'id');
                    // let a= $('#table').jqGrid('getCell', data,'isCheck');
                    // let b= $('#table').jqGrid('getCell', data,'name');
                    // let c= $('#table').jqGrid('getCell', data,'planExplain');
                    // let d= $('#table').jqGrid('getCell', data,'status');
                    // dataSelect.id=selectedId;
                    // dataSelect.isCheck=a=='否'?2:1;//是否审核 1-审核，2-不审核 ,
                    // dataSelect.name=b;//name
                    // dataSelect.planExplain=c;//说明
                    // dataSelect.status=d=='启用'?1:2;
                },
                gridComplete: function () {

                }
            });

            turnTypeManage.getServerData(1);
            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        getServerData: function (pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            var datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                position: pageIndex,
                rows: pageCount
            };
            win.utils.ajaxPost(win.utils.services.getDistributeType, datas, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                        utils.showMessage('暂无数据！');
                    } else {
                        gridData = result.types;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
    };
    win.turnTypeManage = turnTypeManage;
})(window, jQuery);
