/**
 * Created by song.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    //table选中行#id
    var selectedId = 0;
    var firstTypeSearch = '';//一级工单类型---搜索
    var secondNameSearch = '';//二级工单名称---搜索
    var secondSkillSearch = '';//二级工单技能分类---搜索
    var secondStaSearch = '';//二级工单状态---搜索
    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
    };
    var swot_list = {
        //修改-getinfo
        updateList: function () {
            let url = win.utils.services.configMan_firstClassWorkOrder_list;
            let datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                serviceId: selectedId,
                rank: 2
            };
            win.utils.ajaxPost(url, datas, function (result) {
                if (result.result_code == 200) {
                    var myObj = result.serviceItems[0];
                    $("#firstClassifyAdd").val(myObj.firstClassifyId);
                    $("#secondClassifyName").val(myObj.name);
                    let arr = myObj.applyArea.split(',');
                    $("#area input:checkbox").each(function () {
                        let str = $(this).val();
                        if (arr.indexOf(str) >= 0) {
                            $(this).attr("checked", 'true');
                        }
                    });
                    $("#staAdd").val(myObj.status);
                    $("#isCheck").val(myObj.isCheck);
                    $("#checkManAdd").val(myObj.checkMan);
                    $("#ManualClaimsAdd").val(myObj.isManualClaim);
                    $("#isHouseCorrelationAdd").val(myObj.isHouseCorrelation);
                    $("#isSendAdd").val(myObj.isSend);
                    $("#skilClassifyAdd").val(myObj.skillClassify);
                } else {
                    utils.showMessage('获取数据失败，请稍后重试！');
                }
            });
        },
        // 获取一级类别名称
        getFirstClassName: function () {
            win.utils.ajaxPost(win.utils.services.configMan_firstClassWorkOrder_list, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 1,
                status: 1
            }, function (result) {
                if (result.result_code == 200) {
                    var obj = result.serviceItems;
                    var str = "";
                    $.each(obj, function (index, value) {
                        str += "<option value='" + value.serviceId + "'>" + value.name + "</option>"
                    });
                    $("#firstClassify").append(str);
                    $("#firstClassifyAdd").append(str);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('config_second', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                    win.utils.clearSpaces();
                    swot_list.getFirstClassName()
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });
            // 搜索
            $('#btnSearch').click(obj.searchBox);
            //刷新
            $('#btnQuery').on('click', function () {
                firstTypeSearch = '';//一级工单类型---搜索
                secondNameSearch = '';//二级工单名称---搜索
                secondSkillSearch = '';//二级工单技能分类---搜索
                secondStaSearch = '';//二级工单状态---搜索
                $('#firstClassify').val('');
                $('#secName').val('');
                $('#seaSta').val('');
                $('#skilClassify').val('');
                obj.refreshServerData();
            });
            //新增
            $('#btnAdd').on("click", function () {
                swot_list.addWorkOrder(1)
            });
            //修改
            $('#btnUpdate').on("click", function () {
                swot_list.addWorkOrder(2)
            });
            //删除
            $('#btnDel').click(obj.delWorkOrder);
        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: 20,
                rownumbers: true,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["ID", "父级分类", "名称", "适配区域", "技能分类", "状态", "是否对租户显示", "是否验收", "验收人", "是否手动认领", "是否与房源关联", "是否发送到门锁供应商", "归属原因"],
                colModel: [{
                    name: "serviceId",
                    index: "serviceId",
                    editable: false,
                    width: 90,
                    sorttype: "string",
                    hidden: true
                },
                    {
                        name: "firstClassifyName",
                        index: "firstClassifyName",
                        editable: false,
                        width: 120,
                        sorttype: "string"
                    },
                    {
                        name: "name",
                        index: "name",
                        editable: false,
                        width: 150,
                        sorttype: "string"
                    },
                    {
                        name: "applyArea",
                        index: "applyArea",
                        editable: false,
                        width: 300,
                        sorttype: "string"
                    },
                    {
                        name: "skillClassify",
                        index: "skillClassify",
                        editable: false,
                        width: 120,
                        sorttype: "string"
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 64,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '启用';
                            else return '禁用';
                        }
                    },
                    {
                        name: "isShow",
                        index: "isShow",
                        editable: false,
                        width: 130,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '显示';
                            else if (cellvalue == 2) return '不显示';
                            else return '——';
                        }
                    },
                    {
                        name: "isCheck",
                        index: "isCheck",
                        editable: false,
                        width: 80,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '验收';
                            else return '不验收';
                        }
                    },
                    {
                        name: "checkMan",
                        index: "checkMan",
                        editable: false,
                        width: 120,
                        sorttype: "string",
                    },
                    {
                        name: "isManualClaim",
                        index: "isManualClaim",
                        editable: false,
                        width: 150,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '认领';
                            else return '不认领';
                        }
                    },
                    {
                        name: "isHouseCorrelation",
                        index: "isHouseCorrelation",
                        editable: false,
                        width: 150,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '相关';
                            else return '不相关';
                        }
                    },
                    {
                        name: "isSend",
                        index: "isSend",
                        editable: false,
                        width: 150,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '发送';
                            else return '不发送';
                        }
                    },
                    {
                        name: "reasonAttribution",
                        index: "reasonAttribution",
                        editable: false,
                        width: 250,
                        sorttype: "string",
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    if (deferLoadData == 0) {
                        swot_list.getServerData(currentPage);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        swot_list.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    swot_list.getServerData(currentPage);
                },
                //当选择行时触发此事件。
                onSelectRow: function () {
                    //返回选中的id
                    var data = $("#table").jqGrid('getGridParam', 'selrow');
                    // var selectedIds = $("#table").jqGrid('getRowData',data,'serviceId');
                    selectedId = $('#table').jqGrid('getCell', data, 'serviceId');
                },
                gridComplete: function () {

                }
            });

            swot_list.getServerData(1);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        refreshServerData: function () {
            swot_list.getServerData(1);
        },
        getServerData: function (pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.configMan_firstClassWorkOrder_list, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                firstClassifyId: firstTypeSearch == '*一级工单类型*' ? '' : firstTypeSearch,
                name: secondNameSearch,
                status: secondStaSearch,
                skillClassify: secondSkillSearch,
                rank: 2,
                position: pageIndex,
                rows: pageCount
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.serviceItems;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        //搜索
        searchBox: function () {
            initializeEditForm();
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '查询',
                content: $('#SearchCon'),
                btn: ['查询', '取消'],
                yes: function (index, layero) {
                    layer.close(i);
                    firstTypeSearch = $('#firstClassify').val();
                    secondNameSearch = $('#secName').val();
                    secondStaSearch = $('#seaSta').val();
                    secondSkillSearch = $('#skilClassify').val();
                    swot_list.refreshServerData();
                }
            });
        },
        //新增-修改
        addWorkOrder: function (act) {
            // act: 1-新增  2-修改
            if (act == 2) {
                if (selectedId == null || selectedId == 0) {
                    utils.showMessage('请选择要进行修改的记录！');
                    return;
                }
                ;
                swot_list.updateList();
            }
            ;
            let titleVal = act == 1 ? '新增' : '修改';
            let btnArr = act == 1 ? ['新增', '取消'] : ['修改', '取消'];
            initializeEditForm();
            var nn = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: titleVal,
                content: $('#AddCon'),
                btn: btnArr,
                yes: function (index, layero) {
                    var idFirst = $("#firstClassifyAdd").val();
                    var nameFirst = $("#firstClassifyAdd :selected").text();
                    if ($.trim(idFirst).length <= 0) {
                        utils.showLayerMessage('请选择一级类别名称！');
                        return;
                    }
                    ;
                    if ($("#secondClassifyName").val().length <= 0) {
                        utils.showLayerMessage('请填写二级类别名称！');
                        return;
                    }
                    ;
                    var ss = $("#area input:checked");
                    var areas = "";
                    $.each(ss, function (index, vals) {
                        areas += vals.defaultValue + ",";
                    });

                    if ($.trim(areas).length <= 0) {
                        utils.showLayerMessage('请选择适用区域！');
                        return;
                    }
                    ;
                    var statuss = $("#staAdd").val();
                    if (statuss.length <= 0) {
                        utils.showLayerMessage('请选择状态！');
                        return;
                    }
                    ;
                    if ($("#isCheck").val() <= 0) {
                        utils.showLayerMessage('请选择是否验收！');
                        return;
                    }
                    ;
                    if ($("#checkManAdd").val() <= 0 && $("#isCheck").val() == '1') {
                        utils.showLayerMessage('请填写验收人！');
                        return;
                    }
                    ;
                    var ManualClaimsAdd = $("#ManualClaimsAdd").val();
                    if ($.trim(ManualClaimsAdd).length <= 0) {
                        utils.showLayerMessage('请选择是否手动认领！');
                        return;
                    }
                    ;
                    var isHouseCorrelationAdd = $("#isHouseCorrelationAdd").val();
                    if ($.trim(isHouseCorrelationAdd).length <= 0) {
                        utils.showLayerMessage('请选择是否与房源相关！');
                        return;
                    }
                    ;
// 					var isSendAdd=$("#isSendAdd").val();
// 					if($.trim(isSendAdd).length<=0){
// 						utils.showLayerMessage('请选择是否发送到门锁供应商！');
// 						return;
// 					};		
// 					var skilClassifyAdd=$("#skilClassifyAdd").val();
// 					if($.trim(skilClassifyAdd).length<=0){
// 						utils.showLayerMessage('请选择技能分类！');
// 						return;
// 					};
                    let url = act == 1 ? win.utils.services.configMan_WorkOrder_add : win.utils.services.configMan_WorkOrder_update;
                    let datas = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        firstClassifyId: idFirst,
                        firstClassifyName: nameFirst,
                        name: $("#secondClassifyName").val(),
                        applyArea: areas,
                        status: statuss,
                        isCheck: $("#isCheck").val(),
                        checkMan: $("#checkManAdd").val(),
                        isManualClaim: ManualClaimsAdd,
                        isHouseCorrelation: isHouseCorrelationAdd,
                        rank: 2,
                        isSend: $("#isSendAdd").val(),
                        skillClassify: $("#skilClassifyAdd").val()
                    };
                    if (act == 2) {
                        datas.serviceId = selectedId;
                    }
                    ;
                    win.utils.ajaxPost(url, datas, function (result) {
                        if (result.result_code == 200) {
                            layer.close(nn);
                            swot_list.refreshServerData();
                        } else {
                            let msg = act == 1 ? '新增失败，请稍后重试！' : '修改失败，请稍后重试！';
                            utils.showLayerMessage(msg);
                        }
                    });
                }
            });
        },
        //删除
        delWorkOrder: function () {
            initializeEditForm();
            if (selectedId == null || selectedId == 0) {
                utils.showMessage('请选择要进行删除的记录！');
                return;
            }
            ;
        },
    };
    var flagInput = false;
    //禁止弹窗与浏览器滚动
    $("body").on('focus', 'input,textarea', function () {
        flagInput = true;
    });
    $("body").on('blur', 'input,textarea', function () {
        flagInput = false;
    });
    $(document).keydown(function (event) {
        var e = window.event || event;
        var keyCode = event.keyCode;
        if (keyCode == 13 || keyCode == 32) {
            if (flagInput == false) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    window.event.returnValue = false;
                }
                return false;
            }
            ;
        }
    });
    win.swot_list = swot_list;
})(window, jQuery);