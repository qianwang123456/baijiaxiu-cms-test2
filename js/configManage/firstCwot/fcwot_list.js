/**
 * Created by song.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //table选中-id
    var selectedId = 0;
    var firstNameSearch = '';//一级工单名称---搜索
    var firstStaSearch = '';//一级工单状态---搜索
    //初始化---
    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
    };

    //搜索初始化---
    function initSearch() {
        firstNameSearch = '';//一级工单名称
        firstStaSearch = '';//一级工单状态
        $("#seaTxt").val("");
        $("#seaSta").val("");
    };
    var fcwot_list = {
        //修改-getinfo
        updateList: function () {
            let url = win.utils.services.configMan_firstClassWorkOrder_list;
            let datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                serviceId: selectedId
            };
            win.utils.ajaxPost(url, datas, function (result) {
                if (result.result_code == 200) {
                    var myObj = result.serviceItems[0];
                    $("#addName").val(myObj.name);
                    $("#addSta").val(myObj.status);
                    $("#isHaveGoods").val(myObj.isHaveGoods);
                    $("#ManualClaims").val(myObj.isManualClaim);
                    $("#isShow").val(myObj.isShow);
                } else {
                    utils.showMessage('获取数据失败，请稍后重试！');
                }
            });
        },
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('config_first', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                    win.utils.clearSpaces();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });
            // 搜索
            $('#btnSearch').click(obj.searchBox);
            //刷新
            $('#btnQuery').on('click', function () {
                initSearch();
                obj.refreshServerData(1);
            });
            //新增
            $('#btnAdd').on('click', function () {
                fcwot_list.addWorkOrder(1)
            });
            //修改
            $('#btnUpdate').on('click', function () {
                fcwot_list.addWorkOrder(2)
            });
            //删除
            $('#btnDel').click(obj.delWorkOrder);
        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: 20,
                rownumbers: true,
                pgtext: "第X页 / 共X页",
                colNames: ["ID", "分类名称", "状态", "商品是否必填", "是否手动认领", "是否对租户显示", "归属原因"],
                colModel: [{
                    name: "serviceId",
                    index: "serviceId",
                    editable: false,
                    width: 90,
                    sorttype: "string",
                    hidden: true
                },
                    {
                        name: "name",
                        index: "name",
                        editable: false,
                        width: 360,
                        sorttype: "string",
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 90,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '启用';
                            else if (cellvalue == 2) return '禁用';
                            else return "——"
                        }
                    },
                    {
                        name: "isHaveGoods",
                        index: "isHaveGoods",
                        editable: false,
                        width: 150,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '是';
                            else if (cellvalue == 2) return '否';
                            else return "——"
                        }
                    },
                    {
                        name: "isManualClaim",
                        index: "isManualClaim",
                        editable: false,
                        width: 150,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '是';
                            else if (cellvalue == 2) return '否';
                            else return "——"
                        }
                    },
                    {
                        name: "isShow",
                        index: "isShow",
                        editable: false,
                        width: 150,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '是';
                            else if (cellvalue == 2) return '否';
                            else return "——"
                        }
                    },
                    {
                        name: "reasonAttribution",
                        index: "reasonAttribution",
                        editable: false,
                        width: 250,
                        sorttype: "string",
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    if (deferLoadData == 0) {
                        fcwot_list.getServerData(currentPage);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        fcwot_list.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    fcwot_list.getServerData(currentPage);
                },
                //当选择行时触发此事件。
                onSelectRow: function () {
                    //返回选中的id
                    var data = $("#table").jqGrid('getGridParam', 'selrow');
                    selectedId = $('#table').jqGrid('getCell', data, 'serviceId');
                },
                gridComplete: function () {
                }
            });

            fcwot_list.getServerData(1);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        refreshServerData: function () {
            fcwot_list.getServerData(1);
        },
        getServerData: function (pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.configMan_firstClassWorkOrder_list, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                name: firstNameSearch,
                status: firstStaSearch,
                rank: 1,
                position: pageIndex,
                rows: pageCount
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.serviceItems;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        //搜索
        searchBox: function () {
            initializeEditForm();
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '搜索',
                content: $('#SearchCon'),
                btn: ['查询', '取消'],
                yes: function (index, layero) {
                    layer.close(i);
                    firstNameSearch = $("#seaTxt").val();
                    firstStaSearch = $("#seaSta").val();
                    fcwot_list.getServerData(1);
                }
            });
        },
        //新增-修改-act: 1-新增  2-修改
        addWorkOrder: function (act) {
            initializeEditForm();
            if (act == 2) {
                if (selectedId == null || selectedId == 0) {
                    utils.showMessage('请选择要进行修改的记录！');
                    return;
                }
                ;
                fcwot_list.updateList();
            }
            ;
            let titleVal = act == 1 ? '新增' : '修改';
            let arr = act == 1 ? ['新增', '取消'] : ['修改', '取消'];
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: titleVal,
                content: $('#AddCon'),
                btn: arr,
                yes: function (index, layero) {
                    var str = $("#addName").val();
                    if ($.trim(str).length <= 0) {
                        utils.showLayerMessage('请输入一级类别名称！');
                        return;
                    }
                    ;
                    var statuss = $("#addSta").val();
                    if (statuss.length <= 0) {
                        utils.showLayerMessage('请选择状态！');
                        return;
                    }
                    ;
                    var isHaveGoodss = $("#isHaveGoods").val();
                    if (isHaveGoodss.length <= 0) {
                        utils.showLayerMessage('请选择商品是否必填！');
                        return;
                    }
                    ;
                    var isManualClaimss = $("#ManualClaims").val();
                    if (isManualClaimss.length <= 0) {
                        utils.showLayerMessage('请选择是否手动认领！');
                        return;
                    }
                    ;
                    let url = act == 1 ? win.utils.services.configMan_WorkOrder_add : win.utils.services.configMan_WorkOrder_update;
                    let datas = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        name: str,
                        rank: 1,
                        status: statuss,
                        isManualClaim: isManualClaimss,
                        isHaveGoods: isHaveGoodss,
                        isShow: $("#isShow").val(),
                    };
                    if (act == 2) {
                        datas.serviceId = selectedId;
                    }
                    win.utils.ajaxPost(url, datas, function (result) {
                        if (result.result_code == 200) {
                            layer.close(i);
                            fcwot_list.refreshServerData();
                        } else {
                            let msg = act == 1 ? '添加失败，请稍后重试！' : '修改失败，请稍后重试！';
                            utils.showLayerMessage(msg);
                        }
                    });
                }
            });
        },
        //删除
        delWorkOrder: function () {
            initializeEditForm();
            if (selectedId == null || selectedId == 0) {
                utils.showMessage('请选择要进行删除的记录！');
                return;
            }
            ;
        },
    };
    var flagInput = false;
    //禁止弹窗与浏览器滚动
    $("body").on('focus', 'input,textarea', function () {
        flagInput = true;
    });
    $("body").on('blur', 'input,textarea', function () {
        flagInput = false;
    });
    $(document).keydown(function (event) {
        var e = window.event || event;
        var keyCode = event.keyCode;
        if (keyCode == 13 || keyCode == 32) {
            if (flagInput == false) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    window.event.returnValue = false;
                }
                return false;
            }
            ;
        }
    });
    win.fcwot_list = fcwot_list;
})(window, jQuery);