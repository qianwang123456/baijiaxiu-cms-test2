/**
 * Created by song
 */
(function(win, $) {
	var selectId='';	//table-选中的列表id
	var ConFlag;//弹层关闭
	var flagInput=false;//空格输入按钮  // 为true时,允许 输入空格与换行,false禁止输入空格与换行
	var unitBlFlag = true; //true表示校验通过  flase表示校验没有通过
	var deferLoadData = 0;
	var firstClassifyName = ''
	var secondClassifyName = ''
	//延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题

	// 初始化
	function initializeEditForm() {
		$("#addName").parent('div').find('em').eq(0).text('');
		$(".form-group").removeClass("has-error");
	};
	//关闭弹层
    function closeFlagCon(){
        layer.close(ConFlag);
		$("#AddCon").hide();
    }
	var reasonAndmaint_list = {
		// 根据id获取原因归属维修方案list
		getReasonMatinplanList : function(idval) {
			win.utils.ajaxPost(win.utils.services.getConfigReasonPlanDetail, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				serviceId :idval
			}, function(result) {
				if(result.result_code == 200) {
					//1原因归属  2方案归属
					let data = result.data
					let reasonArr = []
					let matinPlanArr = []
					let reasonArrHtml = ``
					let matinPlanArrHtml = ``
					data.map(function(i,v){
						let aVal = i.status==1?'selected':''
						let bVal = i.status==1?'':'selected'
						if(i.type==1){
							reasonArr.push(i)
							reasonArrHtml+=`<div class="col-sm-8"><input name="" data-id="`+i.id+`" class="form-control" autocomplete="off" value="`+i.name+`"></div><div class="col-sm-4"><select class="form-control"><option value="1" `+aVal+`>启用</option><option value="2" `+bVal+`>禁用</option></select></div>`
						}
						if(i.type==2){
							matinPlanArr.push(i)
							matinPlanArrHtml+=`<div class="col-sm-8"><input name="" data-id="`+i.id+`" class="form-control" autocomplete="off" value="`+i.name+`"></div><div class="col-sm-4"><select class="form-control"><option value="1" `+aVal+`>启用</option><option value="2" `+bVal+`>禁用</option></select></div>`
						}
					})
					$("#reason").empty().append(reasonArrHtml)
					$("#plan").empty().append(matinPlanArrHtml)
				} else {
					let stra=`<div class="col-sm-8"><input name="" class="form-control" autocomplete="off"></div><div class="col-sm-4"><select class="form-control"><option value="1">启用</option><option value="2">禁用</option></select></div>`
					$("#reason").empty().append(stra)
					$("#plan").empty().append(stra)
					let msg= result.description?result.description:'获取数据失败，请重试！';
					utils.showMessage(msg);
				}
			});
		},
		//修改
		editreasonMatinplan: function(idVal){
			initializeEditForm();
			$("#reason").empty()
			$("#plan").empty()

			//模拟测试starts
			// let data = [{"id":1,"name":"灯泡不亮/损坏","serviceId":591,"status":1,"type":1},
			// 	{"id":2,"name":"更换灯泡","serviceId":591,"status":1,"type":2},
			// 	{"id":3,"name":"部件松动/未安装牢固","serviceId":591,"status":2,"type":1},
			// 	{"id":4,"name":"安装紧固","serviceId":591,"status":2,"type":2}]
			// let reasonArr = []
			// let matinPlanArr = []
			// let reasonArrHtml = ``
			// let matinPlanArrHtml = ``
			// data.map(function(i,v){
			// 	if(i.type==1){
			// 		reasonArr.push(i)
			// 		reasonArrHtml+=`<div class="col-sm-8"><input name="" data-id="`+i.id+`" class="form-control" autocomplete="off" value="`+i.name+`"></div><div class="col-sm-4"><select class="form-control"><option value="1">启用</option><option value="2">禁用</option></select></div>`
			// 	}
			// 	if(i.type==2){
			// 		matinPlanArr.push(i)
			// 		matinPlanArrHtml+=`<div class="col-sm-8"><input name="" data-id="`+i.id+`" class="form-control" autocomplete="off" value="`+i.name+`"></div><div class="col-sm-4"><select class="form-control"><option value="1">启用</option><option value="2">禁用</option></select></div>`
			// 	}
			// })
			// $("#reason").empty().append(reasonArrHtml)
			// $("#plan").empty().append(matinPlanArrHtml)
			//模拟测试ends

			reasonAndmaint_list.getReasonMatinplanList(idVal);
			flagInput=false;
			ConFlag = layer.open({
				type: 1,
				area: ['80%', '80%'],
				title:"编辑",
				content: $('#AddCon'),
				btn: ['保存', '取消'],
				yes: function(index, layero) {
					unitBlFlag = true
					let arrReason = []  //提交修改后数据
					let textReasonArr = [] //校验原因归属是否重复
					let textPlanArr = [] //校验方案归属是否重复
					let arrPlan = []
					let emptyFlag = false;
					let longFlag = false;
					let textFlag = false;
					$.each($("#reason input"),function(i,v){
						let options = {
							serviceId:selectId,
							type:1
						}
						let inputVal = v.value;
						let selectVal = $("#reason select").eq(i).val();
						// console.log($("#reason input").eq(i).attr('data-id'))
						if($("#reason input").eq(i).attr('data-id')){
							options.id= $("#reason input").eq(i).attr('data-id')
						}
						if(inputVal.trim().length<=0||inputVal==''){
							unitBlFlag = false
							emptyFlag = true
							return
						}
						if(inputVal.length>30){
							unitBlFlag = false
							longFlag = true
						}
						// 校验输入内容只能为汉字数字字母
						// var pattern = /^[A-Za-z0-9\u4e00-\u9fa5]+$/gi;
						//如果值为空或通过，通过校验
						// if(pattern.test(inputVal)) {
						// 	textFlag = false
						// }else{
						// 	unitBlFlag = false
						// 	textFlag = true
						// }
						options.name = inputVal
						options.status = selectVal
						arrReason.push(options)
						if(textReasonArr.length<=0){
							textReasonArr.push(inputVal)
						}else{
							let comPareArr = [];
							for (let i = 0; i < textReasonArr.length; i++) {
								if(textReasonArr[i].length!=inputVal.length){
									comPareArr.push(textReasonArr[i])
								}else{
									if(textReasonArr[i]!=inputVal){
										comPareArr.push(inputVal)
									}
								}
							}
							if(comPareArr.length==textReasonArr.length){
								textReasonArr.push(inputVal)
							}
						}
					})
					if(emptyFlag == true){
						let msg = `原因归属内容不可为空！`;
						utils.showMessage(msg);
						return;
					}
					if(longFlag == true){
						let msg = `原因归属限制长度为30个字哦！`;
						utils.showMessage(msg);
						return;
					}
					// if (textFlag == true) {
					// 	let msg = `请输入正确格式的原因归属！`;
					// 	utils.showMessage(msg);
					// 	return;
					// }
					if(arrReason.length<=0){
						let msg = `请输入原因归属！`;
						utils.showMessage(msg);
						return;
					}
					// console.log(textReasonArr,arrReason,'ooo')
					if(textReasonArr.length!=arrReason.length){
						let msg = `不可出现重复的原因归属！`;
						utils.showMessage(msg);
						return;
					}
					$.each($("#plan input"),function(i,v){
						// console.log(i,$("#reason select").eq(i).val(),v.value,'原因归属')
						let options = {
							serviceId:selectId,
							type:2
						}
						let inputVal = v.value;
						let selectVal = $("#plan select").eq(i).val();
						if($("#plan input").eq(i).attr('data-id')){
							options.id= $("#plan input").eq(i).attr('data-id')
						}
						options.name = inputVal
						options.status = selectVal
						arrPlan.push(options)
						if(textPlanArr.length<=0){
							textPlanArr.push(inputVal)
						}else{
							let comPareArr = [];
							for (let i = 0; i < textPlanArr.length; i++) {
								if(textPlanArr[i].length!=inputVal.length){
									comPareArr.push(inputVal)
								}else{
									if(textPlanArr[i]!=inputVal){
										comPareArr.push(inputVal)
									}
								}
							}
							if(comPareArr.length==textPlanArr.length){
								textPlanArr.push(inputVal)
							}
						}
						if(inputVal.trim().length<=0){
							unitBlFlag = false
							emptyFlag = true
						}
						if(inputVal.length>30){
							unitBlFlag = false
							longFlag = true
						}
						// 校验输入内容只能为汉字数字字母
						var pattern = /^[A-Za-z0-9\u4e00-\u9fa5]+$/gi;
						//如果值为空或通过，通过校验
						// if (pattern.test(inputVal) == false) {
						// 	unitBlFlag = false
						// 	textFlag = true
						// }
					})
					if(emptyFlag == true){
						let msg = `方案归属内容不可为空！`;
						utils.showMessage(msg);
						return;
					}
					if(longFlag == true){
						let msg = `方案归属限制长度为30个字哦！`;
						utils.showMessage(msg);
						return;
					}
					// if (textFlag == true) {
					// 	let msg = `请输入正确格式的方案归属！`;
					// 	utils.showMessage(msg);
					// 	return;
					// }
					if(arrPlan.length<=0){
						let msg = `请输入方案归属！`;
						utils.showMessage(msg);
						return;
					}
					if(textPlanArr.length!=arrPlan.length){
						let msg = `不可出现重复的方案归属！`;
						utils.showMessage(msg);
						return;
					}
					if(unitBlFlag == true){
						let data={
							user_id: win.utils.getCookie('user_id'),
							session: win.utils.getCookie('session'),
							reasonPlans:arrPlan.concat(arrReason)
								// [{"id":1,"name":"灯泡不亮/损坏","serviceId":591,"status":1,"type":1},
								// {"id":2,"name":"更换灯泡","serviceId":591,"status":1,"type":2},
								// {"id":3,"name":"部件松动/未安装牢固","serviceId":591,"status":1,"type":1},
								// {"id":4,"name":"安装紧固","serviceId":591,"status":1,"type":2},
								// {"id":5,"name":"测试456哈哈哈","serviceId":591,"status":1,"type":1},
								// {"id":6,"name":"侧视777哈哈哈","serviceId":591,"status":1,"type":2}]
						};
						// console.log(arr,data)
						// return;
						let url=win.utils.services.editConfigReasonPlanDetail;
						win.utils.ajaxPost(url, data, function(result) {
							closeFlagCon();
							if(result.result_code == 200) {
								reasonAndmaint_list.refreshServerData();
							} else {
								let msg= result.description?result.description:'失败啦，请稍后重试！';
								utils.showMessage(msg);
							}
						});
					}
				},
				btn2: function(index, layero) {
					layer.close(ConFlag);
					$("#AddCon").hide();
				},
				cancel: function(index, layero) {
					layer.close(ConFlag);
					$("#AddCon").hide();
				},
				end:function(){
					$("#AddCon").hide();
				}

			});
		},
		//初始化
		init: function() {
			var obj = this;
			if(utils.isLogin() == false) {
				utils.loginDomain();
				return;
			}
			//判断是否有权限，此时为配置管理的原因归属维修方案权限
			utils.isUserMenuIndexRight('reasonAndmaint_list', function(result) {
				if(result == 1) {
					//initialize...
					obj.initList();
					win.utils.clearSpacesTextarea();
				} else {
					self.location = '../../../web/error/access_refuse.html';
					return;
				}
			});
			//编辑按钮
			$("#table").on('click',"button",function(){
				selectId = $(this).attr('data-id');
				let rowData = $("#table").jqGrid('getRowData',selectId);
				firstClassifyName = rowData.firstClassifyName
				secondClassifyName = rowData.secondClassifyName
				// $("#maintClassify").html(firstClassifyName+'——'+secondClassifyName)
				reasonAndmaint_list.editreasonMatinplan(selectId);
			});
			// 添加原因归属
			$("#addRea").on('click',function(){
				// console.log('添加')
				let str=`<div class="col-sm-8"><input name="" class="form-control" autocomplete="off"></div><div class="col-sm-4"><select class="form-control"><option value="1">启用</option><option value="2">禁用</option></select></div>`
				$("#reason").append(str)
			})
			// 添加方案归属
			$("#addPlan").on('click',function(){
				// console.log('添加')
				let str=`<div class="col-sm-8"><input name="" class="form-control" autocomplete="off"></div><div class="col-sm-4"><select class="form-control"><option value="1">启用</option><option value="2">禁用</option></select></div>`
				$("#plan").append(str)
			})
		},
		//初始化请求
		initList: function() {
			//订单列表
			$("#table").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height:'auto',//utils.getAutoGridHeightSong(),
				autowidth: true,
				shrinkToFit:false,
				autoScroll: true,
				rowNum: 10,
				rownumbers: false,
				rownumWidth:80,
				// rowList: [10,20,50,100],
				pgtext: "第X页 / 共X页",
				colNames: ['id','操作','一级分类',"二级分类","原因归属","状态","方案归属","状态"],
				colModel: [
					{
						name: "secondClassifyId",
						index: "secondClassifyId",
						width: 84,
						hidden:true,
					},
					{
						name: "secondClassifyId",
						index: "secondClassifyId",
						editable: false,
						width: 140,
						formatter: function(cellvalue,options, rowObject) {
							return '<button name="edit" class="btn btn-primary unified-btn-inline unified-change-btn-inline" type="button"data-id="'+rowObject.secondClassifyId+'">编辑</button>'
						},
					},
					{
						name: "firstClassifyName",
						index: "firstClassifyName",
						editable: false,
						width: 150,
						sorttype: "string",
					},
					{
						name: "secondClassifyName",
						index: "secondClassifyName",
						editable: false,
						width: 150,
						sorttype: "string",
					},
					{
						name: "secondClassifyId",
						index: "secondClassifyId",
						editable: false,
						width: 220,
						formatter:function(cellvalue,options, rowObject) {
							if(rowObject && rowObject.reasonPlans){
								if(rowObject.reasonPlans[0].type==1){
									let nameVal = rowObject.reasonPlans[0].name?rowObject.reasonPlans[0].name:''
									return nameVal
								}else if(rowObject.reasonPlans[1].type==1){
									let nameVal = rowObject.reasonPlans[1].name?rowObject.reasonPlans[1].name:''
									return nameVal
								}else{
									return ''
								}
							}else{
								return ''
							}
						},
					},
					{
						name: "secondClassifyId",
						index: "secondClassifyId",
						editable: false,
						width: 84,
						sorttype: "string",
						formatter:function(cellvalue,options, rowObject) {
							if(rowObject && rowObject.reasonPlans) {
								if (rowObject.reasonPlans[0].type == 1) {
									if (rowObject.reasonPlans[0].status == 1) {
										return '启用'
									} else {
										return '禁用'
									}
								}else if (rowObject.reasonPlans[1].type == 1) {
									if (rowObject.reasonPlans[1].status == 1) {
										return '启用'
									} else {
										return '禁用'
									}
								}else{
									return ''
								}
							}else{
								return ''
							}
						},
					},
					{
						name: "secondClassifyId",
						index: "secondClassifyId",
						editable: false,
						width: 220,
						sorttype: "string",
						formatter:function(cellvalue,options, rowObject) {
							if(rowObject && rowObject.reasonPlans) {
								if (rowObject.reasonPlans[0].type == 2) {
									let nameVal = rowObject.reasonPlans[0].name?rowObject.reasonPlans[0].name:''
									return nameVal
								}else if (rowObject.reasonPlans[1].type == 2) {
									let nameVal = rowObject.reasonPlans[1].name?rowObject.reasonPlans[1].name:''
									return nameVal
								}else{
									return ''
								}
							}else{
								return ''
							}
						},
					},
					{
						name: "secondClassifyId",
						index: "secondClassifyId",
						editable: false,
						width: 84,
						sorttype: "string",
						formatter:function(cellvalue,options, rowObject) {
							if(rowObject && rowObject.reasonPlans) {
								if (rowObject.reasonPlans[0].type == 2) {
									if (rowObject.reasonPlans[0].status == 1) {
										return '启用'
									} else {
										return '禁用'
									}
								}else if (rowObject.reasonPlans[1].type == 2) {
									if (rowObject.reasonPlans[1].status == 1) {
										return '启用'
									} else {
										return '禁用'
									}
								}else{
									return ''
								}
							}else{
								return ''
							}
						},
					}
				],
				pager: "#table_pager",
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
				pgbuttons:true,
				pginput:true,
				onPaging: function(pgButton) {
					deferLoadData = 0;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					lastPage = $("#table").jqGrid('getGridParam', 'lastpage');
					if(pgButton == 'next') {
						currentPage = currentPage + 1;
					}
					if(pgButton == 'last') {
						currentPage = lastPage;
					}
					if(pgButton == 'prev') {
						currentPage = currentPage - 1;
					}
					if(pgButton == 'first') {
						currentPage = 1;
					}
					if(pgButton == 'user') {
						deferLoadData = 1;
					}
					if(pgButton == 'records') {
						deferLoadData = 1;
					}
					if(deferLoadData == 0) {
						reasonAndmaint_list.getServerData(currentPage);
					}
				},
				beforeRequest: function() {
					if(deferLoadData == 1) {
						reasonAndmaint_list.getServerData(currentPage);
						deferLoadData = 0;
					}
				},
				onSortCol: function(index, iCol, sortorder) {

				},
				//当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
				onSelectRow: function () {
					//返回选中的id
					let rowId = $("#" + this.id).getGridParam('selrow');
					let rowData = $("#table").jqGrid('getRowData',rowId);
					// console.log(rowData,'999')
					firstClassifyName = rowData.firstClassifyName
					secondClassifyName = rowData.secondClassifyName
					$("#maintClassify").html(firstClassifyName+'——'+secondClassifyName)
				},
				gridComplete: function() {
					$("#table").jqGrid('resetSelection');
					$("#table").jqGrid('setSelection',selectId);
				}
			});
			$("#table").jqGrid("navGrid", "#table_pager", {
				edit: false,
				add: false,
				del: false,
				search: false,
				refresh: false,
			});
			reasonAndmaint_list.getServerData(1);
			$(window).bind("resize", function () {
				var b = $(".jqGrid_wrapper").width();
				if(b>=1844){
					b=1844;
				};
				$("#table").setGridWidth(b);
			});
		},
		refreshServerData: function() {
			reasonAndmaint_list.getServerData(1);
		},
		//list----getData
		getServerData: function(pageIndex) {
			selectId='';//清空id
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				position: pageIndex,
				rows: 10,
			};

			//分界线---start
			// let lists = [{
			// 	secondClassifyId:123,
			// 	secondClassifyName: '123ffvfv',
			// 	firstClassifyName: '和呼出的成本',
			// 	reasonPlans:[
			// 		{status:1,type:1,name:'测试原因属性'},
			// 		{status:2,type:2,name:'测试3333'}
			// 	]
			// }]
			// gridData = lists;
			// totalPages = Math.ceil(2 / 22);
			// nowCurPage= 1;//订单列表---当前页码数
			// totalNowPages = totalPages;//订单列表---总的页码数
			// var gridJson = {
			// 	total: totalPages,
			// 	rows: gridData,
			// 	page: 1,
			// 	records: 2
			// };
			// // pageText = '第<input id="inputNowPages" type="number" min=1 value="'+pageIndex+'">页 / 共' + totalPages + '页';
			// pageText = '第'+1+'页 / 共' + totalPages + '页';
			// $("#table").jqGrid('setGridParam', {
			// 	pgtext: pageText
			// });
			// $("#table")[0].addJSONData(gridJson);
			// return;
			//分界线---end


			pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
			win.utils.ajaxPost(win.utils.services.getConfigReasonPlanList, datas, function(result) {
				if(result.result_code == 200) {
					if(result.total == 0) {
						gridData = {};
						var gridJson = {
							total: 0,
							rows: gridData,
							page: 0,
							records: result.total
						};
						pageText = '第0页 / 共0页';
						nowCurPage= 0;//订单列表---当前页码数
						totalNowPages = 0;//订单列表---总的页码数
						$("#table").jqGrid('setGridParam', {
							pgtext: pageText
						});
						$("#table")[0].addJSONData(gridJson);
					} else {
						gridData = result.lists;
						if(gridData.length>0){
							selectId=gridData[0].id;
						}else{
							selectId='';
						};
						totalPages = Math.ceil(result.total / pageCount);
						nowCurPage= pageIndex;//订单列表---当前页码数
						totalNowPages = totalPages;//订单列表---总的页码数
						var gridJson = {
							total: totalPages,
							rows: gridData,
							page: pageIndex,
							records: result.total
						};
						// pageText = '第<input id="inputNowPages" type="number" min=1 value="'+pageIndex+'">页 / 共' + totalPages + '页';
						pageText = '第'+pageIndex+'页 / 共' + totalPages + '页';
						$("#table").jqGrid('setGridParam', {
							pgtext: pageText
						});
						$("#table")[0].addJSONData(gridJson);
					}
				} else {
					let msg= result.description?result.description:'操作失败，请稍后重试！';
					utils.showMessage(msg);
				}
			});
		},
	};
	// 阻止空格与enter键触发layer.open事件（即出现蒙层）
	$(document).keydown(function (event) {
		var  e=window.event || event;
		var keyCode = event.keyCode;
		if (keyCode == 13 || keyCode == 32) {
			if(flagInput==false){
				if(e.preventDefault){
					e.preventDefault();
				}else{
					window.event.returnValue = false;
				}
				return false;
			}
		}
	});
	win.reasonAndmaint_list = reasonAndmaint_list;
})(window, jQuery);


//获取维修方案详情
// {"result_code":200,"description":"success","data":[{"id":1,"name":"灯泡不亮/损坏","serviceId":591,"status":1,"type":1},{"id":2,"name":"更换灯泡","serviceId":591,"status":1,"type":2},{"id":3,"name":"部件松动/未安装牢固","serviceId":591,"status":1,"type":1},{"id":4,"name":"安装紧固","serviceId":591,"status":1,"type":2}]}
