/**
 * Created by song.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    var nowPage = 1;
    //table选中行#id
    var selectedId = 0;
    //search
    var behaviorClassifyS = '';
    var reasonS = '';
    var statusS = '';
    var isCheckS = '';

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
    };

    function initSearch() {
        behaviorClassifyS = '';
        reasonS = '';
        statusS = '';
        isCheckS = '';
        $("#behaviorClassifyS").val("");
        $("#reasonS").val("");
        $("#statusS").val("");
        $("#isCheckS").val("");
    };
    var maintenOrderReason = {
        updateList: function () {
            let url = win.utils.services.configMan_maintenOrderReason_list;
            let datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                id: parseInt(selectedId),
            };
            win.utils.ajaxPost(url, datas, function (result) {
                if (result.result_code == 200) {
                    var myObj = result.serviceReturnReasons[0];
                    $("#behaviorClassifyA").val(myObj.behaviorClassify);
                    $("#reasonA").val(myObj.reason);
                    $("#statusA").val(myObj.status);
                    $("#isCheckA").val(myObj.isCheck);
                    $("#sortA").val(myObj.sort);
                } else {
                    utils.showMessage('获取数据失败，请稍后重试！');
                }
            });
        },
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('maintenOrderReason', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                    win.utils.clearSpaces();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });
            // 搜索
            $('#btnSearch').click(obj.searchBox);
            //刷新
            $('#btnQuery').on('click', function () {
                initSearch();
                obj.refreshServerData();
            });
            //新增
            $('#btnAdd').on('click', function () {
                maintenOrderReason.addWorkOrder(1)
            });
            //修改
            $('#btnUpdate').on('click', function () {
                maintenOrderReason.addWorkOrder(2)
            });
            //删除
            $("#btnDel").on('click', function () {
                if (selectedId == null || selectedId == 0) {
                    utils.showMessage('请选择要进行删除的记录！');
                    return;
                }
                ;
                maintenOrderReason.delWorkOrder()
            });
            //校验数字
            $('#sortA').on('blur', function () {
                let val = $(this).val();
                val = parseInt(val);
                if (val < 1) {
                    val = 1
                }
                ;
                $(this).val(val)
            });

        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: 20,
                rownumbers: true,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["ID", "行为分类", "原因", "排序", "状态", "审批", "设置人"],
                colModel: [{
                    name: "id",
                    index: "id",
                    editable: false,
                    width: 90,
                    sorttype: "string",
                    hidden: true
                },
                    {
                        name: "behaviorClassify",
                        index: "behaviorClassify",
                        editable: false,
                        width: 120,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '退单';
                            else return '撤单';
                        }
                    },
                    {
                        name: "reason",
                        index: "reason",
                        editable: false,
                        width: 320,
                        sorttype: "string"
                    },
                    {
                        name: "sort",
                        index: "sort",
                        editable: false,
                        width: 84,
                        sorttype: "string"
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 80,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '启用';
                            else return '禁用';
                        }
                    },
                    {
                        name: "isCheck",
                        index: "isCheck",
                        editable: false,
                        width: 80,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 1) return '需要';
                            else return '不需要';
                        }
                    },
                    {
                        name: "createUser",
                        index: "createUser",
                        editable: false,
                        width: 150,
                        sorttype: "string"
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        maintenOrderReason.getServerData(currentPage);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        nowPage = currentPage;
                        maintenOrderReason.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    maintenOrderReason.getServerData(currentPage);
                },
                //当选择行时触发此事件。
                onSelectRow: function () {
                    //返回选中的id
                    var data = $("#table").jqGrid('getGridParam', 'selrow');
                    // var selectedIds = $("#table").jqGrid('getRowData',data,'serviceId');
                    selectedId = $('#table').jqGrid('getCell', data, 'id');
                },
                gridComplete: function () {

                }
            });

            maintenOrderReason.getServerData(1);
            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        refreshServerData: function () {
            maintenOrderReason.getServerData(1);
        },
        getServerData: function (pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            var datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 3,
                position: nowPage,
                rows: 20,
                behaviorClassify: parseInt(behaviorClassifyS),// 行为分类 1-退单，2-撤单 ,
                reason: reasonS,
                status: parseInt(statusS),//状态 1-启动，2禁用 ,
                isCheck: parseInt(isCheckS),//是否需要审批 1-需要，2不需要 ,
                // sort (integer, optional): 排序 ,
            };
            win.utils.ajaxPost(win.utils.services.configMan_maintenOrderReason_list, datas, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.serviceReturnReasons;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        //搜索
        searchBox: function () {
            initializeEditForm();
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '查询',
                content: $('#SearchCon'),
                btn: ['查询', '取消'],
                yes: function (index, layero) {
                    layer.close(i);
                    behaviorClassifyS = $("#behaviorClassifyS").val();
                    reasonS = $("#reasonS").val();
                    statusS = $("#statusS").val();
                    isCheckS = $("#isCheckS").val();
                    maintenOrderReason.refreshServerData();
                }
            });
        },
        //新增-修改act:1新增  2:修改
        addWorkOrder: function (act) {
            initializeEditForm();
            if (act == 2) {
                if (selectedId == null || selectedId == 0) {
                    utils.showMessage('请选择要进行修改的记录！');
                    return;
                }
                ;
                maintenOrderReason.updateList();
            }
            ;
            let titleVal = act == 1 ? '新增' : '修改';
            let arr = act == 1 ? ['新增', '取消'] : ['修改', '取消'];
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: titleVal,
                content: $('#AddCon'),
                btn: arr,
                yes: function (index, layero) {
                    var behaveVal = $("#behaviorClassifyA").val();
                    if ($.trim(behaveVal).length <= 0) {
                        utils.showLayerMessage('请选择行为分类！');
                        return;
                    }
                    ;
                    var reasonVal = $("#reasonA").val();
                    if ($.trim(reasonVal).length <= 0) {
                        utils.showLayerMessage('请填写分类名称或原因！');
                        return;
                    }
                    ;
                    var sortVal = $("#sortA").val();
                    if ($.trim(sortVal).length <= 0) {
                        utils.showLayerMessage('请填写位置！');
                        return;
                    }
                    ;
                    var isCheckVal = $("#isCheckA").val();
                    if ($.trim(isCheckVal).length <= 0) {
                        utils.showLayerMessage('请选择是否需要审批！');
                        return;
                    }
                    ;
                    var statusVal = $("#statusA").val();
                    if (statusVal.length <= 0) {
                        utils.showLayerMessage('请选择状态！');
                        return;
                    }
                    ;
                    let url = act == 1 ? win.utils.services.configMan_maintenOrderReason_add : win.utils.services.configMan_maintenOrderReason_update;
                    let datas = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        behaviorClassify: parseInt(behaveVal),
                        isCheck: parseInt(isCheckVal),
                        reason: reasonVal,
                        sort: sortVal,
                        status: parseInt(statusVal),
                    };
                    if (act == 2) {
                        datas.id = selectedId;
                    }
                    win.utils.ajaxPost(url, datas, function (result) {
                        if (result.result_code == 200) {
                            selectedId = 0;
                            layer.close(i);
                            maintenOrderReason.refreshServerData();
                        } else {
                            let msg = act == 1 ? '新增失败，请稍后重试！' : '修改失败，请稍后重试！';
                            utils.showLayerMessage(msg);
                        }
                    });
                }
            });
        },
        //删除
        delWorkOrder: function () {
            let url = win.utils.services.configMan_maintenOrderReason_delete;
            let datas = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                id: selectedId
            };
            win.utils.ajaxPost(url, datas, function (result) {
                if (result.result_code == 200) {
                    selectedId = 0;
                    maintenOrderReason.refreshServerData();
                } else {
                    let msg = '删除失败，请稍后重试！';
                    utils.showLayerMessage(msg);
                }
            });
        }
    };
    var flagInput = false;
    //禁止弹窗与浏览器滚动
    $("body").on('focus', 'input,textarea', function () {
        flagInput = true;
    });
    $("body").on('blur', 'input,textarea', function () {
        flagInput = false;
    });
    $(document).keydown(function (event) {
        var e = window.event || event;
        var keyCode = event.keyCode;
        if (keyCode == 13 || keyCode == 32) {
            if (flagInput == false) {
                if (e.preventDefault) {
                    e.preventDefault();
                } else {
                    window.event.returnValue = false;
                }
                return false;
            }
            ;
        }
    });
    win.maintenOrderReason = maintenOrderReason;
})(window, jQuery);