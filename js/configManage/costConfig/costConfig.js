/**
 * Created by hanlu.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';

    var nowPage = 1;

    var flagInput = false;

    var oldPrice = 0;

    var businessArr = [], cityChooseArr = [];

    var getListData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
    };


    var cost_config = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            // 公告管理权限
            utils.isUserMenuIndexRight('constConfig', function (result) {
                if (result == 1) {
                    obj.initList();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });
            // // 刷新
            $('#btnRefresh').click(obj.refersh);
            // 增加
            $('#btnAdd').click(obj.addRecord);
            // 修改
            $('#table').on('click', '#btnUpdate', function () {
                var id = $(this).attr('data-id');
                obj.updateRecord(id);
            });
            // 删除
            $('#table').on('click', '#btnDel', function () {
                var id = $(this).attr('data-id');
                obj.deleteRecord(id);
            });

            // 城市选择
            $('#edit_city').on('change', function () {
                cityId = $('#edit_city').val();
                cityName = $('#edit_city').find("option:selected").text();

                var is_push_city = true; //是否增加城市
                if (cityId) {
                    if (cityChooseArr.length) {
                        cityChooseArr.forEach(function (i, k) {
                            if (i.cityId == cityId) {
                                is_push_city = false;
                            }
                        })
                    }
                    if (is_push_city) {
                        cityChooseArr.push({
                            cityId,
                            cityName,
                            business: []
                        });
                    }

                    cost_config.getTradingData($('#edit_trading'), $('#edit_business'), cityId, cityName);

                    // 行政区域显示
                    $('.edit-trading').show();
                    $('.edit-business').hide();
                } else {
                    $('.edit-trading').hide();
                    $('.edit-business').hide();
                }
            });
            // 行政区域选择
            $('#edit_trading').on('change', function () {
                var tradingId = $(edit_trading).val();
                if (tradingId) {
                    cost_config.getBusinessData($('#edit_business'), tradingId, cityId, cityName);
                    $('.edit-business').show();
                } else {
                    $('.edit-business').hide();
                }
            });
            // 商圈选择
            layui.use('form', function () {
                form = layui.form;
                form.on('select(edit_business)', function (data) {
                    businessArr = data.value;
                });
            });

            $('.addBusiness').on('click', function () {
                cost_config.getCityArr();
                cost_config.arrToDom()
            });


            // 新增--费用校验
            $('#costDetail').on('blur', function () {
                var costType = $('#costType').val() ? $('#costType').val() : null;
                var costNum = Number($('#costDetail').val()) ? Number($('#costDetail').val()) : null;
                if (costNum && costType) {
                    cost_config.cost_check(costType, costNum);
                }
            });
            $('#costType').on('change', function () {
                var costType = $('#costType').val() ? $('#costType').val() : null;
                var costNum = Number($('#costDetail').val()) ? Number($('#costDetail').val()) : null;
                if (costNum && costType) {
                    cost_config.cost_check(costType, costNum);
                }
            });

            // 编辑--费用类型修改
            // $('#edit_costType').on('change',function () {
            //    var costType = $(this).val();
            //    if(costType==3){
            //        $('#edit_layer').addClass('other-edit-box');
            //        cost_config.getCityData($('#edit_city'));
            //    }
            //    else{
            //        $('#edit_layer').removeClass('other-edit-box');
            //        $('.edit-trading').hide();
            //        $('.edit-business').hide();
            //    }
            //    cost_config.cost_check();
            // });

            // 编辑--费用修改
            $('#edit_costDetail').on('blur', function () {
                var costType = $('#edit_costType').val();
                var costNum = Number($('#edit_costDetail').val()) ? Number($('#edit_costDetail').val()) : null;
                if (costNum && costType && oldPrice != costNum) {
                    cost_config.cost_check(costType, costNum);
                }
            });


            // 删除所选商圈
            $('.area-show-item-box').on('click', 'img', function () {
                var cityId = $(this).attr('data-cityId'), index = $(this).attr('data-index');
                cityChooseArr.forEach(function (i, k) {
                    if (i.cityId == cityId) {
                        i.business.splice(index, 1);
                    }
                    if (!i.business.length) {
                        cityChooseArr.splice(k, 1)
                    }
                })
                cost_config.arrToDom(cityChooseArr);
            });


            $('#content').on('focus', function () {
                flagInput = true;
            });

            $(document).keydown(function (event) {
                var e = window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if (flagInput == false) {
                        if (e.preventDefault) {
                            e.preventDefault();
                        } else {
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });
        },
        initList: function () {
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: true,
                rownumbers: true,
                rownumWidth: 60,
                rowNum: -1,
                // pgtext: "第X页 / 共X页",
                colNames: ["操作", "费用类型", "费用详情", "覆盖商圈", "备注"],
                colModel: [
                    {
                        name: "publishStatus",
                        index: "publishStatus",
                        editable: false,
                        width: 140,
                        sortable: false,
                        formatter: optionsBtnFn
                    },
                    {
                        name: "costType",
                        index: "costType",
                        editable: false,
                        width: 100,
                        sortable: false,
                        formatter: function (cellvalue, options, rowObject) {
                            if (rowObject.costType == 1) {
                                return '上门费'
                            } else if (rowObject.costType == 2) {
                                return '加急费'
                            } else if (rowObject.costType == 3) {
                                return '其他费用'
                            } else {
                                return ''
                            }
                        }
                    },
                    {
                        name: "price",
                        index: "price",
                        editable: false,
                        width: 80,
                        sortable: false,
                    },
                    {
                        name: "costAreaVOS",
                        index: "costAreaVOS",
                        editable: false,
                        width: 400,
                        sortable: false,
                        formatter: function (cellvalue, options, rowObject) {
                            if (rowObject.costAreaVOS) {
                                var cityStr = '';
                                rowObject.costAreaVOS.forEach(function (i, k) {
                                    var businessArr = [];
                                    i.business.forEach(function (businessI, businessK) {
                                        businessArr.push(businessI.businessName);
                                    });

                                    cityStr += i.cityName + ':' + businessArr.join('、') + '<br/>';
                                })
                                return cityStr
                            } else {
                                return ''
                            }
                        }
                    },
                    {
                        name: "costRemark",
                        index: "costRemark",
                        editable: false,
                        width: 240,
                        sortable: false
                    }
                ],
                viewrecords: true,
                recordpos: "left",
                caption: "",
                hidegrid: false,
            });

            cost_config.getServerData(nowPage, getListData);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });

            function optionsBtnFn(cellvalue, options, rowObject) {
                return '<button id="btnUpdate" class="btn btn-primary unified-btn-inline unified-change-btn-inline" type="button"data-id="' + rowObject.id + '">编辑</button>' +
                    '<button id="btnDel" class="btn btn-danger unified-btn-inline unified-deleted-btn-inline" type="button" data-id="' + rowObject.id + '">删除</button>'
            };
        },

        // 获取费用配置列表
        getServerData: function (pageIndex, postData) {
            // pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.configMan_costConfig_list, postData, function (result) {
                if (result.result_code == 200) {
                    if (result.costConfigures.length <= 0) {
                        gridData = {};
                        var gridJson = {
                            rows: gridData,
                        };
                        $("#table")[0].addJSONData(gridJson);

                        utils.showMessage('暂无数据！');
                    } else {
                        gridData = result.costConfigures;
                        var gridJson = {
                            rows: gridData,
                        };
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 增加费用配置
        addRecord: function () {
            cost_config.layerInit();
            $('#model strong').show();
            var i = layer.open({
                type: 1,
                area: ['40%', '60%'],
                title: '<i class="fa fa-plus">&nbsp;&nbsp;添加费用配置</i>',
                content: $('#model'),
                btn: ['保存', '取消'],
                success: function () {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);
                    $('#costDetail').on('input', function () {
                        cost_config.priceCheck(this);
                    });
                    // $('#costDetail').on('blur',function () {
                    //     cost_config.overFormat(this);
                    // })
                },
                yes: function (index, layero) {
                    var costType = $('#costType').val();
                    var costName = $('#costType').find('option:selected').text();
                    var price = $('#costDetail').val();
                    var remark = $('textarea[name=remark]').val();


                    if (costType == '') {
                        utils.showMessage('请选择费用类型！');
                        return;
                    }
                    if (price == '') {
                        utils.showMessage('请添加费用详情！');
                        return;
                    }
                    if (remark.length > 100) {
                        utils.showMessage('备注不得超过100个字！');
                        return;
                    }
                    win.utils.ajaxPost(win.utils.services.configMan_costConfig_add, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        costType: costType,
                        costName: costName,
                        price: price.trim(),
                        costRemark: remark.trim()
                    }, function (result) {
                        if (result.result_code == 200) {
                            utils.showMessage('添加成功！');
                            layer.close(i);
                            nowPage = 1;
                            cost_config.refersh();
                        } else if (result.result_code == 6601) {
                            utils.showMessage('该数据已存在，请重新添加！');
                            return;
                        } else {
                            utils.showMessage('添加失败，请重试！');
                            return;
                        }
                    });

                }
            });
        },
        // 增加费用配置弹层初始化
        layerInit: function () {
            $('#costType').val('');
            $('#costDetail').val('');
            $('textarea[name=remark]').val('');
            $('.cost-check').hide();
        },
        // 修改
        updateRecord: function (id) {
            $('.cost-check').hide();

            win.utils.ajaxPost(win.utils.services.configMan_costConfig_detail, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                id: id
            }, function (result) {
                if (result.result_code == 200) {

                    oldPrice = result.configCostConfigure.price;

                    $('#edit_costType').val(result.configCostConfigure.costType);
                    $('#edit_costType').attr('disabled', 'disabled');
                    $('#edit_costDetail').val(result.configCostConfigure.price);
                    $('textarea[name=edit_remark]').val(result.configCostConfigure.costRemark);
                    if (result.configCostConfigure.costAreaVOS) {
                        cityChooseArr = result.configCostConfigure.costAreaVOS;
                    } else {
                        cityChooseArr = [];
                    }
                    cost_config.arrToDom(cityChooseArr);


                    if (result.configCostConfigure.costType == 3) {
                        $('#edit_layer').addClass('other-edit-box');
                        cost_config.getCityData($('#edit_city'));
                    } else {
                        $('#edit_layer').removeClass('other-edit-box');
                    }

                } else {
                    utils.showMessage('获取数据失败，请重试！');
                    return;
                }
            });

            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;修改费用配置',
                content: $('#edit_layer'),
                btn: ['保存', '取消'],
                success: function () {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);

                    $('#costDetail').on('input', function () {
                        cost_config.priceCheck(this);
                    });
                },
                yes: function (index, layero) {
                    var costType = $('#edit_costType').val();
                    var costName = $('#edit_costType').find('option:selected').text();
                    var price = $('#edit_costDetail').val();
                    var remark = $('textarea[name=edit_remark]').val();

                    if (costType == '') {
                        utils.showMessage('请选择费用类型！');
                        return;
                    }
                    if (price == '') {
                        utils.showMessage('请添加费用详情！');
                        return;
                    }
                    ;
                    if (remark.length > 100) {
                        utils.showMessage('备注不得超过100个字！');
                        return;
                    }
                    win.utils.ajaxPost(win.utils.services.configMan_costConfig_update, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        id: id,
                        costType: costType,
                        costName: costName,
                        price: price.trim(),
                        costRemark: remark.trim(),
                        costAreaVOS: costType == 3 ? cityChooseArr : null
                    }, function (result) {
                        if (result.result_code == 200) {
                            layer.close(i);
                            utils.showMessage('修改成功！');
                            // $('#table').jqGrid('setRowData',id,{
                            //     costType:costType,
                            //     price:price,
                            //     id:id,
                            //     costRemark:remark,
                            //     costAreaVOS: costType==3?cityChooseArr:null
                            // });
                            cost_config.refersh();

                            $('.edit-trading').hide();
                            $('.edit-business').hide();
                            cityChooseArr = [];
                        } else if (result.result_code == 6601) {
                            utils.showMessage('该数据已存在！');
                            return;
                        } else {
                            utils.showMessage('修改失败，请重试！');
                            return;
                        }
                    });
                },
                btn2: function () {
                    $('.edit-trading').hide();
                    $('.edit-business').hide();
                    cityChooseArr = [];
                },
                cancel: function () {
                    $('.edit-trading').hide();
                    $('.edit-business').hide();
                    cityChooseArr = [];
                }
            });
        },
        // 删除费用配置
        deleteRecord: function (id) {
            utils.showConfirm('您确定要删除吗？', '删除', function () {
                win.utils.ajaxPost(win.utils.services.configMan_costConfig_delete, {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    id: id
                }, function (result) {
                    if (result.result_code == 200) {
                        utils.closeMessage();
                        $('#table').jqGrid('delRowData', id);
                    } else {
                        utils.showMessage('删除失败，请重试！');
                    }
                });
            });
        },
        // 刷新
        refersh: function () {
            $('#costType').val('');
            $('#costDetail').val('');
            $('#remark').text('');
            getListData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
            };
            cost_config.getServerData(nowPage, getListData);
        },


        // 获取列表搜索城市数据
        getCityData: function (nodeName) {
            // 获取城市数据
            win.utils.ajaxPost(win.utils.services.getSysArea, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                levelFlag: 1
            }, function (result) {
                if (result.result_code == 200) {
                    var cityOptionStr = '<option value="">请选择城市</option>';
                    result.areaVOList.forEach(function (i, k) {
                        cityOptionStr += '<option value="' + i.id + '">' + i.addressName + '</option>';
                    });
                    nodeName.html(cityOptionStr);
                }
            })
        },
        // 查询当前管理员负责城市
        getQueryUserCity: function (nodeName) {
            win.utils.ajaxPost(win.utils.services.queryUserCity, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                // levelFlag:1
                userId: win.utils.getCookie('user_id')
            }, function (result) {
                if (result.result_code == 200) {
                    var cityOptionStr = '<option value="">城市</option>';
                    if (nodeName.attr('id') == 'transferCity') {
                        cityOptionStr = '';
                    }
                    result.sysUserCityVOS.forEach(function (i, k) {
                        cityOptionStr += '<option value="' + i.cityId + '">' + i.addressName + '</option>';
                    });
                    nodeName.html(cityOptionStr);
                    // if(nodeName.attr('id')=='transferCity'){
                    //     nodeName.find('option').eq(1).attr('selected',true);
                    // }
                }
            })
        },
        // 获取列表搜索行政区域数据
        getTradingData: function (areaName, cbdName, cityId) {
            win.utils.ajaxPost(win.utils.services.getSysAreaResultMap, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                levelFlag: 3,
                cityId: cityId,
            }, function (result) {
                if (result.result_code == 200) {
                    var tradingOptionStr = '<option value="">行政区域</option>';
                    result.data[2].forEach(function (i, k) {
                        tradingOptionStr += '<option value="' + i.id + '">' + i.addressName + '</option>';
                    });
                    areaName.html(tradingOptionStr);

                    layui.use('form', function () {
                        form = layui.form;
                        form.render('select');
                    });
                }
            });
        },
        // 获取列表搜索商圈数据
        getBusinessData: function (nodeName, districtId, cityId, cityName) {
            win.utils.ajaxPost(win.utils.services.getSysArea, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                levelFlag: 3,
                districtId: districtId,
                functionType: 1
            }, function (result) {
                if (result.result_code == 200) {
                    var businessOptionStr = '<option value="">商圈</option>';
                    result.areaVOList.forEach(function (i, k) {
                        businessOptionStr += '<option value="' + i.id + '" data-cityId="' + cityId + '" data-cityName="' + cityName + '">' + i.addressName + '</option>';
                    });
                    nodeName.html(businessOptionStr);
                    layui.use('form', function () {
                        form = layui.form;
                        form.render();
                    });
                }
            })
        },

        // 费用校验
        cost_check: function (costType, costNum) {
            win.utils.ajaxPost(win.utils.services.configMna_costConfig_judgeExistCost, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                costType: costType,
                price: costNum
            }, function (result) {
                if (result.result_code == 6601) {
                    $('.cost-check').show();
                } else {
                    $('.cost-check').hide();
                }
            })
        },

        // 城市数组处理
        getCityArr: function () {
            if (businessArr.length) {
                businessArr.forEach(function (businessArrI, businessArrK) {
                    var businessName = $('#edit_business').find('option[value=' + businessArrI + ']').text();
                    var cityId = $('#edit_business').find('option[value=' + businessArrI + ']').attr('data-cityId');
                    for (var i = 0; i < cityChooseArr.length; i++) {
                        if (cityChooseArr[i].cityId == cityId) {
                            if (JSON.stringify(cityChooseArr[i].business).indexOf(JSON.stringify({
                                businessId: businessArrI,
                                businessName
                            })) === -1) {
                                cityChooseArr[i].business.push({businessId: businessArrI, businessName});
                            }
                        }
                    }
                });
            }
        },
        // 城市数组转dom节点
        arrToDom: function (arr = cityChooseArr) {
            var domArr = [];
            arr.forEach(function (i, k) {
                if (i.business.length) {
                    var businessItemArr = [];
                    i.business.forEach(function (businessI, businessK) {
                        businessItemArr.push('' +
                            '<div>' + businessI.businessName + ' ' +
                            '<img src="../../../images/close.png" data-cityId="' + i.cityId + '" data-index="' + businessK + '">' +
                            '</div>'
                        );
                    });

                    var itemDom = '<div class="area-show-item">\n' +
                        '\t\t\t<div class="tit">\n' +
                        '\t\t\t\t<label class="col-sm-2 control-label">城市：</label>\n' +
                        '\t\t\t\t<span>' + i.cityName + '</span>\n' +
                        '\t\t\t</div>\n' +
                        '\t\t\t<div class="area">\n' +
                        '\t\t\t\t<div class="area-content">' + businessItemArr.join("") + '</div>\n' +
                        '\t\t\t</div>\n' +
                        '\t\t</div>';

                    domArr.push(itemDom);
                }
            });
            $('.area-show-item-box').html(domArr.join(''));
        },

        // 输入框价格格式处理
        priceCheck: function (th, event) {
            var event = event || window.event;
            var code = event.keyCode;
            if (navigator.userAgent.indexOf("Firefox") > -1) {
                code = event.which;
            }
            if (code == 37 || code == 39) return;
            var regStrs = [
                ['^0(\\d+)$', '$1'], //禁止录入整数部分两位以上，但首位为0
                ['[^\\d\\.]+$', ''], //禁止录入任何非数字和点
                ['\\.(\\d?)\\.+', '.$1'], //禁止录入两个以上的点
                ['^(\\d+\\.\\d{2}).+', '$1'], //禁止录入小数点后两位以上
                ['^(\\.\\d+)', '$1']//禁止输入情况下小数点出现在首位
            ];
            for (i = 0; i < regStrs.length; i++) {
                var reg = new RegExp(regStrs[i][0]);
                th.value = th.value.replace(reg, regStrs[i][1]);

            }
        },
        overFormat: function (th) {
            // alert('111')
            var v = th.value;
            console.log(v);
            if (v === '') {
                v = '0.00';
            } else if (v === '0') {
                v = '0.00';
            } else if (v === '0.') {
                v = '0.00';
            } else if (/^0+\d+\.?\d*.*$/.test(v)) {
                v = v.replace(/^0+(\d+\.?\d*).*$/, '$1');
                v = inp.getRightPriceFormat(v).val;
            } else if (/^0\.\d$/.test(v)) {
                v = v + '0';
            } else if (!/^\d+\.\d{2}$/.test(v)) {
                if (/^\d+\.\d{2}.+/.test(v)) {
                    v = v.replace(/^(\d+\.\d{2}).*$/, '$1');
                } else if (/^\d+$/.test(v)) {
                    v = v + '.00';
                } else if (/^\d+\.$/.test(v)) {
                    v = v + '00';
                } else if (/^\d+\.\d$/.test(v)) {
                    v = v + '0';
                } else if (/^[^\d]+\d+\.?\d*$/.test(v)) {
                    v = v.replace(/^[^\d]+(\d+\.?\d*)$/, '$1');
                } else if (/\d+/.test(v)) {
                    v = v.replace(/^[^\d]*(\d+\.?\d*).*$/, '$1');
                    ty = false;
                } else if (/^0+\d+\.?\d*$/.test(v)) {
                    v = v.replace(/^0+(\d+\.?\d*)$/, '$1');
                    ty = false;
                } else {
                    v = '0.00';
                }
            }
            th.value = v;
        }
    };
    win.cost_config = cost_config;
})(window, jQuery);
