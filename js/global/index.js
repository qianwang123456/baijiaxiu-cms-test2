/**
 * Created by yimao on 2016-06-24.
 */
(function (win, $) {

    function initializeEditForm() {
        $('#txtOldPassword').val('');
        $('#txtNewPassword').val('');
        $('#txtNewPasswordAgain').val('');
        $('#labPasswordError').html('');
    }

    var index = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            obj.initMenus();

            $('#txtOldPassword,#txtNewPassword,#txtNewPasswordAgain').bind('input propertychange', function () {
                $('#labPasswordError').html('');
            });

            $('#labUserID').html(utils.getCookie('user_id'));
            $('#labUserName').html(utils.getCookie('user_name'));

            //initialize slimScroll
            $(".sidebar-container").slimScroll({
                height: "100%",
                railOpacity: .4,
                wheelStep: 10
            }), $(".small-chat-box .content").slimScroll({
                height: "234px",
                railOpacity: .4
            }), $(function () {
                $(".sidebar-collapse").slimScroll({
                    height: "100%",
                    railOpacity: .9,
                    alwaysVisible: !1
                })
            }), $(".full-height-scroll").slimScroll({
                height: "100%"
            });
        },
        initMenus: function () {
            win.utils.ajax(win.utils.services.user_right_menus, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session')
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.menus == undefined || result.menus == null || result.menus.length == 0) {

                    } else {
                        //initialize menus
                        index.fillMenus(result.menus);
                        $('#side-menu').metisMenu({toggle: false});
                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        fillMenus: function (menus) {
            var $rootUL = $('#side-menu');
            var childCount = 8;
            for (var i = 0; i < menus.length; i++) {
                var item = menus[i];

                // 一级菜单
                if (item.code.length == 2) {
                    var $li_level_one = $("<li></li>").appendTo($rootUL);
                    $("<a href='#'><i class='" + item.icon + "'></i><span class='nav-label'>" + item.name + "</span><span class='fa arrow'></span></a>").appendTo($li_level_one);
                    var $li_level_one_ul = $("<ul class='nav nav-second-level'></ul>").appendTo($li_level_one);
                }
                // 二级菜单
                else {
                    childCount = childCount + 1;
                    var $li_level_two = $("<li></li>").appendTo($li_level_one_ul);
                    $("<a class='J_menuItem' href='" + item.url + "' data-index=" + childCount + "  data-id=" + item.id + ">" + item.name + "</a>").appendTo($li_level_two);
                }
            }
        },
        openNewTab: function (url, caption) {
            var index = Math.round(Math.random() * 100000);
            var p = '<a href="javascript:;" class="active J_menuTab" data-id="' + url + '">' + caption + ' <i class="fa fa-times-circle"></i></a>';
            $(".J_menuTab").removeClass("active");
            var n = '<iframe class="J_iframe" name="iframe' + index + '" width="100%" height="100%" src="' + url + '" frameborder="0" data-id="' + url + '" seamless></iframe>';
            $(".J_mainContent").find("iframe.J_iframe").hide().parents(".J_mainContent").append(n);
            $(".J_menuTabs .page-tabs-content").append(p);
        },
        loginOut: function () {
            utils.deleteCookie('user_id');
            utils.deleteCookie('session');
            window.location.href = window.utils.domain;
        },
        updateUserPassword: function () {
            initializeEditForm();
            var i = layer.open({
                type: 1,
                area: '480px',
                title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;重置密码',
                content: $('#pop_password'),
                btn: ['保存', '取消'],
                yes: function (index, layero) {
                    var oldPassword = $('#txtOldPassword').val();
                    var newPassword = $('#txtNewPassword').val();
                    var newPasswordAgain = $('#txtNewPasswordAgain').val();

                    if (oldPassword == '') {
                        $('#labPasswordError').html('旧密码不能为空！');
                        $('#txtOldPassword').focus();
                        return;
                    }
                    if (newPassword == '') {
                        $('#labPasswordError').html('新密码不能为空！');
                        $('#txtNewPassword').focus();
                        return;
                    }
                    if (newPasswordAgain == '') {
                        $('#labPasswordError').html('新密码验证不能为空！');
                        $('#txtNewPasswordAgain').focus();
                        return;
                    }
                    if (newPassword != newPasswordAgain) {
                        $('#labPasswordError').html('两次输入的密码不一致！');
                        $('#txtNewPassword').focus();
                        return;
                    }

                    win.utils.ajax(win.utils.services.password, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        old_password: oldPassword,
                        new_password: newPassword
                    }, function (result) {
                        if (result.result_code == 200) {
                            layer.close(i);
                            utils.showMessage('重置密码成功！');
                        } else {
                            $('#labPasswordError').html('重置密码失败，请重试！');
                        }
                    });
                }
            });
        },


        // created by hanlu  2019-05-28
        // 维修员列表跳转至维修员区域管理列表，触发initList事件
        getParamsToChild: function (obj) {
            if (obj) {
                // 获取iframe
                var iframeNodes = document.querySelector("iframe[src='web/areaManage/area_list.html']");
                if (iframeNodes.contentWindow.area_list) {
                    iframeNodes.contentWindow.area_list.getParamsData();
                }
            }
        },
        backToRepairmanList: function (obj) {
            if (obj) {
                // 获取iframe
                var iframeNodes = document.querySelector("iframe[src='web/repairmanManage/repairman_list.html']");
                if (iframeNodes.contentWindow.repairman) {
                    iframeNodes.contentWindow.repairman.leaveOk(obj);
                }
            }
        },
        backToSpecialMaintainer: function (obj) {
            if (obj) {
                // 获取iframe
                var iframeNodes = document.querySelector("iframe[src='web/repairmanManage/special_maintainer_list.html']");
                if (iframeNodes.contentWindow.repairman) {
                    iframeNodes.contentWindow.repairman.leaveOk(obj);
                }
            }
        }
    };

    win.index = index;
})(window, jQuery);