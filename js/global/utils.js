(function () {
	var globalhtp='',
		dankeGlobalhtp='';

	// globalhtp=' http://api.baijiaxiu.com/platform';//正式
	globalhtp='http://39.98.227.62:8110/platform';//阿里云---safe前
	// globalhtp='http://127.0.0.1:8110/platform';//阿里云---safe后
	// globalhtp='http://10.10.33.6:8110/platform';//风雷
	// globalhtp='http://10.10.32.173:8110/platform';//刘毓峰---无线
    // globalhtp='http://192.168.2.5:8110/platform';//刘毓峰---有线
	// globalhtp='http://192.168.2.25:8110/platform';//邵俊峰---有线
	// globalhtp='http://10.10.32.65:8110/platform';//邵俊峰---无线
    // globalhtp='http://10.10.32.238:8110/platform';//东伟---无线
    // globalhtp='http://10.10.33.9:8110/platform';//彭想---无线
	// globalhtp='http://192.168.2.41:8110/platform';//彭想---有线


	// BI项目接口地址
	// BIglobalhtp='http://47.92.194.222:8291/bi'
	BIglobalhtp='http://39.98.227.62:8291/bi'//---safe前
	// BIglobalhtp='http://127.0.0.1:8291/bi'//--safe后
	// BIglobalhtp='http://192.168.2.14:8291/bi'
	// BIglobalhtp='http://10.10.33.9:8291/bi'//--彭想---无线
	// BIglobalhtp='http://192.168.2.41:8291/bi'//--彭想---有线


	// dankeGlobalhtp='http://192.168.2.4:8112/danke';//刘毓峰,
	dankeGlobalhtp='http://39.98.227.62:8112/danke'; //测试---safe前
	// dankeGlobalhtp='http://127.0.0.1:8112/danke'; //测试--safe后
	// dankeGlobalhtp='http://10.10.32.61:8112/danke';//刘毓峰,
	// dankeGlobalhtp='http://39.98.227.62:8112/danke'; //测试
	// dankeGlobalhtp='http://api.baijiaxiu.com/danke';//正式,


    var utils = {
        services: {
            'login': globalhtp+'/login',
            'user_right_menus': globalhtp+'/user/right/menus',
            'password': globalhtp+'/password',
            'role_right_tree': globalhtp+'/manage/roleright/tree',
            'role_right_get': globalhtp+'/manage/roleright/get',
            'role_right_save': globalhtp+'/manage/roleright/save',
            'user_role_tree': globalhtp+'/manage/userrole/tree',
            'user_role_get': globalhtp+'/manage/userrole/get',
            'user_role_save': globalhtp+'/manage/userrole/save',

			'role_city_tree': globalhtp+'/manage/rolecity/tree',//获取系统角色
			'role_city_get': globalhtp+'/manage/rolecity/get',//获取城市
			'role_city_save': globalhtp+'/manage/rolecity/save',//保存分派结果

            'user_list': globalhtp+'/manage/user/list',
            'user_detail': globalhtp+'/manage/user/detail',
            'user_save': globalhtp+'/manage/user/save',
            'user_update': globalhtp+'/manage/user/update',
            'user_update_password': globalhtp+'/manage/user/password',
            'user_delete': globalhtp+'/manage/user/delete',
            'role_list': globalhtp+'/manage/role/list',
            'role_detail': globalhtp+'/manage/role/detail',
            'role_save': globalhtp+'/manage/role/save',
            'role_update': globalhtp+'/manage/role/update',
            'role_delete': globalhtp+'/manage/role/delete',
            'user_right_menuindex': globalhtp+'/user/right/menuindex',
            'menuindex_consult': globalhtp+'/manage/menu/consult',
            'user_log': globalhtp+'/manage/log/list',
            'sysparams_list': globalhtp+'/basic/sysparams/list',
            'sysparams_get': globalhtp+'/basic/sysparams/get',
            'sysparams_update': globalhtp+'/basic/sysparams/update',
            'acbsp_file_upload': globalhtp+'/acbsp/file/upload',
            'acbsp_docs_list': globalhtp+'/acbsp/docs/list',
            'acbsp_docs_detail': globalhtp+'/acbsp/docs/detail',
            'acbsp_docs_save': globalhtp+'/acbsp/docs/save',
            'acbsp_docs_update': globalhtp+'/acbsp/docs/update',
            'acbsp_docs_delete': globalhtp+'/acbsp/docs/delete',
            'acbsp_lessons_list': globalhtp+'/acbsp/lessons/list',
            'acbsp_lessons_detail': globalhtp+'/acbsp/lessons/detail',
            'acbsp_lessons_save': globalhtp+'/acbsp/lessons/save',
            'acbsp_lessons_update': globalhtp+'/acbsp/lessons/update',
            'acbsp_lessons_delete': globalhtp+'/acbsp/lessons/delete',
            'acbsp_lessons_update_status': globalhtp+'/acbsp/lessons/update/status',
            'acbsp_newmain_list': globalhtp+'/acbsp/newmain/list',
            'acbsp_newmain_detail': globalhtp+'/acbsp/newmain/detail',
            'acbsp_newmain_save': globalhtp+'/acbsp/newmain/save',
            'acbsp_newmain_update': globalhtp+'/acbsp/newmain/update',
            'acbsp_newmain_delete': globalhtp+'/acbsp/newmain/delete',
            'acbsp_newmain_update_status': globalhtp+'/acbsp/newmain/update/status',
            'acbsp_newsub_list': globalhtp+'/acbsp/newsub/list',
            'acbsp_newsub_detail': globalhtp+'/acbsp/newsub/detail',
            'acbsp_newsub_save': globalhtp+'/acbsp/newsub/save',
            'acbsp_newsub_update': globalhtp+'/acbsp/newsub/update',
            'acbsp_newsub_delete': globalhtp+'/acbsp/newsub/delete',
			// 配置管理
			'configMan_firstClassWorkOrder_list': globalhtp+'/configuration/getfirstItem',//工单list
			'configMan_WorkOrder_add': globalhtp+'/configuration/addServiceItem',//工单add
			'configMan_WorkOrder_update': globalhtp+'/configuration/updateServiceItem',//工单update
			'configMan_maintenSchClasify_list': globalhtp+'/configuration/getServiceScheme',//维修方案-list
			'configMan_maintenSchClasify_add': globalhtp+'/configuration/addServiceScheme',//维修方案-add
			'configMan_maintenSchClasify_del': globalhtp+'/configuration/deleteServiceScheme',//维修方案-delete-配置管理
			'configMan_maintenSchClasify_update': globalhtp+'/configuration/updateServiceScheme',//维修方案-update
			'configMan_skillClasify_list': globalhtp+'/configuration/getSkill',//技能分类-list
			'configMan_skillClasify_add': globalhtp+'/configuration/addSkill',//技能分类-add
			'configMan_skillClasify_update': globalhtp+'/configuration/updateSkill',//技能分类-update
			'configMan_skillClasify_delete': globalhtp+'/configuration/deleteSkill',//技能分类-delete
			'configMan_maintenOrderReason_list': globalhtp+'/configuration/getReasons',//退单撤单原因-list
			'configMan_maintenOrderReason_add': globalhtp+'/configuration/addReasons',//退单撤单-add
			'configMan_maintenOrderReason_update': globalhtp+'/configuration/updateReasons',//退单撤单原因-update
			'configMan_maintenOrderReason_delete': globalhtp+'/configuration/deleteReasons',//退单撤单原因-delete
			'configMan_reservationTime_list': globalhtp+'/configuration/getReservationTime',//预约时间段-list
			'configMan_reservationTime_add': globalhtp+'/configuration/addReservationTime',//预约时间段-add
			'configMan_reservationTime_update': globalhtp+'/configuration/updateReservationTime',//预约时间段-update
			'configMan_reservationTime_delete': globalhtp+'/configuration/deleteReservationTime',//预约时间段-delete
			'configMan_costConfig_list': globalhtp + '/configuration/costConfigureList', //获取费用配置列表
			'configMan_costConfig_add': globalhtp + '/configuration/addCostConfigure', //新增配置费用
			'configMan_costConfig_detail': globalhtp + '/configuration/detailsCostConfigure',//配置费用详情接口
			'configMan_costConfig_update': globalhtp + '/configuration/editCostConfigure', //编辑配置费用
			'configMan_costConfig_delete': globalhtp + '/configuration/deleteCostConfigure', //删除配置费用
			'configMna_costConfig_judgeExistCost':globalhtp + '/configuration/judgeExistCost', //费用配置校验
			'configMan_ConfigServiceItems_list': globalhtp+'/configuration/getConfigServiceItems',//查询归属原因
			'orderMan_order_list': globalhtp+'/order/queryOrderList',//订单管理-list
			'orderMan_order_todo_list': globalhtp+'/order/queryOrderOwnership',//订单管理-list-分配与协调
			'orderMan_order_detail': globalhtp+'/order/queryOrderByOrderId',//订单管理-工单详情
			'orderMan_order_signin': globalhtp+'/order/signIn',//订单管理-上门
			'orderMan_order_assignWorker': globalhtp+'/order/assignWorker',//订单管理-分配师傅接口
			'orderMan_order_redeployWorker': globalhtp+'/order/redeployWorker',//订单管理转派师傅接口
			'orderMan_order_callUser': globalhtp+'/order/callUser',//订单管理-师傅联系结果接口
			'orderMan_order_changeTime': globalhtp+'/order/changeTime',//订单管理-上门改期
			'getWorker': globalhtp + '/workerInfo/getWorker',//订单管理-查询维修员
			'getTransferProxy': globalhtp + '/workerInfo/getTransferProxy',//订单管理-查询代理执行人
			'orderMan_order_updateExecutor': globalhtp + '/orderPlan/updateExecutor',//订单管理-转换防水师傅
			'orderMan_order_createMaintainPlan': globalhtp+'/orderPlan/createMaintainPlan',//订单管理-创建维修方案
			'orderMan_order_queryMaintainPlan': globalhtp+'/orderPlan/queryMaintainPlan',//订单管理-查看维修方案
			'orderMan_order_queryCoordinationRecord': globalhtp+'/orderPlan/queryCoordinationRecord',//订单管理-查询维修方案回复记录
			'orderMan_order_createCoordinationRecord': globalhtp+'/orderPlan/createCoordinationRecord',//订单管理-创建维修方案回复
			'orderMan_order_updateMaintainPlan': globalhtp+'/orderPlan/updateMaintainPlan',//订单管理-审批维修方案/更新维修方案
			'orderMan_order_queryOrderAccount': globalhtp+'/orderPlan/queryOrderAccount',//订单管理-查询结算方案-
			'orderMan_order_queryOrderCloseLabel': globalhtp+'/orderPlan/queryOrderCloseLabel',//订单管理-查询结算方案-工单结算标签
			'orderMan_order_queryWorkerFeedbackInfo': globalhtp+'/orderPlan/queryWorkerFeedbackInfo',//订单管理-查询结算方案-维修员反馈详情
			'orderMan_order_queryNinetyReWorkRecord': globalhtp+'/orderPlan/queryNinetyReWorkRecord',//订单管理-查询结算方案-90天内维修记录
			'orderMan_order_updateAccountPlan': globalhtp+'/orderPlan/updateAccountPlan',//订单管理-审核结算方案-
			'orderMan_order_createLinkOrder': globalhtp+'/order/createLinkOrder',//订单管理-发起报修单
			'orderMan_order_createRentOrder': globalhtp+'/order/createRentOrder',//订单管理-创建租务工单
			'orderMan_order_queryOrderTema': globalhtp+'/order/rentServiceOrderDetail',//订单管理-获取租务工单
			'orderMan_order_forwardSupplier': globalhtp+'/order/forwardSupplier',//订单管理-转派供应商
			'orderMan_order_queryCallHistory': globalhtp+'/order/queryCallHistory',//订单管理-获取通话记录
			'orderMan_order_retreatOrder': globalhtp+'/order/retreatOrder',//订单管理-退单，撤单
			'orderMan_order_queryOrderById': globalhtp+'/order/queryOrderById',//订单管理-退单，撤单(获取退单，撤单详情)
			'orderMan_order_followByOrder': globalhtp+'/order/followByOrder',//订单管理-跟进记录
			'orderMan_order_queryOrderRecord': globalhtp+'/order/queryOrderRecord',//订单管理-跟进记录--详情
			'orderMan_order_queryOrderFollowHis': globalhtp+'/order/queryOrderFollowHis',//订单管理-查询操作日志记录--详情
			'orderMan_order_queryOrderChangeHis': globalhtp+'/order/queryOrderChangeHis',//订单管理-改期记录--详情
			'orderMan_order_applyForAuth': globalhtp+'/order/applyForAuth',//订单管理-申请门密/门锁授权-btn
			'orderMan_order_queryAuthHis': globalhtp+'/order/queryAuthHis',//订单管理-申请门密/门锁授权list-详情
			'orderMan_order_queryUserCity': globalhtp+'/sysUserCity/queryUserCity',//用户登陆后，获取管理城市id
			'orderMan_order_queryTabStatistics': globalhtp+'/order/queryTabStatistics',//订单管理---tab获取数据
			'region_query_list': globalhtp+'/sysArea/getSysArea',//区域查询
			'regionMap_query_list': globalhtp+'/sysArea/getSysAreaResultMap',//区域查询-map
			'region_queryParts': globalhtp+'/partsDepot/queryParts',//查询配件库---创建维修方案
			'region_queryPartsType': globalhtp+'/partsDepot/queryPartsType',//查询配件库-类型---创建维修方案
			'orderMan_order_queryRentClassify': globalhtp+'/configuration/queryRentClassify',//创建租务工单-获取一二级类型
			'orderMan_order_orderRookery': globalhtp+'/order/orderRookery',//订单列表---租务工单返回状态查询
			'orderMan_order_getChangeReason': globalhtp+'/orderLinked/getChangeReason',//订单管理---获取改期原因
			'orderMan_order_getPoliceTag': globalhtp+'/orderLinked/getPoliceTag',//订单管理--查询报警标签
			'orderMan_order_getConfigList': globalhtp+'/order/getConfigList',//订单管理--查询配置清单
			'orderMan_order_getProvider': globalhtp+'/configuration/getProvider',//订单管理--查询配置清单
			'orderMan_order_getCostConfigures': globalhtp+'/configuration/getCostConfigures',//订单管理--查询配置费用-审核结算方案
			'orderMan_order_getCostConfigCj': globalhtp+'/configuration/getCostList',//订单管理--查询配置清单-创建维修方案
			'orderMan_order_judgeFee': globalhtp+'/orderPlan/checkCost',//订单管理--费用校验
			'orderMan_order_queryNewestMPlan': globalhtp+'/orderPlan/queryNewestMPlan',//订单管理--查询最新维修方案
			'orderMan_order_createSubOrderTask': globalhtp+'/order/createSubOrderTask',//订单管理--一键转办
			'orderMan_config_addFeedbackTem': globalhtp+'/feedbackTemplate/addFeedbackTemplate',//配置管理-维修方案模板-创建反馈模板
			'orderMan_config_listFeedbackTem': globalhtp+'/feedbackTemplate/listFeedbackTemplate',//配置管理-维修方案模板-查询反馈模板
			'orderMan_config_updateFeedbackTem': globalhtp+'/feedbackTemplate/updateFeedbackTemplate',//配置管理-维修方案模板-修改反馈模板
			'orderMan_config_delFeedbackTem': globalhtp+'/feedbackTemplate/deleteFeedbackTemplate',//配置管理-维修方案模板-删除反馈模板
			'orderMan_config_getFeedbackTem': globalhtp+'/feedbackTemplate/getFeedbackTemplate',//配置管理-维修方案模板-根据id获取单个反馈模板
			'partMan_queryParts': globalhtp+'/partsDepot/queryOrderPlanParts',//查询报备配件===配件库
			'role_level_per': globalhtp+'/orderRoleSche/listRoleNames',//系统管理---角色等级权限---角色
			'role_level_per_list': globalhtp+'/orderRoleSche/listOrderRoleSort',//系统管理---角色等级权限---列表// 展示用户等级信息(排序)
			'role_level_per_update': globalhtp+'/orderRoleSche/batchUpdateOrderRole',//系统管理---角色等级权限---修改列表// 修改用户等级信息
			'sche_get_time': globalhtp+'/orderRoleSche/getScheTime',//人事管理---派单时间---获取
			'sche_set_time': globalhtp+'/orderRoleSche/schTimeSet',//人事管理---派单时间---设置
			'sche_get_user_list': globalhtp+'/orderRoleSche/listOrderScheVOS',//人事管理---排班用户列表，用户信息---获取list
			'sche_get_user_update': globalhtp+'/orderRoleSche/batchUpdateSche',//人事管理---排班用户选择保存---设置与启动
			'kpiList': BIglobalhtp+'/operation-KPI/findAssessList',//统计管理---KPI报表  /operation-KPI/findAssessExcle
			'kpiListExport': BIglobalhtp+'/operation-KPI/findAssessExcle',//统计管理---报表数据导出  /operation-KPI/findAssessExcle、
			'getConfigReasonPlanList': globalhtp+'/configReasonPlan/getConfigReasonPlan',//统计管理---原因归属维修方案
			'getConfigReasonPlanDetail': globalhtp+'/configReasonPlan/getReasonPlan',//统计管理---通过id获取原因归属维修方案详情
			'editConfigReasonPlanDetail': globalhtp+'/configReasonPlan/addOrUpdateReasonPlan',//统计管理---通过id编辑原因归属维修方案详情
			'querySuiteArea': globalhtp+'/basicsInfo/getSuiteArea',//创建维修方案，审核结算方案---通过id编查询维修区域

            /** 自动结算开始 **/
            //查询自动结算列表
            'getAutoAccountRuleList':globalhtp+'/configAutoAccountRule/getAutoAccountRuleList',
            // 保存自动结算规则
            'saveAutoAccountRule':globalhtp+'/configAutoAccountRule/saveAutoAccountRule',
            // 查询单个结算规则  修改 effectiveStatus=2， 当前状态 effectiveStatus=1
            'getAutoAccountRule':globalhtp+'/configAutoAccountRule/getAutoAccountRule',
            // 修改自动结算规则
            'updateAutoAccountRule':globalhtp+'/configAutoAccountRule/updateAutoAccountRule',
            // 查询自动结算规则操作日志
            'getAutoAccountRuleLog':globalhtp+'/configAutoAccountRule/getAutoAccountRuleLog',
            // 自动结算规则启用禁用接口
            'updateRuleStatus':globalhtp+'/configAutoAccountRule/updateRuleStatus',
			// 自动结算获取配件--去除已选择配件
			'autoClear_queryParts':globalhtp+'/configAutoAccountRule/queryParts',



			// 统计  用户评价列表
			// 'get_evaluate': globalhtp + '/clientEvaluate/getEvaluate',
			'getEvaluate': globalhtp + '/clientEvaluate/getEvaluate',
			// 查询用户评价表头
			'queryHeader': globalhtp + '/clientEvaluate/queryHeader',
			// 用户评价筛选条件
			'queryOption': globalhtp + '/clientEvaluate/queryOption',
			// 查询满意度
			'querySatisficing':globalhtp + '/clientEvaluate/querySatisficing',
			// BI报表
			'findReportList': BIglobalhtp +'/report-query/findReportList',
			// BI报表导出
			'exportReport': BIglobalhtp + '/report-export/exportReport',
			// 订单导出列表
			'findDataList':BIglobalhtp+'/data-export-bi/findDataList',
			// 订单导出接口
			'exportData':BIglobalhtp+'/data-export-bi/exportData',
			// 查询配件库信息
			'queryParts': globalhtp + '/partsDepot/queryParts',
			// 查询配件类型
			'queryPartsType': globalhtp + '/partsDepot/queryPartsType',
			// 增加配件类型
			'addPartsType':globalhtp + '/partsDepot/addPartsType',
			// 修改配件类型
			'updatePartsType':globalhtp + '/partsDepot/updatePartsType',
			// 删除配件类型
			'deletePartsType':globalhtp + '/partsDepot/deletePartsType',
			// 导出配件类型列表
			'exportPartsTypeList':globalhtp + '/partsDepot/exportPartsTypeList',
			// 配件库导出
			'exportPartsList': globalhtp+'/partsDepot/exportPartsList',
			// 配件库导入
			'importPartsList': globalhtp + '/partsDepot/importPartsList',
			// 创建配件信息
			'addParts': globalhtp + '/partsDepot/addParts',
			// 修改配件信息
			'updateParts': globalhtp + '/partsDepot/updateParts',
			// 删除配件信息
			'deleteParts': globalhtp + '/partsDepot/deleteParts',
			// 查询配件单位
			'queryPartsUnit': globalhtp + '/partsDepot/queryPartsUnit',
			// 查询公告
			'getNoticeInfo': globalhtp + '/noticeInfo/getNoticeInfo',
			// 增加公告
			'addNoticeInfo': globalhtp + '/noticeInfo/addNoticeInfo',
			// 修改公告
			'updateNoticeInfo': globalhtp + '/noticeInfo/updateNoticeInfo',
			// 删除公告
			'deleteNoticeInfo': globalhtp + '/noticeInfo/deleteNoticeInfo',
			// 维修员管理维修员申请入驻列表
			'getEnterApply': globalhtp + '/enterApply/getEnterApply',
			// 维修员管理维修员入驻申请---修改申请状态
			'updateEnterApply': globalhtp + '/enterApply/updateEnterApply',
			// 维修员反馈
			'getIssueFeedBack': globalhtp + '/issueFeedBack/getIssueFeedBack',
			// 新增维修员
			'addWorker': globalhtp + '/workerInfo/addWorker',
			// 查询维修员列表
			'getWorker': globalhtp + '/workerInfo/getWorker',
			// 维修员导出
			'exportWorkerInfoList':globalhtp+'/workerInfo/exportWorkerInfoList',
			// 修改维修员
			'updateWorker': globalhtp + '/workerInfo/updateWorker',
			// 删除维修员银行卡接口
			'deleteBankInfo':globalhtp+'/workerInfo/deleteBankInfo',
			// 获取维修员银行卡信息
			'queryBankInfo':globalhtp+'/workerInfo/queryBankInfo',
			// 校验维修员身份证号
			'verifyCardId':globalhtp+'/workerInfo/verifyCardId',
            // 校验银行卡
            'verifyBankId':globalhtp+'/workerInfo/verifyBankId',
			// 维修员请假记录
			'getWorkerLeaveList': globalhtp + '/workerLeave/getWorkerLeaveList',
			// 查询操作记录
			'queryWorkerLog': globalhtp + '/workerInfo/queryWorkerLog',

			// 获取维修员上门签到位置
			'getSysArea': globalhtp + '/sysArea/getSysArea',
			// 维修员编辑维修区域查询
			'queryEditWorkerArea': globalhtp + '/workerArea/queryEditWorkerArea',
			// 搜索维修员管辖区域
			'queryWorkerArea': globalhtp + '/workerArea/queryWorkerArea',
			// 区域API
			'getWorkerLocation': globalhtp + '/workerInfo/getPresentSite',
			// 区域API 排序 新
			'getSysAreaSort': globalhtp + '/sysArea/getSysAreaSort',
			// 区域API，返回商圈数据
			'getSysAreaResultMap': globalhtp + '/sysArea/getSysAreaResultMap',
			// 通过搜索添加小区
			'queryWorkerAddArea': globalhtp + '/workerArea/queryWorkerAddArea',
			// 维修员增加区域提交
			'updateWorkerArea': globalhtp + '/workerArea/updateWorkerArea',
			// 修改维修员接单状态——获取技能区域
			'queryWorkerBlockArea': globalhtp + '/workerArea/queryWorkerBlockArea',
			// 维修员区域管理列表数据
			'getAreaWorkerList': globalhtp + '/workerArea/getAreaWorkerList',
			// 查询代理执行人
			'getTransferProxy': globalhtp + '/workerInfo/getTransferProxy',
			// 转派代理执行人
			'transferProxy': globalhtp + '/workerArea/transferProxy',
			// 永久转派区域
			'transferDistrict': globalhtp + '/workerArea/transferDistrict',
			// 获取操作员
			'queryUser': globalhtp + '/sysUserController/queryUser',
			// 根据维修员id查询需要请假的维修员信息
			'queryLeaveInfo': globalhtp + '/workerLeave/queryLeaveInfo',
			// 维修员请假——添加备用师傅
			'addBackWorker': globalhtp + '/workerArea/addBackWorker',
			// 维修员请假——修改维修员状态
			'addLeaveInfo': globalhtp + '/workerLeave/addLeaveInfo',
			// 维修员销假
			'addCancelLeaveInfo': globalhtp + '/workerLeave/addCancelLeaveInfo',
			// 校验维修员手机号
			'verifyPhone' : globalhtp + '/workerInfo/verifyPhone',
			// 调度员城市对应列表
			'getUserCityList': globalhtp + '/sysUserCity/getUserCityList ',
			// 单个调度员负责城市
			'queryUserCity': globalhtp + '/sysUserCity/queryUserCity',
			// 添加系统用户对应城市权限
			'updateUserCity':globalhtp + '/sysUserCity/updateUserCity',
			// 获取七牛云临时token
			'getQiniuToken': globalhtp + '/testToken',


			// 维修员列表----自动分配开关
			// 获取开关状态
			'queryAutoAssignWorkerSwitch':globalhtp+'/taskSwitch/queryAutoAssignWorkerSwitch',
			// 修改开关状态
			'updateAutoAssignWorkerSwitch': globalhtp+'/taskSwitch/updateAutoAssignWorkerSwitch',
		},

		/** 同步蛋壳数据接口开始 **/
		getDanKe: {
			'syncBlockInfo': dankeGlobalhtp + '/sync/syncBlockInfo', //获取小区信息
			'syncReason': dankeGlobalhtp + '/sync/syncReason', //获取撤单、退单原因
			'syncDankeAreas' : dankeGlobalhtp + '/sync/syncDankeAreas', //获取行政区商圈小区
			'syncServiceItem' : dankeGlobalhtp + '/sync/syncServiceItem', //获取维修一二三级项目
			'syncPlanService': dankeGlobalhtp + '/sync/syncPlanService', //获取维修协调方案
			'syncRentClassify': dankeGlobalhtp + '/sync/syncRentClassify', //获取租务一二级分类
			'syncEvaluateType': dankeGlobalhtp + '/sync/syncEvaluateType', //获取评价类型
			'syncAddHouseNum': dankeGlobalhtp + '/sync/syncAddHouseNum', //获取小区对应的房间数量（增量）
			'syncInitHouseNum': dankeGlobalhtp + '/sync/syncInitHouseNum', //获取小区对应的房间数量（初始化）
			'reviseWorkerAreaInfo': dankeGlobalhtp + '/sync/reviseWorkerAreaInfo', //修正维修员对应区域基础信息
			'syncEvaluate' : dankeGlobalhtp + '/sync/syncEvaluate', //获取订单评价数据
		},
		/** 同步蛋壳数据接口结束 **/


       // domain: 'http://192.168.2.16:8080/baijiaxiu-cms/login.html',//有线
       // domain: 'http://127.0.0.1/login.html', //无线
		// domain: 'http://127.0.0.1:8848/baijiaxiu-cms/login.html',
		// domain: 'http://192.168.2.10:8848/baijiaxiu-cms/login.html',//有线-song
		// domain: 'http://10.10.32.219:8848/baijiaxiu-cms/login.html',//本地-song-company
		// domain: 'http://192.168.1.7:8848/baijiaxiu-cms/login.html',//本地-song-home
		// domain: 'http://192.168.43.195:8848/baijiaxiu-cms/login.html',//本地-song
        // domain: 'http://order.baijiaxiu.com/login.html', //正式
		// domain: 'http://localhost:63342/百家修—admin/baijiaxiu-cms/login.html',
        domain: 'http://39.98.227.62:8091/baijiaxiu-cms/login.html', //阿里云

        ajaxPost: function (url, data, success) {
        		//统一处理session的问题
        		var lastIndex = url.lastIndexOf("/") + 1;
        		var keyLogin = url.substring(lastIndex);
        		if(keyLogin != 'login'){
        			if(utils.isLogin() == false){
					utils.loginDomain();
						return;
					}
        		}
			var loadingLayerIndex;
			if(url.indexOf('/order/orderRookery')>-1){
				utils.closeLayerLoading(loadingLayerIndex);
			}else{
				loadingLayerIndex = utils.showLayerLoading('正在处理数据，请稍候...');
			};
            var num = Math.round(Math.random() * 10000);
            $.ajax({
                url: url,
                data: JSON.stringify(data),
                async: true,
                type: "POST",
                dataType: 'json',
				contentType:"application/json",
                timeout: 300000,
                crossDomain: true,
                cache: true,
                success: function (result) {
					utils.closeLayerLoading(loadingLayerIndex);
					if (result.result_code === 911){
						window.location.href = window.utils.domain + "?error=911";
					}
					else{
						success && success(result);
					}
                },
                error: function (request, textStatus, errorThrown) {
					utils.closeLayerLoading(loadingLayerIndex);
					utils.showMessage('您的网络不稳定，请稍后重试！');
                }
            });
        },
        ajaxGet: function (url, data, success) {
        		//统一处理session的问题
        		var lastIndex = url.lastIndexOf("/") + 1;
        		var keyLogin = url.substring(lastIndex);
        		if(keyLogin != 'login'){
        			if(utils.isLogin() == false){
					utils.loginDomain();
					return;
				}
        		}

		    var loadingLayerIndex=null;
			if(!url.match('order/queryTabStatistics')){
				loadingLayerIndex = utils.showLayerLoading('正在处理数据，请稍候...');
			};
            var num = Math.round(Math.random() * 10000);
            $.ajax({
                url: url,
                data: data,
                async: true,
                type: "get",
                dataType: 'json',
				contentType:"application/json",
                timeout: 300000,
                crossDomain: true,
                cache: true,
                success: function (result) {
                		utils.closeLayerLoading(loadingLayerIndex);
                		if (result.result_code === 911){
                			// alert('911，登录已过期，请重新登录！');
                			window.location.href = window.utils.domain + "?error=911";
                		}
                		else{
                			success && success(result);
                		}
                },
                error: function (request, textStatus, errorThrown) {
                		utils.closeLayerLoading(loadingLayerIndex);
                		utils.showMessage('您的网络不稳定，请稍后重试！');
                }
            });
        },
		ajax: function (url, data, success) {
        		//统一处理session的问题
        		var lastIndex = url.lastIndexOf("/") + 1;
        		var keyLogin = url.substring(lastIndex);
        		if(keyLogin != 'login'){
        			if(utils.isLogin() == false){
					utils.loginDomain();
					return;
				}
        		}

		    var loadingLayerIndex = utils.showLayerLoading('正在处理数据，请稍候...');
            var num = Math.round(Math.random() * 10000);
            $.ajax({
                url: url,
                data: data,
                async: true,
                type: "POST",
                dataType: 'json',
                // jsonpCallback: 'xingbao_callback' + num.toString(),
                timeout: 300000,
                crossDomain: true,
                cache: true,
                success: function (result) {
                		utils.closeLayerLoading(loadingLayerIndex);
                		if (result.result_code === 911){
                			// alert('911，登录已过期，请重新登录！');
                			window.location.href = window.utils.domain + "?error=911";
                		}
                		else{
                			success && success(result);
                		}
                },
                error: function (request, textStatus, errorThrown) {
                		utils.closeLayerLoading(loadingLayerIndex);
                		utils.showMessage('您的网络不稳定，请稍后重试！');
                }
            });
        },
        export: function (url, data) {
        		//统一处理session的问题
        		var lastIndex = url.lastIndexOf("/") + 1;
        		var keyLogin = url.substring(lastIndex);
        		if(keyLogin != 'login'){
        			if(utils.isLogin() == false){
					utils.loginDomain();
					return;
				}
        		}
        		url = url + '?';
            for (key in data) {
            		url = url + key + '=' + data[key] + '&';
            }
            url = url.substring(0, url.length-1);
            self.location = url;
        },
        isLogin: function () {
            var session = utils.getCookie('session');
            if (session == null || session == 'null') {
                return false;
            }
            return true;
        },
        loginDomain: function () {
            var session = utils.getCookie('session');
            if (session == null || session == 'null') {
            		// alert('999，登录已过期，请重新登录！');
                	window.location.href = window.utils.domain;
            }
        },
        isUserMenuIndexRight: function (menuIndex,success) {
        		var user_code = window.utils.getCookie('user_id');
        		var session = window.utils.getCookie('session');
        		if (session == null || session == '') {
                window.location.href = window.utils.domain;
           	}

            window.utils.ajax(window.utils.services.user_right_menuindex,
            		{
            			user_id: user_code,
            			session: session,
            			menu_index: menuIndex
            		},
            		function (result) {
                		if (result.result_code == '200') {
                    		if(result.right_count > 0){
                    			success && success(1);
                    		}
                    		else{
                    			success && success(0);
                    		}
                		}
                		else{
                			success && success(0);
                		}
            		}
            );
        },
        initMenuIndexes:function (obj,fillAll) {
            window.utils.ajax(window.utils.services.menuindex_consult, {
                user_id: window.utils.getCookie('user_id'),
                session: window.utils.getCookie('session')
            },function (result) {
                if (result.result_code == 200){
                    var data = [];
                    if(fillAll == 1){
                    		data.push('<option value="0">全部</option>');
                    }
                    if(result.values != null && result.values.length > 0){
	                    $.each(result.values,function (index, value) {
	                        data.push('<option value="'+value.value+'">'+value.name+'</option>');
	                    });
                    }
                    $(obj).empty().append(data.join(''));
                }
            });
        },
        createCookie: function (key, value) {
        		if(window.localStorage){
        			localStorage.setItem(key, value);
        		}
        		else{
        			var date = new Date();
	            var t = date.getTime();
	            t = t + 100 * 24 * 60 * 60 * 1000;
	            date = new Date(t);
	            $.cookie(key, value, { expires: date, path: '/' });
        		}
        },
        getCookie: function (key) {
        		if(window.localStorage){
        			return localStorage.getItem(key);
        		}
        		else{
        			var value = $.cookie(key);
	            if (value != null) {
	                return value;
	            } else {
	                return null;
	            }
        		}
        },
        deleteCookie: function (key) {
        		if(window.localStorage){
        			localStorage.removeItem(key);
        		}
        		else{
        			$.cookie(key, null, { expires: -1, path: '/' });
        		}
        },
        showMessage: function (message) {
        		swal({
			        title: '',
			        text: message,
			        type: "warning",
					zIndex:99999
			    });
        },
        showConfirm: function (message,confirmText,callback) {
        		swal({
			        title: '',
			        text: message,
			        type: "warning",
			        showCancelButton: true,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: confirmText,
			        cancelButtonText: "取消",
			        closeOnConfirm: false
			    }, function () {
			        callback && callback();
			});
        },
        showConfirmWithCabackSong: function (message,callback) {
        	swal({
        		title: '',
        		text: message,
        		type: "success",
        		showCancelButton: false,
        		confirmButtonColor: "#2BC2A3",
        		confirmButtonText: '确定',
        	}, function () {
        		callback && callback();
        	});
        },
		showMessageWithCallback: function (message,callback) {
        		swal({
			        title: '',
			        text: message,
			        type: "warning",
			        showCancelButton: false,
			        confirmButtonColor: "#DD6B55",
			        confirmButtonText: '确定',
			        closeOnConfirm: false
			    }, function () {
			        callback && callback();
			});
        },
        closeMessage: function () {
        		swal.close();
        },
        showLayerMessage: function (message) {
        		var layerIndex = layer.open({
				type: 1,
				icon: 1,
				skin: 'user-layer-class',
				title:'提示信息',
				closeBtn: false,
				area: ["320px","170px"],
				shadeClose: true,
				content: '<div style="padding:20px;">' + message + '</div>',
				btn:['确定']
        		});
        		return layerIndex;
        },
        showParentLayerMessage: function (message) {
        		var layerIndex = parent.layer.open({
				type: 1,
				icon: 1,
				skin: 'user-layer-class',
				title:'提示信息',
				closeBtn: false,
				area: ["320px","170px"],
				shadeClose: true,
				content: '<div style="padding:20px;">' + message + '</div>',
				btn:['确定']
        		});
        		return layerIndex;
        },
        showLayerConfirm: function (message,confirmText,yesCallback,noCallback) {
        		var layerIndex = layer.confirm(message,{
				skin: 'user-layer-class',
				title:'提示信息',
				shade: false,
				btn:[confirmText,'取消']
        		},function(){
				yesCallback && yesCallback();
			}, function(){
				noCallback && noCallback();
			});
			return layerIndex;
        },
        closeLayer: function (layerIndex) {
        		layer.close(layerIndex);
        },
        showLayerLoading: function (message) {
        		var layerIndex = layer.msg(message, {
        			shade: 0.3,
				icon: 16,
				time: 30000
				},
				function(){

				}
			);
			return layerIndex;
        },
        closeLayerLoading: function (loadingIndex) {
			layer.close(loadingIndex);
        },
		//获取URL中的参数值
        getQueryString: function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg); //获取url中"?"符后的字符串并正则匹配
            var context = "";
            if (r != null)
                context = r[2];
            reg = null;
            r = null;
            return context == null || context == "" || context == "undefined" ? "" : decodeURIComponent(context);
        },
        getAutoScrollGridHeight: function() {
        		return document.body.clientHeight-150;//可视区域高度
        		// return window.screen.height-150;//屏幕高度
        },
		// self-song-strat
		getAutoGridHeightSong: function() {
			var dh=document.body.clientHeight;
			return dh-310;
		},
		clearSpacesTextarea:function(){
			//textarea--强制去除空格
			$("body").on('blur','textarea',function(){
				let vals=$(this).val();
				vals = vals.trim();
				$(this).val(vals);
				let nums=0;
				if($(this).parent('div').find('em').length>0){
					 nums=vals.length;
				}else{
					nums=0;
				}
				$(this).parent('div').find('em').eq(0).text(nums);
			});
		},
		clearSpaces:function(){
			//保留1位或2位小数--并且强制去除空格
			$("body").on('blur','input',function(){
				let valNow=$(this).val();
				valNow = valNow.trim();
				$(this).val(valNow)
			});
			//textarea--强制去除空格
			$("body").on('blur','textarea',function(){
				let vals=$(this).val();
				vals = vals.trim();
				$(this).val(vals);
				let nums=0;
				if($(this).parent('div').find('em').length>0){
					nums=vals.length;
				}else{
					nums=0;
				}
				$(this).parent('div').find('em').eq(0).text(nums);
			});
		},

		// self-song-end
        getAutoGridHeight: function() {
        		return document.body.clientHeight-133;
        		// return window.screen.height-240;
        },
        getDateString: function() {
			var date = new Date();
		    var seperator = "-";
		    var year = date.getFullYear();
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    var currentdate = year + seperator + month + seperator + strDate;

		    return currentdate;
		},
		getChineseDateString: function() {
			var date = new Date();
		    var year = date.getFullYear();
		    var month = date.getMonth() + 1;
		    var strDate = date.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
		    var currentdate = year + '年' + month + '月' + strDate + '日';

		    return currentdate;
		},
		getChineseMonthString: function() {
			var date = new Date();
		    var month = date.getMonth() + 1;
		    return month+'月份';
		},
		getNextNote: function() {
			var date = new Date();
		    var year = date.getFullYear();
		    var month = date.getMonth() + 1;
		    if (month < 9) {
		        month = "09";
		    }
		    else{
		    		year = year + 1;
		    		month = "09";
		    }
		    var currentdate = year + "." + month;
		    return currentdate;
		},
		getDefaultNote: function() {
			var date = new Date();
		    var year = date.getFullYear();
		    var month = month = "09";
		    var currentdate = year + "." + month;
		    return currentdate;
		},
		getCompareDefaultNote: function() {
			var date = new Date();
		    var year = date.getFullYear() - 1;
		    var month = month = "09";
		    var currentdate = year + "." + month;
		    return currentdate;
		},
        //yyyy-mm-dd日期验证
        isDate: function (dateString) {
			var reg = /^(\d{4})-(\d{2})-(\d{2})$/;
			var str = dateString;
			var arr = reg.exec(str);
			if (str=="") return true;
			if (!reg.test(str)){
				return false;
			}
			return true;
		},
		//HH:ss验证
        isTime: function (timeString) {
			var reg = /^(([0-1]\d)|(2[0-3])):[0-5]\d$/;
			var str = timeString;
			var arr = str.match(str);
			if (str=="") return true;
			if (!reg.test(str)){
				return false;
			}
			return true;
		},
        //电话验证
        isMobile: function (mobile) {
            if (mobile.length == 0) {
                return false;
            }
            if (mobile.length != 11) {
                return false;
            }

            var myreg = /^(((1[3-9][0-9]{1})|159|153)+\d{8})$/;
            if (!myreg.test(mobile)) {
                return false;
            }
            return true;
        },
        //邮箱验证
        isEmail: function (email) {
            var re = /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/;
            if (re.test(email)) {
                return true;
            } else {
                return false;
            }
        },
        //数字验证
        isNumber: function (number) {
            var re =/^\d+(\.\d{1,2})?$/;
            if (re.test(number)) {
                return true;
            } else {
                return false;
            }
        },
        validateIDCard: function (idCard) {
            //15位和18位身份证号码的正则表达式
            var regIdCard = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;

            //如果通过该验证，说明身份证格式正确，但准确性还需计算
            if (regIdCard.test(idCard)) {
                if (idCard.length == 18) {
                    var idCardWi = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); //将前17位加权因子保存在数组里
                    var idCardY = new Array(1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2); //这是除以11后，可能产生的11位余数、验证码，也保存成数组
                    var idCardWiSum = 0; //用来保存前17位各自乖以加权因子后的总和
                    for (var i = 0; i < 17; i++) {
                        idCardWiSum += idCard.substring(i, i + 1) * idCardWi[i];
                    }

                    var idCardMod = idCardWiSum % 11;//计算出校验码所在数组的位置
                    var idCardLast = idCard.substring(17);//得到最后一位身份证号码

                    //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
                    if (idCardMod == 2) {
                        if (idCardLast == "X" || idCardLast == "x") {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
                        if (idCardLast == idCardY[idCardMod]) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            } else {
                return false;
            }
        }

    };

    window.utils = utils;
    window.globalhtp=globalhtp;
})();
