(function () {
    var chart = {
        /*
        重绘的问题解决方法，可以采用方法：
    1、chartObject.replot();
    2、if(chartObject != null){chartObject.destroy();}
    3、目前采用方法1来解决
    */
        initBarChart: function (chartObject, chartName, title, label, data, legends, angle, barWidth) {
            var divWidth = document.body.clientWidth - 40;
            var divHeight = document.body.clientHeight - 100;
            $("#chart").css("height", divHeight);
            $("#chart").css("width", divWidth);

            if (barWidth == 0) barWidth = null;
            if (chartObject == null) {
                chartObject = $.jqplot(chartName, {
                    title: title,
                    animate: true,
                    animateReplot: true,
                    data: data,
                    seriesDefaults: {
                        renderer: $.jqplot.BarRenderer,
                        pointLabels: {
                            show: true
                        },
                        shadow: true,
                        rendererOptions: {
                            barWidth: barWidth
                        }
                    },
                    axesDefaults: {
                        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                        tickOptions: {
                            angle: angle
                        }
                    },
                    axes: {
                        xaxis: {
                            renderer: $.jqplot.CategoryAxisRenderer
                        },
                        yaxis: {
                            label: label
                        }
                    },
                    legend: {
                        show: true,
                        renderer: $.jqplot.EnhancedLegendRenderer,
                        border: '10px solid #ffffff',
                        rowSpacing: '0.5em',
                        fontSize: '10pt',
                        placement: 'outsideGrid',
                        labels: legends
                    },
                    grid: {
                        drawGridLines: true,
                        gridLineColor: '#F7F7F7',
                        background: '#ffffff',
                        borderColor: '#efefef',
                        borderWidth: 0.5,
                        shadow: false
                    }
                });
            } else {
                chartObject.replot({
                    title: title,
                    data: data,
                    legend: {
                        labels: legends
                    }
                });
            }

            return chartObject;
        },
        initLineChart: function (chartObject, chartName, title, label, data, legends, angle, barWidth) {
            var divWidth = document.body.clientWidth - 40;
            var divHeight = document.body.clientHeight - 100;
            $("#chart").css("height", divHeight);
            $("#chart").css("width", divWidth);

            if (barWidth == 0) barWidth = null;
            if (chartObject == null) {
                chartObject = $.jqplot(chartName, {
                    title: title,
                    animate: true,
                    animateReplot: true,
                    data: data,
                    seriesDefaults: {
                        pointLabels: {
                            show: true
                        },
                        shadow: true,
                        rendererOptions: {
                            barWidth: barWidth
                        }
                    },
                    axesDefaults: {
                        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                        tickOptions: {
                            angle: angle
                        }
                    },
                    axes: {
                        xaxis: {
                            renderer: $.jqplot.CategoryAxisRenderer
                        },
                        yaxis: {
                            label: label
                        }
                    },
                    legend: {
                        renderer: $.jqplot.EnhancedLegendRenderer,
                        border: '10px solid #ffffff',
                        rowSpacing: '0.5em',
                        fontSize: '10pt',
                        show: true,
                        placement: 'outsideGrid',
                        labels: legends
                    },
                    grid: {
                        drawGridLines: true,
                        gridLineColor: '#F7F7F7',
                        background: '#ffffff',
                        borderColor: '#efefef',
                        borderWidth: 0.5,
                        shadow: false
                    }
                });
            } else {
                chartObject.replot({
                    title: title,
                    data: data,
                    legend: {
                        labels: legends
                    }
                });
            }

            return chartObject;
        },
        initPieChart: function (chartObject, chartName, title, data, legends) {
            var divWidth = document.body.clientWidth - 40;
            var divHeight = document.body.clientHeight - 100;
            $("#chart").css("height", divHeight);
            $("#chart").css("width", divWidth);

            if (chartObject == null) {
                var total = 0;
                $(data).map(function () {
                    total += this[1];
                })
                var arrLabels = $.makeArray($(data).map(function () {
                    return this[0] + " " + Math.round(this[1] / total * 100) + "%";
                }));

                chartObject = $.jqplot(chartName, {
                    title: title,
                    data: [data],
                    seriesDefaults: {
                        shadow: false,
                        renderer: $.jqplot.PieRenderer,
                        rendererOptions: {
                            dataLabels: arrLabels,
                            dataLabelsFormatString: '%s',
                            showDataLabels: true,
                            sliceMargin: 9,
                            shadow: true
                        }
                    },
                    legend: {
                        border: '10px solid #ffffff',
                        rowSpacing: '0.5em',
                        fontSize: '10pt',
                        show: true,
                        placement: 'outsideGrid',
                        background: '#ffffff',
                        escapeHtml: true,
                        labels: legends
                    },
                    grid: {
                        background: '#ffffff',
                        borderColor: '#ffffff',
                        borderWidth: 0.5,
                        shadow: false
                    }
                });
            } else {
                chartObject.replot({
                    title: title,
                    data: [data],
                    legend: {
                        labels: legends
                    }
                });
            }

            return chartObject;
        }

    };

    window.chart = chart;
})();