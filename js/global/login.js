/**
 * Created by Jason
 */
(function (win, $) {
    var login = {
        init: function () {
            if (utils.getQueryString('error') != '911' && utils.isLogin() == true) {
                window.location.href = 'index.html';
                return;
            }
            var obj = this;
            win.vaildation.init('#form');
            $('#login_button').on('click', function () {
                obj.do_login();
            });


            $(document).keyup(function (event) {
                if (event.keyCode == 13) {
                    obj.do_login()
                }
            });
        },
        do_login: function () {
            var user_id = $('#user_id').val();
            var user_password = $('#user_password').val();
            if (!$('#form').valid()) {
                if (user_id == '') {
                    $('#user_id').focus();
                    return;
                }
                if (user_password == '') {
                    $('#user_password').focus();
                    return;
                }
                return;
            }
            utils.ajax(utils.services.login, {
                    user_id: user_id,
                    password: user_password
                },
                function (result) {
                    if (result.result_code === 200) {
                        utils.createCookie('user_id', user_id);
                        utils.createCookie('user_name', result.user.name);
                        utils.createCookie('session', result.session);
                        window.location.href = 'index.html';
                    } else {
                        utils.showMessage('用户名或者密码不正确！');
                    }
                }
            );
        }
    };

    window.login = login;
})(window, jQuery);
