/**
 * Created by hanlu
 */
(function(win, $,qiniu){
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    var nowPage=1;
    var flagInput=false;
    var is_search=false;

    var price='';//配件费---before配件费(采购价格+人工费)
    var priceAf='';//配件费---after配件费(采购价格+人工费)
    var partsName='';//配件名称---before
    var partsNameAf='';//配件名称---after
    var brandName='';//配件品牌---before
    var brandNameAf='';//配件品牌---after
    var partsType='';//配件型号---before
    var partsTypeAf='';//配件型号---after
    var startTime='';//开始时间---before
    var startTimeAf='';//开始时间---after
    var endTime='';//结束时间---before
    var endTimeAf='';//结束时间---after

    //初始化
    function initSer(){
         price='';//配件费---before配件费(采购价格+人工费)
         priceAf='';//配件费---after配件费(采购价格+人工费)
         partsName='';//配件名称---before
         partsNameAf='';//配件名称---after
         brandName='';//配件品牌---before
         brandNameAf='';//配件品牌---after
         partsType='';//配件型号---before
         partsTypeAf='';//配件型号---after
         startTime='';//开始时间---before
         startTimeAf='';//开始时间---after
         endTime='';//结束时间---before
         endTimeAf='';//结束时间---after

        $("#partName").val('');
        $("#partFee").val('');
        $("#partBrand").val('');
        $("#partType").val('');
        $('#fenPaiTime').val('');
    }

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
    }

    // 刷新
    $('#btnQuery').on('click', function () {
        parts_manage.refresh();
    });
    // 搜索
    $('#btnSearch').on('click', function () {
        nowPage=1;
        let strVis=$('#fenPaiTime').val();
        if(strVis.length!=0){
            startTimeAf=strVis.slice(0,10);//上门时间开始
            endTimeAf=strVis.slice(13);//上门时间结束
        }else{
            startTimeAf='';//上门时间开始
            endTimeAf='';//上门时间结束
        };

        startTime=startTimeAf;//开始时间---after
        endTime=endTimeAf;//结束时间---after

        partsNameAf=$("#partName").val();
        partsName=partsNameAf;//配件名称---after

        priceAf=$("#partFee").val();
        price=priceAf;//配件费---after配件费(采购价格+人工费)

        brandNameAf=$("#partBrand").val();
        brandName=brandNameAf;//配件品牌---after

        partsTypeAf=$("#partType").val();
        partsType=partsTypeAf;//配件型号---after

        parts_manage.getServerData(1);
    });

    // JS监听某个输入框---回车事件绑定
    $('#toolbar').on('keyup','input', function(event) {
        if (event.keyCode == "13") {
            //回车执行查询
            $(this).blur();

            partsNameAf=$("#partName").val();// 配件名称---after
            partsName=partsNameAf;//配件名称---before

            priceAf=$("#partFee").val();//配件费---after配件费(采购价格+人工费)
            price=priceAf;//配件费---before配件费(采购价格+人工费)

            brandNameAf=$("#partBrand").val();// 配件品牌---after
            brandName=brandNameAf;//配件品牌---before

            partsTypeAf=$("#partType").val();// 配件型号---after
            partsType=partsTypeAf;//配件型号---before

            // -时间-处理-
            let strVis=$('#fenPaiTime').val();
            if(strVis.length!=0){
                startTimeAf=strVis.slice(0,10);//上门时间开始
                endTimeAf=strVis.slice(13);//上门时间结束
            }else{
                startTimeAf='';//上门时间开始
                endTimeAf='';//上门时间结束
            };
            startTime=startTimeAf;//开始时间---after
            endTime=endTimeAf;//结束时间---after
            nowCurPage =1;
            parts_manage.getServerData(1);
        }
    });

    var parts_manage = {
        //初始化
        init: function() {
            var obj = this;
            if(utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            //判断是否有配件列表权限，此时为配件库的配件列表权限
            utils.isUserMenuIndexRight('preparation_parts_list', function(result) {
                if(result == 1) {
                    //initialize...
                    utils.clearSpaces();
                    obj.initList();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });

            // 配件费校验
            $('#partFee').on('blur',function () {
                parts_manage.priceCheck(this);
                if($('#partFee').val()!=''){
                    price=Number($('#partFee').val());
                    price=isNaN(price)?'':price.toFixed(2);
                    $('#partFee').val(price);
                }
            });

            $(document).keydown(function (event) {
                var  e=window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if(flagInput==false){
                        if(e.preventDefault){
                            e.preventDefault();
                        }else{
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });

        },
        //初始化请求
        initList: function(){
            initSer();

            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height:utils.getAutoGridHeight(),
                autowidth: true,
                rowNum: 10,
                rownumbers: true,
                rownumWidth:60,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["配件名称","配件品牌","配件型号","采购价/元","配件费/元","配件单位","配件图片","创建时间"],
                colModel: [
                    {
                        name: "partsName",
                        index: "partsName",
                        editable: false,
                        sortable: false,
                    },
                    {
                        name: "brandName",
                        index: "brandName",
                        editable: false,
                        sortable: false,
                    },
                    {
                        name: "partsType",
                        index: "partsType",
                        editable: false,
                        sortable: false,
                    },
                    {
                        name: "purchasePrice",
                        index: "purchasePrice",
                        editable: false,
                        sortable: false,
                    },
                    {
                        name: "price",
                        index: "price",
                        editable: false,
                        sortable: false,
                    },
                    {
                        name: "unitName",
                        index: "unitName",
                        editable: false,
                        width: 80,
                        sortable: false,
                    },
                    {
                        name: "picUrl",
                        index: "picUrl",
                        editable: false,
                        sortable: false,
                        formatter: imgFormatter
                    },
                    {
                        name: "createTime",
                        index: "createTime",
                        editable: false,
                        sortable: false,
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                shrinkToFit: false,
                autoScroll:true,
                // 翻页
                onPaging: function(pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if(pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if(pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if(pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if(pgButton == 'first') {
                        currentPage = 1;
                    }
                    if(pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if(pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if(deferLoadData == 0) {
                        parts_manage.getServerData(nowPage);
                    }
                },
                onSortCol: function(index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage=currentPage;
                },
                //当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowindex = $("#" + this.id).getGridParam('selrow');
                    //返回点击这行xlmc的值
                    // selectedRowValue = $("#gridTable").jqGridRowValue("xlmc");
                },
            });

            parts_manage.getServerData(nowPage);

            function imgFormatter(cellvalue, options, rowObject){
                if(rowObject.picUrl){
                    return '<img class="mySelfImg" name="'+rowObject.picUrl.split(',')[0]+'" src="'+rowObject.picUrl.split(',')[0]+'">';
                }
                else return ''
            }

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(win).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        getServerData: function(pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            let postData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                position:nowPage,
                rows:10,
            };
            postData.partsName=partsName;//配件名称
            postData.brandName=brandName;// 配件品牌
            postData.partsType=partsType;//配件型号
            postData.price=price;//配件费(采购价格+人工费)
            let startTimeVal='';
            let endTimeVal='';
            if(startTime!=''){
                startTimeVal=startTime+' 00:00:00';
            };
            if(endTime!=''){
                endTimeVal=endTime+' 23:59:59';
            };
            postData.startTime=startTimeVal;//创建的开始时间
            postData.endTime=endTimeVal;//创建的结束时间
            win.utils.ajaxPost(win.utils.services.partMan_queryParts, postData, function(result) {
                if(result.result_code == 200) {
                    if(result.data.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.data.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                        utils.showMessage('暂无数据！');
                    } else {
                        gridData = result.data.list;
                        totalPages = Math.ceil(result.data.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.data.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },

        // 刷新
        refresh: function () {
            nowPage=1;
            initSer();
            parts_manage.getServerData(1);
        },
        // 输入框价格格式处理
        priceCheck:function(th,event) {
            var event = event || window.event;
            var code = event.keyCode;
            if(navigator.userAgent.indexOf("Firefox")>-1){
                code = event.which;
            }
            if(code == 37 || code ==39) return;
            var regStrs = [
                ['^0(\\d+)$', '$1'], //禁止录入整数部分两位以上，但首位为0
                ['[^\\d\\.]+$', ''], //禁止录入任何非数字和点
                ['\\.(\\d?)\\.+', '.$1'], //禁止录入两个以上的点
                ['^(\\d+\\.\\d{2}).+', '$1'], //禁止录入小数点后两位以上
                ['^(\\.\\d+)','$1']//禁止输入情况下小数点出现在首位
            ];
            for (i = 0; i < regStrs.length; i++) {
                var reg = new RegExp(regStrs[i][0]);
                th.value = th.value.replace(reg, regStrs[i][1]);

            }
        },
    };
	win.parts_manage = parts_manage
})(window, jQuery,qiniu);
