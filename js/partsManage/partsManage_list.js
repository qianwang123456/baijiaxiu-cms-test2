/**
 * Created by hanlu
 */
(function (win, $, qiniu) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    var nowPage = 1;

    var cardImageUrl = '';
    var cardImageSelected;

    var flagInput = false;
    var is_search = false;

    // 请求参数
    var data = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        firstTypeId: null,
        firstTypeName: null,
        secondTypeId: null,
        secondTypeName: null,
        partsName: null,
        brandName: null,
        partsType: null,
        unit: null,
        purchasePrice: null,
        laborCost: null,
        price: null,
        partsNum: null,
        partsTime: null,
        isThreshold: null,
        partsPeriod: null,
        picUrl: null,
        status: null,
        isUse: null,
        remark: null,
        position: nowPage,
        rows: 20
    };

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();

        $('#txtNewTitle').val('');
        $('#txtNewDate').val('');
        $('#chkTopFlag').iCheck('uncheck');
        $('#chkFirstFlag').iCheck('uncheck');

        cardImageUrl = '';
        cardImageSelected = 0;
        $("#cardImage").attr('src', '../../images/award_default.jpg');
        // parts_manage.refreshButtonState();
    }

    // 刷新
    $('#btnQuery').on('click', function () {
        parts_manage.refresh();
    });
    // 筛选
    $('#btnSearch').on('click', function () {
        nowPage = 1;
        is_search = true;
        parts_manage.search();
    });

    // 增加
    $('#btnAdd').on('click', function () {
        // initializeEditForm();
        parts_manage.normalItemStatus(); //弹层内容显示
        parts_manage.getPartsUnit();//获取配件单位
        // 获取一级配件类型
        parts_manage.getFirstTypeName();

        // 一级配件类型、二级配件类型点击事件
        var firstTypeId, firstTypeName, secondTypeId, secondTypeName;
        layui.use(['form'], function () {
            form = layui.form;
            form.on('select(firstTypeName)', function () {
                firstTypeId = $('#firstTypeName').val();
                firstTypeName = $('#firstTypeName').find("option:selected").text();
                if (firstTypeId) {
                    parts_manage.getSecondTypeName(firstTypeId);
                }
            });

            form.on('select(secondTypeName)', function () {
                if (firstTypeId) {
                    secondTypeId = $('#secondTypeName').val();
                    secondTypeName = $('#secondTypeName').find("option:selected").text();
                } else {
                    utils.showMessage('请先选择一级配件类型！');
                }
            });
        });

        var i = layer.open({
            type: 1,
            area: ['80%', '80%'],
            title: '<i class="fa fa-plus">&nbsp;&nbsp;添加配件</i>',
            content: $('#model'),
            btn: ['创建', '取消'],
            success: function (layero, index) {
                // $('#price').attr('readonly','read')
                $('.layui-layer-page').css('zIndex', 333);
                $('.layui-layer-shade').css('zIndex', 300);

                flagInput = false;

                var laborCost = 0, purchasePrice = 0;
                $('#purchasePrice').on('input', function () {
                    parts_manage.priceCheck(this);
                    purchasePrice = parseInt($('#purchasePrice').val() * 100) / 100;
                    $('#purchasePrice').val(purchasePrice);
                    $('#price').val(Math.round((Number(purchasePrice) + Number(laborCost)) * 100) / 100);

                });
                $('#purchasePrice').on('blur', function () {
                    parts_manage.overFormat(this);
                    parts_manage.overFormat($('#price')[0]);
                    if ($('#purchasePrice').val() < 0) {
                        utils.showLayerMessage('配件单价不可小于0,请重新输入！');
                        $('#purchasePrice').val('');
                    }
                });
                $('#laborCost').on('input', function () {
                    parts_manage.priceCheck(this);
                    laborCost = parseInt($('#laborCost').val() * 100) / 100;
                    $('#laborCost').val(laborCost);
                    $('#price').val(Math.round((Number(purchasePrice) + Number(laborCost)) * 100) / 100);

                });
                $('#laborCost').on('blur', function () {
                    parts_manage.overFormat($('#price')[0]);
                    parts_manage.overFormat(this);
                    if ($('#laborCost').val() < 0) {
                        utils.showLayerMessage('人工费不可小于0,请重新输入！');
                        $('#laborCost').val('');
                    }
                });

                $('#partsPeriod').on('blur', function () {
                    if ($('#partsPeriod').val() < 0) {
                        utils.showLayerMessage('配件周期不可小于0,请重新输入！');
                        $('#partsPeriod').val('');
                    } else {
                        $('#partsPeriod').val(Math.round($('#partsPeriod').val()));
                    }
                });

                // 图片放大
                $('#uploadImgBox').on('click', 'img', function () {
                    var index = $(this).attr('layer-pid');

                    var imgs = $('#uploadImgBox').find('img');
                    var imgArr = [];
                    for (var i = 0; i < imgs.length; i++) {
                        imgArr.push({
                            "alt": "",
                            "pid": i, //图片id
                            "src": $(imgs[i]).attr('src'), //原图地址
                            "thumb": $(imgs[i]).attr('layer-src')//缩略图地址
                        })
                    }

                    var json = {
                        "title": "", //相册标题
                        "id": index, //相册id
                        "start": index, //初始显示的图片序号，默认0
                        "data": imgArr
                    };
                    layer.photos({
                        photos: json,
                        closeBtn: true,
                        anim: 5, //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
                    });
                })
            },
            yes: function (index, layero) {
                var new_partsName = $('#addPartsName').val(); //配件名
                var brandName = $('#brandName').val(); //配件品牌
                var partsType = $('#partsType').val(); //品牌型号
                var partsUnit = $('#partsUnit').val(); //配件单位
                var unitName = $("#partsUnit option:selected").text();
                var purchasePrice = $('#purchasePrice').val(); //配件单价
                var laborCost = $('#laborCost').val(); //人工费
                var price = $('#price').val(); //配件费
                var partsNum = $('#partsNum').val(); //数量限制
                var isThreshold = $("input[name='threshold']:checked").val(); //是否可超过限制
                var partsPeriod = $('#partsPeriod').val(); //配件周期
                var partsTime = $('#partsTime').val(); //配件工期
                var addStatus = $('#addStatus').val(); //状态
                var isUse = $('#isUse').val(); //是否常用
                var remark = $('#addRemark').val() || null;
                var imgArr = [];
                var liNode = $('#uploadImgBox').find('li');
                for (var i = 0; i < liNode.length; i++) {
                    imgArr.push($(liNode[i]).find('img').attr('src').split('/')[3]);
                }

                if (!firstTypeId) {
                    utils.showLayerMessage('请选择一级配件类型！');
                    return;
                }
                if (!secondTypeId) {
                    utils.showLayerMessage('请选择二级配件类型！');
                    return;
                }
                if (new_partsName == '') {
                    $('#addPartsName').focus();
                    utils.showLayerMessage('请输入配件名称！');
                    return;
                }
                if (brandName == '') {
                    $('#brandName').focus();
                    utils.showLayerMessage('请输入配件品牌！');
                    return;
                }
                if (partsType == '') {
                    $('#partsType').focus();
                    utils.showLayerMessage('请输入品牌型号！');
                    return;
                }
                if (partsUnit == '') {
                    $('#partsUnit').focus();
                    utils.showLayerMessage('请输入配件单位！');
                    return;
                }
                if (purchasePrice == '') {
                    $('#purchasePrice').focus();
                    utils.showLayerMessage('请输入配件单价！');
                    return;
                }
                if (laborCost == '') {
                    $('#laborCost').focus();
                    utils.showLayerMessage('请输入人工费！');
                    return;
                }
                if (price == '') {
                    $('#price').focus();
                    utils.showLayerMessage('请输入配件费！');
                    return;
                }
                if (partsNum == '') {
                    $('#partsNum').focus();
                    utils.showMessage('请输入配件数量限制');
                    return;
                }
                if (!isThreshold) {
                    utils.showMessage('请选择是否可超出数量限制');
                    return;
                }
                if (partsPeriod == '') {
                    $('#partsPeriod').focus();
                    utils.showLayerMessage('请输入配件周期！');
                    return;
                }
                if (partsTime == '') {
                    $('#partsTime').focus();
                    utils.showMessage('请输入配件工期');
                    return;
                }
                if (partsPeriod < 0) {
                    $('#partsPeriod').focus();
                    utils.showLayerMessage('请输入正确的配件周期！');
                    return;
                }
                if (addStatus == '') {
                    $('#addStatus').focus();
                    utils.showLayerMessage('请输入状态！');
                    return;
                }
                if (isUse == '') {
                    $('#isUse').focus();
                    utils.showLayerMessage('请输入是否常用！');
                    return;
                }

                // 请求参数
                var addPostData = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    firstTypeId: firstTypeId,
                    firstTypeName: firstTypeName,
                    secondTypeId: secondTypeId,
                    secondTypeName: secondTypeName,
                    partsName: new_partsName,
                    brandName: brandName,
                    partsType: partsType,
                    unit: partsUnit,
                    unitName: unitName,
                    purchasePrice: purchasePrice,
                    laborCost: laborCost,
                    price: price,
                    partsNum, //配件数量限制
                    isThreshold, //是否可超
                    partsPeriod: partsPeriod,
                    partsTime, //配件工期
                    picUrl: imgArr.join(','),
                    status: addStatus,
                    isUse: isUse,
                    remark: remark
                };

                win.utils.ajaxPost(win.utils.services.addParts, addPostData, function (result) {
                    if (result.result_code == 200) {
                        utils.showMessage('新增成功！');

                        // 回显图片地址
                        var imgArrBack = [];
                        var liNode = $('#uploadImgBox').find('li');
                        for (var i = 0; i < liNode.length; i++) {
                            imgArrBack.push($(liNode[i]).find('img').attr('src'));
                        }

                        $('#table').jqGrid('addRowData', result.data.id, {
                            id: result.data.id,
                            firstTypeId: firstTypeId,
                            firstTypeName: firstTypeName,
                            secondTypeId: secondTypeId,
                            secondTypeName: secondTypeName,
                            partsName: new_partsName,
                            brandName: brandName,
                            partsType: partsType,
                            unitName: result.data.unitName,
                            purchasePrice: purchasePrice,
                            laborCost: laborCost,
                            price: price,
                            partsNum, //配件数量限制
                            isThreshold,//是否可超
                            partsPeriod: partsPeriod,
                            partsTime, //配件工期
                            picUrl: imgArrBack.join(','),
                            status: addStatus,
                            isUse: isUse,
                            remark: remark,
                            city: '全国',
                            partsCode: result.data.partsCode
                        });

                        parts_manage.layerInit();
                        layer.close(index);
                    } else if (result.result_code == 6601) {
                        utils.showMessage('该配件已存在！');
                    } else {
                        utils.showLayerMessage('创建失败，请重试！');
                    }
                });
            },
            btn2: function () {
                parts_manage.layerInit();
            },
            cancel: function () {
                parts_manage.layerInit();
            }
        });
    });
    // 修改
    $('#table').on('click', '#btnUpdate', function () {
        parts_manage.normalItemStatus();

        var id = $(this).attr('data-id');


        // 一级配件类型、二级配件类型点击事件
        var firstTypeId, firstTypeName, secondTypeId, secondTypeName;
        layui.use(['form'], function () {
            form = layui.form;
            form.on('select(firstTypeName)', function () {
                firstTypeId = $('#firstTypeName').val();
                firstTypeName = $('#firstTypeName').find("option:selected").text();
                if (firstTypeId) {
                    parts_manage.getSecondTypeName(firstTypeId);
                }
            });

            form.on('select(secondTypeName)', function () {
                secondTypeId = $('#secondTypeName').val();
                secondTypeName = $('#secondTypeName').find("option:selected").text();
            });
        });


        var i = layer.open({
            type: 1,
            area: ['80%', '80%'],
            title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;修改配件信息',
            content: $('#model'),
            btn: ['保存', '取消'],
            success: function () {
                $('.layui-layer-page').css('zIndex', 333);
                $('.layui-layer-shade').css('zIndex', 300);

                flagInput = false;

                var laborCost = 0, purchasePrice = 0, price = 0;

                // 获取修改数据信息
                win.utils.ajaxPost(win.utils.services.queryParts, {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    id: id
                }, function (result) {
                    if (result.result_code == 200) {
                        firstTypeId = result.parts[0].firstTypeId;
                        firstTypeName = result.parts[0].firstTypeName;
                        secondTypeId = result.parts[0].secondTypeId;
                        secondTypeName = result.parts[0].secondTypeName;

                        unitId = result.parts[0].unit;
                        unitName = result.parts[0].unitName;


                        laborCost = result.parts[0].laborCost;
                        purchasePrice = result.parts[0].purchasePrice;
                        price = result.parts[0].price;


                        // 获取一级配件类型
                        win.utils.ajaxPost(win.utils.services.queryPartsType, {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            rank: 1,
                            status: 1
                        }, function (res) {
                            if (res.result_code == 200) {
                                if (res.configPartsTypes.length) {
                                    var optionsStr = '';
                                    res.configPartsTypes.forEach(function (i, k) {
                                        optionsStr += '<option value="' + i.firstTypeId + '">' + i.firstTypeName + '</option>';
                                    });
                                    $('#firstTypeName').append($(optionsStr));
                                    layui.use('form', function () {
                                        var form = layui.form;
                                        form.render('select');
                                    });

                                    $('#firstTypeName').val(result.parts[0].firstTypeId);
                                    $('#firstTypeName').find('option[text=' + result.parts[0].secondTypeId + ']').attr('selected', true);
                                    layui.use('form', function () {
                                        var form = layui.form;
                                        form.render('select');
                                    });
                                }
                            }
                        });
                        // 获取二级配件类型
                        win.utils.ajaxPost(win.utils.services.queryPartsType, {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            rank: 2,
                            firstTypeId: result.parts[0].firstTypeId,
                            status: 1
                        }, function (result) {
                            if (result.result_code == 200) {
                                if (result.configPartsTypes.length) {
                                    var optionsStr = '';
                                    result.configPartsTypes.forEach(function (i, k) {
                                        optionsStr += '<option value="' + i.secondTypeId + '">' + i.secondTypeName + '</option>';
                                    });
                                    $('#secondTypeName').append($(optionsStr));
                                    layui.use('form', function () {
                                        var form = layui.form;
                                        form.render('select');
                                    });


                                    $('#secondTypeName').val(secondTypeId);
                                    $('#secondTypeName').find('option[text=' + secondTypeName + ']').attr('selected', true);
                                    layui.use('form', function () {
                                        var form = layui.form;
                                        form.render('select');
                                    });
                                }
                            }
                        });

                        // 获取配件单位
                        win.utils.ajaxPost(win.utils.services.queryPartsUnit, {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                        }, function (result) {
                            if (result.result_code == 200) {
                                var optionStr = '<option selected value="">请选择单位</option>';
                                result.configPartsUnits.forEach(function (i, k) {
                                    optionStr += '<option  value="' + i.id + '">' + i.unitName + '</option>';
                                });
                                $('#partsUnit').html(optionStr);

                                layui.use('form', function () {
                                    var form = layui.form;
                                    form.render('select');
                                });


                                $('#partsUnit').val(unitId);
                                $('#partsUnit').find('option[text=' + unitName + ']').attr('selected', true);
                                layui.use('form', function () {
                                    var form = layui.form;
                                    form.render('select');
                                });

                                // $('#partsUnit').val(result.parts[0].unit); //配件单位
                            } else {
                                utils.showMessage('获取配件单位失败！');
                            }
                        })

                        $('#addPartsName').val(result.parts[0].partsName); //配件名
                        $('#brandName').val(result.parts[0].brandName); //配件品牌
                        $('#partsType').val(result.parts[0].partsType); //品牌型号
                        // $('#partsUnit').val(result.parts[0].unit); //配件单位
                        $('#purchasePrice').val(result.parts[0].purchasePrice); //配件单价
                        $('#laborCost').val(result.parts[0].laborCost); //人工费
                        $('#price').val(result.parts[0].price); //配件费
                        $('#partsNum').val(result.parts[0].partsNum); //数量限制
                        // $("input[name='threshold'][value=\""+result.parts[0].isThreshold+"\"]").attr("checked", true);//是否可超过数量限制

                        $('#partsPeriod').val(result.parts[0].partsPeriod); //配件周期
                        $('#partsTime').val(result.parts[0].partsTime); //配件工期
                        $('#addStatus').val(result.parts[0].status); //状态
                        $('#isUse').val(result.parts[0].isUse); //是否常用
                        $('#addRemark').val(result.parts[0].remark); //备注
                        var img = result.parts[0].picUrl ? result.parts[0].picUrl.split(',') : '';
                        var imgStr = '';
                        for (var i = 0; i < img.length; i++) {
                            imgStr += '<li>\n' +
                                '<img class="cardImage" style="margin-top: 10px; width: 100px; height: 100px; max-width: 400px; border: 10px solid #EEEEEE;" layer-pid="' + i + '" layer-src="' + img[i] + '" src="' + img[i] + '">\n' +
                                '<span class="layui-layer-setwin"><a class="layui-layer-ico layui-layer-close layui-layer-close1" href="javascript:;"></a></span>\n' +
                                '</li>';
                        }
                        $('#uploadImgBox').html(imgStr);


                        console.log(result.parts[0].isThreshold)
                        if(result.parts[0].isThreshold){
                            if(result.parts[0].isThreshold==1){
                                $("input[name='threshold'][value='1']").prop('checked',true);//是否可超过数量限制
                                $("input[name='threshold'][value='2']").prop('checked',false);//是否可超过数量限制
                            }
                            else if(result.parts[0].isThreshold==2){
                                $("input[name='threshold'][value='2']").prop('checked',true);//是否可超过数量限制
                                $("input[name='threshold'][value='1']").prop('checked',false);//是否可超过数量限制
                            }
                        }
                        else{
                            $("input[name='threshold'][value='1']").prop('checked',false);//是否可超过数量限制
                            $("input[name='threshold'][value='2']").prop('checked',false);//是否可超过数量限制
                        }

                        layui.use('form',function () {
                            var form=layui.form;
                            form.render('radio')
                        });
                    } else {
                        utils.showMessage('获取数据失败，请重试！');
                        return;
                    }
                });


                // 配件费计算
                $('#purchasePrice').on('input', function () {
                    purchasePrice = parseInt($('#purchasePrice').val() * 100) / 100;
                    $('#purchasePrice').val(purchasePrice);
                    $('#price').val(Math.round((Number(purchasePrice) + Number(laborCost)) * 100) / 100);
                });
                $('#purchasePrice').on('blur', function () {
                    if ($('#purchasePrice').val() < 0) {
                        utils.showLayerMessage('配件单价不可小于0,请重新输入！');
                        $('#purchasePrice').val('');
                    }
                });
                $('#laborCost').on('input', function () {
                    laborCost = parseInt($('#laborCost').val() * 100) / 100;
                    $('#laborCost').val(laborCost);
                    $('#price').val(Math.round((Number(purchasePrice) + Number(laborCost)) * 100) / 100);
                });
                $('#laborCost').on('blur', function () {
                    if ($('#laborCost').val() < 0) {
                        utils.showLayerMessage('人工费不可小于0,请重新输入！');
                        $('#laborCost').val('');
                    }
                });


                $('#partsPeriod').on('blur', function () {
                    if ($('#partsPeriod').val() < 0) {
                        utils.showLayerMessage('配件周期不可小于0,请重新输入！');
                        $('#partsPeriod').val('');
                    } else {
                        $('#partsPeriod').val(Math.round($('#partsPeriod').val()));
                    }
                });


                // 图片放大
                $('#uploadImgBox').on('click', 'img', function () {
                    var index = $(this).attr('layer-pid');

                    var imgs = $('#uploadImgBox').find('img');
                    var imgArr = [];
                    for (var i = 0; i < imgs.length; i++) {
                        imgArr.push({
                            "alt": "",
                            "pid": i, //图片id
                            "src": $(imgs[i]).attr('src'), //原图地址
                            "thumb": $(imgs[i]).attr('layer-src')//缩略图地址
                        })
                    }

                    var json = {
                        "title": "", //相册标题
                        "id": index, //相册id
                        "start": index, //初始显示的图片序号，默认0
                        "data": imgArr
                    };
                    layer.photos({
                        photos: json,
                        closeBtn: true,
                        anim: 5, //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
                    });
                });
            },
            yes: function (index, layero) {
                var new_partsName = $('#addPartsName').val(); //配件名
                var brandName = $('#brandName').val(); //配件品牌
                var partsType = $('#partsType').val(); //品牌型号
                var partsUnit = $('#partsUnit').val(); //配件单位
                var purchasePrice = $('#purchasePrice').val(); //配件单价
                var laborCost = $('#laborCost').val(); //人工费
                var price = $('#price').val(); //配件费
                var partsNum = $('#partsNum').val(); //数量限制
                var isThreshold = $("input[name='threshold']:checked").val(); //是否可超过限制
                var partsPeriod = $('#partsPeriod').val(); //配件周期
                var partsTime = $('#partsTime').val(); //配件工期
                var addStatus = $('#addStatus').val(); //状态
                var isUse = $('#isUse').val(); //是否常用
                var remark = $('#addRemark').val() || null;
                var imgArr = [];
                var liNode = $('#uploadImgBox').find('li');
                for (var i = 0; i < liNode.length; i++) {
                    imgArr.push($(liNode[i]).find('img').attr('src').split('/')[3]);
                }

                if (!firstTypeId) {
                    utils.showLayerMessage('请选择一级配件类型！');
                    return;
                }
                if (!secondTypeId) {
                    utils.showLayerMessage('请选择二级配件类型！');
                    return;
                }
                if (new_partsName == '') {
                    $('#addPartsName').focus();
                    utils.showLayerMessage('请输入配件名称！');
                    return;
                }
                if (brandName == '') {
                    $('#brandName').focus();
                    utils.showLayerMessage('请输入配件品牌！');
                    return;
                }
                if (partsType == '') {
                    $('#partsType').focus();
                    utils.showLayerMessage('请输入品牌型号！');
                    return;
                }
                if (partsUnit == '') {
                    $('#partsUnit').focus();
                    utils.showLayerMessage('请输入配件单位！');
                    return;
                }
                if (purchasePrice == '') {
                    $('#purchasePrice').focus();
                    utils.showLayerMessage('请输入配件单价！');
                    return;
                }
                if (laborCost == '') {
                    $('#laborCost').focus();
                    utils.showLayerMessage('请输入人工费！');
                    return;
                }
                if (price == '') {
                    $('#price').focus();
                    utils.showLayerMessage('请输入配件费！');
                    return;
                }
                if (partsNum == '') {
                    $('#partsNum').focus();
                    utils.showMessage('请输入配件数量限制');
                    return;
                }
                if (!isThreshold) {
                    utils.showMessage('请选择是否可超过数量限制');
                    return;
                }
                if (partsPeriod == '') {
                    $('#partsPeriod').focus();
                    utils.showLayerMessage('请输入配件周期！');
                    return;
                }
                if (partsTime == '') {
                    $('#partsTime').focus();
                    utils.showMessage('请输入配件工期');
                    return;
                }
                if (partsPeriod < 0) {
                    $('#partsPeriod').focus();
                    utils.showLayerMessage('请输入正确的配件周期！');
                    return;
                }
                if (addStatus == '') {
                    $('#addStatus').focus();
                    utils.showLayerMessage('请输入状态！');
                    return;
                }
                if (isUse == '') {
                    $('#isUse').focus();
                    utils.showLayerMessage('请输入是否常用！');
                    return;
                }

                // 请求参数
                var addPostData = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    firstTypeId: firstTypeId,
                    firstTypeName: firstTypeName,
                    secondTypeId: secondTypeId,
                    secondTypeName: secondTypeName,
                    partsName: new_partsName,
                    brandName: brandName,
                    partsType: partsType,
                    unit: partsUnit,
                    purchasePrice: purchasePrice,
                    laborCost: laborCost,
                    price: price,
                    partsNum, //数量限制
                    isThreshold, //是否可超
                    partsPeriod: partsPeriod,
                    partsTime, //配件工期
                    picUrl: imgArr.join(','),
                    status: addStatus,
                    isUse: isUse,
                    remark: remark,
                    id: id
                };

                win.utils.ajaxPost(win.utils.services.updateParts, addPostData, function (result) {
                    if (result.result_code == 200) {
                        utils.showMessage('修改成功！');
                        // 回显图片地址
                        var imgArrBack = [];
                        var liNode = $('#uploadImgBox').find('li');
                        for (var i = 0; i < liNode.length; i++) {
                            imgArrBack.push($(liNode[i]).find('img').attr('src'));
                        }
                        $('#table').jqGrid('setRowData', id, {
                            firstTypeId: $('#firstTypeName').val(),
                            firstTypeName: $('#firstTypeName').find("option:selected").text(),
                            secondTypeId: $('#secondTypeName').val(),
                            secondTypeName: $('#secondTypeName').find("option:selected").text(),
                            partsName: new_partsName,
                            brandName: brandName,
                            partsType: partsType,
                            unitName: $("#partsUnit option:selected").text(),
                            purchasePrice: purchasePrice,
                            laborCost: laborCost,
                            price: price,
                            partsNum, //数量限制
                            isThreshold, //是否可超
                            partsPeriod: partsPeriod,
                            partsTime, //配件工期
                            picUrl: imgArrBack.join(','),
                            status: addStatus,
                            isUse: isUse,
                            remark: remark,
                        });
                        parts_manage.layerInit();
                        layer.close(index);
                    } else if (result.result_code == 6601) {
                        utils.showMessage('该配件已存在！');
                    } else {
                        utils.showLayerMessage('修改失败，请重试！');
                    }
                });

            },
            // btn2: function () {
            //     parts_manage.layerInit();
            // },
            // cancel: function () {
            //     parts_manage.layerInit();
            // }
            end:function () {
                parts_manage.layerInit();
            }
        });


    });

    // 一级配件类型、二级配件类型点击事件
    var firstTypeId, firstTypeName, secondTypeId, secondTypeName;
    layui.use(['form'], function () {
        form = layui.form;
        form.on('select(firstTypeName)', function () {
            firstTypeId = $('#firstTypeName').val();
            firstTypeName = $('#firstTypeName').find("option:selected").text();
            if (firstTypeId) {
                parts_manage.getSecondTypeName(firstTypeId);
            }
        });

        form.on('select(secondTypeName)', function () {
            if (firstTypeId) {
                secondTypeId = $('#secondTypeName').val();
                secondTypeName = $('#secondTypeName').find("option:selected").text();
            } else {
                utils.showMessage('请先选择一级配件类型！');
            }
        })
    });

    // 删除
    $('#table').on('click', '#btnDelete', function () {
        var id = $(this).attr('data-id');
        utils.showConfirm('您确定要删除该配件吗？', '删除', function () {
            win.utils.ajaxPost(win.utils.services.deleteParts, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                id: id
            }, function (result) {
                if (result.result_code == 200) {
                    // layer.close(index);
                    utils.showMessage('删除成功！');
                    $('#table').jqGrid('delRowData', id);
                }
            });
        });
    });

    // 导出功能
    $('#export').on('click', function () {
        var new_partsName = $('#addPartsName').val(); //配件名
        var brandName = $('#brandName').val(); //配件品牌
        var partsType = $('#partsType').val(); //品牌型号
        var partsUnit = $('#partsUnit').val(); //配件单位
        var purchasePrice = $('#purchasePrice').val(); //配件单价
        var laborCost = $('#laborCost').val(); //人工费
        var price = $('#price').val(); //配件费
        var partsNum = $('#partsNum').val(); //数量限制
        var isThreshold = $("input[name='threshold']:checked").val(); //是否可超过限制
        var partsPeriod = $('#partsPeriod').val(); //配件周期
        var partsTime = $('#partsTime').val(); //配件工期
        var addStatus = $('#addStatus').val(); //状态
        var isUse = $('#isUse').val(); //是否常用
        var remark = $('#addRemark').val() || null;
        var imgArr = [];
        var liNode = $('#uploadImgBox').find('li');
        for (var i = 0; i < liNode.length; i++) {
            imgArr.push($(liNode[i]).find('img').attr('src'));
        }
        // 请求参数
        data = {
            user_id: win.utils.getCookie('user_id'),
            session: win.utils.getCookie('session'),
            firstTypeId: $('#firstTypeName').val() || '',
            firstTypeName: $('#firstTypeName').find("option:selected").text() == "请选择一级配件类型" ? '' : $('#firstTypeName').find("option:selected").text(),
            secondTypeId: $('#secondTypeName').val() || '',
            secondTypeName: $('#secondTypeName').find("option:selected").text() == "请选择二级配件类型" ? '' : $('#secondTypeName').find("option:selected").text(),
            partsName: new_partsName || '',
            brandName: brandName || '',
            partsType: partsType || '',
            unit: partsUnit || '',
            purchasePrice: purchasePrice || '',
            laborCost: laborCost || '',
            price: price || '',
            partsNum: partsNum || '',
            isThreshold: isThreshold || '',
            partsPeriod: partsPeriod || '',
            partsTime: partsTime || '',
            picUrl: '',
            status: addStatus || '',
            isUse: isUse || '',
            remark: remark || ''
        };
        var dataArr = [];
        for (var key in data) {
            dataArr.push(key + '=' + data[key]);
        }
        window.location.href = win.utils.services.exportPartsList + '?' + dataArr.join('&');
        // download(dataArr.join('&'));
    });
    // 导出模板
    $('#exportModul').on('click', function () {
        window.location.href = win.globalhtp + '/excel/配件库导入模板.xlsx';
    });

    // 删除上传图片
    $('#uploadImgBox').on('click', 'span', function () {
        $(this).parents('li').remove();
        var imgNode = $('#uploadImgBox img');
        for (var i = 0; i < imgNode.length; i++) {
            $('#uploadImgBox').find('img').eq(i).attr('layer-pid', i);
        }
    });

    function initSearch() {
        $('#firstTypeName').val('');
        $('#secondTypeName').val('');
        $('#addPartsName').val('');
        $('#partsUnit').val('');
        $('#partsPeriod').val('');
        $('#addStatus').val('');
        $('#isUse').val('');
    };

    var parts_manage = {
        //初始化
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            //判断是否有配件列表权限，此时为配件库的配件列表权限
            utils.isUserMenuIndexRight('partsManage_list', function (result) {
                if (result == 1) {
                    //initialize...
                    utils.clearSpaces();
                    obj.initList();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });

            $('#addRemark').on('focus', function () {
                flagInput = true;
            });

            // 配件数据限制判断
            $('#partsNum').on('blur', function () {
                var parts_num = $('#partsNum').val();
                if (parts_num <= 0 || parts_num > 10000) {
                    utils.showMessage('请输入正确的配件使用数量！');
                    $('#partsNum').val('');
                } else if (parts_num == 0) {
                    $('#partsNum').val(0);
                }
            });

            // 配件工期限制判断
            $('#uploadContainer #partsTime').on('blur', function () {
                var parts_time = $('#partsTime').val();
                if (parts_time < 0 || parts_time > 10000) {
                    utils.showMessage('请输入正确的配件工期！');
                    $('#partsTime').val('');
                } else if (parts_time == 0) {
                    $('#partsTime').val(0);
                }
            });

            $(document).keydown(function (event) {
                var e = window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if (flagInput == false) {
                        if (e.preventDefault) {
                            e.preventDefault();
                        } else {
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });

        },
        //初始化请求
        initList: function () {
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["操作", "配件编号", "一级配件类型id", "一级配件类型", "二级配件类型id", "二级配件类型", "配件名称", "配件品牌", "品牌型号", "配件单位", "采购价格", "人工费", "配件费", "配件数量限制", "是否可超过数量限制", "配件周期", "配件工期", "图片", "状态", "是否常用", "城市", "备注信息"],
                colModel: [
                    {
                        name: "options",
                        index: "options",
                        editable: false,
                        width: 160,
                        sortable: false,
                        formatter: tableOptionsBtn
                    },
                    {
                        name: "partsCode",
                        index: "partsCode",
                        editable: false,
                        width: 100,
                        sortable: false,
                    },
                    {
                        name: "firstTypeId",
                        index: "firstTypeId",
                        editable: false,
                        width: 100,
                        sortable: false,
                        hidden: true
                    },
                    {
                        name: "firstTypeName",
                        index: "firstTypeName",
                        editable: false,
                        width: 100,
                        sortable: false,
                    },
                    {
                        name: "secondTypeId",
                        index: "secondTypeId",
                        editable: false,
                        width: 100,
                        sortable: false,
                        hidden: true
                    },
                    {
                        name: "secondTypeName",
                        index: "secondTypeName",
                        editable: false,
                        width: 100,
                        sortable: false,
                    },
                    {
                        name: "partsName",
                        index: "partsName",
                        editable: false,
                        width: 100,
                        sortable: false,
                    },
                    {
                        name: "brandName",
                        index: "brandName",
                        editable: false,
                        width: 100,
                        sortable: false,
                    },
                    {
                        name: "partsType",
                        index: "partsType",
                        editable: false,
                        width: 100,
                        sortable: false,
                    },
                    {
                        name: "unitName",
                        index: "unitName",
                        editable: false,
                        width: 80,
                        sortable: false,
                    },
                    {
                        name: "purchasePrice",
                        index: "purchasePrice",
                        editable: false,
                        width: 80,
                        sortable: false,
                    },
                    {
                        name: "laborCost",
                        index: "laborCost",
                        editable: false,
                        width: 80,
                        sortable: false,
                    },
                    {
                        name: "price",
                        index: "price",
                        editable: false,
                        width: 80,
                        sortable: false,
                    },
                    {
                        name: "partsNum",
                        index: "partsNum",
                        editable: false,
                        width: 80,
                        sortable: false,
                    },
                    {
                        name: "isThreshold",
                        index: "isThreshold",
                        editable: false,
                        width: 120,
                        sortable: false,
                        formatter: function (cellvalue, options, rowObject) {
                            if (rowObject.isThreshold == 1) {
                                return '可超'
                            } else if (rowObject.isThreshold == 2) {
                                return '不可超'
                            } else {
                                return ''
                            }
                        }
                    },
                    {
                        name: "partsPeriod",
                        index: "partsPeriod",
                        editable: false,
                        width: 80,
                        sortable: false,
                    },
                    {
                        name: "partsTime",
                        index: "partsTime",
                        editable: false,
                        width: 80,
                        sortable: false,
                    },
                    {
                        name: "picUrl",
                        index: "picUrl",
                        editable: false,
                        width: 80,
                        sortable: false,
                        formatter: imgFormatter
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 60,
                        sortable: false,
                        formatter: statusText
                    },
                    {
                        name: "isUse",
                        index: "isUse",
                        editable: false,
                        width: 80,
                        sortable: false,
                        formatter: isUseText
                    },
                    {
                        name: "city",
                        index: "city",
                        editable: false,
                        width: 80,
                        sortable: false,
                    },
                    {
                        name: "remark",
                        index: "remark",
                        editable: false,
                        width: 180,
                        sortable: false,
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                shrinkToFit: false,
                autoScroll: true,
                // 翻页
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        data.position = nowPage;
                        data.rows = 20;
                        parts_manage.submitSearchData();
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    data.position = nowPage;
                    data.rows = 20;
                    parts_manage.submitSearchData();
                },
                //当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowindex = $("#" + this.id).getGridParam('selrow');
                    //返回点击这行xlmc的值
                    // selectedRowValue = $("#gridTable").jqGridRowValue("xlmc");
                },
            });

            parts_manage.getServerData(nowPage, data);

            function imgFormatter(cellvalue, options, rowObject) {
                if (rowObject.picUrl) {
                    return '<img src="' + rowObject.picUrl.split(',')[0] + '">';
                } else return ''
            }

            function tableOptionsBtn(cellvalue, options, rowObject) {
                return '<button type="button" class="btn btn-info opertion unified-btn-inline unified-change-btn-inline" id="btnUpdate" data-id="' + rowObject.id + '" style="margin-right: 20px">修改</button>' +
                    '<button type="button" class="btn btn-info opertion unified-btn-inline unified-deleted-btn-inline" id="btnDelete"  data-id="' + rowObject.id + '">删除</button>'
            }

            function periodText(cellvalue, options, rowObject) {
                if (rowObject.partsPeriod) {
                    return rowObject.partsPeriod + '天';
                } else return '';
            }

            function statusText(cellvalue, options, rowObject) {
                if (rowObject.status == 1) {
                    return '启用'
                } else if (rowObject.status == 2) {
                    return '停用'
                }
            }

            function isUseText(cellvalue, options, rowObject) {
                if (rowObject.isUse == 1) {
                    return '常用'
                } else if (rowObject.isUse == 2) {
                    return '备用'
                }
            }

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(win).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        getServerData: function (pageIndex, postData) {
            is_search = false;
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.queryParts, postData, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                        utils.showMessage('暂无数据！');
                    } else {
                        gridData = result.parts;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 查询
        search: function () {
            // initializeEditForm();
            parts_manage.searchItemStatus(); //弹层内容显示
            // 获取一级配件类型
            parts_manage.getFirstTypeName();
            parts_manage.getPartsUnit();//获取配件单位
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '<i class="fa fa-search">&nbsp;&nbsp;搜索配件</i>',
                content: $('#model'),
                btn: ['搜索', '取消'],
                success: function () {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);

                    flagInput = false;
                },
                yes: function (index, layero) {
                    nowPage = 1;
                    is_search = true;
                    parts_manage.submitSearchData();
                    layer.close(index);
                },
                btn2: function () {
                    // parts_manage.layerInit();
                    is_search = false;
                },
                cancel: function () {
                    // parts_manage.layerInit();
                    is_search = false;
                }
            });
        },
        // 搜索获取数据
        submitSearchData: function () {
            if (is_search) {
                var new_partsName = $('#addPartsName').val(); //配件名
                var brandName = $('#brandName').val(); //配件品牌
                var partsType = $('#partsType').val(); //品牌型号
                var partsUnit = $('#partsUnit').val(); //配件单位
                var purchasePrice = $('#purchasePrice').val(); //配件单价
                var laborCost = $('#laborCost').val(); //人工费
                var price = $('#price').val(); //配件费
                var partsNum = $('#partsNum').val(); //数量限制
                var isThreshold = $("input[name='threshold']:checked").val(); //是否可超过限制
                var partsPeriod = $('#partsPeriod').val(); //配件周期
                var partsTime = $('#partsTime').val(); //配件工期
                var addStatus = $('#addStatus').val(); //状态
                var isUse = $('#isUse').val(); //是否常用
                var remark = $('#addRemark').val() || null;
                var imgArr = [];
                var liNode = $('#uploadImgBox').find('li');
                for (var i = 0; i < liNode.length; i++) {
                    imgArr.push($(liNode[i]).find('img').attr('src'));
                }

                data.firstTypeId = $('#firstTypeName').val() || null;
                data.firstTypeName = $('#firstTypeName').val() ? $('#firstTypeName').find("option:selected").text() : null;
                data.secondTypeId = $('#secondTypeName').val() || null;
                data.secondTypeName = $('#secondTypeName').val() ? $('#secondTypeName').find("option:selected").text() : null;
                data.partsName = new_partsName || null;
                data.brandName = brandName || null;
                data.partsType = partsType || null;
                data.unit = partsUnit || null;
                data.purchasePrice = purchasePrice || null;
                data.laborCost = laborCost || null;
                data.price = price || null;
                data.partsNum = partsNum || null;
                data.isThreshold = isThreshold || null,
                    data.partsPeriod = partsPeriod || null;
                data.partsTime = partsTime || null;
                data.picUrl = null;
                data.status = addStatus || null;
                data.isUse = isUse || null;
                data.remark = remark || null;
                data.position = nowPage;
                data.rows = 20;
            }
            parts_manage.getServerData(nowPage, data);
        },
        // 弹层初始化
        layerInit: function () {
            $('#firstTypeName').html('<option value="">请选择一级配件类型</option>');
            $('#secondTypeName').html('<option value="">请选择二级配件类型</option>');
            $('#addPartsName').val(''); //配件名
            $('#brandName').val(''); //配件品牌
            $('#partsType').val(''); //品牌型号
            $('#partsUnit').val(''); //配件单位
            $('#purchasePrice').val(''); //配件单价
            $('#laborCost').val(''); //人工费
            $('#price').val(''); //配件费
            $('#partsNum').val(''); //配件数量限制

            // 是否可超过数量限制
            $("input[name=threshold][value='1']").prop("checked", false);
            $("input[name=threshold][value='2']").prop("checked", false);

            layui.use('form',function () {
                var form=layui.form;
                form.render('radio')
            });

            $('#partsPeriod').val(''); //配件周期
            $('#partsTime').val(''); //配件工期
            $('#addStatus').val(''); //状态
            $('#isUse').val(''); //是否常用
            $('#addRemark').val('');
            $('#uploadImgBox').html('');
        },
        // 查询弹层显示
        searchItemStatus: function () {
            parts_manage.layerInit();
            $('#model').find('.form-group').find('strong').hide();
            $('#model').find('.form-group').eq(0).show();
            $('#model').find('.form-group').eq(1).show();
            $('#model').find('.form-group').eq(2).show();
            $('#model').find('.form-group').eq(3).hide();
            $('#model').find('.form-group').eq(4).hide();
            $('#model').find('.form-group').eq(5).show();
            $('#model').find('.form-group').eq(6).hide();
            $('#model').find('.form-group').eq(7).hide();
            $('#model').find('.form-group').eq(8).hide();
            $('#model').find('.form-group').eq(9).show();
            $('#model').find('.form-group').eq(10).show();
            $('#model').find('.form-group').eq(11).show();
            $('#model').find('.form-group').eq(12).show();
            $('#model').find('.form-group').eq(13).hide();
            $('#model').find('.form-group').eq(14).show();
            $('#model').find('.form-group').eq(15).show();
            $('#model').find('.form-group').eq(16).hide();

            // 是否可超过数量限制
            $("input[name=isThreshold][value='是']").attr("checked", false);
            $("input[name=isThreshold][value='否']").attr("checked", false);
            form.render('radio');

        },
        // 新增 修改 弹层显示
        normalItemStatus: function () {
            parts_manage.layerInit();
            $('#model').find('strong').show();
            $('#model').find('.form-group').eq(0).show();
            $('#model').find('.form-group').eq(1).show();
            $('#model').find('.form-group').eq(2).show();
            $('#model').find('.form-group').eq(3).show();
            $('#model').find('.form-group').eq(4).show();
            $('#model').find('.form-group').eq(5).show();
            $('#model').find('.form-group').eq(6).show();
            $('#model').find('.form-group').eq(7).show();
            $('#model').find('.form-group').eq(8).show();
            $('#model').find('.form-group').eq(9).show();
            $('#model').find('.form-group').eq(10).show();
            $('#model').find('.form-group').eq(11).show();
            $('#model').find('.form-group').eq(12).show();
            $('#model').find('.form-group').eq(13).show();
            $('#model').find('.form-group').eq(14).show();
            $('#model').find('.form-group').eq(15).show();
            $('#model').find('.form-group').eq(16).show();
            $('#model').find('.form-group').eq(17).show();

            // 是否可超过数量限制
            $("input[name=isThreshold][value='是']").attr("checked", false);
            $("input[name=isThreshold][value='否']").attr("checked", true);
            form.render('radio');

        },
        // 刷新
        refresh: function () {
            initSearch();
            nowPage = 1;
            is_search = false;
            data = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                firstTypeId: null,
                firstTypeName: null,
                secondTypeId: null,
                secondTypeName: null,
                partsName: null,
                brandName: null,
                partsType: null,
                unit: null,
                purchasePrice: null,
                laborCost: null,
                price: null,
                partsNum: null,
                partsTime: null,
                isThreshold: null,
                partsPeriod: null,
                picUrl: null,
                status: null,
                isUse: null,
                remark: null,
                position: nowPage,
                rows: 20
            };
            parts_manage.getServerData(nowPage, data);
        },
        // 获取一级配件类型
        getFirstTypeName: function () {
            win.utils.ajaxPost(win.utils.services.queryPartsType, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 1,
                status: 1
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.configPartsTypes.length) {
                        var optionsStr = '<option value="">请选择一级配件类型</option>';
                        result.configPartsTypes.forEach(function (i, k) {
                            optionsStr += '<option value="' + i.firstTypeId + '">' + i.firstTypeName + '</option>';
                        });
                        $('#firstTypeName').html(optionsStr);
                        form.render('select');
                    }
                }
            })
        },
        // 获取二级配件类型
        getSecondTypeName: function (firstTypeId) {
            win.utils.ajaxPost(win.utils.services.queryPartsType, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 2,
                firstTypeId: firstTypeId,
                status: 1
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.configPartsTypes.length) {
                        var optionsStr = '<option value="">请选择二级配件类型</option>';
                        result.configPartsTypes.forEach(function (i, k) {
                            optionsStr += '<option value="' + i.secondTypeId + '">' + i.secondTypeName + '</option>';
                        });
                        $('#secondTypeName').html(optionsStr);
                        layui.use('form', function () {
                            var form = layui.form;
                            form.render('select');
                        });
                    }
                }
            })
        },
        // 获取配件单位
        getPartsUnit: function () {
            win.utils.ajaxPost(win.utils.services.queryPartsUnit, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
            }, function (result) {
                if (result.result_code == 200) {
                    var optionStr = '<option selected value="">请选择单位</option>';
                    result.configPartsUnits.forEach(function (i, k) {
                        optionStr += '<option  value="' + i.id + '">' + i.unitName + '</option>';
                    });
                    $('#partsUnit').html(optionStr);
                } else {
                    utils.showMessage('获取配件单位失败！');
                }
            })
        },
        loadLocalImage: function (fileObject, imageID, imageType) {
            if (typeof FileReader == 'undefined') {
                alert("当前浏览器不支持FileReader接口");
                return;
            }
            var files = fileObject.files;

            var imgs = $('#uploadImgBox').find('li');

            if ((imgs.length + files.length) > 40) {
                utils.showMessage('最多只能上传40张图片！');

                var filesArr = [];
                for (var i = 0; i < files.length; i++) {
                    filesArr.push(files[i]);
                }
                filesArr = filesArr.slice(0, 40 - imgs.length);
                if (!filesArr.length) {
                    utils.showMessage('图片数量已达上限！');
                    filesArr = [];
                    $('input[type="file"]').val("");
                }
                files = filesArr;
            }

            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var reader = new FileReader();//新建一个FileReader对象
                reader.readAsDataURL(file);//将读取的文件转换成base64格式
                reader.onload = function (e) {
                    // 压缩图片
                    var image = new Image();    //新建一个img标签（不嵌入DOM节点，仅做canvas操作)
                    image.src = e.target.result;    //让该标签加载base64格式的原图

                    image.onload = function () {    //图片加载完毕后再通过canvas压缩图片，否则图片还没加载完就压缩，结果图片是全黑的
                        let canvas = document.createElement('canvas'), //创建一个canvas元素
                            context = canvas.getContext('2d'),    //context相当于画笔，里面有各种可以进行绘图的API
                            imageWidth = image.width / 2,    //压缩后图片的宽度，这里设置为缩小一半
                            imageHeight = image.height / 2,    //压缩后图片的高度，这里设置为缩小一半
                            data = ''    //存储压缩后的图片
                        canvas.width = imageWidth;   //设置绘图的宽度
                        canvas.height = imageHeight;  //设置绘图的高度

                        //使用drawImage重新设置img标签中的图片大小，实现压缩。drawImage方法的参数可以自行查阅W3C
                        context.drawImage(image, 0, 0, imageWidth, imageHeight)

                        //使用toDataURL将canvas上的图片转换为base64格式
                        data = canvas.toDataURL('image/jpeg');

                        var blobURL = parts_manage.dataURItoBlob(data);

                        // 图片上传至七牛云
                        parts_manage.qiniuUpload(
                            blobURL,
                            // file.name,
                            'bjx-public-' + new Date().getTime() + (parseInt(Math.random() * 100 + 1)),
                            $('#uploadImgBox')
                        );
                    }
                }
            }
        },
        // base64转blob
        dataURItoBlob: function (base64Data) {
            var byteString;
            if (base64Data.split(',')[0].indexOf('base64') >= 0)
                byteString = atob(base64Data.split(',')[1]);
            else
                byteString = unescape(base64Data.split(',')[1]);
            var mimeString = base64Data.split(',')[0].split(':')[1].split(';')[0];
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ia], {
                type: mimeString
            });
        },
        /**七牛云上传图片
         * cardImageUrl-----需要上传图片的bolb地址
         * token-----token
         * fileName-----上传图片的原文件名
         * summernoteObj------显示图片的盒子
         * */
        qiniuUpload: function (cardImageUrl, fileName, nodeObj) {
            $.ajax({
                url: win.utils.services.getQiniuToken,
                type: 'get',
                dataType: 'json',
                success: function (res) {
                    var qiniuToken = res.token;
                    var observable = qiniu.upload(
                        cardImageUrl,
                        fileName,
                        // 'bjx_'+new Date().getTime()+(parseInt(Math.random()*100+1)),
                        qiniuToken,
                        {
                            fname: fileName,
                            params: {}, //用来放置自定义变量
                            mimeType: null
                        }, {
                            useCdnDomain: true,
                            // region: qiniu.region.z0
                            region: null
                        }
                    );
                    observable.subscribe({
                        complete(res) {
                            var qiniuImgUrl = 'http://images.baijiaxiu.com/' + res.key;
                            var imgNode = nodeObj.find('img');
                            nodeObj.append('<li>\n' +
                                '<img class="cardImage" style="margin-top: 10px; width: 100px; height: 100px; max-width: 400px; border: 10px solid #EEEEEE;" layer-pid="' + imgNode.length + '" layer-src="' + qiniuImgUrl + '" src=' + qiniuImgUrl + ' />\n' +
                                '<span class="layui-layer-setwin"><a class="layui-layer-ico layui-layer-close layui-layer-close1" href="javascript:;"></a></span>\n' +
                                '</li>');
                            utils.showMessage('图片上传成功！');
                            $('input[type="file"]').val("");
                            // imgArr.push(qiniuImgUrl);
                        }
                    });
                }
            });
        },

        // 导入功能
        importExcel: function (obj) {
            var formData = new FormData();
            var name = $("#articleImageFile").val();
            formData.append("file", $("#articleImageFile")[0].files[0]);
            // formData.append("name",name);//这个地方可以传递多个参数
            formData.append('user_id', win.utils.getCookie('user_id'));//这个地方可以传递多个参数
            formData.append('session', win.utils.getCookie('session'));//这个地方可以传递多个参数

            var loadIndex = '';
            $.ajax({
                url: win.utils.services.importPartsList,
                type: 'POST',
                async: true,
                data: formData,
                // 告诉jQuery不要去处理发送的数据
                processData: false,
                // 告诉jQuery不要去设置Content-Type请求头
                contentType: false,
                beforeSend: function () {
                    //loading层
                    loadIndex = layer.load(1, {
                        shade: [0.3, '#000'], //0.1透明度的白色背景
                        // time: 50000
                    });
                },
                success: function (result) {
                    layer.close(loadIndex);
                    if (result.result_code == 200) {
                        utils.showMessage('导入成功！');
                        parts_manage.refresh();
                        $('input[type="file"]').val("");
                    } else if (result.result_code == 999) {
                        utils.showMessage(result.description + '，请重试！');
                        $('input[type="file"]').val("");
                    } else {
                        utils.showMessage('导入失败，请重试！');
                        $('input[type="file"]').val("");
                    }
                },
                fail: function () {
                    layer.close(loadIndex);
                }
            });
        },

        // 输入框价格格式处理
        priceCheck: function (th, event) {
            var event = event || window.event;
            var code = event.keyCode;
            if (navigator.userAgent.indexOf("Firefox") > -1) {
                code = event.which;
            }
            if (code == 37 || code == 39) return;
            var regStrs = [
                ['^0(\\d+)$', '$1'], //禁止录入整数部分两位以上，但首位为0
                ['[^\\d\\.]+$', ''], //禁止录入任何非数字和点
                ['\\.(\\d?)\\.+', '.$1'], //禁止录入两个以上的点
                ['^(\\d+\\.\\d{2}).+', '$1'], //禁止录入小数点后两位以上
                ['^(\\.\\d+)', '$1']//禁止输入情况下小数点出现在首位
            ];
            for (i = 0; i < regStrs.length; i++) {
                var reg = new RegExp(regStrs[i][0]);
                th.value = th.value.replace(reg, regStrs[i][1]);

            }
        },
        overFormat: function (th) {
            // alert('111')
            var v = th.value;
            if (v === '') {
                v = '0.00';
            } else if (v === '0') {
                v = '0.00';
            } else if (v === '0.') {
                v = '0.00';
            } else if (/^0+\d+\.?\d*.*$/.test(v)) {
                v = v.replace(/^0+(\d+\.?\d*).*$/, '$1');
                v = inp.getRightPriceFormat(v).val;
            } else if (/^0\.\d$/.test(v)) {
                v = v + '0';
            } else if (!/^\d+\.\d{2}$/.test(v)) {
                if (/^\d+\.\d{2}.+/.test(v)) {
                    v = v.replace(/^(\d+\.\d{2}).*$/, '$1');
                } else if (/^\d+$/.test(v)) {
                    v = v + '.00';
                } else if (/^\d+\.$/.test(v)) {
                    v = v + '00';
                } else if (/^\d+\.\d$/.test(v)) {
                    v = v + '0';
                } else if (/^[^\d]+\d+\.?\d*$/.test(v)) {
                    v = v.replace(/^[^\d]+(\d+\.?\d*)$/, '$1');
                } else if (/\d+/.test(v)) {
                    v = v.replace(/^[^\d]*(\d+\.?\d*).*$/, '$1');
                    ty = false;
                } else if (/^0+\d+\.?\d*$/.test(v)) {
                    v = v.replace(/^0+(\d+\.?\d*)$/, '$1');
                    ty = false;
                } else {
                    v = '0.00';
                }
            }
            th.value = v;
        }
    };
    win.parts_manage = parts_manage
})(window, jQuery, qiniu);