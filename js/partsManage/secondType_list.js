/**
 * Created by hanlu
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    var nowPage = 1;

    var flagInput = false;
    var is_search = false;

    // 请求参数
    var data = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        firstTypeId: null,
        firstTypeName: null,
        inputName: null,
        status: null,
        rank: 2,
        rows: 20,
        position: nowPage
    };

    // 刷新
    $('#btnQuery').on('click', function () {
        parts_manage.refresh();
    });
    // 筛选
    $('#btnSearch').on('click', function () {
        $('#model').find('strong').hide();
        parts_manage.search();
    });

    // 增加
    $('#btnAdd').on('click', function () {
        parts_manage.layerInit();
        parts_manage.getFirstTypeName();
        $('#model').find('strong').show();
        var i = layer.open({
            type: 1,
            area: ['60%', '60%'],
            title: '<i class="fa fa-plus">&nbsp;&nbsp;添加二级配件类型</i>',
            content: $('#model'),
            btn: ['保存', '取消'],
            success: function (layero, index) {
                $('.layui-layer-page').css('zIndex', 333);
                $('.layui-layer-shade').css('zIndex', 300);

                flagInput = false
            },
            yes: function (index, layero) {
                var firstTypeId = $('#firstTypeName').val();
                var firstTypeName = $('#firstTypeName').find("option:selected").text();
                var new_partsName = $('#addPartsName').val(); //配件名
                var addStatus = $('#addStatus').val(); //状态

                if (firstTypeId == '') {
                    utils.showLayerMessage('请选择一级配件类型！');
                    return;
                }
                if (new_partsName == '') {
                    $('#addPartsName').focus();
                    utils.showLayerMessage('请输入配件名称！');
                    return;
                }
                if (addStatus == '') {
                    $('#addStatus').focus();
                    utils.showLayerMessage('请输入状态！');
                    return;
                }
                // 请求参数
                var addPostData = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    inputName: new_partsName,
                    status: addStatus,
                    firstTypeId: firstTypeId,
                    firstTypeName: firstTypeName,
                    rank: 2
                };

                win.utils.ajaxPost(win.utils.services.addPartsType, addPostData, function (result) {
                    if (result.result_code == 200) {
                        $('#table').jqGrid('addRowData', result.data.id, {
                            id: result.data.id,
                            secondTypeId: result.data.id,
                            secondTypeName: new_partsName,
                            status: addStatus,
                            firstTypeId: firstTypeId,
                            firstTypeName: firstTypeName,
                        });
                        // parts_manage.refresh();
                        layer.close(index);
                        parts_manage.layerInit();
                    } else if (result.result_code == 6601) {
                        utils.showMessage('该配件类型已存在！');
                    } else {
                        utils.showLayerMessage('创建失败，请重试！');
                    }
                });
            },
            btn2: function () {
                parts_manage.layerInit();
            },
            cancel: function () {
                parts_manage.layerInit();
            }
        });
    });
    // 修改
    $('#table').on('click', '#btnUpdate', function () {

        $('#model').find('strong').show();
        var id = $(this).attr('data-id');
        var rowData = $('#table').jqGrid('getRowData', id);

        // 获取修改数据信息
        win.utils.ajaxPost(win.utils.services.queryPartsType, {
            user_id: win.utils.getCookie('user_id'),
            session: win.utils.getCookie('session'),
            id: id,
            // firstTypeId:gridData[index-1].firstTypeId,
            rank: 2
        }, function (result) {
            if (result.result_code == 200) {
                // 获取一级配件类型数据
                win.utils.ajaxPost(win.utils.services.queryPartsType, {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    rank: 1
                }, function (result) {
                    var optionStr = '';
                    result.configPartsTypes.forEach(function (i, k) {
                        if (i.firstTypeId == rowData.firstTypeId) {
                            optionStr += '<option value="' + i.firstTypeId + '" selected>' + i.firstTypeName + '</option>'
                        } else {
                            optionStr += '<option value="' + i.firstTypeId + '">' + i.firstTypeName + '</option>'
                        }
                    });
                    $('#firstTypeName').html(optionStr);
                    form.render('select', 'firstTypeName');
                })
                $('#addPartsName').val(result.configPartsTypes[0].secondTypeName); //配件名
                $('#addStatus').val(result.configPartsTypes[0].status); //状态
            } else {
                utils.showMessage('获取数据失败，请重试！');
                return;
            }
        });

        var i = layer.open({
            type: 1,
            area: ['60%', '60%'],
            title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;修改二级配件类型信息',
            content: $('#model'),
            btn: ['保存', '取消'],
            success: function () {
                flagInput = false
            },
            yes: function (index, layero) {
                var firstTypeId = $('#firstTypeName').val();
                var firstTypeName = $('#firstTypeName').find("option:selected").text();
                var new_partsName = $('#addPartsName').val(); //配件名
                var addStatus = $('#addStatus').val(); //状态

                if (firstTypeId == '') {
                    utils.showLayerMessage('请选择一级配件类型！');
                    return;
                }
                if (new_partsName == '') {
                    $('#addPartsName').focus();
                    utils.showLayerMessage('请输入配件名称！');
                    return;
                }
                if (addStatus == '') {
                    $('#addStatus').focus();
                    utils.showLayerMessage('请输入状态！');
                    return;
                }
                // 请求参数
                var addPostData = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    firstTypeId: firstTypeId,
                    firstTypeName: firstTypeName,
                    inputName: new_partsName,
                    status: addStatus,
                    rank: 2,
                    id: id
                };

                win.utils.ajaxPost(win.utils.services.updatePartsType, addPostData, function (result) {
                    if (result.result_code == 200) {
                        utils.showMessage('修改成功！');

                        $('#table').jqGrid('setRowData', id, {
                            firstTypeId: firstTypeId,
                            firstTypeName: firstTypeName,
                            secondTypeName: new_partsName,
                            status: addStatus,
                        });
                        layer.close(index);
                        parts_manage.layerInit();
                    } else {
                        utils.showLayerMessage('修改失败，请重试！');
                    }
                });
            },
            btn2: function () {
                parts_manage.layerInit();
            },
            cancel: function () {
                parts_manage.layerInit();
            }
        });


    });
    // 删除
    $('#table').on('click', '#btnDelete', function () {
        var id = $(this).attr('data-id');
        var rowData = $('#table').jqGrid('getRowData', id);
        utils.showConfirm('您确定要删除这条记录吗？', '删除', function () {
            win.utils.ajaxPost(win.utils.services.deletePartsType, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                id: id,
                rank: 2
            }, function (result) {
                if (result.result_code == 200) {
                    utils.showMessage('删除成功！');
                    $('#table').jqGrid('delRowData', id);
                }
            })
        });
    });

    // 导出功能
    $('#export').on('click', function () {
        var firstTypeId = $('#firstTypeName').val();
        var firstTypeName = $('#firstTypeName').find("option:selected").text();
        var new_partsName = $('#addPartsName').val(); //一级配件类型名
        var addStatus = $('#addStatus').val(); //状态
        // 请求参数
        data = {
            user_id: win.utils.getCookie('user_id'),
            session: win.utils.getCookie('session'),
            firstTypeId: firstTypeId || '',
            firstTypeName: firstTypeName || '',
            inputName: new_partsName || '',
            status: addStatus || '',
            rank: 2
        };
        var dataArr = [];
        for (var key in data) {
            dataArr.push(key + '=' + data[key]);
        }
        window.location.href = win.utils.services.exportPartsTypeList + '?' + dataArr.join('&');
        // download(dataArr.join('&'));
    });

    function initSearch() {
        $("#balance").val("");
        $("#MainPlan").val("");
        $("#MainResult").val("");
        $("#relation").val("");
        $("#overtimeRecord").val("");
        //more-clear
        $("#orderTime").val("");
        $("#estiComTime").val("");
        $("#finishTime").val("");
        $("#createTime").val("");
        $("#getOrderTime").val("");
        $("#sendOrder").val("");
        $("#toDoorTime").val("");
        $("#city").val("");
        $("#tradingArea").val("");
        $("#orderId").val("");
        $("#apartmentId").val("");
        $("#apartmentAdress").val("");
        $("#comType").val("");
        $("#teamName").val("");
        $("#workSupply").val("");
        $("#MainType").val("");
        $("#MainOrderWay").val("");
        $("#end").val("");
        $("#relaSta").val("");
        $("#repairman").val("");
        $("#transferOrderId").val("");
    };

    var parts_manage = {
        //初始化
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            //判断是否有二级配件类型权限，此时为配件库管理的二级配件类型权限
            utils.isUserMenuIndexRight('secondType_list', function (result) {
                if (result == 1) {
                    //initialize...
                    win.utils.clearSpaces();
                    obj.initList();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });

            //刷新
            $('#empty').on('click', function () {
                initSearch();
                obj.refreshServerData();
            });


            $(document).keydown(function (event) {
                var e = window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if (flagInput == false) {
                        if (e.preventDefault) {
                            e.preventDefault();
                        } else {
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });

            // parts_manage.getServerData(nowPage,data);
        },
        //初始化请求
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),//utils.getAutoGridHeightSong(),
                autowidth: true,
                shrinkToFit: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["操作", "一级配件类型id", "一级配件类型", "二级配件类型id", "二级配件分类", "状态"],
                colModel: [
                    {
                        name: "options",
                        index: "options",
                        editable: false,
                        width: 40,
                        sortable: false,
                        formatter: tableOptionsBtn
                    },
                    {
                        name: "firstTypeId",
                        index: "firstTypeId",
                        editable: false,
                        width: 60,
                        sortable: false,
                        hidden: true
                        // formatter:statusText
                    },
                    {
                        name: "firstTypeName",
                        index: "firstTypeName",
                        editable: false,
                        width: 60,
                        sortable: false,
                        // formatter:statusText
                    },
                    {
                        name: "secondTypeId",
                        index: "secondTypeId",
                        editable: false,
                        width: 60,
                        sortable: false,
                        hidden: true
                        // formatter:statusText
                    },
                    {
                        name: "secondTypeName",
                        index: "secondTypeName",
                        editable: false,
                        width: 60,
                        sortable: false,
                        // formatter:statusText
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 60,
                        sortable: false,
                        formatter: statusText
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                // 翻页
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        data.position = currentPage;
                        data.rows = 20;
                        parts_manage.searchSubmitData();
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    data.position = nowPage;
                    data.rows = 20;
                    parts_manage.searchSubmitData();
                },
                //当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowindex = $("#" + this.id).getGridParam('selrow');
                    //返回点击这行xlmc的值
                    // selectedRowValue = $("#gridTable").jqGridRowValue("xlmc");
                },
            });


            function tableOptionsBtn(cellvalue, options, rowObject) {
                return '<button type="button" class="btn btn-info opertion  unified-btn-inline unified-change-btn-inline" id="btnUpdate" data-id="' + rowObject.secondTypeId + '" style="margin-right: 20px">修改</button>'
                // '<button type="button" class="btn btn-info opertion unified-btn-inline unified-deleted-btn-inline" id="btnDelete" data-id="'+rowObject.secondTypeId+'">删除</button>'
            }

            function statusText(cellvalue, options, rowObject) {
                if (rowObject.status == 1) {
                    return '启用'
                } else if (rowObject.status == 2) {
                    return '停用'
                }
            }

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(win).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });

            // // 请求参数
            // var data = {
            //     user_id: win.utils.getCookie('user_id'),
            //     session: win.utils.getCookie('session'),
            //     rank:2,
            //     position: nowPage,
            //     rows: 20
            // };
            parts_manage.getServerData(nowPage, data);
        },
        getServerData: function (pageIndex, postData) {
            is_search = false;
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.queryPartsType, postData, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.configPartsTypes;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 查询
        search: function () {
            // parts_manage.refresh();
            parts_manage.getFirstTypeName();
            var i = layer.open({
                type: 1,
                area: ['60%', '60%'],
                title: '<i class="fa fa-search">&nbsp;&nbsp;搜索二级配件类型</i>',
                content: $('#model'),
                btn: ['搜索', '取消'],
                success: function () {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);

                    flagInput = false;
                },
                yes: function (index, layero) {
                    nowPage = 1;
                    is_search = true;
                    parts_manage.searchSubmitData();

                    layer.close(index);
                },
                btn2: function () {
                    is_search = false;
                },
                cancel: function () {
                    is_search = false;
                }
            });
        },
        searchSubmitData: function () {
            if (is_search) {
                var firstTypeId = $('#firstTypeName').val();
                var firstTypeName = $('#firstTypeName').find("option:selected").text();
                var new_partsName = $('#addPartsName').val(); //一级配件类型名
                var addStatus = $('#addStatus').val(); //状态

                data.firstTypeId = firstTypeId || null;
                data.firstTypeName = firstTypeName || null;
                data.status = addStatus || null;
                data.inputName = new_partsName || null;
                data.position = nowPage;
            }

            // // 请求参数
            // data = {
            //     user_id: win.utils.getCookie('user_id'),
            //     session: win.utils.getCookie('session'),
            //     firstTypeId:firstTypeId||null,
            //     firstTypeName:firstTypeName||null,
            //     inputName :new_partsName || null,
            //     status:addStatus || null,
            //     rank:2,
            //     rows:20,
            //     position:nowPage
            // };
            parts_manage.getServerData(nowPage, data);
        },
        // 弹层初始化
        layerInit: function () {
            $('#firstTypeName').val('');//一级配件名称
            $('#addPartsName').val(''); //配件名
            $('#addStatus').val(''); //状态
        },
        // 刷新
        refresh: function () {
            parts_manage.layerInit();
            nowPage = 1;
            is_search = false;
            data = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                firstTypeId: null,
                firstTypeName: null,
                inputName: null,
                status: null,
                rank: 2,
                rows: 20,
                position: nowPage
            };
            parts_manage.getServerData(nowPage, data);
        },
        // 获取一级配件类型名称
        getFirstTypeName: function () {
            win.utils.ajaxPost(win.utils.services.queryPartsType, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 1,
                status: 1
            }, function (result) {
                var optionStr = '<option value="">请选择一级配件类型</option>';
                result.configPartsTypes.forEach(function (i, k) {
                    optionStr += '<option value="' + i.firstTypeId + '">' + i.firstTypeName + '</option>'
                });
                $('#firstTypeName').html(optionStr);
                form.render('select', 'firstTypeName');
            })
        }
    };
    win.parts_manage = parts_manage
})(window, jQuery);