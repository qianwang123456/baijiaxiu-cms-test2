/**
 * Created by hanlu
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    var nowPage = 1;

    var flagInput = false;
    var is_search = false;

    // 请求参数
    var postData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        rank: 1,
        position: nowPage,
        rows: 20,
        inputName: null,
        status: null
    };


    // 刷新
    $('#btnQuery').on('click', function () {
        parts_manage.refresh();
    });
    // 筛选
    $('#btnSearch').on('click', function () {
        $('#model').find('strong').hide();
        parts_manage.search();
    });
    // $('.submit').on('click', function () {
    //     data = {
    //         user_id: win.utils.getCookie('user_id'),
    //         session: win.utils.getCookie('session'),
    //         id: null,
    //         laborCost: null, //人工费
    //         partsCode: document.selected.partsCode.value || null, //配件编码
    //         partsName: document.selected.partsName.value || null, //配件名称
    //         partsType: null, //配件型号
    //         picUrl: null, //图片URL
    //         price: null, //配件费
    //         remark: null, //备注
    //         status: document.selected.status.value || null, //状态
    //         unit: null, //单位
    //         position: nowPage,
    //         rows: 20
    //     };
    //     parts_manage.getServerData(nowPage, data);
    // });

    // 增加
    $('#btnAdd').on('click', function () {
        parts_manage.layerInit();
        $('#model').find('strong').show();
        var i = layer.open({
            type: 1,
            area: ['60%', '60%'],
            title: '<i class="fa fa-plus">&nbsp;&nbsp;添加一级配件类型</i>',
            content: $('#model'),
            btn: ['保存', '取消'],
            success: function (layero, index) {
                $('.layui-layer-page').css('zIndex', 333);
                $('.layui-layer-shade').css('zIndex', 300);

                flagInput = false;
            },
            yes: function (index, layero) {
                var new_partsName = $('#addPartsName').val(); //配件名
                var addStatus = $('#addStatus').val(); //状态

                if (new_partsName == '') {
                    $('#addPartsName').focus();
                    utils.showLayerMessage('请输入配件名称！');
                    return;
                }
                if (addStatus == '') {
                    $('#addStatus').focus();
                    utils.showLayerMessage('请输入状态！');
                    return;
                }
                // 请求参数
                var addPostData = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    inputName: new_partsName,
                    status: addStatus,
                    rank: 1
                };

                win.utils.ajaxPost(win.utils.services.addPartsType, addPostData, function (result) {
                    if (result.result_code == 200) {
                        utils.showMessage('新增成功！');
                        $('#table').jqGrid('addRowData', result.data.id, {
                            id: result.data.id,
                            firstTypeId: result.data.id,
                            firstTypeName: new_partsName,
                            status: addStatus
                        });
                        // $("#table").setSelection(result.result_key, true);
                        // parts_manage.refresh();
                        layer.close(index);
                        parts_manage.layerInit();
                    } else if (result.result_code == 6601) {
                        utils.showMessage('该配件类型已存在！');
                    } else {
                        utils.showMessage('创建失败，请重试！');
                    }
                });
            },
            btn2: function () {
                parts_manage.layerInit();
            },
            cancel: function () {
                parts_manage.layerInit();
            }
        });
    });
    // 修改
    $('#table').on('click', '#btnUpdate', function () {
        $('#model').find('strong').show();

        var id = $(this).attr('data-id');

        // 获取修改数据信息
        win.utils.ajaxPost(win.utils.services.queryPartsType, {
            user_id: win.utils.getCookie('user_id'),
            session: win.utils.getCookie('session'),
            id: id,
            rank: 1
        }, function (result) {
            if (result.result_code == 200) {
                $('#addPartsName').val(result.configPartsTypes[0].firstTypeName); //配件名
                $('#addStatus').val(result.configPartsTypes[0].status); //状态
            } else {
                utils.showMessage('获取数据失败，请重试！');
                return;
            }
        });

        var i = layer.open({
            type: 1,
            area: ['60%', '60%'],
            title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;修改一级配件类型信息',
            content: $('#model'),
            btn: ['保存', '取消'],
            success: function () {
                flagInput = false;
            },
            yes: function (index, layero) {
                var data = $("#table").jqGrid('getGridParam', 'selrow');

                var new_partsName = $('#addPartsName').val(); //配件名
                var addStatus = $('#addStatus').val(); //状态

                if (new_partsName == '') {
                    $('#addPartsName').focus();
                    utils.showLayerMessage('请输入配件名称！');
                    return;
                }
                if (addStatus == '') {
                    $('#addStatus').focus();
                    utils.showLayerMessage('请输入状态！');
                    return;
                }
                // 请求参数
                var addPostData = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    inputName: new_partsName,
                    status: addStatus,
                    rank: 1,
                    id: id
                };

                win.utils.ajaxPost(win.utils.services.updatePartsType, addPostData, function (result) {
                    if (result.result_code == 200) {
                        utils.showMessage('修改成功！');
                        $('#table').jqGrid('setRowData', data, {
                            firstTypeName: new_partsName,
                            status: addStatus
                        });
                        layer.close(index);
                        parts_manage.layerInit();
                    } else {
                        utils.showLayerMessage('修改失败，请重试！');
                    }
                });
            },
            btn2: function () {
                parts_manage.layerInit();
            },
            cancel: function () {
                parts_manage.layerInit();
            }
        });


    });
    // 删除
    $('#table').on('click', '#btnDelete', function () {
        var id = $(this).attr('data-id');
        utils.showConfirm('您确定要删除这条记录吗？', '删除', function () {
            win.utils.ajaxPost(win.utils.services.deletePartsType, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                id: id,
                rank: 1
            }, function (result) {
                var data = $("#table").jqGrid('getGridParam', 'selrow');
                if (result.result_code == 200) {
                    utils.showMessage('删除成功！');
                    $('#table').jqGrid('delRowData', data);
                } else if (result.result_code == 601) {
                    utils.showMessage('有二级配件类型关联，请修改后再删除！');
                } else {
                    utils.showMessage('删除失败，请重试！');
                }
            })
        });
    });

    // 导出功能
    $('#export').on('click', function () {
        var new_partsName = $('#addPartsName').val(); //一级配件类型名
        var addStatus = $('#addStatus').val(); //状态
        // 请求参数
        data = {
            user_id: win.utils.getCookie('user_id'),
            session: win.utils.getCookie('session'),
            inputName: new_partsName || '',
            status: addStatus || '',
            rank: 1
        };
        var dataArr = [];
        for (var key in data) {
            dataArr.push(key + '=' + data[key]);
        }
        window.location.href = win.utils.services.exportPartsTypeList + '?' + dataArr.join('&');
        // download(dataArr.join('&'));
    });

    function initSearch() {
        $('#addPartsName').val('');
        $('#addStatus').val('');
    };

    var parts_manage = {
        //初始化
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            //判断是否有一级配件类型权限，此时为配件库管理的一级配件类型权限
            utils.isUserMenuIndexRight('firstType_list', function (result) {
                if (result == 1) {
                    //initialize...
                    win.utils.clearSpaces();
                    obj.initList();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });

            //刷新
            $('#empty').on('click', function () {
                initSearch();
                obj.refreshServerData();
            });


            $(document).keydown(function (event) {
                var e = window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if (flagInput == false) {
                        if (e.preventDefault) {
                            e.preventDefault();
                        } else {
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });
        },
        //初始化请求
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),//utils.getAutoGridHeightSong(),
                autowidth: true,
                shrinkToFit: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["操作", "一级配件类型", "状态"],
                colModel: [
                    {
                        name: "options",
                        index: "options",
                        editable: false,
                        width: 30,
                        sortable: false,
                        formatter: tableOptionsBtn
                    },
                    {
                        name: "firstTypeName",
                        index: "firstTypeName",
                        editable: false,
                        width: 60,
                        sortable: false,
                        // formatter:statusText
                    },
                    {
                        name: "status",
                        index: "status",
                        editable: false,
                        width: 60,
                        sortable: false,
                        formatter: statusText
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                // 翻页
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        postData.position = nowPage;
                        postData.rows = 20;
                        parts_manage.searchSubmitData();
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    postData.position = nowPage;
                    postData.rows = 20;
                    parts_manage.searchSubmitData();
                },
                //当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowindex = $("#" + this.id).getGridParam('selrow');
                    //返回点击这行xlmc的值
                    // selectedRowValue = $("#gridTable").jqGridRowValue("xlmc");
                },
            });

            function tableOptionsBtn(cellvalue, options, rowObject) {
                return '<button type="button" class="btn btn-info opertion unified-btn-inline unified-change-btn-inline" id="btnUpdate" data-id="' + rowObject.id + '" style="margin-right: 20px">修改</button>'
                // '<button type="button" class="btn btn-info opertion unified-btn-inline unified-deleted-btn-inline" id="btnDelete" data-id="'+rowObject.id+'">删除</button>'
            }

            function statusText(cellvalue, options, rowObject) {
                if (rowObject.status == 1) {
                    return '启用'
                } else if (rowObject.status == 2) {
                    return '停用'
                }
            }

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(win).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });

            parts_manage.getServerData(nowPage, postData);
        },
        getServerData: function (pageIndex, postData) {
            is_search = false;
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.queryPartsType, postData, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                        utils.showMessage('暂无数据！');
                    } else {
                        gridData = result.configPartsTypes;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 查询
        search: function () {
            parts_manage.refresh();
            var i = layer.open({
                type: 1,
                area: ['60%', '60%'],
                title: '<i class="fa fa-search">&nbsp;&nbsp;搜索一级配件类型</i>',
                content: $('#model'),
                btn: ['搜索', '取消'],
                success: function () {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);

                    flagInput = false;
                },
                yes: function (index, layero) {
                    nowPage = 1;
                    is_search = true;
                    parts_manage.searchSubmitData();
                    layer.close(index);
                },
                btn2: function () {
                    is_search = false;
                },
                cancel: function () {
                    is_search = false;
                }
            });
        },
        // 搜索数据
        searchSubmitData: function () {
            if (is_search) {
                var new_partsName = $('#addPartsName').val(); //一级配件类型名
                var addStatus = $('#addStatus').val(); //状态

                postData.inputName = new_partsName || null;
                postData.status = addStatus || null;
                postData.position = nowPage;
            }
            parts_manage.getServerData(nowPage, postData);
        },
        // 弹层初始化
        layerInit: function () {
            $('#addPartsName').val(''); //配件名
            $('#addStatus').val(''); //状态
        },
        // 刷新
        refresh: function () {
            initSearch();
            nowPage = 1;
            is_search = false;
            postData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 1,
                position: nowPage,
                rows: 20,
                inputName: null,
                status: null
            };
            parts_manage.getServerData(nowPage, postData);
        },
    };
    win.parts_manage = parts_manage
})(window, jQuery);