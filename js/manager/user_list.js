/**
 * Created by yimao on 2016-06-24.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    var is_search = false;
    var nowPage = 1;
    var flagInput = false;

    var data = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        batch: nowPage,
        rows: 20,
        userName: null,
        userId: null,
        order: orderByString
    };

    function initializeEditForm() {
        $('#txtUserID').val('');
        $('#txtUserName').val('');
        $('#txtUserPhone').val('');
        $('#userName').val('');
        $('#userNumber').val('');
        $('#userPhone').val('');
        $('#chkStopFlag').iCheck('uncheck');
    }

    var user_list = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('system_user', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });

            win.vaildation.init("#form");
            win.vaildation.init("#form_password");

            $('#table').on('click', '#btnUpdate', function () {
                var id = $(this).attr('data-id');
                obj.updateRecord(id);
            });
            $('#table').on('click', '#btnDel', function () {
                var id = $(this).attr('data-id');
                obj.deleteRecord(id);
            });
            $('#table').on('click', '#btnPassword', function () {
                var id = $(this).attr('data-id');
                obj.updateUserPassword(id);
            });

            $('#btnQuery').on('click', function () {
                nowPage = 1;
                data = {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    batch: nowPage,
                    rows: 20,
                    userName: null,
                    userId: null,
                    order: orderByString
                };
                obj.getServerData(1);
            });
            $('#btnAdd').click(obj.addRecord);
            $('#btnSearch').click(function () {
                nowPage = 1;
                is_search = true;
                obj.search();
            });
        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: false,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["操作", "用户帐号", "用户名称", "用户手机号", "停用标识"],
                colModel: [
                    {
                        name: "option",
                        index: "option",
                        editable: false,
                        width: 480,
                        formatter: function (cellvalue, options, rowObject) {
                            return '<button id="btnUpdate" class="btn btn-info unified-btn-inline unified-change-btn-inline" type="button" data-id="' + rowObject.id + '" style="margin-right: 10px;">修改</button>\n' +
                                '                <button id="btnPassword" class="btn btn-warning unified-btn-inline unified-change-btn-inline" data-id="' + rowObject.id + '" type="button" style="margin-right: 10px;">修改密码</button>' +
                                '                <button id="btnDel" class="btn btn-danger unified-btn-inline unified-deleted-btn-inline"  data-id="' + rowObject.id + '" type="button">删除</button>\n'
                        }
                    },
                    {name: "id", index: "user_id", editable: false, width: 190, sorttype: "string"},
                    {name: "name", index: "user_name", editable: false, width: 190, sorttype: "string"},
                    {name: "phone", index: "user_phone", editable: false, width: 240, sorttype: "string"},
                    {
                        name: "stop",
                        index: "stop_flag",
                        editable: false,
                        width: 190,
                        sortable: false,
                        formatter: function (cellvalue, options, rowObject) {
                            if (cellvalue == 0) return '正常'; else return '已停用';
                        }
                    }
                ],
                pager: "#table_pager",
                viewrecords: true,
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        // user_list.getServerData(currentPage);
                        data.batch = nowPage;
                        user_list.searchSubmitData();
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        // user_list.getServerData(currentPage);
                        user_list.searchSubmitData();
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    user_list.getServerData(currentPage);
                }
            });

            user_list.getServerData(1);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            var b = $(".jqGrid_wrapper").width();
            $("#table").setGridWidth(b);
            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        getServerData: function (pageIndex) {
            is_search = false;

            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            data.rows = pageCount;

            win.utils.ajax(win.utils.services.user_list, data, function (result) {
                if (result.result_code == 200) {
                    if (result.users.total == 0) {
                        gridData = {};
                        var gridJson = {total: 0, rows: gridData, page: 0, records: result.users.total};
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {pgtext: pageText});
                        $("#table")[0].addJSONData(gridJson);
                        utils.showMessage('暂无数据');
                    } else {
                        gridData = result.users.rows;
                        totalPages = Math.ceil(result.users.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.users.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {pgtext: pageText});
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        addRecord: function () {
            initializeEditForm();
            $('#txtUserID').attr("disabled", false);
            var i = layer.open({
                type: 1,
                area: '480px',
                title: '<i class="fa fa-plus">&nbsp;&nbsp;添加用户</i>',
                content: $('#pop_model'),
                btn: ['保存', '取消'],
                yes: function (index, layero) {
                    var set_user_ID = $('#txtUserID').val();
                    var user_name = $('#txtUserName').val();
                    var user_phone = $('#txtUserPhone').val();
                    var stop_flag = 0;
                    if ($('#chkStopFlag').prop('checked')) {
                        stop_flag = 1;
                    }

                    if (!$('#form').valid()) {
                        if (set_user_ID == '') {
                            $('#txtUserID').focus();
                            return;
                        }
                        if (user_name == '') {
                            $('#txtUserName').focus();
                            return;
                        }
                        if (user_phone == '') {
                            $('#txtUserPhone').focus();
                            return;
                        }
                        return;
                    }

                    win.utils.ajax(win.utils.services.user_save, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        set_user_id: set_user_ID,
                        user_name: user_name,
                        user_phone: user_phone,
                        stop_flag: stop_flag
                    }, function (result) {
                        if (result.result_code == 200) {
                            $('#table').jqGrid('addRowData', result.result_key, {
                                id: result.result_key,
                                name: user_name,
                                stop: stop_flag,
                                phone: user_phone
                            });
                            $("#table").setSelection(result.result_key, true);
                            layer.close(i);
                        } else {
                            utils.showLayerMessage('保存失败，请检查用户账号或用户名称是否重复！');
                        }
                    });
                }
            });
        },
        // 修改
        updateRecord: function (data) {
            initializeEditForm();
            win.utils.ajax(win.utils.services.user_detail, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                set_user_id: data
            }, function (result) {
                if (result.result_code == 200) {
                    $('#txtUserID').val(result.user.id);
                    $('#txtUserID').attr("disabled", true);
                    $('#txtUserName').val(result.user.name);
                    $('#txtUserPhone').val(result.user.phone);
                    if (result.user.stop == 1) {
                        $('#chkStopFlag').iCheck('check');
                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                    return;
                }
            });

            var i = layer.open({
                type: 1,
                area: '480px',
                title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;修改用户',
                content: $('#pop_model'),
                btn: ['保存', '取消'],
                yes: function (index, layero) {
                    var user_name = $('#txtUserName').val();
                    var user_phone = $('#txtUserPhone').val();
                    var stop_flag = 0;
                    if ($('#chkStopFlag').prop('checked')) {
                        stop_flag = 1;
                    }

                    if (!$('#form').valid()) {
                        if (user_name == '') {
                            $('#txtUserName').focus();
                            return;
                        }
                        if (user_phone == '') {
                            $('#txtUserPhone').focus();
                            return;
                        }
                        return;
                    }

                    var set_user_id = $('#txtUserID').val();
                    win.utils.ajax(win.utils.services.user_update, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        set_user_id: set_user_id,
                        user_name: user_name,
                        user_phone: user_phone,
                        stop_flag: stop_flag
                    }, function (result) {
                        if (result.result_code == 200) {
                            $('#table').jqGrid('setRowData', set_user_id, {
                                name: user_name,
                                stop: stop_flag,
                                phone: user_phone
                            });
                            layer.close(i);
                        } else {
                            utils.showLayerMessage('保存失败，请检查用户账号或用户名称是否重复！');
                        }
                    });

                }
            });
        },
        // 删除
        deleteRecord: function (data) {
            // var data = $("#table").jqGrid('getGridParam','selrow');
            // if (data == null){
            //     utils.showMessage('请选择要进行删除的记录！');
            //     return;
            // }
            utils.showConfirm('您确定要删除这条记录吗？', '删除', function () {
                win.utils.ajax(win.utils.services.user_delete, {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    set_user_id: data
                }, function (result) {
                    if (result.result_code == 200) {
                        utils.showMessage('删除成功！');
                        utils.closeMessage();
                        $('#table').jqGrid('delRowData', data);
                    } else {
                        utils.showMessage('删除失败，此用户已被使用！');
                    }
                });
            });
        },
        // 修改密码
        updateUserPassword: function (data) {
            // var data = $("#table").jqGrid('getGridParam','selrow');
            // if (data == null){
            //     utils.showMessage('请选择要重置密码的用户！');
            //     return;
            // }
            var i = layer.open({
                type: 1,
                area: '480px',
                title: '<i class="fa fa-plus">&nbsp;&nbsp;重置密码</i>',
                content: $('#pop_password'),
                btn: ['保存', '取消'],
                yes: function (index, layero) {
                    var newPassword = $('#txtNewPassword').val();
                    var newPasswordAgain = $('#txtNewPasswordAgain').val();

                    if (!$('#form_password').valid()) {
                        if (newPassword == '') {
                            $('#txtNewPassword').focus();
                            return;
                        }
                        if (newPasswordAgain == '') {
                            $('#txtNewPasswordAgain').focus();
                            return;
                        }
                        return;
                    }

                    if (newPassword != newPasswordAgain) {
                        utils.showLayerMessage('两次输入的密码不一致！');
                        $('#txtNewPassword').focus();
                        return;
                    }

                    win.utils.ajax(win.utils.services.user_update_password, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        set_user_id: data,
                        password: newPassword
                    }, function (result) {
                        if (result.result_code == 200) {
                            layer.close(i);
                            utils.showMessage('重置密码成功！');
                        } else {
                            utils.showLayerMessage('重置密码失败，请重试！');
                        }
                    });
                }
            });
        },
        // 查询
        search: function () {
            initializeEditForm();
            var i = layer.open({
                type: 1,
                area: ['60%', '60%'],
                title: '<i class="fa fa-search">&nbsp;&nbsp;搜索用户</i>',
                content: $('#model'),
                btn: ['搜索', '取消'],
                success: function () {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);

                    flagInput = false;
                },
                yes: function (index, layero) {
                    nowPage = 1;
                    is_search = true;
                    user_list.searchSubmitData();

                    layer.close(index);
                },
                btn2: function () {
                    is_search = false;
                },
                cancel: function () {
                    is_search = false;
                }
            });
        },
        searchSubmitData: function () {
            if (is_search) {
                var userName = $('#userName').val();
                var userNumber = $('#userNumber').val();
                var userPhone = $('#userPhone').val();

                data.userName = userName || null;
                data.userId = userNumber || null;
                data.user_phone = userPhone || null;
                data.batch = nowPage;
                user_list.getServerData(nowPage);
                return;
            }

            user_list.getServerData(nowPage);
        },
    };

    win.user_list = user_list;
})(window, jQuery);