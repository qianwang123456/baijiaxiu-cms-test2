/**
 * Created by song.
 */
(function(win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题

    var deferLoadData = 0;

    var flagInput=false;//空格输入按钮
    var ConFlag=-1;//弹层关闭

    var  nowCurPage= 0;//订单列表---当前页码数
    var  totalNowPages = 0;//订单列表---总的页码数

    var userArr = [];//用户选择数组

    // 等级：1调度组长、2协调组长、3调度组员、4协调组员
    
    var role_level_permission = {
        //初始化
        init: function() {
            var obj = this;
            if(utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            //判断是否有角色等级权限，此时为系统管理的角色等级权限
            utils.isUserMenuIndexRight('role_level_permission', function(result) {
                if(result == 1) {
                    //initialize...
                    obj.initList();
                    win.utils.clearSpacesTextarea();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });

            // 搜索
            $('#search').on('click',function(){
                role_level_permission.getServerData(1);
            });

            //搜搜-more
            $('#setting').on('click',function(){
                if($("#table input:checked").length>0){
                    $("input[name='levelPos']").eq(0).prop("checked", false);
                    $("input[name='levelPos']").eq(1).prop("checked", false);
                    $("input[name='levelPos']").eq(2).prop("checked", false);
                    $("input[name='levelPos']").eq(3).prop("checked", false);
                    role_level_permission.SearchConss();
                }else{
                    return;
                }
            });
        },
        //初始化请求
        initList: function() {
            //订单列表
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height:'auto',//utils.getAutoGridHeightSong(),
                autowidth: true,
                shrinkToFit:false,
                autoScroll: true,
                rowNum: 10,
                rownumbers: true,
                rownumWidth:80,
                // rowList: [10,20,50,100],
                pgtext: "第X页 / 共X页",
                colNames: ["工单ID",'选择','全部用户',"角色","等级","操作人","操作时间"],
                colModel: [
                    {
                        name: "userId",
                        index: "userId",
                        editable: false,
                        width: 240,
                        hidden:true,
                        sorttype: "string",
                        sortable: false
                    },
                    {
                        name: "userId",
                        index: "userId",
                        editable: false,
                        width: 60,
                        sortable: false,
                        formatter: function(cellvalue,options, rowObject) {
                            let str='<input type="checkbox" onclick="role_level_permission.settingPow()"  name="'+rowObject.userId+'">';
                            return str;
                        },
                    },
                    {
                        name: "userName",
                        index: "userName",
                        editable: false,
                        sortable: false,
                        width: 160,
                    },
                    {
                        name: "roleName",
                        index: "roleName",
                        editable: false,
                        sortable: false,
                        width: 180,
                        sorttype: "string",
                    },
                    {
                        name: "userClass",
                        index: "userClass",
                        editable: false,
                        sortable: false,
                        width: 120,
                        sorttype: "string",
                        formatter: function(cellvalue,options, rowObject) {
                            let str='';
                            switch (cellvalue) {
                                case 1:
                                    str = '调度组长';
                                    return str;
                                    break;
                                case 2:
                                    str = '协调组长';
                                    return str;
                                    break;
                                case 3:
                                    str = '调度组员';
                                    return str;
                                    break;
                                case 4:
                                    str = '协调组员';
                                    return str;
                                    break;
                            }
                            return str;
                        },
                    },
                    {
                        name: "modifyUser",
                        index: "modifyUser",
                        editable: false,
                        sortable: false,
                        width: 240,
                        sorttype: "string",
                    },
                    {
                        name: "modifyTime",
                        index: "modifyTime",
                        sortable: false,
                        editable: false,
                        width: 320,
                        sorttype: "string",
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                pgbuttons:true,
                pginput:true,
                onPaging: function(pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');
                    if(pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if(pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if(pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if(pgButton == 'first') {
                        currentPage = 1;
                    }
                    if(pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if(pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    if(deferLoadData == 0) {
                        role_level_permission.getServerData(currentPage);
                    }
                },
                beforeRequest: function() {
                    if(deferLoadData == 1) {
                        role_level_permission.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },

                //当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    layerFlag=false;
                },
            });
            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false,
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                if(b>=1844){
                    b=1844;
                };
                $("#table").setGridWidth(b);
            });

            role_level_permission.getServerData(1);
            role_level_permission.searchRole();

        },

        // 初始化角色单选
        refreshRole:function () {
            layui.use(['form','code'], function () {
                var form = layui.form,
                    $ = layui.$;
                // 代码块
                layui.code({
                    title: 'html',
                    encode: true,
                    about: false
                });
                form.render();
            });
        },

        refreshServerData: function() {
            role_level_permission.getServerData(1);
        },
        //订单list----getData
        getServerData: function(pageIndex) {
            layerFlag=false;
            let datas={
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                roleId:$("#roleCon").val(),
                position: pageIndex,
                rows: 10,
            };
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');

            win.utils.ajaxPost(win.utils.services.role_level_per_list, datas, function(result) {
                if(result.result_code == 200) {
                    if(result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        nowCurPage= 0;//订单列表---当前页码数
                        totalNowPages = 0;//订单列表---总的页码数
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.lists;

                        totalPages = Math.ceil(result.total / pageCount);

                        nowCurPage= pageIndex;//订单列表---当前页码数
                        totalNowPages = totalPages;//订单列表---总的页码

                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };

                        pageText = '第'+pageIndex+'页 / 共' + totalPages + '页';

                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });

                        $("#table")[0].addJSONData(gridJson);
                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        //设置更多
        SearchConss: function(){
            flagInput=false;
            ConFlag = layer.open({
                type: 1,
                area: ['80%', '60%'],
                title: '等级设置',
                content: $('#AddCon'),
                btn: ['保存'],
                yes: function() {

                    if(!$("input[name='levelPos']:checked").val()){
                        utils.showLayerMessage('请选择等级！');
                        return;
                    };
                    let levVal = Number($("input[name='levelPos']:checked").val());

                    userArr=[];

                    $("#table input:checked").each(function(index,value){
                        userArr.push($("#table input:checked").eq(index).attr('name'));
                    });

                    let datas= {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        userClass:levVal ,//等级：1调度组长、2协调组长、3调度组员、4协调组员 ,
                        userIds:userArr ,//多个用户Id，要设置用户等级（如协调、调度）的用户Id ,
                    };

                    win.utils.ajaxPost(win.utils.services.role_level_per_update,datas , function(result) {
                        layer.close(ConFlag);
                        if(result.result_code == 200) {
                            $('#setting').removeClass("btn-info");
                            let msg = '设置成功！';
                            utils.showConfirmWithCabackSong(msg,role_level_permission.refreshServerData());
                        } else {
                            utils.showLayerMessage('设置失败，请稍后重试！');
                        }
                    });
                }
            });
        },
        //设置权限
        settingPow:function(){
            if($("#table input:checked").length>0){
                $('#setting').addClass("btn-info");
            }else{
                $('#setting').removeClass("btn-info");
            }
        },
        //查询角色
        searchRole:function(){
            let datas={
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
            };
            win.utils.ajaxPost(win.utils.services.role_level_per, datas, function(result) {
                if(result.result_code == 200) {
                    let roleList = result.orderScheVOS;
                    let str=`<option value="">请选择角色</option>`;
                    let strs='';
                    $.each(roleList,function(key,index){
                        strs+=`<option value="`+index.roleId+`">`+index.roleName+`</option>`;
                    });
                    $("#roleCon").empty().append(str+strs);
                    role_level_permission.refreshRole();
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        }
    };
    // 阻止空格与enter键触发layer.open事件（即出现蒙层）
    $(document).keydown(function (event) {
        var  e=window.event || event;
        var keyCode = event.keyCode;
        if (keyCode == 13 || keyCode == 32) {
            if(flagInput==false){
                if(e.preventDefault){
                    e.preventDefault();
                }else{
                    window.event.returnValue = false;
                }
                return false;
            }
        }
    });

    win.role_level_permission = role_level_permission;

})(window, jQuery);
