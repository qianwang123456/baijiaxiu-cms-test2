/**
 * Created by yimao on 2016-06-24.
 */
(function (win, $) {
    var lastSelectedNode;
    var role_right = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            $('#btnSave').click(obj.saveRoleRightData);

            utils.isUserMenuIndexRight('system_role_right', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.getTreeData();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });
        },
        initTree: function (leftDatas, rightDatas) {
            $leftTree = $('#treeview-left').treeview({
                data: leftDatas,
                showIcon: false,
                showCheckbox: false,
                //showBorder: false,
                selectedBackColor: '#1ab394',
                onNodeSelected: function (event, node) {
                    if (lastSelectedNode != undefined && lastSelectedNode != null) {
                        $leftTree.treeview('unselectNode', [lastSelectedNode.nodeId, {silent: true}]);
                    }
                    lastSelectedNode = node;
                    $rightTree.treeview('uncheckNode', [0, {silent: true}]);
                    role_right.getRoleRightData(node.key);
                },
                onNodeUnselected: function (event, node) {
                    $leftTree.treeview('selectNode', [node.nodeId, {silent: true}]);
                }
            });

            $rightTree = $('#treeview-right').treeview({
                data: rightDatas,
                showIcon: false,
                showCheckbox: true,
                //showBorder: false,
                selectedBackColor: '#1ab394',
                onNodeChecked: function (event, node) {
                    role_right.checkAllChildren($rightTree, node);
                    role_right.checkAllParent($rightTree, node);
                },
                onNodeUnchecked: function (event, node) {
                    role_right.uncheckAllChildren($rightTree, node);
                    role_right.uncheckAllParent($rightTree, node);
                }
            });
        },
        checkAllChildren: function (tree, node) {
            childrens = node.nodes;
            if (childrens == null || childrens.length == 0) {
                return;
            }
            $.each(childrens, function (n, children) {
                tree.treeview('checkNode', [children.nodeId, {silent: true}]);
                role_right.checkAllChildren(tree, children);
            });
        },
        uncheckAllChildren: function (tree, node) {
            childrens = node.nodes;
            if (childrens == null || childrens.length == 0) {
                return;
            }
            $.each(childrens, function (n, children) {
                tree.treeview('uncheckNode', [children.nodeId, {silent: true}]);
                role_right.uncheckAllChildren(tree, children);
            });
        },
        checkAllParent: function (tree, node) {
            if (node.parentId == undefined || node.parentId == null) {
                return;
            }
            parent = tree.treeview('getNode', node.parentId);
            tree.treeview('checkNode', [parent.nodeId, {silent: true}]);
            role_right.checkAllParent(tree, parent);
        },
        uncheckAllParent: function (tree, node) {
            if (node.parentId == undefined || node.parentId == null) {
                return;
            }
            parent = tree.treeview('getNode', node.parentId);
            var count = role_right.getCheckedCount(parent);
            if (count == 0) {
                tree.treeview('uncheckNode', [parent.nodeId, {silent: true}]);
                role_right.uncheckAllParent(tree, parent);
            } else {
                return;
            }
        },
        getCheckedCount: function (parent) {
            checkedCount = 0;
            childrens = parent.nodes;
            if (childrens == null || childrens.length == 0) {
                return 0;
            }
            $.each(childrens, function (n, children) {
                if (children.state.checked) {
                    checkedCount = 1;
                }
            });
            return checkedCount;
        },
        setCheckedByData: function (tree, node, datas) {
            childrens = node.nodes;
            if (childrens == null || childrens.length == 0) {
                return;
            }
            $.each(childrens, function (n, children) {
                if (role_right.isExistKey(datas, children.key) == 1) {
                    tree.treeview('checkNode', [children.nodeId, {silent: true}]);
                } else {
                    tree.treeview('uncheckNode', [children.nodeId, {silent: true}]);
                }
                role_right.setCheckedByData(tree, children, datas);
            });
        },
        isExistKey: function (datas, key) {
            if (datas == undefined || datas == null || datas.length == 0) {
                return 0;
            }

            for (var index in datas) {
                if (key == datas[index].key) {
                    return 1;
                }
            }
            return 0;
        },
        getCheckedIndexs: function (tree, node) {
            childrens = node.nodes;
            if (childrens == null || childrens.length == 0) {
                return '';
            }
            checkedString = '';
            $.each(childrens, function (n, children) {
                if (children.state.checked) {
                    checkedString = checkedString + children.key + ",";
                }
                checkedString = checkedString + role_right.getCheckedIndexs(tree, children);
            });
            return checkedString;
        },
        getTreeData: function () {
            win.utils.ajax(win.utils.services.role_right_tree, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session')
            }, function (result) {
                if (result.result_code == 200) {
                    leftData = [result.roles];
                    rightData = [result.menus];
                    role_right.initTree(leftData, rightData);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        getRoleRightData: function (roleID) {
            win.utils.ajax(win.utils.services.role_right_get, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                role_id: roleID
            }, function (result) {
                if (result.result_code == 200) {
                    rootNode = $rightTree.treeview('getNode', 0);
                    if (result.keys != undefined && result.keys != null && result.keys.length > 0) {
                        $rightTree.treeview('checkNode', [0, {silent: true}]);
                    }
                    role_right.setCheckedByData($rightTree, rootNode, result.keys);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        saveRoleRightData: function () {
            rootNode = $rightTree.treeview('getNode', 0);
            keyIDs = $leftTree.treeview('getSelected', 0);
            if (keyIDs == undefined || keyIDs == null || keyIDs.length == 0) {
                utils.showMessage('请选择角色并授权！');
                return;
            }
            keyID = keyIDs[0].key;
            checkedIndexs = role_right.getCheckedIndexs($rightTree, rootNode);

            win.utils.ajax(win.utils.services.role_right_save, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                role_id: keyID,
                keys: checkedIndexs
            }, function (result) {
                if (result.result_code == 200) {
                    utils.showMessage('授权成功！');
                } else {
                    utils.showMessage('保存失败，请重试！');
                }
            });
        }
    };

    win.role_right = role_right;
})(window, jQuery);