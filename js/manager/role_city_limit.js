/**
 * Created by hanlu
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    var nowPage = 1;

    var cities = [];

    // 请求参数
    var data = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        // rank:1,
        position: nowPage,
        rows: 20
    };
    // 修改
    // $('#table').on('click', '#btnUpdate',function() {
    //
    //     var id=$(this).attr('data-id');
    //
    //     // 获取城市数据信息
    //     win.utils.ajaxPost(win.utils.services.getSysArea, {
    //         user_id: win.utils.getCookie('user_id'),
    //         session: win.utils.getCookie('session'),
    //         levelFlag: 1,
    //     }, function(result) {
    //         if(result.result_code == 200) {
    //            var cityStr='<label class="col-sm-12 control-label" style="text-align: left;">城市</label>';
    //            cities=result.areaVOList;
    //            result.areaVOList.forEach(function (i,k) {
    //                cityStr+='<div class="col-sm-12">\n' +
    //                    '                <label class="checkbox-inline i-checks">\n' +
    //                    '                    <input id="'+i.id+'" type="checkbox"><span>'+i.addressName+'</span>\n'+
    //                    '                </label>\n' +
    //                    '            </div>'
    //            });
    //            $('.cityBox').html(cityStr);
    //             $(".i-checks").iCheck({
    //                 checkboxClass: "icheckbox_square-green",
    //                 radioClass: "iradio_square-green",
    //             });
    //
    //         } else {
    //             utils.showMessage('获取数据失败，请重试！');
    //             return;
    //         }
    //     });
    //
    //     var i = layer.open({
    //         type: 1,
    //         area: ['60%', '60%'],
    //         title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;分派城市',
    //         content: $('#model'),
    //         btn: ['保存', '取消'],
    //         yes: function(index, layero) {}
    //     });
    //
    //
    // });

    function initSearch() {
        $("#balance").val("");
        $("#MainPlan").val("");
        $("#MainResult").val("");
        $("#relation").val("");
        $("#overtimeRecord").val("");
        //more-clear
        $("#orderTime").val("");
        $("#estiComTime").val("");
        $("#finishTime").val("");
        $("#createTime").val("");
        $("#getOrderTime").val("");
        $("#sendOrder").val("");
        $("#toDoorTime").val("");
        $("#city").val("");
        $("#tradingArea").val("");
        $("#orderId").val("");
        $("#apartmentId").val("");
        $("#apartmentAdress").val("");
        $("#comType").val("");
        $("#teamName").val("");
        $("#workSupply").val("");
        $("#MainType").val("");
        $("#MainOrderWay").val("");
        $("#end").val("");
        $("#relaSta").val("");
        $("#repairman").val("");
        $("#transferOrderId").val("");
    };

    var parts_manage = {
        //初始化
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            //判断是否有订单权限，此时为配置管理的工单权限
            utils.isUserMenuIndexRight('config_first', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });


            // 分配城市
            $('#table').on('click', '#btnUpdate', function () {
                $('.responsibleCity').html(''); //初始化
                $('.userName').text('');

                var id = $(this).attr('data-id');
                var userName = $(this).attr('data-userName');
                console.log(id);
                if (id) {
                    $('.userName').text(userName);
                    // 获取对应调度信息
                    win.utils.ajaxPost(win.utils.services.queryUserCity, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        userId: id
                    }, function (result) {
                        if (result.result_code == 200) {

                            // 获取城市数据信息
                            win.utils.ajaxPost(win.utils.services.getSysArea, {
                                user_id: win.utils.getCookie('user_id'),
                                session: win.utils.getCookie('session'),
                                levelFlag: 1,
                            }, function (res) {
                                if (res.result_code == 200) {
                                    var cityStr = '<label class="col-sm-12 control-label" style="text-align: left;">城市</label>';
                                    cities = res.areaVOList;
                                    res.areaVOList.forEach(function (i, k) {
                                        cityStr += '<div class="col-sm-12">\n' +
                                            '                <label class="checkbox-inline i-checks">\n' +
                                            '                    <input id="' + i.id + '" type="checkbox"><span>' + i.addressName + '</span>\n' +
                                            '                </label>\n' +
                                            '            </div>'
                                    });
                                    $('.cityBox').html(cityStr);

                                    $(".i-checks").iCheck({
                                        checkboxClass: "icheckbox_square-green",
                                        radioClass: "iradio_square-green",
                                    });


                                    // 显示用户信息
                                    if (result.sysUserCityVOS.length) {
                                        var cityStr = '';
                                        result.sysUserCityVOS.forEach(function (i, k) {
                                            // 显示当前调度负责的区域
                                            cityStr += '<div class="cityItem" data-cityId="' + i.cityId + '" data-cityName="' + i.addressName + '">\n' +
                                                '                        <span  data-cityId="' + i.cityId + '">' + i.addressName + '</span>\n' +
                                                '                        <img src="../../images/close.png"  data-cityId="' + i.cityId + '">\n' +
                                                '                    </div>';


                                            // 当前调度负责的区域和城市同步
                                            var inputNodes = $('#model').find('input');
                                            for (j = 0; j < inputNodes.length; j++) {
                                                var id = $(inputNodes[j]).attr('id');
                                                if (id == i.cityId) {
                                                    $(inputNodes[j]).iCheck('check');
                                                }
                                            }


                                        });
                                        $('.responsibleCity').html(cityStr);


                                    }


                                } else {
                                    utils.showMessage('获取数据失败，请重试！');
                                    return;
                                }
                            });

                        } else {
                            utils.showMessage('获取信息失败，请重试！');
                        }
                    });


                    // 打开弹层
                    var i = layer.open({
                        type: 1,
                        area: ['60%', '60%'],
                        title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;分派城市',
                        content: $('#model'),
                        btn: ['保存', '取消'],
                        success: function () {
                            $('.layui-layer-page').css('zIndex', 333);
                            $('.layui-layer-shade').css('zIndex', 300);
                        },
                        yes: function (index, layero) {
                            var userName = $('.userName').text();
                            var userId = id;
                            var cityIds = [];
                            var citys = [];
                            var cityNodes = $('.responsibleCity .cityItem');
                            if (cityNodes.length) {
                                for (var i = 0; i < cityNodes.length; i++) {
                                    cityIds.push($(cityNodes[i]).attr('data-cityId'));
                                    citys.push({
                                        addressName: $(cityNodes[i]).attr('data-cityName'),
                                        cityId: $(cityNodes[i]).attr('data-cityId'),
                                        id: $(cityNodes[i]).attr('data-cityId')
                                    });
                                }
                            } else {
                                utils.showMessage('请先分派城市！');
                            }

                            win.utils.ajaxPost(win.utils.services.updateUserCity, {
                                user_id: win.utils.getCookie('user_id'),
                                session: win.utils.getCookie('session'),
                                userId: userId,
                                userName: userName,
                                cityIds: cityIds
                            }, function (result) {
                                if (result.result_code == 200) {
                                    utils.showMessage('分派成功！');
                                    layer.close(index);
                                    // 分派成功刷新列表
                                    $('#table').jqGrid('setRowData', id, {
                                        citys: citys
                                    });
                                }
                            })
                        }
                    });
                }

            });

            // 分配城市复选框选择事件
            $('#model').on('ifChanged', 'input', function (event) { //ifCreated 事件应该在插件初始化之前绑定
                var cityId = $(this).attr('id');
                var cityName;
                cities.forEach(function (i, k) {
                    if (i.id == cityId) {
                        cityName = i.addressName;
                    }
                });
                // 选中
                if ($(this).is(':checked')) {
                    $('.responsibleCity').append('<div class="cityItem" data-cityId="' + cityId + '" data-cityName="' + cityName + '">\n' +
                        '                        <span  data-cityId="' + cityId + '">' + cityName + '</span>\n' +
                        '                        <img src="../../images/close.png"  data-cityId="' + cityId + '">\n' +
                        '                    </div>');
                }
                // 取消选中
                else {
                    var cityItemNodes = $('.responsibleCity').find('.cityItem');
                    for (var i = 0; i < cityItemNodes.length; i++) {
                        var dataCityid = $(cityItemNodes[i]).attr('data-cityid');
                        if (dataCityid == cityId) {
                            $('.cityItem').eq(i).remove();
                        }
                    }
                }
            });

            // 删除选中城市
            $('.responsibleCity').on('click', 'img', function (e) {
                e.stopPropagation();
                var preIndex = $(this).parents('.cityItem').index();
                $('.cityItem').eq(preIndex).remove();

                var cityId = $(this).attr('data-cityid');
                var inputNodes = $('#model').find('input');
                for (i = 0; i < inputNodes.length; i++) {
                    var id = $(inputNodes[i]).attr('id');
                    if (id == cityId) {
                        $(inputNodes[i]).iCheck('uncheck');
                    }
                }
            });


            // 搜索
            $('#btnSearch').on('click', function () {
                parts_manage.search();
            });
        },
        //初始化请求
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),//utils.getAutoGridHeightSong(),
                autowidth: true,
                shrinkToFit: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["操作", "id", "调度员姓名", "负责城市"],
                colModel: [
                    {
                        name: "options",
                        index: "options",
                        editable: false,
                        width: 30,
                        sortable: false,
                        formatter: tableOptionsBtn
                    },
                    {
                        name: "id",
                        index: "id",
                        editable: false,
                        width: 60,
                        sortable: false,
                        // formatter:statusText
                    },
                    {
                        name: "userName",
                        index: "userName",
                        editable: false,
                        width: 60,
                        sortable: false,
                        // formatter:statusText
                    },
                    {
                        name: "citys",
                        index: "citys",
                        editable: false,
                        width: 60,
                        sortable: false,
                        formatter: cityText
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                // 翻页
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    data.position = currentPage;
                    if (deferLoadData == 0) {
                        parts_manage.getServerData(nowPage, data);
                    }
                },
                // 改变页显示条数
                beforeRequest: function () {
                    data.rows = $('.ui-pg-selbox.form-control option:selected').text();
                    if (deferLoadData == 1) {
                        nowPage = currentPage;
                        parts_manage.getServerData(nowPage, data);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    parts_manage.getServerData(nowPage, data);
                },
                //当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowindex = $("#" + this.id).getGridParam('selrow');
                    console.log(selectedRowindex, "12345");
                    //返回点击这行xlmc的值
                    // selectedRowValue = $("#gridTable").jqGridRowValue("xlmc");
                },
                gridComplete: function () {

                }
            });

            parts_manage.getServerData(nowPage, data);

            function tableOptionsBtn(cellvalue, options, rowObject) {
                return '<button type="button" class="btn btn-info opertion" id="btnUpdate" data-id="' + rowObject.id + '" data-userName="' + rowObject.userName + '">分派城市</button>'
            }

            function cityText(cellvalue, options, rowObject) {
                var citiesArr = [];
                if (rowObject.citys && rowObject.citys.length) {
                    rowObject.citys.forEach(function (i, k) {
                        citiesArr.push(i.addressName);
                    })
                }
                return citiesArr.join(',');
            }

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(win).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        getServerData: function (pageIndex, postData) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.getUserCityList, postData, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        result.list.forEach(function (i, k) {
                            i.id = i.userId;
                        });
                        gridData = result.list;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 查询
        search: function () {
            // 初始化
            $('#dispatcher').val('');
            $('#dispatcherCity').val('');

            var i = layer.open({
                type: 1,
                area: ['50%', '60%'],
                title: '<i class="fa fa-search">&nbsp;&nbsp;搜索</i>',
                content: $('#searchLayer'),
                btn: ['确认', '取消'],
                success: function () {
                    $('.layui-layer-page').css('zIndex', 333);
                    $('.layui-layer-shade').css('zIndex', 300);

                    // 获取调度员
                    win.utils.ajaxPost(win.utils.services.getUserCityList, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        position: nowPage,
                        rows: 20
                    }, function (result) {
                        if (result.result_code == 200) {
                            if (result.list.length) {
                                var dispatcherOptionStr = '<option value="">请选择调度员</option>';
                                result.list.forEach(function (i, k) {
                                    dispatcherOptionStr += '<option value="' + i.userId + '">' + i.userName + '</option>'
                                });
                                $('#dispatcher').html(dispatcherOptionStr);
                                form.render('select');
                            }
                        } else {
                            utils.showMessage('获取数据失败，请重试！');
                        }
                    });


                    // 获取城市
                    win.utils.ajaxPost(win.utils.services.getSysArea, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        levelFlag: 1,
                    }, function (res) {
                        if (res.result_code == 200) {
                            var cityStr = ' <option value="">请选择城市</option>';
                            res.areaVOList.forEach(function (i, k) {
                                cityStr += ' <option value="' + i.id + '">' + i.addressName + '</option>'
                            });
                            $('#dispatcherCity').html(cityStr);
                            form.render('select');
                        } else {
                            utils.showMessage('获取数据失败，请重试！');
                            return;
                        }
                    });

                },
                yes: function (index, layero) {
                    var userId = $('#dispatcher').val() || null;
                    var userName = $('#dispatcher').find('option:selected').text() || null;
                    var cityId = $('#dispatcherCity').val() || null;
                    var cityName = $('#dispatcherCity').find('option:selected').text() || null;
                    // win.utils.ajaxPost(win.utils.services.getUserCityList,{
                    //     user_id: win.utils.getCookie('user_id'),
                    //     session: win.utils.getCookie('session'),
                    //     userId:userId,
                    //     userName:userName,
                    //
                    //
                    // },function (result) {
                    //
                    // })
                }
            });
        },
        // 弹层初始化
        layerInit: function () {
            $('#addPartsName').val(''); //配件名
            $('#addStatus').val(''); //状态
        },
        // 刷新
        refresh: function () {
            initSearch();
            nowPage = 1;
            data = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rank: 1,
                position: nowPage,
                rows: 20
            };
            parts_manage.getServerData(nowPage, data);
        },
    };
    win.parts_manage = parts_manage
})(window, jQuery);