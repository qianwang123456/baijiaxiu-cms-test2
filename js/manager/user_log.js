/**
 * Created by yimao on 2016-06-24.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //query condition
    var queryUserID = '';
    var queryBusinessDateBegin = '';
    var queryBusinessDateEnd = '';
    var queryMenuIndex = '0';

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
    }

    var user_log = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('system_user_log', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });

            laydate({elem: "#queryLogDateBegin", event: "focus"});
            laydate({elem: "#queryLogDateEnd", event: "focus"});

            $('#btnQuery').on('click', function () {
                queryUserID = '';
                queryBusinessDateBegin = '';
                queryBusinessDateEnd = '';
                queryMenuIndex = '0';
                obj.getServerData(1);
            });
            $('#btnCondition').click(obj.queryOperation);

            utils.initMenuIndexes('#querySelectMenuIndex', 1);
        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["ID", "日期", "操作员", "功能", "操作", "说明"],
                colModel: [
                    {name: "id", index: "id", editable: false, hidden: true, width: 90, sorttype: "int"},
                    {name: "date", index: "date", editable: false, width: 180, sortable: true},
                    {name: "user", index: "user", editable: false, width: 180, sortable: true},
                    {name: "menu", index: "menu", editable: false, width: 180, sortable: true},
                    {name: "operation", index: "operation", editable: false, width: 180, sortable: false},
                    {name: "remark", index: "remark", editable: false, width: 360, sortable: false}
                ],
                pager: "#table_pager",
                viewrecords: true,
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    if (deferLoadData == 0) {
                        user_log.getServerData(currentPage);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        user_log.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    user_log.getServerData(currentPage);
                }
            });

            user_log.getServerData(1);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        getServerData: function (pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajax(win.utils.services.user_log, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                batch: pageIndex,
                rows: pageCount,
                order: orderByString,
                mobile: queryUserID,
                date_begin: queryBusinessDateBegin,
                date_end: queryBusinessDateEnd,
                menu_index: queryMenuIndex
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.logs.total == 0) {
                        gridData = {};
                        var gridJson = {total: 0, rows: gridData, page: 0, records: result.logs.total};
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {pgtext: pageText});
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.logs.rows;
                        totalPages = Math.ceil(result.logs.total / pageCount);
                        var gridJson = {total: totalPages, rows: gridData, page: pageIndex, records: result.logs.total};
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {pgtext: pageText});
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        queryOperation: function () {
            initializeEditForm();
            var i = layer.open({
                type: 1,
                area: ['480px'],
                title: '<i class="fa fa-search">&nbsp;&nbsp;查询条件',
                content: $('#query'),
                btn: ['查询', '取消'],
                yes: function (index, layero) {
                    var query_UserID = $('#queryUserID').val();
                    var query_LogDateBegin = $('#queryLogDateBegin').val();
                    var query_LogDateEnd = $('#queryLogDateEnd').val();
                    var query_MenuIndex = $('#querySelectMenuIndex').val();
                    if (query_LogDateBegin != '' && !utils.isDate(query_LogDateBegin)) {
                        $('#queryLogDateBegin').val('');
                        $('#queryLogDateBegin').focus();
                        return;
                    }
                    if (query_LogDateEnd != '' && !utils.isDate(query_LogDateEnd)) {
                        $('#queryLogDateEnd').val('');
                        $('#queryLogDateEnd').focus();
                        return;
                    }

                    queryUserID = query_UserID;
                    queryBusinessDateBegin = query_LogDateBegin;
                    queryBusinessDateEnd = query_LogDateEnd;
                    queryMenuIndex = query_MenuIndex;
                    user_log.getServerData(1);
                    layer.close(i);
                }
            });
        }
    };

    win.user_log = user_log;
})(window, jQuery);