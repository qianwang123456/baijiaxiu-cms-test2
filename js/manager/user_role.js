/**
 * Created by yimao on 2016-06-24.
 */
(function (win, $) {
    var lastSelectedNode;
    var is_search = false;
    let data = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        userId: null,
        userName: null
    };
    var user_role = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            $('#btnSave').click(obj.saveUserRoleData);
            $('#btnSearch').click(obj.searchFn);
            $('#btnRefresh').click(obj.refresh);

            utils.isUserMenuIndexRight('system_user_role', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.getTreeData();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });
        },
        initTree: function (leftDatas, rightDatas) {
            $leftTree = $('#treeview-left').treeview({
                data: leftDatas,
                showIcon: false,
                showCheckbox: false,
                //showBorder: false,
                selectedBackColor: '#1ab394',
                onNodeSelected: function (event, node) {
                    if (lastSelectedNode != undefined && lastSelectedNode != null) {
                        $leftTree.treeview('unselectNode', [lastSelectedNode.nodeId, {silent: true}]);
                    }
                    lastSelectedNode = node;
                    $rightTree.treeview('uncheckNode', [0, {silent: true}]);
                    user_role.getUserRoleData(node.key);
                },
                onNodeUnselected: function (event, node) {
                    $leftTree.treeview('selectNode', [node.nodeId, {silent: true}]);
                }
            });

            $rightTree = $('#treeview-right').treeview({
                data: rightDatas,
                showIcon: false,
                showCheckbox: true,
                //showBorder: false,
                selectedBackColor: '#1ab394',
                onNodeChecked: function (event, node) {
                    user_role.checkAllChildren($rightTree, node);
                    user_role.checkAllParent($rightTree, node);
                },
                onNodeUnchecked: function (event, node) {
                    user_role.uncheckAllChildren($rightTree, node);
                    user_role.uncheckAllParent($rightTree, node);
                }
            });
        },
        checkAllChildren: function (tree, node) {
            childrens = node.nodes;
            if (childrens == null || childrens.length == 0) {
                return;
            }
            $.each(childrens, function (n, children) {
                tree.treeview('checkNode', [children.nodeId, {silent: true}]);
                user_role.checkAllChildren(tree, children);
            });
        },
        uncheckAllChildren: function (tree, node) {
            childrens = node.nodes;
            if (childrens == null || childrens.length == 0) {
                return;
            }
            $.each(childrens, function (n, children) {
                tree.treeview('uncheckNode', [children.nodeId, {silent: true}]);
                user_role.uncheckAllChildren(tree, children);
            });
        },
        checkAllParent: function (tree, node) {
            if (node.parentId == undefined || node.parentId == null) {
                return;
            }
            parent = tree.treeview('getNode', node.parentId);
            tree.treeview('checkNode', [parent.nodeId, {silent: true}]);
            user_role.checkAllParent(tree, parent);
        },
        uncheckAllParent: function (tree, node) {
            if (node.parentId == undefined || node.parentId == null) {
                return;
            }
            parent = tree.treeview('getNode', node.parentId);
            var count = user_role.getCheckedCount(parent);
            if (count == 0) {
                tree.treeview('uncheckNode', [parent.nodeId, {silent: true}]);
                user_role.uncheckAllParent(tree, parent);
            } else {
                return;
            }
        },
        getCheckedCount: function (parent) {
            checkedCount = 0;
            childrens = parent.nodes;
            if (childrens == null || childrens.length == 0) {
                return 0;
            }
            $.each(childrens, function (n, children) {
                if (children.state.checked) {
                    checkedCount = 1;
                }
            });
            return checkedCount;
        },
        setCheckedByData: function (tree, node, datas) {
            childrens = node.nodes;
            if (childrens == null || childrens.length == 0) {
                return;
            }
            $.each(childrens, function (n, children) {
                if (user_role.isExistKey(datas, children.key) == 1) {
                    tree.treeview('checkNode', [children.nodeId, {silent: true}]);
                } else {
                    tree.treeview('uncheckNode', [children.nodeId, {silent: true}]);
                }
                user_role.setCheckedByData(tree, children, datas);
            });
        },
        isExistKey: function (datas, key) {
            if (datas == undefined || datas == null || datas.length == 0) {
                return 0;
            }

            for (var index in datas) {
                if (key == datas[index].key) {
                    return 1;
                }
            }
            return 0;
        },
        getCheckedIndexs: function (tree, node) {
            childrens = node.nodes;
            if (childrens == null || childrens.length == 0) {
                return '';
            }
            checkedString = '';
            $.each(childrens, function (n, children) {
                if (children.state.checked) {
                    checkedString = checkedString + children.key + ",";
                }
                checkedString = checkedString + user_role.getCheckedIndexs(tree, children);
            });
            return checkedString;
        },
        getTreeData: function () {
            win.utils.ajax(win.utils.services.user_role_tree, data, function (result) {
                if (result.result_code == 200) {
                    leftData = [result.users];
                    rightData = [result.roles];
                    user_role.initTree(leftData, rightData);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        getUserRoleData: function (userID) {

            win.utils.ajax(win.utils.services.user_role_get, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                tree_user_id: userID
            }, function (result) {
                if (result.result_code == 200) {
                    rootNode = $rightTree.treeview('getNode', 0);
                    if (result.keys != undefined && result.keys != null && result.keys.length > 0) {
                        $rightTree.treeview('checkNode', [0, {silent: true}]);
                    }
                    user_role.setCheckedByData($rightTree, rootNode, result.keys);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        saveUserRoleData: function () {
            rootNode = $rightTree.treeview('getNode', 0);
            keyIDs = $leftTree.treeview('getSelected', 0);
            if (keyIDs == undefined || keyIDs == null || keyIDs.length == 0) {
                utils.showMessage('请选择用户！');
                return;
            }
            keyID = keyIDs[0].key;
            checkedIndexs = user_role.getCheckedIndexs($rightTree, rootNode);

            win.utils.ajax(win.utils.services.user_role_save, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                tree_user_id: keyID,
                keys: checkedIndexs
            }, function (result) {
                if (result.result_code == 200) {
                    utils.showMessage('保存成功！');
                } else {
                    utils.showMessage('保存失败，请重试！');
                }
            });
        },
        // 新增搜索功能
        searchFn: function () {
            user_role.initializeEditForm();
            var i = layer.open({
                type: 1,
                area: ['60%', '60%'],
                title: '<i class="fa fa-search">&nbsp;&nbsp;搜索用户</i>',
                content: $('#model'),
                btn: ['搜索', '取消'],
                shade: 0,
                yes: function (index, layero) {
                    is_search = true;
                    user_role.search();

                    layer.close(index);
                },
            });
        },
        search: function () {
            lastSelectedNode = null;
            if (is_search) {
                var userNumber = $('#userNumber').val();
                var userName = $('#userName').val();

                data.userName = userName;
                data.userId = userNumber;
            }
            user_role.getTreeData();
        },
        initializeEditForm: function () {
            $('#userNumber').val('');
            $('#userName').val('');
        },
        refresh: function () {
            data = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                userId: null,
                userName: null
            };
            user_role.getTreeData();
            lastSelectedNode = null;
        }
    };

    win.user_role = user_role;
})(window, jQuery);