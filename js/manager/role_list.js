/**
 * Created by yimao on 2016-06-24.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
        $('#hiddenID').val('');
        $('#txtRoleName').val('');
        $('#txtRemark').val('');
    }

    var role_list = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('system_role', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });

            win.vaildation.init("#form");

            $('#btnQuery').on('click', function () {
                obj.getServerData(1);
            });
            $('#btnAdd').click(obj.addRecord);
            // $('#btnUpdate').click(obj.updateRecord);
            // $('#btnDel').click(obj.deleteRecord);

            $('#table').on('click', '#btnUpdate', function () {
                var id = $(this).attr('data-id');
                obj.updateRecord(id);
            });
            $('#table').on('click', '#btnDel', function () {
                var id = $(this).attr('data-id');
                obj.deleteRecord(id);
            });
        },
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: true,
                shrinkToFit: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["操作", "ID", "角色名称", "角色描述"],
                colModel: [
                    {
                        name: "option",
                        index: "option",
                        editable: false,
                        width: 90,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            return '<button id="btnUpdate" class="btn btn-info unified-btn-inline unified-change-btn-inline" type="button" data-id="' + rowObject.id + '" style="margin-right: 10px;">修改</button>\n' +
                                '                <button id="btnDel" class="btn btn-danger unified-btn-inline unified-deleted-btn-inline" type="button" data-id="' + rowObject.id + '">删除</button>'
                        }
                    },
                    {name: "id", index: "role_id", editable: false, width: 90, sorttype: "string", hidden: true},
                    {name: "name", index: "role_name", editable: false, width: 90, sorttype: "string"},
                    {name: "remark", index: "remark", editable: false, width: 90, sortable: false}
                ],
                pager: "#table_pager",
                viewrecords: true,
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    if (deferLoadData == 0) {
                        role_list.getServerData(currentPage);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        role_list.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    role_list.getServerData(currentPage);
                }
            });

            role_list.getServerData(1);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });
            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });
        },
        getServerData: function (pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajax(win.utils.services.role_list, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                batch: pageIndex,
                rows: pageCount,
                order: orderByString
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.roles.total == 0) {
                        gridData = {};
                        var gridJson = {total: 0, rows: gridData, page: 0, records: result.roles.total};
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {pgtext: pageText});
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.roles.rows;
                        totalPages = Math.ceil(result.roles.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.roles.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {pgtext: pageText});
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        addRecord: function () {
            initializeEditForm();
            var i = layer.open({
                type: 1,
                area: '480px',
                title: '<i class="fa fa-plus">&nbsp;&nbsp;</i>添加角色',
                content: $('#model'),
                btn: ['保存', '取消'],
                yes: function (index, layero) {
                    var role_name = $('#txtRoleName').val();
                    var remark = $('#txtRemark').val();

                    if (!$('#form').valid()) {
                        if (role_name == '') {
                            $('#txtRoleName').focus();
                            return;
                        }
                        return;
                    }

                    win.utils.ajax(win.utils.services.role_save, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        role_name: role_name,
                        remark: remark
                    }, function (result) {
                        if (result.result_code == 200) {
                            $('#table').jqGrid('addRowData', result.result_key, {
                                id: result.result_key,
                                name: role_name,
                                remark: remark
                            });
                            $("#table").setSelection(result.result_key, true);
                            layer.close(i);
                        } else {
                            utils.showLayerMessage('保存失败，请检查角色名称是否重复！');
                        }
                    });
                }
            });
        },
        // 修改
        updateRecord: function (data) {
            initializeEditForm();
            // var data = $("#table").jqGrid('getGridParam','selrow');
            // if (data == null){
            //     utils.showMessage('请选择要进行修改的记录！');
            //     return;
            // }

            win.utils.ajax(win.utils.services.role_detail, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                role_id: data
            }, function (result) {
                if (result.result_code == 200) {
                    $('#hiddenID').val(result.role.id);
                    $('#txtRoleName').val(result.role.name);
                    $('#txtRemark').val(result.role.remark);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                    return;
                }
            });

            var i = layer.open({
                type: 1,
                area: '480px',
                title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;修改角色',
                content: $('#model'),
                btn: ['保存', '取消'],
                yes: function (index, layero) {
                    var role_name = $('#txtRoleName').val();
                    var remark = $('#txtRemark').val();

                    if (!$('#form').valid()) {
                        if (role_name == '') {
                            $('#txtRoleName').focus();
                            return;
                        }
                        return;
                    }

                    var role_id = $('#hiddenID').val();
                    win.utils.ajax(win.utils.services.role_update, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        role_id: role_id,
                        role_name: role_name,
                        remark: remark
                    }, function (result) {
                        if (result.result_code == 200) {
                            $('#table').jqGrid('setRowData', role_id, {name: role_name, remark: remark});
                            layer.close(i);
                        } else {
                            utils.showLayerMessage('保存失败，请检查角色名称是否重复！');
                        }
                    });

                }
            });
        },
        // 删除
        deleteRecord: function (data) {
            // var data = $("#table").jqGrid('getGridParam','selrow');
            // if (data == null){
            //     utils.showMessage('请选择要进行删除的记录！');
            //     return;
            // }

            utils.showConfirm('您确定要删除这条记录吗？', '删除', function () {
                win.utils.ajax(win.utils.services.role_delete, {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    role_id: data
                }, function (result) {
                    if (result.result_code == 200) {
                        utils.closeMessage();
                        $('#table').jqGrid('delRowData', data);
                    } else {
                        utils.showMessage('删除失败，此角色已被使用！');
                    }
                });
            });
        }
    };

    win.role_list = role_list;
})(window, jQuery);