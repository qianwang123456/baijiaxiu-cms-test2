/**
 * Created by song.
 */
(function (win, $) {

    // buttonType按钮类型(1-保存,2启动)

    var data = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
    };

    var schedule = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('schedule', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });
            // 点击事件
            $("ul").off('click').on('click','input',function(){

                let staVal = $(this).attr('status');
                if($(this).is(':checked')){ //true
                    if(Number(staVal)==1){
                        $(this).parents('li').find('input').eq(1).prop("checked", false);
                    }else{
                        $(this).parents('li').find('input').eq(0).prop("checked", false);
                    }
                }
            });
            //设定
            $('.btn').on('click', function () {
                let nameVal = $(this).attr('name');
                schedule.btnFn(nameVal);
            });
        },
        initList: function () {
            schedule.getServerData();
        },
        //btn-fn
        btnFn: function(val){
            let setTingArr = [];
            $(".lists li").each(function(index,value){
                let str = $(".lists li").eq(index).find('input').eq(0).attr('name');
                let staVal = 0;

                if(str){
                    if($(".lists li").eq(index).find('input:checked').length>0){
                        staVal = $(".lists li").eq(index).find('input:checked').attr('status');
                    }
                    let transJson = {
                        userId:str,
                        userType:staVal,
                    };
                    setTingArr.push(transJson);
                };
            });
            let data = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                scheDTOS:setTingArr //多个用户Id(调度或协调) ,//多个用户（调度或协调）班次信息（班次：1.当日早班，2.当日晚班） ,
            };
            // buttonType按钮类型(1-保存,2启动)  set:表示设置 start:表示保存
            data.buttonType=val=='set'?1:2;

            // console.log(data);
            let url =win.utils.services.sche_get_user_update;
            win.utils.ajaxPost(url, data, function (result) {
                if (result.result_code == 200) {
                    let errorMsg = val=='set'?'保存成功，点击启动后才可执行本配置！':'保存成功，并启动本配置！';
                    utils.showMessage(errorMsg);
                } else {
                    let errorMsgs = val=='set'?'设定失败！':'启动失败！';
                    let msg = result.description? result.description:errorMsgs;
                    utils.showMessage(msg);
                }
            });

        },
        //获取list数据
        getServerData: function () {
            win.utils.ajaxPost(win.utils.services.sche_get_user_list, data, function (result) {
                if (result.result_code == 200) {
                    let lists= result.lists;
                    $("#leftNav").empty()
                    $("#middleNav").empty()
                    $("#rightNav").empty()
                    if(lists.length<=0){
                        return
                    }
                    let num_z = Math.floor(lists.length/3); // 向求求整
                    let num_y = Math.ceil(lists.length%3); //  求余数

                    let stra=`<li class="til"><div>姓名</div><div>当日早班</div><div>当日晚班</div></li>`;
                    let strb=`<li class="til"><div>姓名</div><div>当日早班</div><div>当日晚班</div></li>`;
                    let strc=`<li class="til"><div>姓名</div><div>当日早班</div><div>当日晚班</div></li>`;
                    if(lists.length<3){
                        $("#middleNav").hide()
                        $("#rightNav").hide()
                    }
                    if(lists.length<=0){
                        $("#leftNav").hide()
                    }
                    $.each(lists,function(index,value){
                        // console.log(index,value,'000');
                        let typeVal = Number(lists[index].userType);
                        let statusValA = ``;
                        let statusValB = ``;
                        if(typeVal == 1){
                            statusValA = 'checked';
                            statusValB ='';
                        }else if(typeVal == 2){
                            statusValA = '';
                            statusValB ='checked';
                        }else{
                            statusValA = ``;
                            statusValB = ``;
                        };

                        if(index<(num_z+num_y)){
                            stra+=`<li>
                                        <div>`+lists[index].userName+`</div>
                                        <div><input type="checkbox"  status="1" `+statusValA+` name="`+lists[index].userId+`"></div>
                                        <div><input type="checkbox"  status="2" `+statusValB+` name="`+lists[index].userId+`"></div></li>`;
                        };
                        if(index>=(num_z+num_y) && index<(num_z*2+num_y)){
                            strb+=`<li>
                                         <div>`+lists[index].userName+`</div>
                                        <div><input type="checkbox" status="1" `+statusValA+` name="`+lists[index].userId+`"></div>
                                        <div><input type="checkbox" status="2" `+statusValB+` name="`+lists[index].userId+`"></div></li>`;
                        }
                        if(index>=(num_z*2+num_y)){
                            strc+=`<li>
                                       <div>`+lists[index].userName+`</div>
                                        <div><input type="checkbox" status="1" `+statusValA+` name="`+lists[index].userId+`"></div>
                                        <div><input type="checkbox" status="2" `+statusValB+` name="`+lists[index].userId+`"></div></li>`;
                        }
                    });
                    $("#leftNav").empty().append(stra);
                    $("#middleNav").empty().append(strb);
                    $("#rightNav").empty().append(strc);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
    };

    win.schedule = schedule;
})(window, jQuery);
