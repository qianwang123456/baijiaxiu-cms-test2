/**
 * Created by song on 2019-06
 */
(function (win, $) {

    var LastTime = ''; //最晚下班时间---初始化获取
    var updateFlag = true; //设置时间开关

    var data = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
    };

    var dispatchTimeSetting = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            utils.isUserMenuIndexRight('dispatch_time_setting', function (result) {
                if (result == 1) {
                    //initialize...
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });


        },
        initList: function () {

            dispatchTimeSetting.getServerData();

            $("#search").on('click',function(){
                dispatchTimeSetting.judgeTimeFn(1);

                if(updateFlag==true){
                    return;
                }
                let ts= $("#morningTime").val(); //早班时间
                let tef= $("#nightTimeFirst").val(); //晚班时间-
                let tes= $("#nightTimeSecond").val(); //晚班时间-

                let dataNow = data;
                if(ts&&ts!=''){
                    dataNow.morStartTime =ts.slice(0,8);
                    dataNow.morEndTime=ts.slice(11);
                }
                if(tef&&tef!=''){
                    dataNow.nigStartTimeFirst=tef.slice(0,8);
                    dataNow.nigEndTimeFirst=tef.slice(11);
                }
                if(tes&&tes!=''){
                    dataNow.nigStartTimeSecond=tes.slice(0,8);
                    dataNow.nigEndTimeSecond=tes.slice(11);
                }
                dispatchTimeSetting.updateTime(dataNow);
            });
        },
        judgeTimeFn: function(type) {
            updateFlag = false;
            let timeVal = '';
            let errorMsg = '';
            if(type == 1){
                timeVal = $("#morningTime").val(); //早班时间
            };
            if(type == 2){
                timeVal = $("#nightTimeFirst").val(); //晚班时间-
            };
            if(type == 3){
                timeVal = $("#nightTimeSecond").val(); //晚班时间二
            };
            if(!timeVal || timeVal==''){
                if(type == 1){
                    errorMsg = '请选择早班时间！'; //早班时间
                }else {
                    errorMsg = '请选择晚班时间！';//晚班时间
                };
                updateFlag = true;
                utils.showMessage(errorMsg);
                return;
            }else{
                let ts="2019-10-10 "+timeVal.slice(0,8); //-开始-time
                let tend="2019-10-10 "+timeVal.slice(10);//-结束-time

                let stTimeVal = new Date(ts).getTime();
                let etTimeVal = new Date(tend).getTime();

                if(stTimeVal>=etTimeVal){
                    if(type == 1){
                        errorMsg = '您设定的早班时间范围应--结束时间应晚于开始时间！'; //早班时间
                    }else {
                        errorMsg = '您设定的晚班时间范围应--结束时间应晚于开始时间！';//晚班时间
                    };
                    updateFlag = true;
                    utils.showMessage(errorMsg);
                    return;
                }
            };
            if(type==1){
                dispatchTimeSetting.judgeTimeFn(2);
            }else if(type==2){
                dispatchTimeSetting.judgeTimeFn(3);
            }
        },
        getServerData: function () {
            let datas = data;
            win.utils.ajaxPost(win.utils.services.sche_get_time, datas, function (result) {
                if (result.result_code == 200) {
                    let obj = result.workTime;
                    // 排班时间类型（1、早班；2、晚班）workTimeType;
                    // 上班的开始时间startTime;
                    // 班的结束时间endTime;
                    // 班别次数sortWorkTime;
                    let stTime= '';
                    let endFirstTime='';
                    let endSecondTime='';

                    $.each(obj,function (index,value) {
                        if(value.workTimeType==1){
                            if(value.startTime&&value.endTime){
                                stTime=value.startTime+"—"+value.endTime;
                            };
                        }
                        if(value.workTimeType==2){
                            if(value.sortWorkTime==1){
                                if(value.startTime&&value.endTime){
                                    endFirstTime=value.startTime+"—"+value.endTime;
                                };
                            }
                            if(value.sortWorkTime==2){
                                if(value.startTime&&value.endTime){
                                    endSecondTime=value.startTime+"—"+value.endTime;
                                };
                            }
                        }
                    });

                    $("#morStart").html(stTime);
                    $("#nightEnd").html(endFirstTime+"&nbsp;&nbsp;"+endSecondTime);

                } else {
                    utils.showMessage('获取上班时间数据失败，请重试！');
                }
            });
        },
        // 修改
        updateTime: function (data) {
            win.utils.ajaxPost(win.utils.services.sche_set_time, data, function (result) {
                if (result.result_code == 200) {
                    dispatchTimeSetting.getServerData();
                    utils.showMessage('设置成功！');
                } else {
                    let msg = result.description?result.description:'设置数据失败，请重试！';
                    utils.showMessage(msg);
                }
            });
        },
        //最晚时间赋值
        setLastTime: function(obj){
            // LastTime
            if(obj.morEndTime&&obj.nigEndTime){
                let dataA = "2019-10-10 "+obj.morEndTime;
                let dataB = "2019-10-10 "+obj.nigEndTime;

                let dataAval = new Date(dataA).getTime();
                let dataBval = new Date(dataB).getTime();

                if(dataAval>dataBval){
                    LastTime = obj.morEndTime;
                }else if(dataAval<dataBval){
                    LastTime = obj.nigEndTime;
                }else{
                    LastTime = obj.nigEndTime;
                }
            };
        }
    };

    win.dispatchTimeSetting = dispatchTimeSetting;
})(window, jQuery);
