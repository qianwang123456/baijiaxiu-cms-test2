/**
 * Created by song.
 * desc：选中订单清空数据
 */
(function (win, $) {
    var order_clear = {
        //选中订单---请求工单详情before清空订单所有操作以及详情展示
        clearDetailAndBtn() {
            $("#AllBtnAction").empty();
            $("#contanerDetail>div").hide();
            $("#tableGqRecord").empty();
            $("#gqCallRecordCon").empty();
            $("#tableSqRecord").empty();
            $("#maintPlanCon").empty();
            $("#MaintCoordRecordCon").empty();
            $("#rentralistCon").empty();
            $("#MaintettlemeSntCon").empty();
            $("#shwxjsBtn").empty();
            $("#MaintettlemeSntCon").hide();//维修结算---审批通过
            $("#shjsfaCon").hide();//维修结算---未审批
        },
        //清除配置清单
        clearQueryConfigList() {
            $("#suite_addressConfig").html('');//公寓
            $("#sign_typeConfig").html('');//合同类型
            $("#decorationTeamConfig").html('');//装修队
            $("#engineering_contractorConfig").html('');//工程商
            $("#origin_electricConfig").html('');//原有家电
            $("#other_markConfig").html('');//其他备注
            $("#created_atConfig").html('');//创建时间
            $("#roomDetail").empty();
            $("#roomName").empty();
        },
        //清除分配
        clearFenpei: function () {
            $("#fenpeiCon .inputNums>em").text('0');
            $("#assignRemark").val('');
        },
        //清空转派维修员（转派维修员与转派供应商）
        clearZpwxy: function () {
            $("#assignRemarkZp").parent('div').find(".inputNums>em").eq(0).text('0');
            $("#assignRemarkZp").val('');//执行备注
            $("#exigencyFlagZpwxy").val('');
            $("#goodsInfoZpwxy").val('');
            $("#bxAreaZpwxy").val('');
            $("#firstOrderNameZpwxy").val('');
            $("#secondOrderNameZpwxy").val('');
            $("#thirdOrderNameZpwxy").val('');
        },
        //清除退撤单
        clearTuiCheDan: function () {
            $("#tuidanCon .inputNums>em").eq(0).text('0');
            $("#tdNote").val('');
            $("#tdReason").val('');//退单
            $("#chedanCon .inputNums>em").eq(0).text('0');
            $("#cdNote").val('');
            $("#cdReason").val('');//撤单
        },
        //清除联系
        clearContact: function () {
            $("#contactCon .inputNums>em").text('0');
            $("#contactCon textarea").val('');
            $("#gaiqiDateCon").val('');
            $("#gaiqiTimeCon").val('');
            $("#contactCon select").val('');
            $("#txtNewDate").val('');
            $("#contactCon .contactBtnShow").hide();
        },
        //清除改期
        clearGaiqi: function () {
            $("#gaiqiCon .inputNums>em").text('0');
            $("#gaiqiCon textarea").val('');
            $("#gaiqiCon input").val('');
            $("#gaiqiDays").html('--');
            $("#gaiqiCon select").val('');// 改期
        },
        //清除创建维修方案
        clearCjwxfa: function () {
            partsTotals = 0;
            $("#projectDesc").val('');
            $("#projectDescBl").html('');
            $("#projectDescCon .inputNums em").text(0)
            $("#projectDescBlCon .inputNums em").text(0)
            $("#projectDescCon").show();
            $("#projectDescBlCon").hide();
            $("#dutyIdentifyA").prop({'checked':false});
            $("#dutyIdentifyB").prop({'checked':true});
            $("#orderStatus").val('6');
            $("#controlPlanCon").show();
            $("#servicePlanId").val('');
            $("#cjwxfaCon textarea").val('');
            $("#cjwxfaCon .inputNums>em").text('0');
            $("#uploadImgBox").empty();
            $("#checkedPartsOwnShow").empty();
            $("#checkedPartsOwnShows").hide();
            $("#cjPartsPrice").empty();
            $("#checkedPartsSelf").empty();
            $("#matinArea").empty();//清除维修区域
            $("#MatinplanFirstClassify").empty();//清除维修一级分类
            $("#MatinplanSecondClassify").empty();//清除维修二级分类
            $("#reasonCon").empty();//清除原因归属
            $("#planCon").empty();//清除方案归属
            $("#shangmenCost").val('');
            $("#jiajiCost").val('');
            $("#otherCost").val('');
        },
        //清除创建关联工单
        clearFqbxd: function () {
            $("#firstGradeRel").val('');
            $("#secondGradeRel").val('');
            $("#thirdGradeRel").val('');
            $("#appointmentTimeRel").val();//预约上门时间段
            $("#userSourceZw").val('1');
            $("#needMaintainLockRel").val('1');
            $("#exigencyFlagRel").val('');
            $("#appointmentDateRel").val('');
            $("#uploadImgBoxFq").empty();
            $("#fqbxdCon .inputNums>em").text('0');
            $("#fqbxdCon textarea").val('');
            $("#awPeopleEdit").val('');
            $("#awPeopleTel").val('');
        },
        //创建租务工单
        clearCjzwgd: function () {
            $("#temaTypeZw").val('1');
            $("#userSourceZw").val('1');
            $("#systemZw").val('1');
            $("#uploadImgBoxZw").empty();
            $("#firstIdZw").empty();
            $("#secondIdZw").empty();
            $("#cjzwgdCon .inputNums>em").text('0');
            $("#cjzwgdCon textarea").val('');
            $("#awPeopleEdit").val('');
            $("#awPeopleTel").val('');
        },
        //清除转派供应商
        clearApgys: function () {
            layerFlag = false;
            $("#thirdOrderName").empty();
            $("#secondOrderName").empty();
            $("#firstOrderName").empty();
            $("#bxArea").val('');
            $("#needMaintainLockZpgys").html('');
            $("#goodsInfoZpgys").empty();
            $("#exigencyFlagZpgys").val('');
            $("#bxmsZp").html('');
            $("#bxmskfZp").val('');
            $.each($("#zpgysCon .inputNums>em"), function (index, value) {
                $("#zpgysCon .inputNums>em").eq(index).text('0');
            });
            $("#zpTypeS").empty();
            $("#zpSupply").empty();
            $("#zpgysCon .contactBtnShow").hide();
            $("#gaiqiDateConZp").val('');
            $("#gaiqiTimeConZp").val('');
            $("#zpNote").val('');
            order_list.refreshRepairman(4);
        },
        //清除结算方案
        clearShjsfa: function () {
            $("#checkGradeJsA").prop("checked", false);
            $("#checkGradeJsB").prop("checked", true);
            $("input[name='responsity']").eq(0).prop("checked", false);
            $("input[name='responsity']").eq(1).prop("checked", false);
            $("input[name='isReleasedJs']").eq(0).prop("checked", false);
            $("input[name='isReleasedJs']").eq(1).prop("checked", true);
            $("#checkRemarkJs").val('');
            $("#checkRemarkJs").parent('div').find('em').eq(0).text(0);
            $("#matinAreaJs").empty()
            $("#reasonConJs").empty()
            $("#planConJs").empty()
        },
        //清除配件信息---添加自定义配件
        clearSelfPart: function () {
            $("#partNameAdd").val('');
            $("#partBrand").val('');
            $("#brandType").val('');
            $("#purchasePrice").val('');
            $("#partsFee").val('');
            $("#addSelfPartName").val('');
            $("#partsNums").val('');
            $("#partUnitAdd").val('');
            $("#uploadImgPart").empty();
        },
        //清除一键转办---yjzbCon
        clearYjzbFn: function () {
            matinSchemeArr = [];
            $("#yjzbName").val('');
            $("#yjzbChildOype").html('');
            $("#yjzbChildFirstClass").html('');
            $("#yjzbChildSecondClass").html('');
            $("#yjzbChildONote").val('');
            $("#yjzbCon .inputNums>em").text('0');
        },

    };
    win.order_clear = order_clear;
})(window, jQuery);
