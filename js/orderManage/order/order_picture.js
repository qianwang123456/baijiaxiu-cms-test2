/**
 * Created by song.
 * desc:上传图片，删除图片，查看大图
 *        查看大图：正旋/逆旋/放大/缩小/拖拽/复原
 */
(function (win, $) {
    var imgArrDataIndex = -1;//大图切换---图片下标
    var current = 0;//图片旋转角度
    var imgArrData = [];//大图切换
    var nameValType = '';//调用图片对象/


    var order_picture = {
        //禁止与释放滚动条
        forbidScroll: function () {
            document.body.style.height = '100%'
            document.body.style['overflow-y'] = 'hidden';
        },
        allowScroll: function () {
            document.body.style.height = 'auto';
            document.body.style['overflow-y'] = 'auto';
        },
        //关闭图片弹层---查看图片大图
        closeSeeImage: function () {
            order_picture.allowScroll();
            let str = 'rotate(0deg)';
            $("#bigImgA").css({'width': 'auto'}).css({'height': '75%'}).css('transform', str);
            $("#box").css({'left': 0}).css({'top': 20});
            $('#showImg').hide();
        },
        //拖拽图片
        moveImage: function () {
            // 图片拖拽效果box是装图片的容器,fa是图片移动缩放的范围
            let box = document.getElementById("box");
            let fa = document.getElementById('father');
            box.onmousedown = function (ev) {
                let oEvent = ev;
                oEvent.preventDefault();//阻止浏览器有一些图片的默认事件
                let disX = oEvent.clientX - box.offsetLeft;
                let disY = oEvent.clientY - box.offsetTop;
                fa.onmousemove = function (ev) {
                    oEvent = ev;
                    oEvent.preventDefault();
                    let x = oEvent.clientX - disX;
                    let y = oEvent.clientY - disY;
                    // 图形移动的边界判断-暂时不需要
                    // x = x <= 0 ? 0 : x;
                    // x = x >= fa.offsetWidth-box.offsetWidth ? fa.offsetWidth-box.offsetWidth : x;
                    // y = y <= 0 ? 0 : y;
                    // y = y >= fa.offsetHeight-box.offsetHeight ? fa.offsetHeight-box.offsetHeight : y;
                    box.style.left = x + 'px';
                    box.style.top = y + 'px';
                }
                // 图形移出父盒子取消移动事件,防止移动过快触发鼠标移出事件,导致鼠标弹起事件失效
                fa.onmouseleave = function () {
                    fa.onmousemove = null;
                    fa.onmouseup = null;
                }
                // 鼠标弹起后停止移动
                fa.onmouseup = function () {
                    fa.onmousemove = null;
                    fa.onmouseup = null;
                }
            }
        },
        //上传图片
        loadLocalImage: function (fileObject, imageID, imageType) {
            if (typeof FileReader == 'undefined') {
                alert("当前浏览器不支持FileReader接口");
                return;
            }
            ;
            var file = {};
            for (var u in fileObject.files) {
                file = fileObject.files[u];
                let imgTypeVal = file.type;
                if (imgTypeVal == 'image/png' || imgTypeVal == 'image/jpeg') {
                    let reader = new FileReader();//新建一个FileReader对象
                    reader.readAsDataURL(file);
                    reader.onload = function (e) {
                        //压缩图片
                        let transImg = new Image();//新建一个img标签（不嵌入DOM节点，仅做canvas操作）
                        transImg.src = e.target.result; //让该标签加载base64格式的原图
                        transImg.onload = function () { //图片加载完毕后再通过canvas压缩图片，否则图片还没加载完就压缩，结果图片是全黑的
                            var that = this;
                            let canvas = document.createElement('canvas'), //创建一个canvas元素
                                context = canvas.getContext('2d'),    //context相当于画笔，里面有各种可以进行绘图的API
                                imageWidth = that.width / 2,    //压缩后图片的宽度，这里设置为缩小一半
                                imageHeight = that.height / 2,    //压缩后图片的高度，这里设置为缩小一半
                                data = ''; //存储压缩后的图片
                            canvas.width = imageWidth;  //设置绘图的宽度
                            canvas.height = imageHeight; //设置绘图的高度
                            //使用drawImage重新设置img标签中的图片大小，实现压缩。drawImage方法的参数可以自行查阅W3C
                            context.drawImage(that, 0, 0, imageWidth, imageHeight);
                            //使用toDataURL将canvas上的图片转换为base64格式
                            data = canvas.toDataURL(imgTypeVal);
                            var blobData = order_picture.dataURItoBlob(data);
                            // 图片上传至七牛云
                            let objTar = null;
                            if (nameValType == 'partImgCj') {
                                objTar = $('#uploadImgPart')//创建维修方案---添加自定义配件---图片
                            } else if (nameValType == 'cjwxfawx') {
                                objTar = $('#uploadImgBox')//创建维修方案
                            } else if (nameValType == 'cjbp') {
                                objTar = $('#uploadImgBoxBp')//创建补配工单
                            } else if (nameValType == 'fqbxd') {
                                objTar = $('#uploadImgBoxFq')//创建报修工单/即关联工单
                            } else if (nameValType == 'cjzw') {
                                objTar = $('#uploadImgBoxZw')//创建租务工单
                            } else {
                                objTar = $('#uploadImgBoxGj')//跟进 (selectIdStaBtn=='genjin')默认
                            }
                            ;
                            let nums = Math.random() + 1;
                            let nowNum = Math.pow(10, nums);//随机两位小数
                            order_picture.qiniuUpload(
                                blobData,
                                'bjx-public-' + new Date().getTime() + parseInt(nowNum),
                                objTar,
                                nameValType
                            );
                            if (nameValType == 'partImgCj') {
                                $('#cardFileuploadffApart').val('')//创建维修方案-图片配件--审核维修结算
                            } else if (nameValType == 'cjwxfawx') {
                                $('#cardFileuploadCj').val('')//创建维修方案
                            } else if (nameValType == 'fqbxd') {
                                $('#cardFileuploadsc').val('')//创建报修工单/即关联工单
                            } else if (nameValType == 'cjzw') {
                                $('#cardFileuploadsa').val('')//创建租务工单
                            }
                            ;
                        };
                    }
                }
            }
            ;
        },
        // base64转blob
        dataURItoBlob: function (base64Data) {
            var byteString;
            if (base64Data.split(',')[0].indexOf('base64') >= 0)
                byteString = atob(base64Data.split(',')[1]);
            else
                byteString = unescape(base64Data.split(',')[1]);
            var mimeString = base64Data.split(',')[0].split(':')[1].split(';')[0];
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ia], {
                type: mimeString
            });
        },
        // 将图片转64
        getImgBase64:function (imgUrl) {
            console.log(imgUrl);
            // window.URL = imgUrl.Substring(0,imgUrl.indexOf('http://images.baijiaxiu.com'));
            // console.log(window.URL);
            var xhr = new XMLHttpRequest();
            xhr.open("get", imgUrl, true);
            // 至关重要
            xhr.responseType = "blob";
            xhr.onload = function () {
                if (this.status == 200) {
                    //得到一个blob对象
                    var blob = this.response;
                    console.log("blob", blob);
                    // 至关重要
                    let oFileReader = new FileReader();
                    console.log(oFileReader,"99");
                    oFileReader.onloadstart = function (e) {

                    };
                    oFileReader.onloadend = function (e) {
                        let base64 = e.target.result;
                        console.log("方式一》》》》》》》》》", base64);
                        $("#downLoadImg a").eq(0).attr('href',base64);

                    };
                    oFileReader.readAsDataURL(blob);
                }
            }
            xhr.send();
        },
        /**七牛云上传图片
         * cardImageUrl-----需要上传图片的bolb地址
         * token-----token
         * fileName-----上传图片的原文件名
         * summernoteObj------显示图片的盒子
         * */
        qiniuUpload: function (cardImageUrl, fileName, nodeObj, part) {
            $.ajax({
                url: win.utils.services.getQiniuToken,
                type: 'get',
                dataType: 'json',
                success: function (res) {
                    var qiniuToken = res.token;
                    var observable = qiniu.upload(
                        cardImageUrl,
                        fileName,
                        qiniuToken,
                        {
                            fname: fileName,
                            params: {}, //用来放置自定义变量
                            mimeType: null
                        }, {
                            useCdnDomain: true,
                            region: null
                        }
                    );
                    observable.subscribe({
                        complete(res) {
                            var qiniuImgUrl = 'http://images.baijiaxiu.com/' + res.key;
                            nodeObj.append('<li>\n' +
                                '<img class="cardImage mySelfImg" style="margin-top: 10px; width: 100px; height: 100px; max-width: 400px; border: 10px solid #EEEEEE;" name="' + qiniuImgUrl + '" src="' + qiniuImgUrl + '" />\n' +
                                '<span class="layui-layer-setwin">×</span>\n' +
                                '</li>');
                            if (part == 'partImgCj') {//配件图片
                                if ($("#uploadImgPart li").length > 40) {
                                    for (var i = $("#uploadImgPart li").length; i--, i > 39;) {
                                        $("#uploadImgPart li").eq(i).remove();
                                    }
                                    ;
                                }
                                ;
                            } else {
                                if (part == 'cjwxfawx') {//创建维修方案-报修图片
                                    if ($("#uploadImgBox li").length > 40) {
                                        for (var i = $("#uploadImgBox li").length; i--, i > 39;) {
                                            $("#uploadImgBox li").eq(i).remove();
                                        }
                                        ;
                                    }
                                    ;
                                } else if (part == 'fqbxd') {//创建关联报修单
                                    if ($("#uploadImgBoxFq li").length > 40) {
                                        for (var i = $("#uploadImgBoxFq li").length; i--, i > 39;) {
                                            $("#uploadImgBoxFq li").eq(i).remove();
                                        }
                                        ;
                                    }
                                    ;
                                } else if (part == 'cjzw') {//创建租务工单
                                    if ($("#uploadImgBoxZw li").length > 40) {
                                        for (var i = $("#uploadImgBoxZw li").length; i--, i > 39;) {
                                            $("#uploadImgBoxZw li").eq(i).remove();
                                        }
                                        ;
                                    }
                                    ;
                                }
                            }
                            ;
                        }
                    });
                }
            });
        }
    };
    //订单相关信息---放大图片
    $("#showBtnConNav").off('click').on('click', '.mySelfImg', function () {
        order_picture.forbidScroll();
        $('#showImg').show();
        let imgSrc = $(this).attr('name');
        $("#bigImgA").attr('src', imgSrc).css({'width': 'auto'}).css({'height': '75%'});
        if (imgSrc == 'bigImg') {
            return;
        }
        ;
        imgArrData = [];
        $.each($(this).parents('.form-group').find('img'), function (index, value) {
            let is = $(this).parents('.form-group').find('img').eq(index).attr('name');
            imgArrData.push(is);
        });//fa-refresh复原
        imgArrDataIndex = $(this).parents('.form-group').find('img').index($(this));
        $('#showImg').animate(
            {
                width: '100%',
                height: '100%'
            }, 10, function () {
                $(this).fadeIn('fast')
            }
        );
        $('#showImg img').animate(
            {
                width: 'auto',
            }, 10, function () {
                $(this).fadeIn('fast')
            }
        );
        if (imgArrDataIndex <= 0) {
            $('#showImg em.pre').addClass('grayS');
            if (imgArrDataIndex >= imgArrData.length - 1) {
                $('#showImg em.next').addClass('grayS');
            } else {
                $('#showImg em.next').removeClass('grayS');
            }
        } else if (imgArrDataIndex == (imgArrData.length - 1)) {
            $('#showImg em.next').addClass('grayS');
            if (imgArrDataIndex == 0) {
                $('#showImg em.pre').addClass('grayS');
            } else {
                $('#showImg em.pre').removeClass('grayS');
            }
        } else {
            $('#showImg em').removeClass('grayS');
        }
        ;
    });
    //订单相关操作---放大图片
    $("#bomBtnCon").off('click').on('click', '.mySelfImg', function () {
        order_picture.forbidScroll();
        $('#showImg').show();
        let imgSrc = $(this).attr('name');
        $("#bigImgA").attr('src', imgSrc).css({'width': 'auto'}).css({'height': '75%'});
        if (imgSrc == 'bigImg') {
            return;
        }
        ;
        imgArrData = [];
        $.each($(this).parents('.form-group').find('img'), function (index, value) {
            let is = $(this).parents('.form-group').find('img').eq(index).attr('name');
            imgArrData.push(is);
        });//fa-refresh复原
        imgArrDataIndex = $(this).parents('.form-group').find('img').index($(this));
        $('#showImg').animate(
            {
                width: '100%',
                height: '100%'
            }, 10, function () {
                $(this).fadeIn('fast')
            }
        );
        $('#showImg img').animate(
            {
                width: 'auto',
            }, 10, function () {
                $(this).fadeIn('fast')
            }
        );
        if (imgArrDataIndex <= 0) {
            $('#showImg em.pre').addClass('grayS');
            if (imgArrDataIndex >= imgArrData.length - 1) {
                $('#showImg em.next').addClass('grayS');
            } else {
                $('#showImg em.next').removeClass('grayS');
            }
        } else if (imgArrDataIndex == (imgArrData.length - 1)) {
            $('#showImg em.next').addClass('grayS');
            if (imgArrDataIndex == 0) {
                $('#showImg em.pre').addClass('grayS');
            } else {
                $('#showImg em.pre').removeClass('grayS');
            }
        } else {
            $('#showImg em').removeClass('grayS');
        }
        ;
    });
    //配件库管理---报备配件---放大图片
    $("#reparPartList").off('click').on('click', '.mySelfImg', function () {
        order_picture.forbidScroll();
        $('#showImg').show();
        let imgSrc = $(this).attr('name');
        // order_picture.getImgBase64(imgSrc);
        $("#bigImgA").attr('src', imgSrc).css({'width': 'auto'}).css({'height': '75%'});
        if (imgSrc == 'bigImg') {
            return;
        };
        $('#showImg').animate(
            {
                width: '100%',
                height: '100%'
            }, 10, function () {
                $(this).fadeIn('fast')
            }
        );
        $('#showImg img').animate(
            {
                width: 'auto',
            }, 10, function () {
                $(this).fadeIn('fast')
            }
        );
    });

    //图片滚动放大缩小
    $("#showImg").on("mousewheel", "#bigImgA", function (ev) {
        var oImg = this;
        var ev = event || window.event;//返回WheelEvent
        var delta = ev.detail ? ev.detail > 0 : ev.wheelDelta < 0;
        var ratioL = (ev.clientX - oImg.offsetLeft) / oImg.offsetWidth,
            ratioT = (ev.clientY - oImg.offsetTop) / oImg.offsetHeight,
            ratioDelta = !delta ? 1 + 0.1 : 1 - 0.1,
            w = parseInt(oImg.offsetWidth * ratioDelta),
            h = parseInt(oImg.offsetHeight * ratioDelta),
            l = Math.round(ev.clientX - (w * ratioL)),
            t = Math.round(ev.clientY - (h * ratioT));

        if (delta) {
            var fal = $("#box").position().left;
            var fat = $("#box").position().top;
            var faw = $("#box").width();
            var fah = $("#box").height();
            fal = Math.abs(fal * 1);
            fat = Math.abs(fat * 1);
            if (fat > fah || h <= fah) {
                $("#box").css({'top': 20})
            }
            ;
            if (fal > faw || w <= faw) {
                $("#box").css({'left': 50})
            }
            ;
        }
        ;
        with (oImg.style) {
            width = w + 'px';
            height = h + 'px';
            left = l + 'px';
            top = t + 'px';
        }
    });
    //图片拖拽
    $("#showImg").on("mouseover", "#bigImgA", function () {
        order_picture.moveImage();
    });
    //查看图片---前后/旋转/复原/关闭/下载
    $("#btnImgAll").on('click', 'em', function () {
        let nameVal = $(this).attr('name');
        if (nameVal == 'pre') {
            $("#bigImgA").css({'width': 'auto'}).css({'height': '75%'});
            $("#box").css({'left': 0}).css({'top': 20});
            imgArrDataIndex--;
            if (imgArrDataIndex < 0) {
                imgArrDataIndex = 0;
            }
            ;
            let str = 'rotate(0deg)';
            $("#showImg img").css('transform', str);
        } else if (nameVal == 'next') {
            $("#bigImgA").css({'width': 'auto'}).css({'height': '75%'});
            $("#box").css({'left': 0}).css({'top': 20});
            imgArrDataIndex++;
            if (imgArrDataIndex >= imgArrData.length) {
                imgArrDataIndex = imgArrData.length - 1;
            }
            ;
            let str = 'rotate(0deg)';
            $("#showImg img").css('transform', str);
        } else if (nameVal == 'transl') {
            current = (current + 90) % 360;
            let str = 'rotate(' + current + 'deg)';
            $("#showImg img").css('transform', str);
            return;
        } else if (nameVal == 'transr') {
            current = (current - 90) % 360;
            let str = 'rotate(' + current + 'deg)';
            $("#showImg img").css('transform', str);
            return;
        } else if (nameVal == 'refresh') {
            let str = 'rotate(0deg)';
            $("#showImg img").css('transform', str);
            $("#bigImgA").css({'width': 'auto'}).css({'height': '75%'});
            $("#box").css({'left': 0}).css({'top': 20});
            return;
        } else if (nameVal == 'closeImg') {
            order_picture.closeSeeImage();
            return;
        }else if (nameVal == 'download') {
            let imgUrl=$("#bigImgA").attr('src');
            downloadImg(imgUrl);
            return;
        };
        if (imgArrDataIndex <= 0) {
            $('#showImg em.pre').addClass('grayS');
            if (imgArrDataIndex >= imgArrData.length - 1) {
                $('#showImg em.next').addClass('grayS');
            } else {
                $('#showImg em.next').removeClass('grayS');
            }
        } else if (imgArrDataIndex >= imgArrData.length - 1) {
            $('#showImg em.next').addClass('grayS');
            if (imgArrDataIndex == 0) {
                $('#showImg em.pre').addClass('grayS');
            } else {
                $('#showImg em.pre').removeClass('grayS');
            }
        } else {
            $('#showImg em').removeClass('grayS');
        }
        ;
        imgSrcVal = imgArrData[imgArrDataIndex];
        $("#showImg img").eq(0).attr('src', imgSrcVal);
    });
    //删除图片---创建维修方案/审核结算方案---自定义配件图片
    $('#uploadImgPart').on('click', 'span', function () {
        $(this).parents('li').remove();
    });
    //删除图片---创建维修方案----报修图片
    $('#uploadImgBox').on('click', 'span', function () {
        $(this).parents('li').remove();
    });
    //删除图片---创建关联工单
    $('#uploadImgBoxFq').on('click', 'span', function () {
        $(this).parents('li').remove();
    });
    //删除图片---创建租务工单
    $('#uploadImgBoxZw').on('click', 'span', function () {
        $(this).parents('li').remove();
    });
    // 触发上传图片
    $(".btnLoadImg").on('click', function () {
        nameValType = $(this).attr('name');
        if (nameValType == 'cjwxfawx') {//创建维修方案
            if ($('#uploadImgBox li').length >= 40) {
                utils.showMessage('图片最多可上传40张！');
            } else {
                $("#cardFileuploadCj").click();
            }
        } else if (nameValType == 'partImgCj') {//配件图片：1-创建维修方案；2-审核维修结算
            if ($('#uploadImgPart li').length >= 40) {
                utils.showMessage('图片最多可上传40张！');
            } else {
                $("#cardFileuploadffApart").click();
            }
            ;
        } else if (nameValType == 'fqbxd') {//创建关联维修单
            if ($('#uploadImgBoxFq li').length >= 40) {
                utils.showMessage('图片最多可上传40张！');
            } else {
                $("#cardFileuploadsc").click();
            }
        } else if (nameValType == 'cjzw') {//创建租务工单
            if ($('#uploadImgBoxZw li').length >= 40) {
                utils.showMessage('图片最多可上传40张！');
            } else {
                $("#cardFileuploadsa").click();
            }
            ;
        }
    });
    win.order_picture = order_picture;
})(window, jQuery);
