/**
 * Created by song.
 */
(function(win, $) {
	//延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
	var searchStaFlag=false;//默认按搜索状态
	var flagInput=false;//空格输入按钮
	var exigencyFlag=0;
	var ConFlag=-1;//弹层关闭
	var deferLoadData = 0;
	var orderByString = '';
	var selectId='';	//table-选中的订单id
	var selectIdSta=null;	//table-选中的订单---对应的状态-id
	var selectIdStaBtn=null;	//table-选中的订单-按钮---对应的状态(text-汉字)
	var levelFlag='';//区域查询时，levelFlag:1,//区域标识id（1、城市；2、区域；3、商圈；4、小区） ,
	var regionFlag=true;	//区域查询时，regionFlag(开关)
	var queryAdressFlag=true;//公寓地址-query开关
	var selectAdressId='';//公寓地址-选中地址-id
	var layerFlag=false;//详情弹层关闭开关
	var cityIdVal=null;//分配师傅时-城市idregionId: 1
	var secondTypeId=null;//分配师傅时-二级分类ID
	var orderIdOther=null;//补配工单时-三方工单Id
	var orderOrginSelf = '';// 创建维修方案-保存订单id-(未标明三方id,就用自己的id)
	var cardImageUrl = '';//上传图片---
	var cardImageSelected;//上传图片===
	var workIdZp='';//转派师傅时，维修员id
	var workIdZpFlag=true;//转派师傅时，模糊搜索开关
	var partsData=null;//创建维修方案-配件库
	var partsFirstIdVal=null;//创建维修方案-配件-一级类型ID
	var partsSecondIdVal=null;//创建维修方案-配件-二级类型ID
	var partsCheckedCjSelf=[];//创建维修方案-配件-已选择配件---自己添加---修改后
	var partsCheckedCjOwnAf=[];//创建维修方案-配件-已选择配件---配件库---修改后
	var partsCheckedCjOwnBf=[];//创建维修方案-配件-已选择配件---配件库---修改前
	var partsTotals=0;//创建维修方案-配件总价-审核结算方案
	var isExistLinkorder=null;//工单详情-是否存在关联工地
	var linkedNo='';//工单详情-关联工单ID
	var partsArrData=[];//配件数组---类型-partcode
	var partsCheckedJsSelf=[];//审核结算方案-配件-已选择配件---自己添加---
	var partsCheckedJsOwnAf=[];//审核结算方案-配件-已选择配件---配件库---修改后
	var partsCheckedJsOwnBf=[];//审核结算方案-配件-已选择配件---配件库---修改前
	var idPlanJs=null;//审核结算方案id-提交时用
	var citySearchVal='';//搜搜城市id
	var citySearchNameVal='';//搜搜城市name
	var orderIdValSear='';//搜索工单id
	var orderIdValSears='';//搜索工单id-sure
	var repairTelValSear='';//报修电话
	var repairTelValSears='';//报修电话--sure
	var adressValSear='';//公寓地址
	var adressValSears='';//公寓地址---sure
	var progressValSear='';//进度状态
	var progressValSears='';//进度状态---sure
	var OrderFlagValSear='';//工单标签
	var OrderFlagValSears='';//工单标签--sure
	var districtId='';// 区域id ,
	var houseIdcreate = ''; //房间id-cjwxfa
	var secondGradecreate = ''; //二级分类id-cjwxfa
	var firstGradecreate = ''; //一级分类id-cjwxfa
	var tradingAreaSearch='',//商圈名称 ,
		tradingAreaIdSearch='',//商圈id ,
		blockIdSearch='',//小区id ,
		exigencyFlagSearch='',//紧急标识(1、紧急；2、普通) ,
		firstGradeSearch='',//一级工单类型id ,
		secondGradeSearch='',//二级工单类型id ,
		linkOrderStatus='',//关联的工单状态 ,
		houseIdSearch='',//公寓id ,===暂无
		houseIdSearchs='';//公寓id ,==sure
	var orderFpTimeSt='';//分派时间开始
	var orderFpTimeEn='';//分派时间结束
	var orderYyTimeSt='';//预约时间开始
	var orderYyTimeEn='';//预约时间结束
	var orderFinTimeSt='';//完成时间开始
	var orderFinTimeEn='';//完成时间结束
	var orderCreTimeSt='';//创建时间开始
	var orderCreTimeEn='';//创建时间结束
	var orderContTimeSt='';//联系时间开始
	var orderContTimeEn='';//联系时间结束
	var orderDoorTimeSt='';//上门时间开始
	var orderDoorTimeEn='';//上门时间结束
	var servicePlanSer='';//维修方案类型
	var finishStatusSer='25';//工单是否完结
	var isChangeDateSer='';//是否改期上门
	var sysNinetyReworkSer='';//90天返修工单
	var jsCheckUpgradeSer='';//结算是否升级审核
	var workerIdsArr=[];//模糊搜索订单列表时---arr维修员--上限5个---未确定
	var workerIdsArrs=[];//模糊搜索订单列表时---arr维修员--上限5个---确定
	var notnormal='',//一异常---search
		//end---search
		tradingAreaId= -1 ,
		changeDateCom='',//改期日期对比
		changeTimeCom='',//改期时间对比
		providerArr=[],//供应商数据
		newPlan={},//最新维修方案
		visitFeeArr=[],//上门费用Arr
		urgentFeeArr=[],//紧急费用Arr
		otherFeeArr=[],//其他费用Arr
		visitFeeSelected='',//上门费用-selected
		urgentFeeSelected='',//紧急费用-selected
		otherFeeSelected='',//其他费用-selected
		districtArr=[],//区ID
		businessArr=[],//商圈ID
		transDate='',//中转时间
		dankeOrderId='',//蛋壳id
		yjzbPlanId='',//维修方案id
		servicePlanId='',//协调分类id,
		servicePlanName='',//协调分类text
		isCreateSub=false,//是否创建子工单
		yjzbDescFault='',//一键转办---报修描述
		newestChecksta=0,//在查询工单详情里---最新维修方案状态---
		feedbackNo=0,//反馈次数
		matinSchemeArr=[];//查询维修方案分类
	var  nowCurPage= 0;//订单列表---当前页码数
	var  totalNowPages = 0;//订单列表---总的页码数
	var inputPageFlag=false;//订单列表---输入页码跳转
	var MatinPlanTemList = [];//维修方案模板
	var MatinPlanTemListName = [];//维修方案模板名称
	var timeoutFlag = true;//查询蛋壳租务工单状态时间查询开关-true允许请求，false不允许请求
	var timeNow = 5;//时间5秒倒计时
	var timeObj;//时间5秒倒计时函数
	var tancenObj;//弹层模板
	var isShowNintyFlag=false;//结算待审核---根据是否有90天返修标签来展示90天返修
	var mainPlanJsonArr = [] // 创建维修方案-维修方案编辑时变量的json数据
	var isExistPartsBfFlag = false ; // 是否有配件  true==有，false== 无
	var judgeIsHasSecondIdFlag = true ; // 默认  有=true，false=无（无的情况，无需校验原因归属与方案归属）
	var judgeReasonArr = []; //判断原因归属方案是否配置
	var judgePlanArr = []; //判断方案归属方案是否配置
	var reasonBelongIdArrOrgin = [];//结算待审核，查询结算方案时的原因归属保留的id
	var planBelongIdArrOrgin = [];//结算待审核，查询结算方案时的方案归属保留的id
	var initiateRepairAreaVal = ''  //报修区域
	// 初始化
	function initializeEditForm() {
		$(".form-group").removeClass("has-error");
		cardImageUrl = '';
		cardImageSelected = 0;
	}
	function clearAllDataNow(){
		//强制清空所有数据
		dankeOrderId='';
		yjzbPlanId='';//维修方案id
		servicePlanId='';//协调分类id,
		servicePlanName='';//协调分类text
		exigencyFlag=null;
		ConFlag=-1;//弹层关闭
		orderByString = '';
		selectId='';	//table-选中的订单id
		selectIdSta=null;	//table-选中的订单---对应的状态-id
		selectIdStaBtn=null;	//table-选中的订单-按钮---对应的状态(text-汉字)
		layerFlag=false;//详情弹层关闭开关
		cityIdVal=null;//分配师傅时-城市idregionId: 1
		secondTypeId=null;//分配师傅时-二级分类ID
		orderIdOther=null;//补配工单时-三方工单Id
		cardImageUrl = '';//上传图片---
		workIdZp='';//转派师傅时，维修员id
		workIdZpFlag=true;//转派师傅时，模糊搜索开关
		partsData=null;//创建维修方案-配件库
		partsFirstIdVal=null;//创建维修方案-配件-一级类型ID
		partsSecondIdVal=null;//创建维修方案-配件-二级类型ID
		partsCheckedCjSelf=[];//创建维修方案-配件-已选择配件---自己添加---修改后
		partsCheckedCjOwnAf=[];//创建维修方案-配件-已选择配件---配件库---修改后
		partsCheckedCjOwnBf=[];//创建维修方案-配件-已选择配件---配件库---修改前
		partsTotals=0;//创建维修方案-配件总价-审核结算方案
		isExistLinkorder=null;//工单详情-是否存在关联工地
		linkedNo='';//工单详情-关联工单ID
		partsArrData=[];//配件数组---类型-partcode
		partsCheckedJsSelf=[];//审核结算方案-配件-已选择配件---自己添加---
		partsCheckedJsOwnAf=[];//审核结算方案-配件-已选择配件---配件库---修改后
		partsCheckedJsOwnBf=[];//审核结算方案-配件-已选择配件---配件库---修改前
		idPlanJs=null;//审核结算方案id-提交时用
		changeDateCom='';//改期日期对比
		changeTimeCom='';//改期时间对比
		providerArr=[];//供应商数据
		newPlan={};//最新维修方案
		visitFeeArr=[];//上门费用Arr
		urgentFeeArr=[];//紧急费用Arr
		otherFeeArr=[];//其他费用Arr
		visitFeeSelected='';//上门费用-selected
		urgentFeeSelected='';//紧急费用-selected
		otherFeeSelected='';//其他费用-selected
		transDate='';//中转时间
	}
	var setDataSix=null;//定时对象
	function transTime(bpDateVal){
		bpDateVal=new Date(bpDateVal);
		let bpYear=bpDateVal.getFullYear();
		let bpMonth=bpDateVal.getMonth()+1;
		bpMonth=bpMonth<=9?'0'+bpMonth:bpMonth;
		let bpDay=bpDateVal.getDate()<=9?'0'+bpDateVal.getDate():bpDateVal.getDate();
		let bpHour=bpDateVal.getHours()<=9?'0'+bpDateVal.getHours():bpDateVal.getHours();
		let bpMin=bpDateVal.getMinutes()<=9?'0'+bpDateVal.getMinutes():bpDateVal.getMinutes();
		let bpSec=bpDateVal.getSeconds()<=9?'0'+bpDateVal.getSeconds():bpDateVal.getSeconds();
		transDate=bpYear+"-"+bpMonth+"-"+bpDay+" "+bpHour+":"+bpMin+":"+bpSec;
	}
	//定时获取--分配--刷新
	function setIntervalRefreshGetData(){
		//判断是否在iframe中
		setDataSix=setInterval(function(){
			let arr=[];
			$.each(parent.$('iframe'), function (index) {
				let srcVal=parent.$('iframe').eq(index).attr('src');
				arr.push(srcVal);
			});
			if(arr.indexOf('web/order/order_list.html')<0){
				clearInterval(setDataSix);
			}else{
				order_list.getStaNum();
				if (progressValSears === 2 && ConFlag < 0) {
					order_list.getServerData(1);
				}
			}
		}, 12000);
	}
	////// setIntervalRefreshGetData();//定时刷新数据---待分配

	//---创建租务---定时获取蛋壳返回结果---（5s倒计时）
	function fiveSecondsTimeout(){
		timeObj=setInterval(function(){
			if(timeNow<1){
				clearInterval(timeObj);
			}else{
				if (timeoutFlag == true) {
					order_list.queryDankeResult();
				}
			}
		},1000);
	}
	//关闭弹层
	function closeFlagCon(){
		layer.close(ConFlag);
		order_picture.allowScroll();
	}
	//审核结算-计算工单费用
	function setAllPriceJs(){
		let a=Number($("#otherMoneyJs").val()),//其他费用
			b=Number($("#urgentMoneyJs").val()),// 加急费用
			c=Number($("#indoorJs").val()),//上门费用
			d=Number($("#partsTotalCostJs").val());//配件费用
		let allD=Number(a+b+c+d).toFixed(2);
		$("#partsFeeJs").text(allD);//工单费用-未扣除
	}
	// 动态赋值---维修方案内容-手动改变时赋值
	function activeTransText(){
		mainPlanJsonArr = [];
		$.each($("#projectDescBl .highLight"),function(i,k){
			let jsonDataStr = $("#projectDescBl .highLight").eq(i).attr('name');
			let jsonData =  $.parseJSON(jsonDataStr)
			mainPlanJsonArr.push(jsonData)
		})
		let textVal = $("#projectDescBl").text().length
		$("#projectDescBl").parent().find('.inputNums em').eq(0).text(textVal)
	}
	// 动态反赋值---维修方案内容-首次获取时将已有值传递给input
	function activeNextTransText(){
		mainPlanJsonArr = [];
		$("#projectDescBl .highLight").each(function(i,k){
			let nameJsonVal =$("#projectDescBl .highLight").eq(i).attr('name');
			nameJsonVal =$.parseJSON(nameJsonVal)
			mainPlanJsonArr.push(nameJsonVal)
		})
		let textVal = $("#projectDescBl").text().length
		$("#projectDescBl").parent().find('.inputNums em').eq(0).text(textVal)
	}
	// 注释-字段    // orderStatus: 订单状态
	// 1:待认领  2：待分配  3：待联系  4：待上门   5：已上门 (待反馈)  6：维修方案协调中
	// 7:已完成  8：未完成  9：结算待审核  10：结算审核通过   11：已评价   12：已撤销
	// 13：退单中  14：撤单中 15：已退单  16:已转派供应商  20：协调今日待跟进
	//25:未完结工单   26：完结工单
	//对应list-btn权限  //id="workOrderDetail" 工单详情  权限：all
	//id="configInfo" 装修配置信息 权限：all（7:已完成  8：未完成没有）
	//id="housrSouce" 房源信息（暂时去掉）  权限：all
	//id="followUpRecord"  跟进记录  权限： all(有数据-展示-7:已完成，8：未完成---没有)--//id="actRecord"  操作记录  权限： all合并
	//id="contactResult"  改期记录  权限： (2,3，7:已完成，8：未完成---没有)
	//id="authorizationList"  授权列表  权限：(4,5,6,9,10,11,待确定)
	//id="maintPlan"  维修方案  权限： (6,9,10,11)
	//id="MaintCoordRecord"  维修协调记录  权限： (6,9,10,11)
	//id="rentralist"  租务工单  权限：(有数据-展示) (6,9,10,11)
	//id="repairSheet"  补配工单  权限：(有数据-展示) (6,7,8,9,10,11)-暂去掉
	//id="repairlist"  报修工单 （关联维修工单） 权限： (有数据-展示)(6,9,10,11)
	//id="MaintettlemeSnt"  维修结算  权限：(9,10,11)
	//id="quitDetail" 退单详情  权限(有数据-展示)7:已完成，8：未完成---没有)
	//id="backDetail" 撤单详情  权限(有数据-展示)7:已完成，8：未完成---没有)
	//对应工单详情-不同状态对应不同btn操作
	// id="tuidan"退单：权限：2，3，4，5，6     // id="chedan"撤单   权限：2，3，4，5，6
	// id="zpgys"    转派供应商   权限：2，3，4，5，6   // id="contact"    联系   权限：3
	// id="gaiqi"    改期  权限：4    // id="fenpei"    分配维修员   权限：2
	// id="shangmen"    上门   权限：4   // id="sqmmsq"    申请门密授权  权限：4，5，6
	// id="zpwxy"    转派维修员  权限：3，4，5，6   // id="genjin"    跟进   权限：2，3，4，5，6----去掉
	// id="cjwxfa"    创建维修方案   权限：5，6    // id="fqbxd"    创建关联维修单   权限：6
	// id="cjbpgd"    创建补配工单   权限：6       // id="cjzwgd"    创建租务工单   权限：6
	// id="shjsfa"    审核结算方案   权限：9
	// detail-btn
	var order_list = {
		//orderMan_order_judgeFee---费用校验
		orderMan_order_judgeFee(type,orderSta){
			$("#shangmenCost").parent('div').find('em').eq(0).text('');
			$("#jiajiCost").parent('div').find('em').eq(0).text('');
			$("#otherCost").parent('div').find('em').eq(0).text('');
			if(orderSta == 1){
				if(type == 1){
					costVal=$("#shangmenCost").val();
				}else if(type == 2){
					costVal=$("#jiajiCost").val();
				}else if(type == 3){
					costVal=$("#otherCost").val();
				}
			}else{
				// if(type == 1){
				// 	costVal=$("#indoorJs").val();
				// }else if(type == 2){
				// 	costVal=$("#urgentMoneyJs").val();
				// }else if(type == 3){
				// 	costVal=$("#otherMoneyJs").val();
				// };
				setAllPriceJs();
				return;
			}
			if (costVal == '') {
				costVal=0.00;
			}
			let data={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				costType:type,//1上门2加急3其他
				cost:costVal,
				orderId:selectId,
				planPartsDTOList:partsCheckedCjOwnAf,
			};
			// orderSta   1:创建维修方案 2：审核结算方案
			win.utils.ajaxPost(win.utils.services.orderMan_order_judgeFee, data, function(result) {
				if(result.result_code == 200) {
					//是否允许选择费用
					if(result.data){
						if(result.data.status == 0){
							if(orderSta == 1){
								if(type == 1){
									$("#shangmenCost option:first").prop("selected", 'selected');
									$("#shangmenCost").parent('div').find('em').eq(0).text(result.data.description);
								}else if(type == 2){
									$("#jiajiCost option:first").prop("selected", 'selected');
									$("#jiajiCost").parent('div').find('em').eq(0).text(result.data.description);
								}else if(type == 3){
									$("#otherCost option:first").prop("selected", 'selected');
									$("#otherCost").parent('div').find('em').eq(0).text(result.data.description);
								}
							}
						}else{
							if(orderSta == 2){
								setAllPriceJs();
							}
						}
					}
				}else{
					let errorMsg = result.description ? result.description : '哎呀，失败了！！！请稍后重试。。。';
					utils.showLayerMessage(errorMsg);
				}
			})
		},
		// 查询维修区域---房间号-platform/basicsInfo/getSuiteArea
		querySuiteArea(type,strVal){   //type1:cj创建  2结算审核   strVal表示回填值
			win.utils.ajaxPost(win.utils.services.querySuiteArea, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				suiteId:houseIdcreate
			}, function(result) {
				if(result.result_code == 200) {
					if (result.data.area) {
						let obj = result.data.area
						let str = '<option value="">请选择</option>';
						$.each(obj, function (index, value) {
							let selectVal = ''
							if (strVal && value == strVal) {
								selectVal = 'selected'
							}
							str += `<option ` + selectVal + `>` + value + `</option>`
						});
						if(type==1){
							$("#matinArea").empty().append(str);
						}else if(type==2){
							$("#matinAreaJs").empty().append(str);
						}
					}else{
						utils.showLayerMessage('未查询到维修区域！');
					}
				}else{
					let errorMsg = result.description ? result.description : '哎呀，失败了！！！请稍后重试。。。';
					utils.showLayerMessage(errorMsg);

				}
			})
		},
		// 查询维修分类一二级---rank:等级，rank为2，type存在且为1的话，就是请求一级分类下的二级分类，否则就是请求父级id,diff为1表示,diff为2表示切换，不需回填
		// btnAct-创建-1，结算-2
		// rank -等级1，2，3
		// type-1:请求一级分类下的二级，-2：请求二级分类的父级id（根据rank决定）
		// idVal-请求二级分类的父级id,二级分类id
		// diff-1:表示请求，回填选中值  2：表示切换，无需回填
		queryMatinClassify(btnAct,rank,type,idVal,diff){ //btnAct1:表示创建 2：表示审核结算
			let data = {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				status: 1,
				rank: rank,
			}
			if(rank == 2){
				if(type==1){
					data.firstClassifyId = idVal;
				}else{
					data.serviceId =secondGradecreate
				}
			}
			win.utils.ajaxPost(win.utils.services.configMan_firstClassWorkOrder_list, data, function(result) {
				if(result.result_code == 200) {
					let obj = result.serviceItems?result.serviceItems:[];
					let str = ''
					if(type==2 && rank==2){
						if(obj.length>0){
							firstGradecreate = obj[0].parentId
							order_list.queryMatinClassify(btnAct,1)
							order_list.queryMatinClassify(btnAct,2,1,firstGradecreate,1)
							if(btnAct == 1){
								$("#MatinplanFirstClassify").val(firstGradecreate);
							}else if(btnAct == 2){
								$("#MatinplanFirstClassifyJs").val(firstGradecreate);
							}
						}
					}else{
						if(rank ==1){
							str = "<option value=''>请选择一级分类</option>";
						}else if(type==1 && rank==2){
							str = "<option value=''>请选择二级分类</option>";
						}
						$.each(obj, function (index, value) {
							str += "<option value='" + value.serviceId + "'>" + value.name + "</option>"
						});
						if(btnAct==1){
							if(rank==1){
								// console.log(firstGradecreate,'88888888')
								$("#MatinplanFirstClassify").empty().append(str);
								if(firstGradecreate&&firstGradecreate!=''){
									$("#MatinplanFirstClassify").val(firstGradecreate);
								}
							}else{
								$("#MatinplanSecondClassify").empty().append(str);
								if(diff==1){
									$("#MatinplanSecondClassify").val(secondGradecreate);
								}else if(diff==2){
									secondGradecreate=''
								}
							}
						}else if(btnAct==2){
							if(type==4){
								if(obj.length>0){
									let firstNameVal = obj[0].firstClassifyName?obj[0].firstClassifyName+'——':'';
									let secondNameVal = obj[0].name?obj[0].name:'';
									let strVal = firstNameVal+secondNameVal;
									$("#secGradeJsSuc").html(strVal)
								}
							}else{
								if(rank==1){
									$("#MatinplanFirstClassifyJs").empty().append(str);
									// console.log(firstGradecreate,'8888888eeeeeeeeeeeeeee8')
									if(firstGradecreate&&firstGradecreate!=''){
										$("#MatinplanFirstClassifyJs").val(firstGradecreate);
									}
								}else{
									$("#MatinplanSecondClassifyJs").empty().append(str);
									if(diff==1){
										$("#MatinplanSecondClassifyJs").val(secondGradecreate);
									}else if(diff==2){
										secondGradecreate=''
									}
								}
							}

						}
					};
				}
			})
		},
		// 根据id获取原因归属维修方案list
		getReasonMatinplanList : function(btnAct,idval,type,transData) {  //btnAct1:创建  2：审核结算
			//type为1表示查询最新维修方案时查询
			win.utils.ajaxPost(win.utils.services.getConfigReasonPlanDetail, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				serviceId :idval,
				status:1
			}, function(result) {
				if(result.result_code == 200) {
					//1原因归属  2方案归属
					let data = result.data
					if(data.length<=0&&idval){
						judgeReasonArr = []; //判断原因归属方案是否配置
						judgePlanArr = []; //判断方案归属方案是否配置
						$("#addReasonPlanBtnJSCon").hide()
						$("#addReasonPlanBtnCon").hide()
						// utils.showMessage('该维修二级分类尚未配置原因归属与方案归属，请配置后选用！');
						// return;
					}
					let reasonArrHtml = ``
					let matinPlanArrHtml = ``
					reasonBelongIdArrOrgin = [];//结算待审核，查询结算方案时的原因归属保留的id
					planBelongIdArrOrgin = [];//结算待审核，查询结算方案时的方案归属保留的id
					judgeReasonArr = []; //判断原因归属方案是否配置
					judgePlanArr = []; //判断方案归属方案是否配置
					data.map(function(i,v){
						if(i.type==1){
							judgeReasonArr.push(i); //判断原因归属方案是否配置
							reasonArrHtml+=`<option value="`+i.id+`">`+i.name+`</option>`
						}
						if(i.type==2){
							judgePlanArr.push(i); //判断方案归属方案是否配置
							matinPlanArrHtml+=`<option value="`+i.id+`">`+i.name+`</option>`
						}
					})
					if(btnAct==1){
						reasonArrHtml = `<div class="form-group"><div class="col-sm-10"><select name="servicePlanId" id="reasonBelong" class="form-control">`+reasonArrHtml+`</select></div></div>`
						matinPlanArrHtml = `<div class="form-group"><div class="col-sm-10"><select name="servicePlanId" id="planBelong" class="form-control">`+matinPlanArrHtml+`</select></div></div>`
						if(judgeReasonArr.length>0){
							$("#reasonCons").show()
							$("#reasonCon").empty().append(reasonArrHtml)
						}else{
							$("#reasonCons").hide()
						}
						if(judgePlanArr.length>0){
							$("#planCons").show()
							$("#planCon").empty().append(matinPlanArrHtml)
						}else{
							$("#planCons").hide()
						}
						if(judgeReasonArr.length>0&&judgePlanArr.length>0){
							$("#addReasonPlanBtnCon").show()
						}else{
							$("#addReasonPlanBtnCon").hide()
						}
					}else{
						reasonArrHtml = `<div class="form-group"><div class="col-sm-10"><select name="servicePlanIdJs" id="reasonBelongJs" class="form-control">`+reasonArrHtml+`</select></div></div>`
						matinPlanArrHtml = `<div class="form-group"><div class="col-sm-10"><select name="servicePlanIdJs" id="planBelongJs" class="form-control">`+matinPlanArrHtml+`</select></div></div>`
						if(judgeReasonArr.length>0){
							$("#reasonConsJs").show()
							$("#reasonConJs").empty().append(reasonArrHtml)
						}else{
							$("#reasonConsJs").hide()
						}
						if(judgePlanArr.length>0){
							$("#planConsJs").show()
							$("#planConJs").empty().append(matinPlanArrHtml)
						}else{
							$("#planConsJs").hide()
						}
						if(judgeReasonArr.length>0&&judgePlanArr.length>0){
							$("#addReasonPlanBtnJSCon").show()
						}else{
							$("#addReasonPlanBtnJSCon").hide()
						}
					}
					if(type==1){
						// console.log(transData,'呼呼呼')
						if(transData.length>0){
							//1原因归属  2方案归属
							let reasonIndex  = 0;
							let planIndex = 0;
							$.each(transData,function(i,v){
								if(v.type==1){
									if(reasonIndex==0){

									}else{
										let strReason = ''
										if(btnAct==1){
											strReason = $("#reasonBelong").html()
										}else if(btnAct==2){
											strReason = $("#reasonBelongJs").html()
										}
										strReason = `<div class="form-group"><div class="col-sm-10"><select class="form-control">`+strReason+`</select></div><div class="col-sm-2"><em class="remove"><i class="fa fa-trash-o"></i></em></div></div>`
										// console.log(strReason,strPlan,'999')
										if(btnAct==1){
											$("#reasonCon").append(strReason)
										}else if(btnAct==2){

											$("#reasonConJs").append(strReason)
										}
									}
									if(btnAct==1){
										$("#reasonCon select").eq(reasonIndex).val(v.reasonAndPlanId)
									}else if(btnAct==2){
										reasonBelongIdArrOrgin.push(v.reasonAndPlanId);//结算待审核，查询结算方案时的原因归属保留的id
										$("#reasonConJs select").eq(reasonIndex).val(v.reasonAndPlanId)
									}
									reasonIndex++;
								}
								if(v.type==2){
									if(planIndex==0){

									}else{
										let strPlan = ''
										if(btnAct==1){
											strPlan = $("#planBelong").html()
										}else if(btnAct==2){
											strPlan = $("#planBelongJs").html()
										}
										strPlan =  `<div class="form-group"><div class="col-sm-10"><select class="form-control">`+strPlan+`</select></div><div class="col-sm-2"><em class="remove"><i class="fa fa-trash-o"></i></em></div></div>`
										if(btnAct==1){
											$("#planCon").append(strPlan)
										}else if(btnAct==2){
											$("#planConJs").append(strPlan)
										}
									}
									if(btnAct==1){
										$("#planCon select").eq(planIndex).val(v.reasonAndPlanId)
									}else if(btnAct==2){
										planBelongIdArrOrgin.push(v.reasonAndPlanId);//结算待审核，查询结算方案时的原因归属保留的id
										$("#planConJs select").eq(planIndex).val(v.reasonAndPlanId)
									}
									planIndex++;
								}
								// console.log(i,v,v.value)
							})
						}
					}
				} else {
					let msg= result.description?result.description:'获取数据失败，请重试！';
					utils.showMessage(msg);
				}
			});
		},
		//查询配件单位
		queryPartUnit(){
			win.utils.ajaxPost(win.utils.services.queryPartsUnit, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
			}, function(result) {
				if(result.result_code == 200) {
					order_list.setPartUnitFn(result.configPartsUnits);
				}
			})
		},
		//查询上门费用，紧急费用 ---创建维修方案，审核结算方案orderMan_order_getCostConfigures
		queryCostConfigures(conSta){
			let data = {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				tradingAreaId: tradingAreaId,
				orderId:selectId,
				// isExistParts:true    //  是否存在配件
			};
			if(conSta ==1){
				// partsCheckedCjSelf;//创建维修方案-配件-已选择配件---自己添加---修改后
				// partsCheckedCjOwnAf;//创建维修方案-配件-已选择配件---配件库---修改后
				let arr = [];
				arr = partsCheckedCjSelf.concat(partsCheckedCjOwnAf)
				if(arr.length>0){
					data.isExistParts = true
				}else{
					data.isExistParts = false
				}
			}
			let ajaxUrl = conSta==1?win.utils.services.orderMan_order_getCostConfigCj:win.utils.services.orderMan_order_getCostConfigures;
			if(conSta==1){
				win.utils.ajaxGet(ajaxUrl,data , function(result) {
					if(result.result_code == 200) {
						//上门费
						visitFeeArr=result.data.visit_cost?result.data.visit_cost:[];
						//加急费
						urgentFeeArr=result.data.urgent_cost?result.data.urgent_cost:[];
						//其他费
						otherFeeArr=result.data.other_cost?result.data.other_cost:[];
						order_list.setUrgentVisitFeeFn(conSta,type);
					}
				})
			}else{
				win.utils.ajaxPost(ajaxUrl,data , function(result) {
					if(result.result_code == 200) {
						//上门费
						visitFeeArr=[];
						//加急费
						urgentFeeArr=[];
						//其他费
						otherFeeArr=[];
						let visitArr = result.configCostConfigures?result.configCostConfigures:[];
						$.each(visitArr,function(index,value){
							if(value.costType == 1){
								visitFeeArr.push(value);
							}else if(value.costType == 2){
								urgentFeeArr.push(value);
							}else if(value.costType == 3){
								otherFeeArr.push(value);
							}
						});
						order_list.setUrgentVisitFeeFn(conSta);
					}
				})
			}
		},
		//请求不同状态下---数据(列表搜索下的tab切换)
		getStaNum(){
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
			};
			let idVal='';
			let leverVal='';
			if($("#city").val()==''){
				idVal='';
				leverVal=''
			}else{
				if($("#district").val()==''){
					idVal=$("#city").val();
					leverVal=1;
				}else{
					if($("#busDistrict").val()==''){
						idVal=$("#district").val();
						leverVal=2;
					}else{
						if($("#village").val()==''){
							idVal=$("#busDistrict").val();
							leverVal=3;
						}else{
							idVal=$("#village").val();
							leverVal=4;
						}
					}
				}
			}
			datas.endId=idVal;
			datas.levelFlag=leverVal;
			win.utils.ajaxGet(win.utils.services.orderMan_order_queryTabStatistics,datas, function(result) {
				let  setOvertimeNum=result.setOvertimeNum;
				for(let nn in setOvertimeNum){
					let readNumVal=setOvertimeNum[nn].readNum;
					if(nn==2){
						if(readNumVal!=0){
							$("#fenpeiNum").text(readNumVal);
							$("#daifenpeiCon").show();
						}else{
							$("#fenpeiNum").text(0);
							$("#daifenpeiCon").hide();
						}
					}
					let objConG=null;//container
					if(nn=='2'){
						objConG=$("#twoR");
					}else if(nn=='3'){
						objConG=$("#threeR");
					}else if(nn=='4'){
						objConG=$("#fourR");
					}else if(nn=='5'){
						objConG=$("#fiveR");
					}else if(nn=='9'){
						objConG=$("#nineR");
					}
					if(objConG){
						if(readNumVal!=0){
							objConG.text(readNumVal).show();
						}else{
							objConG.hide()
						}
					}
				}
			});
		},
		//计算配件费总价
		calculcateTotalFee(nameVal){
			let partFeeObj=0;//配件费用
			let partObj=null;//配件obj-own
			let partObjS=null;//配件obj-self
			if(nameVal=='shjsfa'){
				// 审核结算方案
				partObj=partsCheckedJsOwnAf;
				partObjS=partsCheckedJsSelf;
			}else if(nameVal=='cjwxfa'){
				// 创建维修方案
				partObj=partsCheckedCjOwnAf;
				partObjS=partsCheckedCjSelf
			}
			$.each(partObj,function(index,value){
				partFeeObj+=Number(value.partsNum)*Number(value.price);
			});
			$.each(partObjS,function(index,value){
				partFeeObj+=Number(value.partsNum)*Number(value.price);
			});
			partFeeObj=Number(partFeeObj+0.00).toFixed(2);
			partsTotals=partFeeObj;
			if(nameVal=='shjsfa'){
				// 审核结算方案
				$("#jsPartsPrice").html('配件费用总计：'+partFeeObj+'元');
				$("#partsTotalCostJs").val(partFeeObj);
				let a=Number($("#otherMoneyJs").val());//其他费用
				let b=Number($("#urgentMoneyJs").val());// 加急费用
				let c=Number($("#indoorJs").val());//上门费用
				$("#partsFeeJs").text(Number(a + b + c + partFeeObj * 1).toFixed(2));//费用
			}else if(nameVal=='cjwxfa'){
				// 创建维修方案
				$("#cjPartsPrice").html('配件费用总计：'+partFeeObj+'元');
			}
		},
		//创建报修单---紧急工单不需要预约日期和时间段
		toggleDate(){
			let idVal=$("#exigencyFlagRel").val();
			if(parseInt(idVal)==2){
				$("#fqbxdCon .hidess").show();
			}else{
				$("#fqbxdCon .hidess").hide();
			}
		},
		//获取租务工单---
		queryOrderTema() {
			$("#rentralistCon").empty();
			let user_idVal=win.utils.getCookie('user_id');
			let sessionVal=win.utils.getCookie('session');
			let str='?user_id='+user_idVal+'&session='+sessionVal+'&outOrderNo='+Number(dankeOrderId);
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryOrderTema+str, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				outOrderNo:dankeOrderId,
			}, function(result) {
				if(result.result_code == 200) {
					let contentStr = "";
					if (!result.rentOrderDetails || result.rentOrderDetails.length <= 0) {
						contentStr=`
						    <ul style="padding: 20px;text-align: center;background: white"><li>暂无</li></ul>
						`;
					} else {
						let orderInfo = result.rentOrderDetails;
						$.each(orderInfo, function (index, value) {
							let tuicheStr = ``,
								reason = '',//退单原因
								tcnote = '';//退单备注
							if (value.refund_reason) {
								reason = value.refund_reason;//退单原因
								tcnote = value.refund_note;//退单备注
								tuicheStr = `
										<div class="form-group">
											<label class="control-label">退单原因:</label>
											<div class="form-control selfBor" id="">` + reason + `</div>
										</div>
										<div class="form-group">
											<label class="control-label">退单备注:</label>
											<div class="form-control selfBor" id="">` + tcnote + `</div>
										</div>`;
							}
							if (value.canceled_reason) {
								reason = value.canceled_reason;//撤销原因
								tcnote = value.canceled_note;//撤销备注
								tuicheStr += `
										<div class="form-group">
											<label class="control-label">撤销原因:</label>
											<div class="form-control selfBor" id="">` + reason + `</div>
										</div>
										<div class="form-group">
											<label class="control-label">撤销备注:</label>
											<div class="form-control selfBor" id="">` + tcnote + `</div>
										</div>`;
							}
							let task_id = value.task_id, //租务工单id
								executor_name=value.executor_name ? value.executor_name : '--',//执行人姓名
								assigned_at=value.assigned_at ? value.assigned_at : '--',//分派时间
								statusVal = value.status ? value.status : '--',//工单状态
								accepter = value.accepter ? value.accepter : '--',//认领人
								accepted_at = value.accepted_at ? value.accepted_at : '--',//认领时间
								finished_at = value.finished_at ? value.finished_at : '--',//完成时间
								finished_note = value.finished_note ? value.finished_note : '--',//完成备注
								createTime = value.create_time ? value.create_time : '--',//创建时间
								is_repeated = value.is_repeated ? value.is_repeated : '--',//是否重单：是或否
								repeated_content = value.repeated_content ? value.repeated_content : '--',//重单内容
								note = value.note ? value.note : '--',//工单内容
								finished_img='',//完成图片
								delay_day = value.delay_day ? value.delay_day : '--',//延期天数
								delay_note = value.delay_note ? value.delay_note : '--',//延期原因
								create_time = value.create_time ? value.create_time : '--',//创建时间
								modify_time = value.modify_time ? value.modify_time : '--';//更新时间
							// resettlement_tasks.id	int	安置工单id
							// resettlement_tasks.status	int	安置工单状态
							// resettlement_tasks.reason	int	安置原因
							// resettlement_tasks.created_at	int	安置工单创建时间
							if(value.finished_images&&value.finished_images.length>0){
								$.each(value.finished_images,function(index,value){
									finished_img += '<img class="mySelfImg" alt="" name="' + value + '" src="' + value + '">';
								})
							};
							let strTaskVal = '';
							let strTaskValHead = `<li><span>id</span><span>状态</span><span>原因</span><span>创建时间</span></li>`;
							if ($.parseJSON(value.resettlement_tasks).length > 0) {
								$.each($.parseJSON(value.resettlement_tasks), function (index, value) {
									strTaskVal += `<li><span>` + value.id + `</span><span>` + value.status + `</span><span>
										` + value.reason + `</span><span>` + value.created_at + `</span></li>`
								})
							} else {
								strTaskValHead = '--';
							}
							;
							contentStr += `
								<div class="detailCssa">
									<div class="form-group title">
									   <label class="control-label active info l" name="` + index + `"><span class="l">工单信息</span></label>
									   <label class="control-label info r" name="` + index + `"><span class="right">基本信息</span></label>
								   </div>	
									<div class="left">
										<div class="bContainer">
											<div class="form-group">
												<label class="control-label">租务工单ID:</label>
												<div class="form-control selfBor" id="">` + task_id + `【` + statusVal + `】</div>
											</div>									  	
											<div class="form-group">
												<label class="control-label">执行人:</label>
												<div class="form-control selfBor" id="">` + executor_name+ `</div>
										   </div>	
										   <div class="form-group">
												<label class="control-label">分派时间:</label>
												<div class="form-control selfBor pre" id="">`+ assigned_at + `</div>
										   </div>
										   <div class="form-group">
												<label class="control-label">完成时间:</label>
												<div class="form-control selfBor" id="">` + finished_at + `</div>
										   </div>
										   <div class="form-group">
												<label class="control-label">完成备注:</label>
												<div class="form-control selfBor" id="noteZw">` + finished_note + `</div>
											</div>
											<div class="form-group">
											    <label class="control-label">完成图片:</label>
											    <div class="form-control selfBor" id="finishImgZw">` + finished_img + `</div>
											</div>
											` + tuicheStr + `
										   <div class="form-group">
												<label class="control-label">创建时间:</label>
												<div class="form-control selfBor" id="">` + create_time + `</div>
										   </div>
											<div class="form-group">
												<label class="control-label">安置工单:</label>
												<div class="form-control selfBor" id=""><ul id="anzhi">` + strTaskValHead + strTaskVal + `</ul></div>
											</div>
										</div>
									</div>
									<div class="right" style="display:none"> 
										<div class="bContainer">
											<div class="form-group">
												<label class="control-label">子工单:</label>
												<div class="form-control selfBor pre" id="">租务工单</div>
											</div>	
											<div class="form-group">
												<label class="control-label">工单内容:</label>
												<div class="form-control selfBor pre" id="">` + note + `</div>
											</div>
										</div>	
									</div>   
								</div>`;
						});
						let sdtre = `<div class="form-group">
												<label class="control-label">工单一级分类:</label>
												<div class="form-control selfBor" id="">--</div>
											</div>
											<div class="form-group">
												<label class="control-label">工单二级分类:</label>
												<div class="form-control selfBor pre" id="">--</div>
											</div>`;
					}
					$("#rentralistCon").append(contentStr);
					order_list.showImg('zwd');
					$("#rentralistCon").show();//租务工单
				} else {
					utils.showLayerMessage('获取失败，请稍后重试！');
				}
			});
		},
		//图片show---#ifn
		showImg(name){
			// name-
			let obj=null;
			if(name=='zwd'){//zwd:租务详情展示
				obj=$("#noteImgZw img");
			}else if(name=='jsdsh'){//结算待审核
				obj=$("#shjsfaCon img");
			}else if(name=='jsshtg'){//结算审核通过
				obj=$("#MaintettlemeSntCon img");
			}else if(name=='cjwxfa'){//创建维修方案
				obj=$("#cjwxfaCon img");
			}else if(name=='wxfaList'){//维修方案list
				obj=$("#maintPlanCon img");
			}else if(name=='wxfaCList'){//维修方案-协调记录list
				obj=$("#MaintCoordRecordCon img");
			}else if(name=='order'){//工单详情
				obj=$("#workOrderDetailCon img");
			}else if(name=='glgd'){//关联工单
				obj=$("#repairlistCon img");
			}else if(name=='scale'){
				return;
			};
			setTimeout(function(){
				let srcVal='';
				$.each(obj, function (index) {
					srcVal=obj.eq(index).attr('name');
					if(name=='scale'){
						srcVal=obj.eq(index).attr('alt');
					};
					obj.eq(index).attr('src',srcVal);
				});//图片
			},10);
		},
		// 获取退撤单原因  configMan_maintenOrderReason_list
		queryOrderReason(msgSta){
			if(msgSta==13){
				behaviorClassify=1
			}else{
				behaviorClassify=2;
			};
			win.utils.ajaxPost(win.utils.services.configMan_maintenOrderReason_list, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				status :1,
				behaviorClassify:behaviorClassify//13:退单 14:撤单
			}, function(result) {
				if(result.result_code == 200) {
					let data=result.serviceReturnReasons;
					let str="<option value=''>请选择原因</option>";
					$.each(data,function(index,value){
						str+="<option value="+value.thirdId+">"+value.reason+"</option>";
					});
					if(msgSta==13){
						$("#tdReason").empty().append(str);
					}else{
						$("#cdReason").empty().append(str);
					};
				} else {
					utils.showLayerMessage('获取失败，请稍后重试！');
				}
			});
		},
		//获取退单，撤单详情
		getTuiCheDetail(msgSta){
			win.utils.ajaxGet(win.utils.services.orderMan_order_queryOrderById, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId:selectId,
				orderStatus:msgSta//13:退单 14:撤单
			}, function(result) {
				if(result.result_code == 200) {
					let dt="";
					if(msgSta==13){
						dt='退单详情';
					}else{
						dt='撤单详情';
					};
					$("#quitBackTil").text(dt);//展示详情标题
					let objT=result.order_audit;
					let SqTime=$("#tdSqTime");
					let SqReason=$("#tdSqReason");
					let SqNote=$("#tdSqNote");
					let SpPeople=$("#tdSpPeople");
					let SpSta=$("#tdSpSta");
					let SpNote=$("#tdSpNote");
					let SpTime=$("#tdSpTime");
					let timeObj=new Date(objT.createTime);//申请时间/创建时间
					let stime='--';
					if(timeObj){
						soy=timeObj.getFullYear();
						som=timeObj.getMonth()+1;
						som=som<10?'0'+som:som;
						sod=timeObj.getDate();
						sod=sod<10?'0'+sod:sod;
						soh=timeObj.getHours();
						soh=soh<10?'0'+soh:soh;
						sof=timeObj.getMinutes();
						sof=sof<10?'0'+sof:sof;
						sos=timeObj.getSeconds();
						sos=sos<10?'0'+sos:sos;
						stime=soy+"-"+som+"-"+sod+"  "+soh+":"+sof+":"+sos;
					};
					SqTime.html(stime);//创建时间
					let causeDesc=objT.causeDesc?objT.causeDesc:'--';
					SqReason.html(objT.causeDesc);//原因描述
					let explainDesc=objT.explainDesc?objT.explainDesc:'--';
					SqNote.html(objT.explainDesc);//备注
					let auditUser=objT.auditUser?objT.auditUser:'--';
					SpPeople.html(auditUser);//审批人
					let strNum='';
					if(objT.auditStatus==1){
						strNum='审核通过';
					}else if(objT.auditStatus==2){
						strNum='审核驳回';
					}else if(objT.auditStatus==3){
						strNum='待审核';
					}else{
						strNum='待审核';
					};
					SpSta.html(strNum);//审批状态
					let auditDesc=objT.auditDesc?objT.auditDesc:'--';
					SpNote.html(auditDesc);//审批备注
					let stimes='--';
					if(objT.auditTime&&objT.auditTime!=''){
						oy=new Date(objT.auditTime).getFullYear();
						om=new Date(objT.auditTime).getMonth()+1;
						om=om<10?'0'+om:om;
						od=new Date(objT.auditTime).getDate();
						od=od<10?'0'+od:od;
						oh=new Date(objT.auditTime).getHours();
						oh=oh<10?'0'+oh:oh;
						of=new Date(objT.auditTime).getMinutes();
						of=of<10?'0'+of:of;
						os=new Date(objT.auditTime).getSeconds();
						os=os<10?'0'+os:os;
						stimes=oy+"-"+om+"-"+od+"  "+oh+":"+of+":"+os;
					};
					SpTime.html(stimes);//审批时间
					$("#quitDetailCon").show();//退单/撤单详情
				} else {
					utils.showLayerMessage('获取失败，请稍后重试！');
				}
			});
		},
		// 获取授权列表
		getAuthHis(msgSta){
			$("#tableSqRecord").empty()
			win.utils.ajaxGet(win.utils.services.orderMan_order_queryAuthHis, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId:selectId,
			}, function(result) {
				if(result.result_code == 200) {
					let gridData = result.auth_his;
					if(gridData.length<=0){
						$("#tableSqRecord").append('<li class="cssn"><span>暂无</span></li>');
						return;
					}
					let str=` <li>
						       <span class="">ID</span> 
						       <span class="">申请人</span> 
						       <span class="">授权状态</span> 
						       <span class="">预约上门时间</span> 
						       <span class="">创建时间</span> 
						   </li>`;
					$.each(gridData,function(index,value){
						let bookingTime=value.bookingTime?value.bookingTime:'--';//预约上门时间
						let createTime=value.createTime?value.createTime:'--';//创建时间
						let examine_status='--';//授权状态
						if(value.examineStatus==0){
							examine_status='待审核';
						}else if(value.examineStatus==1){
							examine_status='审核通过'
						}else if(value.examineStatus==2){
							examine_status='审核驳回'
						}
						let proposerId=value.id?value.id:'--';//申请人id
						let proposerName=value.proposerName?value.proposerName:'--';///申请人姓名
						//授权状态 ---sourceFlag: null
						str+=`<li class="css">
						       <span class="">`+proposerId+`</span> 
						       <span class="">`+proposerName+`</span> 
						       <span class="">`+examine_status+`</span> 
						       <span class="">`+bookingTime+`</span> 
						       <span class="">`+createTime+`</span> 
						   </li>`;
					});
					$("#tableSqRecord").append(str);
					$("#authorizationListCon").show();//授权列表
				} else {
					utils.showLayerMessage('获取失败，请稍后重试！');
				}
			});
		},
		//获取改期记录
		getGaiqiRecord(msgSta){
			win.utils.ajaxGet(win.utils.services.orderMan_order_queryOrderChangeHis, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId:selectId,
			}, function(result) {
				if(result.result_code == 200) {
					let gridData = result.order_change;
					if(gridData.length<=0){
						$("#tableGqRecord").append('<li class="cssn"><span>暂无</span></li>');
						return;
					}
					let str=` <li>
						       <span class="name">创建人</span> 
						       <span class="ct">创建时间</span> 
						       <span class="ct">预约时间</span> 
						       <span class="cd">联系结果</span> 
						       <span class="cd">改期日期</span> 
						       <span class="cd">改期时间段</span> 
						       <span class="cd">改期天数</span> 
						       <span class="note">原因</span> 
						       <span class="note">备注</span> 
						   </li>`;
					$.each(gridData,function(index,value){
						let createUser=value.createUser?value.createUser:"";//创建人
						let changeReason=value.changeReason?value.changeReason:""; //原因
						let changeRemark=value.changeRemark?value.changeRemark:""; //备注
						let changeNumber=value.changeNumber?value.changeNumber+"天":'0天'; //改期天数
						let changeTimes=value.changeTimes?value.changeTimes:'';//改期时间段
						let changeDay=value.changeDay?value.changeDay:''; //改期日期
						let cy=new Date(changeDay).getFullYear();
						let cm=new Date(changeDay).getMonth()+1;
						cm=cm<10?'0'+cm:cm;
						let cd=new Date(changeDay).getDate();
						cd=cd<10?'0'+cd:cd;
						let createTime=value.createTime?value.createTime:'';//创建时间
						let isChangeDate='';//联系结果  	// 改期-1，正常-2
						if(value.isChangeDate==1){
							isChangeDate='<em class="red">改期上门</em>';
						}else if(value.isChangeDate==2){
							isChangeDate='<em class="green">正常上门</em>';
						}else{
							isChangeDate='';
						};
						let ocy='';
						let ocm="";
						let ocd="";
						let appointmentDate=value.appointmentDate?value.appointmentDate:'';  //预约时间
						let appointmentTimes=value.appointmentTimes?value.appointmentTimes:'';
						if(appointmentDate&&appointmentDate!=''){
							ocy=new Date(appointmentDate).getFullYear();
							ocm=new Date(appointmentDate).getMonth()+1;
							ocm=ocm<10?'0'+ocm:ocm;
							ocd=new Date(appointmentDate).getDate();
							ocd=ocd<10?'0'+ocd:ocd;
						};
						let oy='';
						let om="";
						let od="";
						let oh='';
						let of="";
						let os="";
						if(createTime&&createTime!=''){
							oy=new Date(createTime).getFullYear();
							om=new Date(createTime).getMonth()+1;
							om=om<10?'0'+om:om;
							od=new Date(createTime).getDate();
							od=od<10?'0'+od:od;
							oh=new Date(createTime).getHours();
							oh=oh<10?'0'+oh:oh;
							of=new Date(createTime).getMinutes();
							of=of<10?'0'+of:of;
							os=new Date(createTime).getSeconds();
							os=os<10?'0'+os:os;
						};
						str+=`<li class="css">
						       <span class="name">`+createUser+`</span> 
						       <span class="ct">`+oy+"-"+om+"-"+od+" "+oh+":"+of+":"+os+`</span> 
						       <span class="ct">`+ocy+"-"+ocm+"-"+ocd+`  `+appointmentTimes+`</span> 
						       <span class="cd">`+isChangeDate+`</span> 
						       <span class="cd">`+cy+"-"+cm+"-"+cd+`</span> 
						       <span class="cd">`+changeTimes+`</span> 
						       <span class="cd">`+changeNumber+`</span> 
						       <span class="note">`+changeReason+`</span> 
						       <span class="note">`+changeRemark+`</span> 
						   </li>`;
					});
					$("#tableGqRecord").append(str);
				} else {
					utils.showLayerMessage('获取失败，请稍后重试！');
				}
			});
		},
		//查询配置清单
		queryConfigList(){
			order_clear.clearQueryConfigList();
			$("#configInfoCon").hide();//装修配置信息
			win.utils.ajaxPost(win.utils.services.orderMan_order_getConfigList, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId:selectId,
			}, function(result) {
				if(result.result_code == 200) {
					$("#configInfoCon").show();//装修配置信息
					let preInfo=result.data.prepare_config;
					let areaList=result.data.area_list;
					let suite_address=preInfo.suite_address?preInfo.suite_address:'--';
					let sign_type=preInfo.sign_type?preInfo.sign_type:'--';
					let decorationTeam=preInfo.decorationTeam?preInfo.decorationTeam:'--';
					let engineering_contractor=preInfo.engineering_contractor?preInfo.engineering_contractor:'--';
					let origin_electric=preInfo.origin_electric?preInfo.origin_electric:'--';
					let created_at=preInfo.created_at?preInfo.created_at:'--';
					let other_mark=preInfo.other_mark?preInfo.other_mark:'--';

					$("#suite_addressConfig").html(suite_address);//公寓
					$("#sign_typeConfig").html(sign_type);//合同类型
					$("#decorationTeamConfig").html(decorationTeam);//装修队
					$("#engineering_contractorConfig").html(engineering_contractor);//工程商
					$("#origin_electricConfig").html(origin_electric);//原有家电
					$("#other_markConfig").html(other_mark);//其他备注
					$("#created_atConfig").html(created_at);//创建时间
					let nameVal='';
					let detailVal='';
					$.each(areaList,function(index,value){
						nameVal+="<span>"+value.area+"</span>";
						let singDetail=`<ul><li><div>家具/设备</div><div>数量</div></li>`;
						$.each(value.list,function(index,value){
							let nameVal='';
							let numVal='';
							if(value.good_name){
								nameVal=value.good_name;
							};
							if(value.num_supplier){
								numVal=value.num_supplier;
							};
							singDetail+=`<li><div>`+nameVal+`</div><div>`+numVal+`</div></li>`;
						});
						detailVal+=singDetail+"</ul>";
					});
					$("#roomName").append(nameVal);
					$("#roomDetail").append(detailVal);
					$("#configInfoCon .detailCss").eq(0).show();
					$("#configInfoCon .detailCss").eq(1).hide();
					$("#roomName span").eq(0).addClass('check');
					$("#roomDetail ul").hide().eq(0).show();
				} else {
					$("#configInfoCon .detailCss").eq(1).show();
					$("#configInfoCon .detailCss").eq(0).hide();
					utils.showLayerMessage(result.data);
				}
			});
		},
		//获取跟进记录
		getGjRecord(){
			$("#followUpRecordCon").hide();//跟进记录
			$("#gjNote").val('');
			$("#txtNewDateGj").val("");
			$("#gjNote").parent('div').find('em').eq(0).text("0");
			$("#gjTable").empty()
			win.utils.ajaxGet(win.utils.services.orderMan_order_queryOrderFollowHis, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId:selectId,
			}, function(result) {
				if(result.result_code == 200) {
					let btnData = result.data;
					let str="";
					$.each(btnData,function(index,value){
						str+='<li><span class="c">'+value.createTime+'</span><span class="a">'+value.operationDesc+'</span></li>';
					});
					$("#gjTable").empty().append(str);
					$("#followUpRecordCon").show();//跟进记录
				} else {
					utils.showLayerMessage('获取跟进记录失败，请稍后重试！');
				}
			});
		},
		//获取通话记录
		getCallRecord(){
			win.utils.ajaxGet(win.utils.services.orderMan_order_queryCallHistory, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId:selectId,
			}, function(result) {
				$("#gqCallRecordCon").empty();
				if(result.result_code == 200) {
					let gridData=result.call_his;
					let callHis=result.clickHistory;
					if(callHis){
						let clickHistory=$.parseJSON(callHis);
						let nameVal=clickHistory[0].workerName?clickHistory[0].workerName:'--';
						if(clickHistory[0].callStart){
							transTime(clickHistory[0].callStart);
						}else{
							transDate='--';
						}
						$("#firstCallTime").html(transDate);
						$("#firstCallName").html(nameVal);
						$("#firstCall").show();
					}else{
						$("#firstCallTime").html('');
						$("#firstCallName").html('');
						$("#firstCall").hide();
					}
					if(gridData==''){
						let nowData=` <div><ul id="gqCallRecord"><li class="cssn"><span>暂无</span></li></ul></div>`;
						$("#gqCallRecordCon").empty().append(nowData);
						return;
					}
					if(gridData.length>0){
						let str=` <li>
							   <span class="">通话人</span> 
							   <span class="">通话时间</span> 
							   <span class="">通话时长</span> 
						   </li>`;
						let strs=` <li>
							   <span class="">通话人</span> 
							   <span class="">通话时间</span> 
							   <span class="">通话时长</span> 
						   </li>`;
						$.each($.parseJSON(gridData),function(index,value){
							let nameCall=value.workerName?value.workerName:'--';
							let callStart=value.callStart?value.callStart:'--';
							let callTimes=value.callTimes?value.callTimes+'S':'--';
							if(value.callStart){
								bpDateVal=new Date(value.callStart);
								let bpYear=bpDateVal.getFullYear();
								let bpMonth=bpDateVal.getMonth()+1;
								bpMonth=bpMonth<=9?'0'+bpMonth:bpMonth;
								let bpDay=bpDateVal.getDate()<=9?'0'+bpDateVal.getDate():bpDateVal.getDate();
								let bpHour=bpDateVal.getHours()<=9?'0'+bpDateVal.getHours():bpDateVal.getHours();
								let bpMin=bpDateVal.getMinutes()<=9?'0'+bpDateVal.getMinutes():bpDateVal.getMinutes();
								let bpSec=bpDateVal.getSeconds()<=9?'0'+bpDateVal.getSeconds():bpDateVal.getSeconds();
								callStart=bpYear+"-"+bpMonth+"-"+bpDay+'  '+bpHour+":"+bpMin+":"+bpSec;
							};
							if($.parseJSON(gridData).length<=2){
								str+=`<li class="css">
							       <span class="">`+nameCall+`</span> 
							       <span class="">`+callStart+`</span> 
							       <span class="">`+callTimes+`</span> 
							   </li>`;
							}else{
								if($.parseJSON(gridData).length%2==1){
									if(index<=($.parseJSON(gridData).length-1)/2){
										str+=`<li class="css">
                                           <span class="">`+nameCall+`</span> 
                                           <span class="">`+callStart+`</span> 
                                           <span class="">`+callTimes+`</span> 
                                       </li>`;
									}else{
										strs+=`<li class="css">
                                           <span class="">`+nameCall+`</span> 
                                           <span class="">`+callStart+`</span> 
                                           <span class="">`+callTimes+`</span> 
                                       </li>`;
									};
								}else{
									if(index<$.parseJSON(gridData).length/2){
										str+=`<li class="css">
                                           <span class="">`+nameCall+`</span> 
                                           <span class="">`+callStart+`</span> 
                                           <span class="">`+callTimes+`</span> 
                                       </li>`;
									}else{
										strs+=`<li class="css">
                                           <span class="">`+nameCall+`</span> 
                                           <span class="">`+callStart+`</span> 
                                           <span class="">`+callTimes+`</span> 
                                       </li>`;
									};
								};
							};
						});
						if($.parseJSON(gridData).length<=2){
							let nowData=`<div><ul id="gqCallRecord">`+str+`</ul></div>`;
							$("#gqCallRecordCon").empty().append(nowData);
						}else{
							let nowData=`<div><ul id="gqCallRecord">`+str+`</ul></div><div><ul id="gqCallRecords">`+strs+`</ul></div>`;
							$("#gqCallRecordCon").empty().append(nowData);
						};
					}else{
						let nowData=` <div><ul id="gqCallRecord"><li class="cssn"><span>暂无</span></li></ul></div>`;
						$("#gqCallRecordCon").empty().append(nowData);
					};
				} else {
					utils.showLayerMessage('获取通话记录失败，请稍后重试！');
				}
			});
		},
		//创建维修方案时，当状态为协调时，展示协调方案分类，其余隐藏
		controlPlan(){
			let staVal=$("#orderStatus option:selected").val();
			if(parseInt(staVal)==6){
				$("#controlPlanCon").show();
			}else{
				$("#controlPlanCon").hide();
			};
		},
		//查询配件库-类型
		queryOrderPartsType(num,typeVal){
			num=num==undefined?1:num;
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				status:1,
				rank:num
			};
			if(!isNaN(typeVal)){
				datas.firstTypeId=typeVal;
			};
			win.utils.ajaxPost(win.utils.services.region_queryPartsType, datas,function(result){
				let obj=result.configPartsTypes;
				let str="";
				if(isNaN(typeVal)){
					$.each(obj,function(index,value){
						str+='<button type="button" class="btn" name="'+value.firstTypeId+'">'+value.firstTypeName+'</button>';
					});
				}else{
					$.each(obj,function(index,value){
						str+='<button type="button" class="btn" name="'+value.secondTypeId+'">'+value.secondTypeName+'</button>';
					});
				};
				if(isNaN(typeVal)){
					$("#firstPartsType").empty().append(str);
					$("#firstPartsType button").eq(0).addClass('btn-info');
					let numVal=obj[0].firstTypeId;
					order_list.queryOrderPartsType(2,numVal);
				}else{
					$("#secondPartsType").empty().append(str);
					$("#secondPartsType button").eq(0).addClass('btn-info');
					let partsSecondIdVal='';
					if(obj.length>0){
						partsSecondIdVal=obj[0].secondTypeId;
					};
					order_list.queryOrderParts(partsFirstIdVal,partsSecondIdVal,1);
				};
			});
		},
		//查询配件库-list
		queryOrderParts(a,b,num){
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				status:1
			};
			if(a!=''){
				datas.firstTypeId =a;
				datas.rank=1;
			};
			if(b!=''){
				datas.secondTypeId =b;
				datas.rank=2;
			}
			win.utils.ajaxPost(win.utils.services.region_queryParts, datas,function(result){
				let obj=result.parts;
				if(a==''&&b==''){
					partsData=result.parts;
					let str="<option value=''>请选择</option>";
					let strs="<option value=''>请选择</option>";
					$.each(obj,function(index,value){
						let strVal=JSON.stringify(value);
						str+='<option value='+value.partsCode+'>  '+value.partsName+'</option>';
						strs+='<option name="'+strVal.replace(/"/g,"@#")+'" value='+value.partsCode+'>  '+value.partsName+'</option>';
					});
					$("#partsName").empty().append(str);
					$("#partsNameSS").empty().append(strs);
					order_list.refreshRepairman(3,num);
				}else{
					let strVals='';
					partsArrData=[];
					let arrs=[];
					let partsCheckedOwnBf=[];
					if(selectIdSta==9){
						partsCheckedOwnBf=[];
						$.each(partsCheckedJsOwnBf,function(index,value){
							partsCheckedOwnBf.push(value);
						});
					}else{
						partsCheckedOwnBf=[];
						$.each(partsCheckedCjOwnBf,function(index,value){
							partsCheckedOwnBf.push(value);
						});
					};
					$.each(partsCheckedOwnBf,function(index,value){
						arrs.push(value.partsCode);
					});
					$.each(obj,function(index,value){
						partsArrData.push(value.partsCode);
						let strVal=JSON.stringify(value);
						let strNameVal=arrs.indexOf(value.partsCode)<0?'uncheck':'partsTypeCheck';
						strVals+=`<li name="`+strVal.replace(/"/g,"@#")+`" value="`+value.partsCode+`" class="`+strNameVal+`">`
							+`<span class="uncheck">√</span><span class="name">`+value.partsName+`</span><span class="brand">品牌:`+value.brandName+`</span><span class="type">型号:`+value.partsType+`</span><span class="fee">配件费:`+value.price+`元/`+value.unitName+`</span></li><br>`;
					});
					$("#partTypeCon>ul").empty().append(strVals);
				};
			});
		},
		//查询结算方案---工单结算标签
		queryOrderCloseLabel(){
			$("#isReWorkRecordJsCon").hide();
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryOrderCloseLabel, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId :selectId
			},function(result){
				let data=result.data;
				let str='';
				isShowNintyFlag=false;
				$.each(data,function(index,value){
					if(value.labelName.indexOf('90天返修')>-1){
						isShowNintyFlag=true;
					};
					if(value.labelStatus==2){
						str+="<span class='bgr'>"+value.labelName+"</span>";
					}else{
						str+="<span class='bgg'>"+value.labelName+"</span>";
					};
				});
				if(selectIdSta==9){
					$("#gdjsbqJs").empty().html(str);
					if(isShowNintyFlag==true){
						$("#isReWorkRecordJsCon").show();
					}else{
						$("#isReWorkRecordJsCon").hide();
					}
				}else{
					$("#gdjsbqJsOk").empty().html(str);
				};
			});
		},
		//设定结算方案---(审核结算与结算方案为一个页面)
		setOrderAccount: function (obj, str) {
			visitFeeSelected = '';
			urgentFeeSelected = '';
			otherFeeSelected = '';
			if (selectIdSta == 9) {//结算待审核
				$("#shjsfaCon .titleT").eq(0).show();
				initiateRepairAreaVal = obj.maintainArea?obj.maintainArea: initiateRepairAreaVal;
				order_list.querySuiteArea(2,initiateRepairAreaVal)  //查询维修区域
				if(obj.secondClassifyId){
					judgeIsHasSecondIdFlag = true
					secondGradecreate = obj.secondClassifyId
					order_list.queryMatinClassify(2,2,2) //根据二级id查询二级的父级一级id
					// console.log(obj.reasonPlanVOS,'9999')
					let arr = obj.reasonPlanVOS?obj.reasonPlanVOS:[]
					order_list.getReasonMatinplanList(2,secondGradecreate,1,arr)
				}else{
					judgeIsHasSecondIdFlag = false
					$("#reasonConsJs").hide()
					$("#planConsJs").hide()
					$("#addReasonPlanBtnJSCon").hide()
					order_list.queryMatinClassify(2,2,2) //根据二级id查询二级的父级一级id
					order_list.queryMatinClassify(2,1); // 查询一级维修分类
				}
				partsCheckedJsSelf = [];//审核结算方案-配件-已选择配件---自己添加---
				partsCheckedJsOwnAf = [];//审核结算方案-配件-已选择配件---配件库---修改后
				partsCheckedJsOwnBf = [];//审核结算方案-配件-已选择配件---配件库---修改前
				let strSelf = "";//自定义
				let strOwn = '';//配件库拥有
				let checkRemark = obj.checkRemark ? obj.checkRemark : "";
				$("#checkRemarkJs").val(checkRemark);
				$("#checkRemarkJs").parent('div').find('.inputNums em').eq(0).text(checkRemark.length);
				if (obj.accountParts && obj.accountParts.length > 0) {
					$.each(obj.accountParts, function (index, value) {
						if (value.isCustomParts == 2) {//不是
							partsCheckedJsOwnAf.push(value);//审核结算方案-配件-已选择配件---配件库---修改后
							partsCheckedJsOwnBf.push(value);//审核结算方案-配件-已选择配件---配件库---修改前
							strOwn += `<li>
								<div class="b">` + value.partsName + `</div> 
								<div class="a">` + value.brandName + `</div>
								<div class="a">` + value.partsType + `</div>
								<div class="a">` + Number(value.price).toFixed(2) + `元/` + value.unitName + `</div>
								<div class="a"><input class="num" type="number" min=0 title="oneNumber" id="` + value.id + `" value="` + value.partsNum + `">` + value.unitName + `</div>
								<div class="deletePartOwn" name="del">✖</div>
							</li>`;
						} else {//是
							partsCheckedJsSelf.push(value);//审核结算方案-配件-已选择配件---自己添加---
							let picUrlValPart = '';
							if (value.picUrl && value.picUrl.length > 0) {
								$.each(value.picUrl.split(','), function (index, value) {
									if (value != "" && value != ',') {
										if (value.indexOf('http://images.baijiaxiu.com/') < 0) {
											value = 'http://images.baijiaxiu.com/' + value;
										}
										;
										picUrlValPart += `<img  class="mySelfImg" src="../../images/imgLoad.png"  name="` + value + `">`;
									}
								});//配件图片
							}
							;
							let remark = value.remark ? value.remark : '--';
							let unitName = value.unitName ? value.unitName : '';
							strSelf += `<li><div>
									<div>配件名称:` + value.partsName + `</div>
									<div>配件品牌:` + value.brandName + `</div>
									<div>配件型号:` + value.partsType + `</div>
									<div>采购价:` + Number(value.purchasePrice).toFixed(2) + `元</div>
									<div>配件费:` + Number(value.price).toFixed(2)+`元</div>
									<div>配件数量:`+value.partsNum+unitName+`</div>
								</div>								
								<div>
									<div>配件图片:<br>` + picUrlValPart + `</div>
								</div>
								<div>
									<div>备注说明:` + remark + `</div>
								</div>	
								<div class="deletePartOwn" name="del">✖</div>										
								<div class="addSelfPart" name="shjsfa">修改</div></li>`;
						}
						;
					});//配件信息列表
				}
				;
				if (partsCheckedJsOwnAf.length > 0) {
					$("#checkedPartsOwnShowJss").show()
				} else {
					$("#checkedPartsOwnShowJss").hide()
				}
				;
				$("#checkedPartsOwnShowJs").empty().append(strOwn); //配件库选择
				let classNames = "noPadding";
				if (strSelf.length > 0) {
					classNames = "yesPadding";
				}
				;
				$("#checkedPartsSelfJs").empty().append(strSelf).addClass(classNames);  //自定义添加
				$("#servicePlanJs").html(obj.maintainRemark);//维修方案
				let picUrlVal = '';
				if (obj.maintainUrl && obj.maintainUrl.length > 0) {
					$.each(obj.maintainUrl.split(','), function (index, value) {
						if (value != "" && value != ',') {
							picUrlVal += `<img class="mySelfImg" src="../../images/imgLoad.png" name="` + value + `">`;
						}
					});//维修图片
				}
				;
				$("#remarkUrlJs").empty().append(picUrlVal);//维修图片
				order_list.showImg('jsdsh');
				let otherMoney = obj.otherMoney ? Number(obj.otherMoney).toFixed(2) : 0.00;//其他费用
				let urgentMoney = obj.urgentMoney ? Number(obj.urgentMoney).toFixed(2) : 0.00;// 加急费用
				let partsTotalCost = obj.partsTotalCost ? Number(obj.partsTotalCost).toFixed(2) : 0.00;//配件费用总计
				let callMoney = obj.callMoney ? Number(obj.callMoney).toFixed(2) : 0.00;//上门费用
				$("#otherMoneyJs").val(otherMoney);//其他费用
				$("#urgentMoneyJs").val(urgentMoney);//加急费用
				$("#indoorJs").val(callMoney);//上门费用
				$("#partsTotalCostJs").val(Number(partsTotalCost).toFixed(2));//配件费用
				$("#jsPartsPrice").html('配件费用总计:' + Number(partsTotalCost).toFixed(2) + '元');//配件费用总计
				visitFeeSelected = callMoney;
				urgentFeeSelected = urgentMoney;
				otherFeeSelected = otherMoney;
				partsTotals = partsTotalCost;
				order_list.queryCostConfigures( 2);
				let contactOvertime = obj.contactOvertime ? Number(obj.contactOvertime).toFixed(2) : 0.00;//联系超时
				let visitOvertime = obj.visitOvertime ? Number(obj.visitOvertime).toFixed(2) : 0.00;//上门超时
				let feedbackOvertime = obj.feedbackOvertime ? Number(obj.feedbackOvertime).toFixed(2) : 0.00;//反馈超时
				let partsOvertime = obj.partsOvertime ? Number(obj.partsOvertime).toFixed(2) : 0.00;//配件超时
				$("#linkOverJs").val(Number(contactOvertime).toFixed(2));//联系超时
				$("#doorOverJs").val(Number(visitOvertime).toFixed(2));//上门超时
				$("#backOverJs").val(Number(feedbackOvertime).toFixed(2));//反馈超时
				$("#partOverJs").val(Number(partsOvertime).toFixed(2));//配件超时
				let deleteFeeVal = Number(Number(contactOvertime) + Number(visitOvertime) + Number(feedbackOvertime) + Number(partsOvertime)).toFixed(2);
				if (deleteFeeVal > 0) {
					deleteFeeVal = '-' + deleteFeeVal;
				}
				$("#deleteFee").text(deleteFeeVal);//费用扣除总计
				let partsFeeJs = Number(otherMoney * 1 + urgentMoney * 1 + callMoney * 1 + partsTotalCost * 1).toFixed(2);
				$("#partsFeeJs").text(partsFeeJs);//工单费用总计
				if (obj.createTime) {
					let timeObj = new Date(obj.createTime),
						oys = timeObj.getFullYear(),
						oms = timeObj.getMonth() + 1;
					oms = oms < 10 ? '0' + oms : oms,
						ods = timeObj.getDate(),
						ods = ods < 10 ? '0' + ods : ods,
						ohs = timeObj.getHours(),
						ohs = ohs < 10 ? '0' + ohs : ohs,
						ofs = timeObj.getMinutes(),
						ofs = ofs < 10 ? '0' + ofs : ofs,
						oss = timeObj.getSeconds(),
						oss = oss < 10 ? '0' + oss : oss,
						stimes = oys + "-" + oms + "-" + ods + "  " + ohs + ":" + ofs + ":" + oss;
					$("#createTimeJs").html(stimes);
				} else {
					$("#createTimeJs").html('--');
				};
				if(obj.dutyIdentify==1){
					$("input[name='responsity']").eq(0).prop("checked",true);
					$("input[name='responsity']").eq(1).prop("checked",false);
				}else{
					$("input[name='responsity']").eq(0).prop("checked",false);
					$("input[name='responsity']").eq(1).prop("checked",true);
				};

				$("input[name='isReWorkRecord']").eq(0).prop("checked",false);
				$("input[name='isReWorkRecord']").eq(1).prop("checked",true);

				$("input[name='isReleasedJs']").eq(0).prop("checked",false);
				$("input[name='isReleasedJs']").eq(1).prop("checked",true);

				$("input[name='checkGradeJs']").eq(0).prop("checked",false);
				$("input[name='checkGradeJs']").eq(1).prop("checked",true);

				idPlanJs = obj.id;
				if(str=='shjsfaCon'){
					$("#shwxjsBtn").empty().hide();
				}else if(str=='shjsfaBtn'){
					let str=`<div class="form-group">
									<button type="button" class="btn btn-info">确定</button>
								</div>`;
					if($("#shwxjsBtn button").length<=0){
						$("#shwxjsBtn").empty().append(str).show();
					};
				};
				$("#shjsfaCon").show();//维修结算---未审批
			} else {//10结算审核通过
				// console.log(obj,'结算审核通过')
				let passCreateTime = '--';
				if (obj.createTime) {
					let bpDateVal = new Date(obj.createTime);
					let bpYear = bpDateVal.getFullYear();
					let bpMonth = bpDateVal.getMonth() + 1;
					bpMonth = bpMonth <= 9 ? '0' + bpMonth : bpMonth;
					let bpDay = bpDateVal.getDate() <= 9 ? '0' + bpDateVal.getDate() : bpDateVal.getDate();
					let bpHour = bpDateVal.getHours() <= 9 ? '0' + bpDateVal.getHours() : bpDateVal.getHours();
					let bpMin = bpDateVal.getMinutes() <= 9 ? '0' + bpDateVal.getMinutes() : bpDateVal.getMinutes();
					let bpSec = bpDateVal.getSeconds() <= 9 ? '0' + bpDateVal.getSeconds() : bpDateVal.getSeconds();
					let callStart = bpYear + "-" + bpMonth + "-" + bpDay + '  ' + bpHour + ":" + bpMin + ":" + bpSec;
					passCreateTime = callStart;
				};
				let checkTime = '--';
				if (obj.checkTime) {
					let timeObj = new Date(obj.checkTime),
						oy = timeObj.getFullYear(),
						om = timeObj.getMonth() + 1;
					om = om < 10 ? '0' + om : om,
						od = timeObj.getDate(),
						od = od < 10 ? '0' + od : od,
						oh = timeObj.getHours(),
						oh = oh < 10 ? '0' + oh : oh,
						of = timeObj.getMinutes(),
						of = of < 10 ? '0' + of : of,
						os = timeObj.getSeconds(),
						os = os < 10 ? '0' + os : os,
						stime = oy + "-" + om + "-" + od + "  " + oh + ":" + of + ":" + os;
					checkTime = stime;
				}
				;
				let checkUser = obj.checkUser ? obj.checkUser : '--';
				let strSelf = "";//自定义
				let strOwn = '';//配件库拥有
				let partTitle = `<ul id="checkedPartsOwnShowJsss">
								<li>
									<div class="b">配件名称</div> 
									<div class="a">配件品牌</div>
									<div class="a">配件型号</div>
									<div class="a">配件费</div>
									<div class="">数量</div>
								</li>
							</ul> `;//
				if (obj.accountParts && obj.accountParts.length > 0) {
					$.each(obj.accountParts, function (index, value) {
						if (value.isCustomParts == 2) {//不是
							strOwn += `<li>
								<div class="b">` + value.partsName + `</div> 
								<div class="a">` + value.brandName + `</div>
								<div class="a">` + value.partsType + `</div>
								<div class="a">` + Number(value.price).toFixed(2) + `元/` + value.unitName + `</div>
								<div class="a">` + value.partsNum + value.unitName + `</div>
							</li>`;
						} else {//是
							let picUrlValPart = '';
							if (value.picUrl && value.picUrl.length > 0) {
								$.each(value.picUrl.split(','), function (index, value) {
									if (value != "" && value != ',') {
										if (value.indexOf('http://images.baijiaxiu.com/') < 0) {
											value = 'http://images.baijiaxiu.com/' + value;
										}
										;
										picUrlValPart += `<img class="mySelfImg" src="../../images/imgLoad.png" name="` + value + `">`;
									}
								});//配件图片
							}
							;
							let remark = value.remark ? value.remark : '--';
							let unitName = value.unitName ? value.unitName : '';
							strSelf += `<li><div>
									<div>配件名称:` + value.partsName + `</div>
									<div style="font-weight:400">配件品牌:` + value.brandName + `</div>
									<div style="font-weight:400">配件型号:` + value.partsType + `</div>
									<div style="font-weight:400">采购价:` + Number(value.purchasePrice).toFixed(2) + `元</div>
									<div>配件费:` + Number(value.price).toFixed(2) +`元</div>
									<div>配件数量:`+value.partsNum+unitName+`</div>
								</div>								
								<div>
									<div>配件图片:<br>` + picUrlValPart + `</div>
								</div>
								<div>
									<div>备注说明:` + remark + `</div>
								</div></li>`;
						}
						;
					});//配件信息列表
				};
				let AstrOwn = '';
				if (strOwn.length <= 0) {
					partTitle = '';
					AstrOwn = '';
				} else {
					AstrOwn = `<ul class="other">` + strOwn + `</ul>`;
				}
				;
				let AstrSelf = '';
				if (strSelf.length <= 0) {
					AstrSelf = '';
				} else {
					AstrSelf = `<ul class="normal">` + strSelf + `</ul>`;
				}
				;
				let peijianInfo = '';
				if (strSelf.length <= 0 && strOwn.length <= 0) {
					peijianInfo = '--';
				} else {
					peijianInfo = `<div class="peijian"><div class="form-control selfBors">` + partTitle + AstrOwn + AstrSelf + `</div></div>`;
				}
				;
				let picUrlVal = '';
				if (obj.maintainUrl && obj.maintainUrl.length > 0) {
					$.each(obj.maintainUrl.split(','), function (index, value) {
						if (value != "" && value != ',') {
							picUrlVal += `<img class="mySelfImg" src="../../images/imgLoad.png" name="` + value + `">`;
						}
					});//维修图片
				}
				;
				let maintainRemark = obj.maintainRemark ? obj.maintainRemark : '--';//维修方案
				let strdi = '--';
				if (obj.dutyIdentify == 1) {
					strdi = `租户责任`;
				} else {
					strdi = `非租户责任`;
				};//dutyIdentify-责任鉴定(1-租户责任，2-非租户责任)
				let artNinetyRework= '';
				if (obj.artNinetyRework  == 1) {
					let str="是";
					artNinetyRework = '<div class="form-group"><label class="control-label">是否是90天返修工单:</label><div class=""><div class="form-control selfBor faultCause">'+str+'</div></div></div>';
				} else if (obj.artNinetyRework  == 2){
					let str="否";
					artNinetyRework = '<div class="form-group"><label class="control-label">是否是90天返修工单:</label><div class=""><div class="form-control selfBor faultCause">'+str+'</div></div></div>';
				}else{
					artNinetyRework='';
				};//artNinetyRework 人工判断是否是90天返修工单(1-是；2-不是) ,
				let isReleased = '--';
				if (obj.isReleased == 1) {
					isReleased = "是";
				} else {
					isReleased = "否";
				}
				;//是否为保外维修单(1-是；2-否)
				let checkRemark = obj.checkRemark ? obj.checkRemark : "--";
				let otherMoney = isNaN(obj.otherMoney) ? 0.00 : Number(obj.otherMoney).toFixed(2);//其他费用
				let urgentMoney = isNaN(obj.urgentMoney) ? 0.00 : Number(obj.urgentMoney).toFixed(2);// 加急费用
				let partsTotalCost = isNaN(obj.partsTotalCost) ? 0.00 : Number(obj.partsTotalCost).toFixed(2);//配件费用总计
				partsTotals = partsTotalCost;
				let callMoney = isNaN(obj.callMoney) ? 0.00 : Number(obj.callMoney).toFixed(2);//上门费用
				let contactOvertime = isNaN(obj.contactOvertime) ? 0.00 : Number(obj.contactOvertime).toFixed(2);//联系超时
				let visitOvertime = isNaN(obj.visitOvertime) ? 0.00 : Number(obj.visitOvertime).toFixed(2);//上门超时
				let feedbackOvertime = isNaN(obj.feedbackOvertime) ? 0.00 : Number(obj.feedbackOvertime).toFixed(2);//反馈超时
				let partsOvertime = isNaN(obj.partsOvertime) ? 0.00 : Number(obj.partsOvertime).toFixed(2);//配件超时
				let deleteFeeVal = Number(contactOvertime * 1 + visitOvertime * 1 + feedbackOvertime * 1 + partsOvertime * 1).toFixed(2);//费用扣除总计
				let partsFeeJs = Number(otherMoney * 1 + urgentMoney * 1 + callMoney * 1 + partsTotalCost * 1).toFixed(2);//工单费用总计
				secondGradecreate = obj.secondClassifyId
				let maintainAreaVal = obj.maintainArea?obj.maintainArea:'——';
				let classifyNamVal=''
				if(obj.firstName && obj.secondName){
					classifyNamVal = obj.firstName +'——' + obj.secondName
				}else{
					classifyNamVal = '——'
				}
				let reasonBelongStr = obj.reasonPlanVOS?'':'——'
				let planBelongStr = obj.reasonPlanVOS?'':'——'
				let reasonPlanVOS = obj.reasonPlanVOS?obj.reasonPlanVOS:[];
				let ri = 1;
				let pi = 1;
				$.each(reasonPlanVOS,function(i,v){
					if(v.type==1){
						reasonBelongStr+=`<div>`+ri+`：`+v.name+`</div>`
						ri++;
					}
					if(v.type==2){
						planBelongStr+=`<div>`+pi+`：`+v.name+`</div>`
						pi++;
					}
				})

				let contentText = `<div class="planCon">
					<div class="form-group">
						<label class="control-label">维修分类:</label> 
						<div class=""><div class="form-control selfBor pre" id="secGradeJsSuc">`+classifyNamVal+`</div></div>
					</div>
					<div class="form-group">
						<label class="control-label">维修区域:</label> 
						<div class=""><div class="form-control selfBor pre">` + maintainAreaVal + `</div></div>
					</div>
					<div class="form-group">
						<label class="control-label">原因归属:</label> 
						<div class=""><div class="form-control selfBor pre">` + reasonBelongStr + `</div></div>
					</div>
					<div class="form-group">
						<label class="control-label">方案归属:</label> 
						<div class=""><div class="form-control selfBor pre">` + planBelongStr + `</div></div>
					</div>
					<div class="form-group">
						<label class="control-label">维修方案:</label> 
						<div class=""><div class="form-control selfBor pre">` + maintainRemark + `</div></div>
					</div>
					<div class="form-group">
						<label class="control-label">维修图片:</label> 
						<div class="" style="vertical-align:text-top;width:80%">
							<div class="form-control selfBor">` + picUrlVal + `</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">责任鉴定:</label>
						<div class=""><div class="form-control selfBor faultCause">` + strdi + `</div></div>
					</div>
					<div class="form-group">
						<label class="control-label">工单结算标签:</label> 
						<div class=""><div class="form-control selfBor" id="gdjsbqJsOk"></div></div>
					</div>
					<div class="form-group">
						<label class="control-label">配件信息:</label>` + peijianInfo + `
					</div>	
					<div class="otherCon">
						<div class="form-group">
							<label class="control-label">上门费用:</label> 
							<div class=""><div class="form-control selfBor">` + callMoney + `元</div></div>
						</div>								
						<div class="form-group">
							<label class="control-label">加急费用:</label> 
							<div class=""><div class="form-control selfBor">` + urgentMoney + `元</div></div>
						</div>	
						<div class="form-group">
							<label class="control-label">其他费用:</label> 
							<div class=""><div class="form-control selfBor">` + otherMoney + `元</div></div>
						</div>
						<div class="form-group">
							<label class="control-label">配件费用:</label> 
							<div class=""><div class="form-control selfBor">` + partsTotalCost + `元</div></div>
						</div>
					</div>
					<div class="otherCon">
						<div class="form-group">
							<label class="control-label">联系超时:</label> 
							<div class=""><div class="form-control selfBor">` + contactOvertime + `元</div></div>
						</div>								
						<div class="form-group">
							<label class="control-label">上门超时:</label> 
							<div class=""><div class="form-control selfBor">` + visitOvertime + `元</div></div>
						</div>	
						<div class="form-group">
							<label class="control-label">反馈超时:</label> 
							<div class=""><div class="form-control selfBor">` + feedbackOvertime + `元</div></div>
						</div>
						<div class="form-group">
							<label class="control-label">配件超时:</label> 
							<div class=""><div class="form-control selfBor">` + partsOvertime + `元</div></div>
						</div>
					</div>
					<div class="otherCon">							
						<div class="form-group">
							<label class="control-label">工单费用总计:</label> 
							<div class=""><div class="form-control selfBor">` + partsFeeJs + `元</div></div>
						</div>	
						<div class="form-group">
							<label class="control-label">费用扣除总计:</label> 
							<div class=""><div class="form-control selfBor">` + deleteFeeVal + `元</div></div>
						</div>
						<div></div><div></div>
					</div>
					<div class="form-group" id="RecordDetailTilJs" style="display: none">
						<label class="control-label til">维修员反馈详情: </label>
					</div>
					<div class="form-group" id="RecordDetailJs" style="display: none">
						<div>90天返修工单：<em ></em></div>
						<div id="RecordDetailJsNote">备注说明：<em ></em></div>
						<div id="RecordDetailJsImg">图片<div id="feedBackImgJs"></div></div>
					</div>
					<div class="form-group" id="NinetyRecordConNodataJs">
						<label class="control-label til">90天内维修记录: <em class="cirWen" title="90天内维修记录规则：90天内在同一公寓，有相同的报修区域，且一级工单类型，二级工单类型相同已维修完成工单记一次维修">?</em></label>
						<div class="">
							<div class="form-control selfBor"></div>
						</div>
					</div>
					<div class="form-group" id="NinetyRecordConJs">
						<div class="left_record">
							<div style="width: 100%;height:100%;overflow: scroll">
								<ul id="recordListConJs"></ul>
							</div>
						</div>
						<div class="right_record">
							<div>
								<ul>
									<li>故障原因：<em id="faultReasonReJs"></em></li>
									<li>维修方案：<em id="matinPlanReJs"></em></li>
									<li>维修图片：<div id="imgRecordJs"></div></li>
									<li>上门费：<em id="smCostReJs"></em></li>
									<li>加急费：<em id="jsCostReJs"></em></li>
									<li>其他费：<em id="otherCostReJs"></em></li>
									<li>配件费：<em id="partCostReJs"></em></li>
									<li id="partDetailReJs"></li>
									<li>责任鉴定：<em id="zrJs"></em></li>
									<li>结算审核是否通过：<em id="shjsPassJs"></em></li>
									<li id="nintyRecordJs" style="display: none;">是否是90天返修：<em></em></li>
								</ul>
							</div>
						</div>
					</div>`+artNinetyRework+`
					<div class="form-group">
						<label class="control-label">保外维修单:</label> 
						<div class="">
							<div class="form-control selfBor">` + isReleased + `</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">创建时间:</label> 
						<div class="">
							<div class="form-control selfBor">` + passCreateTime + `</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">审核人:</label> 
						<div class="">
							<div class="form-control selfBor">` + checkUser + `</div>
						</div>
					</div>	
					<div class="form-group">
						<label class="control-label">审核时间:</label> 
						<div class="">
							<div class="form-control selfBor">` + checkTime + `</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">审核备注:</label> 
						<div class="">
							<div class="form-control selfBor pre">` + checkRemark + `</div>
						</div>
					</div>	
				</div>`;
				$("#shjsfaCon").hide();
				$("#MaintettlemeSntCon").empty().append(contentText).show();
				order_list.showImg('jsshtg');
			}
			;
			$("#gdjsbqJs").empty();
			$("#gdjsbqJsOk").empty();
			order_list.queryOrderCloseLabel();
			order_list.queryWorkerFeedbackInfo();
			order_list.queryNinetyReWorkRecord();
		},
		// 查询结算方案---(审核结算与结算方案为一个页面)
		queryOrderAccount(str){
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryOrderAccount, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId :selectId
			},function(result){
				let obj=result.accountDTO;
				order_list.setOrderAccount(obj,str);
			});
		},
		// 查询---(维修反馈详情)结算方案
		queryWorkerFeedbackInfo(){
			$("#RecordDetail").hide();
			$("#RecordDetailTil").hide();
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryWorkerFeedbackInfo, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId :selectId
			},function(result){
				if(result.result_code == 200){
					order_list.setWorkerFeedbackInfo(result.data);
				};
			});
		},
		// 查询---(维修反馈详情)结算方案
		setWorkerFeedbackInfo(obj,type){
			if(obj.length>0){
				if(selectIdSta==9){
					let imgUrl = '';
					if(obj[0].reworkUrl&&obj[0].reworkUrl.length>0){
						$.each(obj[0].reworkUrl.split(','), function (index, value) {
							if (value != "" && value != ',') {
								imgUrl += `<img class="mySelfImg" src="`+value+`" name="` + value + `">`;
							}
						});//维修图片
					}else{
						imgUrl = '暂无';
					};
					let staVal = obj[0].isRework==0?'不是':'是';
					let remarkVal = obj[0].remark?obj[0].remark:'暂无';
					$("#RecordDetail em").eq(0).text(staVal);
					$("#RecordDetail em").eq(1).text(remarkVal);
					$("#feedBackImg").empty().append(imgUrl);
					if(obj[0].isRework==0){
						$("#RecordDetailImg").show();
						$("#RecordDetailNote").show();
					}else{
						$("#RecordDetailImg").hide();
						$("#RecordDetailNote").hide();
					};
					$("#RecordDetail").show();
					$("#RecordDetailTil").show();
				}else{
					let imgUrl = '';
					if(obj[0].reworkUrl&&obj[0].reworkUrl.length>0){
						$.each(obj[0].reworkUrl.split(','), function (index, value) {
							if (value != "" && value != ',') {
								imgUrl += `<img class="mySelfImg" src="`+value+`" name="` + value + `">`;
							}
						});//维修图片
					}else{
						imgUrl = '暂无';
					};
					let staVal = obj[0].isRework==0?'不是':'是';
					let remarkVal = obj[0].remark?obj[0].remark:'暂无';
					$("#RecordDetailJs em").eq(0).text(staVal);
					$("#RecordDetailJs em").eq(1).text(remarkVal);
					$("#feedBackImgJs").empty().append(imgUrl);
					if(obj[0].isRework==0){
						$("#RecordDetailJsImg").show();
						$("#RecordDetailJsNote").show();
					}else{
						$("#RecordDetailJsImg").hide();
						$("#RecordDetailJsNote").hide();
					};
					$("#RecordDetailTilJs").show();
					$("#RecordDetailJs").show();
				};
			};
		},
		// 查询---(90天内维修记录)结算方案
		queryNinetyReWorkRecord(){
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryNinetyReWorkRecord, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId :selectId
			},function(result){
				if(result.result_code == 200){
					order_list.setNinetyReWorkRecord(result.data);
				};
			});
		},
		setNinetyReWorkRecord(data){
			if(selectIdSta==9){
				$("#recordListCon").empty();
				let strVal=`<li class="til">
				<div class="fixa">已结算返修标记</div>
				<div class="fixb">维修次数</div>
				<div class="fixc">工单ID</div>
				<div class="fixd">报修类型</div>
				<div class="auto">报修描述</div>
				<div class="fixc">维修师傅</div>
				<div class="fixe">完成时间</div>
				</li>`;
				if(data.length>0){
					order_list.queryOrderNewDesc(data[0].orderStatus,data[0].orderId);
					$.each(data,function(index,value){
						let actStr="";
						if(index==0){
							actStr="active";
						}
						let repairsNumVal='--';
						if(value.repairsNum){
							repairsNumVal=value.repairsNum;
						};
						let inputVal='';
						if(value.orderStatus==10){
							inputVal= value.clearingLabel==1?'√':'<input type="checkbox" name="'+value.orderId+'">';
						};
						let descVal='';
						if(value.faultDescriptionCuse){
							descVal+='客服描述：'+value.faultDescriptionCuse;
						};
						if(value.faultDescriptionUser ){
							descVal+='租户描述：'+value.faultDescriptionUser ;
						};
						strVal+=`<li name="`+value.orderStatus+`" idVal="`+value.orderId+`" class="`+actStr+`">
					<div  class="fixa">`+inputVal+`</div>
					<div  class="fixb">`+repairsNumVal+`</div>
					<div  class="fixc">`+value.outOrderNo+`</div>
					<div  class="fixd">`+value.firstGradeName+`-`+value.secondGradeName+`-`+value.thirdGradeName+`</div>
					<div  class="auto">`+descVal+`</div>
					<div  class="fixc">`+value.workerName+`</div>
					<div  class="fixe">`+value.practicalFinishTime+`</div>
				</li>`;
					});
					$("#NinetyRecordCon").show();
					$("#NinetyRecordConNodata").show();
				}else{
					strVal='';
					$("#NinetyRecordCon").hide();
					$("#NinetyRecordConNodata").hide();
				};
				$("#recordListCon").empty().append(strVal);
			}else{
				$("#recordListConJs").empty();
				let strVal=`<li class="til">
				<div class="fixa">已结算返修标记</div>
				<div class="fixb">维修次数</div>
				<div class="fixc">工单ID</div>
				<div class="fixd">报修类型</div>
				<div class="auto">报修描述</div>
				<div class="fixc">维修师傅</div>
				<div class="fixe">完成时间</div>
				</li>`;
				if(data.length>0){
					order_list.queryOrderNewDesc(data[0].orderStatus,data[0].orderId);
					$.each(data,function(index,value){
						let actStr="";
						if(index==0){
							actStr="active";
						}
						let str= value.clearingLabel==1?'√':'';
						let repairsNumVal='--';
						if(value.repairsNum){
							repairsNumVal=value.repairsNum;
						};
						let descVal='';
						if(value.faultDescriptionCuse){
							descVal+='客服描述：'+value.faultDescriptionCuse;
						};
						if(value.faultDescriptionUser ){
							descVal+='租户描述：'+value.faultDescriptionUser ;
						};
						strVal+=`<li name="`+value.orderStatus+`" idVal="`+value.orderId+`" class="`+actStr+`">
							<div  class="fixa">`+str+`</div>
							<div  class="fixb">`+repairsNumVal+`</div>
							<div  class="fixc">`+value.outOrderNo+`</div>
							<div  class="fixd">`+value.firstGradeName+`-`+value.secondGradeName+`-`+value.thirdGradeName+`</div>
							<div  class="auto">`+descVal+`</div>
							<div  class="fixc">`+value.workerName+`</div>
							<div  class="fixe">`+value.practicalFinishTime+`</div>
						</li>`;
					});
					$("#NinetyRecordConJs").show();
					$("#NinetyRecordConNodataJs").show();
				}else{
					strVal='';
					$("#NinetyRecordConJs").hide();
					$("#NinetyRecordConNodataJs").hide();
				};
				$("#recordListConJs").empty().append(strVal);
			};
		},
		//根据订单状态-判断请求维修方案or审核结算方案
		queryOrderNewDesc(sta,orderId){
			let url= sta==9?win.utils.services.orderMan_order_queryNewestMPlan:win.utils.services.orderMan_order_queryOrderAccount;
			win.utils.ajaxPost(url, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId :orderId
			},function(result){
				let data=sta==9?result.data:result.accountDTO;
				order_list.setOrderNewDesc(sta,data);
			});
		},
		setOrderNewDesc(sta,data){
			$("#shjsPass").parent('li').find('span').eq(0).remove();
			$("#shjsPassJs").parent('li').find('span').eq(0).remove();
			let faultCause=data.faultCause?data.faultCause:'暂无';//故障原因 ,
			let projectDesc='';//维修方案描述说明 ,
			let urgentCost='';//加急费用 ,
			let visitCost='';//上门费 ,
			let otherCost='';//其他费用 ,
			let partsTotalCost = '';
			let dutyIdentifyVal=data.dutyIdentify==1?'租户责任':'非租户责任';// 责任鉴定(1-租户责任，2-非租户责任) ,
			let accountStatus='';// (integer, optional): ;//结算状态(1、通过；2、不通过；3、待审核) ,
			let strOwn='';
			let picUrlValPart='';
			let artNinetyReworkVal='';
			let artNinetyReworkValNin='';
			if(sta==9){
				// faultCause (string, optional): 故障原因
				projectDesc=data.projectDesc?data.projectDesc:'暂无';//维修方案描述说明 ,
				urgentCost=data.urgentCost?data.urgentCost:'0.00';//加急费用 ,
				visitCost=data.visitCost?data.visitCost:'0.00';//上门费 ,
				otherCost=data.otherCost?data.otherCost:'0.00';//其他费用 ,
				partsTotalCost=data.partsTotalCost?data.partsTotalCost:'0.00';//配件费用总计 ,
				accountStatus='待审核';
				if(data.maintainParts&&data.maintainParts.length>0){
					$.each(data.maintainParts,function(index,value){
						let selfNameVal=value.isCustomParts==2?'【配件库】':'【报备配件】';
						strOwn+=selfNameVal+value.partsName+`-`+Number(value.price).toFixed(2)+`元/`+value.unitName+`*`+value.partsNum+`=`+Number(value.price).toFixed(2)*Number(value.partsNum)+`元；`;
					});//配件信息列表
				};
				if(data.maintainPhoto&&data.maintainPhoto.length>0){
					$.each(data.maintainPhoto.split(','),function(index,value){
						if(value!=''&&value!=','){
							picUrlValPart+=`<img class="mySelfImg" src="`+value+`" name="`+value+`">`;
						}
					});//配件图片
				};
			}else{
				// remark (string, optional): 备注说明 ,
				projectDesc=data.maintainRemark?data.maintainRemark:'暂无';//维修方案描述说明 ,
				urgentCost=data.urgentMoney?data.urgentMoney:'0.00';//加急费用 ,
				visitCost=data.callMoney?data.callMoney:'0.00';//上门费 ,
				otherCost=data.otherMoney?data.otherMoney:'0.00';//其他费用 ,
				partsTotalCost=data.partsTotalCost?data.partsTotalCost:'0.00';//配件费用总计 ,
				if(data.accountStatus == 1){
					accountStatus='通过';
					artNinetyReworkVal=data.artNinetyRework?data.artNinetyRework:0;//人工判断是否是90天返修工单(1-是；2-不是) ,
				}else if(data.accountStatus == 2){
					accountStatus='不通过';
				}else if(data.accountStatus == 3){
					accountStatus='待审核';
				};
				if(data.accountParts&&data.accountParts.length>0){
					$.each(data.accountParts,function(index,value){
						let selfNameVal=value.isCustomParts==2?'【配件库】':'【报备配件】';
						strOwn+=selfNameVal+value.partsName+`-`+Number(value.price).toFixed(2)+`元/`+value.unitName+`*`+value.partsNum+`=`+Number(value.price).toFixed(2)*Number(value.partsNum)+`元；`;
					});//配件信息列表
				};
				if(data.maintainUrl&&data.maintainUrl.length>0){
					$.each(data.maintainUrl.split(','),function(index,value){
						if(value!=''&&value!=','){
							picUrlValPart+=`<img class="mySelfImg" src="`+value+`" name="`+value+`">`;
						}
					});//配件图片
				};
			};

			if(selectIdSta==9){
				$("#faultReasonRe").text(faultCause);
				$("#matinPlanRe").text(projectDesc);
				$("#smCostRe").text(visitCost+'元');
				$("#jsCostRe").text(urgentCost+'元');
				$("#otherCostRe").text(otherCost+'元');
				$("#partCostRe").text(partsTotalCost+'元');
				$("#zr").text(dutyIdentifyVal);
				$("#partDetailRe").empty().append(strOwn);
				$("#imgRecord").empty().append(picUrlValPart);
				if(artNinetyReworkVal==1){
					artNinetyReworkValNin=`<span class="bgg">90天返修</span>`;
				};
				$("#shjsPass").text(accountStatus).after(artNinetyReworkValNin);
			}else{
				$("#faultReasonReJs").text(faultCause);
				$("#matinPlanReJs").text(projectDesc);
				$("#smCostReJs").text(visitCost+'元');
				$("#jsCostReJs").text(urgentCost+'元');
				$("#otherCostReJs").text(otherCost+'元');
				$("#partCostReJs").text(partsTotalCost+'元');
				$("#zrJs").text(dutyIdentifyVal);
				$("#partDetailReJs").empty().append(strOwn);
				$("#imgRecordJs").empty().append(picUrlValPart);
				if(artNinetyReworkVal==1){
					artNinetyReworkValNin=`<span class="bgg">90天返修</span>`;
				};
				$("#shjsPassJs").text(accountStatus).after(artNinetyReworkValNin);
			};
		},
		//设定配件单位
		setPartUnitFn(obj){
			let str='<option value="">请选择配件单位</option>';
			$.each(obj,function(index,value){
				str+=`<option value="`+value.unitName+`">`+value.unitName+`</option>`
			});
			$("#partUnitAdd").empty().append(str);
		},
		//紧急费用赋值，上门费用赋值
		setUrgentVisitFeeFn(sta,type){
			//sta:1创建维修方案  2审核结算方案
			visitFeeSelected=visitFeeSelected==0.00?'':visitFeeSelected;//上门费用-selected
			urgentFeeSelected=urgentFeeSelected==0.00?'':urgentFeeSelected;//紧急费用-selected
			otherFeeSelected=otherFeeSelected==0.00?'':otherFeeSelected;//其他费用-selected
			let visitVals='';
			if(visitFeeArr.length>0){
				$.each(visitFeeArr,function(index,value){
					let idVal=Number(value.price).toFixed(2);
					if(idVal==0.00){
						idVal='';
					};
					let selectedVal='';
					if(visitFeeSelected==idVal){
						selectedVal='selected';
					};
					visitVals+=`<option value="`+idVal+`"  `+selectedVal+`>`+Number(value.price).toFixed(2)+`</option>`
				});
			}

			let urgentVals='';
			if(urgentFeeArr.length>0){
				$.each(urgentFeeArr,function(index,value){
					let idVal=Number(value.price).toFixed(2);
					if(idVal==0.00){
						idVal='';
					};
					let selectedVal='';
					if(urgentFeeSelected==idVal){
						selectedVal='selected';
					};
					urgentVals+=`<option value="`+idVal+`"  `+selectedVal+`>`+Number(value.price).toFixed(2)+`</option>`
				});
			}

			let otherVals=`<option value="0.00">0.00</option>`;
			if(otherFeeArr.length>0){
				$.each(otherFeeArr,function(index,value){
					let idVal=Number(value.price).toFixed(2);
					if(idVal==0.00){
						idVal='';
					};
					let selectedVal='';
					if(otherFeeSelected==idVal){
						selectedVal='selected';
					};
					otherVals+=`<option value="`+idVal+`"  `+selectedVal+`>`+Number(value.price).toFixed(2)+`</option>`;
				});
			};
			if(sta==1){
				$("#shangmenCost").empty().append(visitVals);//上门费
				if(type!=1) {
					$("#jiajiCost").empty().append(urgentVals);//加急费
					$("#otherCost").empty().append(otherVals);//其他费
					if ($("#jiajiCost").is(':disabled') == true) {
						$("#jiajiCost").val('');
					}
					;
					if (otherFeeArr.length > 0) {
						$("#otherCostCon").show();
					} else {
						$("#otherCostCon").hide();
					}
				}
			}else{
				$("#indoorJs").empty().append(visitVals);//上门费
				$("#urgentMoneyJs").empty().append(urgentVals);//加急费
				$("#otherMoneyJs").empty().append(otherVals);//其他费
			};
		},
		//赋值-创建最新维修方案---当为维修方案协调中时，调用
		setMaintainPlan(data){
			order_list.queryMatinPlanList();//获取维修方案模板
			visitFeeSelected='';
			urgentFeeSelected='';
			otherFeeSelected='';
			let newObj=data;
			let orderStatus=newObj.orderStatus;
			if(orderStatus==6){
				$("#controlPlanCon").show();
			}else{
				$("#controlPlanCon").hide();
			};
			if(newObj.secondClassifyId){
				secondGradecreate = newObj.secondClassifyId
				order_list.queryMatinClassify(1,2,2)
				let arr = newObj.reasonPlanVOS?newObj.reasonPlanVOS:[]
				order_list.getReasonMatinplanList(1,secondGradecreate,1,arr)
			}else{
				order_list.queryMatinClassify(1,2,2)
				let arr = newObj.reasonPlanVOS?newObj.reasonPlanVOS:[]
				order_list.getReasonMatinplanList(1,secondGradecreate,1,arr)
				// order_list.queryMatinClassify(1,1)
			}
			order_list.querySuiteArea(1,newObj.maintainArea)
			$("#orderStatus").val(orderStatus);
			$("#faultCause").val(newObj.faultCause);
			$("#faultCause").parent('div').find(".inputNums>em").text(newObj.faultCause.length);
			$("#servicePlanId").val(newObj.servicePlanId);
			MatinPlanTemListName=newObj.templateName?newObj.templateName:'';
			if(MatinPlanTemListName.length > 0){
				let projectDescVal = newObj.templateProjectDesc;
				$("#projectDescBlCon").show()
				$("#projectDescCon").hide()
				$("#projectDescBl").html(projectDescVal);
				let objVal = $("#projectDescBl").text()
				$("#projectDescBl").parent('div').find(".inputNums>em").text(objVal.length);
			}else{
				let projectDescVal = newObj.projectDesc;
				$("#projectDescBlCon").hide()
				$("#projectDescCon").show()
				$("#projectDesc").val(projectDescVal);
				$("#projectDesc").parent('div').find(".inputNums>em").text(projectDescVal.length);
			}
			activeNextTransText()
			let imgStr='';
			if(newObj.maintainPhoto){
				$.each(newObj.maintainPhoto.split(','),function(index,value){
					if(value.length>0){
						imgStr+=`<li>` +
							`<img class="cardImage mySelfImg" style="margin-top: 10px; width: 100px; height: 100px; max-width: 400px; border: 10px solid #EEEEEE;" src="../../images/imgLoad.png" name="`+value+`">`+
							`<span class="layui-layer-setwin">×</span>` +`</li>`;
					}
				});
			};
			$("#uploadImgBox").empty().append(imgStr);//维修图片
			if(newObj.dutyIdentify==1){
				$("#dutyIdentifyA").prop({'checked':true});
				$("#dutyIdentifyB").prop({'checked':false});
			}else{
				$("#dutyIdentifyA").prop({'checked':false});
				$("#dutyIdentifyB").prop({'checked':true});
			};// dutyIdentify责任鉴定(1-租户责任，2-非租户责任) ,
			partsTotals=newObj.partsTotalCost; //配件费用
			partsCheckedCjSelf=[];//创建维修方案-配件-已选择配件---自己添加---修改后
			partsCheckedCjOwnAf=[];//创建维修方案-配件-已选择配件---配件库---修改后
			partsCheckedCjOwnBf=[];//创建维修方案-配件-已选择配件---配件库---修改前
			let strSelf="";//自定义
			let strOwn='';//配件库拥有
			if(newObj.maintainParts&&newObj.maintainParts.length>0){
				$.each(newObj.maintainParts,function(index,value){
					if(value.isCustomParts==2){//不是
						partsCheckedCjOwnAf.push(value);//创建维修方案-配件-已选择配件---配件库---修改后
						partsCheckedCjOwnBf.push(value);//创建维修方案-配件-已选择配件---配件库---修改前
						strOwn+=`<li>
							<div class="b">`+value.partsName+`</div> 
							<div class="a">`+value.brandName+`</div>
							<div class="a">`+value.partsType+`</div>
							<div class="a">`+Number(value.price).toFixed(2)+`元/`+value.unitName+`</div>
							<div class="a"><input class="num" type="number" min=0 title="oneNumber" id="`+value.id+`" value="`+value.partsNum+`">`+value.unitName+`</div>
							<div class="deletePartOwn" name="del">✖</div>
						</li>`;
					}else{//是
						partsCheckedCjSelf.push(value);//创建维修方案-配件-已选择配件---自己添加---
						let picUrlValPart='';
						if(value.picUrl&&value.picUrl.length>0){
							$.each(value.picUrl.split(','),function(index,value){
								if(value!=''&&value!=','){
									if(value.indexOf('http://images.baijiaxiu.com/')<0){
										value='http://images.baijiaxiu.com/'+value;
									};
									picUrlValPart+=`<img class="mySelfImg" src="../../images/imgLoad.png" name="`+value+`">`;
								}
							});//配件图片
						};
						let remark=value.remark?value.remark:'--';
						let unitName = value.unitName ? value.unitName : '';
						strSelf+=`<li><div>
								<div>配件名称:`+value.partsName+`</div>
								<div>配件品牌:`+value.brandName+`</div>
								<div>配件型号:`+value.partsType+`</div>
							</div>
							<div>
								<div>采购价:`+Number(value.purchasePrice).toFixed(2)+`元</div>
								<div>配件费:`+Number(value.price).toFixed(2)+`元</div>
								<div>配件数量:`+value.partsNum+unitName+`</div>
							</div>								
							<div>
								<div>配件图片:<br>`+picUrlValPart+`</div>
							</div>
							<div>
								<div>备注说明:`+remark+`</div>
							</div>	
							<div class="deletePartOwn" name="del">✖</div>										
							<div class="addSelfPart" name="cjwxfa">修改</div></li>`;
					};
				});//配件信息列表
			};
			if(partsCheckedCjOwnAf.length>0){
				$("#checkedPartsOwnShows").show()
			}else{
				$("#checkedPartsOwnShows").hide()
			};
			$("#checkedPartsOwnShow").empty().append(strOwn); //配件库选择
			$("#checkedPartsSelf").empty().append(strSelf);  //自定义添加
			let visitCost=newObj.visitCost?Number(newObj.visitCost).toFixed(2):0.00;
			let urgentCost=newObj.urgentCost?Number(newObj.urgentCost).toFixed(2):0.00;
			let otherCost=newObj.otherCost?Number(newObj.otherCost).toFixed(2):0.00;
			visitFeeSelected=visitCost;
			urgentFeeSelected=urgentCost;
			otherFeeSelected=otherCost;
			order_list.queryCostConfigures(1);
			let arr = [];
			arr = partsCheckedCjSelf.concat(partsCheckedCjOwnAf)
			if(arr.length>0){
				isExistPartsBfFlag = true
				order_list.queryCostConfigures(1,1);
			}
			$("#cjPartsPrice").html('配件费用总计：'+partsTotals+'元');
			order_list.showImg("cjwxfa");
		},
		// 查询维修方案list-创建维修方案后的list
		queryMaintainPlan(type){
			if(type!=1){
				$("#maintPlanCon").empty();
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryMaintainPlan, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId :selectId
			},function(result){
				let obj=result.maintainPlanDTOS;//方案Arr---
				if(obj.length<=0){
					newPlan=[];
					return;
				};
				newPlan=obj[0];
				let checkRemarkVal='--';// 审批备注 ,
				let checkStatusVal='--';//审核状态(1、方案通过，2、方案不通过，3、待审批) ,
				let checkTimeVal='--';//审核时间 ,
				let checkUserVal= '--';// 审核人 ,
				let checkBtnVal= '';// 审核按钮 ,
				let dutyIdentifyVal='--';//责任鉴定(1-租户责任，2-非租户责任) ,
				let faultCause='--';//故障原因 ,
				let idVal=null;//维修方案id ,
				let maintainPartsAVal= '';//配件信息列表 ---配件库,
				let maintainPartsBVal= '';//配件信息列表 ---自添加,
				let maintainPhotoVal= '';//维修照片(多张逗号分隔) ,
				let orderIdVal=null;//工单id ,
				let orderStatusVal=null;// 订单状态（6-需要协调，7-已完成，8-未完成） ,
				let otherCost =0;//其他费用 ,
				let visitCost =0;//上门费用 ,
				let urgentCost =0;//加急费用 ,
				let totalCost =0;//总费用 ,
				let partsTotalCostVal=null;//配件费用总计 ,
				let projectDescVal='--';//维修方案描述说明 ,
				let servicePlanIdVal=null;//协调方案id ,
				let servicePlanNameVal ='--';//协调方案名称 ,
				let	createTime	='--';//创建时间
				let	createUser='--';//创建人
				let AppendStr='';
				$.each(obj,function(index,value){
					if(type!=1){
						order_list.queryCoordinationRecord(value.id,index);
					};
					faultCause=value.faultCause;//故障原因
					servicePlanNameVal=value.servicePlanName?value.servicePlanName:'--';//协调方案名称
					projectDescVal=value.projectDesc;//维修方案
					visitCost=value.visitCost?Number(value.visitCost).toFixed(2):0.00;//上门费用
					otherCost=value.otherCost?Number(value.otherCost).toFixed(2):0.00;//其他费用
					urgentCost=value.urgentCost?Number(value.urgentCost).toFixed(2):0.00;//加急费用
					partsTotalCostVal=value.partsTotalCost?Number(value.partsTotalCost).toFixed(2):0.00;//配件总费用
					totalCost=value.totalCost?Number(value.totalCost).toFixed(2):Number(visitCost+otherCost+urgentCost+partsTotalCostVal).toFixed(2);//总费用
					let bpDateVal=new Date(value.createTime);//创建时间
					let bpYear=bpDateVal.getFullYear();
					let bpMonth=bpDateVal.getMonth()+1;
					bpMonth=bpMonth<=9?'0'+bpMonth:bpMonth;
					let bpDay=bpDateVal.getDate()<=9?'0'+bpDateVal.getDate():bpDateVal.getDate();
					let bpHour=bpDateVal.getHours()<=9?'0'+bpDateVal.getHours():bpDateVal.getHours();
					let bpMin=bpDateVal.getMinutes()<=9?'0'+bpDateVal.getMinutes():bpDateVal.getMinutes();
					let bpSec=bpDateVal.getSeconds()<=9?'0'+bpDateVal.getSeconds():bpDateVal.getSeconds();
					createTime=bpYear+"-"+bpMonth+"-"+bpDay+'  '+bpHour+":"+bpMin+":"+bpSec;
					createUser = value.createUser;//创建人
					let maintainPartsAVal= '';//配件信息列表 ---配件库,
					let maintainPartsBVal= '';//配件信息列表 ---自添加,
					let maintainPartsCVal= '';//配件总费用 --,
					if(value.maintainParts&&value.maintainParts.length>0){
						$.each(value.maintainParts,function(index,value){
							let brandNameVal=value.brandName;//配件品牌
							let isCustomParts=value.isCustomParts;// -1是----2不是
							let partsName=value.partsName;//配件名称
							let partsNum=value.partsNum;//配件数量
							let partsType=value.partsType;//: "品牌型号"
							let picUrlVal='';
							if(value.picUrl&&value.picUrl.length>0){
								picUrlVal='';
								$.each(value.picUrl.split(','),function(index,value){
									if(value!=''&&value!=','){
										let dd='';
										if(value.indexOf('http://images.baijiaxiu.com/')<0){
											dd='http://images.baijiaxiu.com/'+value;
										}else if(value.indexOf('http://images.baijiaxiu.com/http://images.baijiaxiu.com')>-1){
											dd=value.replace('http://images.baijiaxiu.com/http://images.baijiaxiu.com','http://images.baijiaxiu.com');
										}else{
											dd=value;
										};
										picUrlVal+=`<img class="mySelfImg" src="../../images/imgLoad.png" name="`+dd+`">`;
									}
								});//配件图片
							};
							let price=value.price;//配件费（采购价+人工费）
							let purchasePrice=value.purchasePrice;//采购价
							let remark=value.remark?value.remark:'--';//备注
							let unitName = value.unitName ? value.unitName : '';//配件单位
							if(isCustomParts==2){
								let partOwn=`<li>
									<div class="b">`+value.partsName+`</div> 
									<div class="a">`+value.brandName+`</div>
									<div class="a">`+value.partsType+`</div>
									<div class="a">`+Number(value.price).toFixed(2)+`元/`+unitName+`</div>
									<div class="a">`+value.partsNum+`</div>
								</li>`;
								maintainPartsAVal+=partOwn;
							}else{
								if(picUrlVal==''){
									picUrlVal='--'
								};
								let partSelf=`<li>
								<div><div style="font-weight:bold;">配件名称:`+partsName+`</div>
									<div>品牌:`+brandNameVal+`</div>
									<div>品牌型号:`+partsType+`</div>
									<div>采购价:`+Number(purchasePrice).toFixed(2)+`元</div>
									<div style="font-weight:bold;">配件费:`+Number(price).toFixed(2)+`元</div>
									<div>配件数量:`+value.partsNum+unitName+`</div></div>
								<div><div class="fourh"><em>配件图片:</em>`+picUrlVal+`</div></div>
								<div><div class="fourh">备注说明:`+remark+`</div></div></li>`;
								maintainPartsBVal+=partSelf;
							};
						});//配件信息列表
					};
					let tHead=`<ul class="normal"><li>
							<div class="b">配件名称</div> 
							<div class="a">配件品牌</div>
							<div class="a">配件型号</div>
							<div class="a">配件费</div>
							<div class="a">配件数量</div>
						</li> `;
					let tbody=`</ul>`;
					let tcHead='';
					let tcbody='';
					maintainPartsCVal=`<label class="control-label">配件费用总计：</label> 
							<div class="form-control selfBor">`+Number(partsTotalCostVal).toFixed(2)+`元</div>`;
					if(maintainPartsAVal==''){
						maintainPartsAVal='暂无';
						tHead='';
						tbody='';
					};
					if(maintainPartsBVal==''){
						maintainPartsBVal='暂无';
						tcHead='';
						tcbody='';
					}else{
						tcHead=`<ul class="other">`;
						tcbody=`</ul>`;
					}
					if(maintainPartsAVal==''&&maintainPartsBVal==''){
						maintainPartsCVal='';
					}
					maintainPhotoVal='';
					if(value.maintainPhoto){
						$.each(value.maintainPhoto.split(','),function(index,value){
							if(value!=','&&value!=''){
								if(value.indexOf('http://images.baijiaxiu.com/')<0){
									value='http://images.baijiaxiu.com/'+value;
								};
								maintainPhotoVal+=`<img class="mySelfImg" src="../../images/imgLoad.png" name="`+value+`">`;
							}
						});//维修图片
					};
					dutyIdentifyVal=value.dutyIdentify==1?'租户责任':'非租户责任';//责任鉴定
					reasonAttributionVal=value.reasonAttribution?value.reasonAttribution:'--';
					//审核状态(1、方案通过，2、方案不通过，3、待审批) ,
					if(value.checkStatus==3){//待审批
						checkStatusVal='<span class="normalThrees">待审核</span>';
						checkRemarkVal='--';
						checkTimeVal='--';
						checkUserVal='--';
						checkBtnVal=`<button type="button" class="btn btn-info shenpiBtn" schemeVal="`+servicePlanNameVal+`" name="`+value.id+`">审批</button>`;
					}else{
						checkRemarkVal=value.checkRemark?value.checkRemark:'--';// 审批备注 ,
						let bpDateVal='--';
						if(value.checkTime){
							let bpDateVal=new Date(value.checkTime);//审核时间
							let bpYear=bpDateVal.getFullYear();
							let bpMonth=bpDateVal.getMonth()+1;
							bpMonth=bpMonth<=9?'0'+bpMonth:bpMonth;
							let bpDay=bpDateVal.getDate()<=9?'0'+bpDateVal.getDate():bpDateVal.getDate();
							let bpHour=bpDateVal.getHours()<=9?'0'+bpDateVal.getHours():bpDateVal.getHours();
							let bpMin=bpDateVal.getMinutes()<=9?'0'+bpDateVal.getMinutes():bpDateVal.getMinutes();
							let bpSec=bpDateVal.getSeconds()<=9?'0'+bpDateVal.getSeconds():bpDateVal.getSeconds();
							checkTimeVal=bpYear+"-"+bpMonth+"-"+bpDay+'  '+bpHour+":"+bpMin+":"+bpSec;
						}
						checkUserVal= value.checkUser?value.checkUser:'--';// 审核人 ,
						if(value.checkStatus==1){//通过
							checkStatusVal='<span class="normalOks">通过</span>';
						}else if(value.checkStatus==2){//不通过
							checkStatusVal='<span class="normalTwos">驳回</span>';
						};
						checkBtnVal='';
					};
					AppendStr+=`
				<div class="form-group boldTils">
					<label class="col-sm-6"style="padding-left:0">维修方案详情</label>
					<div class="col-sm-6 selfBor" style="text-align:right;font-size:18px;padding-right:0">状态:`+checkStatusVal+checkBtnVal+`</div>
				</div>
				<div class="planCon">
					<div class="left">
						<div class="form-group">
							<label class="control-label">故障原因:</label>
							<div class=""><div class="form-control selfBor faultCause pre">`+faultCause.replace(/>/g,'&>;').replace(/</g,'<;')+`</div></div>
						</div>
						<div class="form-group">
							<label class="control-label">协调方案分类:</label> 
							<div class=""><div class="form-control selfBor servicePlanName">`+servicePlanNameVal+`</div></div>
						</div>
						<div class="form-group">
							<label class="control-label">维修方案:</label> 
							<div class=""><div class="form-control selfBor pre">`+projectDescVal+`</div></div>
						</div>	
						<div class="form-group boldTil">
							<label class="control-label">费用总计:</label> 
							<div class=""><div class="form-control selfBor">`+Number(totalCost).toFixed(2)+`元</div></div>
						</div>
						<div class="fanganListFee">
							<div class="form-group">
								<label class="control-label">上门费用:</label> 
								<div class="form-control selfBor">`+Number(visitCost).toFixed(2)+`元</div>
							</div>
							<div class="form-group">
								<label class="control-label">加急费用:</label> 
								<div class="form-control selfBor">`+Number(urgentCost).toFixed(2)+`元</div>
							</div>
							<div class="form-group">
								<label class="control-label">其他费用:</label> 
								<div class="form-control selfBor">`+Number(otherCost).toFixed(2)+`元</div>
							</div>	
							<div class="form-group">`+maintainPartsCVal+`</div>	
						</div>					
						<div class="form-group">
							<label class="control-label">配件信息:</label> 
							<div class="peijian">
								<div class="form-control selfBors">`+tHead+maintainPartsAVal+tbody+`
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">报备配件:</label> 
							<div class="peijian">	
								<div class="form-control selfBors">`+tcHead+maintainPartsBVal+tcbody+`
								</div> 
							</div>
						</div>					
						<div class="form-group">
							<label class="control-label">维修图片:</label> <br>
							<div class="" style="vertical-align:text-top">
								<div class="form-control selfBor">`+maintainPhotoVal+`</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">责任鉴定:</label> 
							<div class="">
								<div class="form-control selfBor">`+dutyIdentifyVal+`</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">原因归属:</label> 
							<div class="">
								<div class="form-control selfBor">`+reasonAttributionVal+`</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">创建人:</label> 
							<div class="">
								<div class="form-control selfBor">`+createUser+`</div>
							</div>
						</div>	
						<div class="form-group">
							<label class="control-label">创建时间:</label> 
							<div class="">
								<div class="form-control selfBor">`+createTime+`</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审批人:</label> 
							<div class="">
								<div class="form-control selfBor shenpiP">`+checkUserVal+`</div>
							</div>
						</div>	
						<div class="form-group">
							<label class="control-label">审批时间:</label> 
							<div class="">
								<div class="form-control selfBor shenpiT">`+checkTimeVal+`</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">审批备注:</label> 
							<div class="">
								<div class="form-control selfBor pre shenpiN">`+checkRemarkVal+`</div>
							</div>
						</div>
						<div class="form-group chatRecord"></div>								
						<div class="form-group">
							<div class="hundred">
								<textarea class="form-control chat" maxlength="200"></textarea>
								<div class="inputNums"><em>0</em>/200</div>
							</div>
							<div class="hundred">
								<button type="button" class="btn btn-info planReply" data-index="`+index+`" name="`+value.id+`">回复</button>
							</div>								
						</div>																																								
					</div>
				</div>`;
				});
				$("#maintPlanCon").append(AppendStr).show();//维修方案
				order_list.showImg("wxfaList");

			});
		},
		// 查询维修协调记录list-详情展示
		queryMaintainPlanChecked(){
			$("#MaintCoordRecordCon").empty();
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryMaintainPlan, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId :selectId,
				checkStatus:1
			},function(result){
				let obj=result.maintainPlanDTOS;//方案Arr---
				let checkRemarkVal='--';// 审批备注 ,
				let checkStatusVal='方案通过';//审核状态(1、方案通过，2、方案不通过，3、待审批) ,
				let dutyIdentifyVal='--';//责任鉴定(1-租户责任，2-非租户责任) ,
				let faultCause='--';//故障原因 ,
				let idVal=null;//维修方案id ,
				let maintainPartsAVal= '';//配件信息列表 ---配件库,
				let maintainPartsBVal= '';//配件信息列表 ---自添加,
				let maintainPhotoVal= '';//维修照片(多张逗号分隔) ,
				let orderIdVal=null;//工单id ,
				let otherCost =0;//其他费用 ,
				let visitCost =0;//上门费用 ,
				let urgentCost =0;//加急费用 ,
				let partsTotalCostVal=null;//配件费用总计 ,
				let projectDescVal='--';//维修方案描述说明 ,
				let servicePlanIdVal=null;//协调方案id ,
				let servicePlanNameVal ='--';//协调方案名称 ,
				let	createTime	='--';//创建时间
				let	createUser='--';//创建人
				$.each(obj,function(index,value){
					faultCause=value.faultCause?value.faultCause:'--';//故障原因
					servicePlanNameVal=value.servicePlanName?value.servicePlanName:'';//协调方案名称
					projectDescVal=value.projectDesc;//维修方案
					otherCost=value.otherCost;//其他费用
					urgentCost=value.urgentCost;//加急费用
					visitCost=value.visitCost;//上门费用 ,
					partsTotalCostVal=value.partsTotalCost;//配件总费用
					let bpDateVal=new Date(value.createTime);//创建时间
					let bpYear=bpDateVal.getFullYear();
					let bpMonth=bpDateVal.getMonth()+1;
					bpMonth=bpMonth<=9?'0'+bpMonth:bpMonth;
					let bpDay=bpDateVal.getDate()<=9?'0'+bpDateVal.getDate():bpDateVal.getDate();
					let bpHour=bpDateVal.getHours()<=9?'0'+bpDateVal.getHours():bpDateVal.getHours();
					let bpMin=bpDateVal.getMinutes()<=9?'0'+bpDateVal.getMinutes():bpDateVal.getMinutes();
					let bpSec=bpDateVal.getSeconds()<=9?'0'+bpDateVal.getSeconds():bpDateVal.getSeconds();
					createTime=bpYear+"-"+bpMonth+"-"+bpDay+'  '+bpHour+":"+bpMin+":"+bpSec;
					createUser = value.createUser;//创建人
					let maintainPartsAVal= '';//配件信息列表 ---配件库,
					let maintainPartsBVal= '';//配件信息列表 ---自添加,
					let maintainPartsCVal= '';//配件总费用 --,
					if(value.maintainParts&&value.maintainParts.length>0){
						$.each(value.maintainParts,function(index,value){
							let brandNameVal=value.brandName;//配件品牌
							let isCustomParts=value.isCustomParts;// -1是----2不是
							let partsName=value.partsName;//配件名称
							let partsNum=value.partsNum;//配件数量
							let partsType=value.partsType;//: "品牌型号"
							let picUrlVal='';
							if(value.picUrl&&value.picUrl.length>0){
								$.each(value.picUrl.split(','),function(index,value){
									if(value!=','&&value!=''){
										if(value.indexOf('http://images.baijiaxiu.com/')<0){
											value='http://images.baijiaxiu.com/'+value;
										};
										picUrlVal+=`<img class="mySelfImg" src="../../images/imgLoad.png" name="`+value+`">`;
									}
								});//配件图片
							};
							let price=value.price;//配件费（采购价+人工费）
							let purchasePrice=value.purchasePrice;//采购价
							let remark=value.remark?value.remark:'--';//备注
							let unitName = value.unitName ? value.unitName : '';//配件单位
							if(isCustomParts==2){
								let	partOwn=`<li>
										<div class="b">`+value.partsName+`</div> 
										<div class="a">`+value.brandName+`</div>
										<div class="a">`+value.partsType+`</div>
										<div class="a">`+Number(value.price).toFixed(2)+`元/`+unitName+`</div>
										<div class="a">`+value.partsNum+unitName+`</div>
									</li>`;
								maintainPartsAVal+=partOwn;
							}else{
								if(picUrlVal==''){
									picUrlVal='--'
								};
								let partSelf=`<li>
									<div><div>配件名称:`+partsName+`</div>
										<div>品牌:`+brandNameVal+`</div>
										<div>品牌型号:`+partsType+`</div>
									    <div>采购价:`+Number(purchasePrice).toFixed(2)+`元</div>
									    <div>配件费:`+Number(price).toFixed(2)+`元</div>
									    <div>配件数量:`+value.partsNum+unitName+`</div>
									</div>
									<div><div class="fourh"><em>配件图片:</em>`+picUrlVal+`</div></div>
									<div><div class="fourh">备注说明:`+remark+`</div></div></li>`;
								maintainPartsBVal+=partSelf;
							};
						});//配件信息列表
					};
					let tHead=`<ul class="normal"><li>
								<div class="b">配件名称</div> 
								<div class="a">配件品牌</div>
								<div class="a">配件型号</div>
								<div class="a">配件费</div>
								<div class="a">配件数量</div>
							</li> `;
					let tbody=`</ul>`;
					let tcHead='';
					let tcbody='';
					maintainPartsCVal=`<div class="form-group"><label class="control-label">配件费用总计：</label> 
								<div class="form-control selfBor" style="width:66%">`+Number(partsTotalCostVal).toFixed(2)+`元</div></div>`;
					if(maintainPartsAVal==''){
						maintainPartsAVal='暂无';
						tHead='';
						tbody='';
					};
					if(maintainPartsBVal==''){
						maintainPartsBVal='暂无';
						maintainPartsCVal='';
						tcHead='';
						tcbody='';
					}else{
						tcHead=`<ul class="other">`;
						tcbody=`</ul>`;
					}
					if(maintainPartsAVal==''&&maintainPartsBVal==''){
						maintainPartsCVal='';
					}
					maintainPhotoVal='';
					if(value.maintainPhoto){
						$.each(value.maintainPhoto.split(','),function(index,value){
							if(value!=''&&value!=","){
								maintainPhotoVal+=`<img class="mySelfImg" src="../../images/imgLoad.png" name="`+value+`">`;
							}
						});//维修图片
					}
					dutyIdentifyVal=value.dutyIdentify==1?'租户责任':'非租户责任';//责任鉴定
					reasonAttributionVal=value.reasonAttribution?value.reasonAttribution:'--';//原因归属
					//审核状态(1、方案通过，2、方案不通过，3、待审批) ,
					let xtfaflVal='';
					if(servicePlanNameVal!=''){
						xtfaflVal=`	<div class="form-group">
							<label class="control-label">协调方案分类:</label> 
							<div class=""><div class="form-control selfBor servicePlanName">`+servicePlanNameVal+`</div></div>
						</div>`;
					};
					let AppendStr=`<div class="form-group boldTils">
						<label class="col-sm-6"style="padding-left:0">维修方案详情</label>
					</div>
					<div class="planCon">
						<div class="left">
							<div class="form-group">
								<label class="control-label">故障原因:</label>
								<div class=""><div class="form-control selfBor faultCause pre">`+faultCause.replace('<','').replace('>','')+`</div></div>
							</div>
							`+xtfaflVal+`
							<div class="form-group">
								<label class="control-label">维修方案:</label> 
								<div class=""><div class="form-control selfBor pre">`+projectDescVal+`</div></div>
							</div>	
							`+maintainPartsCVal+`
							<div class="form-group">
								<label class="control-label">配件信息:</label> 
								<div class="peijian">
									<div class="form-control selfBors">`+tHead+maintainPartsAVal+tbody+`
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">报备配件:</label> 
								<div class="peijian">	
									<div class="form-control selfBors">`+tcHead+maintainPartsBVal+tcbody+`
									</div> 
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label">其他费用:</label> 
								<div class=""><div class="form-control selfBor">`+Number(otherCost).toFixed(2)+`元</div></div>
							</div>
							<div class="form-group">
								<label class="control-label">上门费用:</label> 
								<div class=""><div class="form-control selfBor">`+Number(visitCost).toFixed(2)+`元</div></div>
							</div>								
							<div class="form-group">
								<label class="control-label">加急费用:</label> 
								<div class=""><div class="form-control selfBor">`+Number(urgentCost).toFixed(2)+`元</div></div>
							</div>																														
							<div class="form-group">
								<label class="control-label">维修图片:</label> <br>
								<div class="" style="vertical-align:text-top">
									<div class="form-control selfBor">`+maintainPhotoVal+`</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">责任鉴定:</label> 
								<div class="">
									<div class="form-control selfBor">`+dutyIdentifyVal+`</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">原因归属:</label> 
								<div class="">
									<div class="form-control selfBor">`+reasonAttributionVal+`</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label">创建人:</label> 
								<div class="">
									<div class="form-control selfBor">`+createUser+`</div>
								</div>
							</div>	
							<div class="form-group">
								<label class="control-label">创建时间:</label> 
								<div class="">
									<div class="form-control selfBor">`+createTime+`</div>
								</div>
							</div>																																
						</div>
					</div>`;
					$("#MaintCoordRecordCon").append(AppendStr);
					order_list.showImg('wxfaCList');
				});
				$("#MaintCoordRecordCon").show();//维修协调记录
			});
		},
		// 查询维修方案---回复记录---record
		queryCoordinationRecord(idVal,indexs){
			$("#maintPlanCon .chatRecord").eq(indexs).empty();
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryCoordinationRecord, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId :selectId,
				maintainPlanId :idVal
			},function(result){
				let obj=result.data;//方案Arr---
				let AppendStr='';
				$.each(obj,function(index,value){
					AppendStr+=`<li><em>【`+value.commRole+`】</em>`+value.commUserName+`(`+value.createTime+`):`+
						value.commContent+`</li>`;
				});
				if(obj.length>0){
					let childObj=obj[obj.length-1];
					let saveInfo='!replyRole!:!'+ childObj.commRole+'!,!replyUid!:!'+childObj.commRoleId+'!,!replyUserName!:!'+childObj.commUserName+"!";
					$("#maintPlanCon .chatRecord").eq(indexs).attr('name',saveInfo);
				}
				AppendStr='<ul id="chatRecord">'+AppendStr+'</ul>';
				$("#maintPlanCon .chatRecord").eq(indexs).append(AppendStr);
			});
		},
		//查询时间段
		queryOrderTimeRange(num){
			win.utils.ajaxPost(win.utils.services.configMan_reservationTime_list, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
			},function(result){
				let obj=result.configReservationTimes;
				let str="";
				$.each(obj,function(index,value){
					str+="<option value='"+value.id+"'>"+value.starttime+"-"+value.endtime+"</option>"
				});
				if(num==1){
					$("#appointmentTimeRel").empty().append(str);	//创建报修单
				}else{
					$("#gaiqiTime").empty().append(str);	//改期
					$("#gaiqiTimeCon").empty().append(str);	//改期
				};
			});
		},
		//查询维修员-转派师傅
		queryRepairManZp(){
			if(workIdZpFlag==false){
				return;
			};
			workIdZpFlag=false;
			win.utils.ajaxPost(win.utils.services.getTransferProxy, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				workerId: workIdZp,
				// workerName: $("#firstWorkName").html(),
				isInservice :1,
				workerStatus:1
			}, function(result) {
				workIdZpFlag=true;
				if(result.result_code == 200) {
					let workerObj=result.infoVOList;
					let str="<option value=''>请选择转派维修员</option>";
					$.each(workerObj,function(index,value){
						str+="<option name='"+value.workerPhone +"' value='"+value.workerId+"'>"+value.workerName+"("+value.workerPhone+")"+"</option>"
					});
					$("#workerIdZp").empty().append(str);
					order_list.refreshRepairman(2);
				} else {
					utils.showLayerMessage('获取失败，请稍后重试！');
				}
			});
		},
		//一键转办---查询维修方案分类
		queryMainClassify(val){
			$("#createChildOrder input").prop({'checked':false});
			matinSchemeArr=[];
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				// isExistSub:1,//存在子工单 1：存在  2：不存在
				// isCreateSub:1//创建子工单  1：创建 2：不创建
			};
			if(val){
				datas.name = val;
				$("#createChildOrder input").prop({'checked':false});
				$("#createChildOrder").hide();
				$("#shenpiCon .createChildOrder").hide();
			}else{
				datas.isCreateSub=1;//是否创建子工单(1-创建，2-不创建) ,
				datas.status=1;//1启用
			};
			win.utils.ajaxPost(win.utils.services.configMan_maintenSchClasify_list, datas, function(result) {
				isCreateSub=false;
				if(result.result_code == 200) {
					if (val) {
						//审批调用
						if(result.servicePlans.length>0){
							let obj=result.servicePlans[0];
							if(obj.isCreateSub && obj.isCreateSub==1){//
								isCreateSub=true;//允许
								$("#shenpiPlanName").html(val);
								let firstClassifyText=obj.subTaskCategoryOneName?obj.subTaskCategoryOneName:'';
								let secondClassifyText=obj.subTaskCategoryTwoName?obj.subTaskCategoryTwoName:'';
								let subTaskName=obj.subTaskName?obj.subTaskName:'';
								$("#shenpiPlanFirstName").html(firstClassifyText);
								$("#shenpiPlanSecondName").html(secondClassifyText);
								$("#shenpiPlanType").html(subTaskName);
							}else{
								$("#shenpiPlanName").html('');
								$("#shenpiPlanFirstName").html('');
								$("#shenpiPlanSecondName").html('');
								$("#shenpiPlanType").html('');
								isCreateSub=false;
							};
						};
					} else {
						//一键转办
						matinSchemeArr = result.servicePlans;
						let workerObj = result.servicePlans;
						let firstClassifyText='';
						let secondClassifyText='';
						let subTaskName='';
						let str = "<option value=''>请选择协调原因对应的名称</option>";
						let flag=false;
						$.each(workerObj, function (index, value) {
							if(Number(servicePlanId)== Number(value.id)){
								firstClassifyText=value.subTaskCategoryOneName;
								secondClassifyText=value.subTaskCategoryTwoName;
								subTaskName=value.subTaskName;
								flag=true;
							};
							str += "<option name='" + value.id + "' value='" + value.id + "'>" + value.name + "</option>"
						});
						$("#yjzbName").empty().append(str);
						$("#yjzbChildFirstClass").html(firstClassifyText);
						$("#yjzbChildSecondClass").html(secondClassifyText);
						$("#yjzbChildOype").html(subTaskName);
						if(flag==false){
							servicePlanId='';//协调分类id,
							servicePlanName='';//协调分类text
						};
						order_list.refreshRepairman(7);
					}
				} else {
					utils.showLayerMessage('获取失败，请稍后重试！');
				}
			});
		},
		//查询最新维修方案---order_list
		queryNewestMPlan(type){
			//1：审核时查询   2：一键转办查询  3：创建维修方案时查询
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId :selectId
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryNewestMPlan, datas, function(result) {
				if(result.result_code == 200) {
					if(type ==3){
						order_list.setMaintainPlan(result.data);
					}else{
						let obj=result.data;
						//蛋壳id---dankeOrderId
						let projectDesc =obj.projectDesc?obj.projectDesc:'';//维修方案
						let faultCause =obj.faultCause?obj.faultCause:'';//故障原因
						yjzbPlanId=obj.id;//维修方案id
						servicePlanId=obj.servicePlanId;//协调分类id,
						servicePlanName=obj.servicePlanName;//协调分类text
						let newPlanStr='蛋壳工单id:'+dankeOrderId+yjzbDescFault+'；故障原因：'+faultCause+'；维修方案：'+projectDesc;//最新工单内容
						if(type==1){
							$("#createChildOrder").show();
							$("#shenpiChildNote").val(newPlanStr);
							$("#shenpiCon .inputNums>em").eq(1).text(newPlanStr.length);
							$("#shenpiCon .createChildOrder").show();
						}else{
							$("#yjzbChildONote").val(newPlanStr);
							$("#yjzbCon .inputNums>em").text(newPlanStr.length);
							order_list.queryMainClassify();//查询维修协调分类
						};
					}
				} else {
					utils.showLayerMessage('获取最新维修方案失败，请稍后重试！');
				}
			});
		},
		//查询维修员-分配师傅时
		queryRepairman(num){
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
			};
			if(num==1){//1---分配师傅查询
				datas.regionId=cityIdVal;//城市ID
				datas.secondTypeId=secondTypeId;//二级维修类型ID
				datas.isInservice =1;
				datas.workerStatus=1;
			}else if(num==2){
				datas.regionId=citySearchVal;
			};
			win.utils.ajaxPost(win.utils.services.getWorker, datas, function(result) {
				if(result.result_code == 200) {
					let workerObj=result.workerInfoVOS;
					let str="<option value=''>选择维修员</option>";
					$.each(workerObj,function(index,value){
						str+="<option name='"+value.workerPhone +"' value='"+value.workerId+"'>"+value.workerName+"("+value.workerPhone+")"+"</option>"
					});
					if(num==1){//---分配师傅1
						$("#workerId").empty().append(str);
					}else{
						$("#relation").empty().append(str);
						$("#relations").empty().append(str);
					};
					order_list.refreshRepairman(num);
				} else {
					utils.showLayerMessage('获取失败，请稍后重试！');
				}
			});
		},
		//区域与商圈回显
		setRegionMapFun(name){
			if(name=='district'){
				let nowVal=$("#district").val();
				districtId=nowVal;
				let strs="<option value=''>商圈</option>";
				if(nowVal==''){
					$.each(businessArr,function(index,value){
						strs+="<option value='"+value.id+"' name='"+value.parentId+"'>"+value.addressName+"</option>"
					});
				}else{
					$.each(businessArr,function(index,value){
						if(nowVal==value.parentId){
							strs+="<option value='"+value.id+"' name='"+value.parentId+"'>"+value.addressName+"</option>"
						}
					});
				};
				$("#busDistrict").empty().append(strs);
				tradingAreaSearch='';//商圈名称 ,
				tradingAreaIdSearch='';//商圈id ,
				blockIdSearch='';//小区id
				order_list.refreshServerData(1);
				clearInterval(setDataSix);
				order_list.getStaNum();
				////////setIntervalRefreshGetData();//定时刷新数据---待分配
				$("#village").empty().append("<option value=''>小区</option>");
				order_list.refreshRepairman(5);
				order_list.refreshRepairman(6);
			}
		},
		// 区域查询-商圈regionMap_query_list
		queryRegionMapFun(citySearchVal){
			win.utils.ajaxPost(win.utils.services.regionMap_query_list, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				cityId:citySearchVal,
				levelFlag:3,//区域标识id（1、城市；2、区域；3、商圈；4、小区） ,
			}, function(result) {
				regionFlag=true;
				if(result.result_code == 200) {
					let data=result.data;
					let district='';//区
					let business='';//商圈
					districtArr=data[2];
					businessArr=data[3];
					let str="<option value=''>区</option>";
					$.each(districtArr,function(index,value){
						str+="<option value='"+value.id+"'>"+value.addressName+"</option>"
					});
					$("#district").empty().append(str);

					let strs="<option value=''>商圈</option>";
					$.each(businessArr,function(index,value){
						strs+="<option value='"+value.id+"' name='"+value.parentId+"'>"+value.addressName+"</option>"
					});
					$("#busDistrict").empty().append(strs);
					order_list.refreshRepairman(5);
				} else {
					utils.showLayerMessage('查询失败，请稍后重试！');
				}
			});
		},
		//区域查询-城市,区(县),商圈,小区,地址
		queryRegionFun(name,idVal){
			if(regionFlag==false){
				return;
			};
			levelFlag='';
			regionFlag=false;
			if(name=='city'){
				workerIdsArr=[];//订单列表-搜索-清空维修员数组
				workerIdsArrs=[];//订单列表-搜索-清空维修员数组
				levelFlag=2;
				$("#district").empty().append("<option value=''>区</option>");
				$("#busDistrict").empty().append("<option value=''>商圈</option>");
				$("#village").empty().append("<option value=''>小区</option>");
				tradingAreaIdSearch='';//商圈id ,
				blockIdSearch='';//小区id ,
				districtId='';//区域id
			};
			if(idVal==1){//1是默认展现，2是更多展开
				citySearchVal=isNaN(parseInt($("#cityCon").val()))?null:parseInt($("#cityCon").val());//城市
				citySearchNameVal=citySearchVal?'':$("#cityCon option:selected").text();
				$("#city").val(citySearchVal);
			}else{//1是默认展现，2是更多展开
				citySearchVal=isNaN(parseInt($("#city").val()))?null:parseInt($("#city").val());//城市
				citySearchNameVal=citySearchVal?'':$("#city option:selected").text();
				$("#cityCon").val(citySearchVal);
			};
			if(name=='city'){
				order_list.getStaNum();
				order_list.getServerData(1);
				order_list.queryRepairman(2);
				order_list.queryRegionMapFun(citySearchVal);
			}else if(name=='busDistrict'){
				win.utils.ajaxPost(win.utils.services.region_query_list, {
					user_id: win.utils.getCookie('user_id'),
					session: win.utils.getCookie('session'),
					cityId:citySearchVal,
					districtId : districtId ,
					businessDistrictId: tradingAreaIdSearch,
					levelFlag:4,//区域标识id（1、城市；2、区域；3、商圈；4、小区） ,
				}, function(result) {
					regionFlag = true;
					if (result.result_code == 200) {
						let cityObj = result.areaVOList;
						let str = "";
						$.each(cityObj, function (index, value) {
							str += "<option value='" + value.id + "'>" + value.addressName + "</option>"
						});
						let emptyXiaoQuStr = "";
						if (cityObj.length > 1) {
							emptyXiaoQuStr = "<option value=''>小区</option>";
						};
						str = emptyXiaoQuStr + str;
						$("#village").empty().append(str);
						order_list.refreshRepairman(6)
					} else {
						utils.showLayerMessage('查询失败，请稍后重试！');
					}
				})
			};
		},
		//公寓地址-input获取焦点----
		queryAdressFocus(){
			order_list.queryApartmentAdress();
		},
		//--公寓地址-查询
		queryApartmentAdress(){
			queryAdressFlag=false;
			if(queryAdressFlag==false){
				return;
			};
			queryAdressFlag=false;
			win.utils.ajaxPost(win.utils.services.region_query_list, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				detail_address:$("#apartAdress").val()
			}, function(result) {
				queryAdressFlag=true;
				if(result.result_code == 200) {
					let cityObj=result.areaVOList;
					let str="";
					$.each(cityObj,function(index,value){
						str+="<option value='"+value.id+"'>"+value.addressName+"</option>"
					});
					$("#detailAddress").empty().append(str);
				} else {
					utils.showLayerMessage('获取失败，请稍后重试！');
				}
			});
		},
		//---创建租务fn---
		queryDankeResult:function(){
			let user_idVal=win.utils.getCookie('user_id');
			let sessionVal=win.utils.getCookie('session');
			let str='?user_id='+user_idVal+'&session='+sessionVal+'&outOrderNo='+Number(dankeOrderId);
			win.utils.ajaxPost(win.utils.services.orderMan_order_orderRookery+str, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				outOrderNo:dankeOrderId,
			}, function(result) {
				timeNow--;
				timeoutFlag = false;
				if(result.result_code == 200) {
					if(result.orderRookery.code==0){
						utils.closeLayerLoading(tancenObj);
						order_clear.clearYjzbFn();//清除一键转办
						clearInterval(timeObj);
						let msgError = result.orderRookery.msg?result.orderRookery.msg:'操作成功！';
						utils.showConfirmWithCabackSong(msgError,closeFlagCon());
					}else if(result.orderRookery.code==1){
						if( timeNow >0){
							timeoutFlag = true;
						}else{
							utils.closeLayerLoading(tancenObj);
							order_clear.clearYjzbFn();//清除一键转办
							clearInterval(timeObj);
							closeFlagCon()
							let msgError = result.orderRookery.msg?result.orderRookery.msg:'目前尚无结果，稍后前往租务工单查看状态！'
							utils.showLayerMessage(msgError,closeFlagCon());
						}
					}else if(result.orderRookery.code==-1){
						utils.closeLayerLoading(tancenObj);
						order_clear.clearYjzbFn();//清除一键转办
						clearInterval(timeObj);
						closeFlagCon()
						let msgError = result.orderRookery.msg?result.orderRookery.msg:'哎呀，失败了，稍后前往租务工单查看状态！'
						utils.showLayerMessage(msgError,closeFlagCon());
					};
				}else{
					utils.closeLayerLoading(tancenObj);
					order_clear.clearYjzbFn();//清除一键转办
					clearInterval(timeObj);
					closeFlagCon();
					let msgError = result.orderRookery.msg?result.orderRookery.msg:'目前尚无结果，稍后前往租务工单查看状态！'
					utils.showLayerMessage(msgError);
				}
			});
		},
		// 获取一级类别名称
		getFirstClassName : function(rank,type,id) {
			let datas= {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				rank: rank,
				status:1
			};
			if(id){
				if(rank==1){

				}else if(rank==2){
					datas.firstClassifyId=id;
				}else if(rank==3){
					datas.secondClassifyId=id;
				}
			};
			win.utils.ajaxPost(win.utils.services.configMan_firstClassWorkOrder_list,datas, function(result) {
				if(result.result_code == 200) {
					var obj=result.serviceItems;
					order_list.setOrderClassName(rank,obj,type);
				} else {
					utils.showMessage('获取数据失败，请重试！');
				}
			});
		},
		//工单类型赋值
		setOrderClassName:function(rank,obj,type){
			// rank,1:一级   2：二级    3：三级
			// obj：data数据
			// type:1：订单搜索，2：创建关联维修单  3:转派供应商  4:转派维修员
			let str="";
			if(rank==1){
				str="<option value=''>一级工单类型</option>";
			}else if(rank==2){
				str="<option value=''>二级工单类型</option>";
			}else if(rank==3){
				str="<option value=''>三级工单类型</option>";
			};
			$.each(obj,function(index,value){
				str+="<option value='"+value.serviceId+"' name='"+value.outServiceId+"'>"+value.name+"</option>"
			});
			if(rank==1){
				$("#firstGrade").empty().append(str);
				$("#secondGrade").empty();
				$("#firstGradeRel").empty().append(str);
				$("#secondGradeRel");
				$("#thirdGradeRel").empty();
				$("#firstOrderName").empty().append(str);
				$("#secondOrderName").empty();
				$("#thirdOrderName").empty();
				$("#firstOrderNameZpwxy").empty().append(str);
				$("#secondOrderNameZpwxy").empty();
				$("#thirdOrderNameZpwxy").empty();
				// if(type==1){}else if(type==2){}else if(type==3){};
			}else if(rank==2){
				if(type==1){
					$("#secondGrade").empty().append(str);
				}else if(type==2){
					$("#secondGradeRel").empty().append(str);
					$("#thirdGradeRel").empty()
				}else if(type==3){
					$("#secondOrderName").empty().append(str);
					$("#thirdOrderName").empty()
				}else if(type==4){
					$("#secondOrderNameZpwxy").empty().append(str);
					$("#thirdOrderNameZpwxy").empty();
				};
			}else if(rank==3){
				if(type==2){
					$("#thirdGradeRel").empty().append(str);
				}else if(type==3){
					$("#thirdOrderName").empty().append(str);
				}else if(type==4){
					$("#thirdOrderNameZpwxy").empty().append(str);
				}
			};
		},
		// 获取维修方案-list
		getServiceScheme : function() {
			win.utils.ajaxPost(win.utils.services.configMan_maintenSchClasify_list, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				status :1
			}, function(result) {
				if(result.result_code == 200) {
					var obj=result.servicePlans;
					let str="<option value=''>请选择协调方案分类</option>";
					$.each(obj,function(index,value){
						str+="<option value='"+value.id+"'>"+value.name+"</option>"
					});
					$("#servicePlan").empty().append(str);
					$("#servicePlanId").empty().append(str);
				} else {
					utils.showMessage('获取数据失败，请重试！');
				}
			});
		},
		//订单状态-判断=>信息展示,=>对应的操作btn展示
		judgeOrderFun(str){
			let detailText='';//详情展示
			let btnText="";//操作展示
			// 工单-相关详情-id名称  AllBtnAction
			let aq=`<span  id="workOrderDetail" class="btn btn-info">工单详情</span>`;
			let bq=`<span  id="followUpRecord"  class="btn ">跟进记录</span>`;
			let cq=`<span  id="contactResult"  class="btn ">改期记录</span>`;
			let dq=`<span  id="authorizationList"  class="btn ">授权列表</span> `;
			let eq=`<span  id="maintPlan"  class="btn ">维修方案<em id="maintPlanSta"></em></span> `;
			let fq=`<span  id="MaintCoordRecord"  class="btn ">维修协调记录</span> `;
			let gq=`<span  id="MaintettlemeSnt"  class="btn ">维修结算<em id="MaintettlemeSntSta"></em></span>`;
			let hq=`<span  id="repairlist"  class="btn ">报修工单</span> `;
			let iq=`<span  id="rentralist"  class="btn ">租务工单</span> `;
			let jq=`<span  id="quitDetail" class="btn ">退单详情</span>`;
			let kq=`<span  id="backDetail" class="btn ">撤单详情</span>`;
			let lq=`<span  id="configInfo" class="btn ">装修配置信息</span>`;
			// 工单详情-对应 btn操作 id名称   btnACtTrans
			let a=`<button type="button" id="fenpei"  class="btn btn-info">分配维修员</button>`;
			// let b=`<button type="button" id="genjin"  class="btn">跟进</button>`;
			let c=`<button type="button" id="contact"  class="btn ">联系</button>`;
			let d=`<button type="button" id="gaiqi"  class="btn ">改期</button>`;
			let e=`<button type="button" id="shangmen"  class="btn ">上门</button>`;
			// let f=`<button type="button" id="sqmmsq"  class="btn ">申请门密</button>`;
			let f='';
			let g=`<button type="button" id="cjwxfa"  class="btn ">创建维修方案</button>`;
			let h=`<button type="button" id="shjsfa"  class="btn ">审核结算方案</button>`;
			let l=`<button type="button" id="zpwxy"  class="btn ">转派维修员</button>`;
			let lwa=`<button type="button" id="zpfswxy"  class="btn ">转派防水师傅</button>`;
			let m=`<button type="button" id="zpgys"  class="btn ">转派供应商</button>`;
			let n=`<button type="button" id="fqbxd"  class="btn ">创建关联维修单</button>`;
			let o=`<button type="button" id="cjbpgd"  class="btn ">创建补配工单</button>`;
			let p=`<button type="button" id="cjzwgd"  class="btn ">创建租务工单</button>`;//作废
			let q=`<button type="button" id="tuidan"  class="btn ">退单</button>`;
			let r=`<button type="button" id="chedan"  class="btn ">撤单</button>`;
			let u='';//
			if(selectIdSta==6){
				m=`<button type="button" id="zpgys"  disabled="disabled" class="btn ">转派供应商</button>`;
				l=`<button type="button" id="zpwxy"  disabled="disabled" class="btn ">转派维修员</button>`;
				g=`<button type="button" id="cjwxfa" disabled="disabled" class="btn ">创建维修方案</button>`;
				n=`<button type="button" id="fqbxd"  disabled="disabled" class="btn ">创建关联维修单</button>`;
				p=`<button type="button" id="cjzwgd" disabled="disabled" class="btn ">创建租务工单</button>`;//作废
				u=`<button type="button" id="yjzb" disabled="disabled" class="btn ">一键转办</button>`;
			};
			p='';
			//详情展示控制   let detailText='';//详情展示    let btnText="";//操作展示
			if(selectIdSta==2){
				detailText=aq+bq+lq;//工单详情//跟进记录//装修配置信息
				btnText=a+m+q+r;
			}else if(selectIdSta==3){
				detailText=aq+bq+lq;//工单详情//跟进记录//装修配置信息
				btnText=c+l+m+q+r;
			}else if(selectIdSta==4){
				detailText=aq+bq+cq+dq+lq;//工单详情,跟进记录,改期记录,授权列表,装修配置信息
				btnText=d+e+f+l+m+q+r;
			}else if(selectIdSta==5){
				detailText=aq+bq+cq+dq+lq;//工单详情,跟进记录,改期记录,授权列表,装修配置信息
				btnText=f+g+l+m+q+r;
			}else if(selectIdSta==16){
				detailText=aq+bq+cq+dq+iq+lq;//工单详情,跟进记录,改期记录,授权列表,装修配置信息
				btnText='';
			}else if(selectIdSta==13||selectIdSta==15){
				detailText=aq+bq+cq+jq+iq+lq;
			}else if(selectIdSta==12||selectIdSta==14){
				detailText=aq+bq+cq+kq+iq+lq;
			}else if(selectIdSta==6){
				detailText=aq+bq+cq+dq+eq+fq+hq+iq+lq;
				btnText=f+g+l+lwa+m+n+p+u+q+r;
			}else if(selectIdSta==9||selectIdSta==10){
				detailText=aq+bq+cq+dq+eq+fq+gq+hq+iq+lq;
				if(selectIdSta==9){
					btnText=h;
				}
			};
			// detailText+=`<button type="button" class="btn-info" id="refreshBtnDetail">刷新</button>`;
			$("#AllBtnAction").empty().append(detailText);
			$("#btnACtTrans").empty().append(btnText);
		},
		//退单-撤单-操作
		tuidanFn: function(msgSta){
			let staVal=null;//工单状态
			let resVal=null;//原因id
			let resTextVal='';//
			let noteVal=null;//原因备注
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			if(selectIdStaBtn=='tuidan'){
				staVal=13;
				resVal=$("#tdReason").val();
				resTextVal=$("#tdReason option:selected").text();
				noteVal=$("#tdNote").val();
			};
			if(selectIdStaBtn=='chedan'){
				staVal=14;
				resVal=$("#cdReason").val();
				resTextVal=$("#cdReason option:selected").text();
				noteVal=$("#cdNote").val();
			};
			if(resVal==''||resVal=='请选择原因'){
				let notes='';
				if(staVal==14){
					notes='请选择撤单原因';
				}else{
					notes='请选择退单原因';
				};
				utils.showLayerMessage(notes);
				layerFlag=false;
				return;
			};
			if(noteVal==''){
				let note='';
				if(staVal==14){
					note='请填写撤单说明';
				}else{
					note='请填写退单说明';
				};
				utils.showLayerMessage(note);
				layerFlag=false;
				return;
			};

			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,// 工单id ,
				causeDesc: resTextVal,//原因 ,
				outReasonId:resVal,//原因三方id
				explainDesc :noteVal,// 原因描述 ,
				orderStatus:staVal,// 工单的状态(14、撤单、13、退单) ,
				outOrderId:$("#dankeOrderId").text(),// 三方工单(蛋壳) ,
				oldOrderStatus:selectIdSta,//原订单状态
			};
			layerFlag=true;
			win.utils.ajaxPost(win.utils.services.orderMan_order_retreatOrder, datas, function(result) {
				order_clear.clearTuiCheDan();//清除退撤单内容
				layerFlag=false;
				if(result.result_code == 200) {
					let sucNotes='';
					if(staVal==14){
						sucNotes='提交撤单成功';
					}else{
						sucNotes='提交退单成功';
					};
					$("#table").jqGrid('setCell',selectId,"orderStatus",staVal);
					if(staVal==14){
						order_list.getWorkDetail('');
					}else{
						order_list.getWorkDetail('',);
					};
					utils.showConfirmWithCabackSong(sucNotes,closeFlagCon());
				} else {
					let failnotes='';
					if(staVal==14){
						failnotes='撤单失败，请稍后重试！';
					}else{
						failnotes='退单失败，请稍后重试！';
					};
					utils.showLayerMessage(failnotes);
				}
			});
		},
		//改期天数赋值
		setDays:function(str){
			//1:改期  2：联系
			let nowVal=$("#contactOrderDate").html();//日期
			let dateVal=str==1?$("#gaiqiOrderDate").val():$("#gaiqiDateCon").val();//改期日期-联系2
			let nowValstr=new Date(nowVal).getTime();//日期
			let dateValstr=new Date(dateVal).getTime();//改期日期-联系2
			let changeDays=null;//改期天数
			if(dateVal==''){
				$("#gaiqiDays").html("");
				$("#changeDays").html("");
				return;
			}else if(nowValstr>dateValstr){
				$("#gaiqiDays").html("");
				$("#changeDays").html("");
				return;
			}else{
				changeDays=(dateValstr-nowValstr)/(24*60*60*1000);
				if(str==1){
					$("#gaiqiDays").html(changeDays+'天');//改期
				}else{
					$("#changeDays").html(changeDays+'天');//联系
				};
			};
		},
		//联系
		contactFn: function(obj){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			if($("#txtNewDate").val()==''){
				utils.showLayerMessage('请选择联系结果！');
				layerFlag=false;
				return;
			};
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,// 工单id ,
				changeDate:parseInt($("#txtNewDate option:selected").val()),//是否改期-2正常
				changeRemark:$("#contactNote").val(), //备注说明
				initiateRepairName: $("#initiateRepairName").val(),//报修人名称 ,
				initiateRepairTel:$("#gaiqiTel").text(),//报修人电话
				appointmentDate:changeDateCom,//预约日期
				appointmentTimes:changeTimeCom,//预约时间段
			};
			let val=parseInt($("#txtNewDate option:selected").val());
			if(val==1){
				if($("#gaiqiDateCon").val()==''){
					utils.showLayerMessage('请选择改期日期！');
					layerFlag=false;
					return;
				};
				let changeVal=$("#gaiqiTimeCon").val();//改期时间段
				if(changeVal==''){
					utils.showLayerMessage('请选择改期时间段！');
					layerFlag=false;
					return;
				};
				// changeDateCom=orderInfo.appointmentDate;//预约日期
				// changeTimeCom=orderInfo.appointmentTime;//预约时间段
				let orderT=null;//预约时间段---
				if(changeTimeCom.indexOf(9)>-1){
					orderT=1;
				}else if(changeTimeCom.indexOf(22)>-1){
					orderT=3;
				}else{
					orderT=2;
				};
				let odDateT=new Date(changeDateCom+" 00:00:00").getTime();//预约日期时间戳
				let changeDVal=$("#gaiqiDateCon").val()+" 00:00:00";//改期日期
				let changeDValStr=new Date(changeDVal).getTime();//改期日期-时间戳
				let nowObj=new Date();//当前时间
				let months=nowObj.getMonth()+1;//当前月
				let dates=nowObj.getDate();//当前日
				months=months<10?'0'+months:months;
				dates=dates<10?'0'+dates:dates;
				let Hourstr=nowObj.getHours();//当前-hour-
				let nowDate=nowObj.getFullYear()+'-'+months+"-"+dates+" 00:00:00";//当前日期
				let nowDateT=new Date(nowDate).getTime();//当前日期---时间戳
				if(odDateT>nowDateT){//预约大于当前
					if(odDateT>changeDValStr){//预约大于改期
						utils.showLayerMessage('不能改为预约上门日期前的日期！');
						layerFlag=false;
						return;
					}else if(odDateT==changeDValStr){//预约等于改期
						changeVal=changeVal*1;
						if(orderT>=changeVal){
							utils.showLayerMessage('不能改为同一预约时间段或预约时间段之前的时间段！');
							layerFlag=false;
							return;
						}
					}
				}else if(odDateT<nowDateT){//预约小于当前
					if(nowDateT>changeDValStr){//当前大于改期
						utils.showLayerMessage('不能改为当前日期前的日期！');
						layerFlag=false;
						return;
					}else if(nowDateT==changeDValStr){//当前等于改期
						Hourstr=Hourstr*1;
						changeVal=changeVal*1;
						if(Hourstr<=9&&Hourstr>=0){

						}else if(Hourstr>9&&Hourstr<13){
							if(changeVal==1){
								utils.showLayerMessage('不能改为当前时间前(包括当前时间在内)的时间段！');
								layerFlag=false;
								return;
							}
						}else if(Hourstr>=13&&Hourstr<18){
							if(changeVal!=3){
								utils.showLayerMessage('不能改为当前时间前(包括当前时间在内)的时间段！');
								layerFlag=false;
								return;
							}
						}else{
							utils.showLayerMessage('不能改为当前时间前(包括当前时间在内)的时间段！');
							layerFlag=false;
							return;
						};
					}
				}else{//预约等于当前
					if(nowDateT==changeDValStr){//预约等于改期
						Hourstr=Hourstr*1;
						changeVal=changeVal*1;
						if(Hourstr<=9&&Hourstr>=0){
							if(orderT>=changeVal){
								utils.showLayerMessage('不能改为同一预约时间段或预约时间段之前的时间段！');
								layerFlag=false;
								return;
							}
						}else if(Hourstr>9&&Hourstr<13){
							if(changeVal==1){
								utils.showLayerMessage('不能改为当前时间前(包括当前时间在内)的时间段！');
								layerFlag=false;
								return;
							}else{
								if(orderT>=changeVal){
									utils.showLayerMessage('不能改为同一预约时间段或预约时间段之前的时间段！');
									layerFlag=false;
									return;
								}
							}
						}else if(Hourstr>=13&&Hourstr<18){
							if(changeVal!=3){
								utils.showLayerMessage('不能改为当前时间前(包括当前时间在内)的时间段！');
								layerFlag=false;
								return;
							}else{
								if(orderT>=changeVal){
									utils.showLayerMessage('不能改为同一预约时间段或预约时间段之前的时间段！');
									layerFlag=false;
									return;
								}
							}
						}else{
							utils.showLayerMessage('不能改为当前时间前(包括当前时间在内)的时间段！');
							layerFlag=false;
							return;
						};
					}
				};
				if($("#contactReason").val()==''){
					utils.showLayerMessage('请选择改期原因！');
					layerFlag=false;
					return;
				};
				let changeDays=null;//改期天数
				changeDays=(changeDValStr-odDateT)/(24*60*60*1000);
				datas.changeDay=$("#gaiqiDateCon").val();//改期日期 ,
				datas.changeNumber=changeDays;//改期天数 ,
				datas.changeTimes=$("#gaiqiTimeCon option:selected").text() ;//改期时间段 ,
				datas.changeReason=$("#contactReason option:selected").text();//改期原因
			};
			layerFlag=true;
			win.utils.ajaxPost(win.utils.services.orderMan_order_callUser, datas, function(result) {
				if(val==1){
					changeDateCom=$("#gaiqiDateCon").val();//改期日期对比
					changeTimeCom=$("#gaiqiTimeCon option:selected").text();//改期时间对比
					$("#contactOrderDate").html(changeDateCom);//联系-日期-展示
					$("#gaiqiDates").html(changeDateCom);//待上门-日期-展示
				};
				order_clear.clearContact();//清除联系内容
				layerFlag=false;
				if(result.result_code == 200) {
					if(val==1){
						$("#table").jqGrid('setCell',selectId,"isChangeDate",4);
						utils.showConfirmWithCabackSong('改期成功',closeFlagCon());
					}else{
						utils.showConfirmWithCabackSong('联系成功',closeFlagCon());
					};
					selectIdSta=4;
					$("#table").jqGrid('setCell',selectId,"orderStatus",4);
					order_list.getWorkDetail('');
				} else {
					utils.showLayerMessage('操作失败，请稍后重试！');
				}
			});
		},
		//联系btn-是否改期
		contactFinal:function(type){
			if(type==1){//联系btn-是否改期
				let val=parseInt($("#txtNewDate option:selected").val());
				if(val==1){
					$("#contactCon .contactBtnShow").show();
				}else{
					$("#contactCon .contactBtnShow").hide();
				};
			}else if(type==2){//转派供应商---是否紧急
				let val=parseInt($("#exigencyFlagZpgys option:selected").val());
				if(val==2){
					$("#zpgysCon .contactBtnShow").show();
				}else{
					$("#zpgysCon .contactBtnShow").hide();
				};
			}else if(type==3){//转派维修员---维修方案协调中---是否紧急
				let val=parseInt($("#exigencyFlagZpwxy option:selected").val());
				if(val==2){
					$("#zpwxyCon .contactBtnShows").show();
				}else{
					$("#zpwxyCon .contactBtnShows").hide();
				};
			};
		},
		//改期
		gaiqiFn: function(obj){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let dateVal=$("#gaiqiOrderDate").val();//改期日期
			let changeVal=$("#gaiqiTime").val();//改期时间段
			if(dateVal==''){
				utils.showLayerMessage('请选择改期日期！');
				layerFlag=false;
				return;
			};
			if(changeVal==''){
				utils.showLayerMessage('请选择改期时间段！');
				layerFlag=false;
				return;
			};
			// changeDateCom=orderInfo.appointmentDate;//预约日期
			// changeTimeCom=orderInfo.appointmentTime;//预约时间段
			let orderT=null;//预约时间段---
			if(changeTimeCom.indexOf(9)>-1){
				orderT=1;
			}else if(changeTimeCom.indexOf(22)>-1){
				orderT=3;
			}else{
				orderT=2;
			};
			let odDateT=new Date(changeDateCom+" 00:00:00").getTime();//预约日期时间戳
			let changeDVal=dateVal+" 00:00:00";//改期日期---00：00：00
			let changeDValStr=new Date(changeDVal).getTime();//改期日期-时间戳
			let nowObj=new Date();//当前时间
			let months=nowObj.getMonth()+1;//当前月
			let dates=nowObj.getDate();//当前日
			months=months<10?'0'+months:months;
			dates=dates<10?'0'+dates:dates;
			let Hourstr=nowObj.getHours();//当前-hour-
			let nowDate=nowObj.getFullYear()+'-'+months+"-"+dates+" 00:00:00";//当前日期
			let nowDateT=new Date(nowDate).getTime();//当前日期---时间戳
			if(odDateT>nowDateT){//预约大于当前
				if(odDateT>changeDValStr){//预约大于改期
					utils.showLayerMessage('请重新选择预约时间日期！');
					layerFlag=false;
					return;
				}else if(odDateT==changeDValStr){//预约等于改期
					changeVal=changeVal*1;
					if(orderT>=changeVal){
						utils.showLayerMessage('请重新选择改期时间段！');
						layerFlag=false;
						return;
					}
				}
			}else if(odDateT<nowDateT){//预约小于当前
				if(nowDateT>changeDValStr){//当前大于改期
					utils.showLayerMessage('请重新选择改期日期！');
					layerFlag=false;
					return;
				}else if(nowDateT==changeDValStr){//当前等于改期
					Hourstr=Hourstr*1;
					changeVal=changeVal*1;
					if(Hourstr<=9&&Hourstr>=0){

					}else if(Hourstr>9&&Hourstr<13){
						if(changeVal==1){
							utils.showLayerMessage('请重新选择改期时间段！');
							layerFlag=false;
							return;
						}
					}else if(Hourstr>=13&&Hourstr<18){
						if(changeVal!=3){
							utils.showLayerMessage('请重新选择改期时间段！');
							layerFlag=false;
							return;
						}
					}else{
						utils.showLayerMessage('请重新选择改期时间段！');
						layerFlag=false;
						return;
					};
				}
			}else{//预约等于当前
				if(nowDateT==changeDValStr){//预约等于改期
					Hourstr=Hourstr*1;
					changeVal=changeVal*1;
					if(Hourstr<=9&&Hourstr>=0){
						if(orderT>=changeVal){
							utils.showLayerMessage('请重新选择改期时间段！');
							layerFlag=false;
							return;
						}
					}else if(Hourstr>9&&Hourstr<13){
						if(changeVal==1){
							utils.showLayerMessage('请重新选择改期时间段！');
							layerFlag=false;
							return;
						}else{
							if(orderT>=changeVal){
								utils.showLayerMessage('请重新选择改期时间段！');
								layerFlag=false;
								return;
							}
						}
					}else if(Hourstr>=13&&Hourstr<18){
						if(changeVal!=3){
							utils.showLayerMessage('请重新选择改期时间段！');
							layerFlag=false;
							return;
						}else{
							if(orderT>=changeVal){
								utils.showLayerMessage('请重新选择改期时间段！');
								layerFlag=false;
								return;
							}
						}
					}else{
						utils.showLayerMessage('请重新选择改期时间段！');
						layerFlag=false;
						return;
					};
				}
			};
			if($("#gaiqiReason").val()==''){
				utils.showLayerMessage('请选择改期原因！');
				layerFlag=false;
				return;
			};
			let changeDays=null;//改期天数
			changeDays=(changeDValStr-odDateT)/(24*60*60*1000);
			$("#gaiqiDays").val(changeDays+'天');//改期日期
			layerFlag=true;
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,// 工单id ,
				changeNumber :changeDays,//天数
				changeDay:dateVal,//改期
				changeTimes:$("#gaiqiTime option:selected").text(),//时间段
				appointmentDate:changeDateCom,//预约日期
				appointmentTimes:changeTimeCom,//预约时间段
				changeRemark:$("#gaiqiNote").val(), //备注说明
				changeReason :$("#gaiqiReason option:selected").text(),//改期原因
				initiateRepairName: $("#initiateRepairName").val(),//报修人名称 ,
				initiateRepairTel:$("#gaiqiTel").text(),//报修人电话
				changeDate:1
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_changeTime, datas, function(result) {
				layerFlag=false;
				changeDateCom=dateVal;//改期日期对比
				changeTimeCom=$("#gaiqiTime option:selected").text();//改期时间对比
				//改期-show
				$("#gaiqiDates").html(changeDateCom);//改期-日期-展示
				//联系-show
				$("#contactOrderDate").html(changeDateCom);//联系-日期-展示
				order_clear.clearGaiqi();//清除改期内容
				if(result.result_code == 200) {
					selectIdSta=3;
					$("#table").jqGrid('setCell',selectId,"isChangeDate",3);
					utils.showConfirmWithCabackSong('改期成功',closeFlagCon());
				} else {
					utils.showLayerMessage('改期失败，请稍后重试！');
				}
			});
		},
		//分配
		fenpeiFn:function(obj){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let workerTelVal=$("#workerId option:selected").attr('name');
			let workerIdVal=parseInt($("#workerId option:selected").val());
			let workerNameVal=$("#workerId option:selected").text();
			let assignRemarkVal=$("#assignRemark").val();
			if(workerNameVal=='选择维修员'){
				utils.showLayerMessage('请选择维修员！');
				layerFlag=false;
				return;
			}else{
				layerFlag=true;
			};
			let stra='';
			let indexVal=workerNameVal.indexOf('(');
			if(indexVal>-1){
				stra=workerNameVal.slice(0,indexVal);
			}else{
				stra=workerNameVal;
			}
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,//工单id
				assignRemark:assignRemarkVal, //分派维修师傅备注说明
				workerId:workerIdVal, //维修师傅id ,
				workerName :stra,// 分派维修师傅名称 ,
				workerTel : workerTelVal,//维修师傅电话
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_assignWorker, datas, function(result) {
				layerFlag=false;
				order_clear.clearFenpei();//清除分配内容
				if(result.result_code == 200) {
					selectIdSta=3;
					$("#table").jqGrid('setCell',selectId,"orderStatus",3);
					let str="selfSong"+workerNameVal+"<br>"+"<em class='textColor'>"+workerTelVal+"</em>"
					$("#table").jqGrid('setCell',selectId,"workerName",str);
					$("#firstWorkName").html(workerNameVal);
					$("#fpnote").html(assignRemarkVal);
					order_list.getWorkDetail('');
					utils.showConfirmWithCabackSong('分配成功',closeFlagCon());
				} else {
					utils.showLayerMessage('分配失败，请稍后重试！');
				};
			});
		},
		//上门
		shangmenFn:function(){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			layerFlag=true;
			win.utils.ajaxGet(win.utils.services.orderMan_order_signin, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId:selectId,
			}, function(result) {
				layerFlag=false;
				if(result.result_code == 200) {
					selectIdSta=5;
					$("#table").jqGrid('setCell',selectId,"orderStatus",5);
					order_list.getWorkDetail('');
					utils.showConfirmWithCabackSong('上门操作成功',closeFlagCon());
				} else {
					utils.showLayerMessage('操作失败，请稍后重试！');
				}
			});
		},
		//申请门密授权
		sqmmsqFn:function(obj){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let bpDateVal=$("#mmsqDate").val();//预约日期
			let bpDateValTime=new Date(bpDateVal).getTime();//预约日期-时间戳
			if(bpDateVal==''){
				utils.showLayerMessage('请填写预约日期！');
				layerFlag=false;
				return;
			};
			layerFlag=true;
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,// 维修工单id ,
				bookingTime: bpDateVal,//预约日期 ,
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_applyForAuth, datas, function(result) {
				layerFlag=false;
				if(result.result_code == 200) {
					$("#mmsqDate").val('');
					utils.showConfirmWithCabackSong('获取门密成功',closeFlagCon());
				} else {
					utils.showLayerMessage('获取门密失败，请稍后重试！');
				}
			});
		},
		//转派防水师傅
		zpfswxyFn:function(obj){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let workerTelVal=$("#workerIdZp option:selected").attr('name');
			let workerIdVal=$("#workerIdZp option:selected").val();
			let workerNameVal=$("#workerIdZp option:selected").text();
			if(workerIdVal==''){
				utils.showLayerMessage('请选择转派维修员！');
				layerFlag=false;
				return;
			};
			let stra='';
			let indexVal=workerNameVal.indexOf('(');
			if(indexVal>-1){
				stra=workerNameVal.slice(0,indexVal);
			}else{
				stra=workerNameVal;
			};
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,//工单id
				workerId:workerIdVal, //维修师傅id ,
				workerName :stra,// 分派维修师傅名称 ,
				workerTel : workerTelVal,//维修师傅电话
				// orderStatus:selectIdSta//订单状态
			};//6:维修方案协调中
			layerFlag=true;
			win.utils.ajaxPost(win.utils.services.orderMan_order_updateExecutor, datas, function(result) {
				layerFlag=false;
				$("#zpwxySupply").val('');
				order_list.refreshRepairman(2);
				order_clear.clearZpwxy();//清除转派内容
				if(result.result_code == 200) {
					workIdZp=workerIdVal;//防水师傅id
					let str="selfSong"+workerNameVal+"<br>"+"<em class='textColor'>"+workerTelVal+"</em>";
					$("#table").jqGrid('setCell',selectId,"workerName",str);
					$("#firstWorkName").html(stra);//初始分派维修员
					$("#fpnote").html('--');//分派说明
					utils.showConfirmWithCabackSong('转派防水师傅成功',closeFlagCon());
				} else {
					utils.showLayerMessage(result.error_Msg);
				}
			});
		},
		//转派维修员
		zpwxyFn:function(obj){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let workerTelVal=$("#workerIdZp option:selected").attr('name');
			let workerIdVal=$("#workerIdZp option:selected").val();
			let workerNameVal=$("#workerIdZp option:selected").text();
			let assignRemarkVal=$("#assignRemarkZp").val();
			if(workerIdVal==''){
				utils.showLayerMessage('请选择转派维修员！');
				layerFlag=false;
				return;
			};
			if(assignRemarkVal==''){
				utils.showLayerMessage('请填写执行备注！');
				layerFlag=false;
				return;
			};
			let stra='';
			let indexVal=workerNameVal.indexOf('(');
			if(indexVal>-1){
				stra=workerNameVal.slice(0,indexVal);
			}else{
				stra=workerNameVal;
			};
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,//工单id
				assignRemark:assignRemarkVal, //分派维修师傅备注说明
				workerId:workerIdVal, //维修师傅id ,
				workerName :stra,// 分派维修师傅名称 ,
				workerTel : workerTelVal,//维修师傅电话
				orderStatus:selectIdSta//订单状态
			};
			if(selectIdSta==6){
				if($("#firstOrderNameZpwxy").val()==''){
					utils.showLayerMessage('请选择一级工单类型！');
					layerFlag=false;
					return;
				};
				if($("#secondOrderNameZpwxy").val()==''){
					utils.showLayerMessage('请选择二级工单类型！');
					layerFlag=false;
					return;
				};
				if($("#bxAreaZpwxy").val()==''){
					utils.showLayerMessage('请选择报修区域！');
					layerFlag=false;
					return;
				};
				if($("#exigencyFlagZpwxy").val()==''){
					utils.showLayerMessage('请选择是否紧急处理！');
					layerFlag=false;
					return;
				};
				if($("#exigencyFlagZpwxy").val()=='2'){
					if($("#gaiqiDateConZpwxy").val()==''){
						utils.showLayerMessage('请选择预约日期');
						layerFlag=false;
						return;
					};
					if($("#gaiqiTimeConZpwxy").val()==''){
						utils.showLayerMessage('请选择预约时间段');
						layerFlag=false;
						return;
					};
					let changeVal=$("#gaiqiTimeConZpwxy").val();//预约时间段
					changeVal=changeVal*1;
					let dateVal=$("#gaiqiDateConZpwxy").val();//预约日期
					let changeDVal=dateVal+" 00:00:00";//预约日期---00：00：00
					let changeDValStr=new Date(changeDVal).getTime();//预约日期-时间戳
					let nowObj=new Date();//当前时间
					let months=nowObj.getMonth()+1;//当前月
					let dates=nowObj.getDate();//当前日
					months=months<10?'0'+months:months;
					dates=dates<10?'0'+dates:dates;
					let Hourstr=nowObj.getHours();//当前-hour-
					let nowDate=nowObj.getFullYear()+'-'+months+"-"+dates+" 00:00:00";//当前日期
					let nowDateT=new Date(nowDate).getTime();//当前日期---时间戳
					if(nowDateT==changeDValStr){//预约等于当前
						Hourstr=Hourstr*1;
						if(Hourstr<=9&&Hourstr>=0){

						}else if(Hourstr>9&&Hourstr<13){
							if(changeVal==1){
								utils.showLayerMessage('请重新选择预约时间段！');
								layerFlag=false;
								return;
							}
						}else if(Hourstr>=13&&Hourstr<18){
							if(changeVal!=3){
								utils.showLayerMessage('请重新选择预约时间段！');
								layerFlag=false;
								return;
							}
						}else{
							utils.showLayerMessage('请重新选择预约时间段！');
							layerFlag=false;
							return;
						};
					}
					datas.appointDuration=$("#gaiqiTimeConZpwxy option:selected").text() ;//预约时间段：13:00-18:00 ,
					datas.appointDate=$("#gaiqiDateConZpwxy").val();//预约日期：2019-05-30 ,
				};
				if($("#zpwxyTypeS").val()==''){
					utils.showLayerMessage('请选择转派供应商类型！');
					layerFlag=false;
					return;
				};
				if($("#zpwxySupply").val()==''){
					utils.showLayerMessage('请选择转派供应商/装修队！');
					layerFlag=false;
					return;
				};
				if($("#workerIdZp").val()==''){
					utils.showLayerMessage('请选择转派维修员！');
					layerFlag=false;
					return;
				};
				datas.categoryId=$("#secondOrderNameZpwxy option:selected").attr('name') ;//二级分类id ,
				datas.categoryTagId =$("#thirdOrderNameZpwxy option:selected").attr('name');//三级分类id ,
				datas.goodsBillId=$("#goodsInfoZpwxy").val();//商品id ,
				datas.repairArea=$("#bxAreaZpwxy option:selected").text() ;//维修区域:A、厨房1 ,
				datas.supplierId=$("#zpwxySupply option:selected").val() ;//供应商id ,
				datas.supplierName =$("#zpwxySupply option:selected").text() ;//供应商名称 ,
				datas.supplierType=$("#zpwxyTypeS option:selected").text() ;//转派供应商类型 ,
				datas.urgent=$('#exigencyFlagZpwxy').val();//是否紧急工单：是、否 ,
			}
			// 4:待上门  6:维修方案协调中  3：待联系 5:已上门待反馈
			layerFlag=true;
			win.utils.ajaxPost(win.utils.services.orderMan_order_redeployWorker, datas, function(result) {
				layerFlag=false;
				$("#zpwxySupply").val('');
				order_list.refreshRepairman(2);
				order_clear.clearZpwxy();//清除转派内容
				if(result.result_code == 200) {
					if(selectIdSta==6){
						selectIdSta=9;
						$("#table").jqGrid('setCell',selectId,"orderStatus",9);
						order_list.getWorkDetail('');
					}else{
						selectIdSta=3;
						$("#table").jqGrid('setCell',selectId,"orderStatus",3);
						let str="selfSong"+workerNameVal+"<br>"+"<em class='textColor'>"+workerTelVal+"</em>"
						$("#table").jqGrid('setCell',selectId,"workerName",str);
						order_list.getWorkDetail('');
					};
					utils.showConfirmWithCabackSong('转派成功',closeFlagCon());
				} else {
					let errorMsg= result.error_Msg?result.error_Msg:'哎呀，失败了！！！请稍后重试。。。';
					utils.showLayerMessage(errorMsg);
				}
			});
		},
		//跟进
		genjinFn:function(obj){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let gjNoteVal=$("#gjNote").val();//跟进-处理说明
			let txtNewDateGjVal=$("#txtNewDateGj").val();//跟进-下次处理时间
			if(gjNoteVal==''){
				utils.showLayerMessage('请填写跟进备注！');
				layerFlag=false;
				return;
			};
			if(txtNewDateGjVal==''){
				utils.showLayerMessage('请选择下次跟进时间！');
				layerFlag=false;
				return;
			};
			layerFlag=true;
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,//工单id
				// explainImg:imgStr,// 跟进图片 ,
				nextProcessTime:txtNewDateGjVal+" 00:00:00",// 下次处理时间 yyyy-MM-dd HH:mm:ss ,
				orderStatus :selectIdSta,// 工单状态 ,
				processExplain:gjNoteVal,// 处理说明 ,
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_followByOrder, datas, function(result) {
				layerFlag=false;
				if(result.result_code == 200) {
					order_list.getGjRecord();//获取跟进记录请求
				} else {
					utils.showLayerMessage('操作失败，请稍后重试！');
				}
			});
		},
		//创建维修方案
		cjwxfaFn:function(){
			$("#shangmenCost").parent('div').find('em').eq(0).text('');
			$("#jiajiCost").parent('div').find('em').eq(0).text('');
			$("#otherCost").parent('div').find('em').eq(0).text('');
			if(partsTotals*1>=99999){
				layerFlag=false;
				utils.showLayerMessage('您的配件费用不合理，请重新选择配件！');
				return;
			};
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let faultCauseVal=$("#faultCause").val();//故障原因
			let projectDescVal= '';//维修方案-纯文本
			let templateProjectDesc = '';//维修方案带标签-tag
			let templateNameVal=$("#feedBackMaintenPlanId option:selected").text();
			if(templateNameVal.match('请选择维修方案模板')> -1){
				// console.log(mainPlanJsonArr,'000')
				if(mainPlanJsonArr.length<$("#projectDescBl .highLight").length){
					utils.showLayerMessage('请编辑完维修方案的变量！');
					layerFlag=false;
					return;
				}else{
					let newTextValFlag = false;
					let nowindex = [];
					for(let k=0;k<mainPlanJsonArr.length;k++){
						let newTextVal = mainPlanJsonArr[k].newTextVal
						if(newTextVal){
							newTextVal = newTextVal.replace(/【&/g,'').replace(/】/g,'');
						}else{
							newTextVal = ''
						}
						let attrTypeVal = mainPlanJsonArr[k].attrType
						if(newTextVal.length>0 && newTextVal.length<=50){
							// 2表示输入    textTypeVal1:不限，2:仅数字 3：仅文字)
							if(Number(attrTypeVal) ==2){
								let textTypeVal = Number(mainPlanJsonArr[k].textType)
								if(textTypeVal==3){
									let regHasNum = /[0-9]/
									if(regHasNum.test(newTextVal)){
										let nowIndexs = Number(k)+1
										utils.showLayerMessage('维修方案的第'+nowIndexs+'个变量限输入文字！');
										return;
									}
								}else if(textTypeVal==2){
									let  re = /^[0-9]+.?[0-9]*$/;
									if(!re.test(newTextVal)){
										let nowIndexs = Number(k)+1
										utils.showLayerMessage('维修方案的第'+nowIndexs+'个变量限输入数字！');
										return;
									}
								}else{
									// if(!re.test(newTextVal) || !reg.test(newTextVal)){
									// 	let nowIndexs = Number(k)+1
									// 	utils.showLayerMessage('维修方案的第'+nowIndexs+'个变量限输入汉字或数字！');
									// 	return;
									// }
								}
							}
						}else if(newTextVal.length>50){
							let nowIndexs = Number(k)+1
							utils.showLayerMessage('维修方案的第'+nowIndexs+'个变量超过50个字的限制啦！');
							return;
						}else{
							let nowIndexs = Number(k)+1
							utils.showLayerMessage('请编辑维修方案的第'+nowIndexs+'个变量！');
							return;
						}
					}
				}
				// 模板维修方案描述说明 templateProjectDesc
				projectDescVal = $("#projectDescBl").text().replace(/【&/g,'').replace(/】/g,'');
				// 处理掉 带标签 维修方案内容
				let str = '';
				$("#projectDescBl .highLight").each(function(i,v){
					$("#projectDescBl .highLight").eq(i).removeClass('active').parent().removeAttr('name')
				})
				templateProjectDesc = $("#projectDescBl").html()
			}else{
				projectDescVal=$("#projectDesc").val();
				templateProjectDesc=$("#projectDesc").val();
			};
			let servicePlanIdVal=$("#servicePlanId").val();//协调方案分类
			let servicePlanIdTxt=$("#servicePlanId option:selected").text();//协调方案分类-name
			if (parseInt($("#orderStatus").val()) == 6 && isNaN(parseInt(servicePlanIdVal))) {
				utils.showLayerMessage('请选择协调方案分类！');
				layerFlag=false;
				return;
			}
			let secondClassifyVal=$("#MatinplanSecondClassify option:selected").val();//维修二级分类
			let maintainAreaVal=$("#matinArea option:selected").text();//维修区域
			if(!secondClassifyVal||secondClassifyVal==''||secondClassifyVal.indexOf('请选择二级分类')>=0){
				utils.showLayerMessage('请选择维修二级分类！');
				layerFlag=false;
				return;
			};
			if(maintainAreaVal.indexOf('请选择')>=0){
				utils.showLayerMessage('请选择维修区域！');
				layerFlag=false;
				return;
			};
			if(faultCauseVal==''){
				utils.showLayerMessage('请填写故障原因！');
				layerFlag=false;
				return;
			};
			if(projectDescVal==''){
				utils.showLayerMessage('请填写维修方案！');
				layerFlag=false;
				return;
			};
			let optRArr = [];
			let optRidArr = [];
			let optPArr = [];
			let optPidArr = [];
			$.each($("#reasonCon select"),function(i,v){
				let optR = {orderId:orderOrginSelf,type:1}
				optR.reasonAndPlanId = v.value
				optRArr.push(optR)
				if(optRidArr.length<=0){
					optRidArr.push(v.value)
				}else{
					for (let i = 0; i < optRidArr.length; i++) {
						if (optRidArr.indexOf(v.value) === -1) {
							optRidArr.push(v.value)
						}
					}
				}
				// console.log(i,v,v.value)
			})
			$.each($("#planCon select"),function(i,v){
				let optP = {orderId:orderOrginSelf,type:2}
				optP.reasonAndPlanId = v.value
				optPArr.push(optP)
				if(optPidArr.length<=0){
					optPidArr.push(v.value)
				}else{
					for (let i = 0; i < optPidArr.length; i++) {
						if (optPidArr.indexOf(v.value) === -1) {
							optPidArr.push(v.value)
						}
					}
				}
				// console.log(i,v,v.value)
			})
			// console.log(optRArr,'99',optRidArr,'99',optPArr,'99',optPidArr)
			if(judgeReasonArr.length>0&&judgePlanArr.length>0) {
				if (optRArr.length <= 0) {
					utils.showLayerMessage('请选择原因归属！');
					layerFlag = false;
					return;
				}
				if (optPArr.length <= 0) {
					utils.showLayerMessage('请选择方案归属！');
					layerFlag = false;
					return;
				}
				if (optRArr.length != optRidArr.length) {
					utils.showLayerMessage('请不要选择重复的原因归属！');
					layerFlag = false;
					return;
				}
				if (optPArr.length != optPidArr.length) {
					utils.showLayerMessage('请不要选择重复的方案归属！');
					layerFlag = false;
					return;
				}
			}
			// reasonPlanVOS:[{ orderId:orderOrginSelf,type:1,reasonAndPlanId:7},{ orderId:orderOrginSelf,type:1,reasonAndPlanId:8},{ orderId:orderOrginSelf,type:2,reasonAndPlanId:10},{ orderId:orderOrginSelf,type:2,reasonAndPlanId:11}], // 0423增加-1原因归属2方案归属
			if($("#uploadImgBox li").length<=0){
				utils.showLayerMessage('请上传维修图片！');
				layerFlag=false;
				return;
			};
			if(!$("#dutyIdentify input:checked").val()){
				utils.showLayerMessage('请选择责任鉴定！');
				layerFlag=false;
				return;
			};
			// if($("#cjreasonCon").is(':hidden')==false){
			// 	if($("#cjreason").val()==''||$("#cjreason").val()=='请选择原因归属'){
			// 		utils.showLayerMessage('请选择原因归属！');
			// 		layerFlag=false;
			// 		return;
			// 	};
			// };
			let imgStr='';
			$.each($("#uploadImgBox img"),function(index,value){
				let imgKey=$(this).attr('name');
				let indexNow=imgKey.indexOf('bjx-public');
				imgStr+=imgKey.slice(indexNow)+',';
			});
			let otherCostVal=Number($("#otherCost").val()).toFixed(2);//其他费用
			let urgentMoneyVal=Number($("#jiajiCost").val()).toFixed(2);//加急费用
			let callMoneyVal=Number($("#shangmenCost").val()).toFixed(2);//上门费用
			let imgFlag=true;
			$.each(partsCheckedCjSelf,function(index,value){
				if(value.isCustomParts==1){
					if(!value.picUrl||value.picUrl.length<=0){
						imgFlag=false;
						return;
					};
					let arr=value.picUrl.split(',');
					let imgStr=[];
					$.each(arr,function(index,value){
						if(value.length>1){
							let indexVal=value.indexOf('bjx');
							imgStr.push(value.slice(indexVal));
						}
					});
					value.picUrl=imgStr.join(',');
				}
			});
			if(imgFlag==false){
				utils.showLayerMessage('请上传自定义配件图片！');
				return;
			};

			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,// 工单id ,
				dutyIdentify: parseInt($("#dutyIdentify input:checked").val()),//责任鉴定(1-租户责任，2-非租户责任) ,
				faultCause: faultCauseVal,//故障原因 ,
				maintainPhoto : imgStr.slice(0,imgStr.length-1),//维修照片(多张逗号分隔) ,
				orderStatus :parseInt($("#orderStatus").val()),//订单状态（6-需要协调，7-已完成，8-未完成）
				projectDesc:projectDescVal,// 维修方案描述说明 ,
				templateProjectDesc:templateProjectDesc,// 维修方案描述说明 -带标签,
				maintainParts:partsCheckedCjSelf.concat(partsCheckedCjOwnAf),//配件信息列表
				otherCost:otherCostVal*1,//其他费用 ,
				urgentCost :urgentMoneyVal*1,//加急费用
				visitCost :callMoneyVal*1,//上门费用
				partsTotalCost: partsTotals,// 配件费用总计 ,
				totalCost:0,//合计
				// reasonAttribution: $("#cjreason option:selected").text(),//原因归属
				secondClassifyId:secondClassifyVal, // 0423增加-二级维修id
				maintainArea:maintainAreaVal, // 0423增加-维修区域
				// predictFinishTime (string, optional): 预计完成时间 ,
				// PlanPartsDTO {
				// brandName (string, optional): 配件品牌 ,
				// id (integer, optional): 主键 ,
				// isCustomParts (integer, optional): 是否自定义配件(1-是，2-不是) ,
				// partsName (string, optional): 配件名称 ,
				// partsNum (integer, optional): 配件数量 ,
				// partsType (string, optional): 品牌型号 ,
				// picUrl (string, optional): 配件图片(多个图片以逗号隔开) ,
				// price (number, optional): 配件费(采购价格+人工费) ,
				// purchasePrice (number, optional): 采购价 ,
				// remark (string, optional): 备注 ,
				// unitName (string, optional): 配件单位
				// }
			};
			if(judgeReasonArr.length>0&&judgePlanArr.length>0){
				datas.reasonPlanVOS=optRArr.concat(optPArr)  // 0423增加-1原因归属2方案归属 存在则提交
			}
			if(templateNameVal.match('请选择维修方案模板')> -1){
				// templateName: $("#feedBackMaintenPlanId option:selected").text() ,//模板名称
				datas.templateName=templateNameVal;//模板名称
			};
			if(parseInt($("#orderStatus").val())==6){
				datas.servicePlanId=parseInt(servicePlanIdVal);//维修方案id ,
				datas.servicePlanName=servicePlanIdTxt;//协调方案名称 ,
			};
			layerFlag=true;
			win.utils.ajaxPost(win.utils.services.orderMan_order_createMaintainPlan, datas, function(result) {
				layerFlag=false;
				if(result.result_code == 200) {
					if(result.data && result.data.status == 0){
						// 费用类别 1=上门 2=加急 3=其他费用
						if(result.data.cost_type == 1){
							$("#shangmenCost").parent('div').find('em').eq(0).text(result.data.description);
						}else if(result.data.cost_type == 2){
							$("#jiajiCost").parent('div').find('em').eq(0).text(result.data.description);
						}else if(result.data.cost_type == 3){
							$("#otherCost").parent('div').find('em').eq(0).text(result.data.description);
						}
					}else{
						partsTotals=0;
						judgeReasonArr = []; //判断原因归属方案是否配置
						judgePlanArr = []; //判断方案归属方案是否配置
						order_clear.clearCjwxfa();//清除创建维修方案内容
						utils.showConfirmWithCabackSong('创建维修方案成功',closeFlagCon());
						if(parseInt($("#orderStatus").val())==7){
							selectIdSta=9;
							$("#table").jqGrid('setCell',selectId,"orderStatus",9);
							order_list.getWorkDetail('');
						}else{
							selectIdSta=6;
							$("#table").jqGrid('setCell',selectId,"orderStatus",6);
							order_list.getWorkDetail('');
						};
					}
				}else {
					let errorMsg= result.description?result.description:'哎呀，失败了！！！请稍后重试。。。';
					utils.showLayerMessage(errorMsg);
				}
			});
		},
		//创建报修单
		fqbxdFn:function(obj){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let bxPeople=$("#linkPeople").val();//报修人-下拉选择
			let bxPeopleName=$("#initiateRepairNameRel").val();//报修人姓名
			let bxPeopleTel=$("#initiateRepairTelRel").val();//报修人电话
			let idVal=$("#exigencyFlagRel").val();//工单紧急1-普通2
			let DateVal=$("#appointmentDateRel").val();//预约上门日期
			let TimeVal=$("#appointmentTimeRel").val();//预约上门时间段
			if(bxPeople==''){
				utils.showLayerMessage('请选择报修人！');
				layerFlag=false;
				return;
			};
			if(bxPeopleName==''){
				utils.showLayerMessage('请填写报修人姓名！');
				layerFlag=false;
				return;
			};
			if(bxPeopleTel==''){
				utils.showLayerMessage('请填写报修人电话！');
				layerFlag=false;
				return;
			};
			var  re = /^1\d{10}$/;
			if(!re.test(bxPeopleTel)){
				utils.showLayerMessage('请填写正确的报修人电话！');
				layerFlag=false;
				return;
			};
			if(idVal==''){
				utils.showLayerMessage('请选择工单是否紧急！');
				layerFlag=false;
				return;
			};
			if($("#initiateRepairAreaRel").val()==''){
				utils.showLayerMessage('请选择报修区域！');
				layerFlag=false;
				return;
			};
			if(parseInt(idVal)==2){//普通
				if(DateVal==''){
					utils.showLayerMessage('请选择日期！');
					layerFlag=false;
					return;
				};
				if(TimeVal==''){
					utils.showLayerMessage('请选择上门时间段！');
					layerFlag=false;
					return;
				}else{
					let stime=new Date(DateVal+" 00:00:00").getTime();
					let nowObj=new Date();//当前时间
					let months=nowObj.getMonth()+1;//当前月
					let dates=nowObj.getDate();//当前日
					months=months<10?'0'+months:months;
					dates=dates<10?'0'+dates:dates;
					let Hourstr=nowObj.getHours();//当前-hour-
					let nowDate=nowObj.getFullYear()+'-'+months+"-"+dates+" 00:00:00";//当前日期
					let nowTimw=new Date(nowDate).getTime();
					if(stime==nowTimw){
						TimeVal=TimeVal*1;
						if(Hourstr<=9&&Hourstr>=0){

						}else if(Hourstr>9&&Hourstr<13){
							if(TimeVal==1){
								utils.showLayerMessage('请重新选择预约时间段！');
								layerFlag=false;
								return;
							}
						}else if(Hourstr>=13&&Hourstr<18){
							if(TimeVal!=3){
								utils.showLayerMessage('请重新选择预约时间段！');
								layerFlag=false;
								return;
							}
						}else{
							utils.showLayerMessage('请重新选择预约时间段！');
							layerFlag=false;
							return;
						};
					};
				};
			};
			let oneIdVal=parseInt($("#firstGradeRel option:selected").attr('name'));//一级id
			let oneNameVal=$("#firstGradeRel option:selected").text();//一级name
			let twoIdVal=parseInt($("#secondGradeRel option:selected").attr('name'));//二级id
			let twoNameVal=$("#secondGradeRel option:selected").text();//二级name
			let threeIdVal=parseInt($("#thirdGradeRel option:selected").attr('name'));//三级id
			let threeNameVal=$("#thirdGradeRel option:selected").text();//三级name
			if(isNaN(oneIdVal)){
				utils.showLayerMessage('请选择一级工单类型！');
				layerFlag=false;
				return;
			};
			if(isNaN(twoIdVal)){
				utils.showLayerMessage('请选择二级工单类型！');
				layerFlag=false;
				return;
			};
			// if(isNaN(threeIdVal)){
			// 	utils.showLayerMessage('请选择三级工单类型！');
			// 	layerFlag=false;
			// 	return;
			// };
			let imgStr='';
			$.each($("#uploadImgBoxFq img"),function(index,value){
				let imgKey=$("#uploadImgBoxFq img").eq(index).attr('name');
				let indexNow=imgKey.indexOf('bjx-public');
				imgStr+=imgKey.slice(indexNow)+',';
			});
			if($("#faultDescriptionCuseCreate").val()==''){
				utils.showLayerMessage('请填写客服描述！');
				layerFlag=false;
				return;
			};
			layerFlag=true;
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,// 原维修工单id ,
				faultDescriptionCuse:$("#faultDescriptionCuseCreate").val() ,//故障描述-客服 ,
				customerImgUrls :imgStr.slice(0,imgStr.length-1), //现场照片 ,
				exigencyFlag:idVal ,//紧急标识，紧急-1、普通-2 ,
				firstGrade:oneIdVal ,//一级工单类型 ,
				firstGradeName: oneNameVal,//一级工单名称 ,
				goodsInfo:$("#goodsINfoRel").val(),// 商品信息 ,
				initiateRepairArea: $("#initiateRepairAreaRel option:selected").text(),//报修区域 ,
				customerId:bxPeople,
				initiateRepairName: $("#initiateRepairNameRel").val(),//报修人姓名 ,
				initiateRepairTel: $("#initiateRepairTelRel").val(),//报修人联系电话 ,
				needMaintainLock:$("#needMaintainLockRel option:selected").val() ,//需要维修的门锁 (string, optional),
				secondGrade: twoIdVal,//二级工单类型 ,
				secondGradeName: twoNameVal,//二级工单名称 ,
				thirdGrade:threeIdVal,// 三级工单类型 ,
				thirdGradeName: threeNameVal,//三级工单名称 ,
			};
			if(parseInt(idVal)==2){//普通
				datas.appointmentDate=DateVal ;//预约上门日期 yyyy-MM-dd ,
				datas.appointmentTime=$("#appointmentTimeRel option:selected").text() ;//预约时间段 ,
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_createLinkOrder, datas, function(result) {
				order_clear.clearFqbxd();//清除报修工单内容
				layerFlag=false;
				if(result.result_code == 200) {
					order_list.getWorkDetail('');
					utils.showConfirmWithCabackSong('创建关联维修单成功',closeFlagCon())
				} else if(result.result_code == 919){
					utils.showLayerMessage(result.error_Msg);
				}
			});
		},
		// 创建租务工单
		cjzwgdFn:function(obj){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let userSourceZwVal=$("#userSourceZw").val();//用户来源
			let awPeopleEditVal=$("#awPeopleEdit").val();//联系人姓名
			let awPeopleTelVal=$("#awPeopleTel").val();//联系人电话
			let temaContentZwVal=$("#temaContentZw").val();//工单内容
			let idVal=$("#awPeople").val();//联系人
			if(idVal==''){
				utils.showLayerMessage('请选择联系人！');
				layerFlag=false;
				return;
			};
			if(awPeopleEditVal==''){
				utils.showLayerMessage('请填写联系人姓名！');
				layerFlag=false;
				return;
			};
			if(awPeopleTelVal==''){
				utils.showLayerMessage('请填写联系人电话！');
				layerFlag=false;
				return;
			};
			var  re = /^1\d{10}$/;
			if(!re.test(awPeopleTelVal)){
				utils.showLayerMessage('请填写正确的联系人电话！');
				layerFlag=false;
				return;
			};
			if($("#firstIdZw").val()==''){
				utils.showLayerMessage('请选择一级分类！');
				layerFlag=false;
				return;
			};
			if($("#secondIdZw").val()==''){
				utils.showLayerMessage('请填写二级分类！');
				layerFlag=false;
				return;
			};
			if(temaContentZwVal==''){
				utils.showLayerMessage('请填写工单内容！');
				layerFlag=false;
				return;
			};
			let imgStr='';
			$.each($("#uploadImgBoxZw img"),function(index,value){
				let imgKey=$("#uploadImgBoxZw img").eq(index).attr('name');
				let indexNow=imgKey.indexOf('bjx-public');
				imgStr+=imgKey.slice(indexNow)+',';
			});
			layerFlag=true;
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,// 维修工单id ,
				// createChannel: ,//创建渠道 ,
				detailAddress: $("#zuAdress").html(),//详细地址 ,
				exigencyFlag: parseInt($("#exigencyFlagZw").val()),//紧急标识，紧急-1、普通-2 ,
				firstClassifyId:parseInt($("#firstIdZw").val()) ,//一级分类id ,
				firstClassifyName:$("#firstIdZw option:selected").text() ,//一级分类名字 ,
				// outSource:$("#userSourceZw option:selected").text(),// 三方来源(默认蛋壳) ,
				outTemaId:orderIdOther ,//三方工单id ,
				// policeFlagId (integer, optional): 报警标签id ,
				policeFlagName:$("#policeTag option:selected").text(),//报警标签名字
				remarkImg:imgStr.slice(0,imgStr.length-1),// 备注图片 ,
				secondClassifyId:parseInt($("#secondIdZw").val()),//二级分类id ,
				secondClassifyName:$("#secondIdZw option:selected").text() ,//二级分类名字 ,
				// suggestPlan:$("#suggestPlanZw").val(),// 建议方案 ,
				// system: parseInt($("#systemZw").val()),//系统(默认维修) ,
				temaContent:temaContentZwVal ,//工单内容 ,
				temaType:2 ,//2、默认售后 ,
				customerId:$("#awPeople").val(),
				tenementName:awPeopleEditVal ,//联系人 ,
				tenementPhone:awPeopleTelVal ,//联系人电话 ,
				userSource: parseInt(userSourceZwVal),//用户来源 ,
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_createRentOrder, datas, function(result) {
				layerFlag=false;
				order_clear.clearCjzwgd();//清除创建租务工单
				if(result.result_code == 200) {
					$("#cjzwgd").hide();
					$("#rentralist").show();
					utils.showConfirmWithCabackSong('创建租务工单成功',closeFlagCon());
				} else if(result.result_code == 919){
					utils.showLayerMessage(result.error_Msg);
				}
			});
		},
		//转派供应商
		zpgysFn:function(obj){
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let zpNoteVal=$("#zpNote").val();//转派说明
			if($("#firstOrderName").val()==''){
				utils.showLayerMessage('请选择一级工单类型！');
				layerFlag=false;
				return;
			};
			if($("#secondOrderName").val()==''){
				utils.showLayerMessage('请选择二级工单类型');
				layerFlag=false;
				return;
			};
			if($("#exigencyFlagZpgys").val()==''){
				utils.showLayerMessage('请选择是否紧急');
				layerFlag=false;
				return;
			};
			if($("#exigencyFlagZpgys").val()=='2'){
				if($("#gaiqiDateConZp").val()==''){
					utils.showLayerMessage('请选择预约日期');
					layerFlag=false;
					return;
				};
				if($("#gaiqiTimeConZp").val()==''){
					utils.showLayerMessage('请选择预约时间段');
					layerFlag=false;
					return;
				};
				let changeVal=$("#gaiqiTimeConZp").val();//预约时间段
				changeVal=changeVal*1;
				let dateVal=$("#gaiqiDateConZp").val();//预约日期
				let changeDVal=dateVal+" 00:00:00";//预约日期---00：00：00
				let changeDValStr=new Date(changeDVal).getTime();//预约日期-时间戳
				let nowObj=new Date();//当前时间
				let months=nowObj.getMonth()+1;//当前月
				let dates=nowObj.getDate();//当前日
				months=months<10?'0'+months:months;
				dates=dates<10?'0'+dates:dates;
				let Hourstr=nowObj.getHours();//当前-hour-
				let nowDate=nowObj.getFullYear()+'-'+months+"-"+dates+" 00:00:00";//当前日期
				let nowDateT=new Date(nowDate).getTime();//当前日期---时间戳
				if(nowDateT==changeDValStr){//预约等于当前
					Hourstr=Hourstr*1;
					if(Hourstr<=9&&Hourstr>=0){

					}else if(Hourstr>9&&Hourstr<13){
						if(changeVal==1){
							utils.showLayerMessage('请重新选择改期时间段！');
							layerFlag=false;
							return;
						}
					}else if(Hourstr>=13&&Hourstr<18){
						if(changeVal!=3){
							utils.showLayerMessage('请重新选择改期时间段！');
							layerFlag=false;
							return;
						}
					}else{
						utils.showLayerMessage('请重新选择改期时间段！');
						layerFlag=false;
						return;
					};
				}

			};
			if($("#bxArea").val()==''){
				utils.showLayerMessage('请选择报修区域');
				layerFlag=false;
				return;
			};
			if($("#bxmskfZp").val()==''){
				utils.showLayerMessage('请填写报修描述');
				layerFlag=false;
				return;
			};
			if($("#zpTypeS option:selected").val()){
				if($("#zpTypeS option:selected").val()==''){
					utils.showLayerMessage('请选择转派类型！');
					layerFlag=false;
					return;
				}
			}else{
				utils.showLayerMessage('请选择转派类型！');
				layerFlag=false;
				return;
			};
			if($("#zpSupply option:selected").val()){
				if($("#zpSupply option:selected").val()==''){
					utils.showLayerMessage('请选择供应商/装修队！');
					layerFlag=false;
					return;
				}
			}else{
				utils.showLayerMessage('请选择供应商/装修队！');
				layerFlag=false;
				return;
			};
			layerFlag=true;
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,// 工单id ,
				appointDate: $("#gaiqiDateConZp").val(),//预约日期：2019-05-30 ,
				appointDuration:$("#gaiqiTimeConZp option:selected").text() ,//预约时间段：13:00-18:00 ,
				categoryId: $("#secondOrderName option:selected").attr('name'),//二级分类id ,
				categoryTagId:$("#thirdOrderName option:selected").attr('name') ,//三级分类id ,
				describe:$("#bxmskfZp").val(),//维修描述 ,
				goodsBillId:$("#goodsInfoZpgys").val() ,//商品id
				redeployRemark:$("#zpNote").val(),// 转派说明 ,
				repairArea: $("#bxArea option:selected").text(),//维修区域:A、厨房1 ,
				supplierId: $("#zpSupply option:selected").val(),//供应商id ,
				supplierName: $("#zpSupply option:selected").text(),//供应商名称 ,
				orderStatus :selectIdSta,//工单状态
				supplierType: $("#zpTypeS option:selected").text(),//转派供应商类型 ,
				urgent:parseInt($("#exigencyFlagZpgys").val()) ,//是否紧急工单：是、否 ,
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_forwardSupplier, datas, function(result) {
				layerFlag=false;
				order_clear.clearApgys();//清除转派内容
				if(result.result_code == 200) {
					if(selectIdSta==6){
						selectIdSta=9;
						$("#table").jqGrid('setCell',selectId,"orderStatus",9);
						order_list.getWorkDetail('');
					}else{
						selectIdSta=16;
						$("#table").jqGrid('setCell',selectId,"orderStatus",16);
						order_list.getWorkDetail('');
					};
					$("#zpTypeInfo").html($("#zpTypeS option:selected").text());
					$("#spGysInfo").html($("#zpSupply option:selected").text());
					//协调中-变完成，  其他的--已转派供应商
					utils.showConfirmWithCabackSong('转派供应商成功',closeFlagCon());
				} else {
					utils.showLayerMessage(result.error_Msg);
				}
			});
		},
		// 审核结算方案
		shjsfaFn:function(type){
			if(idPlanJs==''){
				utils.showLayerMessage('请在列表中,点击该订单,获取最新信息！');
				return;
			};
			let secondClassifyVal=$("#MatinplanSecondClassifyJs option:selected").val();//维修二级分类
			// console.log(secondClassifyVal,'结算二级分类id')
			let maintainAreaVal=$("#matinAreaJs option:selected").text();//维修区域
			if(!secondClassifyVal||secondClassifyVal==''||secondClassifyVal.indexOf('请选择二级分类')>=0){
				utils.showLayerMessage('请选择维修二级分类！');
				layerFlag=false;
				return;
			};
			if(maintainAreaVal.indexOf('请选择')>=0){
				utils.showLayerMessage('请选择维修区域！');
				layerFlag=false;
				return;
			};
			let optRArr = [];
			let optRidArr = [];
			let optPArr = [];
			let optPidArr = [];
			$.each($("#reasonConJs select"),function(i,v){
				let optR = {orderId:orderOrginSelf,type:1}
				optR.reasonAndPlanId = v.value
				optR.name = $("#reasonConJs select").eq(i).find("option:selected").text()
				optRArr.push(optR)
				if(optRidArr.length<=0){
					optRidArr.push(v.value)
				}else{
					for (let i = 0; i < optRidArr.length; i++) {
						if (optRidArr.indexOf(v.value) === -1) {
							optRidArr.push(v.value)
						}
					}
				}
				// console.log(i,v,v.value)
			})
			$.each($("#planConJs select"),function(i,v){
				let optP = {orderId:orderOrginSelf,type:2}
				optP.reasonAndPlanId = v.value
				optP.name = $("#planConJs select").eq(i).find("option:selected").text()
				optPArr.push(optP)
				if(optPidArr.length<=0){
					optPidArr.push(v.value)
				}else{
					for (let i = 0; i < optPidArr.length; i++) {
						if (optPidArr.indexOf(v.value) === -1) {
							optPidArr.push(v.value)
						}
					}
				}
				// console.log(i,v,v.value)
			})
			// console.log(optRArr,'99',optRidArr,'99',optPArr,'99',optPidArr)
			// reasonBelongIdArrOrgin
			// planBelongIdArrOrgin
			if (judgeReasonArr.length > 0 && judgePlanArr.length > 0) {
				if (optRArr.length <= 0) {
					utils.showLayerMessage('请选择原因归属！');
					layerFlag = false;
					return;
				}
				if (optPArr.length <= 0) {
					utils.showLayerMessage('请选择方案归属！');
					layerFlag = false;
					return;
				}
				if (optRArr.length != optRidArr.length) {
					utils.showLayerMessage('请不要选择重复的原因归属！');
					layerFlag = false;
					return;
				}
				if (optPArr.length != optPidArr.length) {
					utils.showLayerMessage('请不要选择重复的方案归属！');
					layerFlag = false;
					return;
				}
			}
			// 是否修改原因归属/方案归属(1-有修改,2-无修改) isEdited;
			let isEditedVal = 2;
			if(reasonBelongIdArrOrgin.length!=optRidArr.length||planBelongIdArrOrgin.length!=optPidArr.length){
				isEditedVal = 1
			}else{
				reasonBelongIdArrOrgin = reasonBelongIdArrOrgin.sort();
				optRidArr = optRidArr.sort()
				if(reasonBelongIdArrOrgin[0]!=optRidArr[0]){
					isEditedVal = 1
				};
				planBelongIdArrOrgin = planBelongIdArrOrgin.sort();
				optPidArr = optPidArr.sort()
				if(planBelongIdArrOrgin[0]!=optPidArr[0]){
					isEditedVal = 1
				}
			}
			// console.log(reasonBelongIdArrOrgin,optRidArr,'对比')
			// reasonPlanVOS:[{ orderId:orderOrginSelf,type:1,reasonAndPlanId:7},{ orderId:orderOrginSelf,type:1,reasonAndPlanId:8},{ orderId:orderOrginSelf,type:2,reasonAndPlanId:10},{ orderId:orderOrginSelf,type:2,reasonAndPlanId:11}], // 0423增加-1原因归属2方案归属
			if(!$("input[name='responsity']:checked").val()){
				utils.showLayerMessage('请选择是否为责任鉴定！');
				return;
			};
			if(isShowNintyFlag==true && !$("input[name='isReWorkRecord']:checked").val()){
				utils.showLayerMessage('请选择是否为90天内返修！');
				return;
			};
			if(!$("input[name='isReleasedJs']:checked").val()){
				utils.showLayerMessage('请选择是否为保外维修单！');
				return;
			};
			if(partsTotals*1>=99999){
				layerFlag=false;
				utils.showLayerMessage('您的配件费用不合理，请重新选择配件！');
				return;
			};
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			let imgStr='';
			$.each($("#remarkUrlJs img"),function(index,value){
				let imgKey=$("#remarkUrlJs img").eq(index).attr('name');
				let indexNow=imgKey.indexOf('bjx-public');
				imgStr+=imgKey.slice(indexNow)+',';
			});
			$.each(partsCheckedJsSelf,function(index,value){
				if(value.isCustomParts==1){
					let arr=value.picUrl.split(',');
					let imgStr=[];
					$.each(arr,function(index,value){
						if(value.length>1){
							let indexVal=value.indexOf('bjx');
							imgStr.push(value.slice(indexVal));
						}
					});
					value.picUrl=imgStr.join(',');
				}
			});
			let paidRepairLabelsVal=[];
			$.each($("#recordListCon input:checked"),function(index,value){
				paidRepairLabelsVal.push($("#recordListCon input:checked").eq(index).attr('name'));
			});
			layerFlag=true;
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,// 工单id ,
				accountParts:partsCheckedJsSelf.concat(partsCheckedJsOwnAf),//配件信息列表 ,
				// accountStatus:$("#checkUpgradeJs input:checked").val(),// 结算状态(1、通过；2、不通过；3、待审核) ,
				checkRemark:$("#checkRemarkJs").val() ,//审批备注 ,
				// checkTime (string, optional): 审核时间 ,
				checkUpgrade: $("input[name='checkGradeJs']:checked").val(),//审核升级(1-是，2-否) ,
				// checkUser (string, optional): 审核人 ,
				dutyIdentify:$("input[name='responsity']:checked").val() ,//责任鉴定(1-租户责任，2-非租户责任) ,
				id:idPlanJs,// 结算方案id ,
				isReleased:$("input[name='isReleasedJs']:checked").val()  ,//是否为保外维修单(1-是；2-否) ,
				paidRepairLabels:paidRepairLabelsVal,// 已结算返修标记 ,
				maintainRemark:$("#servicePlanJs").val() ,//维修方案备注 ,
				maintainUrl: imgStr,//维修方案图片url ,
				callMoney:$("#indoorJs").val()==''?0:$("#indoorJs").val()*1,//上门费用 ,
				otherMoney:$("#otherMoneyJs").val()==''?0:$("#otherMoneyJs").val()*1 ,//其他费用 ,
				contactOvertime:$("#linkOverJs").val()*1 ,//联系超时费用 ,
				feedbackOvertime: $("#backOverJs").val()*1,//反馈超时费用 ,
				partsOvertime:$("#partOverJs").val()*1,//配件超时费用 ,
				partsTotalCost: partsTotals*1,//配件费用总计 ,
				totalCost:0,//合计
				urgentMoney: $("#urgentMoneyJs").val()*1,//加急费用 ,
				visitOvertime:$("#doorOverJs").val()*1,//上门超时费用
				secondClassifyId:secondClassifyVal, // 0423增加-二级维修id
				maintainArea:maintainAreaVal, // 0423增加-维修区域
				isEdited:isEditedVal
			};
			if(judgeReasonArr.length>0&&judgePlanArr.length>0){
				datas.reasonPlanVOS=optRArr.concat(optPArr)  // 0423增加-1原因归属2方案归属 存在则提交
			}
			// console.log(datas,'9999999')
			if(isShowNintyFlag==false){
				datas.artNinetyRework=2;//人工判断是否是90天返修工单(1-是；2-不是) ,
			}else{
				let valChecked = $("input[name='isReWorkRecord']:checked").val();
				datas.artNinetyRework=valChecked;//人工判断是否是90天返修工单(1-是；2-不是),
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_updateAccountPlan, datas, function(result) {
				layerFlag=false;
				if(result.result_code == 200) {
					if($("#checkGradeJsA").is(':checked')){//审核升级(1-是，2-否) ,
						utils.showConfirmWithCabackSong('提交成功',closeFlagCon());
					};
					if($("#checkGradeJsB").is(':checked')){
						selectIdSta=10;
						reasonBelongIdArrOrgin = [];//结算待审核，查询结算方案时的原因归属保留的id
						planBelongIdArrOrgin = [];//结算待审核，查询结算方案时的方案归属保留的id
						judgeReasonArr = []; //判断原因归属方案是否配置
						judgePlanArr = []; //判断方案归属方案是否配置
						order_clear.clearShjsfa();
						$("#MaintettlemeSntSta").text('通过').removeClass('a').removeClass('b').addClass('a'); //维修结算（1、通过；2、不通过）---工单详情
						$("#table").jqGrid('setCell',selectId,"orderStatus",10);
						$("#shwxjsBtn").empty().hide();//按钮清除
						$("#btnACtTrans").empty();//按钮清除
						$("#shjsfaCon").hide();
						if(type==1){//工单详情---审核维修结算
							$("#serviceFeeInfo>span").eq(0).removeClass('normalThree').addClass('normalOk').text('通过');
						}else if(type==2){//维修结算--审核维修结算
							idPlanJs='';
							order_list.queryOrderAccount("suc");
						};
						utils.showConfirmWithCabackSong('审核成功',closeFlagCon());
					}
				} else {
					reasonBelongIdArrOrgin = [];//结算待审核，查询结算方案时的原因归属保留的id
					planBelongIdArrOrgin = [];//结算待审核，查询结算方案时的方案归属保留的id
					judgeReasonArr = []; //判断原因归属方案是否配置
					judgePlanArr = []; //判断方案归属方案是否配置
					order_clear.clearShjsfa();
					utils.showLayerMessage('审核失败，请稍后重试！');
				}
			});
		},
		//一键转办
		yjzbFn:function(){
			if($("#yjzbName").val()==''&&$("#cyjzbName option:selected").text().length<=0){
				utils.showLayerMessage('请选择协调原因对应的名称！');
				return;
			};
			if($("#yjzbChildONote").val().length<=0 ){
				utils.showLayerMessage('请输入工单内容！');
				return;
			};
			if($("#yjzbChildONote").val().length>1000 ){
				utils.showLayerMessage('工单内容限制最多输入1000字！');
				return;
			};
			if(layerFlag==true){
				utils.showLayerMessage('请求正在进行中！');
				return;
			};
			layerFlag=true;
			let datas={
				user_id: win.utils.getCookie('user_id'),//用户id
				session: win.utils.getCookie('session'),//session
				orderId:selectId,// 工单id ,
				orderMaintainPlanId:yjzbPlanId,//维修方案id
				servicePlanId:$("#yjzbName").val(),// //协调分类id,---before
				servicePlanName:$("#yjzbName option:selected").text(),// //协调分类text---before
				remark:$("#yjzbChildONote").val(),//一键转办内容
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_createSubOrderTask, datas, function(result) {
				layerFlag=false;
				order_picture.allowScroll();
				if(result.result_code == 200) {
					tancenObj=utils.showLayerLoading('请求正在进行中！');
					fiveSecondsTimeout();
				}else if(result.result_code == 6006){
					utils.showLayerMessage('创建失败，维修方案有变更，请重试！');
				}else if(result.result_code == 6008){
					utils.showLayerMessage('创建失败，维修方案有变更，请重试！');
				} else {
					utils.showLayerMessage('审核失败，请稍后重试！');
				}
			});
		},
		//弹窗赋值
		setPartsSelfVal:function(index,obj){
			if(index<0){//新增
				initializeEditForm();
				$('#uploadImgPart').empty();
				return;
			};//反之编辑
			let imgStrPage='';
			if(obj.picUrl&&obj.picUrl.length>0){
				$.each(obj.picUrl.split(','),function(index,value){
					if(value.length>1){
						if(value.indexOf('http://images.baijiaxiu.com/')<0){
							value='http://images.baijiaxiu.com/'+value;
						};
						imgStrPage+='<li>' +
							'<img class="cardImage mySelfImg" alt="" style="margin-top: 10px; width: 100px; height: 100px; max-width: 400px; border: 10px solid #EEEEEE;" src="' + value + '" name=' + value + '>' +
							'<span class="layui-layer-setwin">×</span>' +
							'</li>'
					}
				});
			}
			$("#partNameAdd").val(obj.partsName);//配件名称
			$("#partBrand").val(obj.brandName);//配件品牌
			$("#brandType").val(obj.partsType);//品牌型号
			$("#partsFee").val(obj.price);//配件费
			$("#partsNums").val(obj.partsNum);//配件数量
			$("#partUnitAdd").val(obj.unitName);//配件单位
			$("#purchasePrice").val(obj.purchasePrice);//采购价
			$("#uploadImgPart").empty().append(imgStrPage);//配件图片
			$("#addSelfPartName").val(obj.remark);//备注说明
		},
		//工单详情-获取具体信息后---页面赋值
		setWorkDetailData:function(id,orderInfo){
			// id-关联工单/本工单,orderInfo-订单信息,selectIdSta-订单状态
			if(!orderInfo||orderInfo==''){
				$("#showBtnConNav").hide();
				return;
			}else{
				$("#showBtnConNav").show();
			}
			let imgStr='';
			if(orderInfo){
				if(orderInfo.customerImgUrls){
					$.each(orderInfo.customerImgUrls.split(','),function(index,value){
						if(value!=''&&value!=','){
							imgStr += `<img alt="" class="mySelfImg" src="../../images/imgLoad.png" name="` + value + `">`;
						}
					});//图片
				};
			};
			imgStr=imgStr==''?'未上传图片':imgStr;
			if(id!=''){
				//工单状态
				let strStaVal='--';
				// 1:待认领  2：待分配  3：待联系  4：待上门   5：已上门 (待反馈)  6：维修方案协调中
				// 7:已完成  8：未完成  9：结算待审核  10：结算审核通过   11：已评价   12：已撤销
				// 13：退单中  14：撤单中 15：已退单  16:已转派供应商
				if(orderInfo.orderStatus==2){
					strStaVal='待分配';
				}else if(orderInfo.orderStatus==3){
					strStaVal='待联系';
				}else if(orderInfo.orderStatus==4){
					strStaVal='待上门';
				}else if(orderInfo.orderStatus==5){
					strStaVal='已上门待反馈';
				}else if(orderInfo.orderStatus==6){
					strStaVal='维修方案协调中';
				}else if(orderInfo.orderStatus==7){
					strStaVal='已完成';
				}else if(orderInfo.orderStatus==8){
					strStaVal='未完成';
				}else if(orderInfo.orderStatus==9){
					strStaVal='结算待审核';
				}else if(orderInfo.orderStatus==10){
					strStaVal='结算审核通过';
				}else if(orderInfo.orderStatus==11){
					strStaVal='已评价';
				}else if(orderInfo.orderStatus==12){
					strStaVal='已撤销';
				}else if(orderInfo.orderStatus==13){
					strStaVal='退单中';
				}else if(orderInfo.orderStatus==14){
					strStaVal='撤单中';
				}else if(orderInfo.orderStatus==15){
					strStaVal='已退单';
				}else if(orderInfo.orderStatus==16){
					strStaVal='已转派供应商';
				}
				$("#bxgsztSta").html(strStaVal);
				$("#orderIdLink").html(orderInfo.orderId);//工单id
				let firstGradeNameVal=orderInfo.firstGradeName?orderInfo.firstGradeName:'暂无';
				let secondGradeNameVal=orderInfo.secondGradeName?orderInfo.secondGradeName:'暂无';
				let thirdGradeNameVal=orderInfo.thirdGradeName?orderInfo.thirdGradeName:'暂无';
				$("#typeLink").html(firstGradeNameVal+'-'+secondGradeNameVal+'-'+thirdGradeNameVal);//工单类型
				$("#initiateRepairNameLink").html(orderInfo.initiateRepairName);//报修人
				$("#initiateRepairTelLink").html(orderInfo.initiateRepairTel);//报修人电话
				$("#detailAddressLink").html(orderInfo.detailAddress);//公寓地址
				$("#initiateRepairAreaLink").html(orderInfo.initiateRepairArea);//报修区域
				$("#needMaintainLockLink").html(orderInfo.needMaintainLock);//维修的门锁====
				$("#goodsInfoLink").html(orderInfo.goodsInfo);//商品===
				let exigencyFlagLink=orderInfo.exigencyFlag==1?'紧急':'普通';
				$("#exigencyFlagLink").html(exigencyFlagLink);
				$("#appointmentDateLink").html(orderInfo.appointmentDate);//预约日期
				$("#appointmentTimeLink").html(orderInfo.appointmentTime);//时间段
				$("#customerImgUrlsLInk").html(imgStr);//照片
				$("#faultDescriptionCuseLink").html(orderInfo.faultDescriptionCuse);//描述
				$("#goodsInfoLink").html(orderInfo.goodsInfo);//商品信息-
				order_list.showImg('glgd');//关联工单
				$("#showBtnConNav").show();
				$("#repairlistCon").show();//报修工单
				return;
			};
			// 工单详情---基本都有
			//订单状态
			selectIdSta=orderInfo.orderStatus;
			//订单状态
			order_list.judgeOrderFun(selectIdSta);
			//table表---修改数据
			newestChecksta=orderInfo.checkStatus;//在查询工单详情里---最新维修方案状态---
			$("#table").jqGrid('setCell',selectId,"orderStatus",selectIdSta);
			// 基本信息
			cityIdVal=!orderInfo.cityId?'':orderInfo.cityId;//城市id  originDankerOrderId
			if(orderInfo.oldOrderId){
				$("#originOrderIdCon").show();//原工单ID
				$("#originOrderId").html(orderInfo.oldOrderId).show();//原工单ID
				$("#originDankerOrderIdCon").show();//原蛋壳工单ID
				$("#originDankerOrderId").html(orderInfo.oldOrderId).show();//原蛋壳工单ID
			}else{
				$("#originOrderId").html('').hide();//原工单ID
				$("#originOrderIdCon").hide();//原工单ID
				$("#originDankerOrderId").html('').hide();//原蛋壳工单ID
				$("#originDankerOrderIdCon").hide();//原蛋壳工单ID
			};
			$("#dankeOrderId").html(orderInfo.outOrderNo);//蛋壳工单id
			dankeOrderId=orderInfo.outOrderNo;
			orderOrginSelf = orderInfo.orderId
			let firstGradeName=orderInfo.firstGradeName?orderInfo.firstGradeName+"--":'-';
			let secondGradeName=orderInfo.secondGradeName?orderInfo.secondGradeName+"--":'-';
			let thirdGradeName=orderInfo.thirdGradeName?orderInfo.thirdGradeName:'';
			let gradeName=firstGradeName+secondGradeName+thirdGradeName;
			$("#gradeName").html(gradeName);//工单类型
			let orderFlagVal='--';
			let sysNinetyReworkVal = '';
			// if(orderInfo.orderFlag){
			// 	let flagVal=orderInfo.orderFlag;
			// 	if(flagVal==1){
			// 		orderFlagVal='报修工单';
			// 	}else if(flagVal==2){
			// 		orderFlagVal='重复工单';
			// 	}else if(flagVal==3){
			// 		orderFlagVal='返工工单';
			// 	}else if(flagVal==4){
			// 		orderFlagVal='逾期工单';
			// 	}else if(flagVal==5){
			// 		orderFlagVal='撤销工单';
			// 	}else if(flagVal==6){
			// 		orderFlagVal='巡检工单';
			// 	};
			// };
			orderFlagVal=orderInfo.orderFlagName?orderInfo.orderFlagName:'--'; //orderFlagName  工单标签名称
			$("#FlagName").html(orderFlagVal);//工单标签
			if(orderInfo.sysNinetyRework&&Number(orderInfo.sysNinetyRework) ==1){
				sysNinetyReworkVal='<span class="snr">90天返修</span>';
			}else{
				sysNinetyReworkVal='--';
			}; //orderFlagName  返工标签名称
			$("#ReturnFlagName").html(sysNinetyReworkVal);//返工标签名称
			let oldCompanyName=orderInfo.oldCompanyName?orderInfo.oldCompanyName:'--';
			$("#oldCompanyName").html(oldCompanyName);//直供供应商
			let initiateRepairArea=orderInfo.initiateRepairArea?orderInfo.initiateRepairArea:'--';
			initiateRepairAreaVal = orderInfo.initiateRepairArea?orderInfo.initiateRepairArea:'';
			$("#initiateRepairAreaInfo").html(initiateRepairArea);//报修区域
			let faultDescriptionCuse=orderInfo.faultDescriptionCuse?orderInfo.faultDescriptionCuse:'--';
			$("#faultDescriptionCuse").html(faultDescriptionCuse);//报修描述-客服
			let faultDescriptionUser=orderInfo.faultDescriptionUser?orderInfo.faultDescriptionUser:'--';
			$("#faultDescriptionUser").html(faultDescriptionUser);//报修描述
			yjzbDescFault='；报修描述-客服：'+faultDescriptionCuse+'；报修描述：'+faultDescriptionUser;//一键转办-报修描述
			let appointmentDate=orderInfo.appointmentDate?orderInfo.appointmentDate+" ":'';
			let appointmentTime=orderInfo.appointmentTime?orderInfo.appointmentTime:'--';
			$("#orderTimeWorkInfo").html(appointmentDate+appointmentTime);//预约上门时间
			$("#customerImgUrlsInfo").html(imgStr);//---租户故障描述图片
			order_list.showImg('order');//本工单
			//联系人信息
			let city=orderInfo.city?orderInfo.city+"-":"";
			let district=orderInfo.district?orderInfo.district+"-":"";
			let tradingArea=orderInfo.tradingArea?orderInfo.tradingArea+"-":"";
			let blockName=orderInfo.blockName?orderInfo.blockName:"";
			let str=city+district+tradingArea+blockName;
			$("#cdbxqu").html(str);//城市-行政区-商圈-小区
			tradingAreaId=orderInfo.tradingAreaId;
			let detailAddress=orderInfo.detailAddress?orderInfo.detailAddress:"--";
			let houseId=orderInfo.houseId?orderInfo.houseId:"--";
			houseIdcreate = orderInfo.houseId //创建维修方案-房间区域id
			secondGradecreate =  orderInfo.secondGrade //创建维修方案-二级分类id
			// firstGradecreate = orderInfo.firstGrade //创建维修方案-一级分类id
			$("#detailAddressInfo").html("#"+houseId+" "+detailAddress);//公寓地址
			let serviceMoneyInfo=orderInfo.serviceMoney==1?"有":"无";
			$("#serviceMoneyInfo").html(serviceMoneyInfo);//维修金
			let initiateRepairName=orderInfo.initiateRepairName?orderInfo.initiateRepairName:"--";
			$("#initiateRepairNameInfo").html(initiateRepairName);//报修人姓名
			let initiateRepairTel=orderInfo.initiateRepairTel?orderInfo.initiateRepairTel:"--";
			$("#initiateRepairTelInfo").html(initiateRepairTel);//报修人电话
			//完成信息
			let nextProcessTime=!orderInfo.nextProcessTime||orderInfo.nextProcessTime==''?'--':orderInfo.nextProcessTime;
			$("#nextProcessTime").html(nextProcessTime);
			let isChangeDate='';
			if(orderInfo.isChangeDate==1){
				isChangeDate='<span class="normalTwo">改期上门</span>'
			}else if(orderInfo.isChangeDate==2){
				isChangeDate='<span class="normalOk">正常上门</span>'
			}else{
				isChangeDate='--'
			};// 改期-1，正常-2
			$("#isChangeDateInfo").html(isChangeDate); //联系结果
			// 维修方案协调中审核状态(1、方案通过，2、方案不通过，3、待审批) ,
			let checkStatus='';
			let checkStatuss='';
			let checkStatussClass='';
			if(orderInfo.checkStatus==1){
				$("#zpgys").attr("disabled",false);
				$("#zpwxy").attr("disabled",false);
				$("#zpfswxy").attr("disabled",false);
				$("#cjwxfa").attr("disabled",false);
				$("#yjzb").attr("disabled",false);
				$("#fqbxd").attr("disabled",false);
				checkStatuss='通过';
				checkStatussClass='a';
				checkStatus='<span class="normalOk">通过</span>'
			}else if(orderInfo.checkStatus==2){
				$("#zpgys").attr("disabled",true);
				$("#zpwxy").attr("disabled",true);
				$("#zpfswxy").attr("disabled",true);
				$("#cjwxfa").attr("disabled",false);
				$("#yjzb").attr("disabled",true);
				$("#fqbxd").attr("disabled",true);
				checkStatuss='驳回';
				checkStatussClass='b';
				checkStatus='<span class="normalTwo">驳回</span>'
			}else if(orderInfo.checkStatus==3){
				$("#zpgys").attr("disabled",true);
				$("#zpwxy").attr("disabled",true);
				$("#zpfswxy").attr("disabled",true);
				$("#cjwxfa").attr("disabled",true);
				$("#yjzb").attr("disabled",true);
				$("#fqbxd").attr("disabled",true);
				checkStatussClass='c';
				checkStatuss='待审核';
				if(selectIdSta==6){
					checkStatus='<span class="normalThree">待审核</span>'
				}else{
					checkStatus='--';
				};
			}else{
				checkStatuss='';
				checkStatussClass='';
				checkStatus='';
				$("#zpfswxy").attr("disabled",false);
				$("#zpgys").attr("disabled",false);
				$("#zpwxy").attr("disabled",false);
				$("#cjwxfa").attr("disabled",false);
				$("#zpfswxy").attr("disabled",false);
				$("#fqbxd").attr("disabled",false);
			}
			$("#servicePlanInfo").html(checkStatus); //维修方案	1、方案通过，2、方案不通过，3、方案待审核
			$("#maintPlanSta").text(checkStatuss).removeClass('a').removeClass('b').addClass(checkStatussClass); //维修方案	1、方案通过，2、方案不通过，3、方案待审核---工单详情展示
			let accountStatus='';
			let accountStatuss='';
			let accountStatusClass='';
			if(selectIdSta==9){
				if(orderInfo.accountStatus==2){
					accountStatuss='驳回';
					accountStatusClass='b';
					accountStatus='<span class="normalTwo">驳回</span>'
				}else{
					accountStatuss='待审核';
					accountStatusClass='c';
					accountStatus='<span class="normalThree">待审核</span>'
				}
			}else if(selectIdSta==10){
				accountStatuss='通过';
				accountStatusClass='a';
				accountStatus='<span class="normalOk">通过</span>'
			}else{
				accountStatuss='';
				accountStatusClass='';
				accountStatus=''
			};
			$("#serviceFeeInfo").html(accountStatus); //维修结算（1、通过；2、不通过）
			$("#MaintettlemeSntSta").text(accountStatuss).removeClass('a').removeClass('b').addClass(accountStatusClass); //维修结算（1、通过；2、不通过）---工单详情
			let practicalFinishTime=!orderInfo.practicalFinishTime||orderInfo.practicalFinishTime==''?'--':orderInfo.practicalFinishTime;
			$("#practicalFinishTimeInfo").html(practicalFinishTime); //实际完成时间
			//分派信息
			let workerName=orderInfo.workerName?orderInfo.workerName:"--";
			$("#firstWorkName").html(workerName);//初始分派维修员
			let assignRemark=orderInfo.assignRemark?orderInfo.assignRemark:"--";
			$("#fpnote").html(assignRemark);//分派说明
			let supplierType=orderInfo.supplierType?orderInfo.supplierType:"--";
			$("#zpTypeInfo").html(supplierType);//转派类型
			let supplierName=orderInfo.supplierName?orderInfo.supplierName:"--";
			$("#spGysInfo").html(supplierName);//转派供应商
			let redeployTime=orderInfo.redeployTime?orderInfo.redeployTime:"--";
			$("#zpTimeInfo").html(redeployTime);//转派时间
			//其他信息
			let createSourceVal=orderInfo.createSource==1?'租户':'蛋壳';// createSource: 1(租户-1，蛋壳-2)
			$("#createSourceInfo").html(createSourceVal);//发起来源
			let createUser=orderInfo.createUser?orderInfo.createUser:'--';
			$("#createUserInfo").html(createUser);//创建人
			let customerCreateTime=orderInfo.customerCreateTime?orderInfo.customerCreateTime:'--';
			$("#createTimeInfo").html(customerCreateTime);//创建时间
			//联系-show
			$("#contactTel").html(orderInfo.initiateRepairTel);//报修人电话
			//改期
			$("#gaiqiTel").html(orderInfo.initiateRepairTel);//报修人电话
			// 根据订单状态进行赋值
			if(selectIdSta==2||selectIdSta==3||selectIdSta==4||selectIdSta==5||selectIdSta==6){
				// 转派供应商
				$("#firstOrderName").html(orderInfo.firstGradeName);//转派供应商-一级工单名称
				$("#secondOrderName").html(orderInfo.secondGradeName);//转派供应商-二级工单名称
				$("#thirdOrderName").html(orderInfo.thirdGradeName);//转派供应商-三级工单名称
				$("#bxArea").html(orderInfo.initiateRepairArea);//转派供应商-报修区域
				faultDescriptionCuseVal=orderInfo.faultDescriptionCuse?orderInfo.faultDescriptionCuse:'';
				$("#bxmskfZp").val(faultDescriptionCuseVal);//转派供应商-客服描述
				let nums=faultDescriptionCuseVal==''?0:faultDescriptionCuseVal.length;
				$("#bxmskfZp").parent('div').find('em').eq(0).text(nums);
				faultDescriptionUserVal=orderInfo.faultDescriptionUser?orderInfo.faultDescriptionUser:'--';
				$("#bxmsZp").html(faultDescriptionUserVal);//转派供应商-描述
// 				let exigencyFlag=orderInfo.exigencyFlag==1?'紧急工单':'普通工单';// ，紧急-1、普通-2
				needMaintainLockVal=orderInfo.needMaintainLock?orderInfo.needMaintainLock:'--';
				$("#needMaintainLockZpgys").html(orderInfo.needMaintainLockVal);//需要维修的门锁
				let goodStr="<option value=''>无</option>";
				if(orderInfo.apartmentGoods&&orderInfo.apartmentGoods.length>0){
					$.each($.parseJSON(orderInfo.apartmentGoods),function(index,value){
						goodStr+="<option value='"+value.id+"'>"+value.name+"</option>";
					});
				};
				$("#goodsINfoRel").empty().append(goodStr);// goodsINfoRel 创建关联维修单
				$("#goodsInfoZpgys").empty().append(goodStr);// goodsInfoZpgys  转派供应商
				$("#goodsInfoZpwxy").empty().append(goodStr);// goodsInfoZpgys  转派维修员
				$("#goodsInfoLink").html(orderInfo.goodsInfo);//商品信息-报修工单
			};
			// firstGrade: 1512
			if(selectIdSta!=2||selectIdSta!=3){
				//改期记录
				$("#gqBxrTel").html(orderInfo.initiateRepairTel);//报修人电话-改期记录
			}
			//转派师傅
			workIdZp=orderInfo.workerId;//转派师傅---原师傅id
			//租务工单
			// $("#zuId").html(selectId);---已废弃
			// $("#zuAdress").html(orderInfo.detailAddress);---已废弃
			//补配工单---已废弃
			// 	退单---撤单---创建租务工单---need
			orderIdOther=orderInfo.outOrderNo; //三方工单id--
			//创建报修单
			// linkPeople
			let linkPeople='<option value="">请选择报修人</option>';
			let linkPeoples='<option value="">请选择联系人</option>';
			$("#linkPeople").empty();
			$("#awPeople").empty();
			let initiateOtherInfoVal='';
			if(orderInfo.lesseeInfo){
				$.each($.parseJSON(orderInfo.lesseeInfo),function(index,value){
					initiateOtherInfoVal+=`<div><em>`+value.room_code+`<em>  </em>`+value.customer_name+`</em>:<em>`+value.customer_mobile+`</em></div>`;
					linkPeople+='<option value="'+value.customer_id+'" name="'+value.customer_mobile+'">'+value.customer_name+'</option>';
					linkPeoples+='<option value="'+value.customer_id+'" name="'+value.customer_mobile+'">'+value.customer_name+'</option>';
				})
			}else{
				initiateOtherInfoVal='--';
			}
			$("#initiateOtherInfoVal").empty().html(initiateOtherInfoVal);
			// 联系人信息---工单详情---租户信息
			$("#linkPeople").append(linkPeople);
			$("#awPeople").append(linkPeoples);//创建租务工单
			let apartmentLockInfo='';
			$("#needMaintainLockRel").empty();
			if(orderInfo.apartmentArea){
				$.each($.parseJSON(orderInfo.apartmentLockInfo),function(index,value){
					apartmentLockInfo+='<option value="'+index+'">'+value+'</option>';
				})
			};
			$("#needMaintainLockRel").append(apartmentLockInfo);//报修区域
			let apartmentArea='<option value="">请选择报修区域</option>';
			$("#initiateRepairAreaRel").empty();
			if(orderInfo.apartmentArea){
				$.each($.parseJSON(orderInfo.apartmentArea),function(index,value){
					index++;
					apartmentArea+='<option value="'+index+'">'+value+'</option>';
				})
			};
			$("#initiateRepairAreaRel").append(apartmentArea);//报修区域
			$("#bxArea").empty().append(apartmentArea);//报修区域---转派供应商
			$("#bxAreaZpwxy").empty().append(apartmentArea);//报修区域---转派维修员
			$("#orderIdRel").html(selectId);//工单id
			$("#detailAddressRel").val(orderInfo.detailAddress);//公寓地址
			// $("#initiateRepairNameRel").val(orderInfo.initiateRepairName);//报修人姓名
			// $("#initiateRepairTelRel").val(orderInfo.initiateRepairTel);//报修人电话
			let faultDescriptionCusess=orderInfo.faultDescriptionCuse?orderInfo.faultDescriptionCuse:'--';
			$("#faultDescriptionCuseRel").html(faultDescriptionCusess);//报修描述-客服
			let faultDescriptionUserss=orderInfo.faultDescriptionUser?orderInfo.faultDescriptionUser:'--';
			$("#faultDescriptionUserRel").html(faultDescriptionUserss);//报修描述
			isExistLinkorder=orderInfo.isExistLinkorder?orderInfo.isExistLinkorder:2;//工单详情-是否存在关联工单
			isRentOrder=orderInfo.isRentOrder?orderInfo.isRentOrder:2;//工单详情-是否存在租务工单
			//非关联工单-获取详情---
			if(isExistLinkorder==1){
				linkedNo=orderInfo.linkedNo?orderInfo.linkedNo:'';//工单详情-关联工单ID
			};
			if(selectIdSta==6){
				if(isExistLinkorder==1){
					$("#fqbxd").remove();
					$("#repairlist").show();
				}else{
					$("#fqbxd").show();
					$("#repairlist").remove();
				};
			}else{
				if(isExistLinkorder==1){
					$("#repairlist").show();
				}else{
					$("#repairlist").remove();
				};
			};
			$("#workOrderDetailCon").show();//工单详情
		},

		//订单列表---查询原因归属列表    GET /configuration/getConfigServiceItems
		getConfigServiceItems:function(){
			$("#cjreason").empty();
			win.utils.ajaxGet(win.utils.services.configMan_ConfigServiceItems_list, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId:selectId,//判断是关联工单-原工单请求
			}, function(result) {
				if(result.result_code == 200) {
					if(result.configServiceItems.length>0){
						let strVal = '<option value="">请选择原因归属</option>';
						let indexSelf=1;
						$.each(result.configServiceItems,function(index,value){
							strVal += '<option value="' + indexSelf + '">' + value + '</option>';
							indexSelf++;
						});
						$("#cjreason").append(strVal);
						$("#cjreasonCon").show();
					}else{
						$("#cjreasonCon").hide();
					};
				};
			})
		},
		//工单详情-获取具体信息
		getWorkDetail: function (id){//id存在-请求关联工单，反之，请求原工单
			isExistLinkorder=null;//工单详情-是否存在关联工地
			if(id){

			}else{
				tradingAreaId=-1;
				newestChecksta=0,//在查询工单详情里---最新维修方案状态---
					yjzbDescFault='';//一键转办---报修描述
				order_clear.clearDetailAndBtn();
				linkedNo='';//工单详情-关联工单ID
			};
			win.utils.ajaxGet(win.utils.services.orderMan_order_detail, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				orderId:id!=''?id:selectId,//判断是关联工单-原工单请求
			}, function(result) {
				if(result.result_code == 200) {
					let orderInfo=result.orderInfo;
					$("#goodsINfoRel").empty();// goodsINfoRel 创建关联维修单-商品
					$("#goodsInfoZpgys").empty();//商品
					order_list.setWorkDetailData(id,orderInfo);
					// id-关联工单/本工单,orderInfo-订单信息,orderStatu-订单状态
				} else {
					utils.showLayerMessage('获取失败，请稍后重试！');
				}
			});
		},
		//维修方案list---维修方案审批
		updateMaintainPlan:function(idVal,statusVal,index){
			let strStr='';
			if(statusVal==1){
				strStr='<span class="normalOks">通过</span>';
			}else if(statusVal==2){
				strStr='<span class="normalTwos">驳回</span>';
			}else{
				statusVal=3;
				strStr='<span class="normalThrees">待审核</span>';
			};
			let AsignStafVal=$("input[name='isAsignStaf']:checked").val();//是否分配协调
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				id:idVal,
				checkStatus:statusVal*1,
				checkRemark: $("#shenpiNote").val(),
				orderId:selectId,
			};
			let createVal=$("input[name='isCreate']:checked").val();//创建状态
			if(statusVal==1){
				datas.isAssignStaff=AsignStafVal // isAssignStaff是否分配协调(1-分配，2-不分配)
			}
			if(statusVal==1&&isCreateSub==true&&createVal==1){
				datas.isCreateTask=1;// isCreateTask: 是否需要创建子工单：1创建,0否 ,
				datas.subTaskNote=$("#shenpiChildNote").val();// subTaskNote  子工单备注 ,
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_updateMaintainPlan, datas, function(result) {
				isCreateSub=false;
				selectIdSta=6;
				$("#shenpiStaClick input").prop({'checked':false});
				if(result.result_code == 200) {
					utils.showConfirmWithCabackSong('操作成功',closeFlagCon());
					$("#table").jqGrid('setCell',selectId,"checkStatus",statusVal);
					$("#table").jqGrid('setCell',selectId,"orderStatus",6);
					let csVal='';
					let checkStatussClass='';
					if(statusVal==1){
						csVal='通过';
						checkStatussClass='a';
					}else if(statusVal==2){
						csVal='驳回';
						checkStatussClass='b';
					};
					$("#maintPlanSta").text(csVal).removeClass('a').removeClass('b').addClass(checkStatussClass);
					$("#maintPlanCon .boldTils").eq(index).find('.selfBor').eq(0).find('span').eq(0).remove();//
					$("#maintPlanCon .boldTils").eq(index).find('.selfBor').eq(0).find('button').eq(0).remove();//
					$("#maintPlanCon .boldTils").eq(index).find('.selfBor').eq(0).append(strStr);//状态更改
					let bpDateVal=new Date(result.checkTime);//审批时间
					let bpYear=bpDateVal.getFullYear();
					let bpMonth=bpDateVal.getMonth()+1;
					bpMonth=bpMonth<=9?'0'+bpMonth:bpMonth;
					let bpDay=bpDateVal.getDate()<=9?'0'+bpDateVal.getDate():bpDateVal.getDate();
					let bpHour=bpDateVal.getHours()<=9?'0'+bpDateVal.getHours():bpDateVal.getHours();
					let bpMin=bpDateVal.getMinutes()<=9?'0'+bpDateVal.getMinutes():bpDateVal.getMinutes();
					let bpSec=bpDateVal.getSeconds()<=9?'0'+bpDateVal.getSeconds():bpDateVal.getSeconds();
					createTime=bpYear+"-"+bpMonth+"-"+bpDay+'  '+bpHour+":"+bpMin+":"+bpSec;
					$("#maintPlanCon .left").eq(index).find('.shenpiP').eq(0).html(result.checkUser);//审批人
					$("#maintPlanCon .left").eq(index).find('.shenpiT').eq(0).html(createTime).show();////审批时间
					$("#maintPlanCon .left").eq(index).find('.shenpiN').eq(0).html($("#shenpiNote").val()).show();//审批内容
					$("#shenpiNote").val('');
					$("#shenpiCon .inputNums em").eq(0).text('0');
					$("#shenpiNote").val('');
				} else {
					$("#shenpiNote").val('');
					utils.showLayerMessage('审批失败，请稍后重试！');
				};
			});
		},
		// 城市-弹窗-初始化维修员/配件
		refreshRepairman:function (obj,orderSta) {//	orderSta  1:创建维修方案 2：审核结算方案
			let partsCheckedOwnBf=[];//配件-已选择配件---配件库---修改前
			if(obj==3){
				if(orderSta==1){
					$.each(partsCheckedCjOwnBf,function(index,value){
						partsCheckedOwnBf.push(value);
					});
				}else if(orderSta==2){
					$.each(partsCheckedJsOwnBf,function(index,value){
						partsCheckedOwnBf.push(value);
					});
				};
			};
			layui.use(['form','code'], function () {
				var form = layui.form,
					$ = layui.$;
				// 代码块
				layui.code({
					title: 'html',
					encode: true,
					about: false
				});
				// 手动赋值
				let idArr=[];
				$.each(partsCheckedOwnBf,function(index,value){
					idArr.push(value.partsCode);
				});
				if(obj==7){//分配师傅
					if(servicePlanId!=''){
						let arr=[];
						arr.push(servicePlanId);
						$('select[name="一键转办查询"]').val(arr);
					}
				}else {
					$('select[name="配件名称搜索"]').val(idArr);
				}
				form.render();
				if(obj==1){//分配师傅

				}else if(obj==2){//转派师傅

				}else if(obj==4){//转派供应商

				}else if(obj==3){//配件库-添加配件---配件名称
					form.on('select(multiPartOwnName)', function (data) {
// 						console.log(data.elem); //得到select原始DOM对象
// 						console.log(data.value,"value"); //得到被选中的值（数组类型）
// 						console.log(data.othis); //得到美化后的DOM对象
// 						console.log(data.current_value,"current_value"); //当前操作的值（选中或取消的值）
// 						console.log(partsData,"partsData===");
						// partsArrData---配件类型arr
						let nowCheckNum=data.value.length;
						if(partsData.length==nowCheckNum){
							$("#checkedPartsOwn").empty();
							partsCheckedOwnBf=partsData;
							$.each(partsCheckedOwnBf,function(index,valuess){
								if(partsArrData.indexOf(valuess.partsCode)>-1){
									$("#partTypeCon>ul>li").eq(partsArrData.indexOf(valuess.partsCode)).removeClass('uncheck').addClass('partsTypeCheck');
								};
								valuess.partsNum=1.0;
							});
							$.each(partsCheckedOwnBf,function(index,valuess){
								valuess.partsNum=1.0;
								let strOwnS=`<li>
										<div>`+valuess.partsName+`</div> 
										<div>`+valuess.brandName+`</div>
										<div>`+valuess.partsType+`</div>
										<div>`+Number(valuess.price).toFixed(2)+`元/`+valuess.unitName+`</div>
									</li>`;
								$("#checkedPartsOwn").append(strOwnS);
							});
						}else if(nowCheckNum==0&&data.current_value==''){
							partsCheckedOwnBf=[];
							$("#checkedPartsOwn").empty();
							$.each($("#partTypeCon>ul>li"),function(index,valuess){
								$("#partTypeCon>ul>li").eq(index).removeClass('partsTypeCheck').addClass('uncheck');
							});
						}else if(data.current_value==''&&nowCheckNum!=0){
							$.each($("#partTypeCon>ul>li"),function(index,valuess){
								let idValNow=$("#partTypeCon>ul>li").eq(index).attr('value');
								if(data.value.indexOf(idValNow)<0){
									$("#partTypeCon>ul>li").eq(index).removeClass('partsTypeCheck').addClass('uncheck');
								}else{
									$("#partTypeCon>ul>li").eq(index).addClass('partsTypeCheck').removeClass('uncheck');
								};
							});
							let bfArr=[];
							$("#checkedPartsOwn").empty();
							let idArr=[];
							$.each(partsCheckedOwnBf,function(index,valuess){
								idArr.push(valuess.partsCode)
							});
							$.each(partsData,function(index,valuess){
								if(idArr.indexOf(valuess.partsCode)<0){
									valuess.partsNum=1.0;
									let strOwnS=`<li>
											<div>`+valuess.partsName+`</div> 
											<div>`+valuess.brandName+`</div>
											<div>`+valuess.partsType+`</div>
											<div>`+Number(valuess.price).toFixed(2)+`元/`+valuess.unitName+`</div>
										</li>`;
									$("#checkedPartsOwn").append(strOwnS);
									bfArr.push(valuess);
								}
							});
							partsCheckedOwnBf=bfArr;
						}else{
							let indexVal=null;
							$.each(partsCheckedOwnBf,function(index,valuess){
								if(valuess.partsCode==data.current_value){
									indexVal=index;
									return;
								};
							});
							if(data.value.indexOf(data.current_value)<0){
								if(partsArrData.indexOf(data.current_value)>-1){
									$("#partTypeCon>ul>li").eq(partsArrData.indexOf(data.current_value)).addClass('uncheck').removeClass('partsTypeCheck');
								};
								if(indexVal>-1){
									partsCheckedOwnBf.splice(indexVal,1);
									$("#checkedPartsOwn>li").eq(indexVal).remove();
								};
							}else{
								if(partsArrData.indexOf(data.current_value)>-1){
									$("#partTypeCon>ul>li").eq(partsArrData.indexOf(data.current_value)).removeClass('uncheck').addClass('partsTypeCheck');
								};
								if(indexVal<0||!indexVal){
									$("#partsNameSS").val(data.current_value);
									let transStr=$("#partsNameSS option:selected")[0].getAttribute('name').replace(/@#/g,'"');
									let obj=JSON.parse(transStr);
									obj.isCustomParts=2;
									obj.partsNum=1;
									partsCheckedOwnBf.push(obj);
									let strOwnS=`<li>
											<div>`+obj.partsName+`</div> 
											<div>`+obj.brandName+`</div>
											<div>`+obj.partsType+`</div>
											<div>`+Number(obj.price).toFixed(2)+`元/`+obj.unitName+`</div>
										</li>`;
									$("#checkedPartsOwn").append(strOwnS);
								}
							};
						};
						if(orderSta==1){
							partsCheckedCjOwnAf=[];
							$.each(partsCheckedOwnBf,function(index,value){
								partsCheckedCjOwnAf.push(value);
							});
						}else if(orderSta==2){
							partsCheckedJsOwnAf=[];
							$.each(partsCheckedOwnBf,function(index,value){
								partsCheckedJsOwnAf.push(value);
							});
						};
					});
				}else if(obj==5){//区域-商圈搜索-订单搜索
					form.on('select(busDistrice)', function (data) {
						// console.log(data.elem); //得到select原始DOM对象
						// console.log(data.value,"value"); //得到被选中的值（数组类型）
						// console.log(data.othis); //得到美化后的DOM对象
						// console.log(data.current_value,"current_value"); //当前操作的值（选中或取消的值）
						let ss=$("#busDistrict option:selected").attr('name');
						tradingAreaSearch=$("#busDistrict option:selected").text()=='商圈'?'':$("#busDistrict option:selected").text();//商圈名称 ,
						tradingAreaIdSearch=data.value;//商圈id ,
						blockIdSearch='';
						districtId=data.value==''?'':districtId;//区id
						clearInterval(setDataSix);
						order_list.getStaNum();
						//////////setIntervalRefreshGetData();//定时刷新数据---待分配
						order_list.refreshServerData();
						$("#district").val(ss);
						if(data.value==''){
							let str='<option value="">小区</option>';
							$("#village").empty().append(str);
							order_list.refreshRepairman(6)
						}else{
							order_list.queryRegionFun('busDistrict');
						};
					});
				}else if(obj==6){//区域-小区搜索-订单搜索
					form.on('select(village)', function (data) {
						blockIdSearch=data.value;//小区id
						clearInterval(setDataSix);
						order_list.getStaNum();
						/////////////////////setIntervalRefreshGetData();//定时刷新数据---待分配
						order_list.refreshServerData();//订单数据
					});
				}else if(obj==7){//一键转办---维修协调分类
					// matinSchemeArr
					let firstClassifyText='';
					let secondClassifyText='';
					let subTaskName='';
					form.on('select(yjzbSelect)', function (data) {
						if(data.value==''){
							firstClassifyText='';
							secondClassifyText='';
							subTaskName='';
						}else{
							$.each(matinSchemeArr,function(index,value){
								if(Number(data.value) == Number(value.id)){
									firstClassifyText=value.subTaskCategoryOneName;
									secondClassifyText=value.subTaskCategoryTwoName;
									subTaskName=value.subTaskName;
									return;
								};
							})
						};
						$("#yjzbChildFirstClass").html(firstClassifyText);
						$("#yjzbChildSecondClass").html(secondClassifyText);
						$("#yjzbChildOype").html(subTaskName);
					});
				}else{
					form.on('select(multi)', function (data) {
						workerIdsArr=data.value;//维修员搜索-订单搜索
					});
				};
			});
		},
		//创建租务工单获取一级分类，二级分类
		queryRentClassify:function(type){
			$("#secondIdZw").empty();
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				status: 1,//状态(1-启用，2-禁用)
			};
			if(type==2){
				datas.parentId = $("#firstIdZw").val();//父级id
			}else{
				$("#firstIdZw").empty();
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryRentClassify, datas, function(result) {
				if(result.result_code == 200) {
					let datasList=result.list;
					let str="";
					let stra=`<option value="">请选择一级分类</option>`;
					let strb=`<option value="">请选择二级分类</option>`;
					if(type==1){
						str=stra;
					}else{
						str=strb;
					};
					$.each(datasList,function(index,value){
						str+=`<option value="`+value.outId+`">`+value.name+`</option>`;
					});
					if(type==1){
						stra=str;
						$("#firstIdZw").append(stra);
						$("#secondIdZw").empty();
					}else{
						strb=str;
						$("#secondIdZw").append(strb);
					};
				}else{
					utils.showLayerMessage('获取失败，请稍后重试！');
				};
			});
		},
		//查询改期原因
		queryChangeReason:function(){
			if(selectIdStaBtn=="contact"){
				$("#contactReason").empty();
			}else{
				$("#gaiqiReason").empty();
			};
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_getChangeReason, datas, function(result) {
				if(result.result_code == 200) {
					let datasList=result.list;
					let str=`<option value="">请选择改期原因</option>`;
					$.each(datasList,function(index,value){
						index++;
						str+=`<option value="`+index+`">`+value+`</option>`;
					});
					if(selectIdStaBtn=="contact"){
						$("#contactReason").append(str);
					}else{
						$("#gaiqiReason").append(str);
					};
				}else{
					utils.showLayerMessage('查询改期原因失败，请稍后重试！');
				};
			});
		},
		//查询报警标签
		queryPoliceTag:function(){
			$("#policeTag").empty();
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_getPoliceTag, datas, function(result) {
				if(result.result_code == 200) {
					let datasList=result.list;
					let str=`<option value="">请选择报警标签</option>`;
					$.each(datasList,function(index,value){
						index++;
						str+=`<option value="`+index+`">`+value+`</option>`;
					});
					$("#policeTag").append(str);
				}else{
					utils.showLayerMessage('查询改期原因失败，请稍后重试！');
				};
			});
		},
		//查询供应商
		queryProvider:function(str){
			if(str=='zpgys'){
				$("#zpTypeS").empty();
			}else if(str=='zpwxy'){
				$("#zpwxyTypeS").empty();
			};
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
			};
			win.utils.ajaxPost(win.utils.services.orderMan_order_getProvider, datas, function(result) {
				if(result.result_code == 200) {
					providerArr=result.data;
					let strs=`<option value="">请选择转派类型</option>`;
					let stea='';
					$.each(providerArr,function(index,value){
						let nuindex=1;
						if(value.supplier_type=='装修队'){
							nuindex=1;
						}else if(value.supplier_type=='供应商'){
							nuindex=3;
							if(str=='zpwxy'){
								if(selectIdSta==5||selectIdSta==6){
									stea=`<option value="`+nuindex+`">`+value.supplier_type+`</option>`;
								}
							};
						}else if(value.supplier_type=='直供供应商'){
							nuindex=2;
						};
						if(str=='zpgys'){
							strs+=`<option value="`+nuindex+`">`+value.supplier_type+`</option>`;
						};
					});
					if(str=='zpgys'){
						$("#zpTypeS").append(strs);
						order_list.refreshRepairman(4);
					}else if(str=='zpwxy'){
						$("#zpwxyTypeS").append(stea);
						order_list.setProvider('zpwxy');
					};
				}else{
					utils.showLayerMessage('查询供应商失败，请稍后重试！');
				};
			});
		},
		//设定供应商类型
		setProvider:function(str){
			var strVal=0;
			if(str=='zpgys'){
				strVal=$("#zpTypeS").val();
				$("#zpSupply").empty();
			}else if(str=='zpwxy'){
				// strVal=$("#zpwxyTypeS").val();
				strVal=3;
				$("#zpwxySupply").empty();
			};
			let data=[];
			$.each(providerArr,function(index,value){
				if(strVal==1){
					if(value.supplier_type=='装修队'){
						data=value.supplier_lists;
					}
				}else if(strVal==3){
					if(value.supplier_type=='供应商'){
						data=value.supplier_lists;
					}
				}else if(strVal==2){
					if(value.supplier_type=='直供供应商'){
						data=value.supplier_lists;
					}
				};
			});
			let strs=`<option value="">请选择供应商/装修队</option>`;
			$.each(data,function(index,value){
				strs+=`<option value="`+value.supplier_id+`">`+value.supplier_name+`</option>`;
			});
			if(str=='zpgys'){
				$("#zpSupply").append(strs);
			}else if(str=='zpwxy'){
				$("#zpwxySupply").append(strs);
			};
			order_list.refreshRepairman(4);
		},
		shedingzpgysPaichuBaijiaxiu:function(){
			if(str=='zpwxy'){
				if(value.supplier_name.indexOf('百家修')>-1){
					strs+=`<option value="`+value.supplier_id+`">`+value.supplier_name+`</option>`;
				}
			}else{
				if(value.supplier_name.indexOf('百家修')<0){
					strs+=`<option value="`+value.supplier_id+`">`+value.supplier_name+`</option>`;
				}
			};
		},
		//初始化
		init: function() {
			var obj = this;
			if(utils.isLogin() == false) {
				utils.loginDomain();
				return;
			}
			//判断是否有订单权限，此时为配置管理的工单权限
			utils.isUserMenuIndexRight('order_list', function(result) {
				if(result == 1) {
					//initialize...
					obj.initList();
					let nameLogin=win.utils.getCookie('user_id');
					if(nameLogin=='lyf'||nameLogin=='songhuanhuan'||nameLogin=='shaojunfeng'){
						$("#baijiaxiuId").show();
					}else{
						$("#baijiaxiuId").hide();
					}
					win.utils.clearSpacesTextarea();
				} else {
					self.location = '../../../web/error/access_refuse.html';
					return;
				}
			});
			// JS监听某个输入框---回车事件绑定
			$('#searchTop').on('keyup','input', function(event) {
				if (event.keyCode == "13") {
					//回车执行查询
					$(this).blur();
					tradingAreaSearch=$("#busDistrict option:selected").val()=='商圈'?'':$("#busDistrict option:selected").val();//商圈名称 ,
					tradingAreaIdSearch=$("#busDistrict").val();//商圈id ,
					blockIdSearch=!$("#village").val()?'':parseInt($("#village").val());//小区id ,
					exigencyFlagSearch=parseInt($("#exigencyFlagSear").val());// 紧急标识(1、紧急；2、普通) ,
					firstGradeSearch=$("#firstGrade").val();// 一级工单类型id ,
					secondGradeSearch=$("#secondGrade").val();// 二级工单类型id ,
					linkOrderStatus=$("#relationWorkId").val();// 关联的工单状态 ,
					districtId=$("#districtId").val();//区域
					houseIdSearch=$("#apartment").val();//公寓id ,===暂无
					orderIdValSear=$("#orderId").val();//工单id ,===暂无
					adressValSear=$("#apartAdressCon").val();//公寓地址 ,===暂无
					orderIdValSears=orderIdValSear;//订单搜索id
					houseIdSearchs=houseIdSearch;//公寓id
					adressValSears=adressValSear;//公寓地址
					progressValSears=progressValSear;//进度状态
					OrderFlagValSears=OrderFlagValSear;//工单标签--sure
					repairTelValSears=repairTelValSear;//报修电话
					servicePlanSer=$("#servicePlan").val();// 维修方案id ,
					finishStatusSer=$("#isFinishFlagSear").val();// 工单是否完结
					isChangeDateSer=$("#isChangeDate").val();//是否改期上门
					sysNinetyReworkSer=$("#sysNinetyRework").val();//90天返修工单
					jsCheckUpgradeSer=$("#jsCheckUpgrade").val();//结算是否升级审核
					workerIdsArrs=workerIdsArr;//维修员
					// 订单-搜索-时间
					let strFp=$('#fenPaiTime').val();
					if(strFp.length!=0){
						orderFpTimeSt=strFp.slice(0,17)+"00";//分派时间开始
						orderFpTimeEn=strFp.slice(22,strFp.length-2)+"00";//分派时间结束
					}else{
						orderFpTimeSt='';//分派时间开始
						orderFpTimeEn='';//分派时间结束
					};
					let strYy=$('#orderTime').val();
					if(strYy.length!=0){
						orderYyTimeSt=strYy.slice(0,17)+"00";//预约时间开始
						orderYyTimeEn=strYy.slice(22,strYy.length-2)+"00";//预约时间结束
					}else{
						orderYyTimeSt='';//预约时间开始
						orderYyTimeEn='';//预约时间结束
					};
					let strFin=$('#finishTime').val();
					if(strFin.length!=0){
						orderFinTimeSt=strFin.slice(0,17)+"00";//完成时间开始
						orderFinTimeEn=strFin.slice(22,strFin.length-2)+"00";//完成时间结束
					}else{
						orderFinTimeSt='';//完成时间开始
						orderFinTimeEn='';//完成时间结束
					};
					let strCr=$('#createTime').val();
					if(strCr.length!=0){
						orderCreTimeSt=strCr.slice(0,17)+"00";//创建时间开始
						orderCreTimeEn=strCr.slice(22,strCr.length-2)+"00";//创建时间结束
					}else{
						orderCreTimeSt='';//创建时间开始
						orderCreTimeEn='';//创建时间结束
					};
					let strContact=$('#contactTime').val();
					if(strContact.length!=0){
						orderContTimeSt=strContact.slice(0,17)+"00";//联系时间开始
						orderContTimeEn=strContact.slice(22,strContact.length-2)+"00";//联系时间结束
					}else{
						orderContTimeSt='';//联系时间开始
						orderContTimeEn='';//联系时间结束
					};
					let strVis=$('#visitTime').val();
					if(strVis.length!=0){
						orderDoorTimeSt=strVis.slice(0,17)+"00"//上门时间开始
						orderDoorTimeEn=strVis.slice(22,strVis.length-2)+"00";//上门时间结束
					}else{
						orderDoorTimeSt='';//上门时间开始
						orderDoorTimeEn='';//上门时间结束
					};
					notnormal=$("#notnormal").val();//异常
					$('#SearchConss').hide();
					order_picture.allowScroll();
					$("#inputNowPages").val(1);
					nowCurPage =1;
					order_list.getServerData(1);
				}
			});
			$('#formSear').on('keyup','input', function(event) {
				if (event.keyCode == "13") {
					//回车执行查询
					$(this).blur();
					tradingAreaSearch=$("#busDistrict option:selected").val()=='商圈'?'':$("#busDistrict option:selected").val();//商圈名称 ,
					tradingAreaIdSearch=$("#busDistrict").val();//商圈id ,
					blockIdSearch=!$("#village").val()?'':parseInt($("#village").val());//小区id ,
					exigencyFlagSearch=parseInt($("#exigencyFlagSear").val());// 紧急标识(1、紧急；2、普通) ,
					firstGradeSearch=$("#firstGrade").val();// 一级工单类型id ,
					secondGradeSearch=$("#secondGrade").val();// 二级工单类型id ,
					linkOrderStatus=$("#relationWorkId").val();// 关联的工单状态 ,
					districtId=$("#districtId").val();//区域
					houseIdSearch=$("#apartment").val();//公寓id ,===暂无
					orderIdValSear=$("#orderId").val();//工单id ,===暂无
					adressValSear=$("#apartAdressCon").val();//公寓地址 ,===暂无
					orderIdValSears=orderIdValSear;//订单搜索id
					houseIdSearchs=houseIdSearch;//公寓id
					adressValSears=adressValSear;//公寓地址
					progressValSears=progressValSear;//进度状态
					OrderFlagValSears=OrderFlagValSear;//工单标签--sure
					repairTelValSears=repairTelValSear;//报修电话
					servicePlanSer=$("#servicePlan").val();// 维修方案id ,
					finishStatusSer=$("#isFinishFlagSear").val();// 工单是否完结
					isChangeDateSer=$("#isChangeDate").val();//是否改期上门
					sysNinetyReworkSer=$("#sysNinetyRework").val();//90天返修工单
					jsCheckUpgradeSer=$("#jsCheckUpgrade").val();//结算是否升级审核
					workerIdsArrs=workerIdsArr;//维修员
					// 订单-搜索-时间
					let strFp=$('#fenPaiTime').val();
					if(strFp.length!=0){
						orderFpTimeSt=strFp.slice(0,17)+"00";//分派时间开始
						orderFpTimeEn=strFp.slice(22,strFp.length-2)+"00";//分派时间结束
					}else{
						orderFpTimeSt='';//分派时间开始
						orderFpTimeEn='';//分派时间结束
					};
					let strYy=$('#orderTime').val();
					if(strYy.length!=0){
						orderYyTimeSt=strYy.slice(0,17)+"00";//预约时间开始
						orderYyTimeEn=strYy.slice(22,strYy.length-2)+"00";//预约时间结束
					}else{
						orderYyTimeSt='';//预约时间开始
						orderYyTimeEn='';//预约时间结束
					};
					let strFin=$('#finishTime').val();
					if(strFin.length!=0){
						orderFinTimeSt=strFin.slice(0,17)+"00";//完成时间开始
						orderFinTimeEn=strFin.slice(22,strFin.length-2)+"00";//完成时间结束
					}else{
						orderFinTimeSt='';//完成时间开始
						orderFinTimeEn='';//完成时间结束
					};
					let strCr=$('#createTime').val();
					if(strCr.length!=0){
						orderCreTimeSt=strCr.slice(0,17)+"00";//创建时间开始
						orderCreTimeEn=strCr.slice(22,strCr.length-2)+"00";//创建时间结束
					}else{
						orderCreTimeSt='';//创建时间开始
						orderCreTimeEn='';//创建时间结束
					};
					let strContact=$('#contactTime').val();
					if(strContact.length!=0){
						orderContTimeSt=strContact.slice(0,17)+"00";//联系时间开始
						orderContTimeEn=strContact.slice(22,strContact.length-2)+"00";//联系时间结束
					}else{
						orderContTimeSt='';//联系时间开始
						orderContTimeEn='';//联系时间结束
					};
					let strVis=$('#visitTime').val();
					if(strVis.length!=0){
						orderDoorTimeSt=strVis.slice(0,17)+"00"//上门时间开始
						orderDoorTimeEn=strVis.slice(22,strVis.length-2)+"00";//上门时间结束
					}else{
						orderDoorTimeSt='';//上门时间开始
						orderDoorTimeEn='';//上门时间结束
					};
					notnormal=$("#notnormal").val();//异常
					$('#SearchConss').hide();
					order_picture.allowScroll();
					$("#inputNowPages").val(1);
					nowCurPage =1;
					order_list.getServerData(1);
				}
			});
			//  创建维修方案-添加原因归属方案归属
			$("#addReasonPlanBtn").on('click',function(){
				let strReason = $("#reasonBelong").html()
				let strPlan = $("#planBelong").html()
				if(strReason&&strPlan&&strPlan.length>=0&&strReason.length>=0){
					strReason = `<div class="form-group"><div class="col-sm-10"><select class="form-control">`+strReason+`</select></div><div class="col-sm-2"><em class="remove"><i class="fa fa-trash-o"></i></em></div></div>`
					strPlan =  `<div class="form-group"><div class="col-sm-10"><select class="form-control">`+strPlan+`</select></div><div class="col-sm-2"><em class="remove"><i class="fa fa-trash-o"></i></em></div></div>`
					$("#reasonCon").append(strReason)
					$("#planCon").append(strPlan)
				}else{
					// utils.showLayerMessage('');
				}
			});
			//  审核结算方案-添加原因归属方案归属
			$("#shjsfaCon").on('click','#addReasonPlanBtnJS',function(){
				let strReason = $("#reasonBelongJs").html()
				let strPlan = $("#planBelongJs").html()
				if(strReason&&strPlan&&strPlan.length>=0&&strReason.length>=0){
					strReason = `<div class="form-group"><div class="col-sm-10"><select class="form-control">`+strReason+`</select></div><div class="col-sm-2"><em class="remove"><i class="fa fa-trash-o"></i></em></div></div>`
					strPlan =  `<div class="form-group"><div class="col-sm-10"><select class="form-control">`+strPlan+`</select></div><div class="col-sm-2"><em class="remove"><i class="fa fa-trash-o"></i></em></div></div>`
					$("#reasonConJs").append(strReason)
					$("#planConJs").append(strPlan)
				}else{
					// utils.showLayerMessage('');
				}
			});
			//  创建维修方案-添加原因归属方案归属---删除操作
			$("#reasonCon").on('click','.remove',function(){
				// console.log($("#reasonCon .remove").index($(this)));
				let indexVal = $("#reasonCon .remove").index($(this)) +1
				$("#reasonCon>div").eq(indexVal).remove()
				$("#planCon>div").eq(indexVal).remove()
			});
			$("#planCon").on('click','.remove',function(){
				// console.log($("#planCon .remove").index($(this)))
				let indexVal = $("#planCon .remove").index($(this)) +1
				$("#reasonCon>div").eq(indexVal).remove()
				$("#planCon>div").eq(indexVal).remove()
			});
			//  审核结算方案-添加原因归属方案归属---删除操作
			$("#reasonConJs").on('click','.remove',function(){
				// console.log($("#reasonCon .remove").index($(this)));
				let indexVal = $("#reasonConJs .remove").index($(this)) +1
				$("#reasonConJs>div").eq(indexVal).remove()
				$("#planConJs>div").eq(indexVal).remove()
			});
			$("#planConJs").on('click','.remove',function(){
				// console.log($("#planCon .remove").index($(this)))
				let indexVal = $("#planConJs .remove").index($(this)) +1
				$("#reasonConJs>div").eq(indexVal).remove()
				$("#planConJs>div").eq(indexVal).remove()
			});
			//  维修方案点击事件
			$("#projectDescBlCon").off('click').on('click','.highLight',function(e){
				let name = $(this).attr('class')
				let jsonStr = $(this).attr('name');
				let jsonData = $.parseJSON(jsonStr)
				// console.log(jsonStr)
				// console.log(jsonData)
				if(name.indexOf('highLight')> -1){
					if($(this).hasClass('active')){
						$(this).next().show()
					}else{
						let nameVal = Number(jsonData.attrType)
						let newTextVal = jsonData.newTextVal
						$(this).addClass('active')
						let selectStr = ``
						let inputStrA = `<input type="text" name="text" autocomplete="off" class="appendTag" placeholder="请输入文字或数字">`
						let inputStrB = `<input type="number" min="0" autocomplete="off" class="appendTag" name="number" placeholder="请输入数字">`
						let inputStrC = `<input type="text" name="all" autocomplete="off" class="appendTag" placeholder="请输入文字">`
						if(nameVal == 1){
							let str = jsonData.val
							str = str.split(',')
							let optVal = '';
							for(let j =0;j< str.length;j++){
								if(str[j]!=''){
									optVal += `<option value="`+j+`">`+str[j]+`</option>`
								}
							}
							selectStr = `<select class="appendTag">`+optVal+`</select>`
							$(this).after(selectStr)
							$(this).hide()
						}else if(nameVal == 2){
							let idVal = Number(jsonData.textType)
							let str = '';
							if(idVal ==1){ // 1不限
								str = inputStrA
							}else if(idVal ==2){ // 2数字
								str = inputStrB
							}else if(idVal ==3){ //3文字
								str = inputStrC
							}
							$(this).after(str)
							$(this).parent().find('.appendTag').eq(0).val(newTextVal)
							$(this).hide()
						}
					}
				}
			})
			$("#projectDescBlCon").on('mouseout','select,input',function(e){
				let preBrotherNode = e.target.previousElementSibling
				let jsonDataStr = preBrotherNode.getAttribute('name');
				let jsonData =  $.parseJSON(jsonDataStr)
				let tagName = e.target.nodeName;
				let textVal = ''
				if(tagName.indexOf('INPUT')>-1){
					textVal = e.target.value
				}else if(tagName.indexOf('SELECT')>-1){
					let index = e.target.selectedIndex; // 选中索引
					textVal = e.target.options[index].text; // 选中文本
				}
				textVal = textVal.replace(/【&/g,'').replace(/】/g,'');
				jsonData.newTextVal = textVal;
				$("#projectDescBl .highLight").show();
				preBrotherNode.setAttribute('name',JSON.stringify(jsonData));
				preBrotherNode.innerHTML = `【&`+textVal+`】`;
				preBrotherNode.setAttribute('class','highLight')
				e.target.remove()
				activeTransText()
			})
			//90天维修记录---点击事件
			$("#recordListCon").on('click','li',function(){
				$("#recordListCon li").removeClass('active');
				$(this).addClass('active');
				let nameVal=$(this).attr('name');
				let idVal=$(this).attr('idval');
				if(nameVal){
					order_list.queryOrderNewDesc(nameVal,idVal);
				};
			});
			//90天维修记录---点击事件---结算审核通过
			$("#MaintettlemeSntCon").on('click','#recordListConJs li',function(){
				$("#MaintettlemeSntCon #recordListConJs li").removeClass('active');
				$(this).addClass('active');
				let nameVal=$(this).attr('name');
				let idVal=$(this).attr('idval');
				if(nameVal){
					order_list.queryOrderNewDesc(nameVal,idVal);
				};
			});
			//订单列表---输入页码跳转
			$("#table_pager").on('input','#inputNowPages',function(event){
				flagInput=true;
				inputPageFlag = true;
				let pag=Number($(this).val());
				if(!isNaN(pag)){
					if(pag<=0){
						$(this).val('');
					}else{
						if(pag<=1){
							nowCurPage=1;
							pag=nowCurPage;
						}else if(pag>totalNowPages){
							pag=totalNowPages;
						}else{
							nowCurPage=pag;
						};
						$(this).val(nowCurPage);
					}
				};
			});
			$("#table_pager").on('keyup','#inputNowPages',function(event){
				if (event.keyCode == "13") {
					//回车执行查询
					$(this).blur();
					tradingAreaSearch=$("#busDistrict option:selected").val()=='商圈'?'':$("#busDistrict option:selected").val();//商圈名称 ,
					tradingAreaIdSearch=$("#busDistrict").val();//商圈id ,
					blockIdSearch=!$("#village").val()?'':parseInt($("#village").val());//小区id ,
					exigencyFlagSearch=parseInt($("#exigencyFlagSear").val());// 紧急标识(1、紧急；2、普通) ,
					firstGradeSearch=$("#firstGrade").val();// 一级工单类型id ,
					secondGradeSearch=$("#secondGrade").val();// 二级工单类型id ,
					linkOrderStatus=$("#relationWorkId").val();// 关联的工单状态 ,
					districtId=$("#districtId").val();//区域
					houseIdSearch=$("#apartment").val();//公寓id ,===暂无
					orderIdValSear=$("#orderId").val();//工单id ,===暂无
					adressValSear=$("#apartAdressCon").val();//公寓地址 ,===暂无
					orderIdValSears=orderIdValSear;//订单搜索id
					houseIdSearchs=houseIdSearch;//公寓id
					adressValSears=adressValSear;//公寓地址
					progressValSears=progressValSear;//进度状态
					OrderFlagValSears=OrderFlagValSear;//工单标签--sure
					repairTelValSears=repairTelValSear;//报修电话
					servicePlanSer=$("#servicePlan").val();// 维修方案id ,
					finishStatusSer=$("#isFinishFlagSear").val();// 工单是否完结
					isChangeDateSer=$("#isChangeDate").val();//是否改期上门
					sysNinetyReworkSer=$("#sysNinetyRework").val();//90天返修工单
					jsCheckUpgradeSer=$("#jsCheckUpgrade").val();//结算是否升级审核
					workerIdsArrs=workerIdsArr;//维修员
					// 订单-搜索-时间
					let strFp=$('#fenPaiTime').val();
					if(strFp.length!=0){
						orderFpTimeSt=strFp.slice(0,17)+"00";//分派时间开始
						orderFpTimeEn=strFp.slice(22,strFp.length-2)+"00";//分派时间结束
					}else{
						orderFpTimeSt='';//分派时间开始
						orderFpTimeEn='';//分派时间结束
					};
					let strYy=$('#orderTime').val();
					if(strYy.length!=0){
						orderYyTimeSt=strYy.slice(0,17)+"00";//预约时间开始
						orderYyTimeEn=strYy.slice(22,strYy.length-2)+"00";//预约时间结束
					}else{
						orderYyTimeSt='';//预约时间开始
						orderYyTimeEn='';//预约时间结束
					};
					let strFin=$('#finishTime').val();
					if(strFin.length!=0){
						orderFinTimeSt=strFin.slice(0,17)+"00";//完成时间开始
						orderFinTimeEn=strFin.slice(22,strFin.length-2)+"00";//完成时间结束
					}else{
						orderFinTimeSt='';//完成时间开始
						orderFinTimeEn='';//完成时间结束
					};
					let strCr=$('#createTime').val();
					if(strCr.length!=0){
						orderCreTimeSt=strCr.slice(0,17)+"00";//创建时间开始
						orderCreTimeEn=strCr.slice(22,strCr.length-2)+"00";//创建时间结束
					}else{
						orderCreTimeSt='';//创建时间开始
						orderCreTimeEn='';//创建时间结束
					};
					let strContact=$('#contactTime').val();
					if(strContact.length!=0){
						orderContTimeSt=strContact.slice(0,17)+"00";//联系时间开始
						orderContTimeEn=strContact.slice(22,strContact.length-2)+"00";//联系时间结束
					}else{
						orderContTimeSt='';//联系时间开始
						orderContTimeEn='';//联系时间结束
					};
					let strVis=$('#visitTime').val();
					if(strVis.length!=0){
						orderDoorTimeSt=strVis.slice(0,17)+"00"//上门时间开始
						orderDoorTimeEn=strVis.slice(22,strVis.length-2)+"00";//上门时间结束
					}else{
						orderDoorTimeSt='';//上门时间开始
						orderDoorTimeEn='';//上门时间结束
					};
					notnormal=$("#notnormal").val();//异常
					$('#SearchConss').hide();
					order_picture.allowScroll();
					nowCurPage = $("#inputNowPages").val();
					order_list.getServerData(nowCurPage);
				}
			});
			$("#table_pager").on('blur','#inputNowPages',function(){
				flagInput=false;
				inputPageFlag= false;
			});
			// 搜索
			$('#search').on('click',function(){
				exigencyFlagSearch=parseInt($("#exigencyFlagSear").val());// 紧急标识(1、紧急；2、普通) ,
				firstGradeSearch=$("#firstGrade").val();// 一级工单类型id ,
				secondGradeSearch=$("#secondGrade").val();// 二级工单类型id ,
				linkOrderStatus=$("#relationWorkId").val();// 关联的工单状态 ,
				houseIdSearch=$("#apartment").val();//公寓id ,===暂无
				orderIdValSear=$("#orderId").val();//工单id ,===暂无
				adressValSear=$("#apartAdressCon").val();//公寓地址 ,===暂无
				orderIdValSears=orderIdValSear;//订单搜索id
				houseIdSearchs=houseIdSearch;//公寓id
				adressValSears=adressValSear;//公寓地址
				OrderFlagValSears=OrderFlagValSear;//工单标签--sure
				progressValSears=progressValSear;//进度状态
				repairTelValSears=repairTelValSear;//报修电话
				servicePlanSer=$("#servicePlan").val();// 维修方案id ,
				finishStatusSer=$("#isFinishFlagSear").val();// 工单是否完结
				isChangeDateSer=$("#isChangeDate").val();//是否改期上门
				sysNinetyReworkSer=$("#sysNinetyRework").val();//90天返修工单
				jsCheckUpgradeSer=$("#jsCheckUpgrade").val();//结算是否升级审核
				workerIdsArrs=workerIdsArr;//维修员

				tradingAreaSearch=$("#busDistrict option:selected").text()=='商圈'?'':$("#busDistrict option:selected").text();//商圈名称 ,
				tradingAreaIdSearch=$("#busDistrict").val()?parseInt($("#busDistrict").val()):'';//商圈id ,
				blockIdSearch=$("#village").val()?parseInt($("#village").val()):'';//小区id ,
				districtId=$("#district").val()?parseInt($("#district").val()):'';//区域
				// 订单-搜索-时间
				let strFp=$('#fenPaiTime').val();
				if(strFp.length!=0){
					orderFpTimeSt=strFp.slice(0,17)+"00";//分派时间开始
					orderFpTimeEn=strFp.slice(22,strFp.length-2)+"00";//分派时间结束
				}else{
					orderFpTimeSt='';//分派时间开始
					orderFpTimeEn='';//分派时间结束
				};
				let strYy=$('#orderTime').val();
				if(strYy.length!=0){
					orderYyTimeSt=strYy.slice(0,17)+"00";//预约时间开始
					orderYyTimeEn=strYy.slice(22,strYy.length-2)+"00";//预约时间结束
				}else{
					orderYyTimeSt='';//预约时间开始
					orderYyTimeEn='';//预约时间结束
				};
				let strFin=$('#finishTime').val();
				if(strFin.length!=0){
					orderFinTimeSt=strFin.slice(0,17)+"00";//完成时间开始
					orderFinTimeEn=strFin.slice(22,strFin.length-2)+"00";//完成时间结束
				}else{
					orderFinTimeSt='';//完成时间开始
					orderFinTimeEn='';//完成时间结束
				};
				let strCr=$('#createTime').val();
				if(strCr.length!=0){
					orderCreTimeSt=strCr.slice(0,17)+"00";//创建时间开始
					orderCreTimeEn=strCr.slice(22,strCr.length-2)+"00";//创建时间结束
				}else{
					orderCreTimeSt='';//创建时间开始
					orderCreTimeEn='';//创建时间结束
				};
				let strContact=$('#contactTime').val();
				if(strContact.length!=0){
					orderContTimeSt=strContact.slice(0,17)+"00";//联系时间开始
					orderContTimeEn=strContact.slice(22,strContact.length-2)+"00";//联系时间结束
				}else{
					orderContTimeSt='';//联系时间开始
					orderContTimeEn='';//联系时间结束
				};
				let strVis=$('#visitTime').val();
				if(strVis.length!=0){
					orderDoorTimeSt=strVis.slice(0,17)+"00"//上门时间开始
					orderDoorTimeEn=strVis.slice(22,strVis.length-2)+"00";//上门时间结束
				}else{
					orderDoorTimeSt='';//上门时间开始
					orderDoorTimeEn='';//上门时间结束
				};
				notnormal=$("#notnormal").val();//异常
				$('#SearchConss').hide();
				order_list.getServerData(1);
			});
			//刷新
			$('#empty').on('click', function() {
				orderFpTimeSt='';//分派时间开始
				orderFpTimeEn='';//分派时间结束
				orderYyTimeSt='';//预约时间开始
				orderYyTimeEn='';//预约时间结束
				orderFinTimeSt='';//完成时间开始
				orderFinTimeEn='';//完成时间结束
				orderCreTimeSt='';//创建时间开始
				orderCreTimeEn='';//创建时间结束
				orderContTimeSt='';//联系时间开始
				orderContTimeEn='';//联系时间结束
				orderDoorTimeSt='';//上门时间开始
				orderDoorTimeEn='';//上门时间结束
				tradingAreaSearch='';//商圈名称 ,
				tradingAreaIdSearch='';//商圈id ,
				blockIdSearch='';//小区id ,
				exigencyFlagSearch='';// 紧急标识(1、紧急；2、普通) ,
				firstGradeSearch='';// 一级工单类型id ,
				secondGradeSearch='';// 二级工单类型id ,
				linkOrderStatus='';// 关联的工单状态 ,
				houseIdSearch='';//公寓id ,===暂无
				houseIdSearchs='';
				districtId='';//区域
				progressValSear='';
				progressValSears='';
				OrderFlagValSears='';
				OrderFlagValSear='';//工单标签
				orderIdValSear='';
				orderIdValSears='';
				repairTelValSear='';
				repairTelValSears='';
				adressValSear='';
				adressValSears='';
				citySearchVal='';
				servicePlanSer='';
				finishStatusSer='25';// 工单是否完结
				isChangeDateSer='';//是否改期上门
				sysNinetyReworkSer='';//90天返修工单
				jsCheckUpgradeSer='';//结算审核是否升级
				notnormal='';
				workerIdsArr=[];
				workerIdsArrs=[];
				$("#secondGrade").empty();//二级工单类型
				order_list.getStaNum();
				$("#seBtnAlls button").removeClass('btn-info');
				$("#seBtnAlls button").eq(0).addClass('btn-info');
				obj.clearSearchVal();
				$("#wxfaxtzPlanSta").val('');
				$("#wxfaxtzPlanStaCon").hide();
				$("#district").empty().append("<option value=''>区</option>");
				$("#busDistrict").empty().append("<option value=''>商圈</option>");
				$("#village").empty().append("<option value=''>小区</option>");
				order_list.refreshRepairman(5);
				order_list.refreshRepairman(6);
				order_list.queryRepairman(2);
				order_picture.allowScroll();
				obj.getServerData(1);
			});
			//新增
			$('#btnAdd').click(obj.addWorkOrder);
			//搜搜-more
			$('#searchMore').on('click',function(){
				order_picture.forbidScroll();
				order_list.SearchConss();
				order_list.refreshRepairman();
			});
			//搜搜-more
			$('#sureMoreSave').on('click',function(){
				order_picture.allowScroll();
				$('#SearchConss').hide();
				exigencyFlagSearch=parseInt($("#exigencyFlagSear").val());// 紧急标识(1、紧急；2、普通) ,
				firstGradeSearch=$("#firstGrade").val();// 一级工单类型id ,
				secondGradeSearch=$("#secondGrade").val();// 二级工单类型id ,
				linkOrderStatus=$("#relationWorkId").val();// 关联的工单状态 ,
				houseIdSearch=$("#apartment").val();//公寓id ,===暂无
				orderIdValSear=$("#orderId").val();//工单id ,===暂无
				adressValSear=$("#apartAdressCon").val();//公寓地址 ,===暂无
				orderIdValSears=orderIdValSear;//订单搜索id
				houseIdSearchs=houseIdSearch;//公寓id
				adressValSears=adressValSear;//公寓地址
				progressValSears=progressValSear;//进度状态
				OrderFlagValSears=OrderFlagValSear;//工单标签--
				repairTelValSears=repairTelValSear;//报修电话
				servicePlanSer=$("#servicePlan").val();// 维修方案id ,
				finishStatusSer=$("#isFinishFlagSear").val();// 工单是否完结
				isChangeDateSer=$("#isChangeDate").val();//是否改期上门
				sysNinetyReworkSer=$("#sysNinetyRework").val();//90天返修工单
				jsCheckUpgradeSer=$("#jsCheckUpgrade").val();//结算是否升级审核
				workerIdsArrs=workerIdsArr;//维修员
				notnormal=$("#notnormal").val();//异常

				tradingAreaSearch=$("#busDistrict option:selected").text()=='商圈'?'':$("#busDistrict option:selected").text();//商圈名称 ,
				tradingAreaIdSearch=$("#busDistrict").val()?parseInt($("#busDistrict").val()):'';//商圈id ,
				blockIdSearch=$("#village").val()?parseInt($("#village").val()):'';//小区id ,
				districtId=$("#district").val()?parseInt($("#district").val()):'';//区域
				// 订单-搜索-时间
				let strFp=$('#fenPaiTime').val();
				if(strFp.length!=0){
					orderFpTimeSt=strFp.slice(0,17)+"00";//分派时间开始
					orderFpTimeEn=strFp.slice(22,strFp.length-2)+"00";//分派时间结束
				}else{
					orderFpTimeSt='';//分派时间开始
					orderFpTimeEn='';//分派时间结束
				};
				let strYy=$('#orderTime').val();
				if(strYy.length!=0){
					orderYyTimeSt=strYy.slice(0,17)+"00";//预约时间开始
					orderYyTimeEn=strYy.slice(22,strYy.length-2)+"00";//预约时间结束
				}else{
					orderYyTimeSt='';//预约时间开始
					orderYyTimeEn='';//预约时间结束
				};
				let strFin=$('#finishTime').val();
				if(strFin.length!=0){
					orderFinTimeSt=strFin.slice(0,17)+"00";//完成时间开始
					orderFinTimeEn=strFin.slice(22,strFin.length-2)+"00";//完成时间结束
				}else{
					orderFinTimeSt='';//完成时间开始
					orderFinTimeEn='';//完成时间结束
				};
				let strCr=$('#createTime').val();
				if(strCr.length!=0){
					orderCreTimeSt=strCr.slice(0,17)+"00";//创建时间开始
					orderCreTimeEn=strCr.slice(22,strCr.length-2)+"00";//创建时间结束
				}else{
					orderCreTimeSt='';//创建时间开始
					orderCreTimeEn='';//创建时间结束
				};
				let strContact=$('#contactTime').val();
				if(strContact.length!=0){
					orderContTimeSt=strContact.slice(0,17)+"00";//联系时间开始
					orderContTimeEn=strContact.slice(22,strContact.length-2)+"00";//联系时间结束
				}else{
					orderContTimeSt='';//联系时间开始
					orderContTimeEn='';//联系时间结束
				};
				let strVis=$('#visitTime').val();
				if(strVis.length!=0){
					orderDoorTimeSt=strVis.slice(0,17)+"00"//上门时间开始
					orderDoorTimeEn=strVis.slice(22,strVis.length-2)+"00";//上门时间结束
				}else{
					orderDoorTimeSt='';//上门时间开始
					orderDoorTimeEn='';//上门时间结束
				};
				order_list.getServerData(1);
			});
			// 搜索-cancel
			$('#cancelMoreSave').on('click',function(){
				order_picture.allowScroll();
				$('#SearchConss').hide()
			});
			//装修配置信息--tab切换
			$("#roomName").on("click",'span',function(){
				let indexVal=$(this).index();
				$("#roomName span").removeClass('check');
				$(this).addClass('check');
				$("#roomDetail ul").hide();
				$("#roomDetail ul").eq(indexVal).show();
			});
			//租务工单--tab切换
			$("#rentralistCon").on("click",'label',function(){
				let indexVal=$(this).attr('name');
				if($(this).hasClass('l')){
					$("#rentralistCon .detailCssa").eq(indexVal).find('label.l').addClass('active');
					$("#rentralistCon .detailCssa").eq(indexVal).find('label.r').removeClass('active');
					$("#rentralistCon .detailCssa").eq(indexVal).find('div.right').hide();
					$("#rentralistCon .detailCssa").eq(indexVal).find('div.left').show();
				}else{
					$("#rentralistCon .detailCssa").eq(indexVal).find('label.l').removeClass('active');
					$("#rentralistCon .detailCssa").eq(indexVal).find('label.r').addClass('active');
					$("#rentralistCon .detailCssa").eq(indexVal).find('div.left').hide();
					$("#rentralistCon .detailCssa").eq(indexVal).find('div.right').show();
				}
			});
			//shenpiCon---审批维修方案状态
			$("#shenpiStaClick").on("click",'input',function(){
				$("#createChildOrder input").prop({'checked':false});
				$("#isAssignStaff input").prop({'checked':false});
				$("#shenpiCon .createChildOrder").hide();
				if($("#shenpiStaClick input:checked").val()==1){
					if(isCreateSub==true){
						$("#createChildOrder").show();
					}else{
						$("#createChildOrder").hide();
					}
					$("#isAssignStaff").show();
				}else{
					$("#isAssignStaff").hide();
					$("#createChildOrder").hide();
					$("#shenpiCon .createChildOrder").hide();
				};
			});
			//shenpiCon---是否创建子工单
			$("#createChildOrder").on("click",'input',function(){
				$("#shenpiCon .createChildOrder").hide();
				if(isCreateSub==true){
					if($("#createChildOrder input:checked").val()==1){
						order_list.queryNewestMPlan(1)
					}else{
						$("#shenpiCon .createChildOrder").hide();
					};
				};
			});
			//配件类型
			$("#partTypeCon>ul").off('li').on('click','li',function(){
				let partsCheckedOwnAf=[];
				if(selectIdSta==9){
					$.each(partsCheckedJsOwnAf,function(index,value){
						partsCheckedOwnAf.push(value);
					});
				}else{  //cjwxfa
					$.each(partsCheckedCjOwnAf,function(index,value){
						partsCheckedOwnAf.push(value);
					});
				};
				$(this).toggleClass('partsTypeCheck');
				let transStr=$(this).attr('name').replace(/@#/g,'"');
				let obj=JSON.parse(transStr);
				obj.isCustomParts=2;
				obj.partsNum=1;
				let idVal=obj.partsCode;//获取id
				let indexVal=-1;
				$.each(partsCheckedOwnAf,function(index,valuess){
					if(valuess.partsCode.indexOf(idVal)>-1){
						indexVal=index;
					};
					return;
				});
				if(indexVal<0){//选中
					partsCheckedOwnAf.push(obj);
					let strOwnS=`<li>
							<div>`+obj.partsName+`</div> 
							<div>`+obj.brandName+`</div>
							<div>`+obj.partsType+`</div>
							<div>`+Number(obj.price).toFixed(2)+`元/`+obj.unitName+`</div>
						</li>`;
					$("#checkedPartsOwn").append(strOwnS);
				}else{//未选中
					partsCheckedOwnAf.splice(indexVal,1);
					$("#checkedPartsOwn>li").eq(indexVal).remove();
				};
				if(selectIdSta==9){
					partsCheckedJsOwnAf=[];
					$.each(partsCheckedOwnAf,function(index,value){
						partsCheckedJsOwnAf.push(value);
					});
				}else{//cjwxfa
					partsCheckedCjOwnAf=[];
					$.each(partsCheckedOwnAf,function(index,value){
						partsCheckedCjOwnAf.push(value);
					});
				};
				layui.use(['form','code'], function () {
					var form = layui.form,
						$ = layui.$;
					// 代码块
					layui.code({
						title: 'html',
						encode: true,
						about: false
					});
					// 手动赋值
					let idArr=[];
					$.each(partsCheckedOwnAf,function(index,value){
						idArr.push(value.partsCode);
					});
					$('select[name="配件名称搜索"]').val(idArr);
					form.render();
				});
			});
			//订单list-search-工单id-
			$("#orderIdCon").on('blur',function(){
				orderIdValSear=$(this).val().replace(/\D/g, '');
				$(this).val(orderIdValSear)
				$("#orderId").val(orderIdValSear);
			});
			$("#orderId").on('blur',function(){
				orderIdValSear=$(this).val().replace(/\D/g, '');
				$(this).val(orderIdValSear)
				$("#orderIdCon").val(orderIdValSear);
			});
			//订单list-search-公寓id-
			$("#apartmentS").on('blur',function(){
				houseIdSearch=$(this).val().replace(/\D/g, '');
				$(this).val(houseIdSearch)
				$("#apartment").val(houseIdSearch);
			});
			$("#apartment").on('blur',function(){
				houseIdSearch=$(this).val().replace(/\D/g, '');
				$(this).val(houseIdSearch)
				$("#apartmentS").val(houseIdSearch);
			});
			//订单list-search-报修电话
			$("#repairTelCon").on('blur',function(){
				repairTelValSear=$(this).val();
				$("#repairTel").val(repairTelValSear);
			});
			$("#repairTel").on('blur',function(){
				repairTelValSear=$(this).val();
				$("#repairTelCon").val(repairTelValSear);
			});
			//订单list-search-公寓地址
			$("#apartAdressCon").on('blur',function(){
				adressValSear=$(this).val();
				$("#apartAdress").val(adressValSear);
			});
			$("#apartAdress").on('blur',function(){
				adressValSear=$(this).val();
				$("#apartAdressCon").val(adressValSear);
			});
			//订单list-search-进度状态
			$("#MainPlanCon").on('blur',function(){
				progressValSear=$(this).val();
				$("#MainPlan").val(progressValSear);
				searchStaFlag=true;
			});
			$("#MainPlan").on('blur',function(){
				progressValSear=$(this).val();
				$("#MainPlanCon").val(progressValSear);
				searchStaFlag=true;
			});
			//订单list-search-工单标签
			$("#OrderFlags").on('blur',function(){
				OrderFlagValSear=$(this).val();
				$("#OrderFlagss").val(OrderFlagValSear);
				searchStaFlag=true;
			});
			$("#OrderFlagss").on('blur',function(){
				OrderFlagValSear=$(this).val();
				$("#OrderFlags").val(OrderFlagValSear);
				searchStaFlag=true;
			});
			// 一级工单改变-创建维修方案
			$("#MatinplanFirstClassify").on('change',function(){
				$("#MatinplanSecondClassify").empty()
				$("#reasonCon").empty()
				$("#planCon").empty()
				if($("#MatinplanFirstClassify").val()!=''&&$("#MatinplanFirstClassify").val().indexOf('请选择一级分类')<0){
					order_list.queryMatinClassify(1,2,1,$("#MatinplanFirstClassify").val(),2)
				}
			});
			// 二级工单改变-创建维修方案-获取原因归属，方案归属
			$("#MatinplanSecondClassify").on('change',function(){
				$("#reasonCon").empty()
				$("#planCon").empty()
				if($("#MatinplanSecondClassify").val()!=''&&$("#MatinplanSecondClassify").val().indexOf('请选择二级分类')<0){
					order_list.getReasonMatinplanList(1,$("#MatinplanSecondClassify").val())
				}
			});
			// 一级工单改变-审核结算方案
			$("#MatinplanFirstClassifyJs").on('change',function(){
				judgeIsHasSecondIdFlag = true
				$("#MatinplanSecondClassifyJs").empty()
				$("#reasonConJs").empty()
				$("#planConJs").empty()
				if($("#MatinplanFirstClassifyJs").val()!=''&&$("#MatinplanFirstClassifyJs").val().indexOf('请选择一级分类')<0){
					order_list.queryMatinClassify(2,2,1,$("#MatinplanFirstClassifyJs").val(),2)
				}
			});
			// 二级工单改变-审核计算方案
			$("#MatinplanSecondClassifyJs").on('change',function(){
				judgeIsHasSecondIdFlag = true
				$("#reasonConJs").empty()
				$("#planConJs").empty()
				if($("#MatinplanSecondClassifyJs").val()!=''&&$("#MatinplanSecondClassifyJs").val().indexOf('请选择二级分类')<0){
					order_list.getReasonMatinplanList(2,$("#MatinplanSecondClassifyJs").val())
				}
			});
			//费用改变---后台校验
			$("#shangmenCost").on('change',function(){
				order_list.orderMan_order_judgeFee(1,1)
			});
			$("#jiajiCost").on('change',function(){
				order_list.orderMan_order_judgeFee(2,1)
			});
			$("#otherCost").on('change',function(){
				order_list.orderMan_order_judgeFee(3,1)
			});
			//费用改变---后台校验
			$("#indoorJs").on('change',function(){
				order_list.orderMan_order_judgeFee(1,2)
			});
			$("#urgentMoneyJs").on('change',function(){
				order_list.orderMan_order_judgeFee(2,2)
			});
			$("#otherMoneyJs").on('change',function(){
				order_list.orderMan_order_judgeFee(3,2)
			});
			$("#MainPlanCon").on('change',function(){
				let proVal=$(this).val();
				proVal=parseInt(proVal)
				if(proVal==6){
					$("#wxfaxtzPlanStaCon").show();
				}else{
					$("#wxfaxtzPlanStaCon").hide();
					$("#wxfaxtzPlanSta").val('');
				}
			});
			$("#MainPlan").on('change',function(){
				let proVal=$(this).val();
				proVal=parseInt(proVal)
				if(proVal==6){
					$("#wxfaxtzPlanStaCon").show();
				}else{
					$("#wxfaxtzPlanStaCon").hide();
					$("#wxfaxtzPlanSta").val('');
				}
			});
			//维修方案模板---点选  MatinPlanTemList
			$("#feedBackMaintenPlanId").on('change',function(){
				let idVal = $(this).val();
				if(idVal){
					mainPlanJsonArr=[]
					$("#projectDescBlCon").show()
					$("#projectDescCon").hide()
					order_list.getFeedbackTem(idVal);
				}else{
					partsCheckedCjOwnAf=[];//创建维修方案-配件-已选择配件---配件库---修改后
					partsCheckedCjOwnBf=[];//创建维修方案-配件-已选择配件---配件库---修改前
					$("#checkedPartsOwnShow").empty()
					$("#cjPartsPrice").html('配件费用总计：0.00元');
					$("#checkedPartsOwnShows").hide()
					$("#projectDescBlCon").hide()
					$("#projectDescCon").show()
				};
			});
			//审核维修结算---删除配件-----配件库---ok
			$("#checkedPartsOwnShowJs").on('click','.deletePartOwn',function(){
				let indexVal=$("#checkedPartsOwnShowJs .deletePartOwn").index($(this));
				partsCheckedJsOwnAf.splice(indexVal,1);
				partsCheckedJsOwnBf=partsCheckedJsOwnAf;
				$("#checkedPartsOwnShowJs li").eq(indexVal).remove();
				order_list.calculcateTotalFee('shjsfa');
				if($("#checkedPartsOwnShowJs>li").length>0){
					$("#checkedPartsOwnShowJss").show();
				}else{
					$("#checkedPartsOwnShowJss").hide();
				}
			});
			//审核维修结算---删除配件-----自定义配件--ok
			$("#checkedPartsSelfJs").on('click','.deletePartOwn',function(){
				let indexVal=$("#checkedPartsSelfJs .deletePartOwn").index($(this));
				partsCheckedJsSelf.splice(indexVal,1);
				$("#checkedPartsSelfJs li").eq(indexVal).remove();
				order_list.calculcateTotalFee('shjsfa');
				if($("#checkedPartsSelfJs>li").length>0){
					$("#checkedPartsSelfJs").removeClass('noPadding').addClass('yesPadding');
				}else{
					$("#checkedPartsSelfJs").removeClass('yesPadding').addClass('noPadding');
				}
			});
			//审核维修结算---修改配件---输入框变化---存值---配件库配件-ok
			$("#checkedPartsOwnShowJs").on('change','input',function(){
				let titleVal=$(this).attr('title');
				let valNow=$(this).val();
				if(titleVal=='oneNumber'){
					valNow=Number(valNow).toFixed(1)<=0?0.0:Number(valNow).toFixed(1);
				}else if(titleVal=='twoNumber'){
					valNow=Number(valNow).toFixed(1)<=0?0.00:Number(valNow).toFixed(2);
				};
				$(this).val(valNow);
				let indexVal=$("#checkedPartsOwnShowJs input").index($(this));
				partsCheckedJsOwnAf[indexVal].partsNum=valNow;
				order_list.calculcateTotalFee('shjsfa');
			});
			//创建维修方案---添加配件---输入框变化---存值---配件库选择-ok
			$("#checkedPartsOwnShow").on('change','input',function(){
				let titleVal=$(this).attr('title');
				let valNow=$(this).val();
				if(titleVal=='oneNumber'){
					valNow=Number(valNow).toFixed(1)<=0?0.0:Number(valNow).toFixed(1);
				}else if(titleVal=='twoNumber'){
					valNow=Number(valNow).toFixed(1)<=0?0.00:Number(valNow).toFixed(2);
				};
				$(this).val(valNow);
				let indexVal=$("#checkedPartsOwnShow input").index($(this));
				partsCheckedCjOwnAf[indexVal].partsNum=valNow;
				order_list.calculcateTotalFee('cjwxfa');
			});
			//创建维修方案---添加配件（配件库中添加）---删除-操作---创建维修方案-ok
			$("#checkedPartsOwnShow").off('click').on('click','.deletePartOwn',function(){
				let indexVal=$("#checkedPartsOwnShow .deletePartOwn").index($(this));
				partsCheckedCjOwnAf.splice(indexVal,1);
				partsCheckedCjOwnBf=[];
				$.each(partsCheckedCjOwnAf,function(index,value){
					partsCheckedCjOwnBf.push(value);
				});
				$("#checkedPartsOwnShow li").eq(indexVal).remove();
				order_list.calculcateTotalFee('cjwxfa');
				if($("#checkedPartsOwnShow li").length>0){
					$("#checkedPartsOwnShows").show()
				}else{
					$("#checkedPartsOwnShows").hide()
				};
				let arr = [];
				arr = partsCheckedCjSelf.concat(partsCheckedCjOwnAf)
				if(arr.length>0 && isExistPartsBfFlag ==false){
					isExistPartsBfFlag = true
					order_list.queryCostConfigures(1,1)
				}else if(arr.length<=0 && isExistPartsBfFlag ==true){
					isExistPartsBfFlag = false
					order_list.queryCostConfigures(1,1)
				}
			});
			//保留1位或2位小数--并且强制去除空格
			$("body").on('input','input',function(){
				let titleVal=$(this).attr('title');
				let valNow=$(this).val();
				if(titleVal=='oneNumber'){
					valNow =valNow.replace(/[^\d.]/g,'');
					$(this).val(valNow);
				}else if(titleVal=='twoNumber'){
					valNow =valNow.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
					$(this).val(valNow);
				};
			});
			//保留1位或2位小数--并且强制去除空格
			$("body").on('blur','input',function(){
				// let typeVal=$(this).attr('type');
				let titleVal=$(this).attr('title');
				let valNow=$(this).val();
				if(titleVal=='oneNumber'||titleVal=='twoNumber'){
					valNow = valNow.replace(/\s*/g,"");
				}else{
					valNow = valNow.trim();
				};
				if(titleVal=='oneNumber'){
					valNow =valNow.replace(/[^\d.]/g,'');
					valNow=Number(valNow).toFixed(1);
				}else if(titleVal=='twoNumber'){
					valNow =valNow.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
					valNow=Number(valNow).toFixed(2);
				};
				$(this).val(valNow);
				let nameVal=$(this).attr('name');
				if(nameVal=="jsfas"){
					let a=Number($("#otherMoneyJs").val()),//其他费用
						b=Number($("#urgentMoneyJs").val()),// 加急费用
						c=Number($("#indoorJs").val()),//上门费用
						d=Number($("#partsTotalCostJs").val()),//配件费用
						e=Number($("#doorOverJs").val()),//上门超时费用
						f=Number($("#linkOverJs").val()) ,//联系超时费用 ,
						g=Number($("#backOverJs").val()),//反馈超时费用 ,
						h=Number($("#partOverJs").val());//配件超时费用 ,
					$("#partsFeeJs").text(Number(a+b+c+d).toFixed(2));//工单费用-未扣除
					let deleteFeeVal=Number(e+f+g+h).toFixed(2);
					if(deleteFeeVal>0){
						deleteFeeVal='-'+deleteFeeVal;
					};
					$("#deleteFee").text(deleteFeeVal);//扣除费用
				};
			});
			//待分配订单---隐藏Yu显示
			$("#daifenpeiCon").on('click','#controlFenpeiNum',function(){
				if($("#controlFenpeiNumShow").is(':hidden')){
					$("#controlFenpeiNumShow").show()
					$("#controlFenpeiNum").removeClass('b').addClass('a');
					$("#controlFenpeiNums").removeClass('b').addClass('a');
				}else{
					$("#controlFenpeiNumShow").hide()
					$("#controlFenpeiNum").removeClass('a').addClass('b');
					$("#controlFenpeiNums").removeClass('a').addClass('b');
				};
			});
			//禁止弹窗与浏览器滚动
			$("body").on('focus','textarea',function(){
				flagInput=true;
			});
			$("body").on('blur','textarea',function(){
				flagInput=false;
			});
			//统计数字--textarea
			$("body").on('input','textarea',function(){
				let nums=$(this).val().length;
				$(this).parent('div').find('em').eq(0).text(nums);
			});
			//创建维修方案---添加配件（自定义添加）---删除-操作---创建维修方案-ok
			$("#checkedPartsSelf").on('click','.deletePartOwn',function(){
				let indexVal=$("#checkedPartsSelf .deletePartOwn").index($(this));
				partsCheckedCjSelf.splice(indexVal,1);
				$("#checkedPartsSelf li").eq(indexVal).remove();
				order_list.calculcateTotalFee('cjwxfa');
				let arr = [];
				arr = partsCheckedCjSelf.concat(partsCheckedCjOwnAf)
				if(arr.length>0 && isExistPartsBfFlag ==false){
					isExistPartsBfFlag = true
					order_list.queryCostConfigures(1,1)
				}else if(arr.length<=0 && isExistPartsBfFlag ==true){
					isExistPartsBfFlag = false
					order_list.queryCostConfigures(1,1)
				}

			});
			//创建维修方案-添加配件(配件库里没有)----审核结算方案---添加配件(配件库里没有)-ok
			$("html").on('click','.addSelfPart',function(){
				let nameVal=$(this).attr('name');//判断审核结算方案---创建维修方案
				let indexVal=-1;//判断新增-编辑
				let obj={};
				if(nameVal=='cjwxfa'){
					//创建维修方案
					indexVal=$("#checkedPartsSelf .addSelfPart").index($(this));//判断新增-编辑
					obj=partsCheckedCjSelf[indexVal];
				}else if(nameVal=='shjsfa'){
					//审核结算方案
					indexVal=$("#checkedPartsSelfJs .addSelfPart").index($(this));//判断新增-编辑
					obj=partsCheckedJsSelf[indexVal];
				};
				order_list.setPartsSelfVal(indexVal,obj);
				flagInput=false;
				var kh = layer.open({
					type: 1,
					area: ['99%', '99%'],
					title: '添加配件',
					content: $('#AddSelfPartCon'),
					btn: ['保存', '取消'],
					yes: function () {
						let  nums=Math.random()+1;
						let nowNum=Math.pow(10,nums);
						nowNum=parseInt(nowNum);
						let sec=new Date().getTime();
						let partCodeVal=sec.toString().slice(sec.toString().length-6);
						let imgStr='';
						if($("#partNameAdd").val()==''){
							utils.showMessage('请输入配件名称！');
							return;
						}else if($("#partBrand").val()==''){
							utils.showMessage('请输入配件品牌！');
							return;
						}else if($("#brandType").val()==''){
							utils.showMessage('请输入配件型号！');
							return;
						}else if($("#partsFee").val()==''){
							utils.showMessage('请输入配件费！');
							return;
						}else if($("#purchasePrice").val()==''){
							utils.showMessage('请输入采购价！');
							return;
						}else if($("#partsNums").val()==''||Number($("#partsNums").val())==0){
							utils.showMessage('请输入配件数量！');
							return;
						}else if($("#partUnitAdd").val()==''){
							utils.showMessage('请选择配件单位！');
							return;
						}else if($("#uploadImgPart li").length<=0){
							utils.showMessage('请上传图片！');
							return;
						}else{
							let imgArr=[];
							let imgArrKey=[];
							$.each($("#uploadImgPart img"),function(index,value){
								let imgKey=$("#uploadImgPart img").eq(index).attr('name');
								imgArr.push(imgKey);
								let indexNow=imgKey.indexOf('bjx-public');
								imgArrKey.push(imgKey.slice(indexNow));
							});
							imgStr=imgArrKey.join(',');
							let	price=$("#partsFee").val();// 配件费(采购价格+人工费) ,
							let purchasePrice=$("#purchasePrice").val();//采购价
							price=Number(price).toFixed(2);
							purchasePrice=Number(purchasePrice).toFixed(2);
							let obj={
								brandName:$("#partBrand").val(),// (string, optional): 配件品牌 ,
								isCustomParts:1,// 是否自定义配件(1-是，2-不是) ,
								partsName:$("#partNameAdd").val(),//配件名称 ,
								partsType:$("#brandType").val(),// 品牌型号 ,
								picUrl:imgStr,// 配件图片(多个图片以逗号隔开) ,
								price:price,// 配件费(采购价格+人工费) ,
								purchasePrice:purchasePrice,//采购价
								remark :$("#addSelfPartName").val(),// 备注 ,
								partsNum:Number($("#partsNums").val()),//配件数量
								unitName:$("#partUnitAdd option:selected").text(),//配件单位-text
								unit:$("#partUnitAdd").val(),//配件单位-id
								partsCode:partCodeVal+nowNum//配件code随机
							};
							let imgStrPage='';
							$.each(imgArr,function(index,value){
								if(value.indexOf('http://images.baijiaxiu.com/')<0){
									value='http://images.baijiaxiu.com/'+value;
								};
								imgStrPage += '<img  alt="" class="mySelfImg" name="' + value + '" src="' + value + '">'
							});
							let str=`<div>
									<div>配件名称:`+$("#partNameAdd").val()+`</div>
									<div>品牌:`+$("#partBrand").val()+`</div>
									<div>品牌型号:`+$("#brandType").val()+`</div>
								</div>
								<div>
									<div>采购价:`+purchasePrice+`元</div>
									<div>配件费:`+price+`元</div>
									<div>配件数量:`+$("#partsNums").val()+$("#partUnitAdd option:selected").text()+`</div>
								</div>								
								<div>
									<div>配件图片:<br>`+imgStrPage+`</div>
								</div>
								<div>
									<div class="pre">备注说明:`+$("#addSelfPartName").val()+`</div>
								</div>	
								<div class="deletePartOwn" name="del">✖</div>										
								<div class="addSelfPart" name="`+nameVal+`">修改</div>`;
							if(nameVal=='cjwxfa'){
								//创建维修方案
								if(indexVal<0){
									partsCheckedCjSelf.push(obj);
									let strs='<li>'+str+'</li>';
									$("#checkedPartsSelf").append(strs);
								}else{
									partsCheckedCjSelf[indexVal]=obj;
									$("#checkedPartsSelf>li").eq(indexVal).empty().append(str);
								}

								let arr = [];
								arr = partsCheckedCjSelf.concat(partsCheckedCjOwnAf)
								if(arr.length>0 && isExistPartsBfFlag ==false){
									isExistPartsBfFlag = true
									order_list.queryCostConfigures(1,1)
								}else if(arr.length<=0 && isExistPartsBfFlag ==true){
									isExistPartsBfFlag = false
									order_list.queryCostConfigures(1,1)
								}

							}else if(nameVal=='shjsfa'){
								//审核结算方案
								if(indexVal<0){
									partsCheckedJsSelf.push(obj);
									let strs='<li>'+str+'</li>';
									$("#checkedPartsSelfJs").append(strs);
								}else{
									partsCheckedJsSelf[indexVal]=obj;
									$("#checkedPartsSelfJs>li").eq(indexVal).empty().append(str);
								};
								$("#checkedPartsSelfJs").removeClass('noPadding').addClass('yesPadding');
								if($("#MaintettlemeSnt").hasClass('btn-info')){
									order_picture.allowScroll();
								};
							};
							order_list.calculcateTotalFee(nameVal);
							order_clear.clearSelfPart();
							layer.close(kh);
						};
					},
					btn2: function(index, layero) {
						order_picture.allowScroll();
						order_clear.clearSelfPart();
						layer.close(kh);
					},
					cancel: function(index, layero) {
						layer.close(kh);
					}
				});
			});
			//维修方案list-右侧维修方案审批
			$("#maintPlanCon").on('click','.shenpiBtn',function(){
				$("#shenpiStaClick input").prop({'checked':false});
				$("#isAssignStaff input").prop({'checked':false});
				$("#isAssignStaff").hide();
				let btnIndex=$(".shenpiBtn").index(this);
				let idVal=$(this).attr('name');
				let schemeVal=$(this).attr('schemeVal');
				flagInput=false;
				order_list.queryMainClassify(schemeVal);//查询是否创建子工单
				var khh = layer.open({
					type: 1,
					area: ['80%', '75%'],
					title: '审核维修方案',
					content: $('#shenpiCon'),
					btn: ['审核', '取消'],
					yes: function(index, layero) {
						let staVal=$("input[name='checksta']:checked").val();//审批状态
						let AsignStafVal=$("input[name='isAsignStaf']:checked").val();//是否分配协调
						let createVal=$("input[name='isCreate']:checked").val();//创建状态
						if(!staVal){
							utils.showLayerMessage('请选择审批状态！');
							return;
						}else if(staVal==1&&!AsignStafVal){
							utils.showLayerMessage('请选择是否分配协调！');
							return;
						}else if(staVal==1&&isCreateSub==true){
							if(!createVal){
								utils.showLayerMessage('请选择是否创建子工单！');
								return;
							}else if(createVal==1){
								if($("#shenpiChildNote").val().length<=0){
									utils.showLayerMessage('请填写工单内容！');
									return;
								}else if($("#shenpiChildNote").val().length>1000){
									utils.showLayerMessage('工单内容最多可输入1000字！');
									return;
								}
							};
							order_list.updateMaintainPlan(idVal,staVal,btnIndex);
							order_picture.allowScroll();
							layer.close(khh);
						}else{
							order_list.updateMaintainPlan(idVal,staVal,btnIndex);
							order_picture.allowScroll();
							layer.close(khh);
						};
					},
					btn2: function () {
						$("#shenpiStaClick input").prop({'checked':false});
						$("#isAssignStaff input").prop({'checked':false});
						$("#shenpiNote").val('');
						$("#shenpiCon .inputNums em").text('0');
						$("#shenpiChildNote").val('');
						$("#createChildOrder").hide();
						$("#isAssignStaff").hide();
						$(".createChildOrder").hide();
						order_picture.allowScroll();
						layer.close(khh);
					},
					cancel: function () {
						layer.close(khh);
					}
				});
			});
			//创建维修方案-配件库按类型搜索-点击事件-二级
			$("#secondPartsType").on('click','button',function(){
				$("#secondPartsType button").removeClass('btn-info');
				$(this).addClass('btn-info');
				partsSecondIdVal = $(this).attr('name');//创建维修方案-配件-二级类型ID
				order_list.queryOrderParts(partsFirstIdVal,partsSecondIdVal,1);
			});
			//创建维修方案-配件库按类型搜索-点击事件-一级
			$("#firstPartsType").on('click','button',function(){
				$("#firstPartsType button").removeClass('btn-info');
				$(this).addClass('btn-info');
				let idVal=$(this).attr('name');
				partsFirstIdVal=idVal;//创建维修方案-配件-一级类型ID
				partsSecondIdVal=null;
				order_list.queryOrderPartsType(2,idVal);
			});
			//创建维修方案-添加配件-配件库---审核结算方案
			$(".addParts").on('click',function(){
				let nameVal=$(this).attr('name');
				order_list.queryOrderPartsType();
				let showPartData=[];
				if(nameVal=='shjsfa'){
					// 审核结算方案
					$.each(partsCheckedJsOwnBf,function(index,value){
						showPartData.push(value);
					});
					order_list.queryOrderParts('','',2);
				}else if(nameVal=='cjwxfa'){
					// 创建维修方案
					$.each(partsCheckedCjOwnBf,function(index,value){
						showPartData.push(value);
					});
					order_list.queryOrderParts('','',1);
				};
				let strOwnS='';
				$.each(showPartData,function(index,value){
					strOwnS+=`<li>
							<div>`+value.partsName+`</div> 
							<div>`+value.brandName+`</div>
							<div>`+value.partsType+`</div>
							<div>`+value.price+`元/`+value.unitName+`</div>
						</li>`;
				})
				$("#checkedPartsOwn").empty().append(strOwnS);
				flagInput=false;
				var parts = layer.open({
					type: 1,
					area: ['99%', '99%'],
					title: '添加配件',
					content: $('#parts'),
					btn: ['保存', '取消'],
					yes: function () {
						let obj=[];
						if(nameVal=='shjsfa'){
							// 审核结算方案
							partsCheckedJsOwnBf=[];
							$.each(partsCheckedJsOwnAf,function(index,value){
								partsCheckedJsOwnBf.push(value);
								obj.push(value);
							});
						}else if(nameVal=='cjwxfa'){
							// 创建维修方案
							partsCheckedCjOwnBf=[];
							$.each(partsCheckedCjOwnAf,function(index,value){
								partsCheckedCjOwnBf.push(value);
								obj.push(value);
							});
						}
						if(obj.length>0){
							$("#checkedPartsOwnShows").show();
							$("#checkedPartsOwnShowJss").show();
						}else{
							$("#checkedPartsOwnShows").hide();
							$("#checkedPartsOwnShowJss").hide();
						}
						let str=``;
						$.each(obj,function(index,value){
							str+=`<li>
									<div class="b">`+value.partsName+`</div> 
									<div class="a">`+value.brandName+`</div>
									<div class="a">`+value.partsType+`</div>
									<div class="a">`+Number(value.price).toFixed(2)+`元</div>
									<div class="a"><input class="num" type="number" onmousewheel="return false;" min=0 title="oneNumber" id="`+value.id+`" value="`+value.partsNum+`">`+value.unitName+`</div>
									<div class="deletePartOwn" name="del">✖</div>
								</li>`;
						});
						if(nameVal=='shjsfa'){
							// 审核结算方案
							$("#checkedPartsOwnShowJss").show();
							$("#checkedPartsOwnShowJs").empty().append(str);
							if($("#MaintettlemeSnt").hasClass('btn-info')){
								order_picture.allowScroll();
							}
						}else if(nameVal=='cjwxfa'){
							// 创建维修方案
							$("#checkedPartsOwnShows").show();
							$("#checkedPartsOwnShow").empty().append(str);
							let arr = [];
							arr = partsCheckedCjSelf.concat(partsCheckedCjOwnAf)
							if(arr.length>0 && isExistPartsBfFlag ==false){
								isExistPartsBfFlag = true
								order_list.queryCostConfigures(1,1)
							}else if(arr.length<=0 && isExistPartsBfFlag ==true){
								isExistPartsBfFlag = false
								order_list.queryCostConfigures(1,1)
							}
						}
						order_list.calculcateTotalFee(nameVal);
						layer.close(parts);
					},
					btn2: function () {
						if(nameVal=='shjsfa'){
							// 审核结算方案
							partsCheckedJsOwnAf=[];
							$.each(partsCheckedJsOwnBf,function(index,value){
								partsCheckedJsOwnAf.push(value);
							});
							if($("#MaintettlemeSnt").hasClass('btn-info')){
								order_picture.allowScroll();
							}
						}else if(nameVal=='cjwxfa'){
							// 创建维修方案
							partsCheckedCjOwnAf=[];
							$.each(partsCheckedCjOwnBf,function(index,value){
								partsCheckedCjOwnAf.push(value);
							});
						}
						layer.close(parts);
					},
					cancel: function () {
						layer.close(parts);
					}
				});
			});
			//订单搜索---获取二级工单类型
			$("#firstGrade").on('change',function(){
				let valNow=$(this).val();
				if(valNow!=''){
					order_list.getFirstClassName(2,1,valNow)
				}else{
					let str = "<option value=''>二级工单类型</option>";
					$("#secondGrade").empty().append(str);
				}
			});
			//转派供应商-二，三级
			$("#firstOrderName").on('change',function(){
				let valNow=$(this).val();
				if(valNow!=''){
					order_list.getFirstClassName(2,3,valNow)
				}else{
					let str = "<option value=''>二级工单类型</option>";
					$("#secondOrderName").empty().append(str);
				}
			});
			$("#secondOrderName").on('change',function(){
				let valNow=$(this).val();
				if(valNow!=''){
					order_list.getFirstClassName(3,3,valNow)
				}else{
					let str = "<option value=''>三级工单类型</option>";
					$("#thirdOrderName").empty().append(str);
				}
			});
			//创建租务工单---获取二级类型
			$("#firstIdZw").on('change',function(){
				let valNow=$(this).val();
				if(valNow!=''){
					order_list.queryRentClassify(2);
				}else{
					$("#secondIdZw").empty();
				}
			});
			//创建关联维修单---获取二级工单类型
			$("#firstGradeRel").on('change',function(){
				let valNow=$(this).val();
				if(valNow!=''){
					order_list.getFirstClassName(2,2,valNow)
				}else{
					let str1 = "<option value=''>二级工单类型</option>";
					let str2 = "<option value=''>三级工单类型</option>";
					$("#secondGradeRel").empty().append(str1);
					$("#thirdGradeRel").empty().append(str2);
				}
			});
			//创建关联维修单---获取三级工单类型
			$("#secondGradeRel").on('change',function(){
				let valNow=$(this).val();
				if(valNow!=''){
					order_list.getFirstClassName(3,2,valNow)
				}else{
					let str = "<option value=''>三级工单类型</option>";
					$("#thirdGradeRel").empty().append(str);
				}
			});
			//维修方案协调中-转派维修员---获取二级工单类型
			$("#firstOrderNameZpwxy").on('change',function(){
				let valNow=$(this).val();
				if(valNow!=''){
					order_list.getFirstClassName(2,4,valNow)
				}else{
					let str1 = "<option value=''>二级工单类型</option>";
					let str2 = "<option value=''>三级工单类型</option>";
					$("#secondOrderNameZpwxy").empty().append(str1);
					$("#thirdOrderNameZpwxy").empty().append(str2);
				}
			});
			//维修方案协调中-转派维修员---获取三级工单类型
			$("#secondOrderNameZpwxy").on('change',function(){
				let valNow=$(this).val();
				if(valNow!=''){
					order_list.getFirstClassName(3,4,valNow)
				}else{
					let str = "<option value=''>三级工单类型</option>";
					$("#thirdOrderNameZpwxy").empty().append(str);
				}
			});
			//公寓地址-select
			$("#detailAddress").on("click","div",function(){
				let str=$(this).html();
				$("#apartAdress").val(str);
				selectAdressId=isNaN(parseInt($(this).attr('title')))?null:parseInt($(this).attr('title'));
				$("#detailAddress").hide();
			});
			//table下面detail-操作
			$("#AllBtnAction").on('click',"span",function(){
				$('#AllBtnAction span').removeClass('btn-info');
				$(this).addClass("btn-info");
				$("#contanerDetail>div").hide();
				let msgSta=$(this).attr('id');
				if(msgSta=='workOrderDetail'){
					order_list.getWorkDetail('');
				}
				if(msgSta=='configInfo'){
					order_list.queryConfigList();
				}
				if(msgSta=='housrSouce'){
					$("#housrSouceCon").show();//房源信息
				}
				if(msgSta=='followUpRecord'){
					order_list.getGjRecord();//获取跟进记录请求
				}
				if(msgSta=='contactResult'){
					$("#tableGqRecord").empty();
					$("#gqCallRecordCon").empty()
					order_list.getGaiqiRecord();//改期记录
					order_list.getCallRecord();//通话记录
					$("#contactResultCon").show();//改期记录
				}
				if(msgSta=='authorizationList'){
					$("#authorizationListCon").hide();//授权列表
					order_list.getAuthHis();
				}
				if(msgSta=='maintPlan'){
					$("#maintPlanCon").empty().hide();//维修方案
					order_list.queryMaintainPlan()
				}
				if(msgSta=='MaintCoordRecord'){
					$("#MaintCoordRecordCon").hide().empty();//维修协调记录
					order_list.queryMaintainPlanChecked();
				}
				if(msgSta=='rentralist'){
					$("#rentralistCon").hide();//租务工单
					order_list.queryOrderTema();
				}
				if(msgSta=='repairSheet'){
					$("#repairSheetCon").show();//补配工单
				}
				if(msgSta=='repairlist'){
					$("#repairlistCon").hide();//报修工单
					if(linkedNo!=''){
						order_list.getWorkDetail(linkedNo);
					}else{
						utils.showLayerMessage('工单正在创建中，请耐心等待!');
						return;
					}
				}
				if(msgSta=='MaintettlemeSnt'){
					partsCheckedJsOwnAf=[];
					partsCheckedJsOwnBf=[];
					partsCheckedJsSelf=[];
					idPlanJs='';
					order_list.queryOrderAccount('shjsfaBtn');
				}
				if(msgSta=='quitDetail'){
					$("#quitDetailCon").hide();//退单详情
					order_list.getTuiCheDetail(13);
				}
				if(msgSta=='backDetail'){
					$("#quitDetailCon").hide();//撤单详情
					order_list.getTuiCheDetail(14);

				}
				if(msgSta=='actRecord'){
					$("#actRecordCon").show();//操作记录---暂时不用
				}
			});
			//搜索栏-下面-状态按钮操作
			$("#seBtnAlls button").on("click",function(){
				$("#MainPlanCon").val('');
				$("#MainPlan").val('');
				let nameVal=$(this).attr('name');
				$(this).find(".green").eq(0).hide();
				$("#seBtnAlls button").removeClass('btn-info');
				$(this).addClass('btn-info');
				searchStaFlag=false;
				nameVal=parseInt(nameVal);
				progressValSear=nameVal;
				progressValSears=progressValSear;
				if(progressValSear==6){
					$("#wxfaxtzPlanStaCon").show();
				}else{
					$("#wxfaxtzPlanStaCon").hide();
					$("#wxfaxtzPlanSta").val('');
				};
				order_list.getServerData(1);
			});
			//审核维修结算
			$("#shwxjsBtn").on('click',"button",function(){
				order_list.shjsfaFn(2);//审核结算方案
			});
			//跟进记录-跟进
			$("#followUpRecordCon").on('click',"#genjins",function(){
				order_list.genjinFn();//跟进
			});
			//工单详情-不同按钮(操作)-对应fn
			$("#btnACtTrans").on('click',"button",function(){
				$("#btnACtTrans button").removeClass('btn-info');
				$(this).addClass('btn-info');
				$("#bomBtnCon>div").hide();
				let strTitle='';
				let strCon=null;
				let msgSta=$(this).attr('id');
				selectIdStaBtn=msgSta;
				let w='100%';
				let h='100%';
				if(msgSta=='tuidan'){
					w='75%';
					h='60%';
					strTitle='退单';
					strCon=$("#tuidanCon");//退单
					order_list.queryOrderReason(13);
				};
				if(msgSta=='chedan'){
					w='75%';
					h='60%';
					strTitle='撤单';
					strCon=$("#chedanCon");//撤单
					order_list.queryOrderReason(14);
				};
				if(msgSta=='contact'){
					order_clear.clearContact();
					w='75%';
					h='80%';
					strTitle='联系';
					strCon=$("#contactCon");//联系
					order_list.queryChangeReason();//获取改期原因
				};
				if(msgSta=='gaiqi'){
					w='75%';
					h='80%';
					strTitle='改期';
					strCon=$("#gaiqiCon");// 改期
					order_list.queryChangeReason();//获取改期原因
				};
				if(msgSta=='fenpei'){
					w='75%';
					h='60%';
					strTitle='分配维修员';
					strCon=$("#fenpeiCon");//分配维修员
					order_list.queryRepairman(1);
				};
				if(msgSta=='shangmen'){
					strTitle='上门';
					utils.showConfirm('您将进行上门操作？', '确定', function() {
						order_list.shangmenFn();//上门
					});
					return;
				};
				if(msgSta=='zpwxy'){
					$("#zpwxyCon .contactBtnShows").hide();
					strTitle='转派维修员';
					strCon=$("#zpwxyCon");//转派维修员
					$("#notefs").show();
					if(selectIdSta==6){
						w='75%';
						h='90%';
						order_list.queryProvider('zpwxy');
						$("#zpwxyCon .contactBtnShow").show();
					}else{
						w='75%';
						h='90%';
						$("#zpwxyCon .contactBtnShow").hide();
					};
					order_list.queryRepairManZp();
				};
				if(msgSta=='zpfswxy'){
					$("#notefs").hide();
					$("#zpwxyCon .contactBtnShows").hide();
					w='75%';
					h='60%';
					strTitle='转派防水师傅';
					strCon=$("#zpwxyCon");//转派防水师傅维修员
					$("#zpwxyCon .contactBtnShow").hide();
					order_list.queryRepairManZp();
				};
				if(msgSta=='cjwxfa'){
					$("#shangmenCost").parent('div').find('em').eq(0).text('');
					$("#jiajiCost").parent('div').find('em').eq(0).text('');
					$("#otherCost").parent('div').find('em').eq(0).text('');
					order_list.getConfigServiceItems();
					visitFeeSelected='';//上门费用-selected
					urgentFeeSelected='';//紧急费用-selected
					otherFeeSelected='';//其他费用-selected
					MatinPlanTemListName='';
					w='98%';
					h='98%';
					judgeReasonArr = []; //判断原因归属方案是否配置
					judgePlanArr = []; //判断方案归属方案是否配置
					order_clear.clearCjwxfa();
					partsCheckedCjSelf=[];//创建维修方案-配件-已选择配件---自己添加---修改后
					partsCheckedCjOwnAf=[];//创建维修方案-配件-已选择配件---配件库---修改后
					partsCheckedCjOwnBf=[];//创建维修方案-配件-已选择配件---配件库---修改前
					isExistPartsBfFlag = false; //默认无配件
					strTitle='创建维修方案';
					strCon=$("#cjwxfaCon");//创建维修方案
					let str="";
					if(exigencyFlag==2){
						$("#jiajiCost").attr("disabled",true);
					}else{
						$("#jiajiCost").attr("disabled",false);
					};
					order_list.queryMatinClassify(1,1);
					if(selectIdSta==6){
						order_list.queryNewestMPlan(3)
					}else{
						order_list.querySuiteArea(1)
						order_list.queryMatinClassify(1,2,2)
						order_list.getReasonMatinplanList(1,secondGradecreate)
						order_list.queryMatinPlanList();//获取维修方案模板
						order_list.queryCostConfigures(1);
					};
				};
				if(msgSta=='fqbxd'){
					$("#awPeopleEdit").val('');
					$("#awPeopleTel").val('');
					$("#initiateRepairNameRel").val('');
					$("#initiateRepairTelRel").val('');
					$("#fqbxdCon .hidess").hide();
					$("#exigencyFlagRel").val('');
					$("#appointmentDateRel").val('');
					$("#appointmentTimeRel").val('');
					w='75%';
					h='90%';
					strTitle='创建关联维修单';
					// order_list.queryOrderTimeRange(1);//获取时间段
					strCon=$("#fqbxdCon");//创建关联维修单
				};
				if(msgSta=='cjzwgd'){
					$("#awPeopleEdit").val('');
					$("#awPeopleTel").val('');
					w='75%';
					h='90%';
					strTitle='创建租务工单';
					strCon=$("#cjzwgdCon");//创建租务工单
					order_list.queryRentClassify(1);
					order_list.queryPoliceTag();
				};
				if(msgSta=='zpgys'){
					$("#zpwxyCon .contactBtnShows").hide();
					$("#gaiqiDateConZp").val('');
					$("#gaiqiTimeConZp").val('');
					w='75%';
					h='90%';
					strTitle='转派供应商';
					strCon=$("#zpgysCon");//转派供应商
					order_list.queryProvider('zpgys');
					order_list.getFirstClassName(1)
				};
				if(msgSta=='shjsfa'){
					reasonBelongIdArrOrgin = [];//结算待审核，查询结算方案时的原因归属保留的id
					planBelongIdArrOrgin = [];//结算待审核，查询结算方案时的方案归属保留的id
					judgeReasonArr = []; //判断原因归属方案是否配置
					judgePlanArr = []; //判断方案归属方案是否配置
					order_clear.clearShjsfa();
					w='90%';
					h='90%';
					partsCheckedJsOwnAf=[];
					partsCheckedJsOwnBf=[];
					partsCheckedJsSelf=[];
					idPlanJs='';
					order_list.queryOrderAccount('shjsfaCon');
					// order_list.queryWorkerFeedbackInfo();
					// order_list.queryNinetyReWorkRecord();
					strTitle='审核结算方案';
					strCon=$("#shjsfaCon");//审核结算方案
					$("#shjsfaCon .titleT").eq(0).hide();
					$("#urgentMoneyJs").attr("disabled",false);
					$("#shwxjsBtn").empty().hide();
				};
				if(msgSta=='yjzb'){
					timeNow = 5;
					timeoutFlag = true;
					matinSchemeArr=[];
					w='75%';
					h='90%';
					strTitle='一键转办';
					strCon=$("#yjzbCon");//一键转办
					order_list.queryNewestMPlan(2);//查询最新维修方案
				};
				flagInput=false;
				ConFlag=-1;
				ConFlag = layer.open({
					type: 1,
					area: [w, h],
					title: strTitle,
					content: strCon,
					btn: ['确定', '取消'],
					yes: function(index, layero) {
						if(msgSta=='fenpei'){
							order_list.fenpeiFn();//分配
						};
						if(msgSta=='zpwxy'){
							order_list.zpwxyFn();//转派师傅/维修员
						};
						if(msgSta=='zpfswxy'){
							order_list.zpfswxyFn();//转派防水师傅/维修员
						};
						if(msgSta=='contact'){
							order_list.contactFn();//联系
						};
						if(msgSta=='gaiqi'){
							order_list.gaiqiFn();// 改期
						};
						if(msgSta=='cjwxfa'){
							order_list.cjwxfaFn();//创建维修方案
						};
						if(msgSta=='zpgys'){
							order_list.zpgysFn();//转派供应商
						};
						if(msgSta=='tuidan'){
							order_list.tuidanFn();//退单
						};
						if(msgSta=='chedan'){
							order_list.tuidanFn();//撤单
						};
						if(msgSta=='fqbxd'){
							order_list.fqbxdFn();//创建关联维修单
						};
						if(msgSta=='cjzwgd'){
							order_list.cjzwgdFn();//创建租务工单
						};
						if(msgSta=='shjsfa'){
							order_list.shjsfaFn(1);//审核结算方案
						};
						if(msgSta=='yjzb'){
							order_list.yjzbFn();//一键转办
						};
					},
					btn2: function(index, layero) {
						if(msgSta=='shangmen'){
							//上门
						};
						if(msgSta=='fenpei'){
							order_clear.clearFenpei();//分配
						};
						if(msgSta=='zpwxy'||msgSta=='zpfswxy'){
							$("#zpwxySupply").val('');
							order_list.refreshRepairman(2);
							order_clear.clearZpwxy();//转派师傅/维修员
						};
						if(msgSta=='contact'){
							order_clear.clearContact();//联系
						};
						if(msgSta=='gaiqi'){
							order_clear.clearGaiqi();// 改期
						};
						if(msgSta=='cjwxfa'){
							partsTotals=0;
							judgeReasonArr = []; //判断原因归属方案是否配置
							judgePlanArr = []; //判断方案归属方案是否配置
							order_clear.clearCjwxfa();
							//创建维修方案
						};
						if(msgSta=='zpgys'){
							order_clear.clearApgys();//转派供应商
						};
						if(msgSta=='tuidan'||msgSta=='chedan'){
							order_clear.clearTuiCheDan();//退单,撤单
						};
						if(msgSta=='fqbxd'){
							order_clear.clearFqbxd();//创建关联维修单
						};
						if(msgSta=='cjzwgd'){
							order_clear.clearCjzwgd();//创建租务工单
						};
						if(msgSta=='shjsfa'){
							reasonBelongIdArrOrgin = [];//结算待审核，查询结算方案时的原因归属保留的id
							planBelongIdArrOrgin = [];//结算待审核，查询结算方案时的方案归属保留的id
							judgeReasonArr = []; //判断原因归属方案是否配置
							judgePlanArr = []; //判断方案归属方案是否配置
							order_clear.clearShjsfa();
							$("#shjsfaCon .inputNums>em").text('0');
							$("#checkRemarkJs").val('');//审核结算方案
						};
						if(msgSta=='yjzb'){
							order_clear.clearYjzbFn();//清除一键转办
							order_list.refreshRepairman(7);
						};
						order_picture.allowScroll();
					},
					cancel: function(index, layero) {
						layer.close(ConFlag);
					}
				});
			});
			//创建关联报修单---联系人选择
			$("#linkPeople").on('change',function(){
				let nameVal=$("#linkPeople option:selected").text();
				let idVal=$("#linkPeople option:selected").val();
				let telVal=$("#linkPeople option:selected").attr('name');
				if(idVal!=''){
					$("#initiateRepairNameRel").val(nameVal);//报修人姓名
					$("#initiateRepairTelRel").val(telVal);//报修人电话
				}else{
					$("#initiateRepairNameRel").val('');//报修人姓名
					$("#initiateRepairTelRel").val('');//报修人电话
				};
			});
			//创建租务工单---联系人选择
			$("#awPeople").on('change',function(){
				let nameVal=$("#awPeople option:selected").text();
				let idVal=$("#awPeople option:selected").val();
				let telVal=$("#awPeople option:selected").attr('name');
				if(idVal!=''){
					$("#awPeopleEdit").val(nameVal);//联系人姓名
					$("#awPeopleTel").val(telVal);//联系人电话
				}else{
					$("#awPeopleEdit").val('');//联系人姓名
					$("#awPeopleTel").val('');//联系人电话
				};
			});
			//回复聊天
			$("body").off('click').on('click','.planReply',function(){
				let idVal=$(this).attr('name');//方案ID
				let indexVal=$(this).attr('data-index');//内容下标
				let textVal=$("#maintPlanCon .chat").eq(indexVal).val();//回复内容
				if(textVal==''){
					utils.showLayerMessage('请填写回复内容！');
					layerFlag=false;
					return;
				};
				if(layerFlag==true){
					return;
				};
				layerFlag=true;
				let datas={
					user_id: win.utils.getCookie('user_id'),//用户id
					session: win.utils.getCookie('session'),//session
					orderId:selectId,// 维修工单id ,
					commContent:textVal,//评论内容 ,
					// 					commRole (string, optional): 评论人角色(调度、结算、维修员) ,
					// 					commRoleId (string, optional): 评论人角色id ,
					// 					commUserName (string, optional): 评论人名称 ,
					maintainPlanId :parseInt(idVal),// 维修方案ID ,
					// 					replyRole (string, optional): 回复人角色(调度、结算、维修员) ,
					// 					replyUid (string, optional): 回复人ID ,
					// 					replyUserName (string, optional): 回复人名称 ,
				};
				//判断是否首次回复
				if($("#maintPlanCon .chatRecord").eq(indexVal).find('ul li').length>0){
					let str=$("#maintPlanCon .chatRecord").eq(indexVal).attr('name');
					let transStr=str.replace(/!/g,'"');
					transStr='{'+transStr+'}'
					let obj=JSON.parse(transStr);//console.log(obj,"obj===");
					datas.replyRole=obj.replyRole;
					datas.replyUid=obj.replyUid;
					datas.replyUserName=obj.replyUserName;
				};
				win.utils.ajaxPost(win.utils.services.orderMan_order_createCoordinationRecord, datas, function(result) {
					if(result.result_code == 200) {
						layerFlag=false;
						$("#maintPlanCon .chat").eq(indexVal).val(" ");
						$("#maintPlanCon .inputNums em").eq(0).text(0);
						order_list.queryCoordinationRecord(idVal,indexVal);
					} else {
						utils.showLayerMessage('创建失败，请稍后重试！');
					}
				});
			});
		},
		//初始化请求
		initList: function() {
			//订单列表
			$("#table").jqGrid({
				url: '',
				styleUI: 'Bootstrap',
				datatype: "json",
				height:'auto',//utils.getAutoGridHeightSong(),
				autowidth: true,
				shrinkToFit:false,
				autoScroll: true,
				rowNum: 10,
				rownumbers: true,
				rownumWidth:80,
				// rowList: [10,20,50,100],
				pgtext: "第X页 / 共X页",
				colNames: ["工单ID",'方案状态','反馈次数',"紧急","蛋壳工单ID","进度状态","超时状态","城市","行政区/商圈","小区","公寓地址","报修人/电话","报修类型","报修描述","维修员/电话","预约上门时间","上门改期","完成时间","关联工单"],
				colModel: [
					{
						name: "orderId",
						index: "orderId",
						editable: false,
						width: 240,
						hidden:true,
						sorttype: "string",
					},
					{
						name: "checkStatus",
						index: "checkStatus",
						editable: false,
						width: 20,
						hidden:true,
					},
					{
						name: "feedbackNo",
						index: "feedbackNo",
						editable: false,
						width: 10,
						hidden:true,
					},
					{
						name: "exigencyFlag",
						index: "exigencyFlag",
						editable: false,
						width: 180,
						hidden:true,
						sorttype: "string",
					},
					{
						name: "outOrderNo",
						index: "outOrderNo",
						editable: false,
						width: 150,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							// 是否催单:1-催单；2-不催单
							let staVal = Number(rowObject.orderStatus);
							let reminderVal = '';
							if(staVal==3||staVal==4 ){
								if(Number(rowObject.reminder)==1){
									reminderVal ='<div class="reminderbg"></div><div class="remindertext">催单</div>';
								}
							};
							let str="";
							if(parseInt(rowObject.exigencyFlag)==1){
								str= '</br><b class="red">紧急工单</b>';
							};
							return cellvalue+reminderVal+str;
						},
					},
					{
						name: "orderStatus",
						index: "orderStatus",
						editable: false,
						width: 70,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							///反馈次数feedbackNo;
							cellvalue=parseInt(cellvalue);
							let staVal='';
							let feedbackNoVal='';
							if(cellvalue==1){
								return '待认领';
							}else if(cellvalue==2){
								return '待分配';
							}else if(cellvalue==3){
								return '待联系'
							}else if(cellvalue==4){
								return '待上门';
							}else if(cellvalue==5){
								return '已上门待反馈';
							}else if(cellvalue==6){
								let statusVal=rowObject.checkStatus;
								let feedbackNos=rowObject.feedbackNo;//反馈次数
								if(statusVal==1){//1、方案通过，2、方案不通过，3、待审批) ,
									staVal='';
								}else if(statusVal==2){
									staVal='<b class="normalTwo">驳回</b>';
								}else if(statusVal==3){
									staVal='<b class="normalThree">待审核</b>';
								}else{
									if(newestChecksta==1){//1、方案通过，2、方案不通过，3、待审批) ,
										staVal='';
									}else if(newestChecksta==2){
										staVal='<b class="normalTwo">驳回</b>';
									}else if(newestChecksta==3){
										staVal='<b class="normalThree">待审核</b>';
									}else{
										staVal='';
									};
								};
								// if(statusVal==2||newestChecksta==2){
								// 	feedbackNoVal='';
								// }else{
								//
								// };
								//需求变更reason---
								//08-23---他们说驳回不需要
								//11-05---他们需要驳回展示
								if(feedbackNos&&feedbackNos>1){
									feedbackNoVal='<b class="fedbackNum">反馈*'+feedbackNos+'</b>';
								}else if(feedbackNo&&feedbackNo>1){
									feedbackNoVal='<b class="fedbackNum">反馈*'+feedbackNo+'</b>';
								}else{
									feedbackNoVal='';
								}
								return '维修方案协调中'+staVal+feedbackNoVal;
							}else if(cellvalue==7){
								return '已完成';
							}else if(cellvalue==8){
								return '未完成';
							}else if(cellvalue==9){
								return '结算待审核';
							}else if(cellvalue==10){
								return '结算审核通过';
							}else if(cellvalue==11){
								return '已评价';
							}else if(cellvalue==12){
								return '已撤销';
							}else if(cellvalue==13){
								return '退单中';
							}else if(cellvalue==14){
								return '撤单中';
							}else if(cellvalue==15){
								return '已退单';
							}else if(cellvalue==16){
								return '已转派供应商';
							}else{
								return '待跟进';
							}
						},
					},
					{
						name: "timeoutOrder",
						index: "timeoutOrder",
						editable: false,
						width: 100,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							let strs="";
							for(let j in cellvalue){
								if(j=='1'){
									if(cellvalue[j]!=0){
										strs+= '<b class="red">分配超时*'+cellvalue[j]+'</b><br>';
									};
								};
								if(j=='2'){
									if(cellvalue[j]!=0){
										strs+= '<b class="red">联系超时*'+cellvalue[j]+'</b><br>';
									}
								};
								if(j=='3'){
									if(cellvalue[j]!=0){
										strs+= '<b class="red">上门超时*'+cellvalue[j]+'</b><br>';
									}
								};
								if(j=='4'){
									if(cellvalue[j]!=0){
										strs+= '<b class="red">反馈超时*'+cellvalue[j]+'</b><br>';
									}
								};
								if(j=='5'){
									if(cellvalue[j]!=0){
										strs+= '<b class="red">配件超时*'+cellvalue[j]+'</b>';
									}
								};
								if(j=='6'){
									if(cellvalue[j]!=0){
										strs+= '<b class="yellow">联系即将超时</b>';
									}
								};
							};
							if(strs==''){
								strs="--";
							};
							return strs;
						},
					},
					{
						name: "city",
						index: "city",
						editable: false,
						width: 80,
						sorttype: "string"
					},
					{
						name: "tradingArea",
						index: "tradingArea",
						editable: false,
						width: 150,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							return rowObject.district+'—'+cellvalue
						},
					},
					{
						name: "blockName",
						index: "blockName",
						editable: false,
						width: 150,
						sorttype: "string",
					},
					{
						name: "detailAddress",
						index: "detailAddress",
						editable: false,
						width: 200,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							return   '#'+rowObject.houseId+"  "+cellvalue
						},
					},
					{
						name: "initiateRepairName",
						index: "initiateRepairName",
						editable: false,
						width: 150,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							return cellvalue+'</br>'+rowObject.initiateRepairTel
						},
					},
					{
						name: "thirdGradeName",
						index: "thirdGradeName",
						editable: false,
						width: 120,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							if(!cellvalue){
								cellvalue='';
							};
							return rowObject.firstGradeName+"-"+rowObject.secondGradeName+"-"+cellvalue;
						},
					},
					{
						name: "faultDescriptionUser",
						index: "faultDescriptionUser",
						editable: false,
						width: 300,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							let z='';
							if(!cellvalue){
								cellvalue='';
							}else{
								z='租户描述：'+cellvalue+'</br>';
							}
							let c='';
							if(!rowObject.faultDescriptionCuse){
								rowObject.faultDescriptionCuse='';
							}else{
								c='客服描述：'
							};
							return  '<div class="pre">'+z+c+rowObject.faultDescriptionCuse+'</div>'
						},
					},
					{
						name: "workerName",
						index: "workerName",
						editable: false,
						width: 150,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							let stra='';let strb='';
							if(cellvalue==undefined){
								stra="--";
							}else{
								if(cellvalue.indexOf('selfSong')<0){
									let indexVal=cellvalue.indexOf('(');
									if(indexVal>-1){
										stra=cellvalue.slice(0,indexVal);
									}else{
										stra=cellvalue;
									}
								}else{
									let indexVal=cellvalue.indexOf('selfSong');
									let endVal=cellvalue.indexOf('(');
									let nextStartVal=cellvalue.indexOf(')');
									if(indexVal>-1){
										stra=cellvalue.slice(8,endVal)+cellvalue.slice(nextStartVal+1);
									}else{
										stra=cellvalue;
									}
								};
							};
							if(rowObject.workerTel==undefined){
								strb="";
							}else{
								strb="</br><em class='textColor'>"+rowObject.workerTel+"</em>";
							};
							return stra+strb;
						},
					},
					{
						name: "isChangeDate",
						index: "isChangeDate",
						editable: false,
						width: 120,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							// 改期-1，正常-2
							if(cellvalue==1){
								return rowObject.changeDay+"<br>"+rowObject.changeTimes;
							}else if(cellvalue==3||cellvalue==4){//contact-gaiqi
								return changeDateCom+"<br>"+changeTimeCom;
							}else{
								return rowObject.appointmentDate+"<br>"+rowObject.appointmentTime;
							};
						},
					},
					{
						name: "changeDateMap",
						index: "changeDateMap",
						editable: false,
						width: 92,
						sorttype: "string",
						formatter: function(cellvalue,options, rowObject) {
							let strVal='';
							for(let mm in cellvalue){
								if(mm=='1'){
									strVal+= '<b class="normalTwo">改期上门*'+cellvalue[mm]+'</b><br>';
								}else if(mm=='2'){
									strVal+= '正常上门';
								};
							};
							if(strVal==''){
								strVal="--";
							};
							return strVal;
						}
					},
					{
						name: "practicalFinishTime",
						index: "practicalFinishTime",
						editable: false,
						width: 100,
						sorttype: "string",
					},
					{
						name: "linkedNo",
						index: "linkedNo",
						editable: false,
						width: 200,
						sorttype: "string",
					},
				],
				pager: "#table_pager",
				viewrecords: true,
				pagerpos: "center",
				recordpos: "left",
				caption: "",
				hidegrid: false,
				pgbuttons:true,
				pginput:true,
				onPaging: function(pgButton) {
					deferLoadData = 0;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					lastPage = $("#table").jqGrid('getGridParam', 'lastpage');
					if(pgButton == 'next') {
						currentPage = currentPage + 1;
					}
					if(pgButton == 'last') {
						currentPage = lastPage;
					}
					if(pgButton == 'prev') {
						currentPage = currentPage - 1;
					}
					if(pgButton == 'first') {
						currentPage = 1;
					}
					if(pgButton == 'user') {
						deferLoadData = 1;
					}
					if(pgButton == 'records') {
						deferLoadData = 1;
					}
					if(deferLoadData == 0) {
						order_list.getServerData(currentPage);
					}
				},
				beforeRequest: function() {
					if(deferLoadData == 1) {
						order_list.getServerData(currentPage);
						deferLoadData = 0;
					}
				},
				onSortCol: function(index, iCol, sortorder) {
					orderByString = index + ' ' + sortorder;
					currentPage = $("#table").jqGrid('getGridParam', 'page');
					order_list.getServerData(currentPage);
				},
				//当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
				onSelectRow: function () {
					clearAllDataNow();
					layerFlag=false;
					$("#contanerDetail>div").hide();//展示信息-隐藏
					//返回选中的id
					selectId = $("#" + this.id).getGridParam('selrow');
					$("#selfOrderId").html(selectId);
					let rowData = $("#table").jqGrid("getRowData", selectId);
					exigencyFlag=rowData.exigencyFlag;
					feedbackNo=rowData.feedbackNo?rowData.feedbackNo:0;
					if(selectId){
						if(selectId!=''){
							let rowData = $("#table").jqGrid("getRowData", selectId);
							let indexVal=rowData.isChangeDate.indexOf('<br>');
							changeDateCom=rowData.isChangeDate.slice(0,indexVal);//预约日期
							changeTimeCom=rowData.isChangeDate.slice(indexVal+4);//预约时间段
							//联系-show
							$("#contactOrderDate").html(changeDateCom);//预约日期
							//改期
							$("#gaiqiDates").html(changeDateCom);//预约日期
							order_list.getWorkDetail('');
						}else{
							order_list.setWorkDetailData('','','');
						}
					}else{
						$("#showBtnConNav").hide();
					}
				},
				gridComplete: function() {
					$("#table").jqGrid('resetSelection');
					$("#table").jqGrid('setSelection',selectId);
				}
			});
			$("#table").jqGrid("navGrid", "#table_pager", {
				edit: false,
				add: false,
				del: false,
				search: false,
				refresh: false,
			});
			order_list.queryUserCity();//获取登录用户--管理城市id---after-请求列表（城市对应下的订单）
			order_list.getFirstClassName(1);
			order_list.getServiceScheme();
			order_list.queryRepairman(2);
			order_list.queryPartUnit();//查询配件单位
			$(window).bind("resize", function () {
				var b = $(".jqGrid_wrapper").width();
				if(b>=1844){
					b=1844;
				};
				$("#table").setGridWidth(b);
			});
		},
		refreshServerData: function() {
			order_list.getServerData(1);
		},
		clearSearchVal:function(){
			$.each($("#searchTop input"),function(index,value){
				$("#searchTop input").eq(index).val('');
			});
			$.each($("#searchTop select"),function(index,value){
				$("#searchTop select").eq(index).val('');
			});
			$.each($("#formSear input"),function(index,value){
				$("#formSear input").eq(index).val('');
			});
			$.each($("#formSear select"),function(index,value){
				$("#formSear select").eq(index).val('');
			});
			$("#isFinishFlagSear").val('25');// 工单是否完结
		},
		// 获取维修方案模板-list
		queryMatinPlanList : function(type) {
			win.utils.ajaxPost(win.utils.services.orderMan_config_listFeedbackTem, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				status :1
			}, function(result) {
				if(result.result_code == 200) {
					MatinPlanTemList=result.lists;
					let str="<option value=''>请选择维修方案模板</option>";
					let strs='';
					let sel='';
					let selFlag=false;
					$.each(MatinPlanTemList,function(index,value){
						sel='';
						if(MatinPlanTemListName == value.name){
							sel='selected';
							selFlag=true;
						}
						strs+="<option "+sel+" value='"+value.id+"' name='"+index+"'>"+value.name+"</option>"
					});
					if(selFlag==true){
						str="<option value=''>请选择维修方案模板</option>";
					}else{
						str="<option selected value=''>请选择维修方案模板</option>";
					};
					str=str+strs;
					$("#feedBackMaintenPlanId").empty().append(str);
				} else {
					let msg= result.description?result.description:'获取数据失败，请重试！';
					utils.showMessage(msg);
				}
			});
		},
		//根据id获取对应方案模板 --- api:orderMan_config_getFeedbackTem
		getFeedbackTem:function(idVal){
			win.utils.ajaxPost(win.utils.services.orderMan_config_getFeedbackTem, {
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				id :idVal
			}, function(result) {
				if(result.result_code == 200) {
					order_list.setFeedbackTem(result.data)
				}else{
					let msg= result.description?result.description:'获取维修方案内容失败，请重试！';
					utils.showMessage(msg);
				}
			});
		},
		//根据id展现对应方案模板
		setFeedbackTem:function(obj){
			partsCheckedCjOwnAf=[];//创建维修方案-配件-已选择配件---配件库---修改后
			partsCheckedCjOwnBf=[];//创建维修方案-配件-已选择配件---配件库---修改前
			if(obj){
				let nowPlanParts=obj.accountParts?obj.accountParts:[];// (Array[PlanPartsDTO]: 配件信息列表 ,
				let faultCause=obj.faultCause?obj.faultCause:'';//故障原因 ,
				let id=obj.id?obj.id:'';//id ,
				let projectDesc=obj.templateProjectDesc?obj.templateProjectDesc:'';//维修方案描述说明 ,
				let servicePlanId=obj.servicePlanId?obj.servicePlanId:'';//协调方案id ,
				$("#faultCause").val(faultCause);//---
				$("#faultCause").parent('div').find(".inputNums>em").text(faultCause.length);//---
				$("#projectDescBl").html(projectDesc);//---
				let textlen = $("#projectDescBl").text().length
				$("#projectDescBl").parent('div').find(".inputNums>em").eq(0).text(textlen);//---
				$("#servicePlanId").val(servicePlanId);//---
				let strOwn='';//配件库拥有
				if(nowPlanParts.length>0){ //isCustomParts=2,确认配件库
					$.each(nowPlanParts,function(index,value){
						partsCheckedCjOwnAf.push(value);//创建维修方案-配件-已选择配件---配件库---修改后
						partsCheckedCjOwnBf.push(value);//创建维修方案-配件-已选择配件---配件库---修改前
						strOwn+=`<li>
						<div class="b">`+value.partsName+`</div> 
						<div class="a">`+value.brandName+`</div>
						<div class="a">`+value.partsType+`</div>
						<div class="a">`+Number(value.price).toFixed(2)+`元/`+value.unitName+`</div>
						<div class="a"><input class="num" type="number" min=0 title="oneNumber" id="`+value.id+`" value="`+value.partsNum+`">`+value.unitName+`</div>
						<div class="deletePartOwn" name="del">✖</div>
					</li>`;
					});//配件信息列表
				};
				if(partsCheckedCjOwnAf.length>0){
					$("#checkedPartsOwnShows").show()
				}else{
					$("#checkedPartsOwnShows").hide()
				};
				$("#checkedPartsOwnShow").empty().append(strOwn); //配件库选择
				order_list.calculcateTotalFee('cjwxfa');
			}
		},
		//进入订单页面后---获取登录用户-管理城市#id
		queryUserCity:function(){
			win.utils.ajaxPost(win.utils.services.orderMan_order_queryUserCity,{
				user_id: win.utils.getCookie('user_id'),
				userId: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session')
			},function(result){
				if(result.result_code == 200){
					if(result.sysUserCityVOS.length>0){
						if(result.sysUserCityVOS.length>1){
							citySearchVal='';
						}else{
							citySearchVal=result.sysUserCityVOS[0].cityId;
						}
					}else{
						citySearchVal='';
					};
					order_list.getStaNum();//根据城市id获取数据
					let str='';
					let emptyCityStr='';
					$.each(result.sysUserCityVOS,function(index,value){
						str+='<option value='+value.cityId+'>'+value.addressName+'</option>'
					});
					if(result.sysUserCityVOS.length>1){
						emptyCityStr="<option value=''>城市</option>";
					};
					str=emptyCityStr+str;
					$("#city").empty().append(str);
					$("#cityCon").empty().append(str);
					order_list.getServerData(1);
				}else{
					utils.showMessage('获取失败，请重试！');
				};
			})
		},
		//订单list----getData
		getServerData: function(pageIndex,orderId) {
			layerFlag=false;
			clearAllDataNow();
			//true时表示根据下拉状态筛选，tab切换回复默认值
			if(searchStaFlag==true){
				$("#seBtnAlls button").removeClass('btn-info');
				$("#seBtnAlls button").eq(0).addClass('btn-info');
			};
			if(progressValSears==2){
				clearInterval(setDataSix);
				order_list.getStaNum();
				//////////////setIntervalRefreshGetData();
			};
			$.each(workerIdsArrs,function(index,value){
				if(value==''){
					workerIdsArrs.splice(index,1)
				}
			});
			feedbackNo=0;
			selectId='';//清空订单id
			let datas={
				user_id: win.utils.getCookie('user_id'),
				session: win.utils.getCookie('session'),
				position: pageIndex,
				rows: 10,
				city:citySearchNameVal=='城市'?'':citySearchNameVal,//城市名称 ,
				cityId:citySearchVal,//城市id ,
				detailAddress:adressValSears,// 小区地址 ,
				districtId:districtId,// 区域id ,
				tradingArea:tradingAreaSearch,//商圈名称 ,
				tradingAreaId:tradingAreaIdSearch,//商圈id ,
				blockId:blockIdSearch ,//小区id ,
				exigencyFlag:exigencyFlagSearch,// 紧急标识(1、紧急；2、普通) ,
				firstGrade:firstGradeSearch,// 一级工单类型id ,
				secondGrade:secondGradeSearch,// 二级工单类型id ,
				linkOrderStatus:linkOrderStatus,// 关联的工单状态 ,
				houseId:houseIdSearchs ,//公寓id ,===暂无
				initiateRepairTel:repairTelValSears,// 报修电话 ,
				orderFlag:OrderFlagValSears,// 工单标签
				outOrderNo: orderIdValSears,//蛋壳工单id ,
				servicePlanId:servicePlanSer,// 维修方案id ,
				finishStatus:finishStatusSer,// 工单是否完结
				isChangeDate:isChangeDateSer,//是否改期上门
				sysNinetyRework:sysNinetyReworkSer,//90天返修工单
				accountUpgrade:jsCheckUpgradeSer,// 审核升级(1-是，2-否)
				allocationTimeEnd: orderFpTimeEn,//分派给师傅的结束时间 ,
				allocationTimeStart:orderFpTimeSt,// 分派给师傅的开始时间 ,
				appointmentDateEnd: orderYyTimeEn,//预约结束日期 ,
				appointmentDateStart: orderYyTimeSt,//预约开始日期 ,
				customerCreateTimeEnd: orderCreTimeEn,//工单创建结束时间 ,
				customerCreateTimeStart: orderCreTimeSt,//工单创建开始时间 ,
				practicalFinishTimeEnd: orderFinTimeEn,//实际完成结束时间 ,
				practicalFinishTimeStart: orderFinTimeSt,//实际完成开始时间 ,
				unusualLabel:notnormal,//异常标签（1、分配超时；2、联系超时；3、上门超时；4、反馈超时；5、配件超时） ,
				visitSignTimeEnd :orderDoorTimeEn,// 上门结束时间 ,
				visitSignTimeStart:orderDoorTimeSt,// 上门开始时间 ,
				workerContactTimeEnd: orderContTimeEn,//联系结束时间 ,
				workerContactTimeStart: orderContTimeSt,//联系开始时间 ,
				workerIds:workerIdsArrs,// 维修员id 数组
			};
			datas.checkStatus='';
			if(progressValSears==11){//已评价
				datas.evaluateStatus=2;
			}else if(progressValSears==21){//待评价
				datas.evaluateStatus=1;
			}else{
				datas.orderStatus=progressValSears;//工单状态/进度状态
				if(progressValSears==6){
					datas.checkStatus=$("#wxfaxtzPlanSta").val();
				}
			};// evaluateStatus    1:未评价2：已评价

			let nameLogin=win.utils.getCookie('user_id');
			if(nameLogin=='lyf'||nameLogin=='songhuanhuan'||nameLogin=='shaojunfeng'){
				datas.orderId=$("#baijiaxiuId").val();//工单id
			};
			pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
			win.utils.ajaxPost(win.utils.services.orderMan_order_todo_list, datas, function(result) {
				if(result.result_code == 200) {
					if(result.total == 0) {
						gridData = {};
						var gridJson = {
							total: 0,
							rows: gridData,
							page: 0,
							records: result.total
						};
						pageText = '第0页 / 共0页';
						nowCurPage= 0;//订单列表---当前页码数
						totalNowPages = 0;//订单列表---总的页码数
						$("#table").jqGrid('setGridParam', {
							pgtext: pageText
						});
						$("#table")[0].addJSONData(gridJson);
					} else {
						gridData = result.orderList;
						$.each(gridData,function(index,value){
							value.id=value.orderId;
						});
						if(gridData.length>0){
							selectId=gridData[0].id;
							selectIdSta=gridData[0].orderStatus;
						}else{
							selectId='';
							selectIdSta='';
							selectIdStaBtn='';
						};
						totalPages = Math.ceil(result.total / pageCount);
						nowCurPage= pageIndex;//订单列表---当前页码数
						totalNowPages = totalPages;//订单列表---总的页码数
						var gridJson = {
							total: totalPages,
							rows: gridData,
							page: pageIndex,
							records: result.total
						};
						pageText = '第<input id="inputNowPages" type="number" min=1 value="'+pageIndex+'">页 / 共' + totalPages + '页';
						// pageText = '第'+pageIndex+'页 / 共' + totalPages + '页';
						$("#table").jqGrid('setGridParam', {
							pgtext: pageText
						});
						$("#table")[0].addJSONData(gridJson);
					}
				} else {
					utils.showMessage('获取数据失败，请重试！');
				}
			});
		},
		//搜搜更多
		SearchConss: function(){
			initializeEditForm();
			$('#SearchConss').show()
		},
		//新增
		addWorkOrder: function(){
			initializeEditForm();
			flagInput=false;
			var i = layer.open({
				type: 1,
				area: ['80%', '80%'],
				title: '新增',
				content: $('#AddCon'),
				btn: ['保存', '取消'],
				yes: function(index, layero) {
					var str=$("#addName").val();
					if($.trim(str).length<=0){
						utils.showLayerMessage('请输入一级类别名称！');
						return;
					};
					var statuss=$("#addSta").val();
					if(statuss.length<=0){
						utils.showLayerMessage('请选择状态！');
						return;
					};
					var isHaveGoodss=$("#isHaveGoods").val();
					if(isHaveGoodss.length<=0){
						utils.showLayerMessage('请选择商品是否必填！');
						return;
					};
					var isManualClaimss=$("#ManualClaims").val();
					if(isManualClaimss.length<=0){
						utils.showLayerMessage('请选择是否手动认领！');
						return;
					};
					order_picture.allowScroll();
					layer.close(i);
					// return;
					win.utils.ajaxPost(win.utils.services.configMan_WorkOrder_add, {
						user_id: win.utils.getCookie('user_id'),
						session: win.utils.getCookie('session'),
						name:str,
						rank:1,
						status:statuss,
						isManualClaim:isManualClaimss,
						isHaveGoods:isHaveGoodss,
						isShow:$("#isShow").val(),
					}, function(result) {
						if(result.result_code == 200) {
							order_picture.allowScroll();
							layer.close(i);
							order_list.refreshServerData();
						} else {
							utils.showLayerMessage('添加失败，请稍后重试！');
						}
					});
				}
			});
		},
	};
	// 阻止空格与enter键触发layer.open事件（即出现蒙层）
	$(document).keydown(function (event) {
		var  e=window.event || event;
		var keyCode = event.keyCode;
		if (keyCode == 13 || keyCode == 32) {
			if(flagInput==false){
				if(e.preventDefault){
					e.preventDefault();
				}else{
					window.event.returnValue = false;
				}
				return false;
			}
		}
	});
	win.order_list = order_list;
})(window, jQuery);
