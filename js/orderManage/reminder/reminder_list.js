/**
 * Created by song.
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';
    //分页-当前页---当前条数
    var nowPage = 1;
    var selectId = 0;

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
        $(".form-group span").remove();
    };
    var reminder_list = {
        //初始化
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            //判断是否有催单权限，此时为订单管理的催单权限
            utils.isUserMenuIndexRight('reminder_list', function (result) {
                if (result == 1) {
                    obj.initList();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });
            // 搜索
            $('#search').on('click', function () {
                reminder_list.getServerData(1);
            });
            //刷新
            $('#empty').on('click', function () {
                obj.refreshServerData();
            });
            //催单回复
            $('#Reply').click(function () {
                utils.showMessage('催单回复。。。')
            });
            //批量处理
            $('#muchDetal').click(function () {
                utils.showMessage('批量处理。。。')
            });
            //催单待回复
            $('#reminReply').click(function () {
                utils.showMessage('催单待回复。。。')
            });
        },
        //初始化请求
        initList: function () {
            var values = {};
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: 260,//utils.getAutoGridHeightSong(),
                autowidth: true,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: 10,
                rownumbers: true,
                // rowList: [10,20,50,100],
                pgtext: "第X页 / 共X页",
                colNames: ["id", "发起人", "工单", "工单状态", "催单时间", "催单内容", "回复内容", "处理状态", "回复时间"],
                colModel: [{
                    name: "id",
                    index: "id",
                    editable: false,
                    width: 72,
                    sorttype: "string",
                    hidden: true,
                    formatter: function () {
                        return "<input type='radio'>"
                    }
                },
                    {
                        name: "people",
                        index: "people",
                        editable: false,
                        width: 100,
                        sorttype: "string"
                    },
                    {
                        name: "order",
                        index: "order",
                        editable: false,
                        width: 150,
                        sorttype: "string"
                    },
                    {
                        name: "orderstatus",
                        index: "orderstatus",
                        editable: false,
                        width: 100,
                        sorttype: "string",
                    },
                    {
                        name: "time",
                        index: "time",
                        editable: false,
                        width: 150,
                        sorttype: "string"
                    },
                    {
                        name: "content",
                        index: "content",
                        editable: false,
                        width: 420,
                        sorttype: "string",
                    },
                    {
                        name: "replyCon",
                        index: "replyCon",
                        editable: false,
                        width: 420,
                        sorttype: "string"
                    },
                    {
                        name: "action",
                        index: "action",
                        editable: false,
                        forzen: true,
                        // width: 120,
                        hidden: true,
                        sorttype: "string",
                        formatter: function (cellvalue, options, rowObject) {
                            return cellvalue;
                            // console.log(cellvalue,options, rowObject)
// 							if(cellvalue==1){
// 								return '<a href="javascript:void(0);" style="color:#337ab7;margin-right:10px;" onclick="reminder_list.zpgys(\''+ rowObject.id+ '\');">转派供应商</a><a href="javascript:void(0);" style="color:#337ab7" onclick="reminder_list.chedan(\''+ rowObject.id+ '\');">撤单</a>';
// 							}else if(cellvalue==2){
// 								return '<a href="javascript:void(0);" style="color:#337ab7;margin-right:10px;" onclick="reminder_list.contact(\''+ rowObject.id+ '\');">联系</a><a href="javascript:void(0);" style="color:#337ab7" onclick="reminder_list.gaiqi(\''+ rowObject.id+ '\');">改期</a>';
// 								
// 							}
                        },
                    },
                    {
                        name: "replyTime",
                        index: "replyTime",
                        editable: false,
                        width: 150,
                        sorttype: "string",
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        reminder_list.getServerData(currentPage);
                    }
                },
                beforeRequest: function () {
                    if (deferLoadData == 1) {
                        nowPage = currentPage;
                        reminder_list.getServerData(currentPage);
                        deferLoadData = 0;
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    reminder_list.getServerData(currentPage);
                },
                //当选择行时触发此事件。rowid：当前行id；status：选择状态，当multiselect 为true时此参数才可用
                onSelectRow: function () {
                    //返回选中的id
                    var selectedRowIndex = $("#" + this.id).getGridParam('selrow');
                    let id = parseInt(selectedRowIndex);
                    selectId = id;
                },
                gridComplete: function () {
                    $("#table").jqGrid('setSelection', 1);
                }
            });

            reminder_list.getServerData(1);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeightSong());
            });
        },
        refreshServerData: function () {
            reminder_list.getServerData(1);
        },
        getServerData: function (pageIndex) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.configMan_firstClassWorkOrder_list, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                name: "",
                status: null,
                rank: 1,
                position: 1,
                rows: 20
            }, function (result) {
                if (result.result_code == 200) {
                    if (result.total == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: result.total
                        };
                        pageText = '第0页 / 共0页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);
                    } else {
                        gridData = result.serviceItems;
                        gridData = [{
                            id: 1, people: '张福辉', order: '13235', orderstatus: '待回复', time: '2019-03-23 12:20:00',
                            content: '业主突然回国，着急住，需要把房子卫生间尽快打扫出来并且修复马桶，热水器',
                            replyCon: '—', action: '已安排', replyTime: '2019-03-23 12:32:00'
                        }];
                        result.total = 1;
                        selectId = gridData[0].id;
                        totalPages = Math.ceil(result.total / pageCount);
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
    };

    win.reminder_list = reminder_list;
})(window, jQuery);