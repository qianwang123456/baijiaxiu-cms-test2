/**
 * Create by hanlu
 */
(function (win, $) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;

    var getVillageInfoFlag = 1;
    var getReasonFlag = 1;
    var getAreaFlag = 1;
    var getProFlag = 1;
    var getPlanFlag = 1;
    var getClassifiFlag = 1;
    var getEvaluateTypeFlag = 1;
    var syncAddHouseNumFlag = 1;
    var syncRentClassifyFlag = 1;
    var reviseWorkerAreaInfoFlag = 1;
    var syncEvaluateFlag = true;

    var sysnData_list = {
        //初始化
        init: function () {
            var that = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }

            //判断是否有订单权限，此时为配置管理的工单权限
            utils.isUserMenuIndexRight('sysnData_list', function (result) {
                if (result == 1) {
                    that.bindEvent();
                } else {
                    self.location = '../../../web/error/access_refuse.html';
                    return;
                }
            });


            // 刷新
            $('#refreshBtn').on('click', function () {
                that.refresh();
            })
        },
        bindEvent: function () {
            // 获取小区信息
            $('#getVillageInfo').on('click', function () {
                if (getVillageInfoFlag) {
                    $.ajax({
                        url: win.utils.getDanKe.syncBlockInfo,
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res);
                        }
                    })
                    getVillageInfoFlag = 0;
                    $('#getVillageInfo').addClass('unified-gray-btn-global');
                    $('#getVillageInfo').attr('disabled', true);
                } else {
                    layer.msg('数据已同步！')
                }
            });

            // 获取撤单、退单原因
            $('#getReason').on('click', function () {
                if (getReasonFlag) {
                    $.ajax({
                        url: win.utils.getDanKe.syncReason,
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res);
                        }
                    })
                    getReasonFlag = 0;
                    $('#getReason').addClass('unified-gray-btn-global');
                    $('#getReason').attr('disabled', true);
                } else {
                    layer.msg('数据已同步！')
                }
            });

            // 获取行政区商圈小区
            $('#getArea').on('click', function () {
                if (getAreaFlag) {
                    $.ajax({
                        url: win.utils.getDanKe.syncDankeAreas,
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res);
                        }
                    });
                    getAreaFlag = 0;
                    $('#getArea').addClass('unified-gray-btn-global');
                    $('#getArea').attr('disabled', true);
                } else {
                    layer.msg('数据已同步！')
                }

            });

            // 获取维修一二三级项目
            $('#getPro').on('click', function () {
                if (getProFlag) {
                    $.ajax({
                        url: win.utils.getDanKe.syncServiceItem,
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res);
                        }
                    });
                    getProFlag = 0;
                    $('#getPro').addClass('unified-gray-btn-global');
                    $('#getPro').attr('disabled', true);
                } else {
                    layer.msg('数据已同步！')
                }
            });

            // 获取维修协调方案
            $('#getPlan').on('click', function () {
                if (getPlanFlag) {
                    $.ajax({
                        url: win.utils.getDanKe.syncPlanService,
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res);
                        }
                    });
                    getPlanFlag = 0;
                    $('#getPlan').addClass('unified-gray-btn-global');
                    $('#getPlan').attr('disabled', true);
                } else {
                    layer.msg('数据已同步！')
                }
            });

            // 获取租务一二级分类
            $('#getClassifi').on('click', function () {
                if (getClassifiFlag) {
                    $.ajax({
                        url: win.utils.getDanKe.syncRentClassify,
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res);
                        }
                    });
                    getClassifiFlag = 0;
                    $('#getClassifi').addClass('unified-gray-btn-global');
                    $('#getClassifi').attr('disabled', true);
                } else {
                    layer.msg('数据已同步！')
                }
            });

            // 获取评价类型
            $('#getEvaluateType').on('click', function () {
                if (getEvaluateTypeFlag) {
                    $.ajax({
                        url: win.utils.getDanKe.syncEvaluateType,
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res);
                        }
                    });
                    getEvaluateTypeFlag = 0;
                    $('#getEvaluateType').addClass('unified-gray-btn-global');
                    $('#getEvaluateType').attr('disabled', true);
                } else {
                    layer.msg('数据已同步！')
                }
            });

            // 获取小区对应的房间数量（增量）
            $('#syncAddHouseNum').on('click', function () {
                if (syncAddHouseNumFlag) {
                    $.ajax({
                        url: win.utils.getDanKe.syncAddHouseNum,
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res);
                        }
                    });
                    syncAddHouseNumFlag = 0;
                    $('#syncAddHouseNum').addClass('unified-gray-btn-global');
                    $('#syncAddHouseNum').attr('disabled', true);
                } else {
                    layer.msg('数据已同步！')
                }
            });

            // 获取小区对应的房间数量（初始化）
            $('#syncRentClassify').on('click', function () {
                if (syncRentClassifyFlag) {
                    $.ajax({
                        url: win.utils.getDanKe.syncInitHouseNum,
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res);
                        }
                    });
                    syncRentClassifyFlag = 0;
                    $('#syncRentClassify').addClass('unified-gray-btn-global');
                    $('#syncRentClassify').attr('disabled', true);
                } else {
                    layer.msg('数据已同步！')
                }
            });

            // 修正维修员对应区域基础信息
            $('#reviseWorkerAreaInfo').on('click', function () {
                if (reviseWorkerAreaInfoFlag) {
                    $.ajax({
                        url: win.utils.getDanKe.reviseWorkerAreaInfo,
                        type: 'get',
                        dataType: 'json',
                        success: function (res) {
                            // console.log(res);
                        }
                    });
                    reviseWorkerAreaInfoFlag = 0;
                    $('#reviseWorkerAreaInfo').addClass('unified-gray-btn-global');
                    $('#reviseWorkerAreaInfo').attr('disabled', true);
                } else {
                    layer.msg('数据已同步！')
                }
            });

            // 同步用户评价信息
            $('#syncEvaluate').on('click', function () {
                if (syncEvaluateFlag) {
                    var orderId = $('#orderId').val();
                    if (orderId) {
                        syncEvaluateFlag = false;
                        $.ajax({
                            url: win.utils.getDanKe.syncEvaluate,
                            type: 'get',
                            dataType: 'json',
                            data: {
                                "orderId": orderId
                            },
                            success: function (res) {
                                layer.msg('评价信息获取成功！');
                                $('.result').html(res.msg);
                                $('.result').show();

                                syncEvaluateFlag = true;
                            },
                            fail: function (res) {
                                layer.msg('评价信息获取失败！');
                                $('.result').html('');
                                $('.result').hide();
                            }
                        })
                    } else {
                        layer.msg('请输入订单号');
                    }
                }
            });
        },
        // 刷新
        refresh: function () {
            getVillageInfoFlag = 1;
            $('#getVillageInfo').removeClass('unified-gray-btn-global');
            $('#getVillageInfo').removeAttr('disabled');

            getReasonFlag = 1;
            $('#getReason').removeClass('unified-gray-btn-global');
            $('#getReason').removeAttr('disabled');

            getAreaFlag = 1;
            $('#getArea').removeClass('unified-gray-btn-global');
            $('#getArea').removeAttr('disabled');

            getProFlag = 1;
            $('#getPro').removeClass('unified-gray-btn-global');
            $('#getPro').removeAttr('disabled');

            getPlanFlag = 1;
            $('#getPlan').removeClass('unified-gray-btn-global');
            $('#getPlan').removeAttr('disabled');

            getClassifiFlag = 1;
            $('#getClassifi').removeClass('unified-gray-btn-global');
            $('#getClassifi').removeAttr('disabled');

            getEvaluateTypeFlag = 1;
            $('#getEvaluateType').removeClass('unified-gray-btn-global');
            $('#getEvaluateType').removeAttr('disabled');

            syncAddHouseNumFlag = 1;
            $('#syncAddHouseNum').removeClass('unified-gray-btn-global');
            $('#syncAddHouseNum').removeAttr('disabled');

            syncRentClassifyFlag = 1;
            $('#syncRentClassify').removeClass('unified-gray-btn-global');
            $('#syncRentClassify').removeAttr('disabled');

            reviseWorkerAreaInfoFlag = 1;
            $('#reviseWorkerAreaInfo').removeClass('unified-gray-btn-global');
            $('#reviseWorkerAreaInfo').removeAttr('disabled');

            syncEvaluateFlag = true;
        },
    };

    win.sysnData_list = sysnData_list;
})(window, jQuery);