/**
 * Created by hanlu.
 */
(function (win, $, qiniu) {
    //延迟加载数据，主要是为了解决切换显示行数，行数不能及时更新的问题
    var deferLoadData = 0;
    var orderByString = '';

    var nowPage = 1;

    // //image urls
    // var cardImageUrl = '';
    var flagInput = false;

    var getNoticeListData = {
        user_id: win.utils.getCookie('user_id'),
        session: win.utils.getCookie('session'),
        rows: 20,
        position: nowPage,
        id: null
    };

    function initializeEditForm() {
        $(".form-group").removeClass("has-error");
    }

    var notice_list = {
        init: function () {
            var obj = this;
            if (utils.isLogin() == false) {
                utils.loginDomain();
                return;
            }
            // 公告管理权限
            utils.isUserMenuIndexRight('notice_list', function (result) {
                if (result == 1) {
                    //initialize...
                    // win.utils.clearSpaces();
                    obj.initList();
                } else {
                    self.location = '../../web/error/access_refuse.html';
                    return;
                }
            });
            // // 刷新
            // $('#btnQuery').click(obj.refersh);
            // 增加
            $('#btnAdd').click(obj.addRecord);
            // 修改
            $('#table').on('click', '#btnUpdate', function () {
                var id = $(this).attr('data-id');
                obj.updateRecord(id);
            });
            // 删除
            $('#table').on('click', '#btnDel', function () {
                var id = $(this).attr('data-id');
                obj.deleteRecord(id);
            });
            // 发布
            $('#table').on('click', '#btnPublish', function () {
                var id = $(this).attr('data-id');
                obj.publishRecord(id);
            });
            // 取消发布
            $('#table').on('click', '#btnUnPublish', function () {
                var id = $(this).attr('data-id');
                obj.unpublishRecord(id);
            });

            // 查看公告详情
            $('#table').on('click', 'a', function () {
                notice_list.toDetail($(this).attr('data-id'));
            });

            // obj.getServerData(nowPage, getNoticeListData);

            $('#content').on('focus', function () {
                flagInput = true;
            });

            $(document).keydown(function (event) {
                var e = window.event || event;
                var keyCode = event.keyCode;
                if (keyCode == 13 || keyCode == 32) {
                    if (flagInput == false) {
                        if (e.preventDefault) {
                            e.preventDefault();
                        } else {
                            window.event.returnValue = false;
                        }
                        return false;
                    }
                }
            });
        },
        initList: function () {
            $("#table").jqGrid({
                url: '',
                styleUI: 'Bootstrap',
                datatype: "json",
                height: utils.getAutoGridHeight(),
                autowidth: false,
                shrinkToFit: false,
                autoScroll: true,
                rowNum: 20,
                rownumbers: true,
                rownumWidth: 60,
                // rowList: [20, 50, 100],
                pgtext: "第X页 / 共X页",
                colNames: ["操作", "公告", "发布状态", "创建时间", "创建者", "操作人", "修改时间", "发布时间", "查看详情"],
                colModel: [
                    {
                        name: "publishStatus",
                        index: "publishStatus",
                        editable: false,
                        width: 280,
                        sortable: false,
                        formatter: optionsBtnFn
                    },
                    {
                        name: "title",
                        index: "title",
                        editable: false,
                        width: 280,
                        sortable: false,
                        formatter: function (cellvalue, options, rowObject) {
                            return '<a class="toDetail unified-a" data-id=' + rowObject.id + '>' + rowObject.title + '</a>'
                        }
                    },
                    {
                        name: "publishStatus",
                        index: "publishStatus",
                        editable: false,
                        width: 160,
                        sortable: false,
                        formatter: publishTimeText
                    },
                    {
                        name: "createTime",
                        index: "createTime",
                        editable: false,
                        width: 180,
                        sorttype: "string",
                        // hidden: true
                    },
                    {
                        name: "createUser",
                        index: "createUser",
                        editable: false,
                        width: 160,
                        sortable: false,
                        hidden: true
                    },
                    {
                        name: "modifyUser",
                        index: "modifyUser",
                        editable: false,
                        width: 160,
                        sortable: false,
                        formatter: updateUser
                    },
                    {
                        name: "modifyTime",
                        index: "modifyTime",
                        editable: false,
                        width: 180,
                        sorttype: "string",
                        formatter: updateTime
                    },
                    {
                        name: "publishTime",
                        index: "publishTime",
                        editable: false,
                        width: 160,
                        sortable: false,
                        hidden: true
                    },
                    {
                        name: "options",
                        index: "options",
                        editable: false,
                        width: 160,
                        sortable: false,
                        hidden: true,
                        formatter: function (cellvalue, options, rowObject) {
                            return '<a class="toDetail" class="unified-a" data-id=' + rowObject.id + '>查看详情</a>'
                        }
                    },
                ],
                pager: "#table_pager",
                viewrecords: true,
                pagerpos: "center",
                recordpos: "left",
                caption: "",
                hidegrid: false,
                // loadonce:true, //一次加载全部数据到客户端，由客户端进行排序。
                // sortable: true,
                // sortname: 'modifyTime', //设置默认的排序列
                // sortorder: 'desc',
                onPaging: function (pgButton) {
                    deferLoadData = 0;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    lastPage = $("#table").jqGrid('getGridParam', 'lastpage');

                    if (pgButton == 'next') {
                        currentPage = currentPage + 1;
                    }
                    if (pgButton == 'last') {
                        currentPage = lastPage;
                    }
                    if (pgButton == 'prev') {
                        currentPage = currentPage - 1;
                    }
                    if (pgButton == 'first') {
                        currentPage = 1;
                    }
                    if (pgButton == 'user') {
                        deferLoadData = 1;
                    }
                    if (pgButton == 'records') {
                        deferLoadData = 1;
                    }
                    nowPage = currentPage;
                    if (deferLoadData == 0) {
                        var getNoticeListData = {
                            user_id: win.utils.getCookie('user_id'),
                            session: win.utils.getCookie('session'),
                            rows: 20,
                            position: nowPage,
                            id: null
                        };
                        notice_list.getServerData(nowPage, getNoticeListData);
                    }
                },
                onSortCol: function (index, iCol, sortorder) {
                    alert('onSortCol')
                    orderByString = index + ' ' + sortorder;
                    currentPage = $("#table").jqGrid('getGridParam', 'page');
                    nowPage = currentPage;
                    var getNoticeListData = {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        rows: 20,
                        position: nowPage,
                        id: null
                    };
                    notice_list.getServerData(nowPage, getNoticeListData);
                },
            });

            notice_list.getServerData(nowPage, getNoticeListData);

            $("#table").jqGrid("navGrid", "#table_pager", {
                edit: false,
                add: false,
                del: false,
                search: false,
                refresh: false
            });

            var b = $(".jqGrid_wrapper").width();
            $("#table").setGridWidth(b);
            $(window).bind("resize", function () {
                var b = $(".jqGrid_wrapper").width();
                $("#table").setGridWidth(b);
                $("#table").setGridHeight(utils.getAutoGridHeight());
            });

            function publishTimeText(cellvalue, options, rowObject) {
                if (rowObject.publishStatus) {
                    if (rowObject.publishStatus == 1) {
                        return '已发布'
                    } else if (rowObject.publishStatus) {
                        return '未发布'
                    }
                }
            };

            function optionsBtnFn(cellvalue, options, rowObject) {
                // 已发布
                if (rowObject.publishStatus == 1) {
                    return '<button id="btnUnPublish" class="btn btn-info unified-btn-inline" type="button" data-id="' + rowObject.id + '">取消发布</button>'
                }
                // 未发布
                else if (rowObject.publishStatus == 2) {
                    return '<button id="btnPublish" class="btn btn-info unified-btn-inline" type="button" data-id="' + rowObject.id + '">发布</button>' +
                        '<button id="btnUpdate" class="btn btn-primary unified-btn-inline unified-change-btn-inline" type="button"data-id="' + rowObject.id + '">修改</button>' +
                        '<button id="btnDel" class="btn btn-danger unified-btn-inline unified-deleted-btn-inline" type="button" data-id="' + rowObject.id + '">删除</button>'
                } else if (rowObject.publishStatus == 3) {
                    return '<button id="btnDel" class="btn btn-danger unified-btn-inline unified-deleted-btn-inline" type="button" data-id="' + rowObject.id + '">删除</button>'
                }
            };

            function updateTime(cellvalue, options, rowObject) {
                if (rowObject.modifyTime) {
                    return rowObject.modifyTime;
                } else {
                    return '';
                }
            };

            function updateUser(cellvalue, options, rowObject) {
                if (rowObject.modifyUser) {
                    return rowObject.modifyUser;
                } else {
                    return rowObject.createUser;
                }
            }
        },

        // 获取公告列表
        getServerData: function (pageIndex, postData) {
            pageCount = $("#table").jqGrid('getGridParam', 'rowNum');
            win.utils.ajaxPost(win.utils.services.getNoticeInfo, postData, function (result) {
                if (result.result_code == 200) {
                    totalPages = Math.ceil(result.total / pageCount);
                    if (result.noticeInfos == 0) {
                        gridData = {};
                        var gridJson = {
                            total: 0,
                            rows: gridData,
                            page: 0,
                            records: 0
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                        utils.showMessage('暂无数据！');
                    } else {
                        gridData = result.noticeInfos;
                        gridData.forEach(function (i, k) {
                            i.createTime = i.createTime ? notice_list.dateFormat(i.createTime) : '';
                            i.modifyTime = i.modifyTime ? notice_list.dateFormat(i.modifyTime) : '';
                            i.publishTime = i.publishTime ? notice_list.dateFormat(i.publishTime) : '';
                        });
                        var gridJson = {
                            total: totalPages,
                            rows: gridData,
                            page: pageIndex,
                            records: result.total
                        };
                        pageText = '第' + pageIndex + '页 / 共' + totalPages + '页';
                        $("#table").jqGrid('setGridParam', {
                            pgtext: pageText
                        });
                        $("#table")[0].addJSONData(gridJson);

                    }
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                }
            });
        },
        // 增加公告
        addRecord: function () {
            notice_list.layerInit();
            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '<i class="fa fa-plus">&nbsp;&nbsp;添加公告</i>',
                content: $('#model'),
                btn: ['保存', '取消'],
                success: function () {
                    $('#txtNewTitle').removeAttr('disabled');

                    flagInput = false;
                },
                yes: function (index, layero) {
                    var addNoticeTit = $('#txtNewTitle').val();
                    var addNoticeContent = $('textarea[name=content]').val();

                    if (addNoticeTit == '') {
                        utils.showLayerMessage('请添加标题！');
                        return;
                    }
                    if (addNoticeTit.length > 25) {
                        utils.showLayerMessage('标题不可超过25个字！');
                        return;
                    }
                    ;
                    if (addNoticeContent.trim() == '') {
                        utils.showLayerMessage('请添加公告内容！');
                        return;
                    }
                    win.utils.ajaxPost(win.utils.services.addNoticeInfo, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        title: addNoticeTit.trim(),
                        content: addNoticeContent.trim()
                    }, function (result) {
                        if (result.result_code == 200) {
                            utils.showMessage('添加成功！');
                            layer.close(i);
                            nowPage = 1;
                            notice_list.refersh();
                        } else {
                            utils.showLayerMessage('添加失败，请重试！');
                        }
                    });

                }
            });
        },
        // 增加公告弹层初始化
        layerInit: function () {
            $('#txtNewTitle').val('');
            $('textarea[name=content]').val('')
        },
        // 修改公告
        updateRecord: function (id) {
            // initializeEditForm();
            // var data = $("#table").jqGrid('getGridParam', 'selrow');
            if (id == null) {
                utils.showMessage('请选择要进行修改的记录！');
                return;
            }
            var publishStatus = notice_list.getPublishStatus(id);
            if (publishStatus == 1) {
                utils.showMessage('该记录已经发布，不可修改！');
                return;
            }
            win.utils.ajaxPost(win.utils.services.getNoticeInfo, {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                id: id
            }, function (result) {
                if (result.result_code == 200) {
                    $('#txtNewTitle').val(result.noticeInfos[0].title);
                    // $('#summernote').summernote('code', result.noticeInfos[0].content);
                    $('textarea[name=content]').val(result.noticeInfos[0].content)
                    // $('#content').text(result.noticeInfos[0].content);
                } else {
                    utils.showMessage('获取数据失败，请重试！');
                    return;
                }
            });

            var i = layer.open({
                type: 1,
                area: ['80%', '80%'],
                title: '<i class="fa fa-pencil"></i>&nbsp;&nbsp;修改公告',
                content: $('#model'),
                btn: ['保存', '取消'],
                success: function () {
                    $('#txtNewTitle').attr('disabled', true);
                    flagInput = false;
                },
                yes: function (index, layero) {
                    var updateNoticeTit = $('#txtNewTitle').val();
                    var updateNoticeContent = $('textarea[name=content]').val();

                    if (updateNoticeTit == '') {
                        utils.showLayerMessage('请添加标题！');
                        return;
                    }
                    if (updateNoticeTit.length > 25) {
                        utils.showMessage('标题不可超过25个字！');
                        return;
                    }
                    ;
                    if (updateNoticeContent.trim() == '') {
                        utils.showLayerMessage('请添加公告内容！');
                        return;
                    }
                    ;
                    win.utils.ajaxPost(win.utils.services.updateNoticeInfo, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        id: id,
                        title: updateNoticeTit,
                        content: updateNoticeContent
                    }, function (result) {
                        if (result.result_code == 200) {
                            layer.close(i);
                            // notice_list.refersh();
                            utils.showMessage('修改成功!');
                            $('#table').jqGrid('setRowData', id, {
                                content: updateNoticeContent,
                                title: updateNoticeTit,
                                modifyUser: win.utils.getCookie('user_id'),
                                modifyTime: notice_list.dateFormat(new Date().getTime()),
                                id: id
                            });
                        } else {
                            utils.showMessage('修改失败，请重试！');
                            return;
                        }
                    });
                },
            });
        },
        // 删除公告
        deleteRecord: function (id) {
            utils.showConfirm('您确定要删除这条记录吗？', '删除', function () {
                win.utils.ajaxPost(win.utils.services.deleteNoticeInfo, {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    id: id
                }, function (result) {
                    if (result.result_code == 200) {
                        utils.closeMessage();
                        $('#table').jqGrid('delRowData', id);
                        // notice_list.refersh();
                    } else {
                        utils.showMessage('删除失败，请重试！');
                    }
                });
            });
        },
        // 发布公告
        publishRecord: function (id) {
            var rowData = $('#table').jqGrid('getRowData', id);
            utils.showConfirm('您确定要发布此公告吗？', '确定', function () {
                win.utils.ajaxPost(win.utils.services.updateNoticeInfo, {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    id: id,
                    publishStatus: 1,
                    title: $(rowData.title).text()
                }, function (result) {
                    if (result.result_code == 200) {
                        utils.closeMessage();
                        $('#table').jqGrid('setRowData', id, {
                            publishStatus: 1,
                            id: id
                        });
                        // notice_list.refersh();
                    } else {
                        utils.showMessage('操作失败，请重试！');
                    }
                });
            });
        },
        // 取消发布
        unpublishRecord: function (id) {
            utils.showConfirm('您确定要取消发布吗？', '确定', function () {
                win.utils.ajaxPost(win.utils.services.updateNoticeInfo, {
                    user_id: win.utils.getCookie('user_id'),
                    session: win.utils.getCookie('session'),
                    id: id,
                    publishStatus: 3
                }, function (result) {
                    if (result.result_code == 200) {
                        utils.closeMessage();
                        $('#table').jqGrid('setRowData', id, {
                            publishStatus: 3,
                            id: id
                        });
                    } else {
                        utils.showMessage('操作失败，请重试！');
                    }
                });
            });
        },
        // 查看公告详情
        toDetail: function (id) {
            var i = layer.open({
                type: 1,
                area: ['60%', '80%'],
                title: '<i class="fa fa-eye">&nbsp;&nbsp;查看公告详情</i>',
                content: $('#detail'),
                // btn: ['保存', '取消'],
                success: function (index) {
                    win.utils.ajaxPost(win.utils.services.getNoticeInfo, {
                        user_id: win.utils.getCookie('user_id'),
                        session: win.utils.getCookie('session'),
                        id: id
                    }, function (result) {
                        if (result.result_code == 200) {
                            var notice_info = result.noticeInfos[0];

                            $('#detail h2').html(notice_info.title);
                            $('#detail .creatInfo').html(' <p style="margin-right: 20px;">创建人：' + notice_info.createUser + '</p><p>创建时间：' + notice_list.timeStampFormat(notice_info.createTime) + '</p>');
                            $('#detail .noticeContent').html(notice_info.content);
                        } else {
                            utils.showMessage('获取公告信息失败，请重试！');
                            layer.close(index);
                        }
                    });

                    flagInput = false;
                },
                yes: function (index, layero) {
                    layer.close(i);
                }
            });
        },
        // 刷新
        refersh: function () {
            $('#txtNewTitle').val('');
            $('#content').text('');
            getNoticeListData = {
                user_id: win.utils.getCookie('user_id'),
                session: win.utils.getCookie('session'),
                rows: 20,
                position: nowPage,
                id: null
            };
            notice_list.getServerData(nowPage, getNoticeListData);
        },
        // 时间格式转换
        dateFormat: function (timestamp) {
            var date = new Date(timestamp);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
            h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y + M + D + h + m + s;
        },
        // 获取选中数据的发布状态
        getPublishStatus: function (id) {
            var status;
            gridData.forEach(function (i, k) {
                if (i.id == id) {
                    status = i.publishStatus;
                }
            });
            return status;
        },
        // base64转blob
        dataURItoBlob: function (base64Data) {
            var byteString;
            if (base64Data.split(',')[0].indexOf('base64') >= 0)
                byteString = atob(base64Data.split(',')[1]);
            else
                byteString = unescape(base64Data.split(',')[1]);
            var mimeString = base64Data.split(',')[0].split(':')[1].split(';')[0];
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ia], {
                type: mimeString
            });
        },
        /**七牛云上传图片
         * cardImageUrl-----需要上传图片的bolb地址
         * token-----token
         * fileName-----上传图片的原文件名
         * summernoteObj------显示图片的盒子
         * */
        qiniuUpload: function (cardImageUrl, fileName, summernoteObj) {
            $.ajax({
                url: win.utils.services.getQiniuToken,
                type: 'get',
                dataType: 'json',
                success: function (res) {
                    var qiniuToken = res.token;
                    var observable = qiniu.upload(
                        cardImageUrl,
                        fileName,
                        // 'bjx_'+new Date().getTime()+(parseInt(Math.random()*100+1)),
                        qiniuToken,
                        {
                            fname: fileName,
                            params: {}, //用来放置自定义变量
                            mimeType: null
                        }, {
                            useCdnDomain: true,
                            // region: qiniu.region.z0
                            region: null
                        }
                    );
                    observable.subscribe({
                        // next(res){
                        // 	console.log(res);
                        // },
                        // error(err){
                        // 	console.log(err);
                        // },
                        complete(res) {
                            // console.log(res);
                            var qiniuImgUrl = 'http://prt8mczfk.bkt.clouddn.com/' + res.key;
                            summernoteObj.summernote('insertImage', qiniuImgUrl);
                            $('input[type="file"]').val("");
                        }
                    });
                }
            });
        },
        // 时间戳转时间格式
        timeStampFormat: function (timestamp) {
            var date = new Date(timestamp);
            Y = date.getFullYear() + '-';
            M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
            D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
            h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y + M + D + h + m + s;
        },
    };
    win.notice_list = notice_list;
})(window, jQuery, qiniu);
